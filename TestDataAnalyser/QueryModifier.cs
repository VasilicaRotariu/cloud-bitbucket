﻿using System;
using System.Text;
using System.Windows.Forms;

namespace SudburyDataAnalyser
{
    /// <summary>
    /// Class used for modifying queries for databases that do not fit the FT structure.
    /// </summary>
    public class QueryModifier
    {
        /// <summary>
        /// Gets or sets a value indicating whether the database follows the FT structure.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public static bool FTDBStructure { get; set; }

        /// <summary>
        /// Converts open loop query from old database structure ("FT") to new database structure style ("Non-FT").
        /// </summary>
        /// <param name="query">Query string to be converted.</param>
        // ReSharper disable once InconsistentNaming
        public static void ModifyOLQueryForNonFT(StringBuilder query)
        {
            string start = query.ToString();

            if (!FTDBStructure)
            {
                // Replace table names and column names for Non-FT db structure
                //    - [FTHeader] -> [Header]
                //        - LuaSVNVersion -> LuaSVN
                //        - IDENT -> PassOffSpec
                //        - TestType -> N/A
                //        - KeepMe -> N/A
                //        - N/A -> OID
                //        - N/A -> NID
                //query.Replace("[FTHeader]", "[Header]");
                //query.Replace("LuaSVNVersion", "LuaSVN");
                query.Replace("IDENT", "PassOffSpec");

                //    - [FTDataHeader] -> [DataHeader]
                //query.Replace("[FTDataHeader]", "[DataHeader]");

                //    - [FTHeaderNCV] -> [HeaderOL]
                //        - URIDNCV -> URIDOL
                //        - Duration -> Logic
                //query.Replace("[FTHeaderNCV]", "[HeaderOL]");
                query.Replace("URIDNCV", "URIDOL");
                query.Replace(".Duration", ".Logic");

                //    - [FTDataNCV] -> [DataOL]
                //        - URIDNCV -> URIDOL     Already replaced above.
                //query.Replace("[FTDataNCV]", "[DataOL]");

                //    - [FTNameIDs] -> [NameID]
                //query.Replace("[FTNameIDs]", "[NameID]");

                //    - [FTResults] -> [Results]
                //query.Replace("[FTResults]", "[Results]");

                //    - [FTTrims] -> [Trims]
                //query.Replace("[FTTrims]", "[Trims]");

                //    - [FTTrimCode] -> [TrimCode]
                //query.Replace("[FTTrimCode]", "[TrimCode]");

                //    - [FTEarlyEnd] -> [EarlyEnd]
                //query.Replace("[FTEarlyEnd]", "[EarlyEnd]");

                //    - [FTTraces] -> [Trace]
                //        - BinaryTraceData -> Trace
                //query.Replace("[FTTraces]", "[ChartTrace]");
                query.Replace("BinaryTraceData", "Trace");

                //    - [FTNominalDataset] -> [NominalDataset]
                //query.Replace("[FTNominalDataset]", "[NominalDataset]");
                if (query.ToString() != start)
                {
                    //throw new Exception("ModifyOLQueryForNonFT exception");
                }

                //nameid on new db is plantnumber specific
                query.Replace("(name.NameID = r.NameID AND name.Name = 'Global')", "(name.NameID = r.NameID AND name.Name = 'Global' AND name.PlantNumber=r.PlantNumber)");
                query.Replace("(name.NameID = ncvda.NameID)", "(name.NameID = ncvda.NameID AND name.PlantNumber=ncvda.PlantNumber)");

                //query.Replace("(name.NameID = cltda.NameID)", "(name.NameID = cltda.NameID AND name.PlantNumber=cltda.PlantNumber)");
                //query.Replace("(name.NameID = fodda.NameID)", "(name.NameID = fodda.NameID AND name.PlantNumber=fodda.PlantNumber)");
                //query.Replace("(name.NameID = re.NameID)", "(name.NameID = re.NameID AND name.PlantNumber=re.Plantnumber)");
                //query.Replace("(name.NameID = tr.NameID)", "(name.NameID = tr.NameID AND name.Plantnumber=tr.Plantnumber)");
                // For Calibration rigs, -1 old incomplete, -5 new incomplete value.
                query.Replace("re.Grade = -1", "re.Grade = -5");
                query.Replace("re.Grade != -1", "re.Grade != -5");

                if (query.ToString() != start)
                {
                    LogDifference(start, query.ToString());
                }
            }
        }

        /// <summary>
        /// Converts open loop query from old database structure ("FT") to new database structure style ("Non-FT").
        /// </summary>
        /// <param name="oldQuery">Query string to be converted.</param>
        /// <returns>Modified query string.</returns>
        // ReSharper disable once InconsistentNaming
        public static string ModifyOLQueryForNonFT(string oldQuery)
        {
            var query = new StringBuilder();
            query.Append(oldQuery);

            ModifyOLQueryForNonFT(query);

            return query.ToString();
        }

        /// <summary>
        /// Closed loop query field and table name modifiers for new and old database structures.
        /// </summary>
        /// <param name="query">Query string to be modified.</param>
        // ReSharper disable once InconsistentNaming
        public static void ModifyCLQueryForNonFT(StringBuilder query)
        {
            var start = query.ToString();

            if (!FTDBStructure)
            {
                // Replace table names and column names for Non-FT db structure
                //    - [FTHeader] -> [Header]
                //        - LuaSVNVersion -> LuaSVN
                //        - IDENT -> PassOffSpec
                //        - TestType -> N/A
                //        - KeepMe -> N/A
                //        - N/A -> OID
                //        - N/A -> NID
                //query.Replace("[FTHeader]", "[Header]");
                //query.Replace("LuaSVNVersion", "LuaSVN");
                query.Replace("IDENT", "PassOffSpec");

                //    - [FTDataHeader] -> [DataHeader]
                //query.Replace("[FTDataHeader]", "[DataHeader]");

                //    - [FTHeaderCLT] -> [HeaderCL]
                //        - URIDCLT -> URIDCL
                //        - Duration -> Logic1
                //        - FuelDemand -> FuelDemand1
                //query.Replace("[FTHeaderCLT]", "[HeaderCL]");
                //query.Replace("URIDCLT", "URIDCL");
                //query.Replace("FuelDemand", "FuelDemand1");
                query.Replace("Duration", "Logic1");

                //    - [FTDataCLT] -> [DataCL]
                //        - URIDCLT -> URIDCL     Already replaced above.
                //query.Replace("[FTDataCLT]", "[DataCL]");

                //    - [FTNameIDs] -> [NameID]
                //query.Replace("[FTNameIDs]", "[NameID]");

                //    - [FTResults] -> [Results]
                //query.Replace("[FTResults]", "[Results]");

                //    - [FTTrims] -> [Trims]
                //query.Replace("[FTTrims]", "[Trims]");

                //TEBBO! nameid link with plantnumber on new db
                //query.Replace("(name.NameID = re.NameID)", "(name.NameID = re.NameID AND name.PlantNumber=re.PlantNumber)");
                //query.Replace("(name.NameID = tr.NameID)", "(name.NameID = tr.NameID AND name.Plantnumber=tr.Plantnumber)");
            }

            if (query.ToString() != start)
            {
                LogDifference(start, query.ToString());
            }
        }

        /// <summary>
        /// Converts closed loop query from old database structure ("FT") to new database structure style ("Non-FT").
        /// </summary>
        /// <param name="oldQuery">Query string to be converted.</param>
        /// <returns>Modified query string.</returns>
        // ReSharper disable once InconsistentNaming
        public static string ModifyCLQueryForNonFT(string oldQuery)
        {
            var query = new StringBuilder();
            query.Append(oldQuery);

            ModifyCLQueryForNonFT(query);

            return query.ToString();
        }

        /// <summary>
        /// When the query modifier detects a difference, this is logged.
        /// </summary>
        /// <param name="unmodified">The query before QueryModifier is called.</param>
        /// <param name="modified">The query after QueryModifier is called.</param>
        private static void LogDifference(string unmodified, string modified)
        {
            DebugLog.WriteStart("QUERY MODIFIER Difference Found");
            DebugLog.WriteLine(string.Empty);
            DebugLog.WriteLine(unmodified);
            DebugLog.WriteLine(string.Empty);
            DebugLog.WriteLine(modified);
            DebugLog.WriteLine(string.Empty);
            DebugLog.WriteLine(Environment.StackTrace);
            DebugLog.WriteEnd("QUERY MODIFIER Difference Found");

            // ReSharper disable LocalizableElement
#if DEBUG
            MessageBox.Show("Information added to log", "QueryModifier difference found");
#endif
            // ReSharper restore LocalizableElement
        }
    }
}
