﻿namespace SudburyDataAnalyser
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title3 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title4 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title5 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title6 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend7 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title7 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea9 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend8 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title8 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea10 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend9 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title9 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea11 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend10 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title10 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea12 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend11 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title11 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea13 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend12 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title12 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea14 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend13 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title13 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea15 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Title title14 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea16 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Title title15 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea17 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Title title16 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea18 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Title title17 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea19 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Title title18 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea20 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend14 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title19 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea21 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend15 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Legend legend16 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title20 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("Mean");
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("Median");
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("Min");
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("Max");
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("SD");
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("Mean");
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem("Median");
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem("Min");
            System.Windows.Forms.ListViewItem listViewItem9 = new System.Windows.Forms.ListViewItem("Max");
            System.Windows.Forms.ListViewItem listViewItem10 = new System.Windows.Forms.ListViewItem("SD");
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.ListViewItem listViewItem11 = new System.Windows.Forms.ListViewItem("Mean");
            System.Windows.Forms.ListViewItem listViewItem12 = new System.Windows.Forms.ListViewItem("Median");
            System.Windows.Forms.ListViewItem listViewItem13 = new System.Windows.Forms.ListViewItem("Min");
            System.Windows.Forms.ListViewItem listViewItem14 = new System.Windows.Forms.ListViewItem("Max");
            System.Windows.Forms.ListViewItem listViewItem15 = new System.Windows.Forms.ListViewItem("SD");
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.ListViewItem listViewItem16 = new System.Windows.Forms.ListViewItem("Mean");
            System.Windows.Forms.ListViewItem listViewItem17 = new System.Windows.Forms.ListViewItem("Median");
            System.Windows.Forms.ListViewItem listViewItem18 = new System.Windows.Forms.ListViewItem("Min");
            System.Windows.Forms.ListViewItem listViewItem19 = new System.Windows.Forms.ListViewItem("Max");
            System.Windows.Forms.ListViewItem listViewItem20 = new System.Windows.Forms.ListViewItem("SD");
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.ListViewItem listViewItem21 = new System.Windows.Forms.ListViewItem("Mean");
            System.Windows.Forms.ListViewItem listViewItem22 = new System.Windows.Forms.ListViewItem("Median");
            System.Windows.Forms.ListViewItem listViewItem23 = new System.Windows.Forms.ListViewItem("Min");
            System.Windows.Forms.ListViewItem listViewItem24 = new System.Windows.Forms.ListViewItem("Max");
            System.Windows.Forms.ListViewItem listViewItem25 = new System.Windows.Forms.ListViewItem("SD");
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.ListViewItem listViewItem26 = new System.Windows.Forms.ListViewItem("Mean");
            System.Windows.Forms.ListViewItem listViewItem27 = new System.Windows.Forms.ListViewItem("Median");
            System.Windows.Forms.ListViewItem listViewItem28 = new System.Windows.Forms.ListViewItem("Min");
            System.Windows.Forms.ListViewItem listViewItem29 = new System.Windows.Forms.ListViewItem("Max");
            System.Windows.Forms.ListViewItem listViewItem30 = new System.Windows.Forms.ListViewItem("SD");
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.ListViewItem listViewItem31 = new System.Windows.Forms.ListViewItem("Mean");
            System.Windows.Forms.ListViewItem listViewItem32 = new System.Windows.Forms.ListViewItem("Median");
            System.Windows.Forms.ListViewItem listViewItem33 = new System.Windows.Forms.ListViewItem("Min");
            System.Windows.Forms.ListViewItem listViewItem34 = new System.Windows.Forms.ListViewItem("Max");
            System.Windows.Forms.ListViewItem listViewItem35 = new System.Windows.Forms.ListViewItem("SD");
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.ListViewItem listViewItem36 = new System.Windows.Forms.ListViewItem("Data");
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.ListViewItem listViewItem37 = new System.Windows.Forms.ListViewItem("Data");
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea22 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend17 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title21 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea23 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend18 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title22 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea24 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend19 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title23 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            this.mainStatusStrip = new System.Windows.Forms.StatusStrip();
            this.mainStatusStripLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.viewChangelogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.mainTabControl = new System.Windows.Forms.TabControl();
            this.queryTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.checkBoxUseSudburyArchive = new System.Windows.Forms.CheckBox();
            this.incompletePumpingQueryTextBox = new System.Windows.Forms.TextBox();
            this.completePumpingQueryTextBox = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.injCountLabel = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.databaseGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel38 = new System.Windows.Forms.TableLayoutPanel();
            this.vsql08NewDBRadioButton = new System.Windows.Forms.RadioButton();
            this.vsql08OldDBRadioButton = new System.Windows.Forms.RadioButton();
            this.dafDB01RadioButton = new System.Windows.Forms.RadioButton();
            this.mdegDB01RadioButton = new System.Windows.Forms.RadioButton();
            this.passesFailsGroupBox = new System.Windows.Forms.GroupBox();
            this.passesAndFailsRadioButton = new System.Windows.Forms.RadioButton();
            this.failsOnlyRadioButton = new System.Windows.Forms.RadioButton();
            this.passesOnlyRadioButton = new System.Windows.Forms.RadioButton();
            this.allowForcedTrimCheckBox = new System.Windows.Forms.CheckBox();
            this.testsForEachISNGroupBox = new System.Windows.Forms.GroupBox();
            this.allTestsRadioButton = new System.Windows.Forms.RadioButton();
            this.latestTestsRadioButton = new System.Windows.Forms.RadioButton();
            this.firstTimeTestsRadioButton = new System.Windows.Forms.RadioButton();
            this.allowIncompleteTestsCheckBox = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lopListDataGridView = new System.Windows.Forms.DataGridView();
            this.specificutidsTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.specificISNsTextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.testTypeListDataGridView = new System.Windows.Forms.DataGridView();
            this.label16 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.specificutidsCheckBox = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.testplanListDataGridView = new System.Windows.Forms.DataGridView();
            this.specificISNsCheckBox = new System.Windows.Forms.CheckBox();
            this.lineNumberListDataGridView = new System.Windows.Forms.DataGridView();
            this.completeCalibrationQueryTextBox = new System.Windows.Forms.TextBox();
            this.plantNumberListDataGridView = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.runMainQueryButton = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.incompleteCalibrationQueryTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.NumericISNCheckBox = new System.Windows.Forms.CheckBox();
            this.checkBoxUseIasiArchive = new System.Windows.Forms.CheckBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.datasetTabPage = new System.Windows.Forms.TabPage();
            this.DatasetTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.trimTrackerChartsTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.trimTrackerPlotGradesCheckBox = new System.Windows.Forms.CheckBox();
            this.trimTrackerLeakdownChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.trimTrackerCDChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.trimTrackerP3Chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.trimTrackerMDPChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.trimTrackerP1Chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.trimTrackerODChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.trimTrackerP4Chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.trimTrackerP5Chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.trimTrackerP2Chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label60 = new System.Windows.Forms.Label();
            this.trimTrackerPlotChronologicalRadioButton = new System.Windows.Forms.RadioButton();
            this.trimTrackerPlotDateRadioButton = new System.Windows.Forms.RadioButton();
            this.pumpingTrackerChartsTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.pumpingTrackerOMVT3FixedPressureChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.pumpingTrackerPressureFixedAngleChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.pumpingTrackerAngleFixedPressureChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.pumpingTrackerOMVT3FixedAngleChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.cltTrackerTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cltTrackerShowRealCheckBox = new System.Windows.Forms.CheckBox();
            this.cltTrackerShowExpectedCheckBox = new System.Windows.Forms.CheckBox();
            this.cltP1TrackerChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.cltP4TrackerChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.cltP5TrackerChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.cltP3TrackerChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.cltP2TrackerChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.failureAnalysisTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.overallPassRateGridView = new System.Windows.Forms.DataGridView();
            this.pumpPasses = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pumpFails = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pumpIncompleteTests = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pumpTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pumpPassRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.failureFullDataGridView = new System.Windows.Forms.DataGridView();
            this.analyseFailedInjectorsButton = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.passRateGridView = new System.Windows.Forms.DataGridView();
            this.calDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calPasses = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calFails = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calIncompleteTests = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calPassRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.failureParetoChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label50 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.SplitResultsByRig = new System.Windows.Forms.CheckBox();
            this.label59 = new System.Windows.Forms.Label();
            this.SplitResultsByShift = new System.Windows.Forms.RadioButton();
            this.SplitResults10Till10 = new System.Windows.Forms.RadioButton();
            this.SplitResults6Till6 = new System.Windows.Forms.RadioButton();
            this.SplitResultsDaily = new System.Windows.Forms.RadioButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.IgnoreIncompleteResults = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.graphFailureModesRadioButton = new System.Windows.Forms.RadioButton();
            this.graphParetoRadioButton = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.filterAllRadioButton = new System.Windows.Forms.RadioButton();
            this.filterTestFailsRadioButton = new System.Windows.Forms.RadioButton();
            this.filterOEEFailsRadioButton = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel34 = new System.Windows.Forms.TableLayoutPanel();
            this.label17 = new System.Windows.Forms.Label();
            this.tableLayoutPanel35 = new System.Windows.Forms.TableLayoutPanel();
            this.rigAlignmentTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rigAlignmentChronologicalRadioButton = new System.Windows.Forms.RadioButton();
            this.rigAlignmentDateTimeRadioButton = new System.Windows.Forms.RadioButton();
            this.rigAlignmentTestTypeGroupBox = new System.Windows.Forms.GroupBox();
            this.rigAlignmentPumpingRadioButton = new System.Windows.Forms.RadioButton();
            this.rigAlignmentCalibrationRadioButton = new System.Windows.Forms.RadioButton();
            this.rigAlignmentParameterTreeView = new System.Windows.Forms.TreeView();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.rigAlignmentLineSelectionListView = new System.Windows.Forms.ListView();
            this.rigAlignmentPanel = new System.Windows.Forms.Panel();
            this.RigAlignmentShowLegend = new System.Windows.Forms.CheckBox();
            this.chartTracesTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.tracePumpingutidsListView = new System.Windows.Forms.ListView();
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.getTraceDataButton = new System.Windows.Forms.Button();
            this.traceGroupDataByGroupBox = new System.Windows.Forms.GroupBox();
            this.traceLineNumberRadioButton = new System.Windows.Forms.RadioButton();
            this.traceISNRadioButton = new System.Windows.Forms.RadioButton();
            this.traceTestplanRadioButton = new System.Windows.Forms.RadioButton();
            this.tracePassFailRadioButton = new System.Windows.Forms.RadioButton();
            this.traceIDENTRadioButton = new System.Windows.Forms.RadioButton();
            this.traceLOPRadioButton = new System.Windows.Forms.RadioButton();
            this.traceTestPointRadioButton = new System.Windows.Forms.RadioButton();
            this.traceUTIDRadioButton = new System.Windows.Forms.RadioButton();
            this.chartTraceChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.updateChartTraceChartButton = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.traceOMVTestPointListView = new System.Windows.Forms.ListView();
            this.label23 = new System.Windows.Forms.Label();
            this.traceFODTestPointListView = new System.Windows.Forms.ListView();
            this.label21 = new System.Windows.Forms.Label();
            this.traceCLTTestPointListView = new System.Windows.Forms.ListView();
            this.label22 = new System.Windows.Forms.Label();
            this.traceCalibrationutidsListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.traceNCVTestPointListView = new System.Windows.Forms.ListView();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.tableLayoutPanel33 = new System.Windows.Forms.TableLayoutPanel();
            this.AutoUpdateTraceChartsCheckBox = new System.Windows.Forms.CheckBox();
            this.label63 = new System.Windows.Forms.Label();
            this.traceCLSTestPointListView = new System.Windows.Forms.ListView();
            this.detailedDataTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.returnNominalDataCheckBox = new System.Windows.Forms.CheckBox();
            this.detailedDataTabControl = new System.Windows.Forms.TabControl();
            this.olTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.label26 = new System.Windows.Forms.Label();
            this.olNominalChartedDataGridView = new System.Windows.Forms.DataGridView();
            this.olPressureLevelsListView = new System.Windows.Forms.ListView();
            this.showOLNominalCheckBox = new System.Windows.Forms.CheckBox();
            this.olUpdateChartButton = new System.Windows.Forms.Button();
            this.label34 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.olParametersListView = new System.Windows.Forms.ListView();
            this.olStatisticsListView = new System.Windows.Forms.ListView();
            this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
            this.olChartLegendCheckBox = new System.Windows.Forms.CheckBox();
            this.olSplitContainer = new System.Windows.Forms.SplitContainer();
            this.olChartedDataGridView = new System.Windows.Forms.DataGridView();
            this.clTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel26 = new System.Windows.Forms.TableLayoutPanel();
            this.clChartLegendCheckBox = new System.Windows.Forms.CheckBox();
            this.label36 = new System.Windows.Forms.Label();
            this.clStatisticsListView = new System.Windows.Forms.ListView();
            this.clUpdateChartButton = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.clPressureLevelsListView = new System.Windows.Forms.ListView();
            this.label35 = new System.Windows.Forms.Label();
            this.clParametersListView = new System.Windows.Forms.ListView();
            this.clSplitContainer = new System.Windows.Forms.SplitContainer();
            this.clChartedDataGridView = new System.Windows.Forms.DataGridView();
            this.clLogPlotTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel27 = new System.Windows.Forms.TableLayoutPanel();
            this.clLogPlotChartLegendCheckBox = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.showFuelAsErrorCheckBox = new System.Windows.Forms.CheckBox();
            this.clLogPlotPressureLevelsListView = new System.Windows.Forms.ListView();
            this.clLogPlotStatisticsListView = new System.Windows.Forms.ListView();
            this.label11 = new System.Windows.Forms.Label();
            this.clLogPlotUpdateChartButton = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.clLogPlotParametersListView = new System.Windows.Forms.ListView();
            this.clLogPlotSsplitContainer = new System.Windows.Forms.SplitContainer();
            this.clLogPlotChartedDataGridView = new System.Windows.Forms.DataGridView();
            this.olStabiliseTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel28 = new System.Windows.Forms.TableLayoutPanel();
            this.olStabiliseChartLegendCheckBox = new System.Windows.Forms.CheckBox();
            this.label42 = new System.Windows.Forms.Label();
            this.olStabiliseNominalChartedDataGridView = new System.Windows.Forms.DataGridView();
            this.olStabilisePressureLevelsListView = new System.Windows.Forms.ListView();
            this.olStabiliseShowNominalCheckBox = new System.Windows.Forms.CheckBox();
            this.olStabiliseUpdateChartButton = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.olStabiliseParametersListView = new System.Windows.Forms.ListView();
            this.olStabiliseStatisticsListView = new System.Windows.Forms.ListView();
            this.olStabiliseSplitContainer = new System.Windows.Forms.SplitContainer();
            this.olStabiliseChartedDataGridView = new System.Windows.Forms.DataGridView();
            this.clStabiliseTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel29 = new System.Windows.Forms.TableLayoutPanel();
            this.clStabiliseChartLegendCheckBox = new System.Windows.Forms.CheckBox();
            this.label45 = new System.Windows.Forms.Label();
            this.clStabiliseNominalChartedDataGridView = new System.Windows.Forms.DataGridView();
            this.clStabilisePressureLevelsListView = new System.Windows.Forms.ListView();
            this.clStabiliseShowNominalCheckBox = new System.Windows.Forms.CheckBox();
            this.clStabiliseUpdateChartButton = new System.Windows.Forms.Button();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.clStabiliseParametersListView = new System.Windows.Forms.ListView();
            this.clStabiliseStatisticsListView = new System.Windows.Forms.ListView();
            this.clStabiliseSplitContainer = new System.Windows.Forms.SplitContainer();
            this.clStabiliseChartedDataGridView = new System.Windows.Forms.DataGridView();
            this.fodTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel30 = new System.Windows.Forms.TableLayoutPanel();
            this.FODChartLegendCheckBox = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.fodStatisticsListView = new System.Windows.Forms.ListView();
            this.label8 = new System.Windows.Forms.Label();
            this.fodParametersListView = new System.Windows.Forms.ListView();
            this.fodUpdateChartButton = new System.Windows.Forms.Button();
            this.fodSplitContainer = new System.Windows.Forms.SplitContainer();
            this.fodChartedDataGridView = new System.Windows.Forms.DataGridView();
            this.omvTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel31 = new System.Windows.Forms.TableLayoutPanel();
            this.OMVChartLegendCheckBox = new System.Windows.Forms.CheckBox();
            this.omvStatisticsListView = new System.Windows.Forms.ListView();
            this.omvUpdateChartButton = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.omvParametersListView = new System.Windows.Forms.ListView();
            this.omvSplitContainer = new System.Windows.Forms.SplitContainer();
            this.omvChartedDataGridView = new System.Windows.Forms.DataGridView();
            this.leakdownTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.label38 = new System.Windows.Forms.Label();
            this.leakdownUpdateChartButton = new System.Windows.Forms.Button();
            this.leakdownParametersListView = new System.Windows.Forms.ListView();
            this.leakdownSplitContainer = new System.Windows.Forms.SplitContainer();
            this.leakdownChartedDataGridView = new System.Windows.Forms.DataGridView();
            this.trimmedTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.label53 = new System.Windows.Forms.Label();
            this.trimmedStatisticsListView = new System.Windows.Forms.ListView();
            this.trimmedUpdateChartButton = new System.Windows.Forms.Button();
            this.label54 = new System.Windows.Forms.Label();
            this.trimmedPressureLevelsListView = new System.Windows.Forms.ListView();
            this.label55 = new System.Windows.Forms.Label();
            this.trimmedParametersListView = new System.Windows.Forms.ListView();
            this.tableLayoutPanel25 = new System.Windows.Forms.TableLayoutPanel();
            this.trimmedChartLegendCheckBox = new System.Windows.Forms.CheckBox();
            this.trimmedChartIndividualCharts = new System.Windows.Forms.CheckBox();
            this.trimmedChart_Limits = new System.Windows.Forms.ComboBox();
            this.trimmedChart_LimitsLabel = new System.Windows.Forms.Label();
            this.trimmedSplitContainer = new System.Windows.Forms.SplitContainer();
            this.trimmedChartPanel = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.trimmedChartedDataGridView = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel40 = new System.Windows.Forms.TableLayoutPanel();
            this.trimmedMeanToggle = new System.Windows.Forms.CheckBox();
            this.trimmedMedianToggle = new System.Windows.Forms.CheckBox();
            this.trimmedLogTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel32 = new System.Windows.Forms.TableLayoutPanel();
            this.trimmedLogPlotChartLegendCheckBox = new System.Windows.Forms.CheckBox();
            this.label56 = new System.Windows.Forms.Label();
            this.trimmedShowFuelAsErrorCheckBox = new System.Windows.Forms.CheckBox();
            this.trimmedLogPlotPressureLevelsListView = new System.Windows.Forms.ListView();
            this.trimmedLogPlotStatisticsListView = new System.Windows.Forms.ListView();
            this.label57 = new System.Windows.Forms.Label();
            this.trimmedLogPlotUpdateChartButton = new System.Windows.Forms.Button();
            this.label58 = new System.Windows.Forms.Label();
            this.trimmedLogPlotParametersListView = new System.Windows.Forms.ListView();
            this.trimmedLogPlotSplitContainer = new System.Windows.Forms.SplitContainer();
            this.trimmedLogPlotChartedDataGridView = new System.Windows.Forms.DataGridView();
            this.untrimmedTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel37 = new System.Windows.Forms.TableLayoutPanel();
            this.label61 = new System.Windows.Forms.Label();
            this.utPressureLevelsListView = new System.Windows.Forms.ListView();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.utChartUpdateButton = new System.Windows.Forms.Button();
            this.utChartedDataGridView = new System.Windows.Forms.DataGridView();
            this.groupDataByGroupBox = new System.Windows.Forms.GroupBox();
            this.rigGroupRadioButton = new System.Windows.Forms.RadioButton();
            this.lineGroupRadioButton = new System.Windows.Forms.RadioButton();
            this.identGroupRadioButton = new System.Windows.Forms.RadioButton();
            this.utidGroupRadioButton = new System.Windows.Forms.RadioButton();
            this.testplanGroupRadioButton = new System.Windows.Forms.RadioButton();
            this.isnGroupRadioButton = new System.Windows.Forms.RadioButton();
            this.lopGroupRadioButton = new System.Windows.Forms.RadioButton();
            this.passFailGroupRadioButton = new System.Windows.Forms.RadioButton();
            this.detailedDataUpdateButton = new System.Windows.Forms.Button();
            this.DetailedUTIDsListView = new System.Windows.Forms.ListView();
            this.colUTID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colPN = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colISN = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colPDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AutoUpdateDetailedChartsCheckBox = new System.Windows.Forms.CheckBox();
            this.genericTrendChartTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.genericTrendPumpingRadioButton = new System.Windows.Forms.RadioButton();
            this.genericTrendCalibrationRadioButton = new System.Windows.Forms.RadioButton();
            this.genericTrendChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.genericTrendChronologicalRadioButton = new System.Windows.Forms.RadioButton();
            this.genericTrendDateTimeRadioButton = new System.Windows.Forms.RadioButton();
            this.genericTrendChartTreeView = new System.Windows.Forms.TreeView();
            this.label29 = new System.Windows.Forms.Label();
            this.xyChartTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.xyPumpingRadioButton = new System.Windows.Forms.RadioButton();
            this.xyCalibrationRadioButton = new System.Windows.Forms.RadioButton();
            this.xyChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.yAxisXYChartTreeView = new System.Windows.Forms.TreeView();
            this.xAxisXYChartTreeView = new System.Windows.Forms.TreeView();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.trendChartTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.trendutidsListView = new System.Windows.Forms.ListView();
            this.utidColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pnColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.isnColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pdateColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rigtypeColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.reasonColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.trendChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.trendChartDataGridView = new System.Windows.Forms.DataGridView();
            this.InjectorComponentsTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel36 = new System.Windows.Forms.TableLayoutPanel();
            this.UpdateInjectorComponentsButton = new System.Windows.Forms.Button();
            this.label62 = new System.Windows.Forms.Label();
            this.tableLayoutPanel39 = new System.Windows.Forms.TableLayoutPanel();
            this.selectedComponentsLabel = new System.Windows.Forms.Label();
            this.missingComponentsLabel = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lbProcesDataExcel = new System.Windows.Forms.PictureBox();
            this.dgvProcessData = new System.Windows.Forms.DataGridView();
            this.tbProcessDataSerial = new System.Windows.Forms.TextBox();
            this.btnProcessData = new System.Windows.Forms.Button();
            this.SerialLabel = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lbPackingDataExcel = new System.Windows.Forms.PictureBox();
            this.lPackingDataOutput = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.tbPackingData = new System.Windows.Forms.TextBox();
            this.btnPackingData = new System.Windows.Forms.Button();
            this.dgvProcessDataPack = new System.Windows.Forms.DataGridView();
            this.euro6DataSet = new SudburyDataAnalyser.Euro6DataSet();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.queryUpdateTimer = new System.Windows.Forms.Timer(this.components);
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tbDaimlerISN = new System.Windows.Forms.TextBox();
            this.btnDaimlerISNExcel = new System.Windows.Forms.PictureBox();
            this.lDaimlerISN = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.btnDaimlerISN = new System.Windows.Forms.Button();
            this.dgvDaimlerISN = new System.Windows.Forms.DataGridView();
            this.dateToExtDateTimePicker = new SudburyDataAnalyser.ExtendedDateTimePicker();
            this.dateFromExtDateTimePicker = new SudburyDataAnalyser.ExtendedDateTimePicker();
            this.incompletePumpingDataGridView = new SudburyDataAnalyser.VirtualDataGridView.IncompleteVirtualDataGridView();
            this.completedPumpingDataGridView = new SudburyDataAnalyser.VirtualDataGridView.CompletedVirtualDataGridView();
            this.completedCalibrationDataGridView = new SudburyDataAnalyser.VirtualDataGridView.CompletedVirtualDataGridView();
            this.incompleteCalibrationDataGridView = new SudburyDataAnalyser.VirtualDataGridView.IncompleteVirtualDataGridView();
            this.CompleteCalibrationExcelButton = new SudburyDataAnalyser.ExcelButton();
            this.CompletePumpingExcelButton = new SudburyDataAnalyser.ExcelButton();
            this.IncompleteCalibrationExcelButton = new SudburyDataAnalyser.ExcelButton();
            this.IncompletePumpingExcelButton = new SudburyDataAnalyser.ExcelButton();
            this.CompletedCalibrationCount0 = new SudburyDataAnalyser.LabelButton();
            this.CompletedCalibrationCount5 = new SudburyDataAnalyser.LabelButton();
            this.CompletedCalibrationCount4 = new SudburyDataAnalyser.LabelButton();
            this.CompletedCalibrationCount1 = new SudburyDataAnalyser.LabelButton();
            this.CompletedCalibrationCount2 = new SudburyDataAnalyser.LabelButton();
            this.CompletedCalibrationCount3 = new SudburyDataAnalyser.LabelButton();
            this.CompletePumpingCount0 = new SudburyDataAnalyser.LabelButton();
            this.CompletePumpingCount1 = new SudburyDataAnalyser.LabelButton();
            this.CompletePumpingCount2 = new SudburyDataAnalyser.LabelButton();
            this.CompletePumpingCount3 = new SudburyDataAnalyser.LabelButton();
            this.CompletePumpingCount4 = new SudburyDataAnalyser.LabelButton();
            this.excelButton11 = new SudburyDataAnalyser.ExcelButton();
            this.powerPointButton11 = new SudburyDataAnalyser.PowerPointButton();
            this.copyButton9 = new SudburyDataAnalyser.CopyButton();
            this.excelButton12 = new SudburyDataAnalyser.ExcelButton();
            this.copyButton1 = new SudburyDataAnalyser.CopyButton();
            this.excelButton1 = new SudburyDataAnalyser.ExcelButton();
            this.powerPointButton1 = new SudburyDataAnalyser.PowerPointButton();
            this.copyButton11 = new SudburyDataAnalyser.CopyButton();
            this.excelButton2 = new SudburyDataAnalyser.ExcelButton();
            this.powerPointButton2 = new SudburyDataAnalyser.PowerPointButton();
            this.excelButton15 = new SudburyDataAnalyser.ExcelButton();
            this.olChart = new SudburyDataAnalyser.ChartSplitLegend();
            this.copyButton3 = new SudburyDataAnalyser.CopyButton();
            this.excelButton3 = new SudburyDataAnalyser.ExcelButton();
            this.powerPointButton3 = new SudburyDataAnalyser.PowerPointButton();
            this.clChart = new SudburyDataAnalyser.ChartSplitLegend();
            this.copyButton4 = new SudburyDataAnalyser.CopyButton();
            this.powerPointButton4 = new SudburyDataAnalyser.PowerPointButton();
            this.excelButton4 = new SudburyDataAnalyser.ExcelButton();
            this.clLogPlotChart = new SudburyDataAnalyser.ChartSplitLegend();
            this.copyButton5 = new SudburyDataAnalyser.CopyButton();
            this.powerPointButton5 = new SudburyDataAnalyser.PowerPointButton();
            this.excelButton5 = new SudburyDataAnalyser.ExcelButton();
            this.olStabiliseChart = new SudburyDataAnalyser.ChartSplitLegend();
            this.copyButton6 = new SudburyDataAnalyser.CopyButton();
            this.powerPointButton6 = new SudburyDataAnalyser.PowerPointButton();
            this.excelButton6 = new SudburyDataAnalyser.ExcelButton();
            this.clStabiliseChart = new SudburyDataAnalyser.ChartSplitLegend();
            this.copyButton7 = new SudburyDataAnalyser.CopyButton();
            this.powerPointButton7 = new SudburyDataAnalyser.PowerPointButton();
            this.excelButton7 = new SudburyDataAnalyser.ExcelButton();
            this.fodChart = new SudburyDataAnalyser.ChartSplitLegend();
            this.powerPointButton8 = new SudburyDataAnalyser.PowerPointButton();
            this.excelButton8 = new SudburyDataAnalyser.ExcelButton();
            this.copyButton8 = new SudburyDataAnalyser.CopyButton();
            this.omvChart = new SudburyDataAnalyser.ChartSplitLegend();
            this.leakdownChart = new SudburyDataAnalyser.ChartSplitLegend();
            this.powerPointButton9 = new SudburyDataAnalyser.PowerPointButton();
            this.trimmedChart_CopyToClipboard = new SudburyDataAnalyser.CopyButton();
            this.excelButton9 = new SudburyDataAnalyser.ExcelButton();
            this.viewStatsWindow = new SudburyDataAnalyser.CopyButton();
            this.trimmedCharts = new SudburyDataAnalyser.TableOfCharts();
            this.trimmedChart = new SudburyDataAnalyser.ChartSplitLegend();
            this.trimmedChartIndividualChartsLegend = new SudburyDataAnalyser.ChartLegend();
            this.copyButton10 = new SudburyDataAnalyser.CopyButton();
            this.excelButton10 = new SudburyDataAnalyser.ExcelButton();
            this.powerPointButton10 = new SudburyDataAnalyser.PowerPointButton();
            this.trimmedLogPlotChart = new SudburyDataAnalyser.ChartSplitLegend();
            this.utChart = new SudburyDataAnalyser.ChartSplitLegend();
            this.InjectorComponentsGridView = new SudburyDataAnalyser.VirtualDataGridView.InjectorComponentDataGridView();
            this.excelButton13 = new SudburyDataAnalyser.ExcelButton();
            this.mainStatusStrip.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.mainTabControl.SuspendLayout();
            this.queryTabPage.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.databaseGroupBox.SuspendLayout();
            this.tableLayoutPanel38.SuspendLayout();
            this.passesFailsGroupBox.SuspendLayout();
            this.testsForEachISNGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lopListDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testTypeListDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testplanListDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lineNumberListDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantNumberListDataGridView)).BeginInit();
            this.datasetTabPage.SuspendLayout();
            this.DatasetTableLayoutPanel.SuspendLayout();
            this.trimTrackerChartsTabPage.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trimTrackerLeakdownChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimTrackerCDChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimTrackerP3Chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimTrackerMDPChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimTrackerP1Chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimTrackerODChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimTrackerP4Chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimTrackerP5Chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimTrackerP2Chart)).BeginInit();
            this.panel2.SuspendLayout();
            this.pumpingTrackerChartsTabPage.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pumpingTrackerOMVT3FixedPressureChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pumpingTrackerPressureFixedAngleChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pumpingTrackerAngleFixedPressureChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pumpingTrackerOMVT3FixedAngleChart)).BeginInit();
            this.cltTrackerTabPage.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cltP1TrackerChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cltP4TrackerChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cltP5TrackerChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cltP3TrackerChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cltP2TrackerChart)).BeginInit();
            this.failureAnalysisTabPage.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.overallPassRateGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.failureFullDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.passRateGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.failureParetoChart)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tableLayoutPanel34.SuspendLayout();
            this.tableLayoutPanel35.SuspendLayout();
            this.rigAlignmentTabPage.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.rigAlignmentTestTypeGroupBox.SuspendLayout();
            this.rigAlignmentPanel.SuspendLayout();
            this.chartTracesTabPage.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.traceGroupDataByGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartTraceChart)).BeginInit();
            this.tableLayoutPanel33.SuspendLayout();
            this.detailedDataTabPage.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.detailedDataTabControl.SuspendLayout();
            this.olTabPage.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olNominalChartedDataGridView)).BeginInit();
            this.tableLayoutPanel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olSplitContainer)).BeginInit();
            this.olSplitContainer.Panel1.SuspendLayout();
            this.olSplitContainer.Panel2.SuspendLayout();
            this.olSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olChartedDataGridView)).BeginInit();
            this.clTabPage.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clSplitContainer)).BeginInit();
            this.clSplitContainer.Panel1.SuspendLayout();
            this.clSplitContainer.Panel2.SuspendLayout();
            this.clSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clChartedDataGridView)).BeginInit();
            this.clLogPlotTabPage.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clLogPlotSsplitContainer)).BeginInit();
            this.clLogPlotSsplitContainer.Panel1.SuspendLayout();
            this.clLogPlotSsplitContainer.Panel2.SuspendLayout();
            this.clLogPlotSsplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clLogPlotChartedDataGridView)).BeginInit();
            this.olStabiliseTabPage.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.tableLayoutPanel28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olStabiliseNominalChartedDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.olStabiliseSplitContainer)).BeginInit();
            this.olStabiliseSplitContainer.Panel1.SuspendLayout();
            this.olStabiliseSplitContainer.Panel2.SuspendLayout();
            this.olStabiliseSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olStabiliseChartedDataGridView)).BeginInit();
            this.clStabiliseTabPage.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.tableLayoutPanel29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clStabiliseNominalChartedDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clStabiliseSplitContainer)).BeginInit();
            this.clStabiliseSplitContainer.Panel1.SuspendLayout();
            this.clStabiliseSplitContainer.Panel2.SuspendLayout();
            this.clStabiliseSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clStabiliseChartedDataGridView)).BeginInit();
            this.fodTabPage.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.tableLayoutPanel30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fodSplitContainer)).BeginInit();
            this.fodSplitContainer.Panel1.SuspendLayout();
            this.fodSplitContainer.Panel2.SuspendLayout();
            this.fodSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fodChartedDataGridView)).BeginInit();
            this.omvTabPage.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.omvSplitContainer)).BeginInit();
            this.omvSplitContainer.Panel1.SuspendLayout();
            this.omvSplitContainer.Panel2.SuspendLayout();
            this.omvSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.omvChartedDataGridView)).BeginInit();
            this.leakdownTabPage.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leakdownSplitContainer)).BeginInit();
            this.leakdownSplitContainer.Panel1.SuspendLayout();
            this.leakdownSplitContainer.Panel2.SuspendLayout();
            this.leakdownSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leakdownChartedDataGridView)).BeginInit();
            this.trimmedTabPage.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            this.tableLayoutPanel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trimmedSplitContainer)).BeginInit();
            this.trimmedSplitContainer.Panel1.SuspendLayout();
            this.trimmedSplitContainer.Panel2.SuspendLayout();
            this.trimmedSplitContainer.SuspendLayout();
            this.trimmedChartPanel.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trimmedChartedDataGridView)).BeginInit();
            this.tableLayoutPanel40.SuspendLayout();
            this.trimmedLogTabPage.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            this.tableLayoutPanel32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trimmedLogPlotSplitContainer)).BeginInit();
            this.trimmedLogPlotSplitContainer.Panel1.SuspendLayout();
            this.trimmedLogPlotSplitContainer.Panel2.SuspendLayout();
            this.trimmedLogPlotSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trimmedLogPlotChartedDataGridView)).BeginInit();
            this.untrimmedTabPage.SuspendLayout();
            this.tableLayoutPanel37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.utChartedDataGridView)).BeginInit();
            this.groupDataByGroupBox.SuspendLayout();
            this.genericTrendChartTabPage.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.genericTrendChart)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.xyChartTabPage.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xyChart)).BeginInit();
            this.trendChartTabPage.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trendChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendChartDataGridView)).BeginInit();
            this.InjectorComponentsTabPage.SuspendLayout();
            this.tableLayoutPanel36.SuspendLayout();
            this.tableLayoutPanel39.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbProcesDataExcel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcessData)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbPackingDataExcel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcessDataPack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.euro6DataSet)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDaimlerISNExcel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDaimlerISN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.incompletePumpingDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.completedPumpingDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.completedCalibrationDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.incompleteCalibrationDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompleteCalibrationExcelButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompletePumpingExcelButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncompleteCalibrationExcelButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncompletePumpingExcelButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.copyButton9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.copyButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.copyButton11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.olChart)).BeginInit();
            this.olChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copyButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clChart)).BeginInit();
            this.clChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copyButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clLogPlotChart)).BeginInit();
            this.clLogPlotChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copyButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.olStabiliseChart)).BeginInit();
            this.olStabiliseChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copyButton6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clStabiliseChart)).BeginInit();
            this.clStabiliseChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copyButton7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fodChart)).BeginInit();
            this.fodChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.copyButton8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.omvChart)).BeginInit();
            this.omvChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leakdownChart)).BeginInit();
            this.leakdownChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimmedChart_CopyToClipboard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewStatsWindow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimmedChart)).BeginInit();
            this.trimmedChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copyButton10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimmedLogPlotChart)).BeginInit();
            this.trimmedLogPlotChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.utChart)).BeginInit();
            this.utChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InjectorComponentsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton13)).BeginInit();
            this.SuspendLayout();
            // 
            // mainStatusStrip
            // 
            this.mainStatusStrip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainStatusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.mainStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainStatusStripLabel,
            this.toolStripStatusLabel1,
            this.toolStripSplitButton1});
            this.mainStatusStrip.Location = new System.Drawing.Point(0, 633);
            this.mainStatusStrip.Name = "mainStatusStrip";
            this.mainStatusStrip.Size = new System.Drawing.Size(1059, 25);
            this.mainStatusStrip.TabIndex = 1;
            this.mainStatusStrip.Text = "statusStrip1";
            // 
            // mainStatusStripLabel
            // 
            this.mainStatusStripLabel.Name = "mainStatusStripLabel";
            this.mainStatusStripLabel.Size = new System.Drawing.Size(48, 20);
            this.mainStatusStripLabel.Text = "Ready...";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(960, 20);
            this.toolStripStatusLabel1.Spring = true;
            // 
            // toolStripSplitButton1
            // 
            this.toolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSplitButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewChangelogToolStripMenuItem});
            this.toolStripSplitButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton1.Image")));
            this.toolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton1.Name = "toolStripSplitButton1";
            this.toolStripSplitButton1.Size = new System.Drawing.Size(36, 23);
            this.toolStripSplitButton1.Text = "toolStripSplitButton1";
            // 
            // viewChangelogToolStripMenuItem
            // 
            this.viewChangelogToolStripMenuItem.Name = "viewChangelogToolStripMenuItem";
            this.viewChangelogToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.viewChangelogToolStripMenuItem.Text = "View Changelog";
            this.viewChangelogToolStripMenuItem.Click += new System.EventHandler(this.viewChangelogToolStripMenuItem_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.mainTabControl, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.mainStatusStrip, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1059, 658);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // mainTabControl
            // 
            this.mainTabControl.Controls.Add(this.queryTabPage);
            this.mainTabControl.Controls.Add(this.datasetTabPage);
            this.mainTabControl.Controls.Add(this.trimTrackerChartsTabPage);
            this.mainTabControl.Controls.Add(this.pumpingTrackerChartsTabPage);
            this.mainTabControl.Controls.Add(this.cltTrackerTabPage);
            this.mainTabControl.Controls.Add(this.failureAnalysisTabPage);
            this.mainTabControl.Controls.Add(this.rigAlignmentTabPage);
            this.mainTabControl.Controls.Add(this.chartTracesTabPage);
            this.mainTabControl.Controls.Add(this.detailedDataTabPage);
            this.mainTabControl.Controls.Add(this.genericTrendChartTabPage);
            this.mainTabControl.Controls.Add(this.xyChartTabPage);
            this.mainTabControl.Controls.Add(this.trendChartTabPage);
            this.mainTabControl.Controls.Add(this.InjectorComponentsTabPage);
            this.mainTabControl.Controls.Add(this.tabPage1);
            this.mainTabControl.Controls.Add(this.tabPage2);
            this.mainTabControl.Controls.Add(this.tabPage3);
            this.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTabControl.Location = new System.Drawing.Point(3, 3);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedIndex = 0;
            this.mainTabControl.Size = new System.Drawing.Size(1053, 627);
            this.mainTabControl.TabIndex = 0;
            // 
            // queryTabPage
            // 
            this.queryTabPage.Controls.Add(this.tableLayoutPanel1);
            this.queryTabPage.Location = new System.Drawing.Point(4, 22);
            this.queryTabPage.Name = "queryTabPage";
            this.queryTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.queryTabPage.Size = new System.Drawing.Size(1045, 601);
            this.queryTabPage.TabIndex = 0;
            this.queryTabPage.Text = "Query";
            this.queryTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.59665F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.59665F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.59665F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.59665F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.59665F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.5945F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.5945F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.82774F));
            this.tableLayoutPanel1.Controls.Add(this.checkBoxUseSudburyArchive, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.incompletePumpingQueryTextBox, 4, 13);
            this.tableLayoutPanel1.Controls.Add(this.completePumpingQueryTextBox, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.label28, 4, 12);
            this.tableLayoutPanel1.Controls.Add(this.label27, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.injCountLabel, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label25, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.databaseGroupBox, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.passesFailsGroupBox, 7, 7);
            this.tableLayoutPanel1.Controls.Add(this.allowForcedTrimCheckBox, 7, 8);
            this.tableLayoutPanel1.Controls.Add(this.dateToExtDateTimePicker, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.testsForEachISNGroupBox, 7, 6);
            this.tableLayoutPanel1.Controls.Add(this.allowIncompleteTestsCheckBox, 7, 9);
            this.tableLayoutPanel1.Controls.Add(this.dateFromExtDateTimePicker, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lopListDataGridView, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.specificutidsTextBox, 6, 6);
            this.tableLayoutPanel1.Controls.Add(this.label5, 6, 5);
            this.tableLayoutPanel1.Controls.Add(this.specificISNsTextBox, 5, 6);
            this.tableLayoutPanel1.Controls.Add(this.label14, 5, 5);
            this.tableLayoutPanel1.Controls.Add(this.testTypeListDataGridView, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.label16, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.specificutidsCheckBox, 7, 4);
            this.tableLayoutPanel1.Controls.Add(this.label15, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.testplanListDataGridView, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.specificISNsCheckBox, 6, 4);
            this.tableLayoutPanel1.Controls.Add(this.lineNumberListDataGridView, 4, 6);
            this.tableLayoutPanel1.Controls.Add(this.completeCalibrationQueryTextBox, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.plantNumberListDataGridView, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.label7, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.runMainQueryButton, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.label10, 4, 10);
            this.tableLayoutPanel1.Controls.Add(this.incompleteCalibrationQueryTextBox, 4, 11);
            this.tableLayoutPanel1.Controls.Add(this.label6, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.NumericISNCheckBox, 7, 3);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxUseIasiArchive, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnUpdate, 0, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 16;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1039, 595);
            this.tableLayoutPanel1.TabIndex = 44;
            // 
            // checkBoxUseSudburyArchive
            // 
            this.checkBoxUseSudburyArchive.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.checkBoxUseSudburyArchive, 2);
            this.checkBoxUseSudburyArchive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxUseSudburyArchive.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxUseSudburyArchive.Location = new System.Drawing.Point(483, 70);
            this.checkBoxUseSudburyArchive.Name = "checkBoxUseSudburyArchive";
            this.checkBoxUseSudburyArchive.Size = new System.Drawing.Size(234, 27);
            this.checkBoxUseSudburyArchive.TabIndex = 4;
            this.checkBoxUseSudburyArchive.Text = "Use Sudbury Archive";
            this.checkBoxUseSudburyArchive.UseVisualStyleBackColor = true;
            this.checkBoxUseSudburyArchive.CheckedChanged += new System.EventHandler(this.checkBoxCheckSudbury_CheckedChanged);
            // 
            // incompletePumpingQueryTextBox
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.incompletePumpingQueryTextBox, 4);
            this.incompletePumpingQueryTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.incompletePumpingQueryTextBox.Location = new System.Drawing.Point(483, 494);
            this.incompletePumpingQueryTextBox.Multiline = true;
            this.incompletePumpingQueryTextBox.Name = "incompletePumpingQueryTextBox";
            this.incompletePumpingQueryTextBox.ReadOnly = true;
            this.incompletePumpingQueryTextBox.Size = new System.Drawing.Size(553, 40);
            this.incompletePumpingQueryTextBox.TabIndex = 51;
            // 
            // completePumpingQueryTextBox
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.completePumpingQueryTextBox, 4);
            this.completePumpingQueryTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.completePumpingQueryTextBox.Location = new System.Drawing.Point(3, 494);
            this.completePumpingQueryTextBox.Multiline = true;
            this.completePumpingQueryTextBox.Name = "completePumpingQueryTextBox";
            this.completePumpingQueryTextBox.ReadOnly = true;
            this.completePumpingQueryTextBox.Size = new System.Drawing.Size(474, 40);
            this.completePumpingQueryTextBox.TabIndex = 50;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label28, 4);
            this.label28.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label28.Location = new System.Drawing.Point(483, 478);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(553, 13);
            this.label28.TabIndex = 49;
            this.label28.Text = "SQL Query - Incomplete pumping tests";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label27, 4);
            this.label27.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label27.Location = new System.Drawing.Point(3, 478);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(474, 13);
            this.label27.TabIndex = 48;
            this.label27.Text = "SQL Query - Complete pumping tests";
            // 
            // injCountLabel
            // 
            this.injCountLabel.AutoSize = true;
            this.injCountLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.injCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.injCountLabel.Location = new System.Drawing.Point(243, 25);
            this.injCountLabel.Name = "injCountLabel";
            this.tableLayoutPanel1.SetRowSpan(this.injCountLabel, 3);
            this.injCountLabel.Size = new System.Drawing.Size(114, 75);
            this.injCountLabel.TabIndex = 47;
            this.injCountLabel.Text = "0";
            this.injCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Dock = System.Windows.Forms.DockStyle.Right;
            this.label25.Location = new System.Drawing.Point(123, 0);
            this.label25.Name = "label25";
            this.tableLayoutPanel1.SetRowSpan(this.label25, 5);
            this.label25.Size = new System.Drawing.Size(114, 125);
            this.label25.TabIndex = 46;
            this.label25.Text = "No. of injectors current selection will return data for:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Location = new System.Drawing.Point(3, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "From";
            // 
            // databaseGroupBox
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.databaseGroupBox, 5);
            this.databaseGroupBox.Controls.Add(this.tableLayoutPanel38);
            this.databaseGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.databaseGroupBox.Location = new System.Drawing.Point(363, 3);
            this.databaseGroupBox.Name = "databaseGroupBox";
            this.tableLayoutPanel1.SetRowSpan(this.databaseGroupBox, 3);
            this.databaseGroupBox.Size = new System.Drawing.Size(673, 61);
            this.databaseGroupBox.TabIndex = 38;
            this.databaseGroupBox.TabStop = false;
            this.databaseGroupBox.Text = "Database";
            // 
            // tableLayoutPanel38
            // 
            this.tableLayoutPanel38.ColumnCount = 3;
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel38.Controls.Add(this.vsql08NewDBRadioButton, 0, 1);
            this.tableLayoutPanel38.Controls.Add(this.vsql08OldDBRadioButton, 0, 1);
            this.tableLayoutPanel38.Controls.Add(this.dafDB01RadioButton, 0, 0);
            this.tableLayoutPanel38.Controls.Add(this.mdegDB01RadioButton, 1, 0);
            this.tableLayoutPanel38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel38.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel38.Name = "tableLayoutPanel38";
            this.tableLayoutPanel38.RowCount = 2;
            this.tableLayoutPanel38.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel38.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel38.Size = new System.Drawing.Size(667, 42);
            this.tableLayoutPanel38.TabIndex = 14;
            // 
            // vsql08NewDBRadioButton
            // 
            this.vsql08NewDBRadioButton.AutoSize = true;
            this.vsql08NewDBRadioButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vsql08NewDBRadioButton.Location = new System.Drawing.Point(3, 24);
            this.vsql08NewDBRadioButton.MinimumSize = new System.Drawing.Size(50, 0);
            this.vsql08NewDBRadioButton.Name = "vsql08NewDBRadioButton";
            this.vsql08NewDBRadioButton.Size = new System.Drawing.Size(216, 15);
            this.vsql08NewDBRadioButton.TabIndex = 10;
            this.vsql08NewDBRadioButton.Text = "VSQL08 - New";
            this.toolTip.SetToolTip(this.vsql08NewDBRadioButton, "VSQL08 - New");
            this.vsql08NewDBRadioButton.UseVisualStyleBackColor = true;
            this.vsql08NewDBRadioButton.Visible = false;
            this.vsql08NewDBRadioButton.CheckedChanged += new System.EventHandler(this.database_CheckedChanged);
            // 
            // vsql08OldDBRadioButton
            // 
            this.vsql08OldDBRadioButton.AutoSize = true;
            this.vsql08OldDBRadioButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vsql08OldDBRadioButton.Location = new System.Drawing.Point(225, 24);
            this.vsql08OldDBRadioButton.MinimumSize = new System.Drawing.Size(50, 0);
            this.vsql08OldDBRadioButton.Name = "vsql08OldDBRadioButton";
            this.vsql08OldDBRadioButton.Size = new System.Drawing.Size(216, 15);
            this.vsql08OldDBRadioButton.TabIndex = 9;
            this.vsql08OldDBRadioButton.Text = "VSQL08 - Old";
            this.toolTip.SetToolTip(this.vsql08OldDBRadioButton, "VSQL08 - Old");
            this.vsql08OldDBRadioButton.UseVisualStyleBackColor = true;
            this.vsql08OldDBRadioButton.Visible = false;
            this.vsql08OldDBRadioButton.CheckedChanged += new System.EventHandler(this.database_CheckedChanged);
            // 
            // dafDB01RadioButton
            // 
            this.dafDB01RadioButton.AutoSize = true;
            this.dafDB01RadioButton.Checked = true;
            this.dafDB01RadioButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dafDB01RadioButton.Location = new System.Drawing.Point(3, 3);
            this.dafDB01RadioButton.Name = "dafDB01RadioButton";
            this.dafDB01RadioButton.Size = new System.Drawing.Size(216, 15);
            this.dafDB01RadioButton.TabIndex = 8;
            this.dafDB01RadioButton.TabStop = true;
            this.dafDB01RadioButton.Text = "DAF DB01";
            this.toolTip.SetToolTip(this.dafDB01RadioButton, "DAF DB01");
            this.dafDB01RadioButton.UseVisualStyleBackColor = true;
            this.dafDB01RadioButton.CheckedChanged += new System.EventHandler(this.database_CheckedChanged);
            // 
            // mdegDB01RadioButton
            // 
            this.mdegDB01RadioButton.AutoSize = true;
            this.mdegDB01RadioButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mdegDB01RadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mdegDB01RadioButton.Location = new System.Drawing.Point(225, 3);
            this.mdegDB01RadioButton.Name = "mdegDB01RadioButton";
            this.mdegDB01RadioButton.Size = new System.Drawing.Size(216, 15);
            this.mdegDB01RadioButton.TabIndex = 6;
            this.mdegDB01RadioButton.Text = "MDEG DB01";
            this.toolTip.SetToolTip(this.mdegDB01RadioButton, "MDEG DB01");
            this.mdegDB01RadioButton.UseVisualStyleBackColor = true;
            this.mdegDB01RadioButton.CheckedChanged += new System.EventHandler(this.database_CheckedChanged);
            // 
            // passesFailsGroupBox
            // 
            this.passesFailsGroupBox.Controls.Add(this.passesAndFailsRadioButton);
            this.passesFailsGroupBox.Controls.Add(this.failsOnlyRadioButton);
            this.passesFailsGroupBox.Controls.Add(this.passesOnlyRadioButton);
            this.passesFailsGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.passesFailsGroupBox.Location = new System.Drawing.Point(843, 248);
            this.passesFailsGroupBox.Name = "passesFailsGroupBox";
            this.passesFailsGroupBox.Size = new System.Drawing.Size(193, 94);
            this.passesFailsGroupBox.TabIndex = 39;
            this.passesFailsGroupBox.TabStop = false;
            this.passesFailsGroupBox.Text = "Passes / Fails";
            // 
            // passesAndFailsRadioButton
            // 
            this.passesAndFailsRadioButton.AutoSize = true;
            this.passesAndFailsRadioButton.Checked = true;
            this.passesAndFailsRadioButton.Location = new System.Drawing.Point(6, 19);
            this.passesAndFailsRadioButton.Name = "passesAndFailsRadioButton";
            this.passesAndFailsRadioButton.Size = new System.Drawing.Size(104, 17);
            this.passesAndFailsRadioButton.TabIndex = 2;
            this.passesAndFailsRadioButton.TabStop = true;
            this.passesAndFailsRadioButton.Text = "Passes and Fails";
            this.passesAndFailsRadioButton.UseVisualStyleBackColor = true;
            this.passesAndFailsRadioButton.CheckedChanged += new System.EventHandler(this.passesAndFailsRadioButtons_CheckedChanged);
            // 
            // failsOnlyRadioButton
            // 
            this.failsOnlyRadioButton.AutoSize = true;
            this.failsOnlyRadioButton.Location = new System.Drawing.Point(6, 65);
            this.failsOnlyRadioButton.Name = "failsOnlyRadioButton";
            this.failsOnlyRadioButton.Size = new System.Drawing.Size(68, 17);
            this.failsOnlyRadioButton.TabIndex = 1;
            this.failsOnlyRadioButton.Text = "Fails only";
            this.failsOnlyRadioButton.UseVisualStyleBackColor = true;
            this.failsOnlyRadioButton.CheckedChanged += new System.EventHandler(this.passesAndFailsRadioButtons_CheckedChanged);
            // 
            // passesOnlyRadioButton
            // 
            this.passesOnlyRadioButton.AutoSize = true;
            this.passesOnlyRadioButton.Location = new System.Drawing.Point(6, 42);
            this.passesOnlyRadioButton.Name = "passesOnlyRadioButton";
            this.passesOnlyRadioButton.Size = new System.Drawing.Size(81, 17);
            this.passesOnlyRadioButton.TabIndex = 0;
            this.passesOnlyRadioButton.Text = "Passes only";
            this.passesOnlyRadioButton.UseVisualStyleBackColor = true;
            this.passesOnlyRadioButton.CheckedChanged += new System.EventHandler(this.passesAndFailsRadioButtons_CheckedChanged);
            // 
            // allowForcedTrimCheckBox
            // 
            this.allowForcedTrimCheckBox.AutoSize = true;
            this.allowForcedTrimCheckBox.Checked = true;
            this.allowForcedTrimCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.allowForcedTrimCheckBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.allowForcedTrimCheckBox.Location = new System.Drawing.Point(843, 350);
            this.allowForcedTrimCheckBox.Name = "allowForcedTrimCheckBox";
            this.allowForcedTrimCheckBox.Size = new System.Drawing.Size(193, 17);
            this.allowForcedTrimCheckBox.TabIndex = 41;
            this.allowForcedTrimCheckBox.Text = "Allow forced trim tests";
            this.allowForcedTrimCheckBox.UseVisualStyleBackColor = true;
            this.allowForcedTrimCheckBox.CheckedChanged += new System.EventHandler(this.allowForcedTrimCheckBox_CheckedChanged);
            // 
            // testsForEachISNGroupBox
            // 
            this.testsForEachISNGroupBox.Controls.Add(this.allTestsRadioButton);
            this.testsForEachISNGroupBox.Controls.Add(this.latestTestsRadioButton);
            this.testsForEachISNGroupBox.Controls.Add(this.firstTimeTestsRadioButton);
            this.testsForEachISNGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.testsForEachISNGroupBox.Location = new System.Drawing.Point(843, 148);
            this.testsForEachISNGroupBox.Name = "testsForEachISNGroupBox";
            this.testsForEachISNGroupBox.Size = new System.Drawing.Size(193, 94);
            this.testsForEachISNGroupBox.TabIndex = 37;
            this.testsForEachISNGroupBox.TabStop = false;
            this.testsForEachISNGroupBox.Text = "Tests for each ISN";
            // 
            // allTestsRadioButton
            // 
            this.allTestsRadioButton.AutoSize = true;
            this.allTestsRadioButton.Checked = true;
            this.allTestsRadioButton.Location = new System.Drawing.Point(6, 19);
            this.allTestsRadioButton.Name = "allTestsRadioButton";
            this.allTestsRadioButton.Size = new System.Drawing.Size(61, 17);
            this.allTestsRadioButton.TabIndex = 2;
            this.allTestsRadioButton.TabStop = true;
            this.allTestsRadioButton.Text = "All tests";
            this.allTestsRadioButton.UseVisualStyleBackColor = true;
            this.allTestsRadioButton.CheckedChanged += new System.EventHandler(this.testsForEachISNRadioButtons_CheckedChanged);
            // 
            // latestTestsRadioButton
            // 
            this.latestTestsRadioButton.AutoSize = true;
            this.latestTestsRadioButton.Location = new System.Drawing.Point(6, 65);
            this.latestTestsRadioButton.Name = "latestTestsRadioButton";
            this.latestTestsRadioButton.Size = new System.Drawing.Size(96, 17);
            this.latestTestsRadioButton.TabIndex = 1;
            this.latestTestsRadioButton.Text = "Latest test only";
            this.latestTestsRadioButton.UseVisualStyleBackColor = true;
            this.latestTestsRadioButton.CheckedChanged += new System.EventHandler(this.testsForEachISNRadioButtons_CheckedChanged);
            // 
            // firstTimeTestsRadioButton
            // 
            this.firstTimeTestsRadioButton.AutoSize = true;
            this.firstTimeTestsRadioButton.Location = new System.Drawing.Point(6, 42);
            this.firstTimeTestsRadioButton.Name = "firstTimeTestsRadioButton";
            this.firstTimeTestsRadioButton.Size = new System.Drawing.Size(108, 17);
            this.firstTimeTestsRadioButton.TabIndex = 0;
            this.firstTimeTestsRadioButton.Text = "1st time tests only";
            this.firstTimeTestsRadioButton.UseVisualStyleBackColor = true;
            this.firstTimeTestsRadioButton.CheckedChanged += new System.EventHandler(this.testsForEachISNRadioButtons_CheckedChanged);
            // 
            // allowIncompleteTestsCheckBox
            // 
            this.allowIncompleteTestsCheckBox.AutoSize = true;
            this.allowIncompleteTestsCheckBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.allowIncompleteTestsCheckBox.Enabled = false;
            this.allowIncompleteTestsCheckBox.Location = new System.Drawing.Point(843, 375);
            this.allowIncompleteTestsCheckBox.Name = "allowIncompleteTestsCheckBox";
            this.allowIncompleteTestsCheckBox.Size = new System.Drawing.Size(193, 17);
            this.allowIncompleteTestsCheckBox.TabIndex = 40;
            this.allowIncompleteTestsCheckBox.Text = "Allow incomplete tests";
            this.allowIncompleteTestsCheckBox.UseVisualStyleBackColor = true;
            this.allowIncompleteTestsCheckBox.Visible = false;
            this.allowIncompleteTestsCheckBox.CheckedChanged += new System.EventHandler(this.allowIncompleteTestsCheckBox_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label2.Location = new System.Drawing.Point(3, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "To";
            // 
            // lopListDataGridView
            // 
            this.lopListDataGridView.AllowUserToAddRows = false;
            this.lopListDataGridView.AllowUserToDeleteRows = false;
            this.lopListDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.lopListDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lopListDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.lopListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lopListDataGridView.ColumnHeadersVisible = false;
            this.lopListDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lopListDataGridView.Location = new System.Drawing.Point(3, 148);
            this.lopListDataGridView.Name = "lopListDataGridView";
            this.lopListDataGridView.ReadOnly = true;
            this.lopListDataGridView.RowHeadersVisible = false;
            this.tableLayoutPanel1.SetRowSpan(this.lopListDataGridView, 4);
            this.lopListDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.lopListDataGridView.Size = new System.Drawing.Size(114, 244);
            this.lopListDataGridView.TabIndex = 23;
            this.lopListDataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.lopListDataGridView_CellMouseClick);
            // 
            // specificutidsTextBox
            // 
            this.specificutidsTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.specificutidsTextBox.Enabled = false;
            this.specificutidsTextBox.Location = new System.Drawing.Point(723, 148);
            this.specificutidsTextBox.Multiline = true;
            this.specificutidsTextBox.Name = "specificutidsTextBox";
            this.tableLayoutPanel1.SetRowSpan(this.specificutidsTextBox, 4);
            this.specificutidsTextBox.Size = new System.Drawing.Size(114, 244);
            this.specificutidsTextBox.TabIndex = 30;
            this.specificutidsTextBox.TextChanged += new System.EventHandler(this.specificutidsTextBox_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label5.Location = new System.Drawing.Point(723, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "(1 per row)";
            this.toolTip.SetToolTip(this.label5, "Enter a list of UTIDs or UITD Plant Number pairs, one per row");
            // 
            // specificISNsTextBox
            // 
            this.specificISNsTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.specificISNsTextBox.Enabled = false;
            this.specificISNsTextBox.Location = new System.Drawing.Point(603, 148);
            this.specificISNsTextBox.Multiline = true;
            this.specificISNsTextBox.Name = "specificISNsTextBox";
            this.tableLayoutPanel1.SetRowSpan(this.specificISNsTextBox, 4);
            this.specificISNsTextBox.Size = new System.Drawing.Size(114, 244);
            this.specificISNsTextBox.TabIndex = 28;
            this.specificISNsTextBox.TextChanged += new System.EventHandler(this.specificISNsTextBox_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label14.Location = new System.Drawing.Point(603, 132);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(114, 13);
            this.label14.TabIndex = 32;
            this.label14.Text = "(1 per row)";
            this.toolTip.SetToolTip(this.label14, "Enter a list of ISNs, one per row");
            // 
            // testTypeListDataGridView
            // 
            this.testTypeListDataGridView.AllowUserToAddRows = false;
            this.testTypeListDataGridView.AllowUserToDeleteRows = false;
            this.testTypeListDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.testTypeListDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.testTypeListDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.testTypeListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.testTypeListDataGridView.ColumnHeadersVisible = false;
            this.testTypeListDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.testTypeListDataGridView.Location = new System.Drawing.Point(243, 148);
            this.testTypeListDataGridView.Name = "testTypeListDataGridView";
            this.testTypeListDataGridView.ReadOnly = true;
            this.testTypeListDataGridView.RowHeadersVisible = false;
            this.tableLayoutPanel1.SetRowSpan(this.testTypeListDataGridView, 4);
            this.testTypeListDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.testTypeListDataGridView.Size = new System.Drawing.Size(114, 244);
            this.testTypeListDataGridView.TabIndex = 34;
            this.testTypeListDataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.testTypeListDataGridView_CellMouseClick);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label16.Location = new System.Drawing.Point(243, 132);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(114, 13);
            this.label16.TabIndex = 36;
            this.label16.Text = "Test Type";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label4, 4);
            this.label4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label4.Location = new System.Drawing.Point(3, 407);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(474, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "SQL Query - Complete calibration tests";
            // 
            // specificutidsCheckBox
            // 
            this.specificutidsCheckBox.AutoSize = true;
            this.specificutidsCheckBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.specificutidsCheckBox.Location = new System.Drawing.Point(843, 105);
            this.specificutidsCheckBox.Name = "specificutidsCheckBox";
            this.specificutidsCheckBox.Size = new System.Drawing.Size(193, 17);
            this.specificutidsCheckBox.TabIndex = 29;
            this.specificutidsCheckBox.Text = "UTIDs";
            this.specificutidsCheckBox.UseVisualStyleBackColor = true;
            this.specificutidsCheckBox.CheckedChanged += new System.EventHandler(this.specificutidsCheckBox_CheckedChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label15.Location = new System.Drawing.Point(123, 132);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(114, 13);
            this.label15.TabIndex = 35;
            this.label15.Text = "Test Plan";
            // 
            // testplanListDataGridView
            // 
            this.testplanListDataGridView.AllowUserToAddRows = false;
            this.testplanListDataGridView.AllowUserToDeleteRows = false;
            this.testplanListDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.testplanListDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.testplanListDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.testplanListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.testplanListDataGridView.ColumnHeadersVisible = false;
            this.testplanListDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.testplanListDataGridView.Location = new System.Drawing.Point(123, 148);
            this.testplanListDataGridView.Name = "testplanListDataGridView";
            this.testplanListDataGridView.ReadOnly = true;
            this.testplanListDataGridView.RowHeadersVisible = false;
            this.tableLayoutPanel1.SetRowSpan(this.testplanListDataGridView, 4);
            this.testplanListDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.testplanListDataGridView.Size = new System.Drawing.Size(114, 244);
            this.testplanListDataGridView.TabIndex = 33;
            this.testplanListDataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.testplanListDataGridView_CellMouseClick);
            // 
            // specificISNsCheckBox
            // 
            this.specificISNsCheckBox.AutoSize = true;
            this.specificISNsCheckBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.specificISNsCheckBox.Location = new System.Drawing.Point(723, 105);
            this.specificISNsCheckBox.Name = "specificISNsCheckBox";
            this.specificISNsCheckBox.Size = new System.Drawing.Size(114, 17);
            this.specificISNsCheckBox.TabIndex = 19;
            this.specificISNsCheckBox.Text = "ISNs";
            this.specificISNsCheckBox.UseVisualStyleBackColor = true;
            this.specificISNsCheckBox.CheckedChanged += new System.EventHandler(this.specificISNsCheckBox_CheckedChanged);
            // 
            // lineNumberListDataGridView
            // 
            this.lineNumberListDataGridView.AllowUserToAddRows = false;
            this.lineNumberListDataGridView.AllowUserToDeleteRows = false;
            this.lineNumberListDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.lineNumberListDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lineNumberListDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.lineNumberListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lineNumberListDataGridView.ColumnHeadersVisible = false;
            this.lineNumberListDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lineNumberListDataGridView.Enabled = false;
            this.lineNumberListDataGridView.Location = new System.Drawing.Point(483, 148);
            this.lineNumberListDataGridView.Name = "lineNumberListDataGridView";
            this.lineNumberListDataGridView.ReadOnly = true;
            this.lineNumberListDataGridView.RowHeadersVisible = false;
            this.tableLayoutPanel1.SetRowSpan(this.lineNumberListDataGridView, 4);
            this.lineNumberListDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.lineNumberListDataGridView.Size = new System.Drawing.Size(114, 244);
            this.lineNumberListDataGridView.TabIndex = 24;
            this.lineNumberListDataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.lineNumberListDataGridView_CellMouseClick);
            // 
            // completeCalibrationQueryTextBox
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.completeCalibrationQueryTextBox, 4);
            this.completeCalibrationQueryTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.completeCalibrationQueryTextBox.Location = new System.Drawing.Point(3, 423);
            this.completeCalibrationQueryTextBox.Multiline = true;
            this.completeCalibrationQueryTextBox.Name = "completeCalibrationQueryTextBox";
            this.completeCalibrationQueryTextBox.ReadOnly = true;
            this.completeCalibrationQueryTextBox.Size = new System.Drawing.Size(474, 40);
            this.completeCalibrationQueryTextBox.TabIndex = 6;
            // 
            // plantNumberListDataGridView
            // 
            this.plantNumberListDataGridView.AllowUserToAddRows = false;
            this.plantNumberListDataGridView.AllowUserToDeleteRows = false;
            this.plantNumberListDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.plantNumberListDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.plantNumberListDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.plantNumberListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.plantNumberListDataGridView.ColumnHeadersVisible = false;
            this.plantNumberListDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plantNumberListDataGridView.Enabled = false;
            this.plantNumberListDataGridView.Location = new System.Drawing.Point(363, 148);
            this.plantNumberListDataGridView.Name = "plantNumberListDataGridView";
            this.plantNumberListDataGridView.ReadOnly = true;
            this.plantNumberListDataGridView.RowHeadersVisible = false;
            this.tableLayoutPanel1.SetRowSpan(this.plantNumberListDataGridView, 4);
            this.plantNumberListDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.plantNumberListDataGridView.Size = new System.Drawing.Size(114, 244);
            this.plantNumberListDataGridView.TabIndex = 22;
            this.plantNumberListDataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.plantNumberListDataGridView_CellMouseClick);
            this.plantNumberListDataGridView.SelectionChanged += new System.EventHandler(this.plantNumberListDataGridView_SelectionChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label7.Location = new System.Drawing.Point(483, 132);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Line Number";
            // 
            // runMainQueryButton
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.runMainQueryButton, 8);
            this.runMainQueryButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.runMainQueryButton.Location = new System.Drawing.Point(3, 540);
            this.runMainQueryButton.Name = "runMainQueryButton";
            this.runMainQueryButton.Size = new System.Drawing.Size(1033, 43);
            this.runMainQueryButton.TabIndex = 17;
            this.runMainQueryButton.Text = "&Run query";
            this.runMainQueryButton.UseVisualStyleBackColor = true;
            this.runMainQueryButton.Click += new System.EventHandler(this.runMainQueryButton_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label10, 4);
            this.label10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label10.Location = new System.Drawing.Point(483, 407);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(553, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "SQL Query - Incomplete calibration tests";
            // 
            // incompleteCalibrationQueryTextBox
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.incompleteCalibrationQueryTextBox, 4);
            this.incompleteCalibrationQueryTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.incompleteCalibrationQueryTextBox.Location = new System.Drawing.Point(483, 423);
            this.incompleteCalibrationQueryTextBox.Multiline = true;
            this.incompleteCalibrationQueryTextBox.Name = "incompleteCalibrationQueryTextBox";
            this.incompleteCalibrationQueryTextBox.ReadOnly = true;
            this.incompleteCalibrationQueryTextBox.Size = new System.Drawing.Size(553, 40);
            this.incompleteCalibrationQueryTextBox.TabIndex = 20;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label6.Location = new System.Drawing.Point(363, 132);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Plant Number";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label3.Location = new System.Drawing.Point(3, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "LOP";
            // 
            // NumericISNCheckBox
            // 
            this.NumericISNCheckBox.AutoSize = true;
            this.NumericISNCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NumericISNCheckBox.Location = new System.Drawing.Point(843, 70);
            this.NumericISNCheckBox.Name = "NumericISNCheckBox";
            this.NumericISNCheckBox.Size = new System.Drawing.Size(193, 27);
            this.NumericISNCheckBox.TabIndex = 52;
            this.NumericISNCheckBox.Text = "Numeric ISN\'s";
            this.NumericISNCheckBox.UseVisualStyleBackColor = true;
            this.NumericISNCheckBox.CheckedChanged += new System.EventHandler(this.NumericISNCheckBox_CheckedChanged);
            // 
            // checkBoxUseIasiArchive
            // 
            this.checkBoxUseIasiArchive.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxUseIasiArchive.AutoSize = true;
            this.checkBoxUseIasiArchive.Location = new System.Drawing.Point(363, 70);
            this.checkBoxUseIasiArchive.Name = "checkBoxUseIasiArchive";
            this.checkBoxUseIasiArchive.Size = new System.Drawing.Size(114, 27);
            this.checkBoxUseIasiArchive.TabIndex = 54;
            this.checkBoxUseIasiArchive.Text = "Use Iasi Archive";
            this.checkBoxUseIasiArchive.UseVisualStyleBackColor = true;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(3, 103);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(114, 19);
            this.btnUpdate.TabIndex = 55;
            this.btnUpdate.Text = "UPDATE";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // datasetTabPage
            // 
            this.datasetTabPage.Controls.Add(this.DatasetTableLayoutPanel);
            this.datasetTabPage.Location = new System.Drawing.Point(4, 22);
            this.datasetTabPage.Name = "datasetTabPage";
            this.datasetTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.datasetTabPage.Size = new System.Drawing.Size(1045, 601);
            this.datasetTabPage.TabIndex = 1;
            this.datasetTabPage.Text = "Dataset";
            this.datasetTabPage.UseVisualStyleBackColor = true;
            // 
            // DatasetTableLayoutPanel
            // 
            this.DatasetTableLayoutPanel.ColumnCount = 8;
            this.DatasetTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.DatasetTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.DatasetTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.DatasetTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.DatasetTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.DatasetTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.DatasetTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.DatasetTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.DatasetTableLayoutPanel.Controls.Add(this.incompletePumpingDataGridView, 0, 7);
            this.DatasetTableLayoutPanel.Controls.Add(this.completedPumpingDataGridView, 0, 3);
            this.DatasetTableLayoutPanel.Controls.Add(this.label33, 0, 2);
            this.DatasetTableLayoutPanel.Controls.Add(this.label32, 0, 6);
            this.DatasetTableLayoutPanel.Controls.Add(this.label24, 0, 0);
            this.DatasetTableLayoutPanel.Controls.Add(this.completedCalibrationDataGridView, 0, 1);
            this.DatasetTableLayoutPanel.Controls.Add(this.incompleteCalibrationDataGridView, 0, 5);
            this.DatasetTableLayoutPanel.Controls.Add(this.label9, 0, 4);
            this.DatasetTableLayoutPanel.Controls.Add(this.CompleteCalibrationExcelButton, 7, 0);
            this.DatasetTableLayoutPanel.Controls.Add(this.CompletePumpingExcelButton, 7, 2);
            this.DatasetTableLayoutPanel.Controls.Add(this.IncompleteCalibrationExcelButton, 7, 4);
            this.DatasetTableLayoutPanel.Controls.Add(this.IncompletePumpingExcelButton, 7, 6);
            this.DatasetTableLayoutPanel.Controls.Add(this.CompletedCalibrationCount0, 6, 0);
            this.DatasetTableLayoutPanel.Controls.Add(this.CompletedCalibrationCount5, 1, 0);
            this.DatasetTableLayoutPanel.Controls.Add(this.CompletedCalibrationCount4, 2, 0);
            this.DatasetTableLayoutPanel.Controls.Add(this.CompletedCalibrationCount1, 5, 0);
            this.DatasetTableLayoutPanel.Controls.Add(this.CompletedCalibrationCount2, 4, 0);
            this.DatasetTableLayoutPanel.Controls.Add(this.CompletedCalibrationCount3, 3, 0);
            this.DatasetTableLayoutPanel.Controls.Add(this.CompletePumpingCount0, 6, 2);
            this.DatasetTableLayoutPanel.Controls.Add(this.CompletePumpingCount1, 5, 2);
            this.DatasetTableLayoutPanel.Controls.Add(this.CompletePumpingCount2, 4, 2);
            this.DatasetTableLayoutPanel.Controls.Add(this.CompletePumpingCount3, 3, 2);
            this.DatasetTableLayoutPanel.Controls.Add(this.CompletePumpingCount4, 2, 2);
            this.DatasetTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DatasetTableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.DatasetTableLayoutPanel.Name = "DatasetTableLayoutPanel";
            this.DatasetTableLayoutPanel.RowCount = 8;
            this.DatasetTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.DatasetTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.DatasetTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.DatasetTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.DatasetTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.DatasetTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.DatasetTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.DatasetTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.DatasetTableLayoutPanel.Size = new System.Drawing.Size(1039, 595);
            this.DatasetTableLayoutPanel.TabIndex = 8;
            this.DatasetTableLayoutPanel.Click += new System.EventHandler(this.CompletedPumpingCounts_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label33.Location = new System.Drawing.Point(3, 210);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(765, 13);
            this.label33.TabIndex = 8;
            this.label33.Text = "Completed pumping tests";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label32.Location = new System.Drawing.Point(3, 507);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(765, 13);
            this.label32.TabIndex = 7;
            this.label32.Text = "Incomplete pumping tests";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label24.Location = new System.Drawing.Point(3, 12);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(765, 13);
            this.label24.TabIndex = 3;
            this.label24.Text = "Completed calibration tests";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label9.Location = new System.Drawing.Point(3, 408);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(765, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Incomplete calibration tests";
            // 
            // trimTrackerChartsTabPage
            // 
            this.trimTrackerChartsTabPage.Controls.Add(this.tableLayoutPanel4);
            this.trimTrackerChartsTabPage.Location = new System.Drawing.Point(4, 22);
            this.trimTrackerChartsTabPage.Name = "trimTrackerChartsTabPage";
            this.trimTrackerChartsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.trimTrackerChartsTabPage.Size = new System.Drawing.Size(1045, 601);
            this.trimTrackerChartsTabPage.TabIndex = 2;
            this.trimTrackerChartsTabPage.Text = "Trim Tracker Charts";
            this.trimTrackerChartsTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.34F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel4.Controls.Add(this.trimTrackerPlotGradesCheckBox, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.trimTrackerLeakdownChart, 2, 3);
            this.tableLayoutPanel4.Controls.Add(this.trimTrackerCDChart, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.trimTrackerP3Chart, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.trimTrackerMDPChart, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.trimTrackerP1Chart, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.trimTrackerODChart, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.trimTrackerP4Chart, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.trimTrackerP5Chart, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.trimTrackerP2Chart, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 4;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.34F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1039, 595);
            this.tableLayoutPanel4.TabIndex = 21;
            // 
            // trimTrackerPlotGradesCheckBox
            // 
            this.trimTrackerPlotGradesCheckBox.AutoSize = true;
            this.trimTrackerPlotGradesCheckBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.trimTrackerPlotGradesCheckBox.Location = new System.Drawing.Point(3, 5);
            this.trimTrackerPlotGradesCheckBox.Name = "trimTrackerPlotGradesCheckBox";
            this.trimTrackerPlotGradesCheckBox.Size = new System.Drawing.Size(340, 17);
            this.trimTrackerPlotGradesCheckBox.TabIndex = 20;
            this.trimTrackerPlotGradesCheckBox.Text = "Plot trim data by Grade";
            this.trimTrackerPlotGradesCheckBox.UseVisualStyleBackColor = true;
            this.trimTrackerPlotGradesCheckBox.CheckedChanged += new System.EventHandler(this.trimTrackerPlotGradesCheckBox_CheckedChanged);
            // 
            // trimTrackerLeakdownChart
            // 
            chartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea1.AxisX.MinorGrid.Enabled = true;
            chartArea1.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea1.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea1.AxisY.MinorGrid.Enabled = true;
            chartArea1.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea1.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea1.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea1.AxisY2.MajorGrid.Enabled = false;
            chartArea1.Name = "ChartArea1";
            this.trimTrackerLeakdownChart.ChartAreas.Add(chartArea1);
            this.trimTrackerLeakdownChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Alignment = System.Drawing.StringAlignment.Center;
            legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend1.Name = "Legend1";
            this.trimTrackerLeakdownChart.Legends.Add(legend1);
            this.trimTrackerLeakdownChart.Location = new System.Drawing.Point(695, 407);
            this.trimTrackerLeakdownChart.Name = "trimTrackerLeakdownChart";
            this.trimTrackerLeakdownChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            this.trimTrackerLeakdownChart.Size = new System.Drawing.Size(341, 185);
            this.trimTrackerLeakdownChart.TabIndex = 18;
            this.trimTrackerLeakdownChart.Text = "chart7";
            title1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title1.Name = "Title1";
            title1.Text = "Leakdown & SOR";
            this.trimTrackerLeakdownChart.Titles.Add(title1);
            this.trimTrackerLeakdownChart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // trimTrackerCDChart
            // 
            chartArea2.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea2.AxisX.MinorGrid.Enabled = true;
            chartArea2.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea2.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea2.AxisY.MinorGrid.Enabled = true;
            chartArea2.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea2.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea2.Name = "ChartArea1";
            this.trimTrackerCDChart.ChartAreas.Add(chartArea2);
            this.trimTrackerCDChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Alignment = System.Drawing.StringAlignment.Center;
            legend2.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend2.Name = "Legend1";
            this.trimTrackerCDChart.Legends.Add(legend2);
            this.trimTrackerCDChart.Location = new System.Drawing.Point(695, 28);
            this.trimTrackerCDChart.Name = "trimTrackerCDChart";
            this.trimTrackerCDChart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0))))),
        System.Drawing.Color.Yellow,
        System.Drawing.Color.Lime,
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))),
        System.Drawing.Color.Navy};
            this.trimTrackerCDChart.Size = new System.Drawing.Size(341, 184);
            this.trimTrackerCDChart.TabIndex = 19;
            this.trimTrackerCDChart.Text = "chart6";
            title2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title2.Name = "Title1";
            title2.Text = "Closing delay";
            this.trimTrackerCDChart.Titles.Add(title2);
            this.trimTrackerCDChart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // trimTrackerP3Chart
            // 
            chartArea3.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea3.AxisX.MinorGrid.Enabled = true;
            chartArea3.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea3.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea3.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea3.AxisY.MinorGrid.Enabled = true;
            chartArea3.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea3.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea3.Name = "ChartArea1";
            this.trimTrackerP3Chart.ChartAreas.Add(chartArea3);
            this.trimTrackerP3Chart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend3.Alignment = System.Drawing.StringAlignment.Center;
            legend3.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend3.Name = "Legend1";
            this.trimTrackerP3Chart.Legends.Add(legend3);
            this.trimTrackerP3Chart.Location = new System.Drawing.Point(695, 218);
            this.trimTrackerP3Chart.Name = "trimTrackerP3Chart";
            this.trimTrackerP3Chart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0))))),
        System.Drawing.Color.Yellow,
        System.Drawing.Color.Lime,
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))),
        System.Drawing.Color.Navy};
            this.trimTrackerP3Chart.Size = new System.Drawing.Size(341, 183);
            this.trimTrackerP3Chart.TabIndex = 4;
            this.trimTrackerP3Chart.Text = "chart3";
            title3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title3.Name = "Title1";
            title3.Text = "Pressure level 3";
            this.trimTrackerP3Chart.Titles.Add(title3);
            this.trimTrackerP3Chart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // trimTrackerMDPChart
            // 
            chartArea4.AxisX.MajorGrid.Interval = 0D;
            chartArea4.AxisX.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea4.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea4.AxisX.MinorGrid.Enabled = true;
            chartArea4.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea4.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea4.AxisY.MajorGrid.Interval = 0D;
            chartArea4.AxisY.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea4.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea4.AxisY.MinorGrid.Enabled = true;
            chartArea4.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea4.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea4.AxisY2.MajorGrid.Interval = 0D;
            chartArea4.AxisY2.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea4.CursorX.IsUserSelectionEnabled = true;
            chartArea4.CursorY.IsUserSelectionEnabled = true;
            chartArea4.Name = "ChartArea1";
            chartArea5.Name = "ChartArea2";
            this.trimTrackerMDPChart.ChartAreas.Add(chartArea4);
            this.trimTrackerMDPChart.ChartAreas.Add(chartArea5);
            this.trimTrackerMDPChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend4.Alignment = System.Drawing.StringAlignment.Center;
            legend4.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend4.Name = "Legend1";
            this.trimTrackerMDPChart.Legends.Add(legend4);
            this.trimTrackerMDPChart.Location = new System.Drawing.Point(3, 28);
            this.trimTrackerMDPChart.Name = "trimTrackerMDPChart";
            this.trimTrackerMDPChart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0))))),
        System.Drawing.Color.Yellow,
        System.Drawing.Color.Lime,
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))),
        System.Drawing.Color.Navy};
            this.trimTrackerMDPChart.Size = new System.Drawing.Size(340, 184);
            this.trimTrackerMDPChart.TabIndex = 16;
            this.trimTrackerMDPChart.Text = "chart1";
            title4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title4.Name = "Title1";
            title4.Text = "MDP";
            this.trimTrackerMDPChart.Titles.Add(title4);
            this.trimTrackerMDPChart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // trimTrackerP1Chart
            // 
            chartArea6.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea6.AxisX.MinorGrid.Enabled = true;
            chartArea6.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea6.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea6.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea6.AxisY.MinorGrid.Enabled = true;
            chartArea6.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea6.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea6.Name = "ChartArea1";
            this.trimTrackerP1Chart.ChartAreas.Add(chartArea6);
            this.trimTrackerP1Chart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend5.Alignment = System.Drawing.StringAlignment.Center;
            legend5.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend5.Name = "Legend1";
            this.trimTrackerP1Chart.Legends.Add(legend5);
            this.trimTrackerP1Chart.Location = new System.Drawing.Point(3, 218);
            this.trimTrackerP1Chart.Name = "trimTrackerP1Chart";
            this.trimTrackerP1Chart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0))))),
        System.Drawing.Color.Yellow,
        System.Drawing.Color.Lime,
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))),
        System.Drawing.Color.Navy};
            this.trimTrackerP1Chart.Size = new System.Drawing.Size(340, 183);
            this.trimTrackerP1Chart.TabIndex = 12;
            this.trimTrackerP1Chart.Text = "chart8";
            title5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title5.Name = "Title1";
            title5.Text = "Pressure level 1";
            this.trimTrackerP1Chart.Titles.Add(title5);
            this.trimTrackerP1Chart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // trimTrackerODChart
            // 
            chartArea7.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea7.AxisX.MinorGrid.Enabled = true;
            chartArea7.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea7.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea7.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea7.AxisY.MinorGrid.Enabled = true;
            chartArea7.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea7.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea7.Name = "ChartArea1";
            this.trimTrackerODChart.ChartAreas.Add(chartArea7);
            this.trimTrackerODChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend6.Alignment = System.Drawing.StringAlignment.Center;
            legend6.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend6.Name = "Legend1";
            this.trimTrackerODChart.Legends.Add(legend6);
            this.trimTrackerODChart.Location = new System.Drawing.Point(349, 28);
            this.trimTrackerODChart.Name = "trimTrackerODChart";
            this.trimTrackerODChart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0))))),
        System.Drawing.Color.Yellow,
        System.Drawing.Color.Lime,
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))),
        System.Drawing.Color.Navy};
            this.trimTrackerODChart.Size = new System.Drawing.Size(340, 184);
            this.trimTrackerODChart.TabIndex = 10;
            this.trimTrackerODChart.Text = "chart6";
            title6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title6.Name = "Title1";
            title6.Text = "Opening delay";
            this.trimTrackerODChart.Titles.Add(title6);
            this.trimTrackerODChart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // trimTrackerP4Chart
            // 
            chartArea8.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea8.AxisX.MinorGrid.Enabled = true;
            chartArea8.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea8.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea8.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea8.AxisY.MinorGrid.Enabled = true;
            chartArea8.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea8.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea8.Name = "ChartArea1";
            this.trimTrackerP4Chart.ChartAreas.Add(chartArea8);
            this.trimTrackerP4Chart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend7.Alignment = System.Drawing.StringAlignment.Center;
            legend7.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend7.Name = "Legend1";
            this.trimTrackerP4Chart.Legends.Add(legend7);
            this.trimTrackerP4Chart.Location = new System.Drawing.Point(3, 407);
            this.trimTrackerP4Chart.Name = "trimTrackerP4Chart";
            this.trimTrackerP4Chart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0))))),
        System.Drawing.Color.Yellow,
        System.Drawing.Color.Lime,
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))),
        System.Drawing.Color.Navy};
            this.trimTrackerP4Chart.Size = new System.Drawing.Size(340, 185);
            this.trimTrackerP4Chart.TabIndex = 6;
            this.trimTrackerP4Chart.Text = "chart4";
            title7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title7.Name = "Title1";
            title7.Text = "Pressure level 4";
            this.trimTrackerP4Chart.Titles.Add(title7);
            this.trimTrackerP4Chart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // trimTrackerP5Chart
            // 
            chartArea9.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea9.AxisX.MinorGrid.Enabled = true;
            chartArea9.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea9.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea9.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea9.AxisY.MinorGrid.Enabled = true;
            chartArea9.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea9.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea9.Name = "ChartArea1";
            this.trimTrackerP5Chart.ChartAreas.Add(chartArea9);
            this.trimTrackerP5Chart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend8.Alignment = System.Drawing.StringAlignment.Center;
            legend8.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend8.Name = "Legend1";
            this.trimTrackerP5Chart.Legends.Add(legend8);
            this.trimTrackerP5Chart.Location = new System.Drawing.Point(349, 407);
            this.trimTrackerP5Chart.Name = "trimTrackerP5Chart";
            this.trimTrackerP5Chart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0))))),
        System.Drawing.Color.Yellow,
        System.Drawing.Color.Lime,
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))),
        System.Drawing.Color.Navy};
            this.trimTrackerP5Chart.Size = new System.Drawing.Size(340, 185);
            this.trimTrackerP5Chart.TabIndex = 8;
            this.trimTrackerP5Chart.Text = "chart5";
            title8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title8.Name = "Title1";
            title8.Text = "Pressure level 5";
            this.trimTrackerP5Chart.Titles.Add(title8);
            this.trimTrackerP5Chart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // trimTrackerP2Chart
            // 
            chartArea10.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea10.AxisX.MinorGrid.Enabled = true;
            chartArea10.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea10.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea10.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea10.AxisY.MinorGrid.Enabled = true;
            chartArea10.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea10.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea10.Name = "ChartArea1";
            this.trimTrackerP2Chart.ChartAreas.Add(chartArea10);
            this.trimTrackerP2Chart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend9.Alignment = System.Drawing.StringAlignment.Center;
            legend9.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend9.Name = "Legend1";
            this.trimTrackerP2Chart.Legends.Add(legend9);
            this.trimTrackerP2Chart.Location = new System.Drawing.Point(349, 218);
            this.trimTrackerP2Chart.Name = "trimTrackerP2Chart";
            this.trimTrackerP2Chart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0))))),
        System.Drawing.Color.Yellow,
        System.Drawing.Color.Lime,
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))),
        System.Drawing.Color.Navy};
            this.trimTrackerP2Chart.Size = new System.Drawing.Size(340, 183);
            this.trimTrackerP2Chart.TabIndex = 2;
            this.trimTrackerP2Chart.Text = "chart2";
            title9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title9.Name = "Title1";
            title9.Text = "Pressure level 2";
            this.trimTrackerP2Chart.Titles.Add(title9);
            this.trimTrackerP2Chart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // panel2
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.panel2, 2);
            this.panel2.Controls.Add(this.label60);
            this.panel2.Controls.Add(this.trimTrackerPlotChronologicalRadioButton);
            this.panel2.Controls.Add(this.trimTrackerPlotDateRadioButton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(349, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(687, 19);
            this.panel2.TabIndex = 21;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(3, 6);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(59, 13);
            this.label60.TabIndex = 1;
            this.label60.Text = "Chart Type";
            // 
            // trimTrackerPlotChronologicalRadioButton
            // 
            this.trimTrackerPlotChronologicalRadioButton.AutoSize = true;
            this.trimTrackerPlotChronologicalRadioButton.Location = new System.Drawing.Point(122, 4);
            this.trimTrackerPlotChronologicalRadioButton.Name = "trimTrackerPlotChronologicalRadioButton";
            this.trimTrackerPlotChronologicalRadioButton.Size = new System.Drawing.Size(89, 17);
            this.trimTrackerPlotChronologicalRadioButton.TabIndex = 0;
            this.trimTrackerPlotChronologicalRadioButton.Text = "Chronological";
            this.trimTrackerPlotChronologicalRadioButton.UseVisualStyleBackColor = true;
            // 
            // trimTrackerPlotDateRadioButton
            // 
            this.trimTrackerPlotDateRadioButton.AutoSize = true;
            this.trimTrackerPlotDateRadioButton.Checked = true;
            this.trimTrackerPlotDateRadioButton.Location = new System.Drawing.Point(68, 4);
            this.trimTrackerPlotDateRadioButton.Name = "trimTrackerPlotDateRadioButton";
            this.trimTrackerPlotDateRadioButton.Size = new System.Drawing.Size(48, 17);
            this.trimTrackerPlotDateRadioButton.TabIndex = 0;
            this.trimTrackerPlotDateRadioButton.TabStop = true;
            this.trimTrackerPlotDateRadioButton.Text = "Date";
            this.trimTrackerPlotDateRadioButton.UseVisualStyleBackColor = true;
            this.trimTrackerPlotDateRadioButton.CheckedChanged += new System.EventHandler(this.trimTrackerPlotRadioButton_CheckedChanged);
            // 
            // pumpingTrackerChartsTabPage
            // 
            this.pumpingTrackerChartsTabPage.Controls.Add(this.tableLayoutPanel5);
            this.pumpingTrackerChartsTabPage.Location = new System.Drawing.Point(4, 22);
            this.pumpingTrackerChartsTabPage.Name = "pumpingTrackerChartsTabPage";
            this.pumpingTrackerChartsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.pumpingTrackerChartsTabPage.Size = new System.Drawing.Size(1045, 601);
            this.pumpingTrackerChartsTabPage.TabIndex = 8;
            this.pumpingTrackerChartsTabPage.Text = "Pumping Tracker Charts";
            this.pumpingTrackerChartsTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 3;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.34F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel5.Controls.Add(this.pumpingTrackerOMVT3FixedPressureChart, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.pumpingTrackerPressureFixedAngleChart, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.pumpingTrackerAngleFixedPressureChart, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.pumpingTrackerOMVT3FixedAngleChart, 0, 2);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.34F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1039, 595);
            this.tableLayoutPanel5.TabIndex = 26;
            // 
            // pumpingTrackerOMVT3FixedPressureChart
            // 
            chartArea11.AxisX.MajorGrid.Interval = 0D;
            chartArea11.AxisX.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea11.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea11.AxisX.MinorGrid.Enabled = true;
            chartArea11.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea11.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea11.AxisY.MajorGrid.Interval = 0D;
            chartArea11.AxisY.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea11.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea11.AxisY.MinorGrid.Enabled = true;
            chartArea11.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea11.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea11.AxisY2.MajorGrid.Interval = 0D;
            chartArea11.AxisY2.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea11.CursorX.IsUserSelectionEnabled = true;
            chartArea11.CursorY.IsUserSelectionEnabled = true;
            chartArea11.Name = "ChartArea1";
            this.pumpingTrackerOMVT3FixedPressureChart.ChartAreas.Add(chartArea11);
            this.pumpingTrackerOMVT3FixedPressureChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend10.Alignment = System.Drawing.StringAlignment.Center;
            legend10.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend10.Name = "Legend1";
            this.pumpingTrackerOMVT3FixedPressureChart.Legends.Add(legend10);
            this.pumpingTrackerOMVT3FixedPressureChart.Location = new System.Drawing.Point(3, 28);
            this.pumpingTrackerOMVT3FixedPressureChart.Name = "pumpingTrackerOMVT3FixedPressureChart";
            this.pumpingTrackerOMVT3FixedPressureChart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0))))),
        System.Drawing.Color.Yellow,
        System.Drawing.Color.Lime,
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))),
        System.Drawing.Color.Navy};
            this.pumpingTrackerOMVT3FixedPressureChart.Size = new System.Drawing.Size(340, 184);
            this.pumpingTrackerOMVT3FixedPressureChart.TabIndex = 22;
            this.pumpingTrackerOMVT3FixedPressureChart.Text = "chart1";
            title10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title10.Name = "Title1";
            title10.Text = "OMVT3 @ fixed pressure";
            this.pumpingTrackerOMVT3FixedPressureChart.Titles.Add(title10);
            this.pumpingTrackerOMVT3FixedPressureChart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // pumpingTrackerPressureFixedAngleChart
            // 
            chartArea12.AxisX.MajorGrid.Interval = 0D;
            chartArea12.AxisX.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea12.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea12.AxisX.MinorGrid.Enabled = true;
            chartArea12.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea12.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea12.AxisY.MajorGrid.Interval = 0D;
            chartArea12.AxisY.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea12.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea12.AxisY.MinorGrid.Enabled = true;
            chartArea12.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea12.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea12.AxisY2.MajorGrid.Interval = 0D;
            chartArea12.AxisY2.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea12.CursorX.IsUserSelectionEnabled = true;
            chartArea12.CursorY.IsUserSelectionEnabled = true;
            chartArea12.Name = "ChartArea1";
            this.pumpingTrackerPressureFixedAngleChart.ChartAreas.Add(chartArea12);
            this.pumpingTrackerPressureFixedAngleChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend11.Alignment = System.Drawing.StringAlignment.Center;
            legend11.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend11.Name = "Legend1";
            this.pumpingTrackerPressureFixedAngleChart.Legends.Add(legend11);
            this.pumpingTrackerPressureFixedAngleChart.Location = new System.Drawing.Point(349, 218);
            this.pumpingTrackerPressureFixedAngleChart.Name = "pumpingTrackerPressureFixedAngleChart";
            this.pumpingTrackerPressureFixedAngleChart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0))))),
        System.Drawing.Color.Yellow,
        System.Drawing.Color.Lime,
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))),
        System.Drawing.Color.Navy};
            this.pumpingTrackerPressureFixedAngleChart.Size = new System.Drawing.Size(340, 183);
            this.pumpingTrackerPressureFixedAngleChart.TabIndex = 25;
            this.pumpingTrackerPressureFixedAngleChart.Text = "chart1";
            title11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title11.Name = "Title1";
            title11.Text = "Rail Pressure @ fixed pumping angle";
            this.pumpingTrackerPressureFixedAngleChart.Titles.Add(title11);
            this.pumpingTrackerPressureFixedAngleChart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // pumpingTrackerAngleFixedPressureChart
            // 
            chartArea13.AxisX.MajorGrid.Interval = 0D;
            chartArea13.AxisX.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea13.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea13.AxisX.MinorGrid.Enabled = true;
            chartArea13.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea13.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea13.AxisY.MajorGrid.Interval = 0D;
            chartArea13.AxisY.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea13.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea13.AxisY.MinorGrid.Enabled = true;
            chartArea13.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea13.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea13.AxisY2.MajorGrid.Interval = 0D;
            chartArea13.AxisY2.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea13.CursorX.IsUserSelectionEnabled = true;
            chartArea13.CursorY.IsUserSelectionEnabled = true;
            chartArea13.Name = "ChartArea1";
            this.pumpingTrackerAngleFixedPressureChart.ChartAreas.Add(chartArea13);
            this.pumpingTrackerAngleFixedPressureChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend12.Alignment = System.Drawing.StringAlignment.Center;
            legend12.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend12.Name = "Legend1";
            this.pumpingTrackerAngleFixedPressureChart.Legends.Add(legend12);
            this.pumpingTrackerAngleFixedPressureChart.Location = new System.Drawing.Point(349, 28);
            this.pumpingTrackerAngleFixedPressureChart.Name = "pumpingTrackerAngleFixedPressureChart";
            this.pumpingTrackerAngleFixedPressureChart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0))))),
        System.Drawing.Color.Yellow,
        System.Drawing.Color.Lime,
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))),
        System.Drawing.Color.Navy};
            this.pumpingTrackerAngleFixedPressureChart.Size = new System.Drawing.Size(340, 184);
            this.pumpingTrackerAngleFixedPressureChart.TabIndex = 23;
            this.pumpingTrackerAngleFixedPressureChart.Text = "chart1";
            title12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title12.Name = "Title1";
            title12.Text = "Pumping Angle @ fixed pressure";
            this.pumpingTrackerAngleFixedPressureChart.Titles.Add(title12);
            this.pumpingTrackerAngleFixedPressureChart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // pumpingTrackerOMVT3FixedAngleChart
            // 
            chartArea14.AxisX.MajorGrid.Interval = 0D;
            chartArea14.AxisX.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea14.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea14.AxisX.MinorGrid.Enabled = true;
            chartArea14.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea14.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea14.AxisY.MajorGrid.Interval = 0D;
            chartArea14.AxisY.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea14.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea14.AxisY.MinorGrid.Enabled = true;
            chartArea14.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea14.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea14.AxisY2.MajorGrid.Interval = 0D;
            chartArea14.AxisY2.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea14.CursorX.IsUserSelectionEnabled = true;
            chartArea14.CursorY.IsUserSelectionEnabled = true;
            chartArea14.Name = "ChartArea1";
            this.pumpingTrackerOMVT3FixedAngleChart.ChartAreas.Add(chartArea14);
            this.pumpingTrackerOMVT3FixedAngleChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend13.Alignment = System.Drawing.StringAlignment.Center;
            legend13.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend13.Name = "Legend1";
            this.pumpingTrackerOMVT3FixedAngleChart.Legends.Add(legend13);
            this.pumpingTrackerOMVT3FixedAngleChart.Location = new System.Drawing.Point(3, 218);
            this.pumpingTrackerOMVT3FixedAngleChart.Name = "pumpingTrackerOMVT3FixedAngleChart";
            this.pumpingTrackerOMVT3FixedAngleChart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0))))),
        System.Drawing.Color.Yellow,
        System.Drawing.Color.Lime,
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))),
        System.Drawing.Color.Navy};
            this.pumpingTrackerOMVT3FixedAngleChart.Size = new System.Drawing.Size(340, 183);
            this.pumpingTrackerOMVT3FixedAngleChart.TabIndex = 24;
            this.pumpingTrackerOMVT3FixedAngleChart.Text = "chart1";
            title13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title13.Name = "Title1";
            title13.Text = "OMVT3 @ fixed pumping angle";
            this.pumpingTrackerOMVT3FixedAngleChart.Titles.Add(title13);
            this.pumpingTrackerOMVT3FixedAngleChart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // cltTrackerTabPage
            // 
            this.cltTrackerTabPage.Controls.Add(this.tableLayoutPanel6);
            this.cltTrackerTabPage.Location = new System.Drawing.Point(4, 22);
            this.cltTrackerTabPage.Name = "cltTrackerTabPage";
            this.cltTrackerTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.cltTrackerTabPage.Size = new System.Drawing.Size(1045, 601);
            this.cltTrackerTabPage.TabIndex = 9;
            this.cltTrackerTabPage.Text = "CLT Tracker";
            this.cltTrackerTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.panel1, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.cltP1TrackerChart, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.cltP4TrackerChart, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.cltP5TrackerChart, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.cltP3TrackerChart, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.cltP2TrackerChart, 0, 1);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 3;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.34F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1039, 595);
            this.tableLayoutPanel6.TabIndex = 20;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cltTrackerShowRealCheckBox);
            this.panel1.Controls.Add(this.cltTrackerShowExpectedCheckBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(522, 399);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(514, 193);
            this.panel1.TabIndex = 21;
            // 
            // cltTrackerShowRealCheckBox
            // 
            this.cltTrackerShowRealCheckBox.AutoSize = true;
            this.cltTrackerShowRealCheckBox.Checked = true;
            this.cltTrackerShowRealCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cltTrackerShowRealCheckBox.Location = new System.Drawing.Point(3, 3);
            this.cltTrackerShowRealCheckBox.Name = "cltTrackerShowRealCheckBox";
            this.cltTrackerShowRealCheckBox.Size = new System.Drawing.Size(133, 17);
            this.cltTrackerShowRealCheckBox.TabIndex = 19;
            this.cltTrackerShowRealCheckBox.Text = "Show Real fuel grades";
            this.cltTrackerShowRealCheckBox.UseVisualStyleBackColor = true;
            this.cltTrackerShowRealCheckBox.CheckedChanged += new System.EventHandler(this.cltTrackerShowRealCheckBox_CheckedChanged);
            // 
            // cltTrackerShowExpectedCheckBox
            // 
            this.cltTrackerShowExpectedCheckBox.AutoSize = true;
            this.cltTrackerShowExpectedCheckBox.Location = new System.Drawing.Point(3, 26);
            this.cltTrackerShowExpectedCheckBox.Name = "cltTrackerShowExpectedCheckBox";
            this.cltTrackerShowExpectedCheckBox.Size = new System.Drawing.Size(156, 17);
            this.cltTrackerShowExpectedCheckBox.TabIndex = 18;
            this.cltTrackerShowExpectedCheckBox.Text = "Show Expected fuel grades";
            this.cltTrackerShowExpectedCheckBox.UseVisualStyleBackColor = true;
            this.cltTrackerShowExpectedCheckBox.CheckedChanged += new System.EventHandler(this.cltTrackerShowExpectedCheckBox_CheckedChanged);
            // 
            // cltP1TrackerChart
            // 
            chartArea15.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea15.AxisX.MinorGrid.Enabled = true;
            chartArea15.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea15.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea15.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea15.AxisY.MinorGrid.Enabled = true;
            chartArea15.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea15.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea15.Name = "ChartArea1";
            this.cltP1TrackerChart.ChartAreas.Add(chartArea15);
            this.cltP1TrackerChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cltP1TrackerChart.Location = new System.Drawing.Point(3, 3);
            this.cltP1TrackerChart.Name = "cltP1TrackerChart";
            this.cltP1TrackerChart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0))))),
        System.Drawing.Color.Yellow,
        System.Drawing.Color.Lime,
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))),
        System.Drawing.Color.Navy};
            this.cltP1TrackerChart.Size = new System.Drawing.Size(513, 192);
            this.cltP1TrackerChart.TabIndex = 13;
            this.cltP1TrackerChart.Text = "chart8";
            title14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title14.Name = "Title1";
            title14.Text = "Closed Loop Trim - Pressure 1";
            this.cltP1TrackerChart.Titles.Add(title14);
            this.cltP1TrackerChart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // cltP4TrackerChart
            // 
            chartArea16.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea16.AxisX.MinorGrid.Enabled = true;
            chartArea16.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea16.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea16.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea16.AxisY.MinorGrid.Enabled = true;
            chartArea16.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea16.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea16.Name = "ChartArea1";
            this.cltP4TrackerChart.ChartAreas.Add(chartArea16);
            this.cltP4TrackerChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cltP4TrackerChart.Location = new System.Drawing.Point(522, 3);
            this.cltP4TrackerChart.Name = "cltP4TrackerChart";
            this.cltP4TrackerChart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0))))),
        System.Drawing.Color.Yellow,
        System.Drawing.Color.Lime,
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))),
        System.Drawing.Color.Navy};
            this.cltP4TrackerChart.Size = new System.Drawing.Size(514, 192);
            this.cltP4TrackerChart.TabIndex = 14;
            this.cltP4TrackerChart.Text = "chart8";
            title15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title15.Name = "Title1";
            title15.Text = "Closed Loop Trim - Pressure 4";
            this.cltP4TrackerChart.Titles.Add(title15);
            this.cltP4TrackerChart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // cltP5TrackerChart
            // 
            chartArea17.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea17.AxisX.MinorGrid.Enabled = true;
            chartArea17.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea17.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea17.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea17.AxisY.MinorGrid.Enabled = true;
            chartArea17.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea17.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea17.Name = "ChartArea1";
            this.cltP5TrackerChart.ChartAreas.Add(chartArea17);
            this.cltP5TrackerChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cltP5TrackerChart.Location = new System.Drawing.Point(522, 201);
            this.cltP5TrackerChart.Name = "cltP5TrackerChart";
            this.cltP5TrackerChart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0))))),
        System.Drawing.Color.Yellow,
        System.Drawing.Color.Lime,
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))),
        System.Drawing.Color.Navy};
            this.cltP5TrackerChart.Size = new System.Drawing.Size(514, 192);
            this.cltP5TrackerChart.TabIndex = 16;
            this.cltP5TrackerChart.Text = "chart8";
            title16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title16.Name = "Title1";
            title16.Text = "Closed Loop Trim - Pressure 5";
            this.cltP5TrackerChart.Titles.Add(title16);
            this.cltP5TrackerChart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // cltP3TrackerChart
            // 
            chartArea18.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea18.AxisX.MinorGrid.Enabled = true;
            chartArea18.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea18.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea18.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea18.AxisY.MinorGrid.Enabled = true;
            chartArea18.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea18.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea18.Name = "ChartArea1";
            this.cltP3TrackerChart.ChartAreas.Add(chartArea18);
            this.cltP3TrackerChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cltP3TrackerChart.Location = new System.Drawing.Point(3, 399);
            this.cltP3TrackerChart.Name = "cltP3TrackerChart";
            this.cltP3TrackerChart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0))))),
        System.Drawing.Color.Yellow,
        System.Drawing.Color.Lime,
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))),
        System.Drawing.Color.Navy};
            this.cltP3TrackerChart.Size = new System.Drawing.Size(513, 193);
            this.cltP3TrackerChart.TabIndex = 17;
            this.cltP3TrackerChart.Text = "chart8";
            title17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title17.Name = "Title1";
            title17.Text = "Closed Loop Trim - Pressure 3";
            this.cltP3TrackerChart.Titles.Add(title17);
            this.cltP3TrackerChart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // cltP2TrackerChart
            // 
            chartArea19.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea19.AxisX.MinorGrid.Enabled = true;
            chartArea19.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea19.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea19.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea19.AxisY.MinorGrid.Enabled = true;
            chartArea19.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea19.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea19.Name = "ChartArea1";
            this.cltP2TrackerChart.ChartAreas.Add(chartArea19);
            this.cltP2TrackerChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cltP2TrackerChart.Location = new System.Drawing.Point(3, 201);
            this.cltP2TrackerChart.Name = "cltP2TrackerChart";
            this.cltP2TrackerChart.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0))))),
        System.Drawing.Color.Yellow,
        System.Drawing.Color.Lime,
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))),
        System.Drawing.Color.Navy};
            this.cltP2TrackerChart.Size = new System.Drawing.Size(513, 192);
            this.cltP2TrackerChart.TabIndex = 15;
            this.cltP2TrackerChart.Text = "chart8";
            title18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title18.Name = "Title1";
            title18.Text = "Closed Loop Trim - Pressure 2";
            this.cltP2TrackerChart.Titles.Add(title18);
            this.cltP2TrackerChart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // failureAnalysisTabPage
            // 
            this.failureAnalysisTabPage.Controls.Add(this.tableLayoutPanel7);
            this.failureAnalysisTabPage.Location = new System.Drawing.Point(4, 22);
            this.failureAnalysisTabPage.Name = "failureAnalysisTabPage";
            this.failureAnalysisTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.failureAnalysisTabPage.Size = new System.Drawing.Size(1045, 601);
            this.failureAnalysisTabPage.TabIndex = 3;
            this.failureAnalysisTabPage.Text = "Failure analysis";
            this.failureAnalysisTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.tableLayoutPanel7.Controls.Add(this.overallPassRateGridView, 1, 5);
            this.tableLayoutPanel7.Controls.Add(this.failureFullDataGridView, 0, 9);
            this.tableLayoutPanel7.Controls.Add(this.analyseFailedInjectorsButton, 1, 7);
            this.tableLayoutPanel7.Controls.Add(this.label20, 0, 8);
            this.tableLayoutPanel7.Controls.Add(this.passRateGridView, 1, 3);
            this.tableLayoutPanel7.Controls.Add(this.failureParetoChart, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.label50, 1, 4);
            this.tableLayoutPanel7.Controls.Add(this.panel3, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.panel4, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel3, 1, 6);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel34, 1, 2);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel35, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.excelButton12, 1, 8);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 10;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 53.84615F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.15385F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1039, 595);
            this.tableLayoutPanel7.TabIndex = 26;
            // 
            // overallPassRateGridView
            // 
            this.overallPassRateGridView.AllowUserToAddRows = false;
            this.overallPassRateGridView.AllowUserToDeleteRows = false;
            this.overallPassRateGridView.AllowUserToResizeRows = false;
            this.overallPassRateGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.overallPassRateGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            this.overallPassRateGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.overallPassRateGridView.ColumnHeadersHeight = 35;
            this.overallPassRateGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.pumpPasses,
            this.pumpFails,
            this.pumpIncompleteTests,
            this.pumpTotal,
            this.pumpPassRate});
            this.overallPassRateGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.overallPassRateGridView.Location = new System.Drawing.Point(692, 246);
            this.overallPassRateGridView.Name = "overallPassRateGridView";
            this.overallPassRateGridView.ReadOnly = true;
            this.overallPassRateGridView.RowHeadersVisible = false;
            this.overallPassRateGridView.Size = new System.Drawing.Size(344, 58);
            this.overallPassRateGridView.TabIndex = 27;
            // 
            // pumpPasses
            // 
            this.pumpPasses.FillWeight = 89.54366F;
            this.pumpPasses.HeaderText = "Passes";
            this.pumpPasses.Name = "pumpPasses";
            this.pumpPasses.ReadOnly = true;
            // 
            // pumpFails
            // 
            this.pumpFails.FillWeight = 82.19127F;
            this.pumpFails.HeaderText = "Fails";
            this.pumpFails.Name = "pumpFails";
            this.pumpFails.ReadOnly = true;
            // 
            // pumpIncompleteTests
            // 
            this.pumpIncompleteTests.FillWeight = 105.9214F;
            this.pumpIncompleteTests.HeaderText = "Incomplete Tests";
            this.pumpIncompleteTests.Name = "pumpIncompleteTests";
            this.pumpIncompleteTests.ReadOnly = true;
            // 
            // pumpTotal
            // 
            this.pumpTotal.FillWeight = 71.66203F;
            this.pumpTotal.HeaderText = "Total";
            this.pumpTotal.Name = "pumpTotal";
            this.pumpTotal.ReadOnly = true;
            // 
            // pumpPassRate
            // 
            this.pumpPassRate.FillWeight = 67.94047F;
            this.pumpPassRate.HeaderText = "PR (%)";
            this.pumpPassRate.Name = "pumpPassRate";
            this.pumpPassRate.ReadOnly = true;
            // 
            // failureFullDataGridView
            // 
            this.failureFullDataGridView.AllowUserToAddRows = false;
            this.failureFullDataGridView.AllowUserToDeleteRows = false;
            this.failureFullDataGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            this.failureFullDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.failureFullDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.failureFullDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel7.SetColumnSpan(this.failureFullDataGridView, 2);
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.failureFullDataGridView.DefaultCellStyle = dataGridViewCellStyle10;
            this.failureFullDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.failureFullDataGridView.Location = new System.Drawing.Point(3, 465);
            this.failureFullDataGridView.Name = "failureFullDataGridView";
            this.failureFullDataGridView.ReadOnly = true;
            this.failureFullDataGridView.Size = new System.Drawing.Size(1033, 127);
            this.failureFullDataGridView.TabIndex = 9;
            // 
            // analyseFailedInjectorsButton
            // 
            this.analyseFailedInjectorsButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.analyseFailedInjectorsButton.Location = new System.Drawing.Point(692, 400);
            this.analyseFailedInjectorsButton.Name = "analyseFailedInjectorsButton";
            this.analyseFailedInjectorsButton.Size = new System.Drawing.Size(344, 34);
            this.analyseFailedInjectorsButton.TabIndex = 25;
            this.analyseFailedInjectorsButton.Text = "Analyse failed injectors";
            this.analyseFailedInjectorsButton.UseVisualStyleBackColor = true;
            this.analyseFailedInjectorsButton.Click += new System.EventHandler(this.analyseFailedInjectorsButton_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label20.Location = new System.Drawing.Point(3, 449);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(683, 13);
            this.label20.TabIndex = 3;
            this.label20.Text = "Full dataset";
            // 
            // passRateGridView
            // 
            this.passRateGridView.AllowUserToAddRows = false;
            this.passRateGridView.AllowUserToDeleteRows = false;
            this.passRateGridView.AllowUserToResizeRows = false;
            this.passRateGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.passRateGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            this.passRateGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.passRateGridView.ColumnHeadersHeight = 35;
            this.passRateGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.calDay,
            this.calPasses,
            this.calFails,
            this.calIncompleteTests,
            this.calTotal,
            this.calPassRate});
            this.passRateGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.passRateGridView.Location = new System.Drawing.Point(692, 68);
            this.passRateGridView.Name = "passRateGridView";
            this.passRateGridView.ReadOnly = true;
            this.passRateGridView.RowHeadersVisible = false;
            this.passRateGridView.Size = new System.Drawing.Size(344, 147);
            this.passRateGridView.TabIndex = 24;
            // 
            // calDay
            // 
            this.calDay.FillWeight = 182.7411F;
            this.calDay.HeaderText = "Time Period";
            this.calDay.Name = "calDay";
            this.calDay.ReadOnly = true;
            // 
            // calPasses
            // 
            this.calPasses.FillWeight = 89.54365F;
            this.calPasses.HeaderText = "Passes";
            this.calPasses.Name = "calPasses";
            this.calPasses.ReadOnly = true;
            // 
            // calFails
            // 
            this.calFails.FillWeight = 82.19127F;
            this.calFails.HeaderText = "Fails";
            this.calFails.Name = "calFails";
            this.calFails.ReadOnly = true;
            // 
            // calIncompleteTests
            // 
            this.calIncompleteTests.FillWeight = 105.9214F;
            this.calIncompleteTests.HeaderText = "Incomplete Tests";
            this.calIncompleteTests.Name = "calIncompleteTests";
            this.calIncompleteTests.ReadOnly = true;
            // 
            // calTotal
            // 
            this.calTotal.FillWeight = 71.66203F;
            this.calTotal.HeaderText = "Total";
            this.calTotal.Name = "calTotal";
            this.calTotal.ReadOnly = true;
            // 
            // calPassRate
            // 
            this.calPassRate.FillWeight = 67.94047F;
            this.calPassRate.HeaderText = "PR (%)";
            this.calPassRate.Name = "calPassRate";
            this.calPassRate.ReadOnly = true;
            // 
            // failureParetoChart
            // 
            chartArea20.AxisX.LabelStyle.Enabled = false;
            chartArea20.AxisX.MajorGrid.Enabled = false;
            chartArea20.AxisX.MajorTickMark.Enabled = false;
            chartArea20.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea20.AxisY.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea20.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea20.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea20.Name = "ChartArea1";
            this.failureParetoChart.ChartAreas.Add(chartArea20);
            this.failureParetoChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend14.Name = "Legend1";
            this.failureParetoChart.Legends.Add(legend14);
            this.failureParetoChart.Location = new System.Drawing.Point(3, 23);
            this.failureParetoChart.Name = "failureParetoChart";
            this.tableLayoutPanel7.SetRowSpan(this.failureParetoChart, 7);
            this.failureParetoChart.Size = new System.Drawing.Size(683, 411);
            this.failureParetoChart.TabIndex = 1;
            this.failureParetoChart.Text = "chart9";
            title19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title19.Name = "Title1";
            title19.Text = "Failure Pareto";
            this.failureParetoChart.Titles.Add(title19);
            this.failureParetoChart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label50.Location = new System.Drawing.Point(692, 230);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(344, 13);
            this.label50.TabIndex = 26;
            this.label50.Text = "Overall Pass Rate";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.SplitResultsByRig);
            this.panel3.Controls.Add(this.label59);
            this.panel3.Controls.Add(this.SplitResultsByShift);
            this.panel3.Controls.Add(this.SplitResults10Till10);
            this.panel3.Controls.Add(this.SplitResults6Till6);
            this.panel3.Controls.Add(this.SplitResultsDaily);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(689, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(350, 20);
            this.panel3.TabIndex = 28;
            // 
            // SplitResultsByRig
            // 
            this.SplitResultsByRig.AutoSize = true;
            this.SplitResultsByRig.Location = new System.Drawing.Point(310, 2);
            this.SplitResultsByRig.Name = "SplitResultsByRig";
            this.SplitResultsByRig.Size = new System.Drawing.Size(42, 17);
            this.SplitResultsByRig.TabIndex = 2;
            this.SplitResultsByRig.Text = "Rig";
            this.SplitResultsByRig.UseVisualStyleBackColor = true;
            this.SplitResultsByRig.CheckStateChanged += new System.EventHandler(this.FailureAnalysis_ParametersChanged);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(3, 4);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(79, 13);
            this.label59.TabIndex = 1;
            this.label59.Text = "Split Results by";
            // 
            // SplitResultsByShift
            // 
            this.SplitResultsByShift.AutoSize = true;
            this.SplitResultsByShift.Location = new System.Drawing.Point(258, 2);
            this.SplitResultsByShift.Name = "SplitResultsByShift";
            this.SplitResultsByShift.Size = new System.Drawing.Size(46, 17);
            this.SplitResultsByShift.TabIndex = 0;
            this.SplitResultsByShift.Text = "Shift";
            this.SplitResultsByShift.UseVisualStyleBackColor = true;
            this.SplitResultsByShift.CheckedChanged += new System.EventHandler(this.FailureAnalysis_ParametersChanged);
            // 
            // SplitResults10Till10
            // 
            this.SplitResults10Till10.AutoSize = true;
            this.SplitResults10Till10.Location = new System.Drawing.Point(192, 2);
            this.SplitResults10Till10.Name = "SplitResults10Till10";
            this.SplitResults10Till10.Size = new System.Drawing.Size(60, 17);
            this.SplitResults10Till10.TabIndex = 0;
            this.SplitResults10Till10.Text = "10 / 10";
            this.SplitResults10Till10.UseVisualStyleBackColor = true;
            this.SplitResults10Till10.CheckedChanged += new System.EventHandler(this.FailureAnalysis_ParametersChanged);
            // 
            // SplitResults6Till6
            // 
            this.SplitResults6Till6.AutoSize = true;
            this.SplitResults6Till6.Location = new System.Drawing.Point(138, 2);
            this.SplitResults6Till6.Name = "SplitResults6Till6";
            this.SplitResults6Till6.Size = new System.Drawing.Size(48, 17);
            this.SplitResults6Till6.TabIndex = 0;
            this.SplitResults6Till6.Text = "6 / 6";
            this.SplitResults6Till6.UseVisualStyleBackColor = true;
            this.SplitResults6Till6.CheckedChanged += new System.EventHandler(this.FailureAnalysis_ParametersChanged);
            // 
            // SplitResultsDaily
            // 
            this.SplitResultsDaily.AutoSize = true;
            this.SplitResultsDaily.Checked = true;
            this.SplitResultsDaily.Location = new System.Drawing.Point(88, 2);
            this.SplitResultsDaily.Name = "SplitResultsDaily";
            this.SplitResultsDaily.Size = new System.Drawing.Size(44, 17);
            this.SplitResultsDaily.TabIndex = 0;
            this.SplitResultsDaily.TabStop = true;
            this.SplitResultsDaily.Text = "Day";
            this.SplitResultsDaily.UseVisualStyleBackColor = true;
            this.SplitResultsDaily.CheckedChanged += new System.EventHandler(this.FailureAnalysis_ParametersChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.IgnoreIncompleteResults);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(689, 20);
            this.panel4.Margin = new System.Windows.Forms.Padding(0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(350, 20);
            this.panel4.TabIndex = 29;
            // 
            // IgnoreIncompleteResults
            // 
            this.IgnoreIncompleteResults.AutoSize = true;
            this.IgnoreIncompleteResults.Location = new System.Drawing.Point(88, 3);
            this.IgnoreIncompleteResults.Name = "IgnoreIncompleteResults";
            this.IgnoreIncompleteResults.Size = new System.Drawing.Size(149, 17);
            this.IgnoreIncompleteResults.TabIndex = 0;
            this.IgnoreIncompleteResults.Text = "Ignore Incomplete Results";
            this.IgnoreIncompleteResults.UseVisualStyleBackColor = true;
            this.IgnoreIncompleteResults.CheckStateChanged += new System.EventHandler(this.FailureAnalysis_ParametersChanged);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.groupBox5, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.groupBox6, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(689, 307);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(350, 90);
            this.tableLayoutPanel3.TabIndex = 30;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.graphFailureModesRadioButton);
            this.groupBox5.Controls.Add(this.graphParetoRadioButton);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(169, 84);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Graph";
            // 
            // graphFailureModesRadioButton
            // 
            this.graphFailureModesRadioButton.AutoSize = true;
            this.graphFailureModesRadioButton.Location = new System.Drawing.Point(6, 39);
            this.graphFailureModesRadioButton.Name = "graphFailureModesRadioButton";
            this.graphFailureModesRadioButton.Size = new System.Drawing.Size(91, 17);
            this.graphFailureModesRadioButton.TabIndex = 3;
            this.graphFailureModesRadioButton.Text = "Failure Modes";
            this.graphFailureModesRadioButton.UseVisualStyleBackColor = true;
            // 
            // graphParetoRadioButton
            // 
            this.graphParetoRadioButton.AutoSize = true;
            this.graphParetoRadioButton.Checked = true;
            this.graphParetoRadioButton.Location = new System.Drawing.Point(6, 16);
            this.graphParetoRadioButton.Name = "graphParetoRadioButton";
            this.graphParetoRadioButton.Size = new System.Drawing.Size(56, 17);
            this.graphParetoRadioButton.TabIndex = 2;
            this.graphParetoRadioButton.TabStop = true;
            this.graphParetoRadioButton.Text = "Pareto";
            this.graphParetoRadioButton.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.filterAllRadioButton);
            this.groupBox6.Controls.Add(this.filterTestFailsRadioButton);
            this.groupBox6.Controls.Add(this.filterOEEFailsRadioButton);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Location = new System.Drawing.Point(178, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(169, 84);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "OEE Filter";
            // 
            // filterAllRadioButton
            // 
            this.filterAllRadioButton.AutoSize = true;
            this.filterAllRadioButton.Location = new System.Drawing.Point(6, 62);
            this.filterAllRadioButton.Name = "filterAllRadioButton";
            this.filterAllRadioButton.Size = new System.Drawing.Size(36, 17);
            this.filterAllRadioButton.TabIndex = 5;
            this.filterAllRadioButton.Text = "All";
            this.filterAllRadioButton.UseVisualStyleBackColor = true;
            this.filterAllRadioButton.CheckedChanged += new System.EventHandler(this.FailureFilterRadioButton_CheckedChanged);
            // 
            // filterTestFailsRadioButton
            // 
            this.filterTestFailsRadioButton.AutoSize = true;
            this.filterTestFailsRadioButton.Checked = true;
            this.filterTestFailsRadioButton.Location = new System.Drawing.Point(6, 16);
            this.filterTestFailsRadioButton.Name = "filterTestFailsRadioButton";
            this.filterTestFailsRadioButton.Size = new System.Drawing.Size(70, 17);
            this.filterTestFailsRadioButton.TabIndex = 4;
            this.filterTestFailsRadioButton.TabStop = true;
            this.filterTestFailsRadioButton.Text = "Test Fails";
            this.filterTestFailsRadioButton.UseVisualStyleBackColor = true;
            this.filterTestFailsRadioButton.CheckedChanged += new System.EventHandler(this.FailureFilterRadioButton_CheckedChanged);
            // 
            // filterOEEFailsRadioButton
            // 
            this.filterOEEFailsRadioButton.AutoSize = true;
            this.filterOEEFailsRadioButton.Location = new System.Drawing.Point(6, 39);
            this.filterOEEFailsRadioButton.Name = "filterOEEFailsRadioButton";
            this.filterOEEFailsRadioButton.Size = new System.Drawing.Size(71, 17);
            this.filterOEEFailsRadioButton.TabIndex = 3;
            this.filterOEEFailsRadioButton.Text = "OEE Fails";
            this.filterOEEFailsRadioButton.UseVisualStyleBackColor = true;
            this.filterOEEFailsRadioButton.CheckedChanged += new System.EventHandler(this.FailureFilterRadioButton_CheckedChanged);
            // 
            // tableLayoutPanel34
            // 
            this.tableLayoutPanel34.ColumnCount = 2;
            this.tableLayoutPanel34.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel34.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel34.Controls.Add(this.label17, 0, 0);
            this.tableLayoutPanel34.Controls.Add(this.excelButton11, 1, 0);
            this.tableLayoutPanel34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel34.Location = new System.Drawing.Point(689, 40);
            this.tableLayoutPanel34.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel34.Name = "tableLayoutPanel34";
            this.tableLayoutPanel34.RowCount = 1;
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel34.Size = new System.Drawing.Size(350, 25);
            this.tableLayoutPanel34.TabIndex = 31;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label17.Location = new System.Drawing.Point(3, 12);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(169, 13);
            this.label17.TabIndex = 23;
            this.label17.Text = "Pass Rate";
            // 
            // tableLayoutPanel35
            // 
            this.tableLayoutPanel35.ColumnCount = 3;
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel35.Controls.Add(this.powerPointButton11, 1, 1);
            this.tableLayoutPanel35.Controls.Add(this.copyButton9, 2, 1);
            this.tableLayoutPanel35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel35.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel35.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel35.Name = "tableLayoutPanel35";
            this.tableLayoutPanel35.RowCount = 2;
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel35.Size = new System.Drawing.Size(689, 20);
            this.tableLayoutPanel35.TabIndex = 32;
            // 
            // rigAlignmentTabPage
            // 
            this.rigAlignmentTabPage.Controls.Add(this.tableLayoutPanel21);
            this.rigAlignmentTabPage.Location = new System.Drawing.Point(4, 22);
            this.rigAlignmentTabPage.Name = "rigAlignmentTabPage";
            this.rigAlignmentTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.rigAlignmentTabPage.Size = new System.Drawing.Size(1045, 601);
            this.rigAlignmentTabPage.TabIndex = 12;
            this.rigAlignmentTabPage.Text = "Rig alignment";
            this.rigAlignmentTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 2;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 225F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel21.Controls.Add(this.groupBox4, 0, 1);
            this.tableLayoutPanel21.Controls.Add(this.rigAlignmentTestTypeGroupBox, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.rigAlignmentParameterTreeView, 0, 5);
            this.tableLayoutPanel21.Controls.Add(this.label51, 0, 4);
            this.tableLayoutPanel21.Controls.Add(this.label52, 0, 2);
            this.tableLayoutPanel21.Controls.Add(this.rigAlignmentLineSelectionListView, 0, 3);
            this.tableLayoutPanel21.Controls.Add(this.rigAlignmentPanel, 1, 0);
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 6;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(1039, 595);
            this.tableLayoutPanel21.TabIndex = 22;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rigAlignmentChronologicalRadioButton);
            this.groupBox4.Controls.Add(this.rigAlignmentDateTimeRadioButton);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(3, 53);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(219, 44);
            this.groupBox4.TabIndex = 24;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Chart type";
            // 
            // rigAlignmentChronologicalRadioButton
            // 
            this.rigAlignmentChronologicalRadioButton.AutoSize = true;
            this.rigAlignmentChronologicalRadioButton.Location = new System.Drawing.Point(120, 20);
            this.rigAlignmentChronologicalRadioButton.Name = "rigAlignmentChronologicalRadioButton";
            this.rigAlignmentChronologicalRadioButton.Size = new System.Drawing.Size(89, 17);
            this.rigAlignmentChronologicalRadioButton.TabIndex = 1;
            this.rigAlignmentChronologicalRadioButton.Text = "Chronological";
            this.rigAlignmentChronologicalRadioButton.UseVisualStyleBackColor = true;
            // 
            // rigAlignmentDateTimeRadioButton
            // 
            this.rigAlignmentDateTimeRadioButton.AutoSize = true;
            this.rigAlignmentDateTimeRadioButton.Checked = true;
            this.rigAlignmentDateTimeRadioButton.Location = new System.Drawing.Point(7, 20);
            this.rigAlignmentDateTimeRadioButton.Name = "rigAlignmentDateTimeRadioButton";
            this.rigAlignmentDateTimeRadioButton.Size = new System.Drawing.Size(78, 17);
            this.rigAlignmentDateTimeRadioButton.TabIndex = 0;
            this.rigAlignmentDateTimeRadioButton.TabStop = true;
            this.rigAlignmentDateTimeRadioButton.Text = "Date / time";
            this.rigAlignmentDateTimeRadioButton.UseVisualStyleBackColor = true;
            this.rigAlignmentDateTimeRadioButton.CheckedChanged += new System.EventHandler(this.rigAlignmentChartType_CheckedChanged);
            // 
            // rigAlignmentTestTypeGroupBox
            // 
            this.rigAlignmentTestTypeGroupBox.Controls.Add(this.rigAlignmentPumpingRadioButton);
            this.rigAlignmentTestTypeGroupBox.Controls.Add(this.rigAlignmentCalibrationRadioButton);
            this.rigAlignmentTestTypeGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rigAlignmentTestTypeGroupBox.Location = new System.Drawing.Point(3, 3);
            this.rigAlignmentTestTypeGroupBox.Name = "rigAlignmentTestTypeGroupBox";
            this.rigAlignmentTestTypeGroupBox.Size = new System.Drawing.Size(219, 44);
            this.rigAlignmentTestTypeGroupBox.TabIndex = 20;
            this.rigAlignmentTestTypeGroupBox.TabStop = false;
            this.rigAlignmentTestTypeGroupBox.Text = "Test type";
            // 
            // rigAlignmentPumpingRadioButton
            // 
            this.rigAlignmentPumpingRadioButton.AutoSize = true;
            this.rigAlignmentPumpingRadioButton.Location = new System.Drawing.Point(120, 20);
            this.rigAlignmentPumpingRadioButton.Name = "rigAlignmentPumpingRadioButton";
            this.rigAlignmentPumpingRadioButton.Size = new System.Drawing.Size(66, 17);
            this.rigAlignmentPumpingRadioButton.TabIndex = 1;
            this.rigAlignmentPumpingRadioButton.Text = "Pumping";
            this.rigAlignmentPumpingRadioButton.UseVisualStyleBackColor = true;
            // 
            // rigAlignmentCalibrationRadioButton
            // 
            this.rigAlignmentCalibrationRadioButton.AutoSize = true;
            this.rigAlignmentCalibrationRadioButton.Checked = true;
            this.rigAlignmentCalibrationRadioButton.Location = new System.Drawing.Point(7, 20);
            this.rigAlignmentCalibrationRadioButton.Name = "rigAlignmentCalibrationRadioButton";
            this.rigAlignmentCalibrationRadioButton.Size = new System.Drawing.Size(74, 17);
            this.rigAlignmentCalibrationRadioButton.TabIndex = 0;
            this.rigAlignmentCalibrationRadioButton.TabStop = true;
            this.rigAlignmentCalibrationRadioButton.Text = "Calibration";
            this.rigAlignmentCalibrationRadioButton.UseVisualStyleBackColor = true;
            // 
            // rigAlignmentParameterTreeView
            // 
            this.rigAlignmentParameterTreeView.CheckBoxes = true;
            this.rigAlignmentParameterTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rigAlignmentParameterTreeView.Location = new System.Drawing.Point(3, 243);
            this.rigAlignmentParameterTreeView.Name = "rigAlignmentParameterTreeView";
            this.rigAlignmentParameterTreeView.Size = new System.Drawing.Size(219, 349);
            this.rigAlignmentParameterTreeView.TabIndex = 18;
            this.rigAlignmentParameterTreeView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.rigAlignmentParameterTreeView_AfterCheck);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label51.Location = new System.Drawing.Point(3, 227);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(219, 13);
            this.label51.TabIndex = 6;
            this.label51.Text = "Parameter selection";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label52.Location = new System.Drawing.Point(3, 107);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(219, 13);
            this.label52.TabIndex = 21;
            this.label52.Text = "Rig / line selection";
            // 
            // rigAlignmentLineSelectionListView
            // 
            this.rigAlignmentLineSelectionListView.CheckBoxes = true;
            this.rigAlignmentLineSelectionListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rigAlignmentLineSelectionListView.HideSelection = false;
            this.rigAlignmentLineSelectionListView.Location = new System.Drawing.Point(3, 123);
            this.rigAlignmentLineSelectionListView.Name = "rigAlignmentLineSelectionListView";
            this.rigAlignmentLineSelectionListView.ShowGroups = false;
            this.rigAlignmentLineSelectionListView.Size = new System.Drawing.Size(219, 94);
            this.rigAlignmentLineSelectionListView.TabIndex = 22;
            this.rigAlignmentLineSelectionListView.UseCompatibleStateImageBehavior = false;
            this.rigAlignmentLineSelectionListView.View = System.Windows.Forms.View.List;
            this.rigAlignmentLineSelectionListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.rigAlignmentLineSelection_ItemChecked);
            // 
            // rigAlignmentPanel
            // 
            this.rigAlignmentPanel.AutoScroll = true;
            this.rigAlignmentPanel.Controls.Add(this.RigAlignmentShowLegend);
            this.rigAlignmentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rigAlignmentPanel.Location = new System.Drawing.Point(228, 3);
            this.rigAlignmentPanel.Name = "rigAlignmentPanel";
            this.tableLayoutPanel21.SetRowSpan(this.rigAlignmentPanel, 6);
            this.rigAlignmentPanel.Size = new System.Drawing.Size(808, 589);
            this.rigAlignmentPanel.TabIndex = 25;
            // 
            // RigAlignmentShowLegend
            // 
            this.RigAlignmentShowLegend.AutoSize = true;
            this.RigAlignmentShowLegend.Location = new System.Drawing.Point(732, 3);
            this.RigAlignmentShowLegend.Name = "RigAlignmentShowLegend";
            this.RigAlignmentShowLegend.Size = new System.Drawing.Size(62, 17);
            this.RigAlignmentShowLegend.TabIndex = 0;
            this.RigAlignmentShowLegend.Text = "Legend";
            this.RigAlignmentShowLegend.UseVisualStyleBackColor = true;
            this.RigAlignmentShowLegend.Visible = false;
            this.RigAlignmentShowLegend.CheckedChanged += new System.EventHandler(this.RigAlignmentShowLegend_CheckedChanged);
            // 
            // chartTracesTabPage
            // 
            this.chartTracesTabPage.Controls.Add(this.tableLayoutPanel8);
            this.chartTracesTabPage.Location = new System.Drawing.Point(4, 22);
            this.chartTracesTabPage.Name = "chartTracesTabPage";
            this.chartTracesTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.chartTracesTabPage.Size = new System.Drawing.Size(1045, 601);
            this.chartTracesTabPage.TabIndex = 4;
            this.chartTracesTabPage.Text = "Chart traces";
            this.chartTracesTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 6;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel8.Controls.Add(this.tracePumpingutidsListView, 0, 6);
            this.tableLayoutPanel8.Controls.Add(this.getTraceDataButton, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.traceGroupDataByGroupBox, 0, 8);
            this.tableLayoutPanel8.Controls.Add(this.chartTraceChart, 1, 4);
            this.tableLayoutPanel8.Controls.Add(this.updateChartTraceChartButton, 1, 3);
            this.tableLayoutPanel8.Controls.Add(this.label18, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.traceOMVTestPointListView, 5, 1);
            this.tableLayoutPanel8.Controls.Add(this.label23, 5, 0);
            this.tableLayoutPanel8.Controls.Add(this.traceFODTestPointListView, 4, 1);
            this.tableLayoutPanel8.Controls.Add(this.label21, 2, 0);
            this.tableLayoutPanel8.Controls.Add(this.traceCLTTestPointListView, 2, 1);
            this.tableLayoutPanel8.Controls.Add(this.label22, 4, 0);
            this.tableLayoutPanel8.Controls.Add(this.traceCalibrationutidsListView, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.traceNCVTestPointListView, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.label48, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label49, 0, 5);
            this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel33, 5, 3);
            this.tableLayoutPanel8.Controls.Add(this.AutoUpdateTraceChartsCheckBox, 0, 7);
            this.tableLayoutPanel8.Controls.Add(this.label63, 3, 0);
            this.tableLayoutPanel8.Controls.Add(this.traceCLSTestPointListView, 3, 1);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 9;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 145F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(1039, 595);
            this.tableLayoutPanel8.TabIndex = 33;
            // 
            // tracePumpingutidsListView
            // 
            this.tracePumpingutidsListView.CheckBoxes = true;
            this.tracePumpingutidsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4,
            this.columnHeader8,
            this.columnHeader5,
            this.columnHeader6});
            this.tracePumpingutidsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tracePumpingutidsListView.HideSelection = false;
            this.tracePumpingutidsListView.Location = new System.Drawing.Point(3, 265);
            this.tracePumpingutidsListView.Name = "tracePumpingutidsListView";
            this.tracePumpingutidsListView.Size = new System.Drawing.Size(214, 156);
            this.tracePumpingutidsListView.TabIndex = 33;
            this.tracePumpingutidsListView.UseCompatibleStateImageBehavior = false;
            this.tracePumpingutidsListView.View = System.Windows.Forms.View.Details;
            this.tracePumpingutidsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.TraceUTIDListView_CheckedChanged);
            this.tracePumpingutidsListView.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ListView_CtrlAHandler);
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "UTID";
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "PN";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "ISN";
            this.columnHeader5.Width = 69;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "PDate";
            this.columnHeader6.Width = 109;
            // 
            // getTraceDataButton
            // 
            this.getTraceDataButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.getTraceDataButton.Location = new System.Drawing.Point(3, 3);
            this.getTraceDataButton.Name = "getTraceDataButton";
            this.getTraceDataButton.Size = new System.Drawing.Size(214, 24);
            this.getTraceDataButton.TabIndex = 4;
            this.getTraceDataButton.Text = "Get trace data";
            this.getTraceDataButton.UseVisualStyleBackColor = true;
            this.getTraceDataButton.Click += new System.EventHandler(this.getTraceDataButton_Click);
            // 
            // traceGroupDataByGroupBox
            // 
            this.traceGroupDataByGroupBox.Controls.Add(this.traceLineNumberRadioButton);
            this.traceGroupDataByGroupBox.Controls.Add(this.traceISNRadioButton);
            this.traceGroupDataByGroupBox.Controls.Add(this.traceTestplanRadioButton);
            this.traceGroupDataByGroupBox.Controls.Add(this.tracePassFailRadioButton);
            this.traceGroupDataByGroupBox.Controls.Add(this.traceIDENTRadioButton);
            this.traceGroupDataByGroupBox.Controls.Add(this.traceLOPRadioButton);
            this.traceGroupDataByGroupBox.Controls.Add(this.traceTestPointRadioButton);
            this.traceGroupDataByGroupBox.Controls.Add(this.traceUTIDRadioButton);
            this.traceGroupDataByGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.traceGroupDataByGroupBox.Location = new System.Drawing.Point(3, 452);
            this.traceGroupDataByGroupBox.Name = "traceGroupDataByGroupBox";
            this.traceGroupDataByGroupBox.Size = new System.Drawing.Size(214, 140);
            this.traceGroupDataByGroupBox.TabIndex = 24;
            this.traceGroupDataByGroupBox.TabStop = false;
            this.traceGroupDataByGroupBox.Text = "Group data by:";
            // 
            // traceLineNumberRadioButton
            // 
            this.traceLineNumberRadioButton.AutoSize = true;
            this.traceLineNumberRadioButton.Location = new System.Drawing.Point(134, 65);
            this.traceLineNumberRadioButton.Name = "traceLineNumberRadioButton";
            this.traceLineNumberRadioButton.Size = new System.Drawing.Size(83, 17);
            this.traceLineNumberRadioButton.TabIndex = 7;
            this.traceLineNumberRadioButton.Text = "Line number";
            this.traceLineNumberRadioButton.UseVisualStyleBackColor = true;
            // 
            // traceISNRadioButton
            // 
            this.traceISNRadioButton.AutoSize = true;
            this.traceISNRadioButton.Location = new System.Drawing.Point(6, 65);
            this.traceISNRadioButton.Name = "traceISNRadioButton";
            this.traceISNRadioButton.Size = new System.Drawing.Size(43, 17);
            this.traceISNRadioButton.TabIndex = 6;
            this.traceISNRadioButton.Text = "ISN";
            this.traceISNRadioButton.UseVisualStyleBackColor = true;
            // 
            // traceTestplanRadioButton
            // 
            this.traceTestplanRadioButton.AutoSize = true;
            this.traceTestplanRadioButton.Location = new System.Drawing.Point(6, 88);
            this.traceTestplanRadioButton.Name = "traceTestplanRadioButton";
            this.traceTestplanRadioButton.Size = new System.Drawing.Size(66, 17);
            this.traceTestplanRadioButton.TabIndex = 5;
            this.traceTestplanRadioButton.Text = "Testplan";
            this.traceTestplanRadioButton.UseVisualStyleBackColor = true;
            // 
            // tracePassFailRadioButton
            // 
            this.tracePassFailRadioButton.AutoSize = true;
            this.tracePassFailRadioButton.Location = new System.Drawing.Point(6, 19);
            this.tracePassFailRadioButton.Name = "tracePassFailRadioButton";
            this.tracePassFailRadioButton.Size = new System.Drawing.Size(69, 17);
            this.tracePassFailRadioButton.TabIndex = 4;
            this.tracePassFailRadioButton.Text = "Pass/Fail";
            this.tracePassFailRadioButton.UseVisualStyleBackColor = true;
            // 
            // traceIDENTRadioButton
            // 
            this.traceIDENTRadioButton.AutoSize = true;
            this.traceIDENTRadioButton.Location = new System.Drawing.Point(134, 19);
            this.traceIDENTRadioButton.Name = "traceIDENTRadioButton";
            this.traceIDENTRadioButton.Size = new System.Drawing.Size(58, 17);
            this.traceIDENTRadioButton.TabIndex = 3;
            this.traceIDENTRadioButton.Text = "IDENT";
            this.traceIDENTRadioButton.UseVisualStyleBackColor = true;
            // 
            // traceLOPRadioButton
            // 
            this.traceLOPRadioButton.AutoSize = true;
            this.traceLOPRadioButton.Location = new System.Drawing.Point(6, 42);
            this.traceLOPRadioButton.Name = "traceLOPRadioButton";
            this.traceLOPRadioButton.Size = new System.Drawing.Size(46, 17);
            this.traceLOPRadioButton.TabIndex = 2;
            this.traceLOPRadioButton.Text = "LOP";
            this.traceLOPRadioButton.UseVisualStyleBackColor = true;
            // 
            // traceTestPointRadioButton
            // 
            this.traceTestPointRadioButton.AutoSize = true;
            this.traceTestPointRadioButton.Location = new System.Drawing.Point(134, 42);
            this.traceTestPointRadioButton.Name = "traceTestPointRadioButton";
            this.traceTestPointRadioButton.Size = new System.Drawing.Size(72, 17);
            this.traceTestPointRadioButton.TabIndex = 1;
            this.traceTestPointRadioButton.Text = "Test point";
            this.traceTestPointRadioButton.UseVisualStyleBackColor = true;
            // 
            // traceUTIDRadioButton
            // 
            this.traceUTIDRadioButton.AutoSize = true;
            this.traceUTIDRadioButton.Checked = true;
            this.traceUTIDRadioButton.Location = new System.Drawing.Point(6, 111);
            this.traceUTIDRadioButton.Name = "traceUTIDRadioButton";
            this.traceUTIDRadioButton.Size = new System.Drawing.Size(94, 17);
            this.traceUTIDRadioButton.TabIndex = 0;
            this.traceUTIDRadioButton.TabStop = true;
            this.traceUTIDRadioButton.Text = "UTID (Default)";
            this.traceUTIDRadioButton.UseVisualStyleBackColor = true;
            // 
            // chartTraceChart
            // 
            chartArea21.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea21.AxisX.MinorGrid.Enabled = true;
            chartArea21.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea21.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea21.AxisX.Title = "Duration (µs)";
            chartArea21.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea21.AxisY.MinorGrid.Enabled = true;
            chartArea21.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea21.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea21.AxisY.Title = "Bits";
            chartArea21.AxisY2.MajorGrid.Enabled = false;
            chartArea21.Name = "ChartArea1";
            this.chartTraceChart.ChartAreas.Add(chartArea21);
            this.tableLayoutPanel8.SetColumnSpan(this.chartTraceChart, 5);
            this.chartTraceChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend15.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Column;
            legend15.Name = "legendLineSeries";
            legend15.TextWrapThreshold = 50;
            legend16.Alignment = System.Drawing.StringAlignment.Center;
            legend16.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend16.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Row;
            legend16.Name = "legendPointSeries";
            this.chartTraceChart.Legends.Add(legend15);
            this.chartTraceChart.Legends.Add(legend16);
            this.chartTraceChart.Location = new System.Drawing.Point(223, 180);
            this.chartTraceChart.Name = "chartTraceChart";
            this.chartTraceChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.tableLayoutPanel8.SetRowSpan(this.chartTraceChart, 5);
            this.chartTraceChart.Size = new System.Drawing.Size(813, 412);
            this.chartTraceChart.TabIndex = 2;
            this.chartTraceChart.Text = "chart10";
            title20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            title20.Name = "chartTraceChartTitle";
            title20.Text = "Chart Trace";
            this.chartTraceChart.Titles.Add(title20);
            this.chartTraceChart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // updateChartTraceChartButton
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.updateChartTraceChartButton, 4);
            this.updateChartTraceChartButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updateChartTraceChartButton.Location = new System.Drawing.Point(223, 150);
            this.updateChartTraceChartButton.Name = "updateChartTraceChartButton";
            this.updateChartTraceChartButton.Size = new System.Drawing.Size(646, 24);
            this.updateChartTraceChartButton.TabIndex = 3;
            this.updateChartTraceChartButton.Text = "Update chart";
            this.updateChartTraceChartButton.UseVisualStyleBackColor = true;
            this.updateChartTraceChartButton.Click += new System.EventHandler(this.chartTraceUpdateChartButton_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label18.Location = new System.Drawing.Point(223, 17);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(157, 13);
            this.label18.TabIndex = 26;
            this.label18.Text = "Open loop";
            // 
            // traceOMVTestPointListView
            // 
            this.traceOMVTestPointListView.CheckBoxes = true;
            this.traceOMVTestPointListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.traceOMVTestPointListView.HideSelection = false;
            this.traceOMVTestPointListView.Location = new System.Drawing.Point(875, 33);
            this.traceOMVTestPointListView.Name = "traceOMVTestPointListView";
            this.tableLayoutPanel8.SetRowSpan(this.traceOMVTestPointListView, 2);
            this.traceOMVTestPointListView.Size = new System.Drawing.Size(161, 111);
            this.traceOMVTestPointListView.TabIndex = 30;
            this.traceOMVTestPointListView.UseCompatibleStateImageBehavior = false;
            this.traceOMVTestPointListView.View = System.Windows.Forms.View.List;
            this.traceOMVTestPointListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.TraceOMVListView_CheckedChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label23.Location = new System.Drawing.Point(875, 17);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(161, 13);
            this.label23.TabIndex = 31;
            this.label23.Text = "OMV";
            // 
            // traceFODTestPointListView
            // 
            this.traceFODTestPointListView.CheckBoxes = true;
            this.traceFODTestPointListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.traceFODTestPointListView.HideSelection = false;
            this.traceFODTestPointListView.Location = new System.Drawing.Point(712, 33);
            this.traceFODTestPointListView.Name = "traceFODTestPointListView";
            this.tableLayoutPanel8.SetRowSpan(this.traceFODTestPointListView, 2);
            this.traceFODTestPointListView.Size = new System.Drawing.Size(157, 111);
            this.traceFODTestPointListView.TabIndex = 28;
            this.traceFODTestPointListView.UseCompatibleStateImageBehavior = false;
            this.traceFODTestPointListView.View = System.Windows.Forms.View.List;
            this.traceFODTestPointListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.TraceFODListView_CheckedChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label21.Location = new System.Drawing.Point(386, 17);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(157, 13);
            this.label21.TabIndex = 27;
            this.label21.Text = "Closed loop";
            // 
            // traceCLTTestPointListView
            // 
            this.traceCLTTestPointListView.CheckBoxes = true;
            this.traceCLTTestPointListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.traceCLTTestPointListView.HideSelection = false;
            this.traceCLTTestPointListView.Location = new System.Drawing.Point(386, 33);
            this.traceCLTTestPointListView.Name = "traceCLTTestPointListView";
            this.tableLayoutPanel8.SetRowSpan(this.traceCLTTestPointListView, 2);
            this.traceCLTTestPointListView.Size = new System.Drawing.Size(157, 111);
            this.traceCLTTestPointListView.TabIndex = 25;
            this.traceCLTTestPointListView.UseCompatibleStateImageBehavior = false;
            this.traceCLTTestPointListView.View = System.Windows.Forms.View.List;
            this.traceCLTTestPointListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.TraceClosedLoopListView_CheckedChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label22.Location = new System.Drawing.Point(712, 17);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(157, 13);
            this.label22.TabIndex = 29;
            this.label22.Text = "Fixed OMV Demand";
            // 
            // traceCalibrationutidsListView
            // 
            this.traceCalibrationutidsListView.CheckBoxes = true;
            this.traceCalibrationutidsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader7,
            this.columnHeader2,
            this.columnHeader3});
            this.traceCalibrationutidsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.traceCalibrationutidsListView.HideSelection = false;
            this.traceCalibrationutidsListView.Location = new System.Drawing.Point(3, 53);
            this.traceCalibrationutidsListView.Name = "traceCalibrationutidsListView";
            this.tableLayoutPanel8.SetRowSpan(this.traceCalibrationutidsListView, 3);
            this.traceCalibrationutidsListView.Size = new System.Drawing.Size(214, 186);
            this.traceCalibrationutidsListView.TabIndex = 14;
            this.traceCalibrationutidsListView.UseCompatibleStateImageBehavior = false;
            this.traceCalibrationutidsListView.View = System.Windows.Forms.View.Details;
            this.traceCalibrationutidsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.TraceUTIDListView_CheckedChanged);
            this.traceCalibrationutidsListView.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ListView_CtrlAHandler);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "UTID";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "PN";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "ISN";
            this.columnHeader2.Width = 69;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "PDate";
            this.columnHeader3.Width = 109;
            // 
            // traceNCVTestPointListView
            // 
            this.traceNCVTestPointListView.CheckBoxes = true;
            this.traceNCVTestPointListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.traceNCVTestPointListView.HideSelection = false;
            this.traceNCVTestPointListView.Location = new System.Drawing.Point(223, 33);
            this.traceNCVTestPointListView.Name = "traceNCVTestPointListView";
            this.tableLayoutPanel8.SetRowSpan(this.traceNCVTestPointListView, 2);
            this.traceNCVTestPointListView.Size = new System.Drawing.Size(157, 111);
            this.traceNCVTestPointListView.TabIndex = 15;
            this.traceNCVTestPointListView.UseCompatibleStateImageBehavior = false;
            this.traceNCVTestPointListView.View = System.Windows.Forms.View.List;
            this.traceNCVTestPointListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.TraceOpenLoopListView_CheckedChanged);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label48.Location = new System.Drawing.Point(3, 37);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(214, 13);
            this.label48.TabIndex = 34;
            this.label48.Text = "Calibration tests";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label49.Location = new System.Drawing.Point(3, 249);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(214, 13);
            this.label49.TabIndex = 35;
            this.label49.Text = "Pumping tests";
            // 
            // tableLayoutPanel33
            // 
            this.tableLayoutPanel33.ColumnCount = 3;
            this.tableLayoutPanel33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 144F));
            this.tableLayoutPanel33.Controls.Add(this.copyButton1, 2, 0);
            this.tableLayoutPanel33.Controls.Add(this.excelButton1, 1, 0);
            this.tableLayoutPanel33.Controls.Add(this.powerPointButton1, 0, 0);
            this.tableLayoutPanel33.Location = new System.Drawing.Point(875, 150);
            this.tableLayoutPanel33.Name = "tableLayoutPanel33";
            this.tableLayoutPanel33.RowCount = 1;
            this.tableLayoutPanel33.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel33.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel33.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel33.Size = new System.Drawing.Size(126, 24);
            this.tableLayoutPanel33.TabIndex = 36;
            // 
            // AutoUpdateTraceChartsCheckBox
            // 
            this.AutoUpdateTraceChartsCheckBox.AutoSize = true;
            this.AutoUpdateTraceChartsCheckBox.Location = new System.Drawing.Point(3, 427);
            this.AutoUpdateTraceChartsCheckBox.Name = "AutoUpdateTraceChartsCheckBox";
            this.AutoUpdateTraceChartsCheckBox.Size = new System.Drawing.Size(119, 17);
            this.AutoUpdateTraceChartsCheckBox.TabIndex = 37;
            this.AutoUpdateTraceChartsCheckBox.Text = "Auto Update Charts";
            this.AutoUpdateTraceChartsCheckBox.UseVisualStyleBackColor = true;
            this.AutoUpdateTraceChartsCheckBox.CheckedChanged += new System.EventHandler(this.AutoUpdateTraceChartsCheckBox_CheckedChanged);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label63.Location = new System.Drawing.Point(549, 17);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(157, 13);
            this.label63.TabIndex = 38;
            this.label63.Text = "CL Stabilise";
            // 
            // traceCLSTestPointListView
            // 
            this.traceCLSTestPointListView.CheckBoxes = true;
            this.traceCLSTestPointListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.traceCLSTestPointListView.HideSelection = false;
            this.traceCLSTestPointListView.Location = new System.Drawing.Point(549, 33);
            this.traceCLSTestPointListView.Name = "traceCLSTestPointListView";
            this.tableLayoutPanel8.SetRowSpan(this.traceCLSTestPointListView, 2);
            this.traceCLSTestPointListView.Size = new System.Drawing.Size(157, 111);
            this.traceCLSTestPointListView.TabIndex = 39;
            this.traceCLSTestPointListView.UseCompatibleStateImageBehavior = false;
            this.traceCLSTestPointListView.View = System.Windows.Forms.View.List;
            // 
            // detailedDataTabPage
            // 
            this.detailedDataTabPage.Controls.Add(this.tableLayoutPanel9);
            this.detailedDataTabPage.Location = new System.Drawing.Point(4, 22);
            this.detailedDataTabPage.Name = "detailedDataTabPage";
            this.detailedDataTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.detailedDataTabPage.Size = new System.Drawing.Size(1045, 601);
            this.detailedDataTabPage.TabIndex = 5;
            this.detailedDataTabPage.Text = "Detailed data";
            this.detailedDataTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.AutoSize = true;
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 245F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.returnNominalDataCheckBox, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.detailedDataTabControl, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.groupDataByGroupBox, 0, 4);
            this.tableLayoutPanel9.Controls.Add(this.detailedDataUpdateButton, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.DetailedUTIDsListView, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.AutoUpdateDetailedChartsCheckBox, 0, 3);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 5;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(1039, 595);
            this.tableLayoutPanel9.TabIndex = 29;
            // 
            // returnNominalDataCheckBox
            // 
            this.returnNominalDataCheckBox.AutoSize = true;
            this.returnNominalDataCheckBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.returnNominalDataCheckBox.Location = new System.Drawing.Point(3, 5);
            this.returnNominalDataCheckBox.Name = "returnNominalDataCheckBox";
            this.returnNominalDataCheckBox.Size = new System.Drawing.Size(239, 17);
            this.returnNominalDataCheckBox.TabIndex = 28;
            this.returnNominalDataCheckBox.Text = "Return nominal data (takes longer)";
            this.returnNominalDataCheckBox.UseVisualStyleBackColor = true;
            // 
            // detailedDataTabControl
            // 
            this.detailedDataTabControl.Controls.Add(this.olTabPage);
            this.detailedDataTabControl.Controls.Add(this.clTabPage);
            this.detailedDataTabControl.Controls.Add(this.clLogPlotTabPage);
            this.detailedDataTabControl.Controls.Add(this.olStabiliseTabPage);
            this.detailedDataTabControl.Controls.Add(this.clStabiliseTabPage);
            this.detailedDataTabControl.Controls.Add(this.fodTabPage);
            this.detailedDataTabControl.Controls.Add(this.omvTabPage);
            this.detailedDataTabControl.Controls.Add(this.leakdownTabPage);
            this.detailedDataTabControl.Controls.Add(this.trimmedTabPage);
            this.detailedDataTabControl.Controls.Add(this.trimmedLogTabPage);
            this.detailedDataTabControl.Controls.Add(this.untrimmedTabPage);
            this.detailedDataTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailedDataTabControl.Location = new System.Drawing.Point(248, 3);
            this.detailedDataTabControl.Name = "detailedDataTabControl";
            this.tableLayoutPanel9.SetRowSpan(this.detailedDataTabControl, 5);
            this.detailedDataTabControl.SelectedIndex = 0;
            this.detailedDataTabControl.Size = new System.Drawing.Size(788, 589);
            this.detailedDataTabControl.TabIndex = 19;
            this.detailedDataTabControl.Tag = "";
            // 
            // olTabPage
            // 
            this.olTabPage.Controls.Add(this.tableLayoutPanel10);
            this.olTabPage.Location = new System.Drawing.Point(4, 22);
            this.olTabPage.Name = "olTabPage";
            this.olTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.olTabPage.Size = new System.Drawing.Size(780, 563);
            this.olTabPage.TabIndex = 0;
            this.olTabPage.Tag = "olChart";
            this.olTabPage.Text = "Open Loop";
            this.olTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 3;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 107F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this.label26, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.olNominalChartedDataGridView, 0, 7);
            this.tableLayoutPanel10.Controls.Add(this.olPressureLevelsListView, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.showOLNominalCheckBox, 0, 6);
            this.tableLayoutPanel10.Controls.Add(this.olUpdateChartButton, 0, 8);
            this.tableLayoutPanel10.Controls.Add(this.label34, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.label39, 0, 4);
            this.tableLayoutPanel10.Controls.Add(this.olParametersListView, 0, 3);
            this.tableLayoutPanel10.Controls.Add(this.olStatisticsListView, 0, 5);
            this.tableLayoutPanel10.Controls.Add(this.tableLayoutPanel24, 2, 0);
            this.tableLayoutPanel10.Controls.Add(this.excelButton15, 1, 6);
            this.tableLayoutPanel10.Controls.Add(this.olSplitContainer, 2, 1);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 9;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(774, 557);
            this.tableLayoutPanel10.TabIndex = 25;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label26.Location = new System.Drawing.Point(3, 10);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(101, 13);
            this.label26.TabIndex = 19;
            this.label26.Text = "Pressure Level(s)";
            // 
            // olNominalChartedDataGridView
            // 
            this.olNominalChartedDataGridView.AllowUserToAddRows = false;
            this.olNominalChartedDataGridView.AllowUserToDeleteRows = false;
            this.olNominalChartedDataGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.olNominalChartedDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.olNominalChartedDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel10.SetColumnSpan(this.olNominalChartedDataGridView, 2);
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.olNominalChartedDataGridView.DefaultCellStyle = dataGridViewCellStyle12;
            this.olNominalChartedDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olNominalChartedDataGridView.Location = new System.Drawing.Point(3, 462);
            this.olNominalChartedDataGridView.Name = "olNominalChartedDataGridView";
            this.olNominalChartedDataGridView.ReadOnly = true;
            this.olNominalChartedDataGridView.Size = new System.Drawing.Size(124, 60);
            this.olNominalChartedDataGridView.TabIndex = 24;
            // 
            // olPressureLevelsListView
            // 
            this.olPressureLevelsListView.CheckBoxes = true;
            this.tableLayoutPanel10.SetColumnSpan(this.olPressureLevelsListView, 2);
            this.olPressureLevelsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olPressureLevelsListView.HideSelection = false;
            this.olPressureLevelsListView.Location = new System.Drawing.Point(3, 26);
            this.olPressureLevelsListView.Name = "olPressureLevelsListView";
            this.olPressureLevelsListView.Size = new System.Drawing.Size(124, 127);
            this.olPressureLevelsListView.TabIndex = 14;
            this.olPressureLevelsListView.Tag = "olChart";
            this.olPressureLevelsListView.UseCompatibleStateImageBehavior = false;
            this.olPressureLevelsListView.View = System.Windows.Forms.View.List;
            this.olPressureLevelsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.PressureLevelsListView_CheckedChanged);
            // 
            // showOLNominalCheckBox
            // 
            this.showOLNominalCheckBox.AutoSize = true;
            this.showOLNominalCheckBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.showOLNominalCheckBox.Enabled = false;
            this.showOLNominalCheckBox.Location = new System.Drawing.Point(3, 437);
            this.showOLNominalCheckBox.Name = "showOLNominalCheckBox";
            this.showOLNominalCheckBox.Size = new System.Drawing.Size(101, 17);
            this.showOLNominalCheckBox.TabIndex = 23;
            this.showOLNominalCheckBox.Text = "Show nominal";
            this.showOLNominalCheckBox.UseVisualStyleBackColor = true;
            // 
            // olUpdateChartButton
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.olUpdateChartButton, 2);
            this.olUpdateChartButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olUpdateChartButton.Location = new System.Drawing.Point(3, 528);
            this.olUpdateChartButton.Name = "olUpdateChartButton";
            this.olUpdateChartButton.Size = new System.Drawing.Size(124, 26);
            this.olUpdateChartButton.TabIndex = 16;
            this.olUpdateChartButton.Text = "Update chart";
            this.olUpdateChartButton.UseVisualStyleBackColor = true;
            this.olUpdateChartButton.Click += new System.EventHandler(this.olUpdateChartButton_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label34.Location = new System.Drawing.Point(3, 163);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(101, 13);
            this.label34.TabIndex = 20;
            this.label34.Text = "Parameter(s)";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label39.Location = new System.Drawing.Point(3, 316);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(101, 13);
            this.label39.TabIndex = 22;
            this.label39.Text = "Statisitic(s)";
            // 
            // olParametersListView
            // 
            this.olParametersListView.CheckBoxes = true;
            this.tableLayoutPanel10.SetColumnSpan(this.olParametersListView, 2);
            this.olParametersListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olParametersListView.HideSelection = false;
            this.olParametersListView.Location = new System.Drawing.Point(3, 179);
            this.olParametersListView.Name = "olParametersListView";
            this.olParametersListView.Size = new System.Drawing.Size(124, 127);
            this.olParametersListView.TabIndex = 15;
            this.olParametersListView.Tag = "olChart";
            this.olParametersListView.UseCompatibleStateImageBehavior = false;
            this.olParametersListView.View = System.Windows.Forms.View.List;
            this.olParametersListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.ParametersListView_CheckChanged);
            // 
            // olStatisticsListView
            // 
            this.olStatisticsListView.CheckBoxes = true;
            this.tableLayoutPanel10.SetColumnSpan(this.olStatisticsListView, 2);
            this.olStatisticsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olStatisticsListView.HideSelection = false;
            listViewItem1.StateImageIndex = 0;
            listViewItem2.StateImageIndex = 0;
            listViewItem3.StateImageIndex = 0;
            listViewItem4.StateImageIndex = 0;
            listViewItem5.StateImageIndex = 0;
            this.olStatisticsListView.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5});
            this.olStatisticsListView.Location = new System.Drawing.Point(3, 332);
            this.olStatisticsListView.Name = "olStatisticsListView";
            this.olStatisticsListView.Scrollable = false;
            this.olStatisticsListView.Size = new System.Drawing.Size(124, 99);
            this.olStatisticsListView.TabIndex = 21;
            this.olStatisticsListView.Tag = "olChart";
            this.olStatisticsListView.UseCompatibleStateImageBehavior = false;
            this.olStatisticsListView.View = System.Windows.Forms.View.List;
            this.olStatisticsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.StatisticsListView_CheckChanged);
            // 
            // tableLayoutPanel24
            // 
            this.tableLayoutPanel24.ColumnCount = 6;
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel24.Controls.Add(this.copyButton11, 0, 0);
            this.tableLayoutPanel24.Controls.Add(this.olChartLegendCheckBox, 5, 0);
            this.tableLayoutPanel24.Controls.Add(this.excelButton2, 1, 0);
            this.tableLayoutPanel24.Controls.Add(this.powerPointButton2, 0, 0);
            this.tableLayoutPanel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel24.Location = new System.Drawing.Point(130, 0);
            this.tableLayoutPanel24.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel24.Name = "tableLayoutPanel24";
            this.tableLayoutPanel24.RowCount = 1;
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel24.Size = new System.Drawing.Size(644, 23);
            this.tableLayoutPanel24.TabIndex = 25;
            // 
            // olChartLegendCheckBox
            // 
            this.olChartLegendCheckBox.AutoSize = true;
            this.olChartLegendCheckBox.Checked = true;
            this.olChartLegendCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.olChartLegendCheckBox.Location = new System.Drawing.Point(577, 3);
            this.olChartLegendCheckBox.Name = "olChartLegendCheckBox";
            this.olChartLegendCheckBox.Size = new System.Drawing.Size(62, 17);
            this.olChartLegendCheckBox.TabIndex = 5;
            this.olChartLegendCheckBox.Tag = "olChart";
            this.olChartLegendCheckBox.Text = "Legend";
            this.olChartLegendCheckBox.UseVisualStyleBackColor = true;
            this.olChartLegendCheckBox.CheckedChanged += new System.EventHandler(this.DetailedChartLegendCheckBox_CheckedChanged);
            // 
            // olSplitContainer
            // 
            this.olSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olSplitContainer.Location = new System.Drawing.Point(133, 26);
            this.olSplitContainer.Name = "olSplitContainer";
            this.olSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // olSplitContainer.Panel1
            // 
            this.olSplitContainer.Panel1.Controls.Add(this.olChart);
            // 
            // olSplitContainer.Panel2
            // 
            this.olSplitContainer.Panel2.Controls.Add(this.olChartedDataGridView);
            this.olSplitContainer.Panel2MinSize = 0;
            this.tableLayoutPanel10.SetRowSpan(this.olSplitContainer, 8);
            this.olSplitContainer.Size = new System.Drawing.Size(638, 528);
            this.olSplitContainer.SplitterDistance = 427;
            this.olSplitContainer.TabIndex = 27;
            // 
            // olChartedDataGridView
            // 
            this.olChartedDataGridView.AllowUserToAddRows = false;
            this.olChartedDataGridView.AllowUserToDeleteRows = false;
            this.olChartedDataGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            this.olChartedDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.olChartedDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.olChartedDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.olChartedDataGridView.DefaultCellStyle = dataGridViewCellStyle14;
            this.olChartedDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olChartedDataGridView.Location = new System.Drawing.Point(0, 0);
            this.olChartedDataGridView.Name = "olChartedDataGridView";
            this.olChartedDataGridView.ReadOnly = true;
            this.olChartedDataGridView.Size = new System.Drawing.Size(638, 97);
            this.olChartedDataGridView.TabIndex = 17;
            // 
            // clTabPage
            // 
            this.clTabPage.Controls.Add(this.tableLayoutPanel11);
            this.clTabPage.Location = new System.Drawing.Point(4, 22);
            this.clTabPage.Name = "clTabPage";
            this.clTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.clTabPage.Size = new System.Drawing.Size(780, 563);
            this.clTabPage.TabIndex = 1;
            this.clTabPage.Tag = "clChart";
            this.clTabPage.Text = "Closed Loop";
            this.clTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel26, 1, 0);
            this.tableLayoutPanel11.Controls.Add(this.label36, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.clStatisticsListView, 0, 5);
            this.tableLayoutPanel11.Controls.Add(this.clUpdateChartButton, 0, 8);
            this.tableLayoutPanel11.Controls.Add(this.label40, 0, 4);
            this.tableLayoutPanel11.Controls.Add(this.clPressureLevelsListView, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.label35, 0, 2);
            this.tableLayoutPanel11.Controls.Add(this.clParametersListView, 0, 3);
            this.tableLayoutPanel11.Controls.Add(this.clSplitContainer, 1, 1);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 9;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(774, 557);
            this.tableLayoutPanel11.TabIndex = 31;
            // 
            // tableLayoutPanel26
            // 
            this.tableLayoutPanel26.ColumnCount = 6;
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel26.Controls.Add(this.clChartLegendCheckBox, 5, 0);
            this.tableLayoutPanel26.Controls.Add(this.copyButton3, 2, 0);
            this.tableLayoutPanel26.Controls.Add(this.excelButton3, 1, 0);
            this.tableLayoutPanel26.Controls.Add(this.powerPointButton3, 0, 0);
            this.tableLayoutPanel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel26.Location = new System.Drawing.Point(130, 0);
            this.tableLayoutPanel26.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel26.Name = "tableLayoutPanel26";
            this.tableLayoutPanel26.RowCount = 1;
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel26.Size = new System.Drawing.Size(644, 23);
            this.tableLayoutPanel26.TabIndex = 31;
            // 
            // clChartLegendCheckBox
            // 
            this.clChartLegendCheckBox.AutoSize = true;
            this.clChartLegendCheckBox.Checked = true;
            this.clChartLegendCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.clChartLegendCheckBox.Location = new System.Drawing.Point(577, 3);
            this.clChartLegendCheckBox.Name = "clChartLegendCheckBox";
            this.clChartLegendCheckBox.Size = new System.Drawing.Size(62, 17);
            this.clChartLegendCheckBox.TabIndex = 5;
            this.clChartLegendCheckBox.Tag = "clChart";
            this.clChartLegendCheckBox.Text = "Legend";
            this.clChartLegendCheckBox.UseVisualStyleBackColor = true;
            this.clChartLegendCheckBox.CheckedChanged += new System.EventHandler(this.DetailedChartLegendCheckBox_CheckedChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label36.Location = new System.Drawing.Point(3, 10);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(124, 13);
            this.label36.TabIndex = 28;
            this.label36.Text = "Pressure Level(s)";
            // 
            // clStatisticsListView
            // 
            this.clStatisticsListView.CheckBoxes = true;
            this.clStatisticsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clStatisticsListView.HideSelection = false;
            listViewItem6.StateImageIndex = 0;
            listViewItem7.StateImageIndex = 0;
            listViewItem8.StateImageIndex = 0;
            listViewItem9.StateImageIndex = 0;
            listViewItem10.StateImageIndex = 0;
            this.clStatisticsListView.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem6,
            listViewItem7,
            listViewItem8,
            listViewItem9,
            listViewItem10});
            this.clStatisticsListView.Location = new System.Drawing.Point(3, 332);
            this.clStatisticsListView.Name = "clStatisticsListView";
            this.clStatisticsListView.Scrollable = false;
            this.clStatisticsListView.Size = new System.Drawing.Size(124, 99);
            this.clStatisticsListView.TabIndex = 29;
            this.clStatisticsListView.Tag = "clChart";
            this.clStatisticsListView.UseCompatibleStateImageBehavior = false;
            this.clStatisticsListView.View = System.Windows.Forms.View.List;
            this.clStatisticsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.StatisticsListView_CheckChanged);
            // 
            // clUpdateChartButton
            // 
            this.clUpdateChartButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clUpdateChartButton.Location = new System.Drawing.Point(3, 528);
            this.clUpdateChartButton.Name = "clUpdateChartButton";
            this.clUpdateChartButton.Size = new System.Drawing.Size(124, 26);
            this.clUpdateChartButton.TabIndex = 23;
            this.clUpdateChartButton.Text = "Update chart";
            this.clUpdateChartButton.UseVisualStyleBackColor = true;
            this.clUpdateChartButton.Click += new System.EventHandler(this.clUpdateChartButton_Click);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label40.Location = new System.Drawing.Point(3, 316);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(124, 13);
            this.label40.TabIndex = 30;
            this.label40.Text = "Statisitic(s)";
            // 
            // clPressureLevelsListView
            // 
            this.clPressureLevelsListView.CheckBoxes = true;
            this.clPressureLevelsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clPressureLevelsListView.HideSelection = false;
            this.clPressureLevelsListView.Location = new System.Drawing.Point(3, 26);
            this.clPressureLevelsListView.Name = "clPressureLevelsListView";
            this.clPressureLevelsListView.Size = new System.Drawing.Size(124, 127);
            this.clPressureLevelsListView.TabIndex = 27;
            this.clPressureLevelsListView.Tag = "clChart";
            this.clPressureLevelsListView.UseCompatibleStateImageBehavior = false;
            this.clPressureLevelsListView.View = System.Windows.Forms.View.List;
            this.clPressureLevelsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.PressureLevelsListView_CheckedChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label35.Location = new System.Drawing.Point(3, 163);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(124, 13);
            this.label35.TabIndex = 25;
            this.label35.Text = "Parameter(s)";
            // 
            // clParametersListView
            // 
            this.clParametersListView.CheckBoxes = true;
            this.clParametersListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clParametersListView.HideSelection = false;
            this.clParametersListView.Location = new System.Drawing.Point(3, 179);
            this.clParametersListView.Name = "clParametersListView";
            this.clParametersListView.Size = new System.Drawing.Size(124, 127);
            this.clParametersListView.TabIndex = 22;
            this.clParametersListView.Tag = "clChart";
            this.clParametersListView.UseCompatibleStateImageBehavior = false;
            this.clParametersListView.View = System.Windows.Forms.View.List;
            this.clParametersListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.ParametersListView_CheckChanged);
            // 
            // clSplitContainer
            // 
            this.clSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clSplitContainer.Location = new System.Drawing.Point(133, 26);
            this.clSplitContainer.Name = "clSplitContainer";
            this.clSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // clSplitContainer.Panel1
            // 
            this.clSplitContainer.Panel1.Controls.Add(this.clChart);
            // 
            // clSplitContainer.Panel2
            // 
            this.clSplitContainer.Panel2.Controls.Add(this.clChartedDataGridView);
            this.clSplitContainer.Panel2MinSize = 0;
            this.tableLayoutPanel11.SetRowSpan(this.clSplitContainer, 8);
            this.clSplitContainer.Size = new System.Drawing.Size(638, 528);
            this.clSplitContainer.SplitterDistance = 427;
            this.clSplitContainer.TabIndex = 33;
            // 
            // clChartedDataGridView
            // 
            this.clChartedDataGridView.AllowUserToAddRows = false;
            this.clChartedDataGridView.AllowUserToDeleteRows = false;
            this.clChartedDataGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            this.clChartedDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.clChartedDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.clChartedDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.clChartedDataGridView.DefaultCellStyle = dataGridViewCellStyle16;
            this.clChartedDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clChartedDataGridView.Location = new System.Drawing.Point(0, 0);
            this.clChartedDataGridView.Name = "clChartedDataGridView";
            this.clChartedDataGridView.ReadOnly = true;
            this.clChartedDataGridView.Size = new System.Drawing.Size(638, 97);
            this.clChartedDataGridView.TabIndex = 24;
            // 
            // clLogPlotTabPage
            // 
            this.clLogPlotTabPage.Controls.Add(this.tableLayoutPanel14);
            this.clLogPlotTabPage.Location = new System.Drawing.Point(4, 22);
            this.clLogPlotTabPage.Name = "clLogPlotTabPage";
            this.clLogPlotTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.clLogPlotTabPage.Size = new System.Drawing.Size(780, 563);
            this.clLogPlotTabPage.TabIndex = 4;
            this.clLogPlotTabPage.Tag = "clLogPlotChart";
            this.clLogPlotTabPage.Text = "CLT log plot";
            this.clLogPlotTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 2;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Controls.Add(this.tableLayoutPanel27, 1, 0);
            this.tableLayoutPanel14.Controls.Add(this.label12, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.showFuelAsErrorCheckBox, 0, 6);
            this.tableLayoutPanel14.Controls.Add(this.clLogPlotPressureLevelsListView, 0, 1);
            this.tableLayoutPanel14.Controls.Add(this.clLogPlotStatisticsListView, 0, 5);
            this.tableLayoutPanel14.Controls.Add(this.label11, 0, 4);
            this.tableLayoutPanel14.Controls.Add(this.clLogPlotUpdateChartButton, 0, 8);
            this.tableLayoutPanel14.Controls.Add(this.label13, 0, 2);
            this.tableLayoutPanel14.Controls.Add(this.clLogPlotParametersListView, 0, 3);
            this.tableLayoutPanel14.Controls.Add(this.clLogPlotSsplitContainer, 1, 1);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 9;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(774, 557);
            this.tableLayoutPanel14.TabIndex = 42;
            // 
            // tableLayoutPanel27
            // 
            this.tableLayoutPanel27.ColumnCount = 6;
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel27.Controls.Add(this.clLogPlotChartLegendCheckBox, 5, 0);
            this.tableLayoutPanel27.Controls.Add(this.copyButton4, 2, 0);
            this.tableLayoutPanel27.Controls.Add(this.powerPointButton4, 0, 0);
            this.tableLayoutPanel27.Controls.Add(this.excelButton4, 1, 0);
            this.tableLayoutPanel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel27.Location = new System.Drawing.Point(130, 0);
            this.tableLayoutPanel27.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel27.Name = "tableLayoutPanel27";
            this.tableLayoutPanel27.RowCount = 1;
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel27.Size = new System.Drawing.Size(644, 23);
            this.tableLayoutPanel27.TabIndex = 42;
            // 
            // clLogPlotChartLegendCheckBox
            // 
            this.clLogPlotChartLegendCheckBox.AutoSize = true;
            this.clLogPlotChartLegendCheckBox.Checked = true;
            this.clLogPlotChartLegendCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.clLogPlotChartLegendCheckBox.Location = new System.Drawing.Point(577, 3);
            this.clLogPlotChartLegendCheckBox.Name = "clLogPlotChartLegendCheckBox";
            this.clLogPlotChartLegendCheckBox.Size = new System.Drawing.Size(62, 17);
            this.clLogPlotChartLegendCheckBox.TabIndex = 5;
            this.clLogPlotChartLegendCheckBox.Tag = "clLogPlotChart";
            this.clLogPlotChartLegendCheckBox.Text = "Legend";
            this.clLogPlotChartLegendCheckBox.UseVisualStyleBackColor = true;
            this.clLogPlotChartLegendCheckBox.CheckedChanged += new System.EventHandler(this.DetailedChartLegendCheckBox_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label12.Location = new System.Drawing.Point(3, 10);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(124, 13);
            this.label12.TabIndex = 38;
            this.label12.Text = "Pressure Level(s)";
            // 
            // showFuelAsErrorCheckBox
            // 
            this.showFuelAsErrorCheckBox.AutoSize = true;
            this.showFuelAsErrorCheckBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.showFuelAsErrorCheckBox.Location = new System.Drawing.Point(3, 437);
            this.showFuelAsErrorCheckBox.Name = "showFuelAsErrorCheckBox";
            this.showFuelAsErrorCheckBox.Size = new System.Drawing.Size(124, 17);
            this.showFuelAsErrorCheckBox.TabIndex = 41;
            this.showFuelAsErrorCheckBox.Text = "Show fuel as error";
            this.showFuelAsErrorCheckBox.UseVisualStyleBackColor = true;
            // 
            // clLogPlotPressureLevelsListView
            // 
            this.clLogPlotPressureLevelsListView.CheckBoxes = true;
            this.clLogPlotPressureLevelsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clLogPlotPressureLevelsListView.HideSelection = false;
            this.clLogPlotPressureLevelsListView.Location = new System.Drawing.Point(3, 26);
            this.clLogPlotPressureLevelsListView.Name = "clLogPlotPressureLevelsListView";
            this.clLogPlotPressureLevelsListView.Size = new System.Drawing.Size(124, 127);
            this.clLogPlotPressureLevelsListView.TabIndex = 37;
            this.clLogPlotPressureLevelsListView.Tag = "clLogPlotChart";
            this.clLogPlotPressureLevelsListView.UseCompatibleStateImageBehavior = false;
            this.clLogPlotPressureLevelsListView.View = System.Windows.Forms.View.List;
            this.clLogPlotPressureLevelsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.PressureLevelsListView_CheckedChanged);
            // 
            // clLogPlotStatisticsListView
            // 
            this.clLogPlotStatisticsListView.CheckBoxes = true;
            this.clLogPlotStatisticsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clLogPlotStatisticsListView.HideSelection = false;
            listViewItem11.StateImageIndex = 0;
            listViewItem12.StateImageIndex = 0;
            listViewItem13.StateImageIndex = 0;
            listViewItem14.StateImageIndex = 0;
            listViewItem15.StateImageIndex = 0;
            this.clLogPlotStatisticsListView.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem11,
            listViewItem12,
            listViewItem13,
            listViewItem14,
            listViewItem15});
            this.clLogPlotStatisticsListView.Location = new System.Drawing.Point(3, 332);
            this.clLogPlotStatisticsListView.Name = "clLogPlotStatisticsListView";
            this.clLogPlotStatisticsListView.Scrollable = false;
            this.clLogPlotStatisticsListView.Size = new System.Drawing.Size(124, 99);
            this.clLogPlotStatisticsListView.TabIndex = 39;
            this.clLogPlotStatisticsListView.Tag = "clLogPlotChart";
            this.clLogPlotStatisticsListView.UseCompatibleStateImageBehavior = false;
            this.clLogPlotStatisticsListView.View = System.Windows.Forms.View.List;
            this.clLogPlotStatisticsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.StatisticsListView_CheckChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label11.Location = new System.Drawing.Point(3, 316);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(124, 13);
            this.label11.TabIndex = 40;
            this.label11.Text = "Statisitic(s)";
            // 
            // clLogPlotUpdateChartButton
            // 
            this.clLogPlotUpdateChartButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clLogPlotUpdateChartButton.Location = new System.Drawing.Point(3, 528);
            this.clLogPlotUpdateChartButton.Name = "clLogPlotUpdateChartButton";
            this.clLogPlotUpdateChartButton.Size = new System.Drawing.Size(124, 26);
            this.clLogPlotUpdateChartButton.TabIndex = 33;
            this.clLogPlotUpdateChartButton.Text = "Update chart";
            this.clLogPlotUpdateChartButton.UseVisualStyleBackColor = true;
            this.clLogPlotUpdateChartButton.Click += new System.EventHandler(this.clLogPlotUpdateChartButton_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label13.Location = new System.Drawing.Point(3, 163);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(124, 13);
            this.label13.TabIndex = 35;
            this.label13.Text = "Parameter(s)";
            // 
            // clLogPlotParametersListView
            // 
            this.clLogPlotParametersListView.CheckBoxes = true;
            this.clLogPlotParametersListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clLogPlotParametersListView.HideSelection = false;
            this.clLogPlotParametersListView.Location = new System.Drawing.Point(3, 179);
            this.clLogPlotParametersListView.Name = "clLogPlotParametersListView";
            this.clLogPlotParametersListView.Size = new System.Drawing.Size(124, 127);
            this.clLogPlotParametersListView.TabIndex = 32;
            this.clLogPlotParametersListView.Tag = "clLogPlotChart";
            this.clLogPlotParametersListView.UseCompatibleStateImageBehavior = false;
            this.clLogPlotParametersListView.View = System.Windows.Forms.View.List;
            this.clLogPlotParametersListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.ParametersListView_CheckChanged);
            // 
            // clLogPlotSsplitContainer
            // 
            this.clLogPlotSsplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clLogPlotSsplitContainer.Location = new System.Drawing.Point(133, 26);
            this.clLogPlotSsplitContainer.Name = "clLogPlotSsplitContainer";
            this.clLogPlotSsplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // clLogPlotSsplitContainer.Panel1
            // 
            this.clLogPlotSsplitContainer.Panel1.Controls.Add(this.clLogPlotChart);
            // 
            // clLogPlotSsplitContainer.Panel2
            // 
            this.clLogPlotSsplitContainer.Panel2.Controls.Add(this.clLogPlotChartedDataGridView);
            this.clLogPlotSsplitContainer.Panel2MinSize = 0;
            this.tableLayoutPanel14.SetRowSpan(this.clLogPlotSsplitContainer, 8);
            this.clLogPlotSsplitContainer.Size = new System.Drawing.Size(638, 528);
            this.clLogPlotSsplitContainer.SplitterDistance = 427;
            this.clLogPlotSsplitContainer.TabIndex = 44;
            // 
            // clLogPlotChartedDataGridView
            // 
            this.clLogPlotChartedDataGridView.AllowUserToAddRows = false;
            this.clLogPlotChartedDataGridView.AllowUserToDeleteRows = false;
            this.clLogPlotChartedDataGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            this.clLogPlotChartedDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.clLogPlotChartedDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.clLogPlotChartedDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.clLogPlotChartedDataGridView.DefaultCellStyle = dataGridViewCellStyle18;
            this.clLogPlotChartedDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clLogPlotChartedDataGridView.Location = new System.Drawing.Point(0, 0);
            this.clLogPlotChartedDataGridView.Name = "clLogPlotChartedDataGridView";
            this.clLogPlotChartedDataGridView.ReadOnly = true;
            this.clLogPlotChartedDataGridView.Size = new System.Drawing.Size(638, 97);
            this.clLogPlotChartedDataGridView.TabIndex = 34;
            // 
            // olStabiliseTabPage
            // 
            this.olStabiliseTabPage.Controls.Add(this.tableLayoutPanel18);
            this.olStabiliseTabPage.Location = new System.Drawing.Point(4, 22);
            this.olStabiliseTabPage.Name = "olStabiliseTabPage";
            this.olStabiliseTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.olStabiliseTabPage.Size = new System.Drawing.Size(780, 563);
            this.olStabiliseTabPage.TabIndex = 6;
            this.olStabiliseTabPage.Tag = "olStabiliseChart;";
            this.olStabiliseTabPage.Text = "OL Stabilise";
            this.olStabiliseTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 2;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.Controls.Add(this.tableLayoutPanel28, 1, 0);
            this.tableLayoutPanel18.Controls.Add(this.label42, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.olStabiliseNominalChartedDataGridView, 0, 7);
            this.tableLayoutPanel18.Controls.Add(this.olStabilisePressureLevelsListView, 0, 1);
            this.tableLayoutPanel18.Controls.Add(this.olStabiliseShowNominalCheckBox, 0, 6);
            this.tableLayoutPanel18.Controls.Add(this.olStabiliseUpdateChartButton, 0, 8);
            this.tableLayoutPanel18.Controls.Add(this.label43, 0, 2);
            this.tableLayoutPanel18.Controls.Add(this.label44, 0, 4);
            this.tableLayoutPanel18.Controls.Add(this.olStabiliseParametersListView, 0, 3);
            this.tableLayoutPanel18.Controls.Add(this.olStabiliseStatisticsListView, 0, 5);
            this.tableLayoutPanel18.Controls.Add(this.olStabiliseSplitContainer, 1, 1);
            this.tableLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel18.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 9;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(774, 557);
            this.tableLayoutPanel18.TabIndex = 25;
            // 
            // tableLayoutPanel28
            // 
            this.tableLayoutPanel28.ColumnCount = 6;
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel28.Controls.Add(this.olStabiliseChartLegendCheckBox, 5, 0);
            this.tableLayoutPanel28.Controls.Add(this.copyButton5, 2, 0);
            this.tableLayoutPanel28.Controls.Add(this.powerPointButton5, 0, 0);
            this.tableLayoutPanel28.Controls.Add(this.excelButton5, 1, 0);
            this.tableLayoutPanel28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel28.Location = new System.Drawing.Point(130, 0);
            this.tableLayoutPanel28.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel28.Name = "tableLayoutPanel28";
            this.tableLayoutPanel28.RowCount = 1;
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel28.Size = new System.Drawing.Size(644, 23);
            this.tableLayoutPanel28.TabIndex = 26;
            // 
            // olStabiliseChartLegendCheckBox
            // 
            this.olStabiliseChartLegendCheckBox.AutoSize = true;
            this.olStabiliseChartLegendCheckBox.Checked = true;
            this.olStabiliseChartLegendCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.olStabiliseChartLegendCheckBox.Location = new System.Drawing.Point(577, 3);
            this.olStabiliseChartLegendCheckBox.Name = "olStabiliseChartLegendCheckBox";
            this.olStabiliseChartLegendCheckBox.Size = new System.Drawing.Size(62, 17);
            this.olStabiliseChartLegendCheckBox.TabIndex = 5;
            this.olStabiliseChartLegendCheckBox.Tag = "olStabiliseChart";
            this.olStabiliseChartLegendCheckBox.Text = "Legend";
            this.olStabiliseChartLegendCheckBox.UseVisualStyleBackColor = true;
            this.olStabiliseChartLegendCheckBox.CheckedChanged += new System.EventHandler(this.DetailedChartLegendCheckBox_CheckedChanged);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label42.Location = new System.Drawing.Point(3, 10);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(124, 13);
            this.label42.TabIndex = 19;
            this.label42.Text = "Pressure Level(s)";
            // 
            // olStabiliseNominalChartedDataGridView
            // 
            this.olStabiliseNominalChartedDataGridView.AllowUserToAddRows = false;
            this.olStabiliseNominalChartedDataGridView.AllowUserToDeleteRows = false;
            this.olStabiliseNominalChartedDataGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.olStabiliseNominalChartedDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.olStabiliseNominalChartedDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.olStabiliseNominalChartedDataGridView.DefaultCellStyle = dataGridViewCellStyle20;
            this.olStabiliseNominalChartedDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olStabiliseNominalChartedDataGridView.Location = new System.Drawing.Point(3, 462);
            this.olStabiliseNominalChartedDataGridView.Name = "olStabiliseNominalChartedDataGridView";
            this.olStabiliseNominalChartedDataGridView.ReadOnly = true;
            this.olStabiliseNominalChartedDataGridView.Size = new System.Drawing.Size(124, 60);
            this.olStabiliseNominalChartedDataGridView.TabIndex = 24;
            // 
            // olStabilisePressureLevelsListView
            // 
            this.olStabilisePressureLevelsListView.CheckBoxes = true;
            this.olStabilisePressureLevelsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olStabilisePressureLevelsListView.HideSelection = false;
            this.olStabilisePressureLevelsListView.Location = new System.Drawing.Point(3, 26);
            this.olStabilisePressureLevelsListView.Name = "olStabilisePressureLevelsListView";
            this.olStabilisePressureLevelsListView.Size = new System.Drawing.Size(124, 127);
            this.olStabilisePressureLevelsListView.TabIndex = 14;
            this.olStabilisePressureLevelsListView.Tag = "olStabiliseChart";
            this.olStabilisePressureLevelsListView.UseCompatibleStateImageBehavior = false;
            this.olStabilisePressureLevelsListView.View = System.Windows.Forms.View.List;
            this.olStabilisePressureLevelsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.PressureLevelsListView_CheckedChanged);
            // 
            // olStabiliseShowNominalCheckBox
            // 
            this.olStabiliseShowNominalCheckBox.AutoSize = true;
            this.olStabiliseShowNominalCheckBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.olStabiliseShowNominalCheckBox.Enabled = false;
            this.olStabiliseShowNominalCheckBox.Location = new System.Drawing.Point(3, 437);
            this.olStabiliseShowNominalCheckBox.Name = "olStabiliseShowNominalCheckBox";
            this.olStabiliseShowNominalCheckBox.Size = new System.Drawing.Size(124, 17);
            this.olStabiliseShowNominalCheckBox.TabIndex = 23;
            this.olStabiliseShowNominalCheckBox.Text = "Show nominal";
            this.olStabiliseShowNominalCheckBox.UseVisualStyleBackColor = true;
            this.olStabiliseShowNominalCheckBox.Visible = false;
            // 
            // olStabiliseUpdateChartButton
            // 
            this.olStabiliseUpdateChartButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olStabiliseUpdateChartButton.Location = new System.Drawing.Point(3, 528);
            this.olStabiliseUpdateChartButton.Name = "olStabiliseUpdateChartButton";
            this.olStabiliseUpdateChartButton.Size = new System.Drawing.Size(124, 26);
            this.olStabiliseUpdateChartButton.TabIndex = 16;
            this.olStabiliseUpdateChartButton.Text = "Update chart";
            this.olStabiliseUpdateChartButton.UseVisualStyleBackColor = true;
            this.olStabiliseUpdateChartButton.Click += new System.EventHandler(this.olStabiliseUpdateChartButton_Click);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label43.Location = new System.Drawing.Point(3, 163);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(124, 13);
            this.label43.TabIndex = 20;
            this.label43.Text = "Parameter(s)";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label44.Location = new System.Drawing.Point(3, 316);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(124, 13);
            this.label44.TabIndex = 22;
            this.label44.Text = "Statisitic(s)";
            // 
            // olStabiliseParametersListView
            // 
            this.olStabiliseParametersListView.CheckBoxes = true;
            this.olStabiliseParametersListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olStabiliseParametersListView.HideSelection = false;
            this.olStabiliseParametersListView.Location = new System.Drawing.Point(3, 179);
            this.olStabiliseParametersListView.Name = "olStabiliseParametersListView";
            this.olStabiliseParametersListView.Size = new System.Drawing.Size(124, 127);
            this.olStabiliseParametersListView.TabIndex = 15;
            this.olStabiliseParametersListView.Tag = "olStabiliseChart";
            this.olStabiliseParametersListView.UseCompatibleStateImageBehavior = false;
            this.olStabiliseParametersListView.View = System.Windows.Forms.View.List;
            this.olStabiliseParametersListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.ParametersListView_CheckChanged);
            // 
            // olStabiliseStatisticsListView
            // 
            this.olStabiliseStatisticsListView.CheckBoxes = true;
            this.olStabiliseStatisticsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olStabiliseStatisticsListView.HideSelection = false;
            listViewItem16.StateImageIndex = 0;
            listViewItem17.StateImageIndex = 0;
            listViewItem18.StateImageIndex = 0;
            listViewItem19.StateImageIndex = 0;
            listViewItem20.StateImageIndex = 0;
            this.olStabiliseStatisticsListView.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem16,
            listViewItem17,
            listViewItem18,
            listViewItem19,
            listViewItem20});
            this.olStabiliseStatisticsListView.Location = new System.Drawing.Point(3, 332);
            this.olStabiliseStatisticsListView.Name = "olStabiliseStatisticsListView";
            this.olStabiliseStatisticsListView.Scrollable = false;
            this.olStabiliseStatisticsListView.Size = new System.Drawing.Size(124, 99);
            this.olStabiliseStatisticsListView.TabIndex = 21;
            this.olStabiliseStatisticsListView.Tag = "olStabiliseChart";
            this.olStabiliseStatisticsListView.UseCompatibleStateImageBehavior = false;
            this.olStabiliseStatisticsListView.View = System.Windows.Forms.View.List;
            this.olStabiliseStatisticsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.StatisticsListView_CheckChanged);
            // 
            // olStabiliseSplitContainer
            // 
            this.olStabiliseSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olStabiliseSplitContainer.Location = new System.Drawing.Point(133, 26);
            this.olStabiliseSplitContainer.Name = "olStabiliseSplitContainer";
            this.olStabiliseSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // olStabiliseSplitContainer.Panel1
            // 
            this.olStabiliseSplitContainer.Panel1.Controls.Add(this.olStabiliseChart);
            // 
            // olStabiliseSplitContainer.Panel2
            // 
            this.olStabiliseSplitContainer.Panel2.Controls.Add(this.olStabiliseChartedDataGridView);
            this.olStabiliseSplitContainer.Panel2MinSize = 0;
            this.tableLayoutPanel18.SetRowSpan(this.olStabiliseSplitContainer, 8);
            this.olStabiliseSplitContainer.Size = new System.Drawing.Size(638, 528);
            this.olStabiliseSplitContainer.SplitterDistance = 427;
            this.olStabiliseSplitContainer.TabIndex = 28;
            // 
            // olStabiliseChartedDataGridView
            // 
            this.olStabiliseChartedDataGridView.AllowUserToAddRows = false;
            this.olStabiliseChartedDataGridView.AllowUserToDeleteRows = false;
            this.olStabiliseChartedDataGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            this.olStabiliseChartedDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.olStabiliseChartedDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.olStabiliseChartedDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.olStabiliseChartedDataGridView.DefaultCellStyle = dataGridViewCellStyle22;
            this.olStabiliseChartedDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olStabiliseChartedDataGridView.Location = new System.Drawing.Point(0, 0);
            this.olStabiliseChartedDataGridView.Name = "olStabiliseChartedDataGridView";
            this.olStabiliseChartedDataGridView.ReadOnly = true;
            this.olStabiliseChartedDataGridView.Size = new System.Drawing.Size(638, 97);
            this.olStabiliseChartedDataGridView.TabIndex = 17;
            // 
            // clStabiliseTabPage
            // 
            this.clStabiliseTabPage.Controls.Add(this.tableLayoutPanel19);
            this.clStabiliseTabPage.Location = new System.Drawing.Point(4, 22);
            this.clStabiliseTabPage.Name = "clStabiliseTabPage";
            this.clStabiliseTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.clStabiliseTabPage.Size = new System.Drawing.Size(780, 563);
            this.clStabiliseTabPage.TabIndex = 7;
            this.clStabiliseTabPage.Tag = "clStabiliseChart";
            this.clStabiliseTabPage.Text = "CL Stabilise";
            this.clStabiliseTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 2;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel19.Controls.Add(this.tableLayoutPanel29, 1, 0);
            this.tableLayoutPanel19.Controls.Add(this.label45, 0, 0);
            this.tableLayoutPanel19.Controls.Add(this.clStabiliseNominalChartedDataGridView, 0, 7);
            this.tableLayoutPanel19.Controls.Add(this.clStabilisePressureLevelsListView, 0, 1);
            this.tableLayoutPanel19.Controls.Add(this.clStabiliseShowNominalCheckBox, 0, 6);
            this.tableLayoutPanel19.Controls.Add(this.clStabiliseUpdateChartButton, 0, 8);
            this.tableLayoutPanel19.Controls.Add(this.label46, 0, 2);
            this.tableLayoutPanel19.Controls.Add(this.label47, 0, 4);
            this.tableLayoutPanel19.Controls.Add(this.clStabiliseParametersListView, 0, 3);
            this.tableLayoutPanel19.Controls.Add(this.clStabiliseStatisticsListView, 0, 5);
            this.tableLayoutPanel19.Controls.Add(this.clStabiliseSplitContainer, 1, 1);
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 9;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(774, 557);
            this.tableLayoutPanel19.TabIndex = 25;
            // 
            // tableLayoutPanel29
            // 
            this.tableLayoutPanel29.ColumnCount = 6;
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel29.Controls.Add(this.clStabiliseChartLegendCheckBox, 5, 0);
            this.tableLayoutPanel29.Controls.Add(this.copyButton6, 2, 0);
            this.tableLayoutPanel29.Controls.Add(this.powerPointButton6, 0, 0);
            this.tableLayoutPanel29.Controls.Add(this.excelButton6, 1, 0);
            this.tableLayoutPanel29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel29.Location = new System.Drawing.Point(130, 0);
            this.tableLayoutPanel29.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel29.Name = "tableLayoutPanel29";
            this.tableLayoutPanel29.RowCount = 1;
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel29.Size = new System.Drawing.Size(644, 23);
            this.tableLayoutPanel29.TabIndex = 26;
            // 
            // clStabiliseChartLegendCheckBox
            // 
            this.clStabiliseChartLegendCheckBox.AutoSize = true;
            this.clStabiliseChartLegendCheckBox.Checked = true;
            this.clStabiliseChartLegendCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.clStabiliseChartLegendCheckBox.Location = new System.Drawing.Point(577, 3);
            this.clStabiliseChartLegendCheckBox.Name = "clStabiliseChartLegendCheckBox";
            this.clStabiliseChartLegendCheckBox.Size = new System.Drawing.Size(62, 17);
            this.clStabiliseChartLegendCheckBox.TabIndex = 5;
            this.clStabiliseChartLegendCheckBox.Tag = "clStabiliseChart";
            this.clStabiliseChartLegendCheckBox.Text = "Legend";
            this.clStabiliseChartLegendCheckBox.UseVisualStyleBackColor = true;
            this.clStabiliseChartLegendCheckBox.CheckedChanged += new System.EventHandler(this.DetailedChartLegendCheckBox_CheckedChanged);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label45.Location = new System.Drawing.Point(3, 10);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(124, 13);
            this.label45.TabIndex = 19;
            this.label45.Text = "Pressure Level(s)";
            // 
            // clStabiliseNominalChartedDataGridView
            // 
            this.clStabiliseNominalChartedDataGridView.AllowUserToAddRows = false;
            this.clStabiliseNominalChartedDataGridView.AllowUserToDeleteRows = false;
            this.clStabiliseNominalChartedDataGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.clStabiliseNominalChartedDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.clStabiliseNominalChartedDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.clStabiliseNominalChartedDataGridView.DefaultCellStyle = dataGridViewCellStyle24;
            this.clStabiliseNominalChartedDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clStabiliseNominalChartedDataGridView.Location = new System.Drawing.Point(3, 462);
            this.clStabiliseNominalChartedDataGridView.Name = "clStabiliseNominalChartedDataGridView";
            this.clStabiliseNominalChartedDataGridView.ReadOnly = true;
            this.clStabiliseNominalChartedDataGridView.Size = new System.Drawing.Size(124, 60);
            this.clStabiliseNominalChartedDataGridView.TabIndex = 24;
            // 
            // clStabilisePressureLevelsListView
            // 
            this.clStabilisePressureLevelsListView.CheckBoxes = true;
            this.clStabilisePressureLevelsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clStabilisePressureLevelsListView.HideSelection = false;
            this.clStabilisePressureLevelsListView.Location = new System.Drawing.Point(3, 26);
            this.clStabilisePressureLevelsListView.Name = "clStabilisePressureLevelsListView";
            this.clStabilisePressureLevelsListView.Size = new System.Drawing.Size(124, 127);
            this.clStabilisePressureLevelsListView.TabIndex = 14;
            this.clStabilisePressureLevelsListView.Tag = "clStabiliseChart";
            this.clStabilisePressureLevelsListView.UseCompatibleStateImageBehavior = false;
            this.clStabilisePressureLevelsListView.View = System.Windows.Forms.View.List;
            this.clStabilisePressureLevelsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.PressureLevelsListView_CheckedChanged);
            // 
            // clStabiliseShowNominalCheckBox
            // 
            this.clStabiliseShowNominalCheckBox.AutoSize = true;
            this.clStabiliseShowNominalCheckBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.clStabiliseShowNominalCheckBox.Enabled = false;
            this.clStabiliseShowNominalCheckBox.Location = new System.Drawing.Point(3, 437);
            this.clStabiliseShowNominalCheckBox.Name = "clStabiliseShowNominalCheckBox";
            this.clStabiliseShowNominalCheckBox.Size = new System.Drawing.Size(124, 17);
            this.clStabiliseShowNominalCheckBox.TabIndex = 23;
            this.clStabiliseShowNominalCheckBox.Text = "Show nominal";
            this.clStabiliseShowNominalCheckBox.UseVisualStyleBackColor = true;
            this.clStabiliseShowNominalCheckBox.Visible = false;
            // 
            // clStabiliseUpdateChartButton
            // 
            this.clStabiliseUpdateChartButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clStabiliseUpdateChartButton.Location = new System.Drawing.Point(3, 528);
            this.clStabiliseUpdateChartButton.Name = "clStabiliseUpdateChartButton";
            this.clStabiliseUpdateChartButton.Size = new System.Drawing.Size(124, 26);
            this.clStabiliseUpdateChartButton.TabIndex = 16;
            this.clStabiliseUpdateChartButton.Text = "Update chart";
            this.clStabiliseUpdateChartButton.UseVisualStyleBackColor = true;
            this.clStabiliseUpdateChartButton.Click += new System.EventHandler(this.clStabiliseUpdateChartButton_Click);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label46.Location = new System.Drawing.Point(3, 163);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(124, 13);
            this.label46.TabIndex = 20;
            this.label46.Text = "Parameter(s)";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label47.Location = new System.Drawing.Point(3, 316);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(124, 13);
            this.label47.TabIndex = 22;
            this.label47.Text = "Statisitic(s)";
            // 
            // clStabiliseParametersListView
            // 
            this.clStabiliseParametersListView.CheckBoxes = true;
            this.clStabiliseParametersListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clStabiliseParametersListView.HideSelection = false;
            this.clStabiliseParametersListView.Location = new System.Drawing.Point(3, 179);
            this.clStabiliseParametersListView.Name = "clStabiliseParametersListView";
            this.clStabiliseParametersListView.Size = new System.Drawing.Size(124, 127);
            this.clStabiliseParametersListView.TabIndex = 15;
            this.clStabiliseParametersListView.Tag = "clStabiliseChart";
            this.clStabiliseParametersListView.UseCompatibleStateImageBehavior = false;
            this.clStabiliseParametersListView.View = System.Windows.Forms.View.List;
            this.clStabiliseParametersListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.ParametersListView_CheckChanged);
            // 
            // clStabiliseStatisticsListView
            // 
            this.clStabiliseStatisticsListView.CheckBoxes = true;
            this.clStabiliseStatisticsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clStabiliseStatisticsListView.HideSelection = false;
            listViewItem21.StateImageIndex = 0;
            listViewItem22.StateImageIndex = 0;
            listViewItem23.StateImageIndex = 0;
            listViewItem24.StateImageIndex = 0;
            listViewItem25.StateImageIndex = 0;
            this.clStabiliseStatisticsListView.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem21,
            listViewItem22,
            listViewItem23,
            listViewItem24,
            listViewItem25});
            this.clStabiliseStatisticsListView.Location = new System.Drawing.Point(3, 332);
            this.clStabiliseStatisticsListView.Name = "clStabiliseStatisticsListView";
            this.clStabiliseStatisticsListView.Scrollable = false;
            this.clStabiliseStatisticsListView.Size = new System.Drawing.Size(124, 99);
            this.clStabiliseStatisticsListView.TabIndex = 21;
            this.clStabiliseStatisticsListView.Tag = "clStabiliseChart";
            this.clStabiliseStatisticsListView.UseCompatibleStateImageBehavior = false;
            this.clStabiliseStatisticsListView.View = System.Windows.Forms.View.List;
            this.clStabiliseStatisticsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.StatisticsListView_CheckChanged);
            // 
            // clStabiliseSplitContainer
            // 
            this.clStabiliseSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clStabiliseSplitContainer.Location = new System.Drawing.Point(133, 26);
            this.clStabiliseSplitContainer.Name = "clStabiliseSplitContainer";
            this.clStabiliseSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // clStabiliseSplitContainer.Panel1
            // 
            this.clStabiliseSplitContainer.Panel1.Controls.Add(this.clStabiliseChart);
            // 
            // clStabiliseSplitContainer.Panel2
            // 
            this.clStabiliseSplitContainer.Panel2.Controls.Add(this.clStabiliseChartedDataGridView);
            this.clStabiliseSplitContainer.Panel2MinSize = 0;
            this.tableLayoutPanel19.SetRowSpan(this.clStabiliseSplitContainer, 8);
            this.clStabiliseSplitContainer.Size = new System.Drawing.Size(638, 528);
            this.clStabiliseSplitContainer.SplitterDistance = 427;
            this.clStabiliseSplitContainer.TabIndex = 28;
            // 
            // clStabiliseChartedDataGridView
            // 
            this.clStabiliseChartedDataGridView.AllowUserToAddRows = false;
            this.clStabiliseChartedDataGridView.AllowUserToDeleteRows = false;
            this.clStabiliseChartedDataGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            this.clStabiliseChartedDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.clStabiliseChartedDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.clStabiliseChartedDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.clStabiliseChartedDataGridView.DefaultCellStyle = dataGridViewCellStyle26;
            this.clStabiliseChartedDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clStabiliseChartedDataGridView.Location = new System.Drawing.Point(0, 0);
            this.clStabiliseChartedDataGridView.Name = "clStabiliseChartedDataGridView";
            this.clStabiliseChartedDataGridView.ReadOnly = true;
            this.clStabiliseChartedDataGridView.Size = new System.Drawing.Size(638, 97);
            this.clStabiliseChartedDataGridView.TabIndex = 17;
            // 
            // fodTabPage
            // 
            this.fodTabPage.Controls.Add(this.tableLayoutPanel15);
            this.fodTabPage.Location = new System.Drawing.Point(4, 22);
            this.fodTabPage.Name = "fodTabPage";
            this.fodTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.fodTabPage.Size = new System.Drawing.Size(780, 563);
            this.fodTabPage.TabIndex = 5;
            this.fodTabPage.Tag = "fodChart";
            this.fodTabPage.Text = "FOD";
            this.fodTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 2;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Controls.Add(this.tableLayoutPanel30, 1, 0);
            this.tableLayoutPanel15.Controls.Add(this.label19, 0, 2);
            this.tableLayoutPanel15.Controls.Add(this.fodStatisticsListView, 0, 5);
            this.tableLayoutPanel15.Controls.Add(this.label8, 0, 4);
            this.tableLayoutPanel15.Controls.Add(this.fodParametersListView, 0, 3);
            this.tableLayoutPanel15.Controls.Add(this.fodUpdateChartButton, 0, 8);
            this.tableLayoutPanel15.Controls.Add(this.fodSplitContainer, 1, 1);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 9;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(774, 557);
            this.tableLayoutPanel15.TabIndex = 35;
            // 
            // tableLayoutPanel30
            // 
            this.tableLayoutPanel30.ColumnCount = 6;
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel30.Controls.Add(this.FODChartLegendCheckBox, 5, 0);
            this.tableLayoutPanel30.Controls.Add(this.copyButton7, 2, 0);
            this.tableLayoutPanel30.Controls.Add(this.powerPointButton7, 0, 0);
            this.tableLayoutPanel30.Controls.Add(this.excelButton7, 1, 0);
            this.tableLayoutPanel30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel30.Location = new System.Drawing.Point(130, 0);
            this.tableLayoutPanel30.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel30.Name = "tableLayoutPanel30";
            this.tableLayoutPanel30.RowCount = 1;
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel30.Size = new System.Drawing.Size(644, 23);
            this.tableLayoutPanel30.TabIndex = 36;
            // 
            // FODChartLegendCheckBox
            // 
            this.FODChartLegendCheckBox.AutoSize = true;
            this.FODChartLegendCheckBox.Checked = true;
            this.FODChartLegendCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.FODChartLegendCheckBox.Location = new System.Drawing.Point(577, 3);
            this.FODChartLegendCheckBox.Name = "FODChartLegendCheckBox";
            this.FODChartLegendCheckBox.Size = new System.Drawing.Size(62, 17);
            this.FODChartLegendCheckBox.TabIndex = 5;
            this.FODChartLegendCheckBox.Tag = "FODChart";
            this.FODChartLegendCheckBox.Text = "Legend";
            this.FODChartLegendCheckBox.UseVisualStyleBackColor = true;
            this.FODChartLegendCheckBox.CheckedChanged += new System.EventHandler(this.DetailedChartLegendCheckBox_CheckedChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label19.Location = new System.Drawing.Point(3, 163);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(124, 13);
            this.label19.TabIndex = 31;
            this.label19.Text = "Parameter(s)";
            // 
            // fodStatisticsListView
            // 
            this.fodStatisticsListView.CheckBoxes = true;
            this.fodStatisticsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fodStatisticsListView.HideSelection = false;
            listViewItem26.StateImageIndex = 0;
            listViewItem27.StateImageIndex = 0;
            listViewItem28.StateImageIndex = 0;
            listViewItem29.StateImageIndex = 0;
            listViewItem30.StateImageIndex = 0;
            this.fodStatisticsListView.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem26,
            listViewItem27,
            listViewItem28,
            listViewItem29,
            listViewItem30});
            this.fodStatisticsListView.Location = new System.Drawing.Point(3, 332);
            this.fodStatisticsListView.Name = "fodStatisticsListView";
            this.fodStatisticsListView.Scrollable = false;
            this.fodStatisticsListView.Size = new System.Drawing.Size(124, 99);
            this.fodStatisticsListView.TabIndex = 33;
            this.fodStatisticsListView.Tag = "fodChart";
            this.fodStatisticsListView.UseCompatibleStateImageBehavior = false;
            this.fodStatisticsListView.View = System.Windows.Forms.View.List;
            this.fodStatisticsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.StatisticsListView_CheckChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label8.Location = new System.Drawing.Point(3, 316);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(124, 13);
            this.label8.TabIndex = 34;
            this.label8.Text = "Statisitic(s)";
            // 
            // fodParametersListView
            // 
            this.fodParametersListView.CheckBoxes = true;
            this.fodParametersListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fodParametersListView.HideSelection = false;
            this.fodParametersListView.Location = new System.Drawing.Point(3, 179);
            this.fodParametersListView.Name = "fodParametersListView";
            this.fodParametersListView.Size = new System.Drawing.Size(124, 127);
            this.fodParametersListView.TabIndex = 28;
            this.fodParametersListView.Tag = "fodChart";
            this.fodParametersListView.UseCompatibleStateImageBehavior = false;
            this.fodParametersListView.View = System.Windows.Forms.View.List;
            this.fodParametersListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.ParametersListView_CheckChanged);
            // 
            // fodUpdateChartButton
            // 
            this.fodUpdateChartButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fodUpdateChartButton.Location = new System.Drawing.Point(3, 528);
            this.fodUpdateChartButton.Name = "fodUpdateChartButton";
            this.fodUpdateChartButton.Size = new System.Drawing.Size(124, 26);
            this.fodUpdateChartButton.TabIndex = 29;
            this.fodUpdateChartButton.Text = "Update chart";
            this.fodUpdateChartButton.UseVisualStyleBackColor = true;
            this.fodUpdateChartButton.Click += new System.EventHandler(this.fodUpdateChartButton_Click);
            // 
            // fodSplitContainer
            // 
            this.fodSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fodSplitContainer.Location = new System.Drawing.Point(133, 26);
            this.fodSplitContainer.Name = "fodSplitContainer";
            this.fodSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // fodSplitContainer.Panel1
            // 
            this.fodSplitContainer.Panel1.Controls.Add(this.fodChart);
            // 
            // fodSplitContainer.Panel2
            // 
            this.fodSplitContainer.Panel2.Controls.Add(this.fodChartedDataGridView);
            this.fodSplitContainer.Panel2MinSize = 0;
            this.tableLayoutPanel15.SetRowSpan(this.fodSplitContainer, 8);
            this.fodSplitContainer.Size = new System.Drawing.Size(638, 528);
            this.fodSplitContainer.SplitterDistance = 427;
            this.fodSplitContainer.TabIndex = 38;
            // 
            // fodChartedDataGridView
            // 
            this.fodChartedDataGridView.AllowUserToAddRows = false;
            this.fodChartedDataGridView.AllowUserToDeleteRows = false;
            this.fodChartedDataGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            this.fodChartedDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.fodChartedDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.fodChartedDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.fodChartedDataGridView.DefaultCellStyle = dataGridViewCellStyle28;
            this.fodChartedDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fodChartedDataGridView.Location = new System.Drawing.Point(0, 0);
            this.fodChartedDataGridView.Name = "fodChartedDataGridView";
            this.fodChartedDataGridView.ReadOnly = true;
            this.fodChartedDataGridView.Size = new System.Drawing.Size(638, 97);
            this.fodChartedDataGridView.TabIndex = 30;
            // 
            // omvTabPage
            // 
            this.omvTabPage.Controls.Add(this.tableLayoutPanel12);
            this.omvTabPage.Location = new System.Drawing.Point(4, 22);
            this.omvTabPage.Name = "omvTabPage";
            this.omvTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.omvTabPage.Size = new System.Drawing.Size(780, 563);
            this.omvTabPage.TabIndex = 2;
            this.omvTabPage.Tag = "omvChart";
            this.omvTabPage.Text = "OMV";
            this.omvTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel31, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.omvStatisticsListView, 0, 5);
            this.tableLayoutPanel12.Controls.Add(this.omvUpdateChartButton, 0, 8);
            this.tableLayoutPanel12.Controls.Add(this.label41, 0, 4);
            this.tableLayoutPanel12.Controls.Add(this.label37, 0, 2);
            this.tableLayoutPanel12.Controls.Add(this.omvParametersListView, 0, 3);
            this.tableLayoutPanel12.Controls.Add(this.omvSplitContainer, 1, 1);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 9;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(774, 557);
            this.tableLayoutPanel12.TabIndex = 35;
            // 
            // tableLayoutPanel31
            // 
            this.tableLayoutPanel31.ColumnCount = 6;
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel31.Controls.Add(this.OMVChartLegendCheckBox, 5, 0);
            this.tableLayoutPanel31.Controls.Add(this.powerPointButton8, 0, 0);
            this.tableLayoutPanel31.Controls.Add(this.excelButton8, 1, 0);
            this.tableLayoutPanel31.Controls.Add(this.copyButton8, 2, 0);
            this.tableLayoutPanel31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel31.Location = new System.Drawing.Point(130, 0);
            this.tableLayoutPanel31.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel31.Name = "tableLayoutPanel31";
            this.tableLayoutPanel31.RowCount = 1;
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel31.Size = new System.Drawing.Size(644, 23);
            this.tableLayoutPanel31.TabIndex = 36;
            // 
            // OMVChartLegendCheckBox
            // 
            this.OMVChartLegendCheckBox.AutoSize = true;
            this.OMVChartLegendCheckBox.Checked = true;
            this.OMVChartLegendCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OMVChartLegendCheckBox.Location = new System.Drawing.Point(577, 3);
            this.OMVChartLegendCheckBox.Name = "OMVChartLegendCheckBox";
            this.OMVChartLegendCheckBox.Size = new System.Drawing.Size(62, 17);
            this.OMVChartLegendCheckBox.TabIndex = 5;
            this.OMVChartLegendCheckBox.Tag = "OMVChart";
            this.OMVChartLegendCheckBox.Text = "Legend";
            this.OMVChartLegendCheckBox.UseVisualStyleBackColor = true;
            this.OMVChartLegendCheckBox.CheckedChanged += new System.EventHandler(this.DetailedChartLegendCheckBox_CheckedChanged);
            // 
            // omvStatisticsListView
            // 
            this.omvStatisticsListView.CheckBoxes = true;
            this.omvStatisticsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.omvStatisticsListView.HideSelection = false;
            listViewItem31.StateImageIndex = 0;
            listViewItem32.StateImageIndex = 0;
            listViewItem33.StateImageIndex = 0;
            listViewItem34.StateImageIndex = 0;
            listViewItem35.StateImageIndex = 0;
            this.omvStatisticsListView.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem31,
            listViewItem32,
            listViewItem33,
            listViewItem34,
            listViewItem35});
            this.omvStatisticsListView.Location = new System.Drawing.Point(3, 332);
            this.omvStatisticsListView.Name = "omvStatisticsListView";
            this.omvStatisticsListView.Scrollable = false;
            this.omvStatisticsListView.Size = new System.Drawing.Size(124, 99);
            this.omvStatisticsListView.TabIndex = 33;
            this.omvStatisticsListView.Tag = "omvChart";
            this.omvStatisticsListView.UseCompatibleStateImageBehavior = false;
            this.omvStatisticsListView.View = System.Windows.Forms.View.List;
            this.omvStatisticsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.StatisticsListView_CheckChanged);
            // 
            // omvUpdateChartButton
            // 
            this.omvUpdateChartButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.omvUpdateChartButton.Location = new System.Drawing.Point(3, 528);
            this.omvUpdateChartButton.Name = "omvUpdateChartButton";
            this.omvUpdateChartButton.Size = new System.Drawing.Size(124, 26);
            this.omvUpdateChartButton.TabIndex = 29;
            this.omvUpdateChartButton.Text = "Update chart";
            this.omvUpdateChartButton.UseVisualStyleBackColor = true;
            this.omvUpdateChartButton.Click += new System.EventHandler(this.omvUpdateChartButton_Click);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label41.Location = new System.Drawing.Point(3, 316);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(124, 13);
            this.label41.TabIndex = 34;
            this.label41.Text = "Statisitic(s)";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label37.Location = new System.Drawing.Point(3, 163);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(124, 13);
            this.label37.TabIndex = 31;
            this.label37.Text = "Parameter(s)";
            // 
            // omvParametersListView
            // 
            this.omvParametersListView.CheckBoxes = true;
            this.omvParametersListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.omvParametersListView.HideSelection = false;
            this.omvParametersListView.Location = new System.Drawing.Point(3, 179);
            this.omvParametersListView.Name = "omvParametersListView";
            this.omvParametersListView.Size = new System.Drawing.Size(124, 127);
            this.omvParametersListView.TabIndex = 28;
            this.omvParametersListView.Tag = "omvChart";
            this.omvParametersListView.UseCompatibleStateImageBehavior = false;
            this.omvParametersListView.View = System.Windows.Forms.View.List;
            this.omvParametersListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.ParametersListView_CheckChanged);
            // 
            // omvSplitContainer
            // 
            this.omvSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.omvSplitContainer.Location = new System.Drawing.Point(133, 26);
            this.omvSplitContainer.Name = "omvSplitContainer";
            this.omvSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // omvSplitContainer.Panel1
            // 
            this.omvSplitContainer.Panel1.Controls.Add(this.omvChart);
            // 
            // omvSplitContainer.Panel2
            // 
            this.omvSplitContainer.Panel2.Controls.Add(this.omvChartedDataGridView);
            this.omvSplitContainer.Panel2MinSize = 0;
            this.tableLayoutPanel12.SetRowSpan(this.omvSplitContainer, 8);
            this.omvSplitContainer.Size = new System.Drawing.Size(638, 528);
            this.omvSplitContainer.SplitterDistance = 427;
            this.omvSplitContainer.TabIndex = 38;
            // 
            // omvChartedDataGridView
            // 
            this.omvChartedDataGridView.AllowUserToAddRows = false;
            this.omvChartedDataGridView.AllowUserToDeleteRows = false;
            this.omvChartedDataGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            this.omvChartedDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.omvChartedDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle29;
            this.omvChartedDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.omvChartedDataGridView.DefaultCellStyle = dataGridViewCellStyle30;
            this.omvChartedDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.omvChartedDataGridView.Location = new System.Drawing.Point(0, 0);
            this.omvChartedDataGridView.Name = "omvChartedDataGridView";
            this.omvChartedDataGridView.ReadOnly = true;
            this.omvChartedDataGridView.Size = new System.Drawing.Size(638, 97);
            this.omvChartedDataGridView.TabIndex = 30;
            // 
            // leakdownTabPage
            // 
            this.leakdownTabPage.Controls.Add(this.tableLayoutPanel13);
            this.leakdownTabPage.Location = new System.Drawing.Point(4, 22);
            this.leakdownTabPage.Name = "leakdownTabPage";
            this.leakdownTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.leakdownTabPage.Size = new System.Drawing.Size(780, 563);
            this.leakdownTabPage.TabIndex = 3;
            this.leakdownTabPage.Text = "Leakdown (NOT USED)";
            this.leakdownTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 2;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Controls.Add(this.label38, 0, 2);
            this.tableLayoutPanel13.Controls.Add(this.leakdownUpdateChartButton, 0, 8);
            this.tableLayoutPanel13.Controls.Add(this.leakdownParametersListView, 0, 3);
            this.tableLayoutPanel13.Controls.Add(this.leakdownSplitContainer, 1, 1);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 9;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(774, 557);
            this.tableLayoutPanel13.TabIndex = 39;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label38.Location = new System.Drawing.Point(3, 163);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(124, 13);
            this.label38.TabIndex = 37;
            this.label38.Text = "Parameter(s)";
            // 
            // leakdownUpdateChartButton
            // 
            this.leakdownUpdateChartButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leakdownUpdateChartButton.Location = new System.Drawing.Point(3, 528);
            this.leakdownUpdateChartButton.Name = "leakdownUpdateChartButton";
            this.leakdownUpdateChartButton.Size = new System.Drawing.Size(124, 26);
            this.leakdownUpdateChartButton.TabIndex = 35;
            this.leakdownUpdateChartButton.Text = "Update chart";
            this.leakdownUpdateChartButton.UseVisualStyleBackColor = true;
            this.leakdownUpdateChartButton.Click += new System.EventHandler(this.leakdownUpdateChartButton_Click);
            // 
            // leakdownParametersListView
            // 
            this.leakdownParametersListView.CheckBoxes = true;
            this.leakdownParametersListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leakdownParametersListView.HideSelection = false;
            this.leakdownParametersListView.Location = new System.Drawing.Point(3, 179);
            this.leakdownParametersListView.Name = "leakdownParametersListView";
            this.leakdownParametersListView.Size = new System.Drawing.Size(124, 127);
            this.leakdownParametersListView.TabIndex = 34;
            this.leakdownParametersListView.UseCompatibleStateImageBehavior = false;
            this.leakdownParametersListView.View = System.Windows.Forms.View.List;
            // 
            // leakdownSplitContainer
            // 
            this.leakdownSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leakdownSplitContainer.Location = new System.Drawing.Point(133, 26);
            this.leakdownSplitContainer.Name = "leakdownSplitContainer";
            this.leakdownSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // leakdownSplitContainer.Panel1
            // 
            this.leakdownSplitContainer.Panel1.Controls.Add(this.leakdownChart);
            // 
            // leakdownSplitContainer.Panel2
            // 
            this.leakdownSplitContainer.Panel2.Controls.Add(this.leakdownChartedDataGridView);
            this.leakdownSplitContainer.Panel2MinSize = 0;
            this.tableLayoutPanel13.SetRowSpan(this.leakdownSplitContainer, 8);
            this.leakdownSplitContainer.Size = new System.Drawing.Size(638, 528);
            this.leakdownSplitContainer.SplitterDistance = 427;
            this.leakdownSplitContainer.TabIndex = 38;
            // 
            // leakdownChartedDataGridView
            // 
            this.leakdownChartedDataGridView.AllowUserToAddRows = false;
            this.leakdownChartedDataGridView.AllowUserToDeleteRows = false;
            this.leakdownChartedDataGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            this.leakdownChartedDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.leakdownChartedDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this.leakdownChartedDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.leakdownChartedDataGridView.DefaultCellStyle = dataGridViewCellStyle32;
            this.leakdownChartedDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leakdownChartedDataGridView.Location = new System.Drawing.Point(0, 0);
            this.leakdownChartedDataGridView.Name = "leakdownChartedDataGridView";
            this.leakdownChartedDataGridView.ReadOnly = true;
            this.leakdownChartedDataGridView.Size = new System.Drawing.Size(638, 97);
            this.leakdownChartedDataGridView.TabIndex = 36;
            // 
            // trimmedTabPage
            // 
            this.trimmedTabPage.Controls.Add(this.tableLayoutPanel22);
            this.trimmedTabPage.Location = new System.Drawing.Point(4, 22);
            this.trimmedTabPage.Name = "trimmedTabPage";
            this.trimmedTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.trimmedTabPage.Size = new System.Drawing.Size(780, 563);
            this.trimmedTabPage.TabIndex = 8;
            this.trimmedTabPage.Tag = "trimmedChart";
            this.trimmedTabPage.Text = "Trimmed";
            this.trimmedTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 2;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel22.Controls.Add(this.label53, 0, 0);
            this.tableLayoutPanel22.Controls.Add(this.trimmedStatisticsListView, 0, 5);
            this.tableLayoutPanel22.Controls.Add(this.trimmedUpdateChartButton, 0, 8);
            this.tableLayoutPanel22.Controls.Add(this.label54, 0, 4);
            this.tableLayoutPanel22.Controls.Add(this.trimmedPressureLevelsListView, 0, 1);
            this.tableLayoutPanel22.Controls.Add(this.label55, 0, 2);
            this.tableLayoutPanel22.Controls.Add(this.trimmedParametersListView, 0, 3);
            this.tableLayoutPanel22.Controls.Add(this.tableLayoutPanel25, 1, 0);
            this.tableLayoutPanel22.Controls.Add(this.trimmedSplitContainer, 1, 1);
            this.tableLayoutPanel22.Controls.Add(this.tableLayoutPanel40, 0, 6);
            this.tableLayoutPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel22.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 9;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(774, 557);
            this.tableLayoutPanel22.TabIndex = 32;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label53.Location = new System.Drawing.Point(3, 10);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(124, 13);
            this.label53.TabIndex = 28;
            this.label53.Text = "Pressure Level(s)";
            // 
            // trimmedStatisticsListView
            // 
            this.trimmedStatisticsListView.CheckBoxes = true;
            this.trimmedStatisticsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedStatisticsListView.HideSelection = false;
            listViewItem36.StateImageIndex = 0;
            this.trimmedStatisticsListView.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem36});
            this.trimmedStatisticsListView.Location = new System.Drawing.Point(3, 298);
            this.trimmedStatisticsListView.Name = "trimmedStatisticsListView";
            this.trimmedStatisticsListView.Scrollable = false;
            this.trimmedStatisticsListView.Size = new System.Drawing.Size(124, 99);
            this.trimmedStatisticsListView.TabIndex = 29;
            this.trimmedStatisticsListView.Tag = "trimmedChart";
            this.trimmedStatisticsListView.UseCompatibleStateImageBehavior = false;
            this.trimmedStatisticsListView.View = System.Windows.Forms.View.List;
            this.trimmedStatisticsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.StatisticsListView_CheckChanged);
            // 
            // trimmedUpdateChartButton
            // 
            this.trimmedUpdateChartButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedUpdateChartButton.Location = new System.Drawing.Point(3, 528);
            this.trimmedUpdateChartButton.Name = "trimmedUpdateChartButton";
            this.trimmedUpdateChartButton.Size = new System.Drawing.Size(124, 26);
            this.trimmedUpdateChartButton.TabIndex = 23;
            this.trimmedUpdateChartButton.Text = "Update chart";
            this.trimmedUpdateChartButton.UseVisualStyleBackColor = true;
            this.trimmedUpdateChartButton.Click += new System.EventHandler(this.trimmedUpdateChartButton_Click);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label54.Location = new System.Drawing.Point(3, 282);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(124, 13);
            this.label54.TabIndex = 30;
            this.label54.Text = "Statisitic(s)";
            // 
            // trimmedPressureLevelsListView
            // 
            this.trimmedPressureLevelsListView.CheckBoxes = true;
            this.trimmedPressureLevelsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedPressureLevelsListView.HideSelection = false;
            this.trimmedPressureLevelsListView.Location = new System.Drawing.Point(3, 26);
            this.trimmedPressureLevelsListView.Name = "trimmedPressureLevelsListView";
            this.trimmedPressureLevelsListView.Size = new System.Drawing.Size(124, 110);
            this.trimmedPressureLevelsListView.TabIndex = 27;
            this.trimmedPressureLevelsListView.Tag = "trimmedChart";
            this.trimmedPressureLevelsListView.UseCompatibleStateImageBehavior = false;
            this.trimmedPressureLevelsListView.View = System.Windows.Forms.View.List;
            this.trimmedPressureLevelsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.PressureLevelsListView_CheckedChanged);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label55.Location = new System.Drawing.Point(3, 146);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(124, 13);
            this.label55.TabIndex = 25;
            this.label55.Text = "Parameter(s)";
            // 
            // trimmedParametersListView
            // 
            this.trimmedParametersListView.CheckBoxes = true;
            this.trimmedParametersListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedParametersListView.HideSelection = false;
            this.trimmedParametersListView.Location = new System.Drawing.Point(3, 162);
            this.trimmedParametersListView.Name = "trimmedParametersListView";
            this.trimmedParametersListView.Size = new System.Drawing.Size(124, 110);
            this.trimmedParametersListView.TabIndex = 22;
            this.trimmedParametersListView.Tag = "trimmedChart";
            this.trimmedParametersListView.UseCompatibleStateImageBehavior = false;
            this.trimmedParametersListView.View = System.Windows.Forms.View.List;
            this.trimmedParametersListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.ParametersListView_CheckChanged);
            // 
            // tableLayoutPanel25
            // 
            this.tableLayoutPanel25.ColumnCount = 8;
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel25.Controls.Add(this.trimmedChartLegendCheckBox, 7, 0);
            this.tableLayoutPanel25.Controls.Add(this.trimmedChartIndividualCharts, 6, 0);
            this.tableLayoutPanel25.Controls.Add(this.trimmedChart_Limits, 5, 0);
            this.tableLayoutPanel25.Controls.Add(this.trimmedChart_LimitsLabel, 4, 0);
            this.tableLayoutPanel25.Controls.Add(this.powerPointButton9, 0, 0);
            this.tableLayoutPanel25.Controls.Add(this.trimmedChart_CopyToClipboard, 2, 0);
            this.tableLayoutPanel25.Controls.Add(this.excelButton9, 1, 0);
            this.tableLayoutPanel25.Controls.Add(this.viewStatsWindow, 3, 0);
            this.tableLayoutPanel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel25.Location = new System.Drawing.Point(130, 0);
            this.tableLayoutPanel25.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel25.Name = "tableLayoutPanel25";
            this.tableLayoutPanel25.RowCount = 1;
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel25.Size = new System.Drawing.Size(644, 23);
            this.tableLayoutPanel25.TabIndex = 31;
            // 
            // trimmedChartLegendCheckBox
            // 
            this.trimmedChartLegendCheckBox.AutoSize = true;
            this.trimmedChartLegendCheckBox.Checked = true;
            this.trimmedChartLegendCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.trimmedChartLegendCheckBox.Location = new System.Drawing.Point(577, 3);
            this.trimmedChartLegendCheckBox.Name = "trimmedChartLegendCheckBox";
            this.trimmedChartLegendCheckBox.Size = new System.Drawing.Size(62, 17);
            this.trimmedChartLegendCheckBox.TabIndex = 0;
            this.trimmedChartLegendCheckBox.Tag = "trimmedChart";
            this.trimmedChartLegendCheckBox.Text = "Legend";
            this.trimmedChartLegendCheckBox.UseVisualStyleBackColor = true;
            this.trimmedChartLegendCheckBox.CheckedChanged += new System.EventHandler(this.DetailedChartLegendCheckBox_CheckedChanged);
            // 
            // trimmedChartIndividualCharts
            // 
            this.trimmedChartIndividualCharts.AutoSize = true;
            this.trimmedChartIndividualCharts.Location = new System.Drawing.Point(427, 3);
            this.trimmedChartIndividualCharts.Name = "trimmedChartIndividualCharts";
            this.trimmedChartIndividualCharts.Size = new System.Drawing.Size(104, 17);
            this.trimmedChartIndividualCharts.TabIndex = 4;
            this.trimmedChartIndividualCharts.Tag = "trimmedChart";
            this.trimmedChartIndividualCharts.Text = "Individual Charts";
            this.trimmedChartIndividualCharts.UseVisualStyleBackColor = true;
            this.trimmedChartIndividualCharts.Visible = false;
            this.trimmedChartIndividualCharts.CheckedChanged += new System.EventHandler(this.IndividualCharts_CheckedChanged);
            // 
            // trimmedChart_Limits
            // 
            this.trimmedChart_Limits.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedChart_Limits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.trimmedChart_Limits.FormattingEnabled = true;
            this.trimmedChart_Limits.Location = new System.Drawing.Point(327, 3);
            this.trimmedChart_Limits.Name = "trimmedChart_Limits";
            this.trimmedChart_Limits.Size = new System.Drawing.Size(94, 21);
            this.trimmedChart_Limits.TabIndex = 12;
            // 
            // trimmedChart_LimitsLabel
            // 
            this.trimmedChart_LimitsLabel.AutoSize = true;
            this.trimmedChart_LimitsLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedChart_LimitsLabel.Location = new System.Drawing.Point(288, 0);
            this.trimmedChart_LimitsLabel.Name = "trimmedChart_LimitsLabel";
            this.trimmedChart_LimitsLabel.Size = new System.Drawing.Size(33, 23);
            this.trimmedChart_LimitsLabel.TabIndex = 13;
            this.trimmedChart_LimitsLabel.Text = "Limits";
            this.trimmedChart_LimitsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // trimmedSplitContainer
            // 
            this.trimmedSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedSplitContainer.Location = new System.Drawing.Point(133, 26);
            this.trimmedSplitContainer.Name = "trimmedSplitContainer";
            this.trimmedSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // trimmedSplitContainer.Panel1
            // 
            this.trimmedSplitContainer.Panel1.Controls.Add(this.trimmedChartPanel);
            // 
            // trimmedSplitContainer.Panel2
            // 
            this.trimmedSplitContainer.Panel2.Controls.Add(this.panel5);
            this.trimmedSplitContainer.Panel2MinSize = 0;
            this.tableLayoutPanel22.SetRowSpan(this.trimmedSplitContainer, 8);
            this.trimmedSplitContainer.Size = new System.Drawing.Size(638, 528);
            this.trimmedSplitContainer.SplitterDistance = 427;
            this.trimmedSplitContainer.TabIndex = 32;
            // 
            // trimmedChartPanel
            // 
            this.trimmedChartPanel.Controls.Add(this.trimmedCharts);
            this.trimmedChartPanel.Controls.Add(this.trimmedChart);
            this.trimmedChartPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedChartPanel.Location = new System.Drawing.Point(0, 0);
            this.trimmedChartPanel.Margin = new System.Windows.Forms.Padding(0);
            this.trimmedChartPanel.Name = "trimmedChartPanel";
            this.trimmedChartPanel.Size = new System.Drawing.Size(638, 427);
            this.trimmedChartPanel.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.trimmedChartIndividualChartsLegend);
            this.panel5.Controls.Add(this.trimmedChartedDataGridView);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(638, 97);
            this.panel5.TabIndex = 27;
            // 
            // trimmedChartedDataGridView
            // 
            this.trimmedChartedDataGridView.AllowUserToAddRows = false;
            this.trimmedChartedDataGridView.AllowUserToDeleteRows = false;
            this.trimmedChartedDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trimmedChartedDataGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            this.trimmedChartedDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.trimmedChartedDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle33;
            this.trimmedChartedDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle34.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.trimmedChartedDataGridView.DefaultCellStyle = dataGridViewCellStyle34;
            this.trimmedChartedDataGridView.Location = new System.Drawing.Point(0, 0);
            this.trimmedChartedDataGridView.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.trimmedChartedDataGridView.Name = "trimmedChartedDataGridView";
            this.trimmedChartedDataGridView.ReadOnly = true;
            this.trimmedChartedDataGridView.Size = new System.Drawing.Size(638, 97);
            this.trimmedChartedDataGridView.TabIndex = 24;
            // 
            // tableLayoutPanel40
            // 
            this.tableLayoutPanel40.ColumnCount = 1;
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel40.Controls.Add(this.trimmedMeanToggle, 0, 0);
            this.tableLayoutPanel40.Controls.Add(this.trimmedMedianToggle, 0, 1);
            this.tableLayoutPanel40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel40.Location = new System.Drawing.Point(3, 403);
            this.tableLayoutPanel40.Name = "tableLayoutPanel40";
            this.tableLayoutPanel40.RowCount = 3;
            this.tableLayoutPanel22.SetRowSpan(this.tableLayoutPanel40, 2);
            this.tableLayoutPanel40.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel40.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel40.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel40.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel40.Size = new System.Drawing.Size(124, 119);
            this.tableLayoutPanel40.TabIndex = 33;
            // 
            // trimmedMeanToggle
            // 
            this.trimmedMeanToggle.AutoSize = true;
            this.trimmedMeanToggle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedMeanToggle.Location = new System.Drawing.Point(3, 3);
            this.trimmedMeanToggle.Name = "trimmedMeanToggle";
            this.trimmedMeanToggle.Size = new System.Drawing.Size(118, 17);
            this.trimmedMeanToggle.TabIndex = 0;
            this.trimmedMeanToggle.Text = "Mean Error Curve";
            this.trimmedMeanToggle.UseVisualStyleBackColor = true;
            this.trimmedMeanToggle.CheckedChanged += new System.EventHandler(this.trimmedMeanToggle_CheckedChanged);
            // 
            // trimmedMedianToggle
            // 
            this.trimmedMedianToggle.AutoSize = true;
            this.trimmedMedianToggle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedMedianToggle.Location = new System.Drawing.Point(3, 26);
            this.trimmedMedianToggle.Name = "trimmedMedianToggle";
            this.trimmedMedianToggle.Size = new System.Drawing.Size(118, 17);
            this.trimmedMedianToggle.TabIndex = 1;
            this.trimmedMedianToggle.Text = "Median Error Curve";
            this.trimmedMedianToggle.UseVisualStyleBackColor = true;
            this.trimmedMedianToggle.CheckedChanged += new System.EventHandler(this.trimmedMedianToggle_CheckedChanged);
            // 
            // trimmedLogTabPage
            // 
            this.trimmedLogTabPage.Controls.Add(this.tableLayoutPanel23);
            this.trimmedLogTabPage.Location = new System.Drawing.Point(4, 22);
            this.trimmedLogTabPage.Name = "trimmedLogTabPage";
            this.trimmedLogTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.trimmedLogTabPage.Size = new System.Drawing.Size(780, 563);
            this.trimmedLogTabPage.TabIndex = 9;
            this.trimmedLogTabPage.Tag = "trimmedLogPlotChart";
            this.trimmedLogTabPage.Text = "Trimmed Log Plot";
            this.trimmedLogTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.ColumnCount = 2;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel23.Controls.Add(this.tableLayoutPanel32, 1, 0);
            this.tableLayoutPanel23.Controls.Add(this.label56, 0, 0);
            this.tableLayoutPanel23.Controls.Add(this.trimmedShowFuelAsErrorCheckBox, 0, 6);
            this.tableLayoutPanel23.Controls.Add(this.trimmedLogPlotPressureLevelsListView, 0, 1);
            this.tableLayoutPanel23.Controls.Add(this.trimmedLogPlotStatisticsListView, 0, 5);
            this.tableLayoutPanel23.Controls.Add(this.label57, 0, 4);
            this.tableLayoutPanel23.Controls.Add(this.trimmedLogPlotUpdateChartButton, 0, 8);
            this.tableLayoutPanel23.Controls.Add(this.label58, 0, 2);
            this.tableLayoutPanel23.Controls.Add(this.trimmedLogPlotParametersListView, 0, 3);
            this.tableLayoutPanel23.Controls.Add(this.trimmedLogPlotSplitContainer, 1, 1);
            this.tableLayoutPanel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel23.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 9;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(774, 557);
            this.tableLayoutPanel23.TabIndex = 43;
            // 
            // tableLayoutPanel32
            // 
            this.tableLayoutPanel32.ColumnCount = 6;
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel32.Controls.Add(this.trimmedLogPlotChartLegendCheckBox, 5, 0);
            this.tableLayoutPanel32.Controls.Add(this.copyButton10, 2, 0);
            this.tableLayoutPanel32.Controls.Add(this.excelButton10, 1, 0);
            this.tableLayoutPanel32.Controls.Add(this.powerPointButton10, 0, 0);
            this.tableLayoutPanel32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel32.Location = new System.Drawing.Point(130, 0);
            this.tableLayoutPanel32.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel32.Name = "tableLayoutPanel32";
            this.tableLayoutPanel32.RowCount = 1;
            this.tableLayoutPanel32.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel32.Size = new System.Drawing.Size(644, 23);
            this.tableLayoutPanel32.TabIndex = 42;
            // 
            // trimmedLogPlotChartLegendCheckBox
            // 
            this.trimmedLogPlotChartLegendCheckBox.AutoSize = true;
            this.trimmedLogPlotChartLegendCheckBox.Checked = true;
            this.trimmedLogPlotChartLegendCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.trimmedLogPlotChartLegendCheckBox.Location = new System.Drawing.Point(577, 3);
            this.trimmedLogPlotChartLegendCheckBox.Name = "trimmedLogPlotChartLegendCheckBox";
            this.trimmedLogPlotChartLegendCheckBox.Size = new System.Drawing.Size(62, 17);
            this.trimmedLogPlotChartLegendCheckBox.TabIndex = 5;
            this.trimmedLogPlotChartLegendCheckBox.Tag = "trimmedLogPlotChart";
            this.trimmedLogPlotChartLegendCheckBox.Text = "Legend";
            this.trimmedLogPlotChartLegendCheckBox.UseVisualStyleBackColor = true;
            this.trimmedLogPlotChartLegendCheckBox.CheckedChanged += new System.EventHandler(this.DetailedChartLegendCheckBox_CheckedChanged);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label56.Location = new System.Drawing.Point(3, 10);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(124, 13);
            this.label56.TabIndex = 38;
            this.label56.Text = "Pressure Level(s)";
            // 
            // trimmedShowFuelAsErrorCheckBox
            // 
            this.trimmedShowFuelAsErrorCheckBox.AutoSize = true;
            this.trimmedShowFuelAsErrorCheckBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.trimmedShowFuelAsErrorCheckBox.Location = new System.Drawing.Point(3, 437);
            this.trimmedShowFuelAsErrorCheckBox.Name = "trimmedShowFuelAsErrorCheckBox";
            this.trimmedShowFuelAsErrorCheckBox.Size = new System.Drawing.Size(124, 17);
            this.trimmedShowFuelAsErrorCheckBox.TabIndex = 41;
            this.trimmedShowFuelAsErrorCheckBox.Text = "Show fuel as error";
            this.trimmedShowFuelAsErrorCheckBox.UseVisualStyleBackColor = true;
            // 
            // trimmedLogPlotPressureLevelsListView
            // 
            this.trimmedLogPlotPressureLevelsListView.CheckBoxes = true;
            this.trimmedLogPlotPressureLevelsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedLogPlotPressureLevelsListView.HideSelection = false;
            this.trimmedLogPlotPressureLevelsListView.Location = new System.Drawing.Point(3, 26);
            this.trimmedLogPlotPressureLevelsListView.Name = "trimmedLogPlotPressureLevelsListView";
            this.trimmedLogPlotPressureLevelsListView.Size = new System.Drawing.Size(124, 127);
            this.trimmedLogPlotPressureLevelsListView.TabIndex = 37;
            this.trimmedLogPlotPressureLevelsListView.Tag = "trimmedLog";
            this.trimmedLogPlotPressureLevelsListView.UseCompatibleStateImageBehavior = false;
            this.trimmedLogPlotPressureLevelsListView.View = System.Windows.Forms.View.List;
            this.trimmedLogPlotPressureLevelsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.PressureLevelsListView_CheckedChanged);
            // 
            // trimmedLogPlotStatisticsListView
            // 
            this.trimmedLogPlotStatisticsListView.CheckBoxes = true;
            this.trimmedLogPlotStatisticsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedLogPlotStatisticsListView.HideSelection = false;
            listViewItem37.StateImageIndex = 0;
            this.trimmedLogPlotStatisticsListView.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem37});
            this.trimmedLogPlotStatisticsListView.Location = new System.Drawing.Point(3, 332);
            this.trimmedLogPlotStatisticsListView.Name = "trimmedLogPlotStatisticsListView";
            this.trimmedLogPlotStatisticsListView.Scrollable = false;
            this.trimmedLogPlotStatisticsListView.Size = new System.Drawing.Size(124, 99);
            this.trimmedLogPlotStatisticsListView.TabIndex = 39;
            this.trimmedLogPlotStatisticsListView.UseCompatibleStateImageBehavior = false;
            this.trimmedLogPlotStatisticsListView.View = System.Windows.Forms.View.List;
            this.trimmedLogPlotStatisticsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.StatisticsListView_CheckChanged);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label57.Location = new System.Drawing.Point(3, 316);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(124, 13);
            this.label57.TabIndex = 40;
            this.label57.Text = "Statisitic(s)";
            // 
            // trimmedLogPlotUpdateChartButton
            // 
            this.trimmedLogPlotUpdateChartButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedLogPlotUpdateChartButton.Location = new System.Drawing.Point(3, 528);
            this.trimmedLogPlotUpdateChartButton.Name = "trimmedLogPlotUpdateChartButton";
            this.trimmedLogPlotUpdateChartButton.Size = new System.Drawing.Size(124, 26);
            this.trimmedLogPlotUpdateChartButton.TabIndex = 33;
            this.trimmedLogPlotUpdateChartButton.Text = "Update chart";
            this.trimmedLogPlotUpdateChartButton.UseVisualStyleBackColor = true;
            this.trimmedLogPlotUpdateChartButton.Click += new System.EventHandler(this.trimmedLogPlotUpdateChartButton_Click);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label58.Location = new System.Drawing.Point(3, 163);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(124, 13);
            this.label58.TabIndex = 35;
            this.label58.Text = "Parameter(s)";
            // 
            // trimmedLogPlotParametersListView
            // 
            this.trimmedLogPlotParametersListView.CheckBoxes = true;
            this.trimmedLogPlotParametersListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedLogPlotParametersListView.HideSelection = false;
            this.trimmedLogPlotParametersListView.Location = new System.Drawing.Point(3, 179);
            this.trimmedLogPlotParametersListView.Name = "trimmedLogPlotParametersListView";
            this.trimmedLogPlotParametersListView.Size = new System.Drawing.Size(124, 127);
            this.trimmedLogPlotParametersListView.TabIndex = 32;
            this.trimmedLogPlotParametersListView.UseCompatibleStateImageBehavior = false;
            this.trimmedLogPlotParametersListView.View = System.Windows.Forms.View.List;
            this.trimmedLogPlotParametersListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.ParametersListView_CheckChanged);
            // 
            // trimmedLogPlotSplitContainer
            // 
            this.trimmedLogPlotSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedLogPlotSplitContainer.Location = new System.Drawing.Point(133, 26);
            this.trimmedLogPlotSplitContainer.Name = "trimmedLogPlotSplitContainer";
            this.trimmedLogPlotSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // trimmedLogPlotSplitContainer.Panel1
            // 
            this.trimmedLogPlotSplitContainer.Panel1.Controls.Add(this.trimmedLogPlotChart);
            // 
            // trimmedLogPlotSplitContainer.Panel2
            // 
            this.trimmedLogPlotSplitContainer.Panel2.Controls.Add(this.trimmedLogPlotChartedDataGridView);
            this.trimmedLogPlotSplitContainer.Panel2MinSize = 0;
            this.tableLayoutPanel23.SetRowSpan(this.trimmedLogPlotSplitContainer, 8);
            this.trimmedLogPlotSplitContainer.Size = new System.Drawing.Size(638, 528);
            this.trimmedLogPlotSplitContainer.SplitterDistance = 427;
            this.trimmedLogPlotSplitContainer.TabIndex = 44;
            // 
            // trimmedLogPlotChartedDataGridView
            // 
            this.trimmedLogPlotChartedDataGridView.AllowUserToAddRows = false;
            this.trimmedLogPlotChartedDataGridView.AllowUserToDeleteRows = false;
            this.trimmedLogPlotChartedDataGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            this.trimmedLogPlotChartedDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.trimmedLogPlotChartedDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle35;
            this.trimmedLogPlotChartedDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle36.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle36.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.trimmedLogPlotChartedDataGridView.DefaultCellStyle = dataGridViewCellStyle36;
            this.trimmedLogPlotChartedDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedLogPlotChartedDataGridView.Location = new System.Drawing.Point(0, 0);
            this.trimmedLogPlotChartedDataGridView.Name = "trimmedLogPlotChartedDataGridView";
            this.trimmedLogPlotChartedDataGridView.ReadOnly = true;
            this.trimmedLogPlotChartedDataGridView.Size = new System.Drawing.Size(638, 97);
            this.trimmedLogPlotChartedDataGridView.TabIndex = 34;
            // 
            // untrimmedTabPage
            // 
            this.untrimmedTabPage.Controls.Add(this.tableLayoutPanel37);
            this.untrimmedTabPage.Location = new System.Drawing.Point(4, 22);
            this.untrimmedTabPage.Name = "untrimmedTabPage";
            this.untrimmedTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.untrimmedTabPage.Size = new System.Drawing.Size(780, 563);
            this.untrimmedTabPage.TabIndex = 10;
            this.untrimmedTabPage.Text = "Untrimmed";
            this.untrimmedTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel37
            // 
            this.tableLayoutPanel37.ColumnCount = 2;
            this.tableLayoutPanel37.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel37.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel37.Controls.Add(this.label61, 0, 0);
            this.tableLayoutPanel37.Controls.Add(this.utPressureLevelsListView, 0, 1);
            this.tableLayoutPanel37.Controls.Add(this.checkBox1, 1, 0);
            this.tableLayoutPanel37.Controls.Add(this.utChartUpdateButton, 0, 2);
            this.tableLayoutPanel37.Controls.Add(this.utChartedDataGridView, 1, 3);
            this.tableLayoutPanel37.Controls.Add(this.utChart, 1, 1);
            this.tableLayoutPanel37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel37.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel37.Name = "tableLayoutPanel37";
            this.tableLayoutPanel37.RowCount = 4;
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 29.66418F));
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.26865F));
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.06716F));
            this.tableLayoutPanel37.Size = new System.Drawing.Size(774, 557);
            this.tableLayoutPanel37.TabIndex = 0;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label61.Location = new System.Drawing.Point(3, 10);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(124, 13);
            this.label61.TabIndex = 0;
            this.label61.Text = "Pressure Level(s)";
            // 
            // utPressureLevelsListView
            // 
            this.utPressureLevelsListView.CheckBoxes = true;
            this.utPressureLevelsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.utPressureLevelsListView.HideSelection = false;
            this.utPressureLevelsListView.Location = new System.Drawing.Point(3, 26);
            this.utPressureLevelsListView.Name = "utPressureLevelsListView";
            this.utPressureLevelsListView.Size = new System.Drawing.Size(124, 152);
            this.utPressureLevelsListView.TabIndex = 1;
            this.utPressureLevelsListView.UseCompatibleStateImageBehavior = false;
            this.utPressureLevelsListView.View = System.Windows.Forms.View.List;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBox1.Location = new System.Drawing.Point(709, 3);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(62, 17);
            this.checkBox1.TabIndex = 2;
            this.checkBox1.Text = "Legend";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // utChartUpdateButton
            // 
            this.utChartUpdateButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.utChartUpdateButton.Location = new System.Drawing.Point(3, 184);
            this.utChartUpdateButton.Name = "utChartUpdateButton";
            this.utChartUpdateButton.Size = new System.Drawing.Size(124, 23);
            this.utChartUpdateButton.TabIndex = 3;
            this.utChartUpdateButton.Text = "Update Chart";
            this.utChartUpdateButton.UseVisualStyleBackColor = true;
            this.utChartUpdateButton.Click += new System.EventHandler(this.utChartUpdateButton_Click);
            // 
            // utChartedDataGridView
            // 
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle37.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle37.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.utChartedDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle37;
            this.utChartedDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.utChartedDataGridView.DefaultCellStyle = dataGridViewCellStyle38;
            this.utChartedDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.utChartedDataGridView.Location = new System.Drawing.Point(133, 431);
            this.utChartedDataGridView.Name = "utChartedDataGridView";
            this.utChartedDataGridView.Size = new System.Drawing.Size(638, 123);
            this.utChartedDataGridView.TabIndex = 4;
            // 
            // groupDataByGroupBox
            // 
            this.groupDataByGroupBox.Controls.Add(this.rigGroupRadioButton);
            this.groupDataByGroupBox.Controls.Add(this.lineGroupRadioButton);
            this.groupDataByGroupBox.Controls.Add(this.identGroupRadioButton);
            this.groupDataByGroupBox.Controls.Add(this.utidGroupRadioButton);
            this.groupDataByGroupBox.Controls.Add(this.testplanGroupRadioButton);
            this.groupDataByGroupBox.Controls.Add(this.isnGroupRadioButton);
            this.groupDataByGroupBox.Controls.Add(this.lopGroupRadioButton);
            this.groupDataByGroupBox.Controls.Add(this.passFailGroupRadioButton);
            this.groupDataByGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupDataByGroupBox.Location = new System.Drawing.Point(3, 458);
            this.groupDataByGroupBox.Name = "groupDataByGroupBox";
            this.groupDataByGroupBox.Size = new System.Drawing.Size(239, 134);
            this.groupDataByGroupBox.TabIndex = 23;
            this.groupDataByGroupBox.TabStop = false;
            this.groupDataByGroupBox.Text = "Group data by:";
            // 
            // rigGroupRadioButton
            // 
            this.rigGroupRadioButton.AutoSize = true;
            this.rigGroupRadioButton.Location = new System.Drawing.Point(144, 66);
            this.rigGroupRadioButton.Name = "rigGroupRadioButton";
            this.rigGroupRadioButton.Size = new System.Drawing.Size(41, 17);
            this.rigGroupRadioButton.TabIndex = 6;
            this.rigGroupRadioButton.TabStop = true;
            this.rigGroupRadioButton.Text = "Rig";
            this.rigGroupRadioButton.UseVisualStyleBackColor = true;
            this.rigGroupRadioButton.CheckedChanged += new System.EventHandler(this.GroupDataBy_CheckedChanged);
            // 
            // lineGroupRadioButton
            // 
            this.lineGroupRadioButton.AutoSize = true;
            this.lineGroupRadioButton.Location = new System.Drawing.Point(144, 43);
            this.lineGroupRadioButton.Name = "lineGroupRadioButton";
            this.lineGroupRadioButton.Size = new System.Drawing.Size(45, 17);
            this.lineGroupRadioButton.TabIndex = 6;
            this.lineGroupRadioButton.TabStop = true;
            this.lineGroupRadioButton.Text = "Line";
            this.lineGroupRadioButton.UseVisualStyleBackColor = true;
            this.lineGroupRadioButton.CheckedChanged += new System.EventHandler(this.GroupDataBy_CheckedChanged);
            // 
            // identGroupRadioButton
            // 
            this.identGroupRadioButton.AutoSize = true;
            this.identGroupRadioButton.Location = new System.Drawing.Point(144, 20);
            this.identGroupRadioButton.Name = "identGroupRadioButton";
            this.identGroupRadioButton.Size = new System.Drawing.Size(58, 17);
            this.identGroupRadioButton.TabIndex = 5;
            this.identGroupRadioButton.Text = "IDENT";
            this.identGroupRadioButton.UseVisualStyleBackColor = true;
            this.identGroupRadioButton.CheckedChanged += new System.EventHandler(this.GroupDataBy_CheckedChanged);
            // 
            // utidGroupRadioButton
            // 
            this.utidGroupRadioButton.AutoSize = true;
            this.utidGroupRadioButton.Location = new System.Drawing.Point(6, 112);
            this.utidGroupRadioButton.Name = "utidGroupRadioButton";
            this.utidGroupRadioButton.Size = new System.Drawing.Size(51, 17);
            this.utidGroupRadioButton.TabIndex = 4;
            this.utidGroupRadioButton.Text = "UTID";
            this.utidGroupRadioButton.UseVisualStyleBackColor = true;
            this.utidGroupRadioButton.CheckedChanged += new System.EventHandler(this.GroupDataBy_CheckedChanged);
            // 
            // testplanGroupRadioButton
            // 
            this.testplanGroupRadioButton.AutoSize = true;
            this.testplanGroupRadioButton.Location = new System.Drawing.Point(6, 89);
            this.testplanGroupRadioButton.Name = "testplanGroupRadioButton";
            this.testplanGroupRadioButton.Size = new System.Drawing.Size(66, 17);
            this.testplanGroupRadioButton.TabIndex = 3;
            this.testplanGroupRadioButton.Text = "Testplan";
            this.testplanGroupRadioButton.UseVisualStyleBackColor = true;
            this.testplanGroupRadioButton.CheckedChanged += new System.EventHandler(this.GroupDataBy_CheckedChanged);
            // 
            // isnGroupRadioButton
            // 
            this.isnGroupRadioButton.AutoSize = true;
            this.isnGroupRadioButton.Location = new System.Drawing.Point(6, 66);
            this.isnGroupRadioButton.Name = "isnGroupRadioButton";
            this.isnGroupRadioButton.Size = new System.Drawing.Size(43, 17);
            this.isnGroupRadioButton.TabIndex = 2;
            this.isnGroupRadioButton.Text = "ISN";
            this.isnGroupRadioButton.UseVisualStyleBackColor = true;
            this.isnGroupRadioButton.CheckedChanged += new System.EventHandler(this.GroupDataBy_CheckedChanged);
            // 
            // lopGroupRadioButton
            // 
            this.lopGroupRadioButton.AutoSize = true;
            this.lopGroupRadioButton.Location = new System.Drawing.Point(6, 43);
            this.lopGroupRadioButton.Name = "lopGroupRadioButton";
            this.lopGroupRadioButton.Size = new System.Drawing.Size(46, 17);
            this.lopGroupRadioButton.TabIndex = 1;
            this.lopGroupRadioButton.Text = "LOP";
            this.lopGroupRadioButton.UseVisualStyleBackColor = true;
            this.lopGroupRadioButton.CheckedChanged += new System.EventHandler(this.GroupDataBy_CheckedChanged);
            // 
            // passFailGroupRadioButton
            // 
            this.passFailGroupRadioButton.AutoSize = true;
            this.passFailGroupRadioButton.Checked = true;
            this.passFailGroupRadioButton.Location = new System.Drawing.Point(7, 20);
            this.passFailGroupRadioButton.Name = "passFailGroupRadioButton";
            this.passFailGroupRadioButton.Size = new System.Drawing.Size(112, 17);
            this.passFailGroupRadioButton.TabIndex = 0;
            this.passFailGroupRadioButton.TabStop = true;
            this.passFailGroupRadioButton.Text = "Pass/Fail (Default)";
            this.passFailGroupRadioButton.UseVisualStyleBackColor = true;
            this.passFailGroupRadioButton.CheckedChanged += new System.EventHandler(this.GroupDataBy_CheckedChanged);
            // 
            // detailedDataUpdateButton
            // 
            this.detailedDataUpdateButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailedDataUpdateButton.Location = new System.Drawing.Point(3, 28);
            this.detailedDataUpdateButton.Name = "detailedDataUpdateButton";
            this.detailedDataUpdateButton.Size = new System.Drawing.Size(239, 24);
            this.detailedDataUpdateButton.TabIndex = 5;
            this.detailedDataUpdateButton.Text = "Update from selected injectors";
            this.detailedDataUpdateButton.UseVisualStyleBackColor = true;
            this.detailedDataUpdateButton.Click += new System.EventHandler(this.detailedDataUpdateButton_Click);
            // 
            // DetailedUTIDsListView
            // 
            this.DetailedUTIDsListView.CheckBoxes = true;
            this.DetailedUTIDsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colUTID,
            this.colPN,
            this.colISN,
            this.colPDate});
            this.DetailedUTIDsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DetailedUTIDsListView.HideSelection = false;
            this.DetailedUTIDsListView.Location = new System.Drawing.Point(3, 58);
            this.DetailedUTIDsListView.Name = "DetailedUTIDsListView";
            this.DetailedUTIDsListView.Size = new System.Drawing.Size(239, 369);
            this.DetailedUTIDsListView.TabIndex = 13;
            this.DetailedUTIDsListView.UseCompatibleStateImageBehavior = false;
            this.DetailedUTIDsListView.View = System.Windows.Forms.View.Details;
            this.DetailedUTIDsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.DetailedUTIDListView_CheckedChanged);
            this.DetailedUTIDsListView.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ListView_CtrlAHandler);
            // 
            // colUTID
            // 
            this.colUTID.Text = "UTID";
            // 
            // colPN
            // 
            this.colPN.Text = "PN";
            this.colPN.Width = 55;
            // 
            // colISN
            // 
            this.colISN.Text = "ISN";
            this.colISN.Width = 75;
            // 
            // colPDate
            // 
            this.colPDate.Text = "PDate";
            this.colPDate.Width = 109;
            // 
            // AutoUpdateDetailedChartsCheckBox
            // 
            this.AutoUpdateDetailedChartsCheckBox.AutoSize = true;
            this.AutoUpdateDetailedChartsCheckBox.Location = new System.Drawing.Point(3, 433);
            this.AutoUpdateDetailedChartsCheckBox.Name = "AutoUpdateDetailedChartsCheckBox";
            this.AutoUpdateDetailedChartsCheckBox.Size = new System.Drawing.Size(119, 17);
            this.AutoUpdateDetailedChartsCheckBox.TabIndex = 29;
            this.AutoUpdateDetailedChartsCheckBox.Text = "Auto Update Charts";
            this.AutoUpdateDetailedChartsCheckBox.UseVisualStyleBackColor = true;
            this.AutoUpdateDetailedChartsCheckBox.CheckedChanged += new System.EventHandler(this.AutoUpdateDetailedChartsCheckBox_CheckedChanged);
            // 
            // genericTrendChartTabPage
            // 
            this.genericTrendChartTabPage.Controls.Add(this.tableLayoutPanel16);
            this.genericTrendChartTabPage.Location = new System.Drawing.Point(4, 22);
            this.genericTrendChartTabPage.Name = "genericTrendChartTabPage";
            this.genericTrendChartTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.genericTrendChartTabPage.Size = new System.Drawing.Size(1045, 601);
            this.genericTrendChartTabPage.TabIndex = 6;
            this.genericTrendChartTabPage.Text = "Generic trend chart";
            this.genericTrendChartTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 2;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 225F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Controls.Add(this.groupBox2, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.genericTrendChart, 1, 0);
            this.tableLayoutPanel16.Controls.Add(this.groupBox3, 0, 1);
            this.tableLayoutPanel16.Controls.Add(this.genericTrendChartTreeView, 0, 3);
            this.tableLayoutPanel16.Controls.Add(this.label29, 0, 2);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 4;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(1039, 595);
            this.tableLayoutPanel16.TabIndex = 22;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.genericTrendPumpingRadioButton);
            this.groupBox2.Controls.Add(this.genericTrendCalibrationRadioButton);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(219, 44);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Test type";
            // 
            // genericTrendPumpingRadioButton
            // 
            this.genericTrendPumpingRadioButton.AutoSize = true;
            this.genericTrendPumpingRadioButton.Location = new System.Drawing.Point(120, 20);
            this.genericTrendPumpingRadioButton.Name = "genericTrendPumpingRadioButton";
            this.genericTrendPumpingRadioButton.Size = new System.Drawing.Size(66, 17);
            this.genericTrendPumpingRadioButton.TabIndex = 1;
            this.genericTrendPumpingRadioButton.Text = "Pumping";
            this.genericTrendPumpingRadioButton.UseVisualStyleBackColor = true;
            this.genericTrendPumpingRadioButton.CheckedChanged += new System.EventHandler(this.rigAlignmentOrtrendOrXYTestTypeChanged);
            // 
            // genericTrendCalibrationRadioButton
            // 
            this.genericTrendCalibrationRadioButton.AutoSize = true;
            this.genericTrendCalibrationRadioButton.Checked = true;
            this.genericTrendCalibrationRadioButton.Location = new System.Drawing.Point(7, 20);
            this.genericTrendCalibrationRadioButton.Name = "genericTrendCalibrationRadioButton";
            this.genericTrendCalibrationRadioButton.Size = new System.Drawing.Size(74, 17);
            this.genericTrendCalibrationRadioButton.TabIndex = 0;
            this.genericTrendCalibrationRadioButton.TabStop = true;
            this.genericTrendCalibrationRadioButton.Text = "Calibration";
            this.genericTrendCalibrationRadioButton.UseVisualStyleBackColor = true;
            this.genericTrendCalibrationRadioButton.CheckedChanged += new System.EventHandler(this.rigAlignmentOrtrendOrXYTestTypeChanged);
            // 
            // genericTrendChart
            // 
            chartArea22.AxisX.IsStartedFromZero = false;
            chartArea22.AxisX.Title = "Title";
            chartArea22.AxisY.IsStartedFromZero = false;
            chartArea22.Name = "ChartArea1";
            this.genericTrendChart.ChartAreas.Add(chartArea22);
            this.genericTrendChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend17.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Left;
            legend17.Name = "Legend1";
            this.genericTrendChart.Legends.Add(legend17);
            this.genericTrendChart.Location = new System.Drawing.Point(228, 3);
            this.genericTrendChart.Name = "genericTrendChart";
            this.tableLayoutPanel16.SetRowSpan(this.genericTrendChart, 4);
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.genericTrendChart.Series.Add(series1);
            this.genericTrendChart.Size = new System.Drawing.Size(808, 589);
            this.genericTrendChart.TabIndex = 19;
            this.genericTrendChart.Text = "chart12";
            title21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            title21.Name = "Title1";
            title21.Text = "Trend chart - F2 performance test data";
            this.genericTrendChart.Titles.Add(title21);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.genericTrendChronologicalRadioButton);
            this.groupBox3.Controls.Add(this.genericTrendDateTimeRadioButton);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 53);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(219, 44);
            this.groupBox3.TabIndex = 21;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Chart type";
            // 
            // genericTrendChronologicalRadioButton
            // 
            this.genericTrendChronologicalRadioButton.AutoSize = true;
            this.genericTrendChronologicalRadioButton.Location = new System.Drawing.Point(120, 20);
            this.genericTrendChronologicalRadioButton.Name = "genericTrendChronologicalRadioButton";
            this.genericTrendChronologicalRadioButton.Size = new System.Drawing.Size(89, 17);
            this.genericTrendChronologicalRadioButton.TabIndex = 1;
            this.genericTrendChronologicalRadioButton.Text = "Chronological";
            this.genericTrendChronologicalRadioButton.UseVisualStyleBackColor = true;
            // 
            // genericTrendDateTimeRadioButton
            // 
            this.genericTrendDateTimeRadioButton.AutoSize = true;
            this.genericTrendDateTimeRadioButton.Checked = true;
            this.genericTrendDateTimeRadioButton.Location = new System.Drawing.Point(7, 20);
            this.genericTrendDateTimeRadioButton.Name = "genericTrendDateTimeRadioButton";
            this.genericTrendDateTimeRadioButton.Size = new System.Drawing.Size(78, 17);
            this.genericTrendDateTimeRadioButton.TabIndex = 0;
            this.genericTrendDateTimeRadioButton.TabStop = true;
            this.genericTrendDateTimeRadioButton.Text = "Date / time";
            this.genericTrendDateTimeRadioButton.UseVisualStyleBackColor = true;
            // 
            // genericTrendChartTreeView
            // 
            this.genericTrendChartTreeView.CheckBoxes = true;
            this.genericTrendChartTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.genericTrendChartTreeView.Location = new System.Drawing.Point(3, 123);
            this.genericTrendChartTreeView.Name = "genericTrendChartTreeView";
            this.genericTrendChartTreeView.Size = new System.Drawing.Size(219, 469);
            this.genericTrendChartTreeView.TabIndex = 18;
            this.genericTrendChartTreeView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.genericTrendChartTreeView_AfterCheck);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label29.Location = new System.Drawing.Point(3, 107);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(219, 13);
            this.label29.TabIndex = 6;
            this.label29.Text = "Parameter selection (y-axis)";
            // 
            // xyChartTabPage
            // 
            this.xyChartTabPage.Controls.Add(this.tableLayoutPanel17);
            this.xyChartTabPage.Location = new System.Drawing.Point(4, 22);
            this.xyChartTabPage.Name = "xyChartTabPage";
            this.xyChartTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.xyChartTabPage.Size = new System.Drawing.Size(1045, 601);
            this.xyChartTabPage.TabIndex = 7;
            this.xyChartTabPage.Text = "x-y chart";
            this.xyChartTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 2;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 225F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.xyChart, 1, 0);
            this.tableLayoutPanel17.Controls.Add(this.yAxisXYChartTreeView, 0, 4);
            this.tableLayoutPanel17.Controls.Add(this.xAxisXYChartTreeView, 0, 2);
            this.tableLayoutPanel17.Controls.Add(this.label30, 0, 3);
            this.tableLayoutPanel17.Controls.Add(this.label31, 0, 1);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 5;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(1039, 595);
            this.tableLayoutPanel17.TabIndex = 19;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.xyPumpingRadioButton);
            this.groupBox1.Controls.Add(this.xyCalibrationRadioButton);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(219, 44);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Test type";
            // 
            // xyPumpingRadioButton
            // 
            this.xyPumpingRadioButton.AutoSize = true;
            this.xyPumpingRadioButton.Location = new System.Drawing.Point(119, 20);
            this.xyPumpingRadioButton.Name = "xyPumpingRadioButton";
            this.xyPumpingRadioButton.Size = new System.Drawing.Size(66, 17);
            this.xyPumpingRadioButton.TabIndex = 1;
            this.xyPumpingRadioButton.Text = "Pumping";
            this.xyPumpingRadioButton.UseVisualStyleBackColor = true;
            this.xyPumpingRadioButton.CheckedChanged += new System.EventHandler(this.rigAlignmentOrtrendOrXYTestTypeChanged);
            // 
            // xyCalibrationRadioButton
            // 
            this.xyCalibrationRadioButton.AutoSize = true;
            this.xyCalibrationRadioButton.Checked = true;
            this.xyCalibrationRadioButton.Location = new System.Drawing.Point(7, 20);
            this.xyCalibrationRadioButton.Name = "xyCalibrationRadioButton";
            this.xyCalibrationRadioButton.Size = new System.Drawing.Size(74, 17);
            this.xyCalibrationRadioButton.TabIndex = 0;
            this.xyCalibrationRadioButton.TabStop = true;
            this.xyCalibrationRadioButton.Text = "Calibration";
            this.xyCalibrationRadioButton.UseVisualStyleBackColor = true;
            this.xyCalibrationRadioButton.CheckedChanged += new System.EventHandler(this.rigAlignmentOrtrendOrXYTestTypeChanged);
            // 
            // xyChart
            // 
            chartArea23.AxisX.IsStartedFromZero = false;
            chartArea23.AxisX.Title = "Title";
            chartArea23.AxisY.IsStartedFromZero = false;
            chartArea23.Name = "ChartArea1";
            this.xyChart.ChartAreas.Add(chartArea23);
            this.xyChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend18.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Left;
            legend18.Name = "Legend1";
            this.xyChart.Legends.Add(legend18);
            this.xyChart.Location = new System.Drawing.Point(228, 3);
            this.xyChart.Name = "xyChart";
            this.tableLayoutPanel17.SetRowSpan(this.xyChart, 5);
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.xyChart.Series.Add(series2);
            this.xyChart.Size = new System.Drawing.Size(808, 589);
            this.xyChart.TabIndex = 8;
            this.xyChart.Text = "chart12";
            title22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            title22.Name = "Title1";
            title22.Text = "X-Y chart - F2 performance test data";
            this.xyChart.Titles.Add(title22);
            this.xyChart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // yAxisXYChartTreeView
            // 
            this.yAxisXYChartTreeView.CheckBoxes = true;
            this.yAxisXYChartTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.yAxisXYChartTreeView.Enabled = false;
            this.yAxisXYChartTreeView.Location = new System.Drawing.Point(3, 345);
            this.yAxisXYChartTreeView.Name = "yAxisXYChartTreeView";
            this.yAxisXYChartTreeView.Size = new System.Drawing.Size(219, 247);
            this.yAxisXYChartTreeView.TabIndex = 16;
            this.yAxisXYChartTreeView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.yAxisXYChartTreeView_AfterCheck);
            // 
            // xAxisXYChartTreeView
            // 
            this.xAxisXYChartTreeView.CheckBoxes = true;
            this.xAxisXYChartTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xAxisXYChartTreeView.Location = new System.Drawing.Point(3, 73);
            this.xAxisXYChartTreeView.Name = "xAxisXYChartTreeView";
            this.xAxisXYChartTreeView.Size = new System.Drawing.Size(219, 246);
            this.xAxisXYChartTreeView.TabIndex = 17;
            this.xAxisXYChartTreeView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.xAxisXYChartTreeView_AfterCheck);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label30.Location = new System.Drawing.Point(3, 329);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(219, 13);
            this.label30.TabIndex = 10;
            this.label30.Text = "y-axis";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label31.Location = new System.Drawing.Point(3, 57);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(219, 13);
            this.label31.TabIndex = 9;
            this.label31.Text = "x-axis";
            // 
            // trendChartTabPage
            // 
            this.trendChartTabPage.Controls.Add(this.tableLayoutPanel20);
            this.trendChartTabPage.Location = new System.Drawing.Point(4, 22);
            this.trendChartTabPage.Name = "trendChartTabPage";
            this.trendChartTabPage.Size = new System.Drawing.Size(1045, 601);
            this.trendChartTabPage.TabIndex = 10;
            this.trendChartTabPage.Text = "Trend chart";
            this.trendChartTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 2;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel20.Controls.Add(this.trendutidsListView, 0, 0);
            this.tableLayoutPanel20.Controls.Add(this.trendChart, 0, 1);
            this.tableLayoutPanel20.Controls.Add(this.trendChartDataGridView, 1, 0);
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 2;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(1045, 601);
            this.tableLayoutPanel20.TabIndex = 0;
            // 
            // trendutidsListView
            // 
            this.trendutidsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.utidColumnHeader,
            this.pnColumnHeader,
            this.isnColumnHeader,
            this.pdateColumnHeader,
            this.rigtypeColumnHeader,
            this.reasonColumnHeader});
            this.trendutidsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trendutidsListView.FullRowSelect = true;
            this.trendutidsListView.HideSelection = false;
            this.trendutidsListView.Location = new System.Drawing.Point(3, 3);
            this.trendutidsListView.MultiSelect = false;
            this.trendutidsListView.Name = "trendutidsListView";
            this.trendutidsListView.Size = new System.Drawing.Size(739, 194);
            this.trendutidsListView.TabIndex = 14;
            this.trendutidsListView.UseCompatibleStateImageBehavior = false;
            this.trendutidsListView.View = System.Windows.Forms.View.Details;
            this.trendutidsListView.SelectedIndexChanged += new System.EventHandler(this.trendutidsListView_SelectedIndexChanged);
            // 
            // utidColumnHeader
            // 
            this.utidColumnHeader.Text = "UTID";
            // 
            // pnColumnHeader
            // 
            this.pnColumnHeader.Text = "PN";
            // 
            // isnColumnHeader
            // 
            this.isnColumnHeader.Text = "ISN";
            this.isnColumnHeader.Width = 70;
            // 
            // pdateColumnHeader
            // 
            this.pdateColumnHeader.Text = "PDate";
            this.pdateColumnHeader.Width = 120;
            // 
            // rigtypeColumnHeader
            // 
            this.rigtypeColumnHeader.Text = "Rig Type";
            // 
            // reasonColumnHeader
            // 
            this.reasonColumnHeader.Text = "Reason";
            this.reasonColumnHeader.Width = 370;
            // 
            // trendChart
            // 
            chartArea24.AxisX.Title = "Time (s)";
            chartArea24.Name = "ChartArea1";
            this.trendChart.ChartAreas.Add(chartArea24);
            this.tableLayoutPanel20.SetColumnSpan(this.trendChart, 2);
            this.trendChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend19.Name = "legendLineSeries";
            this.trendChart.Legends.Add(legend19);
            this.trendChart.Location = new System.Drawing.Point(3, 203);
            this.trendChart.Name = "trendChart";
            this.trendChart.Size = new System.Drawing.Size(1039, 395);
            this.trendChart.TabIndex = 0;
            this.trendChart.Text = "chart1";
            title23.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            title23.Name = "Title1";
            title23.Text = "Trend chart";
            this.trendChart.Titles.Add(title23);
            this.trendChart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartMouseDblClick);
            // 
            // trendChartDataGridView
            // 
            this.trendChartDataGridView.AllowUserToAddRows = false;
            this.trendChartDataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle39.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle39.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.trendChartDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle39;
            this.trendChartDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle40.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle40.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.trendChartDataGridView.DefaultCellStyle = dataGridViewCellStyle40;
            this.trendChartDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trendChartDataGridView.Location = new System.Drawing.Point(748, 3);
            this.trendChartDataGridView.Name = "trendChartDataGridView";
            this.trendChartDataGridView.ReadOnly = true;
            this.trendChartDataGridView.Size = new System.Drawing.Size(294, 194);
            this.trendChartDataGridView.TabIndex = 15;
            // 
            // InjectorComponentsTabPage
            // 
            this.InjectorComponentsTabPage.Controls.Add(this.tableLayoutPanel36);
            this.InjectorComponentsTabPage.Location = new System.Drawing.Point(4, 22);
            this.InjectorComponentsTabPage.Name = "InjectorComponentsTabPage";
            this.InjectorComponentsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.InjectorComponentsTabPage.Size = new System.Drawing.Size(1045, 601);
            this.InjectorComponentsTabPage.TabIndex = 13;
            this.InjectorComponentsTabPage.Text = "Injector Components";
            this.InjectorComponentsTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel36
            // 
            this.tableLayoutPanel36.ColumnCount = 3;
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 92F));
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel36.Controls.Add(this.InjectorComponentsGridView, 0, 2);
            this.tableLayoutPanel36.Controls.Add(this.excelButton13, 2, 1);
            this.tableLayoutPanel36.Controls.Add(this.UpdateInjectorComponentsButton, 1, 0);
            this.tableLayoutPanel36.Controls.Add(this.label62, 0, 1);
            this.tableLayoutPanel36.Controls.Add(this.tableLayoutPanel39, 0, 0);
            this.tableLayoutPanel36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel36.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel36.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel36.Name = "tableLayoutPanel36";
            this.tableLayoutPanel36.RowCount = 3;
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel36.Size = new System.Drawing.Size(1039, 595);
            this.tableLayoutPanel36.TabIndex = 0;
            // 
            // UpdateInjectorComponentsButton
            // 
            this.tableLayoutPanel36.SetColumnSpan(this.UpdateInjectorComponentsButton, 2);
            this.UpdateInjectorComponentsButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UpdateInjectorComponentsButton.Location = new System.Drawing.Point(926, 3);
            this.UpdateInjectorComponentsButton.Name = "UpdateInjectorComponentsButton";
            this.UpdateInjectorComponentsButton.Size = new System.Drawing.Size(110, 42);
            this.UpdateInjectorComponentsButton.TabIndex = 12;
            this.UpdateInjectorComponentsButton.Text = "Update Table";
            this.UpdateInjectorComponentsButton.UseVisualStyleBackColor = true;
            this.UpdateInjectorComponentsButton.Click += new System.EventHandler(this.UpdateInjectorComponentsDataButton_Click);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label62.Location = new System.Drawing.Point(3, 56);
            this.label62.Margin = new System.Windows.Forms.Padding(3);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(917, 13);
            this.label62.TabIndex = 11;
            this.label62.Text = "Injector Components";
            // 
            // tableLayoutPanel39
            // 
            this.tableLayoutPanel39.ColumnCount = 1;
            this.tableLayoutPanel39.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel39.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel39.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel39.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel39.Controls.Add(this.selectedComponentsLabel, 0, 0);
            this.tableLayoutPanel39.Controls.Add(this.missingComponentsLabel, 0, 1);
            this.tableLayoutPanel39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel39.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel39.Name = "tableLayoutPanel39";
            this.tableLayoutPanel39.RowCount = 2;
            this.tableLayoutPanel39.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.Size = new System.Drawing.Size(917, 42);
            this.tableLayoutPanel39.TabIndex = 14;
            // 
            // selectedComponentsLabel
            // 
            this.selectedComponentsLabel.AutoSize = true;
            this.selectedComponentsLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectedComponentsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.selectedComponentsLabel.Location = new System.Drawing.Point(3, 0);
            this.selectedComponentsLabel.Name = "selectedComponentsLabel";
            this.selectedComponentsLabel.Size = new System.Drawing.Size(911, 21);
            this.selectedComponentsLabel.TabIndex = 0;
            this.selectedComponentsLabel.Text = "{} Selected";
            // 
            // missingComponentsLabel
            // 
            this.missingComponentsLabel.AutoSize = true;
            this.missingComponentsLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.missingComponentsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.missingComponentsLabel.ForeColor = System.Drawing.Color.Red;
            this.missingComponentsLabel.Location = new System.Drawing.Point(3, 26);
            this.missingComponentsLabel.Name = "missingComponentsLabel";
            this.missingComponentsLabel.Size = new System.Drawing.Size(911, 16);
            this.missingComponentsLabel.TabIndex = 2;
            this.missingComponentsLabel.Text = "{} missing";
            this.missingComponentsLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.missingComponentsLabel.Visible = false;
            this.missingComponentsLabel.Click += new System.EventHandler(this.missingComponentsLabel_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lbProcesDataExcel);
            this.tabPage1.Controls.Add(this.dgvProcessData);
            this.tabPage1.Controls.Add(this.tbProcessDataSerial);
            this.tabPage1.Controls.Add(this.btnProcessData);
            this.tabPage1.Controls.Add(this.SerialLabel);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1045, 601);
            this.tabPage1.TabIndex = 14;
            this.tabPage1.Text = "Process Data";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // lbProcesDataExcel
            // 
            this.lbProcesDataExcel.Image = global::SudburyDataAnalyser.Properties.Resources.Excel;
            this.lbProcesDataExcel.Location = new System.Drawing.Point(239, 37);
            this.lbProcesDataExcel.Name = "lbProcesDataExcel";
            this.lbProcesDataExcel.Size = new System.Drawing.Size(23, 23);
            this.lbProcesDataExcel.TabIndex = 13;
            this.lbProcesDataExcel.TabStop = false;
            this.lbProcesDataExcel.Click += new System.EventHandler(this.ProcessDataExcel_Click);
            // 
            // dgvProcessData
            // 
            this.dgvProcessData.AllowUserToAddRows = false;
            this.dgvProcessData.AllowUserToDeleteRows = false;
            this.dgvProcessData.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvProcessData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProcessData.Location = new System.Drawing.Point(52, 81);
            this.dgvProcessData.Name = "dgvProcessData";
            this.dgvProcessData.ReadOnly = true;
            this.dgvProcessData.Size = new System.Drawing.Size(495, 305);
            this.dgvProcessData.TabIndex = 4;
            // 
            // tbProcessDataSerial
            // 
            this.tbProcessDataSerial.Location = new System.Drawing.Point(52, 40);
            this.tbProcessDataSerial.Name = "tbProcessDataSerial";
            this.tbProcessDataSerial.Size = new System.Drawing.Size(100, 20);
            this.tbProcessDataSerial.TabIndex = 2;
            // 
            // btnProcessData
            // 
            this.btnProcessData.Location = new System.Drawing.Point(158, 37);
            this.btnProcessData.Name = "btnProcessData";
            this.btnProcessData.Size = new System.Drawing.Size(75, 23);
            this.btnProcessData.TabIndex = 1;
            this.btnProcessData.Text = "Search";
            this.btnProcessData.UseVisualStyleBackColor = true;
            this.btnProcessData.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnProcessData_MouseClick);
            // 
            // SerialLabel
            // 
            this.SerialLabel.AutoSize = true;
            this.SerialLabel.Location = new System.Drawing.Point(49, 24);
            this.SerialLabel.Name = "SerialLabel";
            this.SerialLabel.Size = new System.Drawing.Size(71, 13);
            this.SerialLabel.TabIndex = 0;
            this.SerialLabel.Text = "Injector Serial";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.lbPackingDataExcel);
            this.tabPage2.Controls.Add(this.lPackingDataOutput);
            this.tabPage2.Controls.Add(this.label64);
            this.tabPage2.Controls.Add(this.tbPackingData);
            this.tabPage2.Controls.Add(this.btnPackingData);
            this.tabPage2.Controls.Add(this.dgvProcessDataPack);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1045, 601);
            this.tabPage2.TabIndex = 15;
            this.tabPage2.Text = "Packing Data";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lbPackingDataExcel
            // 
            this.lbPackingDataExcel.Image = global::SudburyDataAnalyser.Properties.Resources.Excel;
            this.lbPackingDataExcel.Location = new System.Drawing.Point(261, 50);
            this.lbPackingDataExcel.Name = "lbPackingDataExcel";
            this.lbPackingDataExcel.Size = new System.Drawing.Size(23, 23);
            this.lbPackingDataExcel.TabIndex = 12;
            this.lbPackingDataExcel.TabStop = false;
            this.lbPackingDataExcel.Click += new System.EventHandler(this.PackingDataExcel_Click);
            // 
            // lPackingDataOutput
            // 
            this.lPackingDataOutput.AutoSize = true;
            this.lPackingDataOutput.ForeColor = System.Drawing.Color.Red;
            this.lPackingDataOutput.Location = new System.Drawing.Point(167, 134);
            this.lPackingDataOutput.Name = "lPackingDataOutput";
            this.lPackingDataOutput.Size = new System.Drawing.Size(64, 13);
            this.lPackingDataOutput.TabIndex = 11;
            this.lPackingDataOutput.Text = "Error Output";
            this.lPackingDataOutput.Visible = false;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(36, 28);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(92, 13);
            this.label64.TabIndex = 10;
            this.label64.Text = "Serial/SAP/Crate:";
            // 
            // tbPackingData
            // 
            this.tbPackingData.Location = new System.Drawing.Point(36, 52);
            this.tbPackingData.Multiline = true;
            this.tbPackingData.Name = "tbPackingData";
            this.tbPackingData.Size = new System.Drawing.Size(112, 95);
            this.tbPackingData.TabIndex = 9;
            // 
            // btnPackingData
            // 
            this.btnPackingData.Location = new System.Drawing.Point(170, 50);
            this.btnPackingData.Name = "btnPackingData";
            this.btnPackingData.Size = new System.Drawing.Size(75, 23);
            this.btnPackingData.TabIndex = 8;
            this.btnPackingData.Text = "Search";
            this.btnPackingData.UseVisualStyleBackColor = true;
            this.btnPackingData.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnPackingData_MouseClick);
            // 
            // dgvProcessDataPack
            // 
            this.dgvProcessDataPack.AllowUserToAddRows = false;
            this.dgvProcessDataPack.AllowUserToDeleteRows = false;
            this.dgvProcessDataPack.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvProcessDataPack.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProcessDataPack.Location = new System.Drawing.Point(36, 170);
            this.dgvProcessDataPack.Name = "dgvProcessDataPack";
            this.dgvProcessDataPack.ReadOnly = true;
            this.dgvProcessDataPack.Size = new System.Drawing.Size(770, 409);
            this.dgvProcessDataPack.TabIndex = 6;
            // 
            // euro6DataSet
            // 
            this.euro6DataSet.DataSetName = "Euro6DataSet";
            this.euro6DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // queryUpdateTimer
            // 
            this.queryUpdateTimer.Tick += new System.EventHandler(this.queryUpdateTimer_Tick);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnDaimlerISNExcel);
            this.tabPage3.Controls.Add(this.lDaimlerISN);
            this.tabPage3.Controls.Add(this.label66);
            this.tabPage3.Controls.Add(this.btnDaimlerISN);
            this.tabPage3.Controls.Add(this.dgvDaimlerISN);
            this.tabPage3.Controls.Add(this.tbDaimlerISN);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1045, 601);
            this.tabPage3.TabIndex = 16;
            this.tabPage3.Text = "Daimler ISN Checker";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tbDaimlerISN
            // 
            this.tbDaimlerISN.Location = new System.Drawing.Point(59, 43);
            this.tbDaimlerISN.Multiline = true;
            this.tbDaimlerISN.Name = "tbDaimlerISN";
            this.tbDaimlerISN.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbDaimlerISN.Size = new System.Drawing.Size(112, 95);
            this.tbDaimlerISN.TabIndex = 10;
            // 
            // btnDaimlerISNExcel
            // 
            this.btnDaimlerISNExcel.Image = global::SudburyDataAnalyser.Properties.Resources.Excel;
            this.btnDaimlerISNExcel.Location = new System.Drawing.Point(284, 43);
            this.btnDaimlerISNExcel.Name = "btnDaimlerISNExcel";
            this.btnDaimlerISNExcel.Size = new System.Drawing.Size(23, 23);
            this.btnDaimlerISNExcel.TabIndex = 17;
            this.btnDaimlerISNExcel.TabStop = false;
            this.btnDaimlerISNExcel.Click += new System.EventHandler(this.btnDaimlerISNExcel_Click);
            // 
            // lDaimlerISN
            // 
            this.lDaimlerISN.AutoSize = true;
            this.lDaimlerISN.ForeColor = System.Drawing.Color.Red;
            this.lDaimlerISN.Location = new System.Drawing.Point(190, 125);
            this.lDaimlerISN.Name = "lDaimlerISN";
            this.lDaimlerISN.Size = new System.Drawing.Size(64, 13);
            this.lDaimlerISN.TabIndex = 16;
            this.lDaimlerISN.Text = "Error Output";
            this.lDaimlerISN.Visible = false;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(59, 28);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(66, 13);
            this.label66.TabIndex = 15;
            this.label66.Text = "Daimler ISN:";
            // 
            // btnDaimlerISN
            // 
            this.btnDaimlerISN.Location = new System.Drawing.Point(193, 43);
            this.btnDaimlerISN.Name = "btnDaimlerISN";
            this.btnDaimlerISN.Size = new System.Drawing.Size(75, 23);
            this.btnDaimlerISN.TabIndex = 14;
            this.btnDaimlerISN.Text = "Search";
            this.btnDaimlerISN.UseVisualStyleBackColor = true;
            this.btnDaimlerISN.Click += new System.EventHandler(this.btnDaimlerISN_Click);
            // 
            // dgvDaimlerISN
            // 
            this.dgvDaimlerISN.AllowUserToAddRows = false;
            this.dgvDaimlerISN.AllowUserToDeleteRows = false;
            this.dgvDaimlerISN.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvDaimlerISN.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDaimlerISN.Location = new System.Drawing.Point(59, 170);
            this.dgvDaimlerISN.Name = "dgvDaimlerISN";
            this.dgvDaimlerISN.ReadOnly = true;
            this.dgvDaimlerISN.Size = new System.Drawing.Size(770, 409);
            this.dgvDaimlerISN.TabIndex = 13;
            // 
            // dateToExtDateTimePicker
            // 
            this.dateToExtDateTimePicker.CustomFormat = "dd-MMM-yyyy HH:mm:ss";
            this.dateToExtDateTimePicker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateToExtDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateToExtDateTimePicker.Location = new System.Drawing.Point(3, 70);
            this.dateToExtDateTimePicker.MaximumSize = new System.Drawing.Size(140, 20);
            this.dateToExtDateTimePicker.Name = "dateToExtDateTimePicker";
            this.dateToExtDateTimePicker.Size = new System.Drawing.Size(114, 20);
            this.dateToExtDateTimePicker.TabIndex = 43;
            this.dateToExtDateTimePicker.CloseUp += new System.EventHandler(this.dateTimePicker_CloseUp);
            // 
            // dateFromExtDateTimePicker
            // 
            this.dateFromExtDateTimePicker.BackColor = System.Drawing.Color.White;
            this.dateFromExtDateTimePicker.CustomFormat = "dd-MMM-yyyy HH:mm:ss";
            this.dateFromExtDateTimePicker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateFromExtDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateFromExtDateTimePicker.Location = new System.Drawing.Point(3, 28);
            this.dateFromExtDateTimePicker.MaximumSize = new System.Drawing.Size(140, 20);
            this.dateFromExtDateTimePicker.Name = "dateFromExtDateTimePicker";
            this.dateFromExtDateTimePicker.Size = new System.Drawing.Size(114, 20);
            this.dateFromExtDateTimePicker.TabIndex = 42;
            this.dateFromExtDateTimePicker.CloseUp += new System.EventHandler(this.dateTimePicker_CloseUp);
            this.dateFromExtDateTimePicker.Enter += new System.EventHandler(this.dateTimePickers_Enter);
            this.dateFromExtDateTimePicker.Leave += new System.EventHandler(this.dateTimePicker_Leave);
            // 
            // incompletePumpingDataGridView
            // 
            this.incompletePumpingDataGridView.AllowUserToAddRows = false;
            this.incompletePumpingDataGridView.AllowUserToDeleteRows = false;
            this.incompletePumpingDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.incompletePumpingDataGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            this.incompletePumpingDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.incompletePumpingDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.incompletePumpingDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DatasetTableLayoutPanel.SetColumnSpan(this.incompletePumpingDataGridView, 8);
            this.incompletePumpingDataGridView.Cursor = System.Windows.Forms.Cursors.IBeam;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightCoral;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.incompletePumpingDataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.incompletePumpingDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.incompletePumpingDataGridView.Location = new System.Drawing.Point(3, 523);
            this.incompletePumpingDataGridView.Name = "incompletePumpingDataGridView";
            this.incompletePumpingDataGridView.ReadOnly = true;
            this.incompletePumpingDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.incompletePumpingDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.incompletePumpingDataGridView.ShowEditingIcon = false;
            this.incompletePumpingDataGridView.Size = new System.Drawing.Size(1033, 69);
            this.incompletePumpingDataGridView.TabIndex = 10;
            this.incompletePumpingDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.VirtualDataGridView_ColumnHeaderMouseClick);
            // 
            // completedPumpingDataGridView
            // 
            this.completedPumpingDataGridView.AllowUserToAddRows = false;
            this.completedPumpingDataGridView.AllowUserToDeleteRows = false;
            this.completedPumpingDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.completedPumpingDataGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            this.completedPumpingDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.completedPumpingDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.completedPumpingDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DatasetTableLayoutPanel.SetColumnSpan(this.completedPumpingDataGridView, 8);
            this.completedPumpingDataGridView.Cursor = System.Windows.Forms.Cursors.IBeam;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.completedPumpingDataGridView.DefaultCellStyle = dataGridViewCellStyle4;
            this.completedPumpingDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.completedPumpingDataGridView.Location = new System.Drawing.Point(3, 226);
            this.completedPumpingDataGridView.Name = "completedPumpingDataGridView";
            this.completedPumpingDataGridView.ReadOnly = true;
            this.completedPumpingDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.completedPumpingDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.completedPumpingDataGridView.ShowEditingIcon = false;
            this.completedPumpingDataGridView.Size = new System.Drawing.Size(1033, 167);
            this.completedPumpingDataGridView.TabIndex = 9;
            this.completedPumpingDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.VirtualDataGridView_ColumnHeaderMouseClick);
            // 
            // completedCalibrationDataGridView
            // 
            this.completedCalibrationDataGridView.AllowUserToAddRows = false;
            this.completedCalibrationDataGridView.AllowUserToDeleteRows = false;
            this.completedCalibrationDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.completedCalibrationDataGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            this.completedCalibrationDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.completedCalibrationDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.completedCalibrationDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DatasetTableLayoutPanel.SetColumnSpan(this.completedCalibrationDataGridView, 8);
            this.completedCalibrationDataGridView.Cursor = System.Windows.Forms.Cursors.IBeam;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.completedCalibrationDataGridView.DefaultCellStyle = dataGridViewCellStyle6;
            this.completedCalibrationDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.completedCalibrationDataGridView.Location = new System.Drawing.Point(3, 28);
            this.completedCalibrationDataGridView.Name = "completedCalibrationDataGridView";
            this.completedCalibrationDataGridView.ReadOnly = true;
            this.completedCalibrationDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.completedCalibrationDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.completedCalibrationDataGridView.ShowEditingIcon = false;
            this.completedCalibrationDataGridView.Size = new System.Drawing.Size(1033, 167);
            this.completedCalibrationDataGridView.TabIndex = 0;
            this.completedCalibrationDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.VirtualDataGridView_ColumnHeaderMouseClick);
            // 
            // incompleteCalibrationDataGridView
            // 
            this.incompleteCalibrationDataGridView.AllowUserToAddRows = false;
            this.incompleteCalibrationDataGridView.AllowUserToDeleteRows = false;
            this.incompleteCalibrationDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.incompleteCalibrationDataGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            this.incompleteCalibrationDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.incompleteCalibrationDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.incompleteCalibrationDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DatasetTableLayoutPanel.SetColumnSpan(this.incompleteCalibrationDataGridView, 8);
            this.incompleteCalibrationDataGridView.Cursor = System.Windows.Forms.Cursors.IBeam;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.LightCoral;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.incompleteCalibrationDataGridView.DefaultCellStyle = dataGridViewCellStyle8;
            this.incompleteCalibrationDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.incompleteCalibrationDataGridView.Location = new System.Drawing.Point(3, 424);
            this.incompleteCalibrationDataGridView.Name = "incompleteCalibrationDataGridView";
            this.incompleteCalibrationDataGridView.ReadOnly = true;
            this.incompleteCalibrationDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.incompleteCalibrationDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.incompleteCalibrationDataGridView.ShowEditingIcon = false;
            this.incompleteCalibrationDataGridView.Size = new System.Drawing.Size(1033, 68);
            this.incompleteCalibrationDataGridView.TabIndex = 5;
            this.incompleteCalibrationDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.VirtualDataGridView_ColumnHeaderMouseClick);
            // 
            // CompleteCalibrationExcelButton
            // 
            this.CompleteCalibrationExcelButton.Image = ((System.Drawing.Image)(resources.GetObject("CompleteCalibrationExcelButton.Image")));
            this.CompleteCalibrationExcelButton.Location = new System.Drawing.Point(1014, 3);
            this.CompleteCalibrationExcelButton.Name = "CompleteCalibrationExcelButton";
            this.CompleteCalibrationExcelButton.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.CompleteCalibrationExcelButton.Size = new System.Drawing.Size(18, 18);
            this.CompleteCalibrationExcelButton.TabIndex = 12;
            this.CompleteCalibrationExcelButton.TabStop = false;
            this.CompleteCalibrationExcelButton.Tag = "completedCalibrationDataGridView";
            this.CompleteCalibrationExcelButton.Visible = false;
            // 
            // CompletePumpingExcelButton
            // 
            this.CompletePumpingExcelButton.Image = ((System.Drawing.Image)(resources.GetObject("CompletePumpingExcelButton.Image")));
            this.CompletePumpingExcelButton.Location = new System.Drawing.Point(1014, 201);
            this.CompletePumpingExcelButton.Name = "CompletePumpingExcelButton";
            this.CompletePumpingExcelButton.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.CompletePumpingExcelButton.Size = new System.Drawing.Size(18, 18);
            this.CompletePumpingExcelButton.TabIndex = 13;
            this.CompletePumpingExcelButton.TabStop = false;
            this.CompletePumpingExcelButton.Tag = "completedPumpingDataGridView";
            this.CompletePumpingExcelButton.Visible = false;
            // 
            // IncompleteCalibrationExcelButton
            // 
            this.IncompleteCalibrationExcelButton.Image = ((System.Drawing.Image)(resources.GetObject("IncompleteCalibrationExcelButton.Image")));
            this.IncompleteCalibrationExcelButton.Location = new System.Drawing.Point(1014, 399);
            this.IncompleteCalibrationExcelButton.Name = "IncompleteCalibrationExcelButton";
            this.IncompleteCalibrationExcelButton.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.IncompleteCalibrationExcelButton.Size = new System.Drawing.Size(18, 18);
            this.IncompleteCalibrationExcelButton.TabIndex = 14;
            this.IncompleteCalibrationExcelButton.TabStop = false;
            this.IncompleteCalibrationExcelButton.Tag = "incompleteCalibrationDataGridView";
            this.IncompleteCalibrationExcelButton.Visible = false;
            // 
            // IncompletePumpingExcelButton
            // 
            this.IncompletePumpingExcelButton.Image = ((System.Drawing.Image)(resources.GetObject("IncompletePumpingExcelButton.Image")));
            this.IncompletePumpingExcelButton.Location = new System.Drawing.Point(1014, 498);
            this.IncompletePumpingExcelButton.Name = "IncompletePumpingExcelButton";
            this.IncompletePumpingExcelButton.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.IncompletePumpingExcelButton.Size = new System.Drawing.Size(18, 18);
            this.IncompletePumpingExcelButton.TabIndex = 15;
            this.IncompletePumpingExcelButton.TabStop = false;
            this.IncompletePumpingExcelButton.Tag = "incompletePumpingDataGridView";
            this.IncompletePumpingExcelButton.Visible = false;
            // 
            // CompletedCalibrationCount0
            // 
            this.CompletedCalibrationCount0.AutoSize = true;
            this.CompletedCalibrationCount0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CompletedCalibrationCount0.Location = new System.Drawing.Point(975, 4);
            this.CompletedCalibrationCount0.Margin = new System.Windows.Forms.Padding(4);
            this.CompletedCalibrationCount0.Name = "CompletedCalibrationCount0";
            this.CompletedCalibrationCount0.Size = new System.Drawing.Size(32, 17);
            this.CompletedCalibrationCount0.TabIndex = 16;
            this.CompletedCalibrationCount0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CompletedCalibrationCount0.ButtonClick += new System.EventHandler(this.CompletedCalibrationCounts_Click);
            // 
            // CompletedCalibrationCount5
            // 
            this.CompletedCalibrationCount5.AutoSize = true;
            this.CompletedCalibrationCount5.BackColor = System.Drawing.SystemColors.Control;
            this.CompletedCalibrationCount5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CompletedCalibrationCount5.Location = new System.Drawing.Point(775, 4);
            this.CompletedCalibrationCount5.Margin = new System.Windows.Forms.Padding(4);
            this.CompletedCalibrationCount5.Name = "CompletedCalibrationCount5";
            this.CompletedCalibrationCount5.Size = new System.Drawing.Size(32, 17);
            this.CompletedCalibrationCount5.TabIndex = 17;
            this.CompletedCalibrationCount5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CompletedCalibrationCount5.Visible = false;
            this.CompletedCalibrationCount5.ButtonClick += new System.EventHandler(this.CompletedCalibrationCounts_Click);
            // 
            // CompletedCalibrationCount4
            // 
            this.CompletedCalibrationCount4.AutoSize = true;
            this.CompletedCalibrationCount4.BackColor = System.Drawing.SystemColors.Control;
            this.CompletedCalibrationCount4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CompletedCalibrationCount4.Location = new System.Drawing.Point(815, 4);
            this.CompletedCalibrationCount4.Margin = new System.Windows.Forms.Padding(4);
            this.CompletedCalibrationCount4.Name = "CompletedCalibrationCount4";
            this.CompletedCalibrationCount4.Size = new System.Drawing.Size(32, 17);
            this.CompletedCalibrationCount4.TabIndex = 18;
            this.CompletedCalibrationCount4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CompletedCalibrationCount4.Visible = false;
            this.CompletedCalibrationCount4.ButtonClick += new System.EventHandler(this.CompletedCalibrationCounts_Click);
            // 
            // CompletedCalibrationCount1
            // 
            this.CompletedCalibrationCount1.AutoSize = true;
            this.CompletedCalibrationCount1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CompletedCalibrationCount1.Location = new System.Drawing.Point(935, 4);
            this.CompletedCalibrationCount1.Margin = new System.Windows.Forms.Padding(4);
            this.CompletedCalibrationCount1.Name = "CompletedCalibrationCount1";
            this.CompletedCalibrationCount1.Size = new System.Drawing.Size(32, 17);
            this.CompletedCalibrationCount1.TabIndex = 19;
            this.CompletedCalibrationCount1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CompletedCalibrationCount1.ButtonClick += new System.EventHandler(this.CompletedCalibrationCounts_Click);
            // 
            // CompletedCalibrationCount2
            // 
            this.CompletedCalibrationCount2.AutoSize = true;
            this.CompletedCalibrationCount2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CompletedCalibrationCount2.Location = new System.Drawing.Point(895, 4);
            this.CompletedCalibrationCount2.Margin = new System.Windows.Forms.Padding(4);
            this.CompletedCalibrationCount2.Name = "CompletedCalibrationCount2";
            this.CompletedCalibrationCount2.Size = new System.Drawing.Size(32, 17);
            this.CompletedCalibrationCount2.TabIndex = 20;
            this.CompletedCalibrationCount2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CompletedCalibrationCount2.ButtonClick += new System.EventHandler(this.CompletedCalibrationCounts_Click);
            // 
            // CompletedCalibrationCount3
            // 
            this.CompletedCalibrationCount3.AutoSize = true;
            this.CompletedCalibrationCount3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CompletedCalibrationCount3.Location = new System.Drawing.Point(855, 4);
            this.CompletedCalibrationCount3.Margin = new System.Windows.Forms.Padding(4);
            this.CompletedCalibrationCount3.Name = "CompletedCalibrationCount3";
            this.CompletedCalibrationCount3.Size = new System.Drawing.Size(32, 17);
            this.CompletedCalibrationCount3.TabIndex = 21;
            this.CompletedCalibrationCount3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CompletedCalibrationCount3.ButtonClick += new System.EventHandler(this.CompletedCalibrationCounts_Click);
            // 
            // CompletePumpingCount0
            // 
            this.CompletePumpingCount0.AutoSize = true;
            this.CompletePumpingCount0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CompletePumpingCount0.Location = new System.Drawing.Point(975, 202);
            this.CompletePumpingCount0.Margin = new System.Windows.Forms.Padding(4);
            this.CompletePumpingCount0.Name = "CompletePumpingCount0";
            this.CompletePumpingCount0.Size = new System.Drawing.Size(32, 17);
            this.CompletePumpingCount0.TabIndex = 22;
            this.CompletePumpingCount0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CompletePumpingCount0.ButtonClick += new System.EventHandler(this.CompletedPumpingCounts_Click);
            // 
            // CompletePumpingCount1
            // 
            this.CompletePumpingCount1.AutoSize = true;
            this.CompletePumpingCount1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CompletePumpingCount1.Location = new System.Drawing.Point(935, 202);
            this.CompletePumpingCount1.Margin = new System.Windows.Forms.Padding(4);
            this.CompletePumpingCount1.Name = "CompletePumpingCount1";
            this.CompletePumpingCount1.Size = new System.Drawing.Size(32, 17);
            this.CompletePumpingCount1.TabIndex = 23;
            this.CompletePumpingCount1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CompletePumpingCount1.ButtonClick += new System.EventHandler(this.CompletedPumpingCounts_Click);
            // 
            // CompletePumpingCount2
            // 
            this.CompletePumpingCount2.AutoSize = true;
            this.CompletePumpingCount2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CompletePumpingCount2.Location = new System.Drawing.Point(895, 202);
            this.CompletePumpingCount2.Margin = new System.Windows.Forms.Padding(4);
            this.CompletePumpingCount2.Name = "CompletePumpingCount2";
            this.CompletePumpingCount2.Size = new System.Drawing.Size(32, 17);
            this.CompletePumpingCount2.TabIndex = 24;
            this.CompletePumpingCount2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CompletePumpingCount2.ButtonClick += new System.EventHandler(this.CompletedPumpingCounts_Click);
            // 
            // CompletePumpingCount3
            // 
            this.CompletePumpingCount3.AutoSize = true;
            this.CompletePumpingCount3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CompletePumpingCount3.Location = new System.Drawing.Point(855, 202);
            this.CompletePumpingCount3.Margin = new System.Windows.Forms.Padding(4);
            this.CompletePumpingCount3.Name = "CompletePumpingCount3";
            this.CompletePumpingCount3.Size = new System.Drawing.Size(32, 17);
            this.CompletePumpingCount3.TabIndex = 25;
            this.CompletePumpingCount3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CompletePumpingCount3.ButtonClick += new System.EventHandler(this.CompletedPumpingCounts_Click);
            // 
            // CompletePumpingCount4
            // 
            this.CompletePumpingCount4.AutoSize = true;
            this.CompletePumpingCount4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CompletePumpingCount4.Location = new System.Drawing.Point(815, 202);
            this.CompletePumpingCount4.Margin = new System.Windows.Forms.Padding(4);
            this.CompletePumpingCount4.Name = "CompletePumpingCount4";
            this.CompletePumpingCount4.Size = new System.Drawing.Size(32, 17);
            this.CompletePumpingCount4.TabIndex = 26;
            this.CompletePumpingCount4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CompletePumpingCount4.ButtonClick += new System.EventHandler(this.CompletedPumpingCounts_Click);
            // 
            // excelButton11
            // 
            this.excelButton11.Dock = System.Windows.Forms.DockStyle.Right;
            this.excelButton11.Image = ((System.Drawing.Image)(resources.GetObject("excelButton11.Image")));
            this.excelButton11.Location = new System.Drawing.Point(329, 3);
            this.excelButton11.Name = "excelButton11";
            this.excelButton11.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.excelButton11.Size = new System.Drawing.Size(18, 19);
            this.excelButton11.TabIndex = 24;
            this.excelButton11.TabStop = false;
            this.excelButton11.Tag = "passRateGridView";
            // 
            // powerPointButton11
            // 
            this.powerPointButton11.Image = ((System.Drawing.Image)(resources.GetObject("powerPointButton11.Image")));
            this.powerPointButton11.Location = new System.Drawing.Point(639, 0);
            this.powerPointButton11.Margin = new System.Windows.Forms.Padding(0);
            this.powerPointButton11.Name = "powerPointButton11";
            this.powerPointButton11.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.powerPointButton11.Size = new System.Drawing.Size(25, 20);
            this.powerPointButton11.TabIndex = 0;
            this.powerPointButton11.TabStop = false;
            this.powerPointButton11.Tag = "failureParetoChart";
            // 
            // copyButton9
            // 
            this.copyButton9.Image = ((System.Drawing.Image)(resources.GetObject("copyButton9.Image")));
            this.copyButton9.Location = new System.Drawing.Point(664, 0);
            this.copyButton9.Margin = new System.Windows.Forms.Padding(0);
            this.copyButton9.Name = "copyButton9";
            this.copyButton9.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.copyButton9.Size = new System.Drawing.Size(25, 20);
            this.copyButton9.TabIndex = 1;
            this.copyButton9.TabStop = false;
            this.copyButton9.Tag = "failureParetoChart";
            // 
            // excelButton12
            // 
            this.excelButton12.Dock = System.Windows.Forms.DockStyle.Right;
            this.excelButton12.Image = ((System.Drawing.Image)(resources.GetObject("excelButton12.Image")));
            this.excelButton12.Location = new System.Drawing.Point(1018, 442);
            this.excelButton12.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.excelButton12.Name = "excelButton12";
            this.excelButton12.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.excelButton12.Size = new System.Drawing.Size(18, 17);
            this.excelButton12.TabIndex = 33;
            this.excelButton12.TabStop = false;
            this.excelButton12.Tag = "failureFullDataGridView";
            // 
            // copyButton1
            // 
            this.copyButton1.Image = ((System.Drawing.Image)(resources.GetObject("copyButton1.Image")));
            this.copyButton1.Location = new System.Drawing.Point(59, 3);
            this.copyButton1.Name = "copyButton1";
            this.copyButton1.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.copyButton1.Size = new System.Drawing.Size(19, 17);
            this.copyButton1.TabIndex = 7;
            this.copyButton1.TabStop = false;
            this.copyButton1.Tag = "chartTraceChart";
            // 
            // excelButton1
            // 
            this.excelButton1.Image = ((System.Drawing.Image)(resources.GetObject("excelButton1.Image")));
            this.excelButton1.Location = new System.Drawing.Point(31, 3);
            this.excelButton1.Name = "excelButton1";
            this.excelButton1.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.excelButton1.Size = new System.Drawing.Size(18, 17);
            this.excelButton1.TabIndex = 8;
            this.excelButton1.TabStop = false;
            this.excelButton1.Tag = "chartTraceChart";
            // 
            // powerPointButton1
            // 
            this.powerPointButton1.Image = ((System.Drawing.Image)(resources.GetObject("powerPointButton1.Image")));
            this.powerPointButton1.Location = new System.Drawing.Point(3, 3);
            this.powerPointButton1.Name = "powerPointButton1";
            this.powerPointButton1.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.powerPointButton1.Size = new System.Drawing.Size(18, 17);
            this.powerPointButton1.TabIndex = 9;
            this.powerPointButton1.TabStop = false;
            this.powerPointButton1.Tag = "chartTraceChart";
            // 
            // copyButton11
            // 
            this.copyButton11.Image = ((System.Drawing.Image)(resources.GetObject("copyButton11.Image")));
            this.copyButton11.Location = new System.Drawing.Point(31, 3);
            this.copyButton11.Name = "copyButton11";
            this.copyButton11.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.copyButton11.Size = new System.Drawing.Size(19, 16);
            this.copyButton11.TabIndex = 9;
            this.copyButton11.TabStop = false;
            this.copyButton11.Tag = "olChart";
            // 
            // excelButton2
            // 
            this.excelButton2.Image = ((System.Drawing.Image)(resources.GetObject("excelButton2.Image")));
            this.excelButton2.Location = new System.Drawing.Point(59, 3);
            this.excelButton2.Name = "excelButton2";
            this.excelButton2.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.excelButton2.Size = new System.Drawing.Size(18, 16);
            this.excelButton2.TabIndex = 7;
            this.excelButton2.TabStop = false;
            this.excelButton2.Tag = "olChartedDataGridView";
            // 
            // powerPointButton2
            // 
            this.powerPointButton2.Image = ((System.Drawing.Image)(resources.GetObject("powerPointButton2.Image")));
            this.powerPointButton2.Location = new System.Drawing.Point(3, 3);
            this.powerPointButton2.Name = "powerPointButton2";
            this.powerPointButton2.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.powerPointButton2.Size = new System.Drawing.Size(18, 16);
            this.powerPointButton2.TabIndex = 8;
            this.powerPointButton2.TabStop = false;
            this.powerPointButton2.Tag = "olChart";
            // 
            // excelButton15
            // 
            this.excelButton15.Image = ((System.Drawing.Image)(resources.GetObject("excelButton15.Image")));
            this.excelButton15.Location = new System.Drawing.Point(110, 437);
            this.excelButton15.Name = "excelButton15";
            this.excelButton15.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.excelButton15.Size = new System.Drawing.Size(17, 18);
            this.excelButton15.TabIndex = 26;
            this.excelButton15.TabStop = false;
            this.excelButton15.Tag = "olNominalChartedDataGridView";
            // 
            // olChart
            // 
            this.olChart.Cursor = System.Windows.Forms.Cursors.Default;
            this.olChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olChart.Location = new System.Drawing.Point(0, 0);
            this.olChart.Name = "olChart";
            this.olChart.ShowLegend = true;
            this.olChart.Size = new System.Drawing.Size(638, 427);
            this.olChart.SplitterDistance = 448;
            this.olChart.TabIndex = 3;
            this.olChart.Title = "Open loop test results";
            // 
            // copyButton3
            // 
            this.copyButton3.Image = ((System.Drawing.Image)(resources.GetObject("copyButton3.Image")));
            this.copyButton3.Location = new System.Drawing.Point(59, 3);
            this.copyButton3.Name = "copyButton3";
            this.copyButton3.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.copyButton3.Size = new System.Drawing.Size(19, 16);
            this.copyButton3.TabIndex = 7;
            this.copyButton3.TabStop = false;
            this.copyButton3.Tag = "clChart";
            // 
            // excelButton3
            // 
            this.excelButton3.Image = ((System.Drawing.Image)(resources.GetObject("excelButton3.Image")));
            this.excelButton3.Location = new System.Drawing.Point(31, 3);
            this.excelButton3.Name = "excelButton3";
            this.excelButton3.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.excelButton3.Size = new System.Drawing.Size(18, 16);
            this.excelButton3.TabIndex = 8;
            this.excelButton3.TabStop = false;
            this.excelButton3.Tag = "clChartedDataGridView";
            // 
            // powerPointButton3
            // 
            this.powerPointButton3.Image = ((System.Drawing.Image)(resources.GetObject("powerPointButton3.Image")));
            this.powerPointButton3.Location = new System.Drawing.Point(3, 3);
            this.powerPointButton3.Name = "powerPointButton3";
            this.powerPointButton3.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.powerPointButton3.Size = new System.Drawing.Size(18, 16);
            this.powerPointButton3.TabIndex = 9;
            this.powerPointButton3.TabStop = false;
            this.powerPointButton3.Tag = "clChart";
            // 
            // clChart
            // 
            this.clChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clChart.Location = new System.Drawing.Point(0, 0);
            this.clChart.Name = "clChart";
            this.clChart.ShowLegend = true;
            this.clChart.Size = new System.Drawing.Size(638, 427);
            this.clChart.SplitterDistance = 445;
            this.clChart.TabIndex = 32;
            this.clChart.Title = "Closed loop test results";
            // 
            // copyButton4
            // 
            this.copyButton4.Image = ((System.Drawing.Image)(resources.GetObject("copyButton4.Image")));
            this.copyButton4.Location = new System.Drawing.Point(59, 3);
            this.copyButton4.Name = "copyButton4";
            this.copyButton4.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.copyButton4.Size = new System.Drawing.Size(19, 16);
            this.copyButton4.TabIndex = 8;
            this.copyButton4.TabStop = false;
            this.copyButton4.Tag = "clLogPlotChart";
            // 
            // powerPointButton4
            // 
            this.powerPointButton4.Image = ((System.Drawing.Image)(resources.GetObject("powerPointButton4.Image")));
            this.powerPointButton4.Location = new System.Drawing.Point(3, 3);
            this.powerPointButton4.Name = "powerPointButton4";
            this.powerPointButton4.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.powerPointButton4.Size = new System.Drawing.Size(18, 16);
            this.powerPointButton4.TabIndex = 9;
            this.powerPointButton4.TabStop = false;
            this.powerPointButton4.Tag = "clLogPlotChart";
            // 
            // excelButton4
            // 
            this.excelButton4.Image = ((System.Drawing.Image)(resources.GetObject("excelButton4.Image")));
            this.excelButton4.Location = new System.Drawing.Point(31, 3);
            this.excelButton4.Name = "excelButton4";
            this.excelButton4.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.excelButton4.Size = new System.Drawing.Size(18, 16);
            this.excelButton4.TabIndex = 10;
            this.excelButton4.TabStop = false;
            this.excelButton4.Tag = "clLogPlotChartedDataGridView";
            // 
            // clLogPlotChart
            // 
            this.clLogPlotChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clLogPlotChart.Location = new System.Drawing.Point(0, 0);
            this.clLogPlotChart.Name = "clLogPlotChart";
            this.clLogPlotChart.ShowLegend = true;
            this.clLogPlotChart.Size = new System.Drawing.Size(638, 427);
            this.clLogPlotChart.SplitterDistance = 445;
            this.clLogPlotChart.TabIndex = 43;
            this.clLogPlotChart.Title = "Closed loop test results (Log plot)";
            // 
            // copyButton5
            // 
            this.copyButton5.Image = ((System.Drawing.Image)(resources.GetObject("copyButton5.Image")));
            this.copyButton5.Location = new System.Drawing.Point(59, 3);
            this.copyButton5.Name = "copyButton5";
            this.copyButton5.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.copyButton5.Size = new System.Drawing.Size(19, 16);
            this.copyButton5.TabIndex = 9;
            this.copyButton5.TabStop = false;
            this.copyButton5.Tag = "olStabiliseChart";
            // 
            // powerPointButton5
            // 
            this.powerPointButton5.Image = ((System.Drawing.Image)(resources.GetObject("powerPointButton5.Image")));
            this.powerPointButton5.Location = new System.Drawing.Point(3, 3);
            this.powerPointButton5.Name = "powerPointButton5";
            this.powerPointButton5.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.powerPointButton5.Size = new System.Drawing.Size(18, 16);
            this.powerPointButton5.TabIndex = 10;
            this.powerPointButton5.TabStop = false;
            this.powerPointButton5.Tag = "olStabiliseChart";
            // 
            // excelButton5
            // 
            this.excelButton5.Image = ((System.Drawing.Image)(resources.GetObject("excelButton5.Image")));
            this.excelButton5.Location = new System.Drawing.Point(31, 3);
            this.excelButton5.Name = "excelButton5";
            this.excelButton5.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.excelButton5.Size = new System.Drawing.Size(18, 16);
            this.excelButton5.TabIndex = 11;
            this.excelButton5.TabStop = false;
            this.excelButton5.Tag = "olStabiliseChartedDataGridView";
            // 
            // olStabiliseChart
            // 
            this.olStabiliseChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olStabiliseChart.Location = new System.Drawing.Point(0, 0);
            this.olStabiliseChart.Name = "olStabiliseChart";
            this.olStabiliseChart.ShowLegend = true;
            this.olStabiliseChart.Size = new System.Drawing.Size(638, 427);
            this.olStabiliseChart.SplitterDistance = 445;
            this.olStabiliseChart.TabIndex = 27;
            this.olStabiliseChart.Title = "OL Stabilise test results";
            // 
            // copyButton6
            // 
            this.copyButton6.Image = ((System.Drawing.Image)(resources.GetObject("copyButton6.Image")));
            this.copyButton6.Location = new System.Drawing.Point(59, 3);
            this.copyButton6.Name = "copyButton6";
            this.copyButton6.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.copyButton6.Size = new System.Drawing.Size(19, 16);
            this.copyButton6.TabIndex = 10;
            this.copyButton6.TabStop = false;
            this.copyButton6.Tag = "clStabiliseChart";
            // 
            // powerPointButton6
            // 
            this.powerPointButton6.Image = ((System.Drawing.Image)(resources.GetObject("powerPointButton6.Image")));
            this.powerPointButton6.Location = new System.Drawing.Point(3, 3);
            this.powerPointButton6.Name = "powerPointButton6";
            this.powerPointButton6.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.powerPointButton6.Size = new System.Drawing.Size(18, 16);
            this.powerPointButton6.TabIndex = 11;
            this.powerPointButton6.TabStop = false;
            this.powerPointButton6.Tag = "clStabiliseChart";
            // 
            // excelButton6
            // 
            this.excelButton6.Image = ((System.Drawing.Image)(resources.GetObject("excelButton6.Image")));
            this.excelButton6.Location = new System.Drawing.Point(31, 3);
            this.excelButton6.Name = "excelButton6";
            this.excelButton6.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.excelButton6.Size = new System.Drawing.Size(18, 16);
            this.excelButton6.TabIndex = 12;
            this.excelButton6.TabStop = false;
            this.excelButton6.Tag = "clStabiliseChartedDataGridView";
            // 
            // clStabiliseChart
            // 
            this.clStabiliseChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clStabiliseChart.Location = new System.Drawing.Point(0, 0);
            this.clStabiliseChart.Name = "clStabiliseChart";
            this.clStabiliseChart.ShowLegend = true;
            this.clStabiliseChart.Size = new System.Drawing.Size(638, 427);
            this.clStabiliseChart.SplitterDistance = 445;
            this.clStabiliseChart.TabIndex = 27;
            this.clStabiliseChart.Title = "CL Stabilise test results";
            // 
            // copyButton7
            // 
            this.copyButton7.Image = ((System.Drawing.Image)(resources.GetObject("copyButton7.Image")));
            this.copyButton7.Location = new System.Drawing.Point(59, 3);
            this.copyButton7.Name = "copyButton7";
            this.copyButton7.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.copyButton7.Size = new System.Drawing.Size(19, 16);
            this.copyButton7.TabIndex = 11;
            this.copyButton7.TabStop = false;
            this.copyButton7.Tag = "fodChart";
            // 
            // powerPointButton7
            // 
            this.powerPointButton7.Image = ((System.Drawing.Image)(resources.GetObject("powerPointButton7.Image")));
            this.powerPointButton7.Location = new System.Drawing.Point(3, 3);
            this.powerPointButton7.Name = "powerPointButton7";
            this.powerPointButton7.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.powerPointButton7.Size = new System.Drawing.Size(18, 16);
            this.powerPointButton7.TabIndex = 12;
            this.powerPointButton7.TabStop = false;
            this.powerPointButton7.Tag = "fodChart";
            // 
            // excelButton7
            // 
            this.excelButton7.Image = ((System.Drawing.Image)(resources.GetObject("excelButton7.Image")));
            this.excelButton7.Location = new System.Drawing.Point(31, 3);
            this.excelButton7.Name = "excelButton7";
            this.excelButton7.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.excelButton7.Size = new System.Drawing.Size(18, 16);
            this.excelButton7.TabIndex = 13;
            this.excelButton7.TabStop = false;
            this.excelButton7.Tag = "fodChartedDataGridView";
            // 
            // fodChart
            // 
            this.fodChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fodChart.Location = new System.Drawing.Point(0, 0);
            this.fodChart.Name = "fodChart";
            this.fodChart.ShowLegend = true;
            this.fodChart.Size = new System.Drawing.Size(638, 427);
            this.fodChart.SplitterDistance = 445;
            this.fodChart.TabIndex = 37;
            this.fodChart.Title = "FOD test results";
            // 
            // powerPointButton8
            // 
            this.powerPointButton8.Image = ((System.Drawing.Image)(resources.GetObject("powerPointButton8.Image")));
            this.powerPointButton8.Location = new System.Drawing.Point(3, 3);
            this.powerPointButton8.Name = "powerPointButton8";
            this.powerPointButton8.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.powerPointButton8.Size = new System.Drawing.Size(18, 16);
            this.powerPointButton8.TabIndex = 6;
            this.powerPointButton8.TabStop = false;
            this.powerPointButton8.Tag = "omvChart";
            // 
            // excelButton8
            // 
            this.excelButton8.Image = ((System.Drawing.Image)(resources.GetObject("excelButton8.Image")));
            this.excelButton8.Location = new System.Drawing.Point(31, 3);
            this.excelButton8.Name = "excelButton8";
            this.excelButton8.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.excelButton8.Size = new System.Drawing.Size(18, 16);
            this.excelButton8.TabIndex = 7;
            this.excelButton8.TabStop = false;
            this.excelButton8.Tag = "omvChartedDataGridView";
            // 
            // copyButton8
            // 
            this.copyButton8.Image = ((System.Drawing.Image)(resources.GetObject("copyButton8.Image")));
            this.copyButton8.Location = new System.Drawing.Point(59, 3);
            this.copyButton8.Name = "copyButton8";
            this.copyButton8.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.copyButton8.Size = new System.Drawing.Size(19, 16);
            this.copyButton8.TabIndex = 8;
            this.copyButton8.TabStop = false;
            this.copyButton8.Tag = "omvChart";
            // 
            // omvChart
            // 
            this.omvChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.omvChart.Location = new System.Drawing.Point(0, 0);
            this.omvChart.Name = "omvChart";
            this.omvChart.ShowLegend = true;
            this.omvChart.Size = new System.Drawing.Size(638, 427);
            this.omvChart.SplitterDistance = 445;
            this.omvChart.TabIndex = 37;
            this.omvChart.Title = "OMV test results";
            // 
            // leakdownChart
            // 
            this.leakdownChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leakdownChart.Location = new System.Drawing.Point(0, 0);
            this.leakdownChart.Name = "leakdownChart";
            this.leakdownChart.ShowLegend = true;
            this.leakdownChart.Size = new System.Drawing.Size(638, 427);
            this.leakdownChart.SplitterDistance = 445;
            this.leakdownChart.TabIndex = 0;
            this.leakdownChart.Title = "Leakdown test results";
            // 
            // powerPointButton9
            // 
            this.powerPointButton9.Image = ((System.Drawing.Image)(resources.GetObject("powerPointButton9.Image")));
            this.powerPointButton9.Location = new System.Drawing.Point(3, 3);
            this.powerPointButton9.Name = "powerPointButton9";
            this.powerPointButton9.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.powerPointButton9.Size = new System.Drawing.Size(18, 16);
            this.powerPointButton9.TabIndex = 15;
            this.powerPointButton9.TabStop = false;
            this.powerPointButton9.Tag = "trimmedChart";
            // 
            // trimmedChart_CopyToClipboard
            // 
            this.trimmedChart_CopyToClipboard.Image = ((System.Drawing.Image)(resources.GetObject("trimmedChart_CopyToClipboard.Image")));
            this.trimmedChart_CopyToClipboard.Location = new System.Drawing.Point(59, 3);
            this.trimmedChart_CopyToClipboard.Name = "trimmedChart_CopyToClipboard";
            this.trimmedChart_CopyToClipboard.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.trimmedChart_CopyToClipboard.Size = new System.Drawing.Size(19, 16);
            this.trimmedChart_CopyToClipboard.TabIndex = 16;
            this.trimmedChart_CopyToClipboard.TabStop = false;
            this.trimmedChart_CopyToClipboard.Tag = "trimmedChart";
            // 
            // excelButton9
            // 
            this.excelButton9.Image = ((System.Drawing.Image)(resources.GetObject("excelButton9.Image")));
            this.excelButton9.Location = new System.Drawing.Point(31, 3);
            this.excelButton9.Name = "excelButton9";
            this.excelButton9.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.excelButton9.Size = new System.Drawing.Size(18, 16);
            this.excelButton9.TabIndex = 17;
            this.excelButton9.TabStop = false;
            this.excelButton9.Tag = "trimmedChartedDataGridView";
            // 
            // viewStatsWindow
            // 
            this.viewStatsWindow.Image = ((System.Drawing.Image)(resources.GetObject("viewStatsWindow.Image")));
            this.viewStatsWindow.Location = new System.Drawing.Point(87, 3);
            this.viewStatsWindow.Name = "viewStatsWindow";
            this.viewStatsWindow.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.viewStatsWindow.Size = new System.Drawing.Size(20, 16);
            this.viewStatsWindow.TabIndex = 18;
            this.viewStatsWindow.TabStop = false;
            this.viewStatsWindow.Tag = "olChart";
            this.toolTip.SetToolTip(this.viewStatsWindow, "View Mean / Median Curve Data");
            this.viewStatsWindow.Click += new System.EventHandler(this.viewStatsWindow_Click);
            // 
            // trimmedCharts
            // 
            this.trimmedCharts.ColumnCount = 2;
            this.trimmedCharts.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.trimmedCharts.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.trimmedCharts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedCharts.Location = new System.Drawing.Point(0, 0);
            this.trimmedCharts.Margin = new System.Windows.Forms.Padding(0);
            this.trimmedCharts.Name = "trimmedCharts";
            this.trimmedCharts.RowCount = 3;
            this.trimmedCharts.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.trimmedCharts.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.trimmedCharts.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.trimmedCharts.Size = new System.Drawing.Size(638, 427);
            this.trimmedCharts.TabIndex = 25;
            this.trimmedCharts.Title = "Trimmed test results @ {0}bar";
            this.trimmedCharts.Visible = false;
            // 
            // trimmedChart
            // 
            this.trimmedChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedChart.Location = new System.Drawing.Point(0, 0);
            this.trimmedChart.Name = "trimmedChart";
            this.trimmedChart.ShowLegend = true;
            this.trimmedChart.Size = new System.Drawing.Size(638, 427);
            this.trimmedChart.SplitterDistance = 445;
            this.trimmedChart.TabIndex = 24;
            this.trimmedChart.Title = "Trimmed test results";
            // 
            // trimmedChartIndividualChartsLegend
            // 
            this.trimmedChartIndividualChartsLegend.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.trimmedChartIndividualChartsLegend.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedChartIndividualChartsLegend.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.trimmedChartIndividualChartsLegend.HideSelection = false;
            this.trimmedChartIndividualChartsLegend.Location = new System.Drawing.Point(0, 0);
            this.trimmedChartIndividualChartsLegend.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.trimmedChartIndividualChartsLegend.Name = "trimmedChartIndividualChartsLegend";
            this.trimmedChartIndividualChartsLegend.Size = new System.Drawing.Size(638, 97);
            this.trimmedChartIndividualChartsLegend.TabIndex = 25;
            this.trimmedChartIndividualChartsLegend.UseCompatibleStateImageBehavior = false;
            this.trimmedChartIndividualChartsLegend.View = System.Windows.Forms.View.List;
            this.trimmedChartIndividualChartsLegend.Visible = false;
            // 
            // copyButton10
            // 
            this.copyButton10.Image = ((System.Drawing.Image)(resources.GetObject("copyButton10.Image")));
            this.copyButton10.Location = new System.Drawing.Point(59, 3);
            this.copyButton10.Name = "copyButton10";
            this.copyButton10.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.copyButton10.Size = new System.Drawing.Size(19, 16);
            this.copyButton10.TabIndex = 6;
            this.copyButton10.TabStop = false;
            this.copyButton10.Tag = "trimmedLogPlotChart";
            // 
            // excelButton10
            // 
            this.excelButton10.Image = ((System.Drawing.Image)(resources.GetObject("excelButton10.Image")));
            this.excelButton10.Location = new System.Drawing.Point(31, 3);
            this.excelButton10.Name = "excelButton10";
            this.excelButton10.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.excelButton10.Size = new System.Drawing.Size(18, 16);
            this.excelButton10.TabIndex = 7;
            this.excelButton10.TabStop = false;
            this.excelButton10.Tag = "trimmedLogPlotChartedDataGridView";
            // 
            // powerPointButton10
            // 
            this.powerPointButton10.Image = ((System.Drawing.Image)(resources.GetObject("powerPointButton10.Image")));
            this.powerPointButton10.Location = new System.Drawing.Point(3, 3);
            this.powerPointButton10.Name = "powerPointButton10";
            this.powerPointButton10.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.powerPointButton10.Size = new System.Drawing.Size(18, 16);
            this.powerPointButton10.TabIndex = 8;
            this.powerPointButton10.TabStop = false;
            this.powerPointButton10.Tag = "trimmedLogPlotChart";
            // 
            // trimmedLogPlotChart
            // 
            this.trimmedLogPlotChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trimmedLogPlotChart.Location = new System.Drawing.Point(0, 0);
            this.trimmedLogPlotChart.Name = "trimmedLogPlotChart";
            this.trimmedLogPlotChart.ShowLegend = true;
            this.trimmedLogPlotChart.Size = new System.Drawing.Size(638, 427);
            this.trimmedLogPlotChart.SplitterDistance = 445;
            this.trimmedLogPlotChart.TabIndex = 43;
            this.trimmedLogPlotChart.Title = "Trimmed test results (Log plot)";
            // 
            // utChart
            // 
            this.utChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.utChart.Location = new System.Drawing.Point(133, 26);
            this.utChart.Name = "utChart";
            this.tableLayoutPanel37.SetRowSpan(this.utChart, 2);
            this.utChart.ShowLegend = true;
            this.utChart.Size = new System.Drawing.Size(638, 399);
            this.utChart.SplitterDistance = 445;
            this.utChart.TabIndex = 5;
            this.utChart.Title = "Untrimmed Data Plot";
            // 
            // InjectorComponentsGridView
            // 
            this.InjectorComponentsGridView.AllowUserToAddRows = false;
            this.InjectorComponentsGridView.AllowUserToDeleteRows = false;
            this.InjectorComponentsGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.InjectorComponentsGridView.BackgroundColor = System.Drawing.Color.LightYellow;
            this.InjectorComponentsGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle41.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle41.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle41.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.InjectorComponentsGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle41;
            this.InjectorComponentsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel36.SetColumnSpan(this.InjectorComponentsGridView, 6);
            this.InjectorComponentsGridView.Cursor = System.Windows.Forms.Cursors.IBeam;
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle42.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle42.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle42.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle42.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle42.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.InjectorComponentsGridView.DefaultCellStyle = dataGridViewCellStyle42;
            this.InjectorComponentsGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InjectorComponentsGridView.Location = new System.Drawing.Point(3, 75);
            this.InjectorComponentsGridView.Name = "InjectorComponentsGridView";
            this.InjectorComponentsGridView.ReadOnly = true;
            this.InjectorComponentsGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.InjectorComponentsGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.InjectorComponentsGridView.ShowEditingIcon = false;
            this.InjectorComponentsGridView.Size = new System.Drawing.Size(1033, 517);
            this.InjectorComponentsGridView.TabIndex = 13;
            this.InjectorComponentsGridView.VirtualMode = true;
            // 
            // excelButton13
            // 
            this.excelButton13.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.excelButton13.Image = ((System.Drawing.Image)(resources.GetObject("excelButton13.Image")));
            this.excelButton13.Location = new System.Drawing.Point(1018, 51);
            this.excelButton13.Name = "excelButton13";
            this.excelButton13.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.excelButton13.Size = new System.Drawing.Size(18, 18);
            this.excelButton13.TabIndex = 10;
            this.excelButton13.TabStop = false;
            this.excelButton13.Tag = "InjectorComponentsGridView";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1059, 658);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "FrmMain";
            this.Text = "Test Data Analyser";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.mainStatusStrip.ResumeLayout(false);
            this.mainStatusStrip.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.mainTabControl.ResumeLayout(false);
            this.queryTabPage.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.databaseGroupBox.ResumeLayout(false);
            this.tableLayoutPanel38.ResumeLayout(false);
            this.tableLayoutPanel38.PerformLayout();
            this.passesFailsGroupBox.ResumeLayout(false);
            this.passesFailsGroupBox.PerformLayout();
            this.testsForEachISNGroupBox.ResumeLayout(false);
            this.testsForEachISNGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lopListDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testTypeListDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testplanListDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lineNumberListDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantNumberListDataGridView)).EndInit();
            this.datasetTabPage.ResumeLayout(false);
            this.DatasetTableLayoutPanel.ResumeLayout(false);
            this.DatasetTableLayoutPanel.PerformLayout();
            this.trimTrackerChartsTabPage.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trimTrackerLeakdownChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimTrackerCDChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimTrackerP3Chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimTrackerMDPChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimTrackerP1Chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimTrackerODChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimTrackerP4Chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimTrackerP5Chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimTrackerP2Chart)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pumpingTrackerChartsTabPage.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pumpingTrackerOMVT3FixedPressureChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pumpingTrackerPressureFixedAngleChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pumpingTrackerAngleFixedPressureChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pumpingTrackerOMVT3FixedAngleChart)).EndInit();
            this.cltTrackerTabPage.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cltP1TrackerChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cltP4TrackerChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cltP5TrackerChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cltP3TrackerChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cltP2TrackerChart)).EndInit();
            this.failureAnalysisTabPage.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.overallPassRateGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.failureFullDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.passRateGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.failureParetoChart)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tableLayoutPanel34.ResumeLayout(false);
            this.tableLayoutPanel34.PerformLayout();
            this.tableLayoutPanel35.ResumeLayout(false);
            this.rigAlignmentTabPage.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.tableLayoutPanel21.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.rigAlignmentTestTypeGroupBox.ResumeLayout(false);
            this.rigAlignmentTestTypeGroupBox.PerformLayout();
            this.rigAlignmentPanel.ResumeLayout(false);
            this.rigAlignmentPanel.PerformLayout();
            this.chartTracesTabPage.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.traceGroupDataByGroupBox.ResumeLayout(false);
            this.traceGroupDataByGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartTraceChart)).EndInit();
            this.tableLayoutPanel33.ResumeLayout(false);
            this.detailedDataTabPage.ResumeLayout(false);
            this.detailedDataTabPage.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.detailedDataTabControl.ResumeLayout(false);
            this.olTabPage.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olNominalChartedDataGridView)).EndInit();
            this.tableLayoutPanel24.ResumeLayout(false);
            this.tableLayoutPanel24.PerformLayout();
            this.olSplitContainer.Panel1.ResumeLayout(false);
            this.olSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.olSplitContainer)).EndInit();
            this.olSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.olChartedDataGridView)).EndInit();
            this.clTabPage.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanel26.ResumeLayout(false);
            this.tableLayoutPanel26.PerformLayout();
            this.clSplitContainer.Panel1.ResumeLayout(false);
            this.clSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.clSplitContainer)).EndInit();
            this.clSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.clChartedDataGridView)).EndInit();
            this.clLogPlotTabPage.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.tableLayoutPanel27.ResumeLayout(false);
            this.tableLayoutPanel27.PerformLayout();
            this.clLogPlotSsplitContainer.Panel1.ResumeLayout(false);
            this.clLogPlotSsplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.clLogPlotSsplitContainer)).EndInit();
            this.clLogPlotSsplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.clLogPlotChartedDataGridView)).EndInit();
            this.olStabiliseTabPage.ResumeLayout(false);
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel18.PerformLayout();
            this.tableLayoutPanel28.ResumeLayout(false);
            this.tableLayoutPanel28.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olStabiliseNominalChartedDataGridView)).EndInit();
            this.olStabiliseSplitContainer.Panel1.ResumeLayout(false);
            this.olStabiliseSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.olStabiliseSplitContainer)).EndInit();
            this.olStabiliseSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.olStabiliseChartedDataGridView)).EndInit();
            this.clStabiliseTabPage.ResumeLayout(false);
            this.tableLayoutPanel19.ResumeLayout(false);
            this.tableLayoutPanel19.PerformLayout();
            this.tableLayoutPanel29.ResumeLayout(false);
            this.tableLayoutPanel29.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clStabiliseNominalChartedDataGridView)).EndInit();
            this.clStabiliseSplitContainer.Panel1.ResumeLayout(false);
            this.clStabiliseSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.clStabiliseSplitContainer)).EndInit();
            this.clStabiliseSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.clStabiliseChartedDataGridView)).EndInit();
            this.fodTabPage.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.tableLayoutPanel30.ResumeLayout(false);
            this.tableLayoutPanel30.PerformLayout();
            this.fodSplitContainer.Panel1.ResumeLayout(false);
            this.fodSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fodSplitContainer)).EndInit();
            this.fodSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fodChartedDataGridView)).EndInit();
            this.omvTabPage.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tableLayoutPanel31.ResumeLayout(false);
            this.tableLayoutPanel31.PerformLayout();
            this.omvSplitContainer.Panel1.ResumeLayout(false);
            this.omvSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.omvSplitContainer)).EndInit();
            this.omvSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.omvChartedDataGridView)).EndInit();
            this.leakdownTabPage.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.leakdownSplitContainer.Panel1.ResumeLayout(false);
            this.leakdownSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.leakdownSplitContainer)).EndInit();
            this.leakdownSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.leakdownChartedDataGridView)).EndInit();
            this.trimmedTabPage.ResumeLayout(false);
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tableLayoutPanel22.PerformLayout();
            this.tableLayoutPanel25.ResumeLayout(false);
            this.tableLayoutPanel25.PerformLayout();
            this.trimmedSplitContainer.Panel1.ResumeLayout(false);
            this.trimmedSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trimmedSplitContainer)).EndInit();
            this.trimmedSplitContainer.ResumeLayout(false);
            this.trimmedChartPanel.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trimmedChartedDataGridView)).EndInit();
            this.tableLayoutPanel40.ResumeLayout(false);
            this.tableLayoutPanel40.PerformLayout();
            this.trimmedLogTabPage.ResumeLayout(false);
            this.tableLayoutPanel23.ResumeLayout(false);
            this.tableLayoutPanel23.PerformLayout();
            this.tableLayoutPanel32.ResumeLayout(false);
            this.tableLayoutPanel32.PerformLayout();
            this.trimmedLogPlotSplitContainer.Panel1.ResumeLayout(false);
            this.trimmedLogPlotSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trimmedLogPlotSplitContainer)).EndInit();
            this.trimmedLogPlotSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trimmedLogPlotChartedDataGridView)).EndInit();
            this.untrimmedTabPage.ResumeLayout(false);
            this.tableLayoutPanel37.ResumeLayout(false);
            this.tableLayoutPanel37.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.utChartedDataGridView)).EndInit();
            this.groupDataByGroupBox.ResumeLayout(false);
            this.groupDataByGroupBox.PerformLayout();
            this.genericTrendChartTabPage.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.genericTrendChart)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.xyChartTabPage.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xyChart)).EndInit();
            this.trendChartTabPage.ResumeLayout(false);
            this.tableLayoutPanel20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trendChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendChartDataGridView)).EndInit();
            this.InjectorComponentsTabPage.ResumeLayout(false);
            this.tableLayoutPanel36.ResumeLayout(false);
            this.tableLayoutPanel36.PerformLayout();
            this.tableLayoutPanel39.ResumeLayout(false);
            this.tableLayoutPanel39.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbProcesDataExcel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcessData)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbPackingDataExcel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcessDataPack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.euro6DataSet)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDaimlerISNExcel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDaimlerISN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.incompletePumpingDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.completedPumpingDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.completedCalibrationDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.incompleteCalibrationDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompleteCalibrationExcelButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompletePumpingExcelButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncompleteCalibrationExcelButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncompletePumpingExcelButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.copyButton9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.copyButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.copyButton11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.olChart)).EndInit();
            this.olChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.copyButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clChart)).EndInit();
            this.clChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.copyButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clLogPlotChart)).EndInit();
            this.clLogPlotChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.copyButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.olStabiliseChart)).EndInit();
            this.olStabiliseChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.copyButton6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clStabiliseChart)).EndInit();
            this.clStabiliseChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.copyButton7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fodChart)).EndInit();
            this.fodChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.copyButton8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.omvChart)).EndInit();
            this.omvChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.leakdownChart)).EndInit();
            this.leakdownChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimmedChart_CopyToClipboard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewStatsWindow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimmedChart)).EndInit();
            this.trimmedChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.copyButton10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPointButton10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trimmedLogPlotChart)).EndInit();
            this.trimmedLogPlotChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.utChart)).EndInit();
            this.utChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.InjectorComponentsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelButton13)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.StatusStrip mainStatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel mainStatusStripLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Timer queryUpdateTimer;
        private System.Windows.Forms.TabControl mainTabControl;
        private System.Windows.Forms.TabPage queryTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox incompletePumpingQueryTextBox;
        private System.Windows.Forms.TextBox completePumpingQueryTextBox;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label injCountLabel;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox databaseGroupBox;
        private System.Windows.Forms.GroupBox passesFailsGroupBox;
        private System.Windows.Forms.RadioButton passesAndFailsRadioButton;
        private System.Windows.Forms.RadioButton failsOnlyRadioButton;
        private System.Windows.Forms.RadioButton passesOnlyRadioButton;
        private System.Windows.Forms.CheckBox allowForcedTrimCheckBox;
        private System.Windows.Forms.GroupBox testsForEachISNGroupBox;
        private System.Windows.Forms.RadioButton allTestsRadioButton;
        private System.Windows.Forms.RadioButton latestTestsRadioButton;
        private System.Windows.Forms.RadioButton firstTimeTestsRadioButton;
        private System.Windows.Forms.CheckBox allowIncompleteTestsCheckBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView lopListDataGridView;
        private System.Windows.Forms.TextBox specificutidsTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox specificISNsTextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView testTypeListDataGridView;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox specificutidsCheckBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridView testplanListDataGridView;
        private System.Windows.Forms.CheckBox specificISNsCheckBox;
        private System.Windows.Forms.DataGridView lineNumberListDataGridView;
        private System.Windows.Forms.TextBox completeCalibrationQueryTextBox;
        private System.Windows.Forms.DataGridView plantNumberListDataGridView;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button runMainQueryButton;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox incompleteCalibrationQueryTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox NumericISNCheckBox;
        private System.Windows.Forms.TabPage datasetTabPage;
        private System.Windows.Forms.TableLayoutPanel DatasetTableLayoutPanel;
        private VirtualDataGridView.IncompleteVirtualDataGridView incompletePumpingDataGridView;
        private VirtualDataGridView.CompletedVirtualDataGridView completedPumpingDataGridView;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label24;
        private VirtualDataGridView.CompletedVirtualDataGridView completedCalibrationDataGridView;
        private VirtualDataGridView.IncompleteVirtualDataGridView incompleteCalibrationDataGridView;
        private System.Windows.Forms.Label label9;
        private ExcelButton CompleteCalibrationExcelButton;
        private ExcelButton CompletePumpingExcelButton;
        private ExcelButton IncompleteCalibrationExcelButton;
        private ExcelButton IncompletePumpingExcelButton;
        private LabelButton CompletedCalibrationCount0;
        private LabelButton CompletedCalibrationCount5;
        private LabelButton CompletedCalibrationCount4;
        private LabelButton CompletedCalibrationCount1;
        private LabelButton CompletedCalibrationCount2;
        private LabelButton CompletedCalibrationCount3;
        private LabelButton CompletePumpingCount0;
        private LabelButton CompletePumpingCount1;
        private LabelButton CompletePumpingCount2;
        private LabelButton CompletePumpingCount3;
        private LabelButton CompletePumpingCount4;
        private System.Windows.Forms.TabPage trimTrackerChartsTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.CheckBox trimTrackerPlotGradesCheckBox;
        private System.Windows.Forms.DataVisualization.Charting.Chart trimTrackerLeakdownChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart trimTrackerCDChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart trimTrackerP3Chart;
        private System.Windows.Forms.DataVisualization.Charting.Chart trimTrackerMDPChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart trimTrackerP1Chart;
        private System.Windows.Forms.DataVisualization.Charting.Chart trimTrackerODChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart trimTrackerP4Chart;
        private System.Windows.Forms.DataVisualization.Charting.Chart trimTrackerP5Chart;
        private System.Windows.Forms.DataVisualization.Charting.Chart trimTrackerP2Chart;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.RadioButton trimTrackerPlotChronologicalRadioButton;
        private System.Windows.Forms.RadioButton trimTrackerPlotDateRadioButton;
        private System.Windows.Forms.TabPage pumpingTrackerChartsTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.DataVisualization.Charting.Chart pumpingTrackerOMVT3FixedPressureChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart pumpingTrackerPressureFixedAngleChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart pumpingTrackerAngleFixedPressureChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart pumpingTrackerOMVT3FixedAngleChart;
        private System.Windows.Forms.TabPage cltTrackerTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox cltTrackerShowRealCheckBox;
        private System.Windows.Forms.CheckBox cltTrackerShowExpectedCheckBox;
        private System.Windows.Forms.DataVisualization.Charting.Chart cltP1TrackerChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart cltP4TrackerChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart cltP5TrackerChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart cltP3TrackerChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart cltP2TrackerChart;
        private System.Windows.Forms.TabPage failureAnalysisTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.DataGridView overallPassRateGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn pumpPasses;
        private System.Windows.Forms.DataGridViewTextBoxColumn pumpFails;
        private System.Windows.Forms.DataGridViewTextBoxColumn pumpIncompleteTests;
        private System.Windows.Forms.DataGridViewTextBoxColumn pumpTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn pumpPassRate;
        private System.Windows.Forms.DataGridView failureFullDataGridView;
        private System.Windows.Forms.Button analyseFailedInjectorsButton;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DataGridView passRateGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn calDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn calPasses;
        private System.Windows.Forms.DataGridViewTextBoxColumn calFails;
        private System.Windows.Forms.DataGridViewTextBoxColumn calIncompleteTests;
        private System.Windows.Forms.DataGridViewTextBoxColumn calTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn calPassRate;
        private System.Windows.Forms.DataVisualization.Charting.Chart failureParetoChart;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox SplitResultsByRig;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.RadioButton SplitResultsByShift;
        private System.Windows.Forms.RadioButton SplitResults10Till10;
        private System.Windows.Forms.RadioButton SplitResults6Till6;
        private System.Windows.Forms.RadioButton SplitResultsDaily;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.CheckBox IgnoreIncompleteResults;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton graphFailureModesRadioButton;
        private System.Windows.Forms.RadioButton graphParetoRadioButton;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton filterAllRadioButton;
        private System.Windows.Forms.RadioButton filterTestFailsRadioButton;
        private System.Windows.Forms.RadioButton filterOEEFailsRadioButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel34;
        private System.Windows.Forms.Label label17;
        private ExcelButton excelButton11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel35;
        private PowerPointButton powerPointButton11;
        private CopyButton copyButton9;
        private ExcelButton excelButton12;
        private System.Windows.Forms.TabPage rigAlignmentTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rigAlignmentChronologicalRadioButton;
        private System.Windows.Forms.RadioButton rigAlignmentDateTimeRadioButton;
        private System.Windows.Forms.GroupBox rigAlignmentTestTypeGroupBox;
        private System.Windows.Forms.RadioButton rigAlignmentPumpingRadioButton;
        private System.Windows.Forms.RadioButton rigAlignmentCalibrationRadioButton;
        private System.Windows.Forms.TreeView rigAlignmentParameterTreeView;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.ListView rigAlignmentLineSelectionListView;
        private System.Windows.Forms.Panel rigAlignmentPanel;
        private System.Windows.Forms.CheckBox RigAlignmentShowLegend;
        private System.Windows.Forms.TabPage chartTracesTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.ListView tracePumpingutidsListView;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Button getTraceDataButton;
        private System.Windows.Forms.GroupBox traceGroupDataByGroupBox;
        private System.Windows.Forms.RadioButton traceISNRadioButton;
        private System.Windows.Forms.RadioButton traceTestplanRadioButton;
        private System.Windows.Forms.RadioButton tracePassFailRadioButton;
        private System.Windows.Forms.RadioButton traceIDENTRadioButton;
        private System.Windows.Forms.RadioButton traceLOPRadioButton;
        private System.Windows.Forms.RadioButton traceTestPointRadioButton;
        private System.Windows.Forms.RadioButton traceUTIDRadioButton;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartTraceChart;
        private System.Windows.Forms.Button updateChartTraceChartButton;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ListView traceOMVTestPointListView;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ListView traceFODTestPointListView;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ListView traceCLTTestPointListView;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ListView traceCalibrationutidsListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ListView traceNCVTestPointListView;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel33;
        private CopyButton copyButton1;
        private ExcelButton excelButton1;
        private PowerPointButton powerPointButton1;
        private System.Windows.Forms.CheckBox AutoUpdateTraceChartsCheckBox;
        private System.Windows.Forms.TabPage detailedDataTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.CheckBox returnNominalDataCheckBox;
        private System.Windows.Forms.GroupBox groupDataByGroupBox;
        private System.Windows.Forms.RadioButton rigGroupRadioButton;
        private System.Windows.Forms.RadioButton lineGroupRadioButton;
        private System.Windows.Forms.RadioButton identGroupRadioButton;
        private System.Windows.Forms.RadioButton utidGroupRadioButton;
        private System.Windows.Forms.RadioButton testplanGroupRadioButton;
        private System.Windows.Forms.RadioButton isnGroupRadioButton;
        private System.Windows.Forms.RadioButton lopGroupRadioButton;
        private System.Windows.Forms.RadioButton passFailGroupRadioButton;
        private System.Windows.Forms.Button detailedDataUpdateButton;
        private System.Windows.Forms.ListView DetailedUTIDsListView;
        private System.Windows.Forms.ColumnHeader colUTID;
        private System.Windows.Forms.ColumnHeader colPN;
        private System.Windows.Forms.ColumnHeader colISN;
        private System.Windows.Forms.ColumnHeader colPDate;
        private System.Windows.Forms.CheckBox AutoUpdateDetailedChartsCheckBox;
        private System.Windows.Forms.TabPage genericTrendChartTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton genericTrendPumpingRadioButton;
        private System.Windows.Forms.RadioButton genericTrendCalibrationRadioButton;
        private System.Windows.Forms.DataVisualization.Charting.Chart genericTrendChart;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton genericTrendChronologicalRadioButton;
        private System.Windows.Forms.RadioButton genericTrendDateTimeRadioButton;
        private System.Windows.Forms.TreeView genericTrendChartTreeView;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TabPage xyChartTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton xyPumpingRadioButton;
        private System.Windows.Forms.RadioButton xyCalibrationRadioButton;
        private System.Windows.Forms.DataVisualization.Charting.Chart xyChart;
        private System.Windows.Forms.TreeView yAxisXYChartTreeView;
        private System.Windows.Forms.TreeView xAxisXYChartTreeView;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TabPage trendChartTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.ListView trendutidsListView;
        private System.Windows.Forms.ColumnHeader utidColumnHeader;
        private System.Windows.Forms.ColumnHeader pnColumnHeader;
        private System.Windows.Forms.ColumnHeader isnColumnHeader;
        private System.Windows.Forms.ColumnHeader pdateColumnHeader;
        private System.Windows.Forms.ColumnHeader rigtypeColumnHeader;
        private System.Windows.Forms.ColumnHeader reasonColumnHeader;
        private System.Windows.Forms.DataVisualization.Charting.Chart trendChart;
        private System.Windows.Forms.DataGridView trendChartDataGridView;
        private System.Windows.Forms.TabPage InjectorComponentsTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel36;
        private ExcelButton excelButton13;
        private System.Windows.Forms.Button UpdateInjectorComponentsButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel38;
        private System.Windows.Forms.RadioButton dafDB01RadioButton;
        private System.Windows.Forms.RadioButton mdegDB01RadioButton;
        private VirtualDataGridView.InjectorComponentDataGridView InjectorComponentsGridView;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel39;
        private System.Windows.Forms.Label selectedComponentsLabel;
        private System.Windows.Forms.Label missingComponentsLabel;
        private System.Windows.Forms.RadioButton traceLineNumberRadioButton;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
        private System.Windows.Forms.ToolStripMenuItem viewChangelogToolStripMenuItem;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.ListView traceCLSTestPointListView;
        private System.Windows.Forms.TabControl detailedDataTabControl;
        private System.Windows.Forms.TabPage olTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.DataGridView olNominalChartedDataGridView;
        private System.Windows.Forms.ListView olPressureLevelsListView;
        private System.Windows.Forms.CheckBox showOLNominalCheckBox;
        private System.Windows.Forms.Button olUpdateChartButton;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ListView olParametersListView;
        private System.Windows.Forms.ListView olStatisticsListView;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;
        private System.Windows.Forms.CheckBox olChartLegendCheckBox;
        private ExcelButton excelButton2;
        private PowerPointButton powerPointButton2;
        private ExcelButton excelButton15;
        private System.Windows.Forms.SplitContainer olSplitContainer;
        private ChartSplitLegend olChart;
        private System.Windows.Forms.DataGridView olChartedDataGridView;
        private System.Windows.Forms.TabPage clTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel26;
        private System.Windows.Forms.CheckBox clChartLegendCheckBox;
        private CopyButton copyButton3;
        private ExcelButton excelButton3;
        private PowerPointButton powerPointButton3;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ListView clStatisticsListView;
        private System.Windows.Forms.Button clUpdateChartButton;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.ListView clPressureLevelsListView;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ListView clParametersListView;
        private System.Windows.Forms.SplitContainer clSplitContainer;
        private ChartSplitLegend clChart;
        private System.Windows.Forms.DataGridView clChartedDataGridView;
        private System.Windows.Forms.TabPage clLogPlotTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel27;
        private System.Windows.Forms.CheckBox clLogPlotChartLegendCheckBox;
        private CopyButton copyButton4;
        private PowerPointButton powerPointButton4;
        private ExcelButton excelButton4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox showFuelAsErrorCheckBox;
        private System.Windows.Forms.ListView clLogPlotPressureLevelsListView;
        private System.Windows.Forms.ListView clLogPlotStatisticsListView;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button clLogPlotUpdateChartButton;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ListView clLogPlotParametersListView;
        private System.Windows.Forms.SplitContainer clLogPlotSsplitContainer;
        private ChartSplitLegend clLogPlotChart;
        private System.Windows.Forms.DataGridView clLogPlotChartedDataGridView;
        private System.Windows.Forms.TabPage olStabiliseTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel28;
        private System.Windows.Forms.CheckBox olStabiliseChartLegendCheckBox;
        private CopyButton copyButton5;
        private PowerPointButton powerPointButton5;
        private ExcelButton excelButton5;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.DataGridView olStabiliseNominalChartedDataGridView;
        private System.Windows.Forms.ListView olStabilisePressureLevelsListView;
        private System.Windows.Forms.CheckBox olStabiliseShowNominalCheckBox;
        private System.Windows.Forms.Button olStabiliseUpdateChartButton;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ListView olStabiliseParametersListView;
        private System.Windows.Forms.ListView olStabiliseStatisticsListView;
        private System.Windows.Forms.SplitContainer olStabiliseSplitContainer;
        private ChartSplitLegend olStabiliseChart;
        private System.Windows.Forms.DataGridView olStabiliseChartedDataGridView;
        private System.Windows.Forms.TabPage clStabiliseTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel29;
        private System.Windows.Forms.CheckBox clStabiliseChartLegendCheckBox;
        private CopyButton copyButton6;
        private PowerPointButton powerPointButton6;
        private ExcelButton excelButton6;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.DataGridView clStabiliseNominalChartedDataGridView;
        private System.Windows.Forms.ListView clStabilisePressureLevelsListView;
        private System.Windows.Forms.CheckBox clStabiliseShowNominalCheckBox;
        private System.Windows.Forms.Button clStabiliseUpdateChartButton;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.ListView clStabiliseParametersListView;
        private System.Windows.Forms.ListView clStabiliseStatisticsListView;
        private System.Windows.Forms.SplitContainer clStabiliseSplitContainer;
        private ChartSplitLegend clStabiliseChart;
        private System.Windows.Forms.DataGridView clStabiliseChartedDataGridView;
        private System.Windows.Forms.TabPage fodTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel30;
        private System.Windows.Forms.CheckBox FODChartLegendCheckBox;
        private CopyButton copyButton7;
        private PowerPointButton powerPointButton7;
        private ExcelButton excelButton7;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ListView fodStatisticsListView;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListView fodParametersListView;
        private System.Windows.Forms.Button fodUpdateChartButton;
        private System.Windows.Forms.SplitContainer fodSplitContainer;
        private ChartSplitLegend fodChart;
        private System.Windows.Forms.DataGridView fodChartedDataGridView;
        private System.Windows.Forms.TabPage omvTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel31;
        private System.Windows.Forms.CheckBox OMVChartLegendCheckBox;
        private PowerPointButton powerPointButton8;
        private ExcelButton excelButton8;
        private CopyButton copyButton8;
        private System.Windows.Forms.ListView omvStatisticsListView;
        private System.Windows.Forms.Button omvUpdateChartButton;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.ListView omvParametersListView;
        private System.Windows.Forms.SplitContainer omvSplitContainer;
        private ChartSplitLegend omvChart;
        private System.Windows.Forms.DataGridView omvChartedDataGridView;
        private System.Windows.Forms.TabPage leakdownTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Button leakdownUpdateChartButton;
        private System.Windows.Forms.ListView leakdownParametersListView;
        private System.Windows.Forms.SplitContainer leakdownSplitContainer;
        private ChartSplitLegend leakdownChart;
        private System.Windows.Forms.DataGridView leakdownChartedDataGridView;
        private System.Windows.Forms.TabPage trimmedTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.ListView trimmedStatisticsListView;
        private System.Windows.Forms.Button trimmedUpdateChartButton;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.ListView trimmedPressureLevelsListView;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.ListView trimmedParametersListView;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel25;
        private System.Windows.Forms.CheckBox trimmedChartLegendCheckBox;
        private System.Windows.Forms.CheckBox trimmedChartIndividualCharts;
        private System.Windows.Forms.ComboBox trimmedChart_Limits;
        private System.Windows.Forms.Label trimmedChart_LimitsLabel;
        private PowerPointButton powerPointButton9;
        private CopyButton trimmedChart_CopyToClipboard;
        private ExcelButton excelButton9;
        private System.Windows.Forms.SplitContainer trimmedSplitContainer;
        private System.Windows.Forms.Panel trimmedChartPanel;
        private TableOfCharts trimmedCharts;
        private ChartSplitLegend trimmedChart;
        private System.Windows.Forms.Panel panel5;
        private ChartLegend trimmedChartIndividualChartsLegend;
        private System.Windows.Forms.DataGridView trimmedChartedDataGridView;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel40;
        private System.Windows.Forms.CheckBox trimmedMeanToggle;
        private System.Windows.Forms.CheckBox trimmedMedianToggle;
        private System.Windows.Forms.TabPage trimmedLogTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel32;
        private System.Windows.Forms.CheckBox trimmedLogPlotChartLegendCheckBox;
        private CopyButton copyButton10;
        private ExcelButton excelButton10;
        private PowerPointButton powerPointButton10;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.CheckBox trimmedShowFuelAsErrorCheckBox;
        private System.Windows.Forms.ListView trimmedLogPlotPressureLevelsListView;
        private System.Windows.Forms.ListView trimmedLogPlotStatisticsListView;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Button trimmedLogPlotUpdateChartButton;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.ListView trimmedLogPlotParametersListView;
        private System.Windows.Forms.SplitContainer trimmedLogPlotSplitContainer;
        private ChartSplitLegend trimmedLogPlotChart;
        private System.Windows.Forms.DataGridView trimmedLogPlotChartedDataGridView;
        private System.Windows.Forms.TabPage untrimmedTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel37;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.ListView utPressureLevelsListView;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button utChartUpdateButton;
        private System.Windows.Forms.DataGridView utChartedDataGridView;
        private ChartSplitLegend utChart;
        private CopyButton copyButton11;
        private CopyButton viewStatsWindow;
        private System.Windows.Forms.CheckBox checkBoxUseIasiArchive;
        private System.Windows.Forms.CheckBox checkBoxUseSudburyArchive;
        private System.Windows.Forms.RadioButton vsql08NewDBRadioButton;
        private System.Windows.Forms.RadioButton vsql08OldDBRadioButton;
        private System.Windows.Forms.Button btnUpdate;
        public ExtendedDateTimePicker dateToExtDateTimePicker;
        public ExtendedDateTimePicker dateFromExtDateTimePicker;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox tbProcessDataSerial;
        private System.Windows.Forms.Button btnProcessData;
        private System.Windows.Forms.Label SerialLabel;
        
        private Euro6DataSet euro6DataSet;
        private System.Windows.Forms.DataGridView dgvProcessData;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnPackingData;
        private System.Windows.Forms.DataGridView dgvProcessDataPack;
        private System.Windows.Forms.TextBox tbPackingData;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label lPackingDataOutput;
        private System.Windows.Forms.PictureBox lbPackingDataExcel;
        private System.Windows.Forms.PictureBox lbProcesDataExcel;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.PictureBox btnDaimlerISNExcel;
        private System.Windows.Forms.Label lDaimlerISN;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Button btnDaimlerISN;
        private System.Windows.Forms.DataGridView dgvDaimlerISN;
        private System.Windows.Forms.TextBox tbDaimlerISN;
    }
}

