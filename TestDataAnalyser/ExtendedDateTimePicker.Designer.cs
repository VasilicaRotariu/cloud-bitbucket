﻿namespace SudburyDataAnalyser
{
    /// <summary>
    /// Represents an instance of the modified DateTimePicker tool to allow for background colour changes whilst editing.
    /// </summary>
    public partial class ExtendedDateTimePicker
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
                solidbrushBackBrush.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ExtendedDateTimePicker
            // 
            //this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            //this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ExtendedDateTimePicker";
            //this.Load += new System.EventHandler(this.ExtendedDateTimePicker_Load);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
