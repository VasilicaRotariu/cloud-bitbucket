﻿namespace SudburyDataAnalyser
{
    partial class SplashScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.loadingDescriptionText = new System.Windows.Forms.Label();
            this.loadingHeaderText = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.loadingDescriptionText, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.loadingHeaderText, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(436, 134);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // loadingDescriptionText
            // 
            this.loadingDescriptionText.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.loadingDescriptionText, 2);
            this.loadingDescriptionText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loadingDescriptionText.Location = new System.Drawing.Point(3, 100);
            this.loadingDescriptionText.Name = "loadingDescriptionText";
            this.loadingDescriptionText.Size = new System.Drawing.Size(430, 34);
            this.loadingDescriptionText.TabIndex = 0;
            this.loadingDescriptionText.Text = "Please Wait ...";
            this.loadingDescriptionText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // loadingHeaderText
            // 
            this.loadingHeaderText.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.loadingHeaderText, 2);
            this.loadingHeaderText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loadingHeaderText.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadingHeaderText.Location = new System.Drawing.Point(3, 0);
            this.loadingHeaderText.Name = "loadingHeaderText";
            this.loadingHeaderText.Size = new System.Drawing.Size(430, 100);
            this.loadingHeaderText.TabIndex = 1;
            this.loadingHeaderText.Text = "Loading";
            this.loadingHeaderText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SplashScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(436, 134);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SplashScreen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SplashScreen";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label loadingDescriptionText;
        private System.Windows.Forms.Label loadingHeaderText;
    }
}