﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SudburyDataAnalyser
{
    /// <summary>
    /// Display form for the Changelog
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed.")]
    public partial class ChangelogReport : Form
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ChangelogReport" /> class
        /// </summary>
        public ChangelogReport()
        {
            InitializeComponent();

            Changelog instance = new Changelog();
            txtCurrentVersion.Text = instance.Version;
            rtbChangeLog.Text = instance.ChangeLogText;
        }

        /// <summary>
        /// Called when the Close button is clicked
        /// </summary>
        /// <param name="sender">Sender component</param>
        /// <param name="e">Event arguments</param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
