﻿using System.Collections.Generic;
using System.Text;
using System.Diagnostics.CodeAnalysis;

namespace SudburyDataAnalyser.Queries.DataTable
{
    /// <summary>
    /// Base class for the unique test ID queries.
    /// </summary>
    /// <typeparam name="T">The query type.</typeparam>
    public class Query<T> : QueryBase<T> where T : CompleteRow, new()
    {
        /// <summary>
        /// Gets or sets the query parameters.
        /// </summary>
        protected DataTableQueryParameters Parameters { get; set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="Query{T}"/> class.
        /// </summary>
        /// <param name="parameters">The parameters used in the query.</param>
        public Query(DataTableQueryParameters parameters)
            : base(parameters.Database)
        {
            Parameters = parameters;
        }

        /// <summary>
        /// Creates SQL friendly string for rig type (calibration or pumping).
        /// </summary>
        /// <param name="rigType">Rig type string (i.e. calibration or pumping).</param>
        /// <returns>SQL formatted string for RigType.</returns>
        protected string GenerateSqlStringForRigType(string rigType)
        {
            if (!Database.FtStructure) // New DB structure, where RigType exists and each rig is either a calibration or pumping rig, but not both
            {
                var query = new StringBuilder();
                query.AppendLines("INNER JOIN [", RigTypeTable, "] AS rt ON (rt.PlantNumber = he.PlantNumber AND rt.RigTypeIndex IN (SELECT MAX(mx.RigTypeIndex) FROM [", RigTypeTable, "] AS mx WHERE mx.PlantNumber = rt.PlantNumber AND mx.RigType = '{0}'))".Args(rigType));

                return query.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Creates SQL friendly string for the column and column values.
        /// </summary>
        /// <param name="columnName">The name of the column.</param>
        /// <param name="list">The select values for the column.</param>
        /// <returns>SQL formatted string for the column values.</returns>
        protected string GenerateSqlStringFromList(string columnName, List<string> list)
        {
            if (list.Count > 0)
            {
                return "AND he.{0} IN ('{1}')".Args(columnName, string.Join("','", list));
            }
            return string.Empty;
        }

        /// <summary>
        /// Creates SQL friendly string for the UTID and plant number columns and keys.
        /// </summary>
        /// <param name="utidColumnName">The name of the UTID column.</param>
        /// <param name="plantNumberColumnName">The name of the plant number column.</param>
        /// <param name="keys">The select values for the keys.</param>
        /// <returns>SQL formatted string for the key values.</returns>
        protected string GenerateSqlStringFromUtidPlantNumberList(string utidColumnName, string plantNumberColumnName, UTIDPlantNumberKeys keys)
        {
            if (keys.Count == 0) return string.Empty;
            return "AND EXISTS (SELECT * FROM (values {0}) as X(a,b) WHERE he.{1} = a AND he.{2} = b)".Args(keys.ToString(), utidColumnName, plantNumberColumnName);
        }

        /// <summary>
        /// Create SQL string for TestType list.
        /// </summary>
        /// <param name="rigType">Rig type string (i.e. calibration or pumping).</param>
        /// <returns>SQL formatted string for TestType.</returns>
        protected string GenerateSqlStringForTestType(string rigType)
        {
            if (!Database.FtStructure) return string.Empty;

            if (rigType == "C")
            {
                return "AND he.TestType != 'PumpCheck'";
            }

            return "AND he.TestType = 'PumpCheck'";
        }

        /// <summary>
        /// Creates a SQL string to ensure the column values are numeric.
        /// </summary>
        /// <param name="columnName">The name of the column.</param>
        /// <param name="numericIsn">Value indicating the column values must be numeric.</param>
        /// <returns>SQL formatted string.</returns>
        protected string GenerateSqlStringForIsNumeric(string columnName, bool numericIsn)
        {
            if (!numericIsn) return string.Empty;
            return "AND ISNUMERIC(he.{0}) = 1".Args(columnName);
        }

        /// <summary>
        /// Creates SQL friendly string for test number (All, first or latest).
        /// </summary>
        /// <param name="testNumber">Test number string (i.e. All, first or latest).</param>
        /// <param name="rigType">Rig type string (i.e. calibration or pumping).</param>
        /// <returns>SQL formatted string for TestNumber.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        protected string GenerateSqlStringForTestNumber(string testNumber, string rigType)
        {
            var sql = new StringBuilder();

            switch (testNumber)
            {
                case "First":
                    sql.AppendLines(
                        "AND he.UTID IN (",
                        "SELECT MIN(mn.UTID)",
                        "FROM [", HeaderTable, "] AS mn");
                    if (!Database.FtStructure)
                    {
                        sql.AppendLines("INNER JOIN [", RigTypeTable, "] AS rt ON (rt.PlantNumber = mn.PlantNumber AND rt.RigTypeIndex IN (SELECT MAX(rtmx.RigTypeIndex) FROM [", RigTypeTable, "] AS rtmx WHERE rtmx.PlantNumber = rt.PlantNumber AND rtmx.RigType = '{0}'))".Args(rigType));
                    }
                    sql.AppendLine("WHERE mn.ISN = he.ISN");
                    if (Database.FtStructure)
                    {
                        sql.AppendLine("AND mn.TestType = he.TestType");
                    }
                    sql.Append(")");
                    break;
                case "Latest":
                    sql.AppendLines(
                        "AND he.UTID IN (",
                        "SELECT MAX(mx.UTID)",
                        "FROM [", HeaderTable, "] AS mx");
                    if (!Database.FtStructure)
                    {
                        sql.AppendLines("INNER JOIN [", RigTypeTable, "] AS rt ON (rt.PlantNumber = mx.PlantNumber AND rt.RigTypeIndex IN (SELECT MAX(rtmx.RigTypeIndex) FROM [", RigTypeTable, "] AS rtmx WHERE rtmx.PlantNumber = rt.PlantNumber AND rtmx.RigType = '{0}'))".Args(rigType));
                    }
                    sql.AppendLine("WHERE mx.ISN = he.ISN");
                    if (Database.FtStructure)
                    {
                        sql.AppendLine("AND mx.TestType = he.TestType");
                    }
                    sql.Append(")");
                    break;
            }

            return sql.ToString();
        }

        /// <summary>
        /// Creates SQL friendly string for result (All, passes or fails).
        /// </summary>
        /// <param name="result">Result string (i.e. All, passes or fails).</param>
        /// <returns>SQL formatted string for Result.</returns>
        protected string GenerateSqlStringForResult(string result)
        {
            switch (result)
            {
                case "Passes":
                    return "AND (name.Name = 'Global' AND (re.Grade = 0 OR re.Grade = -2 OR re.Grade = -3))";
                case "Fails":
                    return "AND (name.Name = 'Global' AND (re.Grade != 0 AND re.Grade != -2 AND re.Grade != -3))";
                default:
                    return string.Empty;
            }
        }
    }
}
