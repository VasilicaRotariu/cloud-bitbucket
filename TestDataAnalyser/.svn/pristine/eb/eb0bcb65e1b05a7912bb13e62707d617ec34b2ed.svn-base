﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Class based on a table layout panel, where each cell contains a chart.
    /// </summary>
    public class TableOfCharts : TableLayoutPanel
    {
        /// <summary>
        /// Gets or sets the column count.
        /// Overrides the default property. Sets the default property and reallocates the charts.
        /// </summary>
        public new int ColumnCount
        {
            get
            {
                return base.ColumnCount;
            }

            set
            {
                base.ColumnCount = value;
                AllocateCharts();
            }
        }

        /// <summary>
        /// Gets or sets the row count.
        /// Overrides the default property. Sets the default property and reallocates the charts.
        /// </summary>
        public new int RowCount
        {
            get
            {
                return base.RowCount;
            }

            set
            {
                base.RowCount = value;
                AllocateCharts();
            }
        }

        /// <summary>
        /// Gets or sets the chart title (format).
        /// </summary>
        [Description("Set the charts common title"),
         Bindable(true),
         EditorBrowsable(EditorBrowsableState.Always),
         DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
         Browsable(true)]
        public string Title { get; set; }

        /// <summary>
        /// Formats the indexed chart title with the parameters specified.
        /// </summary>
        /// <param name="chartIndex">The chart index.</param>
        /// <param name="parameters">The text formatting parameters.</param>
        public void FormatTitle(int chartIndex, params object[] parameters)
        {
            var chart = Controls[chartIndex] as Chart;
            if (chart != null)
            {
                chart.Titles[0] = new Title(string.Format(Title, parameters));
            }
        }

        /// <summary>
        /// Hides an chart controls.
        /// </summary>
        public void Clear()
        {
            foreach (Control control in Controls)
            {
                control.Visible = false;
            }
        }

        /// <summary>
        /// Sets the chart legend control, used to get selection change events.
        /// </summary>
        public ChartLegend ChartLegend
        {
            set
            {
                if (value != null)
                {
                    value.SelectionChanged += LegendSelectionChanged;
                }
            }
        }

        /// <summary>
        /// Returns the indexed chart.
        /// </summary>
        /// <param name="index">The chart index.</param>
        /// <returns>The indexed chart.</returns>
        public Chart this[int index]
        {
            get
            {
                return Controls[index] as Chart;
            }
        }

        /// <summary>Gets the number of visible charts.</summary>
        public int Count
        {
            get
            {
                return (from Control control in Controls where control.Visible select control).Count();
            }
        }

        /// <summary>Gets a list of all the visible charts.</summary>
        public List<Chart> Charts
        {
            get
            {
                return (from Control control in Controls where control.Visible select (control as Chart)).ToList();
            }
        }

        /// <summary>
        /// Called when the legend selection changes. Updates the highlight on all the charts.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event arguments.</param>
        private void LegendSelectionChanged(object sender, ChartLegend.SelectionChangedEventArgs e)
        {
            foreach (Control control in Controls)
            {
                var chart = control as HighlightChart;
                if (chart != null)
                {
                    chart.HighlightSeries(e.Selection);
                }
            }
        }

        /// <summary>
        /// Allocates and initialises the charts, one for each cell in the table.
        /// </summary>
        private void AllocateCharts()
        {
            if (ColumnCount > 0 && RowCount > 0)
            {
                Controls.Clear();

                for (int columnIndex = 0; columnIndex < ColumnCount * RowCount; ++columnIndex)
                {
                    for (int rowIndex = 0; rowIndex < RowCount; ++rowIndex)
                    {
                        var chart = new HighlightChart { Visible = false };
                        chart.ChartAreas.Add(new ChartArea());
                        chart.Titles.Add("");
                        chart.Dock = DockStyle.Fill;
                        chart.Margin = new Padding(
                            columnIndex == 0 ? 0 : 3,
                            rowIndex == 0 ? 0 : 3,
                            columnIndex == 0 ? 3 : 0,
                            0);

                        Controls.Add(chart, columnIndex, rowIndex);
                    }
                }
            }
        }
    }
}
