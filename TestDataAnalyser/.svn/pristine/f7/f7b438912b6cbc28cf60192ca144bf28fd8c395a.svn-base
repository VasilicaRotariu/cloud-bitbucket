﻿namespace SudburyDataAnalyser
{
    /// <summary>
    /// Basic implementation of a generic singleton class.
    /// </summary>
    /// <typeparam name="T">The type of singleton object required.</typeparam>
    public class Singleton<T> where T : class, new()
    {
        /// <summary>
        /// Gets the instance of the singleton.
        /// </summary>
        public static T Instance
        {
            get
            {
                if (theObject == null)
                {
                    lock (LockObject)
                    {
                        if (theObject == null)
                        {
                            theObject = new T();
                        }
                    }
                }

                return theObject;
            }
        }

        /// <summary>
        /// Lock used to lock access to the object to ensure it is only created once.
        /// </summary>
        // ReSharper disable once StaticFieldInGenericType
        private static readonly object LockObject = new object();

        /// <summary>
        /// The instance of the singleton object.
        /// </summary>
        private static T theObject;
    }
}
