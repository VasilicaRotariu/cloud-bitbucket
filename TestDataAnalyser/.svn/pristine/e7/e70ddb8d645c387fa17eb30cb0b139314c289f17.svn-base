﻿using System.Drawing;

namespace SudburyDataAnalyser
{
    /// <summary>
    /// Class used to decide on series colours.
    /// </summary>
    public class SeriesColour
    {
        /// <summary>
        /// Sets series colour for group in which the current series falls within.
        /// E.g. if grouping by LOP, all LOPs with the same number will have the same colour,
        /// but different LOP will have a different colour - so series colour coded by LOP.
        /// </summary>
        /// <param name="item">Item index.</param>
        /// <returns>Colour for series, based on item index.</returns>
        public static Color BetterColours(int item)
        {
            var colours = new[] 
            {
                Color.Blue, Color.OrangeRed, Color.Indigo, Color.LimeGreen,
                Color.Gray, Color.Gold, Color.DeepSkyBlue, Color.SpringGreen,
                Color.Maroon, Color.Aquamarine, Color.Magenta, Color.DarkOrange,
                Color.RoyalBlue, Color.LawnGreen, Color.LightCoral, Color.Violet,
                Color.DarkKhaki, Color.Teal, Color.Pink, Color.Olive
            };

            return colours[item % colours.Length];
        }

        /// <summary>
        /// Sets series colour for group in which the current series falls within.
        /// E.g. if grouping by LOP, all LOPs with the same number will have the same colour,
        /// but different LOP will have a different colour - so series colour coded by LOP.
        /// </summary>
        /// <param name="item">Item index.</param>
        /// <returns>Colour for series, based on item index.</returns>
        public static Color BetterColours(string item)
        {
            return BetterColours(item.ToInt());
        }

        /// <summary>
        /// Sets series colour for pass or fail.
        /// </summary>
        /// <param name="result">Pass/fail flag for current series.</param>
        /// <returns>Colour for series, based on pass/fail result.</returns>
        public static Color PassFailCustomPalette(int result)
        {
            if (result == 0 || result == -2 || result == -3)
            {
                return Color.Green;
            }

            return Color.Red;
        }

        /// <summary>
        /// Sets series colour for group in which the current series falls within.
        /// E.g. if grouping by LOP, all LOPs with the same number will have the same colour,
        /// but different LOP will have a different colour - so series colour coded by LOP.
        /// </summary>
        /// <param name="item">Item index.</param>
        /// <returns>Colour for series, based on item index.</returns>
        public static Color FrequencyPlotColours(int item)
        {
            var colours = new[] 
            {
                Color.Blue, Color.Red, Color.Green, Color.Indigo, 
                Color.DarkGray, Color.OrangeRed, Color.SpringGreen, Color.Maroon, 
                Color.Aquamarine, Color.Magenta, Color.DarkOrange, Color.RoyalBlue,
                Color.LawnGreen, Color.LightCoral, Color.Violet, Color.Black
            };

            return colours[item % colours.Length];
        }
    }
}
