﻿using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// The PowerPoint button class, based on the picture box button.
    /// </summary>
    public class PowerPointButton : PictureBoxButton
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="PowerPointButton"/> class.
        /// </summary>
        public PowerPointButton()
            : base(null, Properties.Resources.PowerPoint)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="PowerPointButton"/> class.
        /// </summary>
        /// <param name="parent">The buttons parent.</param>
        public PowerPointButton(Control parent)
            : base(parent, Properties.Resources.PowerPoint)
        {
        }

        /// <summary>
        /// Copies a chart into a Power Point document.
        /// </summary>
        protected override void OnMouseClick()
        {
            var tag = Tag as string;
            if (tag == null) return;

            var chartScrollableLegend = ControlExt.GetControl<ChartSplitLegend>(tag);
            if (chartScrollableLegend != null)
            {
                var individualChartCheckBox = ControlExt.GetControl<CheckBox>(tag + "IndividualCharts");
                if (individualChartCheckBox == null || !individualChartCheckBox.Checked)
                {
                    chartScrollableLegend.ExportToPowerPoint();
                }
                else
                {
                    var legendCheckBox = ControlExt.GetControl<CheckBox>(tag + "LegendCheckBox");
                    var showLegend = (legendCheckBox != null) && legendCheckBox.Checked;

                    Singleton<PowerPointHelper>.Instance.Export(chartScrollableLegend.Title, ControlExt.GetControl<TableOfCharts>(tag + "s"), showLegend);
                }
            }

            var chart = ControlExt.GetControl<Chart>(tag);
            if (chart != null)
            {
                Singleton<PowerPointHelper>.Instance.Export(chart, false);
            }
        }
    }
}
