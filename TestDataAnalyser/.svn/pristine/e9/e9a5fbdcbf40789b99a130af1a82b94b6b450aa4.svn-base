﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Class for the Chart and Legend with splitter, based on a split container.
    /// </summary>
    public class ChartSplitLegend : SplitContainer
    {
        /// <summary>
        /// Event triggered when Chart detected a mouse down event
        /// </summary>
        [Description("Set Chart Event For mouse Down"),
            Bindable(false),
            EditorBrowsable(EditorBrowsableState.Always),
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
            Browsable(true)]
        public event EventHandler<MouseEventArgs> OnChartMouseDown = delegate { };

         /// <summary>
        /// Event triggered when Chart detected a Legend Item Right Click
        /// </summary>
        [Description("Set Chart Event For Legend Item Right Click"),
           Bindable(false),
           EditorBrowsable(EditorBrowsableState.Always),
           DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
           Browsable(true)]
        public event EventHandler OnLegendItemChange = delegate { };

        /// <summary>Gets the chart</summary>
        public HighlightChart Chart { get; private set; }

        /// <summary>Gets the legend</summary>
        public ChartLegend Legend { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="ChartSplitLegend"/> class.
        /// </summary>
        public ChartSplitLegend()
        {
            Chart = new HighlightChart();
            Legend = new ChartLegend();

            Panel1.Controls.Add(this.Chart);
            Panel2.Controls.Add(this.Legend);

            var title = new Title();
            title.Font = new System.Drawing.Font(title.Font.FontFamily.Name, 12, System.Drawing.FontStyle.Bold);

            this.Chart.Dock = DockStyle.Fill;
            this.Chart.ChartAreas.Add(new ChartArea());
            this.Chart.Titles.Add(title);
            this.Chart.MouseDown += (s, e) => { OnChartMouseDown(s, e); };

            this.Legend.Dock = DockStyle.Fill;
            this.Legend.SelectionChanged += LegendSelectionChanged;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the Legend is visible or not. 
        /// Set false, the Legend and splitter will not be visible.Set true, the 
        /// Chart, Legend and splitter will all be visible. Hiding the Legend will 
        /// also clear the series selection in the Chart.
        /// </summary>
        public bool ShowLegend
        {
            get
            {
                return !Panel2Collapsed;
            }

            set
            {
                if (value)
                {
                    Chart.HighlightSeries(Legend.Selection);
                    Panel2Collapsed = false;
                    Panel2.Show();
                }
                else
                {
                    Chart.ClearHighlight();
                    Panel2Collapsed = true;
                    Panel2.Hide();
                }
            }
        }

        /// <summary>
        /// Gets or sets the Chart title.
        /// </summary>
        [Description("Set the Chart title"),
            Bindable(true),
            EditorBrowsable(EditorBrowsableState.Always),
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
            Browsable(true)]
        public string Title
        {
            get
            {
                return Chart.Titles[0].Text;
            }

            set
            {
                Chart.Titles[0].Text = value;
            }
        }

        /// <summary>
        /// Sets a value indicating whether the Chart is a logarithmic Chart or not.
        /// </summary>
        public bool Logarithmic
        {
            set
            {
                Chart.ChartAreas[0].AxisX.IsLogarithmic = value;
                if (value)
                {
                    Chart.ChartAreas[0].AxisX.MinorGrid.Interval = 1;
                    Chart.ChartAreas[0].AxisX.MinorTickMark.Interval = 1;
                }

                Chart.ChartAreas[0].CursorX.IsUserSelectionEnabled = Chart.ChartAreas[0].CursorY.IsUserSelectionEnabled = true;
            }
        }

        /// <summary>
        /// Clears the Chart and Legend of all series data.
        /// </summary>
        public void Clear()
        {
            Chart.ChartAreas[0].AxisX.IsLogarithmic = false;
            Chart.Series.Clear();
            Legend.Clear();
            Logarithmic = false;
        }

        /// <summary>
        /// Adds a series to the Chart and Legend.
        /// </summary>
        /// <param name="series">The series to add to the Chart and Legend.</param>
        public void Add(Series series)
        {
            Chart.Series.Add(series);
            Legend.Add(series);
        }

        /// <summary>
        /// Automatically scales the Chart axis.
        /// </summary>
        /// <param name="postfixStrings">Strings used to find settings in the SQL database.</param>
        public void ScaleAxis(List<string> postfixStrings)
        {
            ChartAxisScaling.AxisScaler.ScaleAxis(Chart.ChartAreas[0], Name, postfixStrings);
        }

        /// <summary>
        /// Exports the Chart to a PowerPoint presentation.
        /// </summary>
        public void ExportToPowerPoint()
        {
            Chart.ClearHighlight();
            Singleton<PowerPointHelper>.Instance.Export(Chart, ShowLegend);
            Chart.HighlightSeries(Legend.Selection);
        }

        /// <summary>
        /// Copies the Chart to the clipboard. Adds a Legend to the Chart (on the clipboard) if the
        /// Legend is visible.
        /// </summary>
        public void CopyToClipboard()
        {
            Legend chartLegend = null;

            Chart.ClearHighlight();

            if (ShowLegend)
            {
                chartLegend = Chart.AddLegendIfAbsent();
            }

            Chart.CopyToClipboard();
            Chart.RemoveLegend(chartLegend);

            Chart.HighlightSeries(Legend.Selection);
        }

        /// <summary>
        /// Loads the Chart and Legend from a Memory stream
        /// </summary>
        /// <param name="stream">the memory stream object</param>
        public void LoadFromStream(MemoryStream stream)
        {
            //Load the Chart from the stream
            Chart.Serializer.Load(stream);

            //Take the Legend off the other Chart
            Chart.AddLegendIfAbsent();
            var oldLegend = Chart.Legends[0];
            Chart.RemoveLegend(oldLegend);

            //Build a new Chart Legend
            Legend.Clear();

            foreach (Series s in Chart.Series)
                Legend.Add(s);
        }

        /// <summary>
        /// Handles selection change events send from the Legend. Updates the selection on the Chart.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event arguments.</param>
        private void LegendSelectionChanged(object sender, ChartLegend.SelectionChangedEventArgs e)
        {
            Chart.HighlightSeries(e.Selection);
            OnLegendItemChange(sender, e);
        }
    }
}
