﻿using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.UniqueTestPoints
{
    /// <summary>
    /// Represents a single unique test point record.
    /// </summary>
    public class Row : RowBase
    {
        /// <summary>Gets or sets TestPoint.</summary>
        public short TestPoint { get; protected set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="UniqueTestPoints.Row"/> class.
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        public override void Initialise(SqlDataReader reader)
        {
            TestPoint = ReadShort(reader, "TestPoint");
        }
    }
}
