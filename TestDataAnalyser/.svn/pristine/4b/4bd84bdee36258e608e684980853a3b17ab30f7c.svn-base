﻿using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.Leak
{
    /// <summary>
    /// Represents a single record (one test point for one injector) of leak data.
    /// </summary>
    public class Row : UTIDRow
    {
        /// <summary>Gets test point.</summary>
        public short TestPoint { get; private set; }

        /// <summary>Gets measured pressure.</summary>
        public short Pressure { get; private set; }

        /// <summary>Gets elapsed time.</summary>
        public double OffTime { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="Leak.Row"/> class.
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        public override void Initialise(SqlDataReader reader)
        {
            Initialise(reader, UTIDRowParameters.All);
            Initialise(reader, NameRowParameters.IDENT, NameRowParameters.PressureReq);

            TestPoint = ReadShort(reader, "TestPoint");
            Pressure = ReadShort(reader, "Pressure");
            OffTime = ReadDoubleFromString(reader, "OffTime");
        }
    }
}
