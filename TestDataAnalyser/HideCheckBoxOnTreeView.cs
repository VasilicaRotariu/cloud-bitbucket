﻿namespace SudburyDataAnalyser
{
    using System;
    using System.Windows.Forms;
    using System.Runtime.InteropServices;
    
    /// <summary>
    /// This is for removing the parent CheckBoxes and is complicated.  Copied from the link at the top.
    /// </summary>
    public class HideCheckBoxOnTreeView
    {      
        /// <summary>Hexadecimal constant. </summary>
        // ReSharper disable once InconsistentNaming
        private const int TVIF_STATE = 0x8;

        /// <summary>Hexadecimal constant. </summary>
        // ReSharper disable once InconsistentNaming
        private const int TVIS_STATEIMAGEMASK = 0xF000;

        /// <summary>Hexadecimal constant. </summary>
        // ReSharper disable once InconsistentNaming
        private const int TV_FIRST = 0x1100;

        /// <summary>Hexadecimal constant. </summary>
        // ReSharper disable once InconsistentNaming
        private const int TVM_SETITEM = TV_FIRST + 63;

        /// <summary> Struct for removing/hiding checkboxes. </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 8, CharSet = CharSet.Auto)]
        // ReSharper disable MemberCanBePrivate.Local
        // ReSharper disable FieldCanBeMadeReadOnly.Local
        // ReSharper disable once InconsistentNaming
        private struct TVITEM
        {
            /// <summary>Mask integer.</summary>
            public int Mask;

            /// <summary>Item pointer.</summary>
            public IntPtr Item;

            /// <summary>State integer.</summary>
            public int State;

            /// <summary>State mask integer.</summary>
            public int StateMask;

            /// <summary>A string.</summary>
            [MarshalAs(UnmanagedType.LPTStr)]
            public string LpszText;

            /// <summary>Maximum text integer.</summary>
            public int CchTextMax;

            /// <summary>Image integer.</summary>
            public int Image;

            /// <summary>Selected image integer.</summary>
            public int SelectedImage;

            /// <summary>Children integer.</summary>
            public int Children;

            /// <summary>Parameter pointer.</summary>
            public IntPtr Param;

            // ReSharper restore MemberCanBePrivate.Local
            // ReSharper restore FieldCanBeMadeReadOnly.Local
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SendMessage(IntPtr window, int message, IntPtr param, ref TVITEM tviParam);

        /// <summary>
        /// Hides the CheckBox for the specified node on a TreeView control.
        /// </summary>
        /// <param name="tvw">Selected TreeView.</param>
        /// <param name="node">Selected TreeNode</param>                                                                                                 
        public void HideCheckBox(TreeView tvw, TreeNode node)
        {
            var tvi = new TVITEM { Item = node.Handle, Mask = TVIF_STATE, StateMask = TVIS_STATEIMAGEMASK, State = 0 };
            tvw.InvokeIfRequired(c => SendMessage(c.Handle, TVM_SETITEM, IntPtr.Zero, ref tvi));
        }
    }
}
