﻿using System.Data.SqlClient;

namespace SudburyDataAnalyser.ChartAxisScaling
{
    /// <summary>
    /// Represents a single chart axis configuration record.
    /// </summary>
    public class Row : RowBase
    {
        /// <summary>
        /// Gets the name of the configuration.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the name of the configuration this configuration defaults to.
        /// </summary>
        public string Defaults { get; private set; }

        /// <summary>
        /// Gets the configuration parameters.
        /// </summary>
        public ChartAxisParameters Parameters { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Row"/> class.
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        public override void Initialise(SqlDataReader reader)
        {
            this.Name = this.ReadString(reader, "Name");
            this.Defaults = this.ReadString(reader, "Defaults");

            this.Parameters = new ChartAxisParameters
            {
                Minimum = ReadDoubleFromString(reader, "Minimum"),
                Maximum = ReadDoubleFromString(reader, "Maximum"),
                Interval = ReadDoubleFromString(reader, "Interval"),
                MajorTick = ReadDoubleFromString(reader, "MajorTick"),
                MinorTick = ReadDoubleFromString(reader, "MinorTick"),
                MajorGrid = ReadDoubleFromString(reader, "MajorGrid"),
                MinorGrid = ReadDoubleFromString(reader, "MinorGrid")
            };
        }
    }
}
