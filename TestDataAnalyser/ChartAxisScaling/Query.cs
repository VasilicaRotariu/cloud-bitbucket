﻿namespace SudburyDataAnalyser.ChartAxisScaling
{
    /// <summary>
    /// Query to get chart axis configuration from the database.
    /// </summary>
    public class Query : QueryBase<Row>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        public Query(Database.DatabaseBase database)
            : base(database)
        {
        }

        /// <summary>
        /// Returns the SQL query to get the chart axis configuration from the database.
        /// </summary>
        /// <returns>The SQL query to get the chart axis configuration from the database.</returns>
        protected override string GetQuery()
        {
            return "SELECT * from AnalyserChartAxisSettings";
        }
    }
}
