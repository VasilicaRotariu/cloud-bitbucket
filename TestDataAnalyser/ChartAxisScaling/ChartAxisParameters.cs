﻿using System.Diagnostics.CodeAnalysis;
using System.Windows.Forms.DataVisualization.Charting;

namespace SudburyDataAnalyser.ChartAxisScaling
{
    /// <summary>
    /// Class used to encapsulate the parameters used to configure a chart axis.
    /// </summary>
    public class ChartAxisParameters
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ChartAxisParameters"/> class.
        /// </summary>
        public ChartAxisParameters()
        {
            this.Minimum =
            this.Maximum =
            this.Interval =
            this.MajorTick =
            this.MinorTick =
            this.MajorGrid =
            this.MinorGrid = double.NaN;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ChartAxisParameters"/> class.
        /// </summary>
        /// <param name="minimum">The minimum value.</param>
        /// <param name="maximum">The maximum value.</param>
        public ChartAxisParameters(double minimum, double maximum)
        {
            this.Minimum = minimum;
            this.Maximum = maximum;
            this.Interval =
            this.MajorTick =
            this.MinorTick =
            this.MajorGrid =
            this.MinorGrid = double.NaN;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ChartAxisParameters"/> class.
        /// </summary>
        /// <param name="minimum">The minimum value.</param>
        /// <param name="maximum">The maximum value.</param>
        /// <param name="interval">The interval value.</param>
        /// <param name="majorTick">The major tick value.</param>
        /// <param name="minorTick">The minor tick value.</param>
        /// <param name="majorGrid">The major grid value.</param>
        /// <param name="minorGrid">The minor grid value.</param>
        public ChartAxisParameters(double minimum, double maximum, double interval, double majorTick, double minorTick, double majorGrid, double minorGrid)
        {
            this.Minimum = minimum;
            this.Maximum = maximum;
            this.Interval = interval;
            this.MajorTick = majorTick;
            this.MinorTick = minorTick;
            this.MajorGrid = majorGrid;
            this.MinorGrid = minorGrid;
        }

        /// <summary> Gets a value indicating whether the axis parameters are valid. </summary>
        public bool Valid
        {
            get
            {
                return !(double.IsNaN(this.Minimum) ||
                         double.IsNaN(this.Maximum) ||
                         double.IsNaN(this.Interval) ||
                         double.IsNaN(this.MajorTick) ||
                         double.IsNaN(this.MinorTick) ||
                         double.IsNaN(this.MajorGrid) ||
                         double.IsNaN(this.MinorGrid));
            }
        }

        /// <summary> Gets or sets the axis minimum value. </summary>
        public double Minimum { get; set; }

        /// <summary> Gets or sets the axis maximum value. </summary>
        public double Maximum { get; set; }

        /// <summary> Gets or sets the axis interval value. </summary>
        public double Interval { get; set; }

        /// <summary> Gets or sets the axis major tick value. </summary>
        public double MajorTick { get; set; }

        /// <summary> Gets or sets the axis minor tick value. </summary>
        public double MinorTick { get; set; }

        /// <summary> Gets or sets the axis major grid value. </summary>
        public double MajorGrid { get; set; }

        /// <summary> Gets or sets the axis minor grid value. </summary>
        public double MinorGrid { get; set; }

        /// <summary>
        /// Merges the parameters with the parameters provided.
        /// Minimum is merged by making minimum the lower of the minimum parameters.
        /// Maximum is merged by making maximum the larger of the maximum parameters.
        /// The remaining parameters are copied from the parameters provided if not
        /// previously set.
        /// </summary>
        /// <param name="parameters">The parameters to merge with the parameters.</param>
        public void Merge(ChartAxisParameters parameters)
        {
            this.SetIfNotSet(parameters);

            if (double.IsNaN(this.Minimum) || 
                (!double.IsNaN(parameters.Minimum) && (this.Minimum > parameters.Minimum)))
            {
                this.Minimum = parameters.Minimum;
            }

            if (double.IsNaN(this.Maximum) ||
                (!double.IsNaN(parameters.Maximum) && (this.Maximum < parameters.Maximum)))
            {
                this.Maximum = parameters.Maximum;
            }
        }

        /// <summary>
        /// Copies the parameters provided to the parameters when the parameter has not 
        /// already been set.
        /// </summary>
        /// <param name="parameters">The parameters to copy.</param>
        [SuppressMessage("StyleCop.CSharp.LayoutRules", "SA1503:CurlyBracketsMustNotBeOmitted", Justification = "Reviewed.")]
        public void SetIfNotSet(ChartAxisParameters parameters)
        {
            if (double.IsNaN(this.Minimum)) this.Minimum = parameters.Minimum;
            if (double.IsNaN(this.Maximum)) this.Maximum = parameters.Maximum;
            if (double.IsNaN(this.Interval)) this.Interval = parameters.Interval;
            if (double.IsNaN(this.MajorTick)) this.MajorTick = parameters.MajorTick;
            if (double.IsNaN(this.MinorTick)) this.MinorTick = parameters.MinorTick;
            if (double.IsNaN(this.MajorGrid)) this.MajorGrid = parameters.MajorGrid;
            if (double.IsNaN(this.MinorGrid)) this.MinorGrid = parameters.MinorGrid;
        }

        /// <summary>
        /// Copies the parameters provided (where set) to the parameters, overwriting
        /// any existing value.
        /// </summary>
        /// <param name="parameters">The parameters to copy.</param>
        [SuppressMessage("StyleCop.CSharp.LayoutRules", "SA1503:CurlyBracketsMustNotBeOmitted", Justification = "Reviewed.")]
        public void Override(ChartAxisParameters parameters)
        {
            if (!double.IsNaN(parameters.Minimum)) this.Minimum = parameters.Minimum;
            if (!double.IsNaN(parameters.Maximum)) this.Maximum = parameters.Maximum;
            if (!double.IsNaN(parameters.Interval)) this.Interval = parameters.Interval;
            if (!double.IsNaN(parameters.MajorTick)) this.MajorTick = parameters.MajorTick;
            if (!double.IsNaN(parameters.MinorTick)) this.MinorTick = parameters.MinorTick;
            if (!double.IsNaN(parameters.MajorGrid)) this.MajorGrid = parameters.MajorGrid;
            if (!double.IsNaN(parameters.MinorGrid)) this.MinorGrid = parameters.MinorGrid;
        }

        /// <summary>
        /// Uses the parameters (where set) to configure the axis.
        /// </summary>
        /// <param name="axis">The axis to configure.</param>
        [SuppressMessage("StyleCop.CSharp.LayoutRules", "SA1503:CurlyBracketsMustNotBeOmitted", Justification = "Reviewed.")]
        public void ConfigureAxis(Axis axis)
        {
            if (!double.IsNaN(this.Minimum)) axis.Minimum = this.Minimum;
            if (!double.IsNaN(this.Maximum)) axis.Maximum = this.Maximum;
            if (!double.IsNaN(this.Interval)) axis.Interval = this.Interval;
            if (!double.IsNaN(this.MajorTick)) axis.MajorTickMark.Interval = this.MajorTick;
            if (!double.IsNaN(this.MinorTick)) axis.MinorTickMark.Interval = this.MinorTick;
            if (!double.IsNaN(this.MajorGrid)) axis.MajorGrid.Interval = this.MajorGrid;
            if (!double.IsNaN(this.MinorGrid)) axis.MinorGrid.Interval = this.MinorGrid;
        }
    }
}
