﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace SudburyDataAnalyser.ChartAxisScaling
{
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Class used to load axis configuration from a XML configuration file.
    /// </summary>
    public class XmlConfiguration
    {
        /// <summary>
        /// Loads and returns axes configuration from the file specified.
        /// </summary>
        /// <param name="fileName">The name and path of the XML file.</param>
        /// <returns>The axes configuration.</returns>
        [SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:ElementsMustAppearInTheCorrectOrder", Justification = "Reviewed.")]
        public Dictionary<string, ChartAxisParameters> Load(string fileName)
        {
            var namedParameters = new Dictionary<string, ChartAxisParameters>();

            if (!File.Exists(fileName))
            {
                return namedParameters;
            }

            try
            {
                var document = new XmlDocument();
                document.Load(fileName);

                var rootNode = document.FirstChild;
                while ((rootNode != null) && (rootNode.Name.ToLower() != RootNode))
                {
                    rootNode = rootNode.NextSibling;
                }

                if (rootNode == null)
                {
                    return namedParameters;
                }

                var childNode = rootNode.FirstChild;
                while (childNode != null)
                {
                    if (childNode.Attributes == null)
                    {
                        continue;
                    }

                    var name = string.Empty;
                    var parameters = new ChartAxisParameters();

                    foreach (XmlAttribute attribute in childNode.Attributes)
                    {
                        switch (attribute.Name.ToLower())
                        {
                            case NameAttribute:
                            {
                                name = attribute.Value;
                                break;
                            }             

                            case MinimumAttribute:
                            {
                                parameters.Minimum = attribute.Value.ToDouble();
                                break;
                            } 

                            case MaximumAttribute:
                            {
                                parameters.Maximum = attribute.Value.ToDouble();
                                break;
                            }  

                            case IntervalAttribute:
                            {
                                parameters.Interval = attribute.Value.ToDouble();
                                break;
                            }

                            case MajorTickAttribute:
                            {
                                parameters.MajorTick = attribute.Value.ToDouble();
                                break;
                            } 

                            case MinorTickAttribute:
                            {
                                parameters.MinorTick = attribute.Value.ToDouble();
                                break;
                            }
                                
                            case MajorGridAttribute:
                            {
                                parameters.MajorGrid = attribute.Value.ToDouble();
                                break;
                            }

                            case MinorGridAttribute:
                            {
                                parameters.MinorGrid = attribute.Value.ToDouble();
                                break;
                            }
                        }
                    }

                    if (!name.IsEmpty())
                    {
                        namedParameters[name] = parameters;
                    }

                    childNode = childNode.NextSibling;
                }
            }
            catch (Exception ex)
            {
                DebugLog.WriteLine("Error reading configuration file");
                DebugLog.WriteLine(ex.Message);
            }

            return namedParameters;
        }

        /// <summary>
        /// The name of the root node.
        /// </summary>
        private const string RootNode = "AnalyserAxisConfiguration";

        /// <summary>
        /// The name of the name attribute.
        /// </summary>
        private const string NameAttribute = "name";

        /// <summary>
        /// The name of the minimum attribute.
        /// </summary>
        private const string MinimumAttribute = "minimum";

        /// <summary>
        /// The name of the maximum attribute.
        /// </summary>
        private const string MaximumAttribute = "maximum";

        /// <summary>
        /// The name of the interval attribute.
        /// </summary>
        private const string IntervalAttribute = "interval";

        /// <summary>
        /// The name of the major tick attribute.
        /// </summary>
        private const string MajorTickAttribute = "majortick";

        /// <summary>
        /// The name of the minor tick attribute.
        /// </summary>
        private const string MinorTickAttribute = "minortick";

        /// <summary>
        /// The name of the major grid attribute.
        /// </summary>
        private const string MajorGridAttribute = "majorgrid";

        /// <summary>
        /// The name of the minor grid attribute.
        /// </summary>
        private const string MinorGridAttribute = "minorgrid";
    }
}
