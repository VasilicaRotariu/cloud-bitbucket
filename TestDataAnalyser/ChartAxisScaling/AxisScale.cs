﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Windows.Forms.DataVisualization.Charting;

namespace SudburyDataAnalyser.ChartAxisScaling
{
    /// <summary>
    /// Class used to configure a charts axis scale, ticks etc.
    /// The chart is configured based on the settings found in the database
    /// or found in the configuration file.
    /// </summary>
    public class AxisScaler
    {
        /// <summary> Constant for XAxis (to improve code readability) </summary>
        private const bool XAxis = true;

        /// <summary> Constant for YAxis (to improve code readability) </summary>
        private const bool YAxis = false;

        /// <summary>  Cache of all the axis parameters, to avoid frequently accessing the database and configuration file.  </summary>
        private static Dictionary<string, ChartAxisParameters> axisParameters;

        /// <summary>
        /// Initialises a new instance of the <see cref="AxisScaler"/> class.
        /// Uses the settings found in the database and configuration file.
        /// </summary>
        /// <param name="database">The database storing the configuration settings.</param>
        /// <param name="fileName">The name and path of the XML file storing configuration settings.</param>
        public AxisScaler(Database.DatabaseBase database, string fileName)
        {
            var query = new Query(database);
            var rows = query.Execute();

            axisParameters = new Dictionary<string, ChartAxisParameters>();

            if (rows.Count > 0)
            {
                // Initialise all the parameters with actual values
                foreach (Row row in rows)
                {
                    axisParameters[row.Name] = row.Parameters;
                }

                // Initialise all the parameters that reference default parameters.
                foreach (Row row in rows)
                {
                    if (row.Defaults.Length > 0)
                    {
                        axisParameters[row.Name].SetIfNotSet(axisParameters[row.Defaults]);
                    }
                }
            }

            var xmlConfig = new XmlConfiguration();
            var overrideRows = xmlConfig.Load(fileName);

            foreach (var row in overrideRows)
            {
                if (axisParameters.ContainsKey(row.Key))
                {
                    axisParameters[row.Key].Override(row.Value);
                }
                else
                {
                    axisParameters[row.Key] = row.Value;
                }
            }
        }

        /// <summary>
        /// Scales the X and Y axes for the chart area based on the name and extensions
        /// supplied. The name and extension are used to find the axes settings from
        /// the database or configuration file.
        /// </summary>
        /// <param name="chartArea">The chart area to configure.</param>
        /// <param name="name">The chart name used to configure axes.</param>
        /// <param name="extensions">A list of possible extensions used to configure the axes.</param>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation",
                         Justification = "Variables xAxisParameters and yAxisParameters are chart axis parameters for the X and Y axis.")]
        public static void ScaleAxis(ChartArea chartArea, string name, List<string> extensions)
        {
            // If the name contains an underscore, set the name to the text befoe the underscore.
            name = name.Split('_')[0];

            var xAxisParameters = new ChartAxisParameters();
            var yAxisParameters = new ChartAxisParameters();

            //jz5w78 - change the order of these two segments. 
            //  should have no affect on the program but can take into account the override functionality provided by DAF
            xAxisParameters.Merge(GetAxisParameters(name, XAxis));
            yAxisParameters.Merge(GetAxisParameters(name, YAxis));

            foreach (string extension in extensions)
            {
                xAxisParameters.Merge(GetAxisParameters(name, XAxis, extension));
                yAxisParameters.Merge(GetAxisParameters(name, YAxis, extension));
            }

            ScaleChartArea(chartArea, xAxisParameters, yAxisParameters);
        }

        /// <summary>
        /// Scales the X and Y axes for the chart area based on the name and extension
        /// supplied. The name and extension are used to find the axes settings from
        /// the database or configuration file.
        /// </summary>
        /// <param name="chartArea">The chart area to configure.</param>
        /// <param name="name">The chart name used to configure axes.</param>
        /// <param name="extension">Extension used to configure the axes.</param>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation",
                         Justification = "Variables xAxisParameters and yAxisParameters are chart axis parameters for the X and Y axis.")]
        public static void ScaleAxis(ChartArea chartArea, string name, string extension)
        {
            var xAxisParameters = new ChartAxisParameters();
            var yAxisParameters = new ChartAxisParameters();

            xAxisParameters.Merge(GetAxisParameters(name, XAxis, extension));
            yAxisParameters.Merge(GetAxisParameters(name, YAxis, extension));

            xAxisParameters.Merge(GetAxisParameters(name, XAxis));
            yAxisParameters.Merge(GetAxisParameters(name, YAxis));

            ScaleChartArea(chartArea, xAxisParameters, yAxisParameters);
        }

        /// <summary>
        /// Scales the X and Y axes for the chart area based on the name supplied.
        /// </summary>
        /// <param name="chartArea">The chart area to configure.</param>
        /// <param name="name">The chart name used to configure axes.</param>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation",
                         Justification = "Variables xAxisParameters and yAxisParameters are chart axis parameters for the X and Y axis.")]
        public static void ScaleAxis(ChartArea chartArea, string name)
        {
            var xAxisParameters = new ChartAxisParameters();
            var yAxisParameters = new ChartAxisParameters();

            xAxisParameters.Merge(GetAxisParameters(name, XAxis));
            yAxisParameters.Merge(GetAxisParameters(name, YAxis));

            ScaleChartArea(chartArea, xAxisParameters, yAxisParameters);
        }

        /// <summary>
        /// Gets the axis parameters based on the name, the axis and the extension.
        /// </summary>
        /// <param name="name">The name (of the chart) to get axis parameters for.</param>
        /// <param name="xAxis">The axis (true for X axis, false for Y axis) to get axis parameters for.</param>
        /// <param name="extension">An (name) extension used to get the axis parameters.</param>
        /// <returns>Axis parameters.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation",
                         Justification = "Variables xAxis is a flag indicating if the axis is the X axis or Y axis.")]
        private static ChartAxisParameters GetAxisParameters(string name, bool xAxis, string extension)
        {
            ChartAxisParameters parameters;
            if (axisParameters.TryGetValue("{0}_{1}_{2}".Args(name, AxisText(xAxis), extension), out parameters))
            {
                return parameters;
            }

            return new ChartAxisParameters();
        }

        /// <summary>
        /// Gets the axis parameters based on the name and the axis.
        /// </summary>
        /// <param name="name">The name (of the chart) to get axis parameters for.</param>
        /// <param name="xAxis">The axis (true for X axis, false for Y axis) to get axis parameters for.</param>
        /// <returns>Axis parameters.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation",
                         Justification = "Variables xAxis is a flag indicating if the axis is the X axis or Y axis.")]
        private static ChartAxisParameters GetAxisParameters(string name, bool xAxis)
        {
            ChartAxisParameters parameters;
            if (axisParameters.TryGetValue("{0}_{1}".Args(name, AxisText(xAxis)), out parameters))
            {
                return parameters;
            }

            return new ChartAxisParameters();
        }

        /// <summary>
        /// Converts a boolean to string representing an axis.
        /// </summary>
        /// <param name="xAxis">The required axis.</param>
        /// <returns>The string representation of the axis.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation",
                         Justification = "Variables xAxis is a flag indicating if the axis is the X axis or Y axis.")]
        private static string AxisText(bool xAxis)
        {
            return xAxis ? "X" : "Y";
        }

        /// <summary>
        /// Applies the chart axis parameters to the X and Y axis of the chart area. If the axis parameters
        /// are not valid, the axis parameters are calculated (based on the data) and then any valid axis
        /// parameters override the calculated values.
        /// </summary>
        /// <param name="chartArea">The chart area.</param>
        /// <param name="xParameters">The X axis parameters.</param>
        /// <param name="yParameters">The Y axis parameters.</param>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation",
                         Justification = "Variables xParameters and yParameters are parameters for the X and Y axis.")]
        private static void ScaleChartArea(ChartArea chartArea, ChartAxisParameters xParameters, ChartAxisParameters yParameters)
        {
            if (!xParameters.Valid || !yParameters.Valid)
            {
                chartArea.RecalculateAxesScale();
                chartArea.AxisX.RoundAxisValues();
                chartArea.AxisY.RoundAxisValues();
            }

            xParameters.ConfigureAxis(chartArea.AxisX);
            yParameters.ConfigureAxis(chartArea.AxisY);
        }
    }
}
