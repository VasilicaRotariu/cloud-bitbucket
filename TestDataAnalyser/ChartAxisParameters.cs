﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SudburyDataAnalyser
{
    /// <summary>
    /// Class used to encapsulate the parameters used to configure a chart axis.
    /// </summary>
    public class ChartAxisParameters
    {
        /// <summary>
        /// Gets the axis minimum value.
        /// </summary>
        public double Minimum { get; private set; }

        /// <summary>
        /// Gets the axis maximum value.
        /// </summary>
        public double Maximum { get; private set; }

        /// <summary>
        /// Gets the axis interval value.
        /// </summary>
        public double Interval { get; private set; }

        /// <summary>
        /// Gets the axis major tick value.
        /// </summary>
        public double MajorTick { get; private set; }

        /// <summary>
        /// Gets the axis minor tick value.
        /// </summary>
        public double MinorTick { get; private set; }

        /// <summary>
        /// Gets the axis major grid value.
        /// </summary>
        public double MajorGrid { get; private set; }

        /// <summary>
        /// Gets the axis minor grid value.
        /// </summary>
        public double MinorGrid { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="ChartAxisParameters"/> class.
        /// </summary>
        /// <param name="minimum">The minimum value.</param>
        /// <param name="maximum">The maximum value.</param>
        /// <param name="interval">The interval value.</param>
        /// <param name="majorTick">The major tick value.</param>
        /// <param name="minorTick">The minor tick value.</param>
        /// <param name="majorGrid">The major grid value.</param>
        /// <param name="minorGrid">The minor grid value.</param>
        public ChartAxisParameters(double minimum, double maximum, double interval, double majorTick, double minorTick, double majorGrid, double minorGrid)
        {
            Minimum = minimum;
            Maximum = maximum;
            Interval = interval;
            MajorTick = majorTick;
            MinorTick = minorTick;
            MajorGrid = majorGrid;
            MinorGrid = minorGrid;
        }

        /// <summary>
        /// Default axis parameters.
        /// </summary>
        public static readonly ChartAxisParameters GradesAxisParameters = new ChartAxisParameters(-1.5, 1.5, 0.5, 0.5, 0.1, 0.5, 0.1);

        /// <summary>
        /// Axis parameters with min and max of -+100, interval and major values of 25 and minor values of 5.
        /// </summary>
        public static readonly ChartAxisParameters AxisParameters_60_20_5 = new ChartAxisParameters(-60, 60, 20, 20, 5, 20, 5);

        /// <summary>
        /// Axis parameters with min and max of -+100, interval and major values of 25 and minor values of 5.
        /// </summary>
        public static readonly ChartAxisParameters AxisParameters_100_25_5 = new ChartAxisParameters(-100, 100, 25, 25, 5, 25, 5);

        /// <summary>
        /// Axis parameters with min and max of -+200, interval and major values of 50 and minor values of 10.
        /// </summary>
        public static readonly ChartAxisParameters AxisParameters_200_50_10 = new ChartAxisParameters(-200, 200, 50, 50, 10, 50, 10);

        /// <summary>
        /// Axis parameters with min value of 0, max value of 1200, interval and major values of 200 and minor values of 100.
        /// </summary>
        public static readonly ChartAxisParameters AxisParameters_1200_200_100 = new ChartAxisParameters(0, 1200, 200, 200, 100, 200, 100);
    }
}
