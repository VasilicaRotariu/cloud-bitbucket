﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SudburyDataAnalyser
{
    /// <summary>
    /// Partial call of the Missing Injector Form
    /// </summary>
    public partial class MissingInjectorInformation : Form
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="MissingInjectorInformation" /> class
        /// </summary>
        /// <param name="missingISNs">A List of Missing Serial Numbers</param>
        public MissingInjectorInformation(IEnumerable<string> missingISNs)
        {
            InitializeComponent();

            missingInjectorDataGrid.SetColumns("ISN");

            foreach (string isn in missingISNs)
                missingInjectorDataGrid.Rows.Add(isn);
        }
    }
}
