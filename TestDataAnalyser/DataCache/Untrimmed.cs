﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SudburyDataAnalyser.Untrimmed
{
    /// <summary>
    /// The DataCache for Untrimmed Data points
    /// </summary>
    public sealed class DataCache : UtidDataCache<Row>
    { 
        /// <summary>
        /// Gets the row for the untrimmed data cache
        /// </summary>
        /// <param name="database">The current database</param>
        /// <param name="keys">The Plant number and UTID Keys</param>
        /// <returns>the List of Row data</returns>
        protected override List<Row> GetRows(Database.DatabaseBase database, UTIDPlantNumberKeys keys)
        {
            return new Query(database, keys).Execute();
        }
    }
}
