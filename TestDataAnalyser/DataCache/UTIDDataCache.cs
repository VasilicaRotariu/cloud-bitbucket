﻿using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Base class for cached data rows classes. Cached data row classes cache rows 
    /// returned from SQL queries so that the rows can be reused.
    /// </summary>
    /// <typeparam name="T">The row type.</typeparam>
    public abstract class UtidDataCache<T> where T : UTIDRow, new()
    {
        /// <summary>
        /// The row cache.
        /// </summary>
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private Dictionary<UTIDPlantNumberKey, List<T>> rows = new Dictionary<UTIDPlantNumberKey, List<T>>();

        /// <summary>
        /// Clears the cache of all rows.
        /// </summary>
        public void Clear()
        {
            rows.Clear();
        }

        /// <summary>
        /// Returns the rows for the list of UTIDs. The rows will either be taken
        /// from the cache or if they are not present in the cache, they will be
        /// retrieved from the database and also placed in the cache.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="keys">The keys specifying the required rows.</param>
        /// <returns>The rows of data.</returns>
        public List<T> Get(Database.DatabaseBase database, UTIDPlantNumberKeys keys)
        {
            var rowsToReturn = new List<T>();
            var rowsToGet = new UTIDPlantNumberKeys();

            foreach (var key in keys)
            {
                List<T> keyRows;
                if (rows.TryGetValue(key, out keyRows))
                {
                    rowsToReturn.AddRange(keyRows);
                }
                else
                {
                    rowsToGet.Add(key);
                    rows[key] = new List<T>();
                }
            }

            if (rowsToGet.Count > 0)
            {
                // If an exception is thrown, return no rows.
                try
                {
                    foreach (var row in GetRows(database, rowsToGet))
                    {
                        rows[new UTIDPlantNumberKey(row.UTID, row.PlantNumber)].Add(row);
                        rowsToReturn.Add(row);
                    }
                }
                catch
                {
                    rowsToReturn.Clear();
                }
            }

            return rowsToReturn;
        }

        /// <summary>
        /// Get rows of data from the database.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="keys">The keys specifying the required rows.</param>
        /// <returns>The rows from the database.</returns>
        protected abstract List<T> GetRows(Database.DatabaseBase database, UTIDPlantNumberKeys keys);
    }
}
