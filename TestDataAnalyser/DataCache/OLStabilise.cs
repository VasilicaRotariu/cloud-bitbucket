﻿using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.OLStabilise
{
    /// <summary>
    /// Open loop stabilise data cache class.
    /// </summary>
    public class DataCache : UtidDataCache<Row>
    {
        /// <summary>
        /// Get rows of data from the database.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="keys">The keys specifying the required rows.</param>
        /// <returns>The rows from the database.</returns>
        protected override List<Row> GetRows(Database.DatabaseBase database, UTIDPlantNumberKeys keys)
        {
            return new Query(database, keys).Execute();
        }
    }
}
