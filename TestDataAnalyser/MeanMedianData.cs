﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SudburyDataAnalyser
{
    /// <summary>
    /// Partial class for the MeanMedian Data form containing Logic
    /// </summary>
    public partial class MeanMedianData : Form
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="MeanMedianData" /> class
        /// </summary> 
        public MeanMedianData()
        {
            InitializeComponent();

            this.data.Columns.Add("Pressure", "Pressure");
            this.data.Columns.Add("NominalFuelPoint", "Nominal Fuel Point");
            this.data.Columns.Add("Value", "Value");
        }

        /// <summary>
        /// Adds a Dataset to the Data Form
        /// </summary>
        /// <param name="pressure">The Pressure</param>
        /// <param name="data">The Dataset</param>
        public void AddDataSet(string pressure, List<Tuple<double, double>> data)
        { 
             foreach (var row in data) 
                 this.data.Rows.Add(pressure, row.Item1, row.Item2); 
        }

        /// <summary>
        /// Clears all data currently in the form
        /// </summary>
        public void ClearData()
        {
            this.data.Rows.Clear(); 
        }

        /// <summary>
        /// Handles on form closing, hide not close.
        /// </summary>
        /// <param name="sender">The Sender</param>
        /// <param name="e">Some arguments</param>
        private void MeanMedianData_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }
    }
}
