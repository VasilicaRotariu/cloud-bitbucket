﻿namespace SudburyDataAnalyser
{
    partial class ChangelogReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelButton1 = new SudburyDataAnalyser.LabelButton();
            this.txtCurrentVersion = new System.Windows.Forms.Label();
            this.rtbChangeLog = new System.Windows.Forms.RichTextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.labelButton1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtCurrentVersion, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.rtbChangeLog, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnClose, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(449, 395);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // labelButton1
            // 
            this.labelButton1.AutoSize = true;
            this.labelButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelButton1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelButton1.Location = new System.Drawing.Point(3, 0);
            this.labelButton1.Name = "labelButton1";
            this.labelButton1.Size = new System.Drawing.Size(218, 19);
            this.labelButton1.TabIndex = 0;
            this.labelButton1.Text = "Current Version";
            this.labelButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCurrentVersion
            // 
            this.txtCurrentVersion.AutoSize = true;
            this.txtCurrentVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCurrentVersion.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrentVersion.Location = new System.Drawing.Point(227, 0);
            this.txtCurrentVersion.Name = "txtCurrentVersion";
            this.txtCurrentVersion.Size = new System.Drawing.Size(219, 19);
            this.txtCurrentVersion.TabIndex = 1;
            this.txtCurrentVersion.Text = "0.0.0.0";
            this.txtCurrentVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rtbChangeLog
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.rtbChangeLog, 2);
            this.rtbChangeLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbChangeLog.Enabled = false;
            this.rtbChangeLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbChangeLog.Location = new System.Drawing.Point(3, 22);
            this.rtbChangeLog.Name = "rtbChangeLog";
            this.rtbChangeLog.Size = new System.Drawing.Size(443, 341);
            this.rtbChangeLog.TabIndex = 2;
            this.rtbChangeLog.Text = "";
            // 
            // btnClose
            // 
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnClose.Location = new System.Drawing.Point(227, 369);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(219, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // ChangelogReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 395);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ChangelogReport";
            this.Text = "Changelog";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private LabelButton labelButton1;
        private System.Windows.Forms.Label txtCurrentVersion;
        private System.Windows.Forms.RichTextBox rtbChangeLog;
        private System.Windows.Forms.Button btnClose;
    }
}