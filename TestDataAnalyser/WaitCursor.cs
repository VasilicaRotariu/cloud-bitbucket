﻿using System;
using System.Windows.Forms;

namespace SudburyDataAnalyser
{
    /// <summary>
    /// WaitCursor is used to display a wait mouse cursor when the application
    /// is busy processing.
    /// Create a WaitCursor to change the appearance of the mouse cursor. Dispose
    /// of the WaitCursor to return the mouse cursor to its original state.
    /// Create inside a using block and the mouse cursor will return to its
    /// its previous state when exiting the block.
    /// </summary>
    public class WaitCursor : IDisposable
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="WaitCursor"/> class.
        /// Causes the mouse cursor to be displayed as a wait cursor.
        /// </summary>
        public WaitCursor()
            : this(Cursors.WaitCursor)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="WaitCursor"/> class.
        /// Causes the mouse cursor to be displayed as the cursor specified.
        /// </summary>
        /// <param name="cursor">Cursor type to be displayed</param>
        public WaitCursor(Cursor cursor)
        {
            oldCursor = Cursor.Current;
            Cursor.Current = cursor;
        }

        /// <summary>
        /// Returns the mouse cursor to its original state.
        /// </summary>
        public void Dispose()
        {
            Cursor.Current = oldCursor;
        }

        /// <summary>
        /// The cursor to be displayed on disposal.
        /// </summary>
        private readonly Cursor oldCursor;
    }
}
