﻿using System.Text;
using System.Diagnostics.CodeAnalysis;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.Virgin
{
    /// <summary>
    /// Default query for DAF virgins.
    /// See Virgin.<see cref="Row"/> for the rows returned from Execute.
    /// </summary>
    public class Query : QueryBase<Row>
    { 
        /// <summary>
        /// Gets or sets a value indicating whether to include Virgin NCV components
        /// </summary>
        private bool IncludeVirginNCV { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether to include Virgin Nozzle components
        /// </summary>
        private bool IncludeVirginNozzle { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to include Virgin Piston Guide Components
        /// </summary>
        private bool IncludeVirginPistonGuide { get; set; }

        /// <summary>
        /// Gets or sets the ISNs to filter
        /// </summary>
        private List<string> ISNs { get; set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="isns">List of Injector Serial Numbers to check</param>
        /// <param name="virginNCV">Indicates check of virgin NCVs</param>
        /// <param name="virginNozzle">Indicates check of virgin Nozzles</param>
        /// <param name="virginPG">Indicates check of virgin Piston Guides</param>
        public Query(Database.DatabaseBase database, List<string> isns, bool virginNCV, bool virginNozzle, bool virginPG)
            : base(database)
        {
            this.ISNs = isns;
            this.IncludeVirginNCV = virginNCV;
            this.IncludeVirginNozzle = virginNozzle;
            this.IncludeVirginPistonGuide = virginPG;
        }

        /// <summary>
        /// Returns the query used to get the rows.
        /// </summary>
        /// <returns>The query used to get the rows.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        protected override string GetQuery()
        {
            var query = new StringBuilder();
           
            query.AppendLines(
                "SELECT RIGHT(ISN, 9) as ISN, NCVCount, NozzleCount, PistonGuideCount",
                "FROM (",
                "   SELECT ISN, MAX(NCVCount) as NCVCount,",
                "	    (SELECT MAX(i) FROM (VALUES (MAX(agg.NozzleCount)), (MAX(nnc.NozzleCount))) as Prices(i)) as NozzleCount,",
                "	    MAX(nnc.PistonGuideCount) as PistonGuideCount",
                "   FROM (  ",
                "	    SELECT lowVol.ISN as ISN, ",
                "		    lowVol.NCVSerialCount as NCVCount, ",
                "		    lowVol.NozzleSerialCount as NozzleCount, ",
                "		    lowVol.NozzleSerial as NozzleSerial",
                "	    FROM LowVolumeMX13Line1 as lowVol",
                "	    UNION",
                "	    SELECT mas.Serial as ISN,",
                "		    mas.NCVCount as NCVCount, ",
                "		    mas.NozzleCount as NozzleCount,",
                "		    mas.Nozzle_SerialNumber as NozzleSerial",
                "	    FROM MASMEC_SelAsmb_OP20B_Results as mas",
                "   ) as agg",
                "   FULL JOIN NNCAssembly as nnc ON (nnc.NozzleSerial = agg.NozzleSerial)",
                "   WHERE SUBSTRING(ISN, 2, LEN(ISN)-1)IN (", //SUBSTRING is to remove the 3 that comes from MASMEC etc.
                "	    SELECT *",
                "	    FROM ({0}) as X(i) --IS THE BROKEN ONE".Args(GenerateISNStringFromList()),
                "	    )",
                "	GROUP BY ISN ",
                ") as result",
                "WHERE ISN = ISN --DUMMY CHECK"); //dummy check to simply the next three Filters

            if (IncludeVirginNCV)
                query.AppendLine("AND NCVCount = 1");

            if (IncludeVirginPistonGuide)
                query.AppendLine("AND PistonGuideCount = 1");

            if (IncludeVirginNozzle) 
                query.AppendLine("AND NozzleCount = 1");

            query.AppendLine("ORDER BY ISN");
            string q = query.ToString();
            return query.ToString();
        }

        /// <summary>
        /// Generates a list of values for use in SQL using a list of ISNs
        /// </summary>
        /// <returns>SQL String</returns>
        protected string GenerateISNStringFromList()
        {
            if (ISNs.Count > 0)
            {
                return "values ('{0}')".Args(string.Join("'),('", ISNs));
            }
            return string.Empty;
        }
    }
}