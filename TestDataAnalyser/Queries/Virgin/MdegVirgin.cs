﻿using System.Text;
using System.Diagnostics.CodeAnalysis;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.MdegVirgin
{
    /// <summary>
    /// Default query for MDEG virgin components.
    /// See <see cref="MdegVirgin.Row"/> for the rows returned from Execute.
    /// </summary>
    public class Query : QueryBase<Row>
    {
        /// <summary>
        /// Gets or sets a value indicating whether to include Virgin NCV components
        /// </summary>
        private bool IncludeVirginNVC { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to include Virgin Nozzle components
        /// </summary>
        private bool IncludeVirginNozzle { get; set; }

        /// <summary>
        /// Gets or sets a List of ISNs to check for Virginity
        /// </summary>
        private List<string> ISNs { get; set; }
                
        /// <summary>
        /// Initialises a new instance of the <see cref="Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="isns">List of Injector Serial Numbers to check</param>
        /// <param name="virginNCVCheck">Indicates check of virgin NCVs</param>
        /// <param name="virginNozzleCheck">Indicates check of virgin Nozzles</param>
        public Query(Database.DatabaseBase database, List<string> isns, bool virginNCVCheck, bool virginNozzleCheck)
            : base(database)
        {
            this.IncludeVirginNVC = virginNCVCheck;
            this.IncludeVirginNozzle = virginNozzleCheck;
            this.ISNs = isns;
        }

        /// <summary>
        /// Returns the query used to get the rows.
        /// </summary>
        /// <returns>The query used to get the rows.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        protected override string GetQuery()
        {
            var query = new StringBuilder();

            query.AppendLines(
                "SELECT Injector_ID as ISN ",
                "FROM (",
                "   SELECT rspl.Injector_ID AS Injector_ID, ",
                "       MAX(rspl.NCV_DataMatrixCount) as NCVCount,",
                "       MAX(NLS.Nozzle_DataMatrixCount) as NozzleCount",
                "   FROM RSPL AS rspl",
                "   INNER JOIN NLS AS nls ON (nls.Injector_ID = rspl.Injector_ID)",
                "   GROUP BY rspl.Injector_ID",
                " ",
                "   UNION",
                "",
                "   SELECT res.InjectorSerialNumber as Injector_ID, ",
                "       MAX(NCVSerialNumberCount) as NCVCount,",
                "       MAX(NozzleSerialNumberCount) as NozzleCount",
                "   FROM MDEGLowVolumeLineResults as res",
                "   GROUP BY res.InjectorSerialNumber", 
                ") AS t",
                "WHERE Injector_ID IN (",
                "   SELECT v FROM ({0})) as X(v))".Args(GenerateISNStringFromList()));

            if (IncludeVirginNVC)
                query.AppendLine("AND NCVCount = 1");
            if (IncludeVirginNozzle)
                query.AppendLine("AND NozzleCount = 1");

            string mdegVQuery = query.ToString();
            return mdegVQuery;
        }

        /// <summary>
        /// Generates a list of values for use in SQL using a list of ISNs
        /// </summary>
        /// <returns>SQL string</returns>
        protected string GenerateISNStringFromList()
        {
            if (ISNs.Count > 0)
            {
                ISNs = ISNs.OrderBy(x => x).ToList();
                return "values ('{0}'".Args(string.Join("'),('", ISNs));
            }
            return string.Empty;
        }
    }
}
