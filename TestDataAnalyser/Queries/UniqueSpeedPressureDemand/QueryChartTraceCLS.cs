﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SudburyDataAnalyser.ChartTraceCLS
{
    /// <summary>
    /// Unique Speed-Pressure-Demand query for CLS Chart traces
    /// </summary>
    public class UniqueSpeedPressureDemand : UniqueSpeedPressureDemandBase.Query
    { 
        /// <summary>
        /// Initialises a new instance of the <see cref="UniqueSpeedPressureDemand" /> class
        /// </summary>
        public UniqueSpeedPressureDemand(Database.DatabaseBase database, UTIDPlantNumberKeys keys)
            : base(database, keys, HeaderStabiliseCLTable, HeaderStabiliseCLTableURIDColumn)
        { 
        }
    }
}
