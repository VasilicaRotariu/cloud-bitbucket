﻿using System.Diagnostics.CodeAnalysis;
using System.Text;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.UniqueSpeedPressureDemandBase
{
    /// <summary>
    /// Base class for a unique speed pressure demand classes.
    /// </summary>
    public class Query : QueryBase<UniqueSpeedPressureDemand.Row>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="UniqueSpeedPressureDemandBase.Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="keys">List of keys used to filter the returned rows.</param>
        /// <param name="table">The name of the test point header table.</param>
        /// <param name="column">The name of the test point header table URID column.</param>
        public Query(Database.DatabaseBase database, UTIDPlantNumberKeys keys, Queries.Parameters.HeaderTable table, Queries.Parameters.HeaderTableColumn column)
            : base(database)
        {
            Keys = keys;
            TestPointHeaderTable = table;
            TestPointHeaderTableUridColumn = column;
        }

        /// <summary>
        /// Gets or sets the filter keys.
        /// </summary>
        private UTIDPlantNumberKeys Keys { get; set; }

        /// <summary>
        /// Gets or sets the name of the header table.
        /// </summary>
        private Queries.Parameters.HeaderTable TestPointHeaderTable { get; set; }

        /// <summary>
        /// Gets or sets the header table URID column name.
        /// </summary>
        private Queries.Parameters.HeaderTableColumn TestPointHeaderTableUridColumn { get; set; }

        /// <summary>
        /// Returns the query to get all the unique speed pressure demands.
        /// </summary>
        /// <returns>The query to execute.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        protected override string GetQuery()
        {
            var query = new StringBuilder();
            query.AppendLines(
                GetDeclareGlobalData(Keys, !Database.FtStructure),
                "SELECT DISTINCT",
                "('p' + CAST(hn.PressureReq as VARCHAR(4)) + '_fd' + CAST(hn.", FuelDemandColumn, " as VARCHAR(3))) AS SpeedPressureDemand, ",
                "ct.", TraceTableBinaryTraceDataColumn, ", he.LineNumber",
                "FROM [", HeaderTable, "] AS he",
                GetJoinGlobalData("he"),
                "INNER JOIN [", DataHeaderTable, "] AS dh ON (dh.UTID = he.UTID AND dh.PlantNumber = he.PlantNumber)",
                "INNER JOIN [", TestPointHeaderTable, "] AS hn ON (hn.URID = dh.URID AND hn.PlantNumber = dh.PlantNumber)",
                "INNER JOIN [", TracesTable, "] AS ct ON (ct.URID = hn.URID AND ct.PlantNumber = hn.PlantNumber)",
                "ORDER BY SpeedPressureDemand DESC");

            QueryModifier.ModifyOLQueryForNonFT(query);

            return query.ToString();
        }
    }
}