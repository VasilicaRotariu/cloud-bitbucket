﻿namespace SudburyDataAnalyser.ChartTraceCLT
{
    /// <summary>
    /// Unique speed pressure demand query for chart trace CLT.
    /// </summary>
    public class QueryUniqueSpeedPressureDemand : UniqueSpeedPressureDemandBase.Query
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ChartTraceCLT.QueryUniqueSpeedPressureDemand"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="keys">The keys to filter the rows by.</param>
        public QueryUniqueSpeedPressureDemand(Database.DatabaseBase database, UTIDPlantNumberKeys keys)
            : base(database, keys, HeaderCLTable, HeaderCLTableURIDColumn)
        {
        }
    }
}
