﻿using System.Diagnostics.CodeAnalysis;
using System.Text;
using Redmine.Net.Api.Types;

using SudburyDataAnalyser.Queries.Parameters;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Class for sub queries available to all queries.
    /// </summary>
    public class QueryBaseSubQueries : Queries.Parameters.QueryParameters
    {
        /// <summary>
        /// Returns a SQL query used to filter rows based on the keys.
        /// </summary>
        /// <param name="table">The table used to compare the keys against.</param>
        /// <param name="keys">The list of keys to filter the rows.</param>
        /// <returns>The SQL query.</returns>
        public static string GetFilterOnKeys(string table, UTIDPlantNumberKeys keys)
        {
            var query = new StringBuilder();
            query.AppendLines(
                "WHERE EXISTS (SELECT * FROM (values {0}) as X(a,b)".Args(keys.ToString()),
                "WHERE {0}.UTID = a AND {0}.PlantNumber = b)".Args(table));
            string q = query.ToString();
            return q;
        }

        /// <summary>
        /// Creating temp table
        /// </summary>
        /// <param name="arg">The list of keys to filter the rows.</param>
        /// <returns>The SQL query.</returns>
        public static string GetFilterOnKeys_V2(UTIDPlantNumberKeys arg)
        {
            var query = new StringBuilder();
            query.AppendLines(
               "CREATE TABLE #temp (UTID INT NOT NULL,PlantNumber INT NOT NULL);", 
                "INSERT INTO #temp VALUES {0};".Args(arg.ToString()));        
            string q = query.ToString();
            return q;
        }

        /// <summary>
        /// Using temp table in join
        /// </summary>
        /// <param name="arg">The list of keys to filter the rows.</param>
        /// <returns>The SQL query.</returns>
        public static string GetFilterOnKeys_V3(string arg)
        {
            var query = new StringBuilder();
            query.AppendLines(
               
            "INNER JOIN #temp T ON {0}.UTID = t.UTID AND {0}.PlantNumber = t.PlantNumber".Args(arg));

            string q = query.ToString();
            return q;
        }

      
        /// <summary>
        /// Returns a SQL query to generate the global data table commonly used in queries.
        /// </summary>
        /// <param name="keys">The list of keys used to filter the rows.</param>
        /// <param name="filterOnPlantNumber">Add a filter to filter the rows based on the plant number.</param>
        /// <returns>The global data table SQL query.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        protected string GetDeclareGlobalData(UTIDPlantNumberKeys keys, bool filterOnPlantNumber)
        {
            var query = new StringBuilder();
            query.AppendLines(
                "DECLARE @globaldata Table (UTID int, PN smallint, Glob int)",
                 GetFilterOnKeys_V2(keys),
                "INSERT @globaldata",
                "SELECT r.UTID, r.PlantNumber, r.Grade",
                "FROM [", ResultsTable, "] AS r",
                "INNER JOIN [", NameIDsTable, "] AS na ON (na.NameID = r.NameID AND na.Name = 'Global'{0})".Args(filterOnPlantNumber ? " AND na.PlantNumber = r.PlantNumber" : ""),
                GetFilterOnKeys_V3("r")//," DROP TABLE #temp;"
                );

            return query.ToString();
        }

        /// <summary>
        /// Returns a SQL query used to join the global data table to the specified table.
        /// </summary>
        /// <param name="headerTable">The name of the table to join too.</param>
        /// <returns>The join SQL query.</returns>
        protected string GetJoinGlobalData(string headerTable)
        {
            return "INNER JOIN @globaldata AS g ON g.UTID = {0}.UTID AND g.PN = {0}.PlantNumber".Args(headerTable);
        }

        /// <summary>
        /// Returns a SQL query used to get the injector serial numbers of all the non virgin injectors.
        /// </summary>
        /// <returns>The SQL query.</returns>
        protected string GetDeclareNoneVirgins(bool ncvVirgin, bool nozzlevirgin, bool pistonGuideVirgin)
        {
            //jz5w78 - Alex
            // Modify query so that it is possible to filter by virgin type
            // NCV, Nozzle or Pistion Guide
            var query = new StringBuilder();
            query.AppendLines(
                "DECLARE @NonVirgins Table (InjectorID varchar(10), primary key (InjectorID))",
                "INSERT @NonVirgins",
                "SELECT DISTINCT mas.Serial as InjectorID",
                "FROM MASMEC_SelAsmb_OP20B_Results as mas",
                "INNER JOIN NNCAssembly AS nnc ON (nnc.NozzleSerial = mas.Nozzle_SerialNumber)",
                "WHERE ",
                nozzlevirgin ? "mas.NozzleCount != 1 OR nnc.NozzleCount != 1" : "",
                nozzlevirgin && ncvVirgin ? "OR" : "",
                ncvVirgin ? "mas.NCVCount != 1" : "",
                (pistonGuideVirgin && ncvVirgin) || (pistonGuideVirgin && nozzlevirgin) ? "OR" : "",
                pistonGuideVirgin ? "nnc.PistonGuideCount != 1 " : "");

            string returnQuery = query.ToString();
            return returnQuery;
        }
    }
}
