﻿namespace SudburyDataAnalyser.CLT
{
    /// <summary>
    /// Default query for CLT.
    /// See CLT.<see cref="Row"/> for the rows returned from Execute.
    /// </summary>
    public class Query : TestPointsQuery.Query<Row>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="keys">The list of keys to filter the rows by.</param>
        public Query(Database.DatabaseBase database, UTIDPlantNumberKeys keys)
            : base(database, keys, DataCLTable, DataCLTableURIDColumn, HeaderCLTable, HeaderCLTableURIDColumn)
        {
        }
    }
}
