﻿using System.Diagnostics.CodeAnalysis;
using System.Text;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.TestPointsQuery
{
    /// <summary>
    /// Base class for all test point queries.
    /// This defines the default query used to get rows of test point data.
    /// </summary>
    /// <typeparam name="T">Test point type</typeparam>
    public class Query<T> : QueryBase<T> where T : RowBase, new()
    {
        /// <summary>
        /// Gets or sets the filter keys.
        /// </summary>
        private UTIDPlantNumberKeys Keys { get; set; }

        /// <summary>
        /// Gets or sets the test point data table used in the query.
        /// </summary>
        private Queries.Parameters.QueryTable TestPointDataTable { get; set; }

        /// <summary>
        /// Gets or sets the test point data table URID column name used in the query.
        /// </summary>
        private Queries.Parameters.QueryColumn TestPointDataTableUridColumn { get; set; }

        /// <summary>
        /// Gets or sets the test point header table.
        /// </summary>
        private Queries.Parameters.QueryTable TestPointHeaderTable { get; set; }

        /// <summary>
        /// Gets or sets the test point header table URID column name used in the query.
        /// </summary>
        private Queries.Parameters.QueryColumn TestPointHeaderTableUridColumn { get; set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="Query{T}"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="keys">List of keys used to filter the returned rows.</param>
        /// <param name="testPointDataTable">The test point data table used in the query.</param>
        /// <param name="testPointDataTableUridColumn">Test point data table URID column name used in the query.</param>
        /// <param name="testPointHeaderTable">The test point header table.</param>
        /// <param name="testPointHeaderTableUridColumn">Test point header table URID column name used in the query.</param>
        public Query(
            Database.DatabaseBase database, 
            UTIDPlantNumberKeys keys,
            Queries.Parameters.QueryTable testPointDataTable,
            Queries.Parameters.QueryColumn testPointDataTableUridColumn,
            Queries.Parameters.QueryTable testPointHeaderTable,
            Queries.Parameters.QueryColumn testPointHeaderTableUridColumn)
            : base(database)
        {
            Keys = keys;

            TestPointDataTable = testPointDataTable;
            TestPointDataTableUridColumn = testPointDataTableUridColumn;
            TestPointHeaderTable = testPointHeaderTable;
            TestPointHeaderTableUridColumn = testPointHeaderTableUridColumn;
        }

        /// <summary>
        /// Returns the query used to get the rows.
        /// </summary>
        /// <returns>The query used to get the rows.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        protected override string GetQuery()
        {
            var query = new StringBuilder();
            query.AppendLines(
                GetDeclareGlobalData(Keys, !Database.FtStructure),
               // GetFilterOnKeys_V2(Keys),
                "SELECT he.*, g.Glob, dahe.*, tphe.*, tpda.*, name.*",
                "FROM [", HeaderTable, "] AS he",
                GetJoinGlobalData("he"),
                "INNER JOIN [", DataHeaderTable, "] AS dahe ON (dahe.UTID = he.UTID AND dahe.PlantNumber = he.PlantNumber)",
                "INNER JOIN [", TestPointHeaderTable, "] AS tphe ON (tphe.URID = dahe.URID AND tphe.PlantNumber = dahe.PlantNumber)",
                "INNER JOIN [", TestPointDataTable, "] AS tpda ON (tpda.", TestPointDataTableUridColumn, " = tphe.", TestPointHeaderTableUridColumn, " AND tpda.PlantNumber = tphe.PlantNumber)",
                "INNER JOIN [", NameIDsTable, "] AS name ON (", EqualNames.Args("name", "tpda"), ")",
                GetFilterOnKeys_V3("he"),
                "ORDER BY he.UTID asc, tpda.", TestPointDataTableUridColumn, " ASC; DROP TABLE #temp;")
            ;
            QueryModifier.ModifyOLQueryForNonFT(query);

            string q = query.ToString();
            return query.ToString();
        }
    }
}
