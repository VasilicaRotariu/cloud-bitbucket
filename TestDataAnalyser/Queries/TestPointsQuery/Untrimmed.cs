﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using SudburyDataAnalyser.Database;

namespace SudburyDataAnalyser.Untrimmed
{
    /// <summary>
    /// Query for the Untrimmed Data Points
    /// </summary>
    public sealed class Query : QueryBase<Row>
    {
        /// <summary>
        /// Gets the UTID Keys with their plant numbers
        /// </summary>
        public UTIDPlantNumberKeys PlantNumberKeys { get; private set; }
         
        /// <summary>
        /// Initialises a new instance of the <see cref="Query" /> class
        /// </summary>
        public Query(DatabaseBase database, UTIDPlantNumberKeys keys)
            : base(database)
        {
            PlantNumberKeys = keys;
        }

        /// <summary>
        /// Returns the query used to get the rows.
        /// </summary>
        /// <returns>The query used to get the rows.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1027:TabsMustNotBeUsed",
                         Justification = "Disabled rule to improve readability of SQL.")] 
        protected override string GetQuery()
        {
            StringBuilder query = new StringBuilder();

            query.AppendLines(
               // GetFilterOnKeys_V2(PlantNumberKeys),
                "SELECT ",
                "	OffsetResult.UTID,  OffsetResult.PlantNumber,  name.Name,  OffsetResult.Pressure as [PressureReq],  OffsetResult.Logic,",
                "	NominalResult.Value as NomValue, OffsetResult.Offset as Offset, NominalResult.Value + OffsetResult.Offset as CP",
                "FROM (",
                "SELECT",
                "	Header.UTID, Header.PlantNumber, DataOL.NameID, HeaderOL.PressureReq AS Pressure,  HeaderOL.Logic,  DataOL.Offset",
                "FROM dbo.Header ",
                "INNER JOIN dbo.DataHeader ON dbo.Header.UTID = dbo.DataHeader.UTID AND dbo.Header.PlantNumber = dbo.DataHeader.PlantNumber ",
                "INNER JOIN dbo.HeaderOL ON dbo.DataHeader.PlantNumber = dbo.HeaderOL.PlantNumber AND dbo.DataHeader.URID = dbo.HeaderOL.URID ",
                "INNER JOIN dbo.DataOL ON dbo.HeaderOL.URIDOL = dbo.DataOL.URIDOL AND dbo.HeaderOL.PlantNumber = dbo.DataOL.PlantNumber ",
                GetFilterOnKeys_V3("header"),//,PlantNumberKeys),
                ") as OffsetResult",
                "JOIN (",
                "SELECT", 
                "	Header.UTID, Header.PlantNumber, dbo.NominalHeaderOL.Pressure,  dbo.NominalHeaderOL.Logic, dbo.NominalDataOL.Value, dbo.NominalDataOL.NameID",
                "FROM dbo.Header",
                "INNER JOIN dbo.NominalHeaderOL ON dbo.Header.NID = dbo.NominalHeaderOL.NID AND dbo.Header.PlantNumber = dbo.NominalHeaderOL.PlantNumber ",
                "INNER JOIN dbo.NominalDataOL ON dbo.NominalHeaderOL.PlantNumber = dbo.NominalDataOL.PlantNumber AND dbo.NominalHeaderOL.NRIDOL = dbo.NominalDataOL.NRIDOL",
                GetFilterOnKeys_V3("header"),
                ") as NominalResult ",
                "on OffsetResult.PlantNumber = NominalResult.PlantNumber",
                "   AND OffsetResult.UTID = NominalResult.UTID",
	            "   AND OffsetResult.Pressure = NominalResult.Pressure",
	            "   AND OffsetResult.Logic = NominalResult.Logic",
	            "   AND OffsetResult.NameID = NominalResult.NameID",
                "INNER JOIN NameID as name on Name.NameID = NominalResult.NameID AND name.PlantNumber = OffsetResult.PlantNumber",
                "Where NominalResult.NameID = 11",
                "ORDER BY UTID, PressureReq Desc, Logic Desc",
                "DROP TABLE #temp");

            string resultQ = query.ToString();
            return resultQ;
        }
    }
}
