﻿using System.Linq;
using System.Text;
using System.Diagnostics.CodeAnalysis;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.NCVNominal
{
    /// <summary>
    /// Default query for NCVNominal.
    /// See NCVNominal.<see cref="Row"/> for the rows returned from Execute.
    /// </summary>
    public class Query : QueryBase<Row>
    {
        /// <summary>Gets or sets the UTID's used in the SQL query.</summary>
        // ReSharper disable once InconsistentNaming
        private string UTIDs { get; set; }

        /// <summary>Gets or sets the plant number used in the SQL query.</summary>
        private short PlantNumber { get; set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="utids">Comma separated string of UTID's used to filter the returned rows.</param>
        /// <param name="plantNumber">The plant number to use with the UTID's to filter the returned rows.</param>
        public Query(Database.DatabaseBase database, string utids, short plantNumber)
            : base(database)
        {
            UTIDs = utids;
            PlantNumber = plantNumber;
        }

        /// <summary>
        /// Returns the query used to get the rows.
        /// </summary>
        /// <returns>The query used to get the rows.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        protected override string GetQuery()
        {
            var keys = new UTIDPlantNumberKeys { { from utid in UTIDs.Split(',') select utid.ToInt(), PlantNumber } };

            var query = new StringBuilder();
            query.AppendLines(
                "SELECT ncvhe.", HeaderOLTableDurationColumn, ", name.Name, ncvhe.PressureReq, ROUND(DBO.MEDIAN(ncvda.Median),1) AS Median",
                "FROM [", HeaderTable, "] AS he",
                "INNER JOIN [", DataHeaderTable, "] AS dahe ON (dahe.UTID = he.UTID AND dahe.PlantNumber = he.PlantNumber)",
                "INNER JOIN [", HeaderOLTable, "] AS ncvhe ON (ncvhe.URID = dahe.URID AND ncvhe.PlantNumber = dahe.PlantNumber)",
                "INNER JOIN [", DataOLTable, "] AS ncvda ON (ncvda.", HeaderOLTableURIDColumn, " = ncvhe.", HeaderOLTableURIDColumn, " AND ncvda.PlantNumber = ncvhe.PlantNumber)",
                "INNER JOIN [", NameIDsTable, "] AS name ON (", EqualNames.Args("name", "ncvda"), ")",
                "WHERE EXISTS (SELECT * FROM (values {0}) as X(a,b)".Args(keys.ToString()),
                "WHERE he.UTID = a AND he.PlantNumber = b)",
                "GROUP BY name.Name, ncvhe.PressureReq, ncvhe.", HeaderOLTableDurationColumn, "",
                "ORDER BY ncvhe.PressureReq asc, name.Name asc, ncvhe.", HeaderOLTableDurationColumn, " desc");
            QueryModifier.ModifyOLQueryForNonFT(query);

            return query.ToString();
        }
    }
}
