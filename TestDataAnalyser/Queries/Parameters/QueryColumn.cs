﻿namespace SudburyDataAnalyser.Queries.Parameters
{
    /// <summary>
    /// Query parameter specifying the name of a column.
    /// </summary>
    public class QueryColumn : QueryParameter
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="QueryColumn"/> class.
        /// </summary>
        /// <param name="name">The name of the query table.</param>
        public QueryColumn(string name)
            : base(name)
        {
        }

        /// <summary>
        /// Returns the exception message when an undefined query column is used.
        /// </summary>
        /// <returns>The exception message.</returns>
        protected override string NotDefined()
        {
            return "The column {0} is not defined".Args(Name);
        }
    }
}
