﻿namespace SudburyDataAnalyser.Queries.Parameters
{
    /// <summary>
    /// Query parameter specifying the name of a table.
    /// </summary>
    public class QueryTable : QueryParameter
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="QueryTable"/> class.
        /// </summary>
        /// <param name="name">The name of the query table.</param>
        public QueryTable(string name)
            : base(name)
        {
        }

        /// <summary>
        /// Returns the exception message when an undefined query table is used.
        /// </summary>
        /// <returns>The exception message.</returns>
        protected override string NotDefined()
        {
            return "The table {0} is not defined".Args(Name);
        }
    }
}
