﻿using System;

namespace SudburyDataAnalyser.Queries.Parameters
{
    /// <summary>
    /// Base class for all query parameters.
    /// All query parameters have a name (for debugging purposes) and a value.
    /// Using a query parameter without a value will cause an exception.
    /// </summary>
    public abstract class QueryParameter
    {
        /// <summary>
        /// The query parameter value.
        /// </summary>
        private string value;

        /// <summary>
        /// Initialises a new instance of the <see cref="QueryParameter"/> class.
        /// </summary>
        /// <param name="name">The name of the query parameter, used in the exception for identification.</param>
        protected QueryParameter(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Gets the query parameter name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Casts a query parameter to a string, returning the query parameter value.
        /// </summary>
        /// <param name="parameter">The query parameter to cast.</param>
        /// <returns>The query parameter value.</returns>
        public static implicit operator string(QueryParameter parameter)
        {
            return parameter.Value;
        }

        /// <summary>
        /// Gets or sets the query parameter value. If the value has not been set, a
        /// NotSupportedException is thrown.
        /// </summary>
        public string Value 
        { 
            protected get
            {
                if (value == null)
                {
                    throw new NotSupportedException(NotDefined());
                }

                return value;
            }

            set
            {
                this.value = value;
            }
        }

        /// <summary>
        /// Returns the exception message when an undefined query parameter is used.
        /// </summary>
        /// <returns>The exception message.</returns>
        protected abstract string NotDefined();
    }
}
