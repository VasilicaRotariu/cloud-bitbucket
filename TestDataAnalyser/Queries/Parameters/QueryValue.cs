﻿namespace SudburyDataAnalyser.Queries.Parameters
{
    /// <summary>
    /// Query parameter specifying the a query value.
    /// </summary>
    public class QueryValue : QueryParameter
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="QueryValue"/> class.
        /// </summary>
        /// <param name="name">The name of the query value.</param>
        public QueryValue(string name)
            : base(name)
        {
        }

        /// <summary>
        /// Returns the exception message when an undefined query value is used.
        /// </summary>
        /// <returns>The exception message.</returns>
        protected override string NotDefined()
        {
            return "The query value {0} is not defined".Args(Name);
        }
    }
}
