﻿namespace SudburyDataAnalyser.Queries.Parameters
{
    /// <summary>
    /// Query parameter specifying some SQL.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class QuerySQL : QueryParameter
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="QuerySQL"/> class.
        /// </summary>
        /// <param name="name">The name of the query SQL.</param>
        public QuerySQL(string name)
            : base(name)
        {
        }

        /// <summary>
        /// Returns the query SQL as a formatted string using the arguments supplied.
        /// </summary>
        /// <param name="values">Arguments used to format the SQL.</param>
        /// <returns>The query SQL string.</returns>
        public string Args(params object[] values)
        {
            if (Value.IsNotEmpty())
            {
                return Value.Args(values);
            }

            return string.Empty;
        }

        /// <summary>
        /// Returns the exception message when undefined query SQL is used.
        /// </summary>
        /// <returns>The exception message.</returns>
        protected override string NotDefined()
        {
            return "The SQL {0} is not defined".Args(Name);
        }
    }
}
