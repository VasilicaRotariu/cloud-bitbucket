﻿namespace SudburyDataAnalyser.Queries.Parameters
{
    /// <summary>
    /// Query parameter specifying the name of a header table column.
    /// </summary>
    public class HeaderTableColumn : QueryColumn
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="HeaderTableColumn"/> class.
        /// </summary>
        /// <param name="name">The name of the header table column.</param>
        public HeaderTableColumn(string name)
            : base(name)
        {
        }
    }
}
