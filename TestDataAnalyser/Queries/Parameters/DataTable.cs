﻿namespace SudburyDataAnalyser.Queries.Parameters
{
    /// <summary>
    /// Query parameter specifying the name of a data table.
    /// </summary>
    public class DataTable : QueryTable
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="DataTable"/> class.
        /// </summary>
        /// <param name="name">The name of the data table.</param>
        public DataTable(string name)
            : base(name)
        {
        }
    }
}
