﻿namespace SudburyDataAnalyser.Queries.Parameters
{
    /// <summary>
    /// Query parameter specifying the name of a header table.
    /// </summary>
    public class HeaderTable : QueryTable
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="HeaderTable"/> class.
        /// </summary>
        /// <param name="name">The name of the header table.</param>
        public HeaderTable(string name)
            : base(name)
        {
        }
    }
}
