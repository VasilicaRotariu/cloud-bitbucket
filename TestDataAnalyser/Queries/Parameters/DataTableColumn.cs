﻿namespace SudburyDataAnalyser.Queries.Parameters
{
    /// <summary>
    /// Query parameter specifying the name of a data table column.
    /// </summary>
    public class DataTableColumn : QueryColumn
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="DataTableColumn"/> class.
        /// </summary>
        /// <param name="name">The name of the data table column.</param>
        public DataTableColumn(string name)
            : base(name)
        {
        }
    }
}
