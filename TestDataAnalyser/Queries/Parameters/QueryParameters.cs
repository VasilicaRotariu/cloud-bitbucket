﻿namespace SudburyDataAnalyser.Queries.Parameters
{
    /// <summary>
    /// Class containing all the query parameters. The value of each parameter
    /// is set depending on the type of database being accessed.
    /// </summary>
    // ReSharper disable InconsistentNaming
    public class QueryParameters
    {
        /// <summary>Gets the name of the early end table.</summary>
        public static QueryTable EarlyEndTable
        {
            get { return EarlyEndTableValue; }
        }

        /// <summary>Gets the name of the leak down table.</summary>
        public static QueryTable LeakDownTable
        {
            get { return LeakDownTableValue; }
        }

        /// <summary>Gets the name of the name ID table.</summary>
        public static QueryTable NameIDsTable
        {
            get { return NameIDsTableValue; }
        }

        /// <summary>Gets the name of the nominal dataset table.</summary>
        public static QueryTable NominalDatasetTable
        {
            get { return NominalDatasetTableValue; }
        }

        /// <summary>Gets the name of the results table.</summary>
        public static QueryTable ResultsTable
        {
            get { return ResultsTableValue; }
        }

        /// <summary>Gets the name of the rig type table.</summary>
        public static QueryTable RigTypeTable
        {
            get { return RigTypeTableValue; }
        }

        /// <summary>Gets the name of the traces table.</summary>
        public static QueryTable TracesTable
        {
            get { return TracesTableValue; }
        }

        /// <summary>Gets the name of the trend chart table.</summary>
        public static QueryTable TrendChartTable
        {
            get { return TrendChartTableValue; }
        }

        /// <summary>Gets the name of the trim code table.</summary>
        public static QueryTable TrimCodeTable
        {
            get { return TrimCodeTableValue; }
        }

        /// <summary>Gets the name of the trims table.</summary>
        public static QueryTable TrimsTable
        {
            get { return TrimsTableValue; }
        }

        /// <summary>Gets the name of the trims table.</summary>
        public static QueryTable TrimmedIdentityTable
        {
            get { return TrimmedIdentityTableValue; }
        }

        /// <summary>Gets the name of the trims table.</summary>
        public static QueryTable TrimmedPassOffTable
        {
            get { return TrimmedPassOffTableValue; }
        }

        /// <summary>Gets the name of the data header table.</summary>
        public static DataTable DataHeaderTable
        {
            get { return DataHeaderTableValue; }
        }

        /// <summary>Gets the name of the closed loop data table.</summary>
        // ReSharper disable once InconsistentNaming
        public static DataTable DataCLTable
        {
            get { return DataCLTableValue; }
        }

        /// <summary>Gets the name of the FOD data table.</summary>
        // ReSharper disable once InconsistentNaming
        public static DataTable DataFODTable
        {
            get { return DataFODTableValue; }
        }

        /// <summary>Gets the name of the leak data table.</summary>
        public static DataTable DataLeakTable
        {
            get { return DataLeakTableValue; }
        }

        /// <summary>Gets the name of the open loop data table.</summary>
        // ReSharper disable once InconsistentNaming
        public static DataTable DataOLTable
        {
            get { return DataOLTableValue; }
        }

        /// <summary>Gets the name of the OMV data table.</summary>
        // ReSharper disable once InconsistentNaming
        public static DataTable DataOMVTable
        {
            get { return DataOMVTableValue; }
        }

        /// <summary>Gets the name of the stabilise closed loop data table.</summary>
        // ReSharper disable once InconsistentNaming
        public static DataTable DataStabiliseCLTable
        {
            get { return DataStabiliseCLTableValue; }
        }

        /// <summary>Gets the name of the stabilise open loop data table.</summary>
        // ReSharper disable once InconsistentNaming
        public static DataTable DataStabiliseOLTable
        {
            get { return DataStabiliseOLTableValue; }
        }

        /// <summary>Gets the name of the trimmed data table.</summary>
        public static DataTable DataTrimmedTable
        {
            get { return DataTrimmedTableValue; }
        }

        /// <summary>Gets the name of the header table.</summary>
        public static HeaderTable HeaderTable
        {
            get { return HeaderTableValue; }
        }

        /// <summary>Gets the name of the closed loop header table.</summary>
        // ReSharper disable once InconsistentNaming
        public static HeaderTable HeaderCLTable
        {
            get { return HeaderCLTableValue; }
        }

        /// <summary>Gets the name of the FOD header table.</summary>
        // ReSharper disable once InconsistentNaming
        public static HeaderTable HeaderFODTable
        {
            get { return HeaderFODTableValue; }
        }

        /// <summary>Gets the name of the leak header table.</summary>
        public static HeaderTable HeaderLeakTable
        {
            get { return HeaderLeakTableValue; }
        }

        /// <summary>Gets the name of the open loop header table.</summary>
        // ReSharper disable once InconsistentNaming
        public static HeaderTable HeaderOLTable
        {
            get { return HeaderOLTableValue; }
        }

        /// <summary>Gets the name of the OMV header table.</summary>
        // ReSharper disable once InconsistentNaming
        public static HeaderTable HeaderOMVTable
        {
            get { return HeaderOMVTableValue; }
        }

        /// <summary>Gets the name of the stabilise closed loop header table.</summary>
        // ReSharper disable once InconsistentNaming
        public static HeaderTable HeaderStabiliseCLTable
        {
            get { return HeaderStabiliseCLTableValue; }
        }

        /// <summary>Gets the name of the stabilise open loop header table.</summary>
        // ReSharper disable once InconsistentNaming
        public static HeaderTable HeaderStabiliseOLTable
        {
            get { return HeaderStabiliseOLTableValue; }
        }

        /// <summary>Gets the name of the trimmed header table.</summary>
        public static HeaderTable HeaderTrimmedTable
        {
            get { return HeaderTrimmedTableValue; }
        }

        /// <summary>Gets the name of the closed loop data table URID column.</summary>
        // ReSharper disable once InconsistentNaming
        public static DataTableColumn DataCLTableURIDColumn
        {
            get { return DataCLTableURIDColumnValue; }
        }

        /// <summary>Gets the name of the FOD data table URID column.</summary>
        // ReSharper disable once InconsistentNaming
        public static DataTableColumn DataFODTableURIDColumn
        {
            get { return DataFODTableURIDColumnValue; }
        }

        /// <summary>Gets the name of the leak data table URID column.</summary>
        // ReSharper disable once InconsistentNaming
        public static DataTableColumn DataLeakTableURIDColumn
        {
            get { return DataLeakTableURIDColumnValue; }
        }

        /// <summary>Gets the name of the open loop data table URID column.</summary>
        // ReSharper disable once InconsistentNaming
        public static DataTableColumn DataOLTableURIDColumn
        {
            get { return DataOLTableURIDColumnValue; }
        }

        /// <summary>Gets the name of the OMV data table URID column.</summary>
        // ReSharper disable once InconsistentNaming
        public static DataTableColumn DataOMVTableURIDColumn
        {
            get { return DataOMVTableURIDColumnValue; }
        }

        /// <summary>Gets the name of the stabilise closed loop data table URID column.</summary>
        // ReSharper disable once InconsistentNaming
        public static DataTableColumn DataStabiliseCLTableURIDColumn
        {
            get { return DataStabiliseCLTableURIDColumnValue; }
        }

        /// <summary>Gets the name of the stabilise open loop data table URID column.</summary>
        // ReSharper disable once InconsistentNaming
        public static DataTableColumn DataStabiliseOLTableURIDColumn
        {
            get { return DataStabiliseOLTableURIDColumnValue; }
        }

        /// <summary>Gets the name of the trimmed data table URID column.</summary>
        // ReSharper disable once InconsistentNaming
        public static DataTableColumn DataTrimmedTableURIDColumn
        {
            get { return DataTrimmedTableURIDColumnValue; }
        }

        /// <summary>Gets the name of the header table identifier column.</summary>
        // ReSharper disable once InconsistentNaming
        public static HeaderTableColumn HeaderTableIDENTColumn
        {
            get { return HeaderTableIDENTColumnValue; }
        }

        /// <summary>Gets the name of the header table test type column.</summary>
        public static HeaderTableColumn HeaderTableTestTypeColumn
        {
            get { return HeaderTableTestTypeColumnValue; }
        }

        /// <summary>Gets the name of the header table Lua SVN column.</summary>
        // ReSharper disable once InconsistentNaming
        public static HeaderTableColumn HeaderTableLuaSVNColumn
        {
            get { return HeaderTableLuaSVNColumnValue; }
        }

        /// <summary>Gets the name of the nominal dataset table identifier column.</summary>
        // ReSharper disable once InconsistentNaming
        public static HeaderTableColumn NominalDatasetTableIDENTColumn
        {
            get { return NominalDatasetTableIDENTColumnValue; }
        }

        /// <summary>Gets the name of the closed loop header table URID column.</summary>
        // ReSharper disable once InconsistentNaming
        public static HeaderTableColumn HeaderCLTableURIDColumn
        {
            get { return HeaderCLTableURIDColumnValue; }
        }

        /// <summary>Gets the name of the FOD header table URID column.</summary>
        // ReSharper disable once InconsistentNaming
        public static HeaderTableColumn HeaderFODTableURIDColumn
        {
            get { return HeaderFODTableURIDColumnValue; }
        }

        /// <summary>Gets the name of the leak header table URID column.</summary>
        // ReSharper disable once InconsistentNaming
        public static HeaderTableColumn HeaderLeakTableURIDColumn
        {
            get { return HeaderLeakTableURIDColumnValue; }
        }

        /// <summary>Gets the name of the open loop header table URID column.</summary>
        // ReSharper disable once InconsistentNaming
        public static HeaderTableColumn HeaderOLTableURIDColumn
        {
            get { return HeaderOLTableURIDColumnValue; }
        }

        /// <summary>Gets the name of the OMV header table URID column.</summary>
        // ReSharper disable once InconsistentNaming
        public static HeaderTableColumn HeaderOMVTableURIDColumn
        {
            get { return HeaderOMVTableURIDColumnValue; }
        }

        /// <summary>Gets the name of the stabilise closed loop header table URID column.</summary>
        // ReSharper disable once InconsistentNaming
        public static HeaderTableColumn HeaderStabiliseCLTableURIDColumn
        {
            get { return HeaderStabiliseCLTableURIDColumnValue; }
        }

        /// <summary>Gets the name of the stabilise open loop header table URID column.</summary>
        // ReSharper disable once InconsistentNaming
        public static HeaderTableColumn HeaderStabiliseOLTableURIDColumn
        {
            get { return HeaderStabiliseOLTableURIDColumnValue; }
        }

        /// <summary>Gets the name of the trimmed header table URID column.</summary>
        // ReSharper disable once InconsistentNaming
        public static HeaderTableColumn HeaderTrimmedTableURIDColumn
        {
            get { return HeaderTrimmedTableURIDColumnValue; }
        }

        /// <summary>Gets the name of the closed loop header table duration column.</summary>
        // ReSharper disable once InconsistentNaming
        public static QueryColumn HeaderCLTableDurationColumn
        {
            get { return HeaderCLTableDurationColumnValue; }
        }

        /// <summary>Gets the name of the trimmed header table median column.</summary>
        // ReSharper disable once InconsistentNaming
        public static QueryColumn HeaderCLTableMedianColumn
        {
            get { return HeaderCLTableMedianColumnValue; }
        }

        /// <summary>Gets the name of the open loop header table duration column.</summary>
        // ReSharper disable once InconsistentNaming
        public static QueryColumn HeaderOLTableDurationColumn
        {
            get { return HeaderOLTableDurationColumnValue; }
        }

        /// <summary>Gets the name of the trimmed header table median column.</summary>
        // ReSharper disable once InconsistentNaming
        public static QueryColumn HeaderOLTableMedianColumn
        {
            get { return HeaderOLTableMedianColumnValue; }
        }

        /// <summary>Gets the name of the trimmed header table duration column.</summary>
        public static QueryColumn HeaderTrimmedTableDurationColumn
        {
            get { return HeaderTrimmedTableDurationColumnValue; }
        }

        /// <summary>Gets the name of the trimmed header table median column.</summary>
        public static QueryColumn HeaderTrimmedTableMedianColumn
        {
            get { return HeaderTrimmedTableMedianColumnValue; }
        }

        /// <summary>Gets the name of the fuel demand column.</summary>
        public static QueryColumn FuelDemandColumn
        {
            get { return FuelDemandColumnValue; }
        }

        /// <summary>Gets the name of the nominal fuel column.</summary>
        public static QueryColumn NominalFuelColumn
        {
            get { return NominalFuelColumnValue; }
        }

        /// <summary>Gets the name of the trace table identifier column.</summary>
        public static QueryColumn TraceTableIdentColumn
        {
            get { return TraceTableIdentColumnValue; }
        }

        /// <summary>Gets the name of the trace table binary trace data column.</summary>
        public static QueryColumn TraceTableBinaryTraceDataColumn
        {
            get { return TraceTableBinaryTraceDataColumnValue; }
        }

        /// <summary>Gets the name of the trims table value column.</summary>
        public static QueryColumn TrimsTableValueColumn
        {
            get { return TrimsTableValueColumnValue; }
        }

        /// <summary>Gets the name of the grades table value column.</summary>
        public static QueryColumn GradesTableValueColumn
        {
            get { return GradesTableValueColumnValue; }
        }

        /// <summary>Gets the SQL comparing name fields between tables.</summary>
        public static QuerySQL EqualNames
        {
            get { return EqualNamesValue; }
        }

        /// <summary>Gets the SQL for the incomplete grade value.</summary>
        public static QueryValue IncompleteGradeValue
        {
            get { return IncompleteGradeValueValue; }
        }

        /// <summary>The name of the early end table.</summary>
        private static readonly QueryTable EarlyEndTableValue = new QueryTable("EarlyEndTableValue");

        /// <summary>The name of the leak down table.</summary>
        private static readonly QueryTable LeakDownTableValue = new QueryTable("LeakDownTable");

        /// <summary>The name of the name ID table.</summary>
        private static readonly QueryTable NameIDsTableValue = new QueryTable("NameIDsTable");

        /// <summary>The name of the nominal dataset table.</summary>
        private static readonly QueryTable NominalDatasetTableValue = new QueryTable("NominalDatasetTable");

        /// <summary>The name of the results table.</summary>
        private static readonly QueryTable ResultsTableValue = new QueryTable("ResultsTable");

        /// <summary>The name of the rig type table.</summary>
        private static readonly QueryTable RigTypeTableValue = new QueryTable("RigTypeTable");

        /// <summary>The name of the traces table.</summary>
        private static readonly QueryTable TracesTableValue = new QueryTable("TracesTable");

        /// <summary>The name of the trend chart table.</summary>
        private static readonly QueryTable TrendChartTableValue = new QueryTable("TrendChartTable");

        /// <summary>The name of the trim code table.</summary>
        private static readonly QueryTable TrimCodeTableValue = new QueryTable("TrimCodeTable");

        /// <summary>The name of the trims table.</summary>
        private static readonly QueryTable TrimsTableValue = new QueryTable("TrimsTable");

        /// <summary>The name of the trimmed identify table.</summary>
        private static readonly QueryTable TrimmedIdentityTableValue = new QueryTable("TrimmedIdentityTable");

        /// <summary>The name of the trimmed pass off table.</summary>
        private static readonly QueryTable TrimmedPassOffTableValue = new QueryTable("TrimmedPassOffTable");

        /// <summary>The name of the data header table.</summary>
        private static readonly DataTable DataHeaderTableValue = new DataTable("DataHeaderTable");

        /// <summary>The name of the closed loop data table.</summary>
        private static readonly DataTable DataCLTableValue = new DataTable("DataCLTable");

        /// <summary>The name of the FOD data table.</summary>
        private static readonly DataTable DataFODTableValue = new DataTable("DataFODTable");

        /// <summary>The name of the leak data table.</summary>
        private static readonly DataTable DataLeakTableValue = new DataTable("DataLeakTable");

        /// <summary>The name of the open loop data table.</summary>
        private static readonly DataTable DataOLTableValue = new DataTable("DataOLTable");

        /// <summary>The name of the OMV data table.</summary>
        private static readonly DataTable DataOMVTableValue = new DataTable("DataOMVTable");

        /// <summary>The name of the stabilise closed loop data table.</summary>
        private static readonly DataTable DataStabiliseCLTableValue = new DataTable("DataStabiliseCLTable");

        /// <summary>The name of the stabilise open loop data table.</summary>
        private static readonly DataTable DataStabiliseOLTableValue = new DataTable("DataStabiliseOLTable");

        /// <summary>The name of the trimmed data table.</summary>
        private static readonly DataTable DataTrimmedTableValue = new DataTable("DataTrimmedTable");

        /// <summary>The name of the header table.</summary>
        private static readonly HeaderTable HeaderTableValue = new HeaderTable("HeaderTable");

        /// <summary>The name of the closed loop header table.</summary>
        private static readonly HeaderTable HeaderCLTableValue = new HeaderTable("HeaderCLTable");

        /// <summary>The name of the FOD header table.</summary>
        private static readonly HeaderTable HeaderFODTableValue = new HeaderTable("HeaderFODTable");

        /// <summary>The name of the leak header table.</summary>
        private static readonly HeaderTable HeaderLeakTableValue = new HeaderTable("HeaderLeakTable");

        /// <summary>he name of the open loop header table.</summary>
        private static readonly HeaderTable HeaderOLTableValue = new HeaderTable("HeaderOLTable");

        /// <summary>The name of the OMV header table.</summary>
        private static readonly HeaderTable HeaderOMVTableValue = new HeaderTable("HeaderOMVTable");

        /// <summary>The name of the stabilise closed loop header table.</summary>
        private static readonly HeaderTable HeaderStabiliseCLTableValue = new HeaderTable("HeaderStabiliseCLTable");

        /// <summary>The name of the stabilise open loop header table.</summary>
        private static readonly HeaderTable HeaderStabiliseOLTableValue = new HeaderTable("HeaderStabiliseOLTable");

        /// <summary>The name of the trimmed header table.</summary>
        private static readonly HeaderTable HeaderTrimmedTableValue = new HeaderTable("HeaderTrimmedTable");

        /// <summary>The name of the closed loop data table URID column.</summary>
        private static readonly DataTableColumn DataCLTableURIDColumnValue = new DataTableColumn("DataCLTableURIDColumn");

        /// <summary>The name of the FOD data table URID column.</summary>
        private static readonly DataTableColumn DataFODTableURIDColumnValue = new DataTableColumn("DataFODTableURIDColumn");

        /// <summary>The name of the leak data table URID column.</summary>
        private static readonly DataTableColumn DataLeakTableURIDColumnValue = new DataTableColumn("DataLeakTableURIDColumn");

        /// <summary>The name of the open loop data table URID column.</summary>
        private static readonly DataTableColumn DataOLTableURIDColumnValue = new DataTableColumn("DataOLTableURIDColumn");

        /// <summary>The name of the OMV data table URID column.</summary>
        private static readonly DataTableColumn DataOMVTableURIDColumnValue = new DataTableColumn("DataOMVTableURIDColumn");

        /// <summary>The name of the stabilise closed loop data table URID column.</summary>
        private static readonly DataTableColumn DataStabiliseCLTableURIDColumnValue = new DataTableColumn("DataStabiliseCLTableURIDColumn");

        /// <summary>The name of the stabilise open loop data table URID column.</summary>
        private static readonly DataTableColumn DataStabiliseOLTableURIDColumnValue = new DataTableColumn("DataStabiliseOLTableURIDColumn");

        /// <summary>The name of the trimmed data table URID column.</summary>
        private static readonly DataTableColumn DataTrimmedTableURIDColumnValue = new DataTableColumn("DataTrimmedTableURIDColumn");

        /// <summary>The name of the header table identifier column.</summary>
        private static readonly HeaderTableColumn HeaderTableIDENTColumnValue = new HeaderTableColumn("HeaderTableIDENTColumn");

        /// <summary>The name of the header table test type column.</summary>
        private static readonly HeaderTableColumn HeaderTableTestTypeColumnValue = new HeaderTableColumn("HeaderTableTestTypeColumn");

        /// <summary>The name of the header table Lua SVN column.</summary>
        private static readonly HeaderTableColumn HeaderTableLuaSVNColumnValue = new HeaderTableColumn("HeaderTableLuaSVNColumn");

        /// <summary>The name of the nominal dataset table identifier column.</summary>
        private static readonly HeaderTableColumn NominalDatasetTableIDENTColumnValue = new HeaderTableColumn("NominalDatasetTableIDENTColumn");

        /// <summary>The name of the closed loop header table URID column.</summary>
        private static readonly HeaderTableColumn HeaderCLTableURIDColumnValue = new HeaderTableColumn("HeaderCLTableURIDColumn");

        /// <summary>The name of the FOD header table URID column.</summary>
        private static readonly HeaderTableColumn HeaderFODTableURIDColumnValue = new HeaderTableColumn("HeaderFODTableURIDColumn");

        /// <summary>The name of the leak header table URID column.</summary>
        private static readonly HeaderTableColumn HeaderLeakTableURIDColumnValue = new HeaderTableColumn("HeaderLeakTableURIDColumn");

        /// <summary>The name of the open loop header table URID column.</summary>
        private static readonly HeaderTableColumn HeaderOLTableURIDColumnValue = new HeaderTableColumn("HeaderOLTableURIDColumn");

        /// <summary>The name of the OMV header table URID column.</summary>
        private static readonly HeaderTableColumn HeaderOMVTableURIDColumnValue = new HeaderTableColumn("HeaderOMVTableURIDColumn");

        /// <summary>The name of the stabilise closed loop header table URID column.</summary>
        private static readonly HeaderTableColumn HeaderStabiliseCLTableURIDColumnValue = new HeaderTableColumn("HeaderStabiliseCLTableURIDColumn");

        /// <summary>The name of the stabilise open loop header table URID column.</summary>
        private static readonly HeaderTableColumn HeaderStabiliseOLTableURIDColumnValue = new HeaderTableColumn("HeaderStabiliseOLTableURIDColumn");

        /// <summary>The name of the trimmed header table URID column.</summary>
        private static readonly HeaderTableColumn HeaderTrimmedTableURIDColumnValue = new HeaderTableColumn("HeaderTrimmedTableURIDColumn");

        /// <summary>The name of the closed loop header table duration column.</summary>
        private static readonly QueryColumn HeaderCLTableDurationColumnValue = new QueryColumn("HeaderCLTableDurationColumn");

        /// <summary>The name of the open loop header table duration column.</summary>
        private static readonly QueryColumn HeaderOLTableDurationColumnValue = new QueryColumn("HeaderOLTableDurationColumn");

        /// <summary>The name of the trimmed header table duration column.</summary>
        private static readonly QueryColumn HeaderTrimmedTableDurationColumnValue = new QueryColumn("HeaderTrimmedTableDurationColumn");

        /// <summary>The name of the closed loop header table median column.</summary>
        private static readonly QueryColumn HeaderCLTableMedianColumnValue = new QueryColumn("HeaderCLTableMedianColumn");

        /// <summary>The name of the open loop header table median column.</summary>
        private static readonly QueryColumn HeaderOLTableMedianColumnValue = new QueryColumn("HeaderOLTableMedianColumn");

        /// <summary>The name of the trimmed header table median column.</summary>
        private static readonly QueryColumn HeaderTrimmedTableMedianColumnValue = new QueryColumn("HeaderTrimmedTableMedianColumn");

        /// <summary>The name of the fuel demand column.</summary>
        private static readonly QueryColumn FuelDemandColumnValue = new QueryColumn("FuelDemandColumn");

        /// <summary>The name of the nominal fuel column.</summary>
        private static readonly QueryColumn NominalFuelColumnValue = new QueryColumn("NominalFuelColumn");

        /// <summary>The name of the trace identifier column.</summary>
        private static readonly QueryColumn TraceTableIdentColumnValue = new QueryColumn("TraceTableIdentColumn");

        /// <summary>The name of the trace binary trace data column.</summary>
        private static readonly QueryColumn TraceTableBinaryTraceDataColumnValue = new QueryColumn("TraceTableBinaryTraceDataColumn");

        /// <summary>The name of the trims table value column.</summary>
        private static readonly QueryColumn TrimsTableValueColumnValue = new QueryColumn("TrimsTableValueColumn");

        /// <summary>The name of the grades table value column.</summary>
        private static readonly QueryColumn GradesTableValueColumnValue = new QueryColumn("GradesTableValueColumn");

        /// <summary>The SQL used in the join of the name ID table to join based on plant number.</summary>
        private static readonly QuerySQL EqualNamesValue = new QuerySQL("EqualNames");

        /// <summary>The incomplete grade value. </summary>
        private static readonly QueryValue IncompleteGradeValueValue = new QueryValue("IncompleteGradeValue");
    }
}
