﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.TestTypeListQuery
{
    /// <summary>
    /// Class used to return distinct test type values between and start and end date
    /// for the selected LOP's and selected test plans.
    /// </summary>
    public class Query : TestPlanListQuery.Query
    {
        /// <summary>
        /// Gets the selected test plans used in the queries.
        /// </summary>
        protected List<string> SelectedTestPlans { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="TestTypeListQuery.Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="dateFrom">The start date to be used in selecting the test types.</param>
        /// <param name="dateTo">The end date to be used in selecting the test types.</param>
        /// <param name="selectedLops">The selected LOP's to be used in selecting the test types.</param>
        /// <param name="selectedTestPlans">The selected test plans to be used in selecting the test types.</param>
        public Query(Database.DatabaseBase database, string dateFrom, string dateTo, List<string> selectedLops, List<string> selectedTestPlans)
            : base(database, dateFrom, dateTo, selectedLops)
        {
            SelectedTestPlans = selectedTestPlans;
        }

        /// <summary>
        /// Returns the query to execute.
        /// </summary>
        /// <returns>The query to execute.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        public override string GetQuery()
        {
            var query = new StringBuilder();

            // Query distinct records for selected dates
            query.AppendLines(
                "SELECT DISTINCT he.TestType FROM [", HeaderTable, "] AS he",
                "WHERE he.PDate > {0} AND he.PDate < {1}".Args(DateFrom, DateTo),
                "AND he.LOP in ({0})".Args(StringExt.QuotedJoin(SelectedLops)),
                "AND he.TestPlan in ({0})".Args(StringExt.QuotedJoin(SelectedTestPlans)),
                "ORDER BY he.TestType asc");
            QueryModifier.ModifyOLQueryForNonFT(query);

            return query.ToString();
        }
    }
}
