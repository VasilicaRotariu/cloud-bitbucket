﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.LineNumberListQuery
{
    /// <summary>
    /// Class used to return distinct line number values between and start and end date
    /// for the selected LOP's, selected test plans, selected test types and selected
    /// plant numbers.
    /// </summary>
    public class Query : PlantNumberListQuery.Query
    {
        /// <summary>
        /// Gets the selected plant numbers used in the queries.
        /// </summary>
        protected List<string> SelectedPlantNumbers { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="LineNumberListQuery.Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="dateFrom">The start date to be used in selecting the line numbers.</param>
        /// <param name="dateTo">The end date to be used in selecting the line numbers.</param>
        /// <param name="selectedLops">The selected LOP's to be used in selecting the line numbers.</param>
        /// <param name="selectedTestPlans">The selected test plans to be used in selecting the line numbers.</param>
        /// <param name="selectedTestTypes">The selected test types to be used in selecting the line numbers.</param>
        /// <param name="selectedPlantNumbers">The selected plant numbers to be used in selecting the line numbers.</param>
        public Query(
            Database.DatabaseBase database, 
            string dateFrom, 
            string dateTo, 
            List<string> selectedLops, 
            List<string> selectedTestPlans, 
            List<string> selectedTestTypes, 
            List<string> selectedPlantNumbers)
            : base(database, dateFrom, dateTo, selectedLops, selectedTestPlans, selectedTestTypes)
        {
            SelectedPlantNumbers = selectedPlantNumbers;
        }

        /// <summary>
        /// Returns the query to execute.
        /// </summary>
        /// <returns>The query to execute.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        public override string GetQuery()
        {
            var query = new StringBuilder();

            // Query distinct records for selected dates
            query.AppendLines(
                "SELECT DISTINCT he.LineNumber FROM [", HeaderTable, "] AS he",
                "WHERE he.PDate > {0} AND he.PDate < {1}".Args(DateFrom, DateTo),
                "AND he.LOP in ({0})".Args(StringExt.QuotedJoin(SelectedLops)),
                "AND he.TestPlan in ({0})".Args(StringExt.QuotedJoin(SelectedTestPlans)));
            if (Database.FtStructure)
            {
                query.AppendLines("AND he.TestType in ({0})".Args(StringExt.QuotedJoin(SelectedTestTypes)));
            }
            query.AppendLines(
                "AND he.PlantNumber in ({0})".Args(StringExt.QuotedJoin(SelectedPlantNumbers)),
                "ORDER BY he.LineNumber asc");
            QueryModifier.ModifyOLQueryForNonFT(query);

            return query.ToString();
        }
    }
}
