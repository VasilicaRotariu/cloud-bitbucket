﻿using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Base class for all list queries. List queries are queries used to return a
    /// list of single column results.
    /// </summary>
    public abstract class ListQueryBase : Queries.Parameters.QueryParameters
    {
        /// <summary>
        /// Gets the database.
        /// </summary>
        protected Database.DatabaseBase Database { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="ListQueryBase"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        protected ListQueryBase(Database.DatabaseBase database)
        {
            Database = database;
        }

        /// <summary>
        /// Returns the query to execute.
        /// </summary>
        /// <returns>The query to execute.</returns>
        public abstract string GetQuery();

        /// <summary>
        /// Returns a data table with the query results.
        /// </summary>
        /// <returns>The query results as a data table.</returns>
        public DataTable GetDataTable()
        {
            var dataTable = new DataTable();

            // Open the query, fill the data table
            try
            {
                var query = GetQuery();
                if (query.IsNotEmpty())
                {
                    var adapter = new SqlDataAdapter(query, Database.ConnectionString);
                    adapter.Fill(dataTable);

                    if (Database.HasIasiArchiveStructure && Database.QueryIasiArchiveStructure)
                    {
                        adapter.FillLoadOption = LoadOption.Upsert;
                        adapter = new SqlDataAdapter(query, Database.IasiArchiveDatabase.ConnectionString);
                        adapter.Fill(dataTable);
                    }

                    if (Database.HasSudburyArchiveStructure && Database.QuerySudburyArchiveStructure)
                    {
                        adapter.FillLoadOption = LoadOption.Upsert;
                        adapter = new SqlDataAdapter(query, Database.SudburyArchiveDatabase.ConnectionString);
                        adapter.Fill(dataTable);
                    }

                    dataTable = dataTable.DefaultView.ToTable( /*distinct*/ true);
                }
            }
            catch (SqlException exception)
            {
                MessageBox.Show(exception.Message);
            }

            return dataTable;
        }
    }
}
