﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.PlantNumberListQuery
{
    /// <summary>
    /// Class used to return distinct plant number values between and start and end date
    /// for the selected LOP's, selected test plans and selected test types.
    /// </summary>
    public class Query : TestTypeListQuery.Query
    {
        /// <summary>
        /// Gets or sets the selected test types used in the queries.
        /// </summary>
        public List<string> SelectedTestTypes { protected get; set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="PlantNumberListQuery.Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="dateFrom">The start date to be used in selecting the plant numbers.</param>
        /// <param name="dateTo">The end date to be used in selecting the plant numbers.</param>
        /// <param name="selectedLops">The selected LOP's to be used in selecting the plant numbers.</param>
        /// <param name="selectedTestPlans">The selected test plans to be used in selecting the plant numbers.</param>
        /// <param name="selectedTestTypes">The selected test types to be used in selecting the plant numbers.</param>
        public Query(Database.DatabaseBase database, string dateFrom, string dateTo, List<string> selectedLops, List<string> selectedTestPlans, List<string> selectedTestTypes)
            : base(database, dateFrom, dateTo, selectedLops, selectedTestPlans)
        {
            SelectedTestTypes = selectedTestTypes;
        }

        /// <summary>
        /// Returns the query to execute.
        /// </summary>
        /// <returns>The query to execute.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        public override string GetQuery()
        {
            var query = new StringBuilder();

            if (SelectedTestPlans.Count > 0)
            {
                // Query distinct records for selected dates
                query.AppendLines(
                    "SELECT DISTINCT he.PlantNumber FROM [", HeaderTable, "] AS he",
                    "WHERE he.PDate > {0} AND he.PDate < {1}".Args(DateFrom, DateTo),
                    "AND he.LOP in ({0})".Args(StringExt.QuotedJoin(SelectedLops)),
                    "AND he.TestPlan in ({0})".Args(StringExt.QuotedJoin(SelectedTestPlans)));
                if (Database.FtStructure)
                {
                    query.AppendLines("AND he.TestType in ({0})".Args(StringExt.QuotedJoin(SelectedTestTypes)));
                }
                query.AppendLines("ORDER BY he.PlantNumber asc");
                QueryModifier.ModifyOLQueryForNonFT(query);
            }

            return query.ToString();
        }
    }
}
