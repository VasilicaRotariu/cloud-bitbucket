﻿using System.Diagnostics.CodeAnalysis;
using System.Text;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.NominalUTIDQuery
{
    /// <summary>
    /// Class used to return open loop nominal data rows for the specified identifiers.
    /// </summary>
    public class Query : ListQueryBase
    {
        /// <summary>
        /// Gets or sets the identifiers used in the queries.
        /// </summary>
        public string Idents { protected get; set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="NominalUTIDQuery.Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="idents">The identifiers to be used in the query.</param>
        public Query(Database.DatabaseBase database, string idents)
            : base(database)
        {
            Idents = idents;
        }

        /// <summary>
        /// Returns the query to execute.
        /// </summary>
        /// <returns>The query to execute.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        public override string GetQuery()
        {
            var query = new StringBuilder();

            // Query distinct records for selected dates
            query.AppendLines(
                "SELECT * FROM [", NominalDatasetTable, "] AS nd",
                "WHERE nd.", NominalDatasetTableIDENTColumn, " IN ({0})".Args(Idents),
                "AND nd.ValidFrom IN",
                "(SELECT MAX(mx.validfrom)",
                "FROM [", NominalDatasetTable, "] AS mx",
                "WHERE mx.", NominalDatasetTableIDENTColumn, " = nd.", NominalDatasetTableIDENTColumn, ")");
            QueryModifier.ModifyOLQueryForNonFT(query);

            return query.ToString();
        }
    }
}
