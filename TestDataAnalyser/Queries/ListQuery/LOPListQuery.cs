﻿using System.Diagnostics.CodeAnalysis;
using System.Text;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.LOPListQuery
{
    /// <summary>
    /// Class used to return distinct LOP values between and start and end date.
    /// </summary>
    public class Query : ListQueryBase
    {
        /// <summary>
        /// Gets the start date used in the queries.
        /// </summary>
        protected string DateFrom { get; private set; }

        /// <summary>
        /// Gets the end date used in the queries.
        /// </summary>
        protected string DateTo { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="LOPListQuery.Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="dateFrom">The start date to be used in selecting the LOP's.</param>
        /// <param name="dateTo">The end date to be used in selecting the LOP's.</param>
        public Query(Database.DatabaseBase database, string dateFrom, string dateTo)
            : base(database)
        {
            DateFrom = dateFrom;
            DateTo = dateTo;
        }

        /// <summary>
        /// Returns the query to execute.
        /// </summary>
        /// <returns>The query to execute.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        public override string GetQuery()
        {
            var query = new StringBuilder();

            // Query distinct records for selected dates
            query.AppendLines(
                "SELECT DISTINCT he.LOP FROM [", HeaderTable, "] AS he",
                "WHERE he.PDate > {0} AND he.PDate < {1}".Args(DateFrom, DateTo),
                "ORDER BY he.LOP asc");
            QueryModifier.ModifyOLQueryForNonFT(query);

            return query.ToString();
        }
    }
}
