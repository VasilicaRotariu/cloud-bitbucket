﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.TestPlanListQuery
{
    /// <summary>
    /// Class used to return distinct test plan values between and start and end date
    /// for the selected LOP's.
    /// </summary>
    public class Query : LOPListQuery.Query
    {
        /// <summary>
        /// Gets the selected LOP's used in the queries.
        /// </summary>
        protected List<string> SelectedLops { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="TestPlanListQuery.Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="dateFrom">The start date to be used in selecting the test plans.</param>
        /// <param name="dateTo">The end date to be used in selecting the test plans.</param>
        /// <param name="selectedLops">The selected LOP's to be used in selecting the test plans.</param>
        public Query(Database.DatabaseBase database, string dateFrom, string dateTo, List<string> selectedLops)
            : base(database, dateFrom, dateTo)
        {
            SelectedLops = selectedLops;
        }

        /// <summary>
        /// Returns the query to execute.
        /// </summary>
        /// <returns>The query to execute.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        public override string GetQuery()
        {
            var query = new StringBuilder();

            // Query distinct records for selected dates
            query.AppendLines(
                "SELECT DISTINCT he.TestPlan FROM [", HeaderTable, "] AS he",
                "WHERE he.PDate > {0} AND he.PDate < {1}".Args(DateFrom, DateTo),
                "AND he.LOP in ({0})".Args(StringExt.QuotedJoin(SelectedLops)),
                "ORDER BY he.TestPlan asc");
            QueryModifier.ModifyOLQueryForNonFT(query);

            return query.ToString();
        }
    }
}
