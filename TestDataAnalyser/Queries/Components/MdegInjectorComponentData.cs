﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.MDEGInjectorComponentData
{
    /// <summary>
    /// Default query for virgins.
    /// See Virgin.<see cref="Row"/> for the rows returned from Execute.
    /// </summary>
    public class Query : QueryBase<Row>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="startDate">The start date. Used when no injectors are specified.</param>
        /// <param name="endDate">The end date. Used when no injectors are specified.</param>
        /// <param name="injectorSerialNumbers">The set of injector serial numbers used in the query.</param>
        public Query(Database.DatabaseBase database, string startDate, string endDate, List<string> injectorSerialNumbers)
            : base(database)
        {
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.InjectorSerialNumbers = injectorSerialNumbers;
        }

        /// <summary>Gets or sets the start date. Used when no injectors are specified.</summary>
        private string StartDate { get; set; }

        /// <summary>Gets or sets the end date. Used when no injectors are specified.</summary>
        private string EndDate { get; set; }

        /// <summary>Gets or sets the set of injector serial numbers.</summary>
        private List<string> InjectorSerialNumbers { get; set; } 

        /// <summary>
        /// Returns the query used to get the rows.
        /// </summary>
        /// <returns>The query used to get the rows.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines", Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma", Justification = "Disabled rule to improve readability of SQL.")]
        protected override string GetQuery()
        {
            string query = @"
SELECT 
	MAX(ISNData.AssemblyDate) as AssemblyDate, 
	ISNData.ISN, 
	MAX(ISNData.NozzleSerial) as NozzleSerial, 
	MAX(ISNData.NCVSerial) as NCVSerial, 	
	MAX(ISNData.PistonGuideSerial) as PistonGuideSerial, 	
	--Counts
	CONVERT(INT, MAX(ISNData.NozzleCount)) as NozzleCount,
	CONVERT(INT, MAX(ISNData.NCVCount)) as NCVCount,	
	CONVERT(INT, MAX(ISNData.PistonGuideCount)) as PistonGuideCount,
	--FAG / Tilt
	MAX(FagTiltData.Fag) as Fag,
	MAX(FagTiltData.Tilt) as Tilt,

	MAX(NeedleInFlow.Flow_MeasuredFlow) as NeedleInFlow,
	MAX(NeedleOutFlow.FM_Flow) as NeedleOutFlow,

	MAX([Capsule_NeedleLift]) as NeedleLift, 

	MAX(AssemblyClearance_mm) as NCVClearance,
	MAX(AssemblyLift) as NCVLift,

	MAX(ComponentHEGNCV.FM_Flow2) as RdO,
	MAX(ComponentHEGPG.FM_Flow2) as InO,

	MAX(RDFLow.Flow) as RDFlow,
	MAX(InOFlow.FM_Flow2) as InOFlow,
	MAX(SPO.FM_Flow) as SPO

FROM (
	SELECT AssemblyDate, ISN, NCVSerial, nnc.NozzleSerial, nnc.PistonGuideSerial, NCVCount, MDEGRefined.NozzleCount, nnc.PistonGuideCount
	FROM (
		SELECT 
			MAX(AssemblyDate) as AssemblyDate, 
			ISN,
			MAX(NCVSerial) as NCVSerial, 
			MAX(NozzleSerial) as NozzleSerial, 
			MAX(NCVCount) as NCVCount, 
			MAX(NozzleCount) as NozzleCount		
		FROM (
			SELECT
				rspl.Pdate as AssemblyDate, 
				rspl.Injector_ID as ISN, 
				NCV_DataMatrix as NCVSerial,
				Nozzle_DataMatrix as NozzleSerial, 		
				NCV_DataMatrixCount as NCVCount,
				Nozzle_DataMatrixCount as NozzleCount 
			FROM RSPL as rspl 
			LEFT JOIN NLS as nls on (nls.Injector_ID = rspl.Injector_ID) 
			UNION ALL
			SELECT 
				Pdate as AssemblyDate, 
				InjectorSerialNumber as ISN, 
				NCVSerialNumber, 
				NozzleSerialNumber as NozzleSerial, 
				NCVSerialNumberCount as NCVCount,
				NozzleSerialNumberCount as NozzleCount 				
			FROM MDEGLowVolumeLineResults 
		) as MdegResults	
		GROUP BY ISN
	) as MDEGRefined
	LEFT JOIN NNCAssembly as nnc on (nnc.NozzleSerial = MDEGRefined.NozzleSerial)
	WHERE ISN in (SELECT isn FROM ({0}) as X(isn)  
)) as ISNData
-- JOIN FAG AND TILT Data FRom NCV Armature
LEFT JOIN (
	SELECT   
		[SerialNumber] as Serial,
		[Fag],
		[Tilt]
	FROM [Euro6].[dbo].[Zygo] As Z
    WHERE Result = 0
	union all  
	SELECT
		[DataMatrix] as Serial, 
		[Fag],
		[Tilt]
	FROM NCVArmaturePress as CAT
    WHERE Result = 0 
) as FagTiltData on (ISNData.NCVSerial = FagTiltData.Serial)

-- JOIN Needle In Flow
LEFT JOIN NozzleTestWithError_Results as NeedleInFlow on (ISNData.NozzleSerial = NeedleInFlow.Serial) 
-- JOIN Needle Out Flow
LEFT JOIN NozzleHegsData as NeedleOutFlow on (ISNData.NozzleSerial = NeedleOutFlow.SerialNum) 
-- JOIN Needle Lift
LEFT JOIN [NNCAssembly] as NeedleLift on (ISNData.NozzleSerial = NeedleLift.NozzleSerial) 
 -- SA Data NCV Lift and Clearance
LEFT JOIN [SelAssyAssembly] as SA_NCVData on (ISNData.NCVSerial = SA_NCVData.BodyMatrix COLLATE database_default AND FailureCode = 1)
-- RdO
LEFT JOIN [ComponentHegs] as ComponentHEGNCV on (ISNData.NCVSerial = ComponentHEGNCV.Serial COLLATE database_default)
---- InO
LEFT JOIN [ComponentHegs] as ComponentHEGPG on (ISNData.PistonGuideSerial = ComponentHEGPG.Serial COLLATE database_default) 
--  RdFlow
LEFT JOIN HPVT as RDFlow on (ISNData.NCVSerial = RDFlow.SerialNumber )
-- InOFlow
LEFT JOIN ManualComponentHeg as InOFlow on (ISNData.PistonGuideSerial = RDFlow.SerialNumber )
-- SPO Flow
LEFT JOIN ComponentHegs as SPO on (ISNData.PistonGuideSerial = SPO.Serial )

GROUP BY ISN
ORDER BY ISN".Args(GenerateISNStringFromList());

            return query;
        }

        /// <summary>
        /// Generates a list of values for use in SQL using a list of ISNs
        /// </summary>
        /// <returns>SQL String</returns>
        protected string GenerateISNStringFromList()
        {
            if (InjectorSerialNumbers.Count > 0)
            { 
                return "values ('{0}')".Args(string.Join("'),('", InjectorSerialNumbers));
            }
            return string.Empty;
        }
    }
}
