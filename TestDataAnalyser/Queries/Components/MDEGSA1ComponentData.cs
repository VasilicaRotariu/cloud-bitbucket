﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SudburyDataAnalyser.MDEGSA1ComponentData
{
    /// <summary>
    /// Query to get the SA1 Component Data
    /// </summary>
    public class Query : QueryBase<Row>
    {
        /// <summary>
        /// Gets a list of NCV Matrices 
        /// </summary>
        public List<string> NCVMatrices { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="Query" /> class
        /// </summary>
        /// <param name="database">The database connection to use</param>
        /// <param name="ncvMatricies">List of Matrices to get</param>
        public Query(Database.DatabaseBase database, IEnumerable<string> ncvMatricies)
            : base(database)
        {
            this.NCVMatrices = ncvMatricies.ToList();
        }
         
        /// <summary>
        /// Gets the Query
        /// </summary>
        /// <returns>The Query</returns>
        protected override string GetQuery()
        {
            string query = @"
SELECT serials.serial, MAX(Clearance.Clearance) as Clearance, MAX(Lift.Lift) as Lift
FROM (
	SELECT serial
	FROM ({0})
	as X(serial)
) as serials
LEFT JOIN Assembly_ClearanceMeasureComplete as Clearance on (Clearance.Matrix = serials.serial AND Clearance.Passed = 0)
LEFT JOIN Assembly_LiftMeasureComplete as Lift on (Lift.Matrix = serials.serial AND Lift.Passed = 0)
GROUP BY serials.serial".Args(GenerateMatriciesStringFromList());

            return query;
        }

        /// <summary>
        /// Generates a list of values for use in SQL using a list of ISNs
        /// </summary>
        /// <returns>SQL String</returns>
        protected string GenerateMatriciesStringFromList()
        {
            if (NCVMatrices.Count > 0)
            {
                return "values ('{0}')".Args(string.Join("'),('", NCVMatrices));
            }
            return string.Empty;
        }
    }
}
