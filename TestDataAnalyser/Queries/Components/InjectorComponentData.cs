﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.InjectorComponentData
{
    /// <summary>
    /// Query for DAF Injector Components
    /// See Virgin.<see cref="Row"/> for the rows returned from Execute.
    /// </summary>
    public class Query : QueryBase<Row>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="startDate">The start date. Used when no injectors are specified.</param>
        /// <param name="endDate">The end date. Used when no injectors are specified.</param>
        /// <param name="injectorSerialNumbers">The set of injector serial numbers used in the query.</param>
        public Query(Database.DatabaseBase database, string startDate, string endDate, List<string> injectorSerialNumbers) : base(database)
        {
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.InjectorSerialNumbers = injectorSerialNumbers;
        }

        /// <summary>Gets or sets the start date. Used when no injectors are specified.</summary>
        private string StartDate { get; set; }

        /// <summary>Gets or sets the end date. Used when no injectors are specified.</summary>
        private string EndDate { get; set; }

        /// <summary>Gets or sets the set of injector serial numbers.</summary>
        private List<string> InjectorSerialNumbers { get; set; } 

        /// <summary>
        /// Returns the query used to get the rows.
        /// </summary>
        /// <returns>The query used to get the rows.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines", Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma", Justification = "Disabled rule to improve readability of SQL.")]
        protected override string GetQuery()
        {
            string newQuery = @"USE Euro6;

SELECT 
    MAX(AssemblyDate) as AssemblyDate, 
    ISN, 
    MAX(NozzleSerial) as NozzleSerial, 
    MAX(NCVSerial) as NCVSerial,     
    MAX(PistonGuideSerial) as PistonGuideSerial,     

    --Counts
    CONVERT(INT, MAX(NozzleCount)) as NozzleCount,
    CONVERT(INT, MAX(NCVCount)) as NCVCount,    
    CONVERT(INT, MAX(PistonGuideCount)) as PistonGuideCount,
    
    --FAG / Tilt
    MAX(FagTiltData.Fag) as Fag,
    MAX(FagTiltData.Tilt) as Tilt,
    MAX(Flow_MeasuredFlow) as NeedleInFlow,
    MAX(OutFlow) as NeedleOutFlow,
    MAX([Capsule_NeedleLift]) as NeedleLift, 
    MAX(NCVClearance) as NCVClearance,
    MAX(NCVLift) as NCVLift,
    MAX(RdO) as RdO,
    MAX(InO) as InO
    FROM (
    SELECT 
        AssemblyDate, 
        ISN, 
        NCVSerial, 
        nnc.NozzleSerial, --nnc Nozzle Serial, we join on this so doesnt matter if NNC or not
        nnc.PistonGuideSerial,
        NCVCount, 
        --Select the maximum of the nozzle counts
        CASE
            WHEN nnc.NozzleCount > DAFResultsRefined.NozzleCount THEN nnc.NozzleCount 
            ELSE DAFResultsRefined.NozzleCount
        END as NozzleCount,
        nnc.PistonGuideCount 
FROM (
    SELECT MAX(AssemblyDate) as AssemblyDate, 
            ISN,
            MAX(NCVSerial) as NCVSerial, 
            MAX(NozzleSerial) as NozzleSerial, 
            MAX(NCV_DataMatrixCount) as NCVCount, 
            MAX(Nozzle_DataMatrixCount) as NozzleCount         
    FROM (
        SELECT 
            PDate as AssemblyDate, 
				   
            ISN, 
            NCVSerial as NCVSerial, 
            NozzleSerial as NozzleSerial, 
            NCV_DataMatrixCount,     
            Nozzle_DataMatrixCount
        FROM vw_AssemblyComponentTotals
        --FROM MASMEC_SelAsmb_OP20B_Results 
        --UNION
        --SELECT 
        --    PDate as AssemblyDate, 
        --    ISN, 
        --    NCVSerial as NCVSerial, 
        --    NozzleSerial as NozzleSerial, 
        --    NCVSerialCount, 
        --    NozzleSerialCount
        --FROM LowVolumeMX13Line1
    ) as DAFResults
    GROUP BY ISN
) as DAFResultsRefined
INNER JOIN NNCAssembly as nnc on (nnc.NozzleSerial = DAFResultsRefined.NozzleSerial)
        WHERE isn IN (
		SELECT isn FROM  ({0}) as X(isn) )
  
    ) as ISNData

	
-- JOIN FAG AND TILT Data FRom NCV Armature
LEFT JOIN (
    SELECT   
        [SerialNumber],
        [PDate],
        [Fag],
        [Tilt],
        [Result]
    FROM [Euro6].[dbo].[Zygo] As Z
    union all  
    SELECT
        [DataMatrix],
        [PDate],
        [FAG],
        [Tilt],
        [Result]
    FROM NCVArmaturePress as CAT
    union all  
    SELECT
        [DataMatrix],
        [PDate],
        [FAG],
        [Tilt],
        [Result]
    FROM ManualArmatureGaugeResults as AP
) as FagTiltData on (ISNData.NCVSerial = FagTiltData.SerialNumber)

 

-- JOIN Needle In Flow
LEFT JOIN (
    SELECT Serial, Flow_MeasuredFlow
    FROM NozzleTestWithError_Results
) as NeedleInFlow on (ISNData.NozzleSerial = NeedleInFlow.Serial)

 

-- JOIN Needle Out Flow
LEFT JOIN (
    SELECT 
        PDate, 
        SerialNum AS Serial, 
        FM_Flow AS OutFlow
    FROM NozzleHegsData
) as NeedleOutFlow on (ISNData.NozzleSerial = NeedleOutFlow.Serial)

 

-- JOIN Needle Lift
LEFT JOIN (
    SELECT 
    [NozzleSerial] as Serial, 
    [Capsule_NeedleLift] 
FROM [Euro6].[dbo].[NNCAssembly]
) as NeedleLift on (ISNData.NozzleSerial = NeedleLift.Serial)
 
 -- SA Data NCV Lift and Clearance
LEFT JOIN (
    SELECT     
        [BodyMatrix] as Serial,      
        [AssemblyClearance_mm] as NCVClearance,
        [AssemblyLift] as NCVLift
    FROM [SelAssyAssembly]
    WHERE FailureCode = 1
) as SA_NCVData on (ISNData.NCVSerial = SA_NCVData.Serial COLLATE database_default)

 

-- RdO
LEFT JOIN (
    SELECT
    Serial, FM_Flow2 as RdO
    FROM [ComponentHegs]
) as ComponentHEGNCV on (ISNData.NCVSerial = ComponentHEGNCV.Serial COLLATE database_default)

 

---- InO
LEFT JOIN (
    SELECT 
    Serial, FM_Flow2 as InO 
    FROM [ComponentHegs]
) as ComponentHEGPG on (ISNData.PistonGuideSerial = ComponentHEGPG.Serial COLLATE database_default)

 

-- FINAL GROUP BY 
GROUP BY ISN".Args(GenerateISNStringFromList());
            
            return newQuery;
        }

        /// <summary>
        /// Generates a list of values for use in SQL using a list of ISNs
        /// </summary>
        /// <returns>SQL string</returns>
        protected string GenerateISNStringFromList()
        {
            if (InjectorSerialNumbers.Count > 0)
            {
                return "values ('{0}')".Args(string.Join("'),('", InjectorSerialNumbers));
            }
            return string.Empty;
        }
    }
}
