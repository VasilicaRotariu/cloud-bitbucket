﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Office.Interop.Excel;
using Application = Microsoft.Office.Interop.Excel.Application;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Base class for all SQL query classes.
    /// Override GetQuery to return the query to execute.
    /// Calling Execute returns all the rows returned from the query.
    /// </summary>
    /// <typeparam name="T">Row type returned from the query</typeparam>
    public class QueryBase<T> : QueryBaseSubQueries where T : RowBase, new()
    {
        /// <summary>
        /// Gets the database.
        /// </summary>
        protected Database.DatabaseBase Database { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="QueryBase{T}"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        public QueryBase(Database.DatabaseBase database)
        {
            Database = database;
        }

        /// <summary>
        /// Executes the SQL query returned by GetQuery and returns all the rows returned from that query.
        /// </summary>
        /// <returns>Rows returned from the SQL query.</returns>
        public List<T> Execute(bool enableCaching = false)
        {
            List<T> rows = null;
            string query = GetQuery();

            if (enableCaching)
            {
                rows = GetCachedResults();

                if (rows != null)
                {
                    if (RequiresCacheUpdate())
                    {
                        System.Threading.Thread t = new System.Threading.Thread(() =>
                        {
                            List<T> newRows = null;
                            newRows = this.GetLatestRows(query);
                            SetCachedResults(newRows);
                        });
                        t.IsBackground = true;
                        t.Start();
                    }

                    return rows;
                }
            }

            rows = this.GetLatestRows(query);

            if (enableCaching)
            {
                SetCachedResults(rows);
            }

            return rows;
        }

        

        private List<T> GetLatestRows(string query)
        {
            List<T> rows = new List<T>();
            

            DebugLog.WriteLine("Using connection string {0}, executing query".Args(Database.ConnectionString));
            DebugLog.WriteLine(query);

            Stopwatch sw = new Stopwatch();
            //dawid 26-03 if ((DateTime.Today - FrmMain.dateToExtDateTimePicker.Value.ToLocalTime()).TotalDays < 160)
            if ((DateTime.Today - FrmMain.DateTo.ToLocalTime()).TotalDays < 160)
            {
                sw.Start();
                ExecQuery(Database, rows, query);
                sw.Stop();
            }
            //dawid 26-03 && (DateTime.Today - FrmMain.dateFromExtDateTimePicker.Value.ToLocalTime()).TotalDays > 160
            if (Database.HasIasiArchiveStructure && Database.QueryIasiArchiveStructure && (DateTime.Today - FrmMain.DateFrom.ToLocalTime()).TotalDays > 160)
            {
               // MessageBox.Show(">160");
                sw = new Stopwatch();
                sw.Start();
                ExecQuery(Database.IasiArchiveDatabase, rows, query);
                sw.Stop();
            }

            if (Database.HasSudburyArchiveStructure && Database.QuerySudburyArchiveStructure)
            {
                sw = new Stopwatch();
                sw.Start();
                ExecQuery(Database.SudburyArchiveDatabase, rows, query);
                sw.Stop();
            }

            DebugLog.WriteLine("Retrieved {0} rows in approximately {1}ms".Args(rows.Count, sw.Elapsed));

            return rows;
        }

        /// <summary>
        /// Executes the Query against a Given Data structure
        /// </summary>
        /// <param name="database"></param>
        /// <param name="rows"></param>
        /// <param name="query"></param>
        private void ExecQuery(Database.DatabaseBase database, List<T> rows, string query)
        {
            using (var connection = new SqlConnection(database.ConnectionString))
            using (var command = new SqlCommand(query, connection))
            {
                //dawid 26-03
                //MessageBox.Show(query + " connection: " + database.ConnectionString);
                connection.Open();
                bool failed = false;
                SqlDataReader reader = null;
                
                Console.WriteLine(query);

                // Set a 10 minute timeout
                command.CommandTimeout = 600;

                try
                {
                    reader = command.ExecuteReader();
                }
                catch (SqlException ex)
                {
                    // Log the error
                    failed = true;
                    DebugLog.WriteLine(ex.Message);
                }

                if (!failed)
                {
                    try
                    {
                        while (reader.Read())
                        {
                            var row = new T();
                            row.Initialise(reader);

                            rows.Add(row);
                        }
                    }
                    // ReSharper disable once EmptyGeneralCatchClause
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// Returns the query to execute. Override this to return a valid SQL query.
        /// </summary>
        /// <returns>Query to execute.</returns>
        protected virtual string GetQuery()
        {
            return string.Empty;
        }

        /// <summary>
        /// Will pull required data from cache then run the query stored in GetQuery() on a baground thread update the 
        /// cache as required. The cache will inly be updated the first time this is called. This is to speed up 
        /// long running qureies that retrun the same results most of the time and slow the app down for no reason
        /// </summary>
        protected virtual List<T> GetCachedResults()
        {
            return null;
        }

        /// <summary>
        /// Will write required data into local Cache
        /// </summary>
        protected virtual void SetCachedResults(object objectToWrite)
        {
        }

        /// <summary>
        /// Will write required data into local Cache
        /// </summary>
        protected virtual bool RequiresCacheUpdate()
        {
            return true;
        }

        private byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA256.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
    }
}
