﻿using System.Diagnostics.CodeAnalysis;
using System.Text;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.LimitsBase
{
    /// <summary>
    /// Base class for a limits class.
    /// </summary>
    public class Query : QueryBase<Row>
    {
        /// <summary>
        /// Gets or sets the plant number used in the limits query.
        /// </summary>
        public string PlantNumber { private get; set; }

        /// <summary>
        /// Gets or sets the LOP used in the limits query.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public string LOP { private get; set; }

        /// <summary>
        /// Gets or sets the pressure level used in the limit query.
        /// </summary>
        public string Pressure { private get; set; }

        /// <summary>
        /// Gets or sets the identity table used in the limits query.
        /// </summary>
        private Queries.Parameters.QueryTable IdentityTable { get; set; }

        /// <summary>
        /// Gets or sets the pass off table used in the limits query.
        /// </summary>
        private Queries.Parameters.QueryTable PassOffTable { get; set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="LimitsBase.Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="identityTable">The identity table.</param>
        /// <param name="passOffTable">The pass off table.</param>
        protected Query(
            Database.DatabaseBase database,
            Queries.Parameters.QueryTable identityTable,
            Queries.Parameters.QueryTable passOffTable)
            : base(database)
        {
            IdentityTable = identityTable;
            PassOffTable = passOffTable;
        }

        /// <summary>
        /// Returns the query used to get the limits rows.
        /// </summary>
        /// <returns>The query used to get the limits rows.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        protected override string GetQuery()
        {
            var query = new StringBuilder();
            query.AppendLines(
                "SELECT DISTINCT FuelRange, Min, Max",
                "FROM ", PassOffTable, "",
                "WHERE PassOffTrimmedID =",
                "( SELECT TOP 1 PassOffTrimmedID FROM ", IdentityTable, "",
                "  WHERE PlantNumber = '{0}' AND PassOffSpec =".Args(PlantNumber),
                "  ( SELECT TOP 1 PassOffSpec FROM InjectorDetails",
                "    WHERE PlantNumber = '{0}' AND LOP = '{1}'".Args(PlantNumber, LOP),
                "    ORDER BY ValidFrom DESC )",
                "  ORDER BY ValidFrom DESC )",
                "AND Pressure = '{0}'".Args(Pressure));

            string queryStr = query.ToString();
            return queryStr;
        }
    }
}
