﻿namespace SudburyDataAnalyser.Trimmed
{
    /// <summary>
    /// Limits query for trimmed.
    /// </summary>
    public class QueryLimits : LimitsBase.Query
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="QueryLimits"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="plantNumber">The plant number.</param>
        public QueryLimits(Database.DatabaseBase database, string plantNumber)
            : base(database, TrimmedIdentityTable, TrimmedPassOffTable)
        {
            PlantNumber = plantNumber;
        }
    }
}
