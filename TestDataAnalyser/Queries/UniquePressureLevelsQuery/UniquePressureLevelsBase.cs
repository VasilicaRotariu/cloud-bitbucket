﻿using SudburyDataAnalyser.Persistent;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.UniquePressureLevelsBase
{
    /// <summary>
    /// Base class for a unique pressure levels class.
    /// </summary>
    public class Query : QueryBase<Row>
    {
        public readonly string UniqueName = "UniquePressureLevelsBase";

        private static DateTime lastCheckedCache = DateTime.FromOADate(0);

        private const int CheckCacheIntervalMinutes = 15;

        /// <summary>
        /// Gets or sets the filter keys.
        /// </summary>
        private UTIDPlantNumberKeys Keys { get; set; }

        /// <summary>
        /// Gets or sets the name of the data table.
        /// </summary>
        private Queries.Parameters.DataTable TestPointDataTable { get; set; }

        /// <summary>
        /// Gets or sets the name of the data table.
        /// </summary>
        private Queries.Parameters.HeaderTable TestPointHeaderTable { get; set; }

        /// <summary>
        /// Gets or sets the data table URID column name.
        /// </summary>
        private Queries.Parameters.DataTableColumn TestPointDataTableUridColumn { get; set; }

        /// <summary>
        /// Gets or sets the header table URID column name.
        /// </summary>
        private Queries.Parameters.HeaderTableColumn TestPointHeaderTableUridColumn { get; set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="keys">List of keys used to filter the returned rows.</param>
        /// <param name="dataTable">The name of the test point data table.</param>
        /// <param name="dataColumn">The name of the test point data table URID column.</param>
        /// <param name="headerTable">The name of the test point header table.</param>
        /// <param name="headerColumn">The name of the test point header table URID column.</param>
        protected Query(
            Database.DatabaseBase database,
            UTIDPlantNumberKeys keys,
            Queries.Parameters.DataTable dataTable,
            Queries.Parameters.DataTableColumn dataColumn,
            Queries.Parameters.HeaderTable headerTable,
            Queries.Parameters.HeaderTableColumn headerColumn)
            : base(database)
        {
            Keys = keys;

            TestPointDataTable = dataTable;
            TestPointDataTableUridColumn = dataColumn;
            TestPointHeaderTable = headerTable;
            TestPointHeaderTableUridColumn = headerColumn;
        }

        /// <summary>
        /// Returns the query used to get the unique pressure levels rows.
        /// </summary>
        /// <returns>The query used to get the unique pressure levels rows.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        protected override string GetQuery()
        {
            var query = new StringBuilder();
            query.AppendLines(
                GetDeclareGlobalData(Keys, !Database.FtStructure),
                "SELECT DISTINCT tphe.PressureReq",
                "FROM ", HeaderTable, " AS he",
                GetJoinGlobalData("he"),
                "INNER JOIN [", DataHeaderTable, "] AS dahe ON (dahe.UTID = he.UTID AND dahe.PlantNumber = he.PlantNumber)",
                "INNER JOIN [", TestPointHeaderTable, "] AS tphe ON (tphe.URID = dahe.URID AND tphe.PlantNumber = dahe.PlantNumber)",
                "INNER JOIN [", TestPointDataTable, "] AS tpda ON (tpda.", TestPointDataTableUridColumn, " = tphe.", TestPointHeaderTableUridColumn, " AND tpda.PlantNumber = tphe.PlantNumber)",
                "INNER JOIN [", NameIDsTable, "] AS name ON (", EqualNames.Args("name", "tpda"), ")",
                "ORDER BY PressureReq desc");

            return query.ToString();
        }

        protected override List<Row> GetCachedResults()
        {
            List<short> fromCache = XMLCache.ReadFromXmlFile<List<short>>(this.UniqueNameAndTable());

            if (fromCache == null)
            {
                return null;
            }

            return fromCache.Select((short x) =>
            {
                Row newRow = new Row();
                newRow.Initialise(x);
                return newRow;
            }).ToList();
        }

        protected override void SetCachedResults(object objectToWrite)
        {
            List<Row> orignalData = (List<Row>)objectToWrite;
            List<short> convertedDate = orignalData.Select(x => x.PressureReq).ToList();
            XMLCache.WriteToXMLFile<List<short>>(convertedDate, this.UniqueNameAndTable());
        }

        protected override bool RequiresCacheUpdate()
        {
            DateTime now = DateTime.UtcNow;

            if (now > lastCheckedCache.AddMinutes(CheckCacheIntervalMinutes))
            {
                lastCheckedCache = now;
                return true;
            }
            else
            {
                return false;
            }
        }

        private string UniqueNameAndTable()
        {
            string hashedConecctionString = base.GetHashString(Database.ConnectionString);
            return UniqueName + hashedConecctionString;
        }
    }
}
