﻿namespace SudburyDataAnalyser.OLStabilise
{
    /// <summary>
    /// Unique pressure levels query for OLStabilise.
    /// </summary>
    public class QueryUniquePressureLevels : UniquePressureLevelsBase.Query
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="QueryUniquePressureLevels"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="keys">List of keys used to filter the returned rows.</param>
        public QueryUniquePressureLevels(Database.DatabaseBase database, UTIDPlantNumberKeys keys)
            : base(database, keys, DataStabiliseOLTable, DataStabiliseOLTableURIDColumn, HeaderStabiliseOLTable, HeaderStabiliseOLTableURIDColumn)
        {
        }
    }
}
