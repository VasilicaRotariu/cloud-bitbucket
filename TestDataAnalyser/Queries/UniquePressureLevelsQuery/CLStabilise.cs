﻿namespace SudburyDataAnalyser.CLStabilise
{
    /// <summary>
    /// Unique pressure levels query for CLStabilise.
    /// </summary>
    public class QueryUniquePressureLevels : UniquePressureLevelsBase.Query
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="QueryUniquePressureLevels"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="keys">List of keys used to filter the returned rows.</param>
        public QueryUniquePressureLevels(Database.DatabaseBase database, UTIDPlantNumberKeys keys)
            : base(database, keys, DataStabiliseCLTable, DataStabiliseCLTableURIDColumn, HeaderStabiliseCLTable, HeaderStabiliseCLTableURIDColumn)
        {
        }
    }
}
