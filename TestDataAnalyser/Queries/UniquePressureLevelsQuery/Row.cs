﻿using System;
using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.UniquePressureLevelsBase
{
    /// <summary>
    /// Rows returned from a unique pressure levels query.
    /// </summary>
    [Serializable]
    public class Row : RowBase
    {
        /// <summary>
        /// Gets or sets the pressure level column in the row.
        /// </summary>
        public short PressureReq { get; protected set; }

        /// <summary>
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a row.
        /// </summary>
        /// <param name="reader">SQLDataReader used to read row from.</param>
        public override void Initialise(SqlDataReader reader)
        {
            PressureReq = ReadShort(reader, "PressureReq");
        }

        public void Initialise(short pressureReq)
        {
            this.PressureReq = pressureReq;
        }
    }
}
