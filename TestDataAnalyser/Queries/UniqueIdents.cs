﻿using System.Diagnostics.CodeAnalysis;
using System.Text;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.UniqueIdents
{
    /// <summary>
    /// Unique identifier query class.
    /// </summary>
    public class Query : QueryBase<Row>
    {
        /// <summary>
        /// Gets or sets the filter keys.
        /// </summary>
        private UTIDPlantNumberKeys Keys { get; set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="keys">Keys used to filter the returned rows.</param>
        public Query(Database.DatabaseBase database, UTIDPlantNumberKeys keys)
            : base(database)
        {
            Keys = keys;
        }

        /// <summary>
        /// Returns the query used to get the unique identifier rows.
        /// </summary>
        /// <returns>The query used to get the unique identifier rows.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        protected override string GetQuery()
        {
            var query = new StringBuilder();
            query.AppendLines(
                GetDeclareGlobalData(Keys, !Database.FtStructure),
                //GetFilterOnKeys_V2(Keys),
                "SELECT DISTINCT he.", HeaderTableIDENTColumn, "",
                "FROM [", HeaderTable, "] AS he",
                GetFilterOnKeys_V3("he"),
                "ORDER BY ", HeaderTableIDENTColumn,
                " desc ;",
                "DROP TABLE #temp;");

            return query.ToString();
        }
    }
}
