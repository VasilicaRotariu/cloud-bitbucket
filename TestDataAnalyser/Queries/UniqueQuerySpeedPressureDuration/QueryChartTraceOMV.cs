﻿using System.Diagnostics.CodeAnalysis;
using System.Text;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.ChartTraceOMV
{
    /// <summary>
    /// Unique speed pressure durations query for chart trace OMV.
    /// </summary>
    public class QueryUniqueSpeedPressureDuration : QueryBase<UniqueSpeedPressureDuration.Row>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ChartTraceOMV.QueryUniqueSpeedPressureDuration"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="keys">The list of keys to filter the rows by.</param>
        public QueryUniqueSpeedPressureDuration(Database.DatabaseBase database, UTIDPlantNumberKeys keys)
            : base(database)
        {
            Keys = keys;
        }

        /// <summary>
        /// Gets or sets the filter keys.
        /// </summary>
        private UTIDPlantNumberKeys Keys { get; set; }

        /// <summary>
        /// Returns the query to get all the unique speed pressure durations.
        /// </summary>
        /// <returns>The query to execute.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        protected override string GetQuery()
        {
            var query = new StringBuilder();
            query.AppendLines(
                GetDeclareGlobalData(Keys, !Database.FtStructure),
                "SELECT DISTINCT SpeedPressureDuration = ('p' + CAST(hn.PressureReq as VARCHAR(4)) + '_d' + CAST(hn.Duration as VARCHAR(4)))",
                "FROM [", HeaderTable, "] AS he",
                GetJoinGlobalData("he"),
                "INNER JOIN [", DataHeaderTable, "] AS dh ON (dh.UTID = he.UTID AND dh.PlantNumber = he.PlantNumber)",
                "INNER JOIN [", HeaderOMVTable, "] AS hn ON (hn.URID = dh.URID AND hn.PlantNumber = dh.PlantNumber)",
                "INNER JOIN [", TracesTable, "] AS ct ON (ct.URID = hn.URID AND ct.PlantNumber = hn.PlantNumber)",
                "ORDER BY SpeedPressureDuration desc");
            QueryModifier.ModifyOLQueryForNonFT(query);

            return query.ToString();
        }
    }
}
