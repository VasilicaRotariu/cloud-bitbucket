﻿using System.Text;
using System.Diagnostics.CodeAnalysis;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.ChartTraceCLT
{
    /// <summary>
    /// Trace CLT query class.
    /// </summary>
    public class Query : QueryBase<Row>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ChartTraceCLT.Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="keys">The keys specifying the required rows.</param>
        public Query(Database.DatabaseBase database, UTIDPlantNumberKeys keys)
            : base(database)
        {
            Keys = keys;
        }

        /// <summary>
        /// Gets or sets the filter keys.
        /// </summary>
        private UTIDPlantNumberKeys Keys { get; set; }

        /// <summary>
        /// Returns the query to get the trace CLT rows.
        /// </summary>
        /// <returns>The query to execute.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        protected override string GetQuery()
        {
            var query = new StringBuilder();
            query.AppendLines(
                GetDeclareGlobalData(Keys, !Database.FtStructure),
                "SELECT he.LOP, dh.UTID, he.ISN, dh.PlantNumber, he.", HeaderTableIDENTColumn, ", he.Testplan, g.Glob, dh.URID, ",
                "('p' + CAST(hn.PressureReq as VARCHAR(4)) + '_fd' + CAST(hn.", FuelDemandColumn, " as VARCHAR(3))) AS SpeedPressureDemand, ",
                "ct.", TraceTableBinaryTraceDataColumn, ", he.LineNumber",
                "FROM [", HeaderTable, "] AS he",
                GetJoinGlobalData("he"),
                "INNER JOIN [", DataHeaderTable, "] AS dh ON (dh.UTID = he.UTID AND dh.PlantNumber = he.PlantNumber)",
                "INNER JOIN [", HeaderCLTable, "] AS hn ON (hn.URID = dh.URID AND hn.PlantNumber = dh.PlantNumber)",
                "INNER JOIN [", TracesTable, "] AS ct ON (ct.URID = hn.URID AND ct.PlantNumber = hn.PlantNumber)",
                "ORDER BY dh.UTID ASC, dh.URID ASC");
            QueryModifier.ModifyCLQueryForNonFT(query);

            return query.ToString();
        }
    }
}
