﻿using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.UniqueNamesBase
{
    /// <summary>
    /// Rows returned from a unique names query.
    /// </summary>
    public class Row : RowBase
    {
        /// <summary>
        /// Gets or sets the name column in the row.
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a row.
        /// </summary>
        /// <param name="reader">SQLDataReader used to read row from.</param>
        public override void Initialise(SqlDataReader reader)
        {
            Name = ReadString(reader, "Name");
        }

        public void Initialise(string name)
        {
            this.Name = name;
        }
    }
}