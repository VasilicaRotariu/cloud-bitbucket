﻿using System.Text;
using System.Diagnostics.CodeAnalysis;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.Trims
{
    /// <summary>
    /// Unique names query for trims.
    /// </summary>
    public class QueryUniqueNames : QueryBase<UniqueNamesBase.Row>
    {
        /// <summary>
        /// Gets or sets the filter keys.
        /// </summary>
        private UTIDPlantNumberKeys Keys { get; set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="QueryUniqueNames"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="keys">List of keys used to filter the returned rows.</param>
        public QueryUniqueNames(Database.DatabaseBase database, UTIDPlantNumberKeys keys)
            : base(database)
        {
            Keys = keys;
        }

        /// <summary>
        /// Returns the query used to get the unique trims name rows.
        /// </summary>
        /// <returns>The query used to get the unique trims name rows.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        protected override string GetQuery()
        {
            var query = new StringBuilder();
            query.AppendLines(
                GetDeclareGlobalData(Keys, !Database.FtStructure),
                "SELECT DISTINCT name.Name",
                "FROM ", HeaderTable, " AS he",
                GetJoinGlobalData("he"),
                "INNER JOIN [", TrimsTable, "] AS dahe ON (dahe.UTID = he.UTID AND dahe.PlantNumber = he.PlantNumber)",
                "INNER JOIN [", NameIDsTable, "] AS name ON (", EqualNames.Args("name", "dahe"), ")",
                "ORDER BY Name");

            return query.ToString();
        }
    }
}
