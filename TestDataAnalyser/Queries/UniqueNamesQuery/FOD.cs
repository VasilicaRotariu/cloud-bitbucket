﻿namespace SudburyDataAnalyser.FOD
{
    /// <summary>
    /// Unique names query for FOD.
    /// </summary>
    public class QueryUniqueNames : UniqueNamesBase.Query
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="QueryUniqueNames"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="keys">List of keys used to filter the returned rows.</param>
        public QueryUniqueNames(Database.DatabaseBase database, UTIDPlantNumberKeys keys)
            : base(database, keys, DataFODTable, DataFODTableURIDColumn, HeaderFODTable, HeaderFODTableURIDColumn)
        {
        }
    }
}
