﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics.CodeAnalysis;

using SudburyDataAnalyser.Queries.Parameters;

namespace SudburyDataAnalyser.Queries.DataTable
{
    /// <summary>
    /// The summarised data is used to get summary data as a data table.
    /// </summary>
    /// <typeparam name="T">The type of summarised data.</typeparam>
    public abstract class SummarisedData<T> where T : CompleteRow
    {
        /// <summary>
        /// Gets or sets the query parameters.
        /// </summary>
        public DataTableQueryParameters Parameters { protected get; set; }

        public System.Data.DataTable DataTable { get; private set; }

        public SummarisedData()
        {
            DataTable = new System.Data.DataTable();
        }

        public void SetDataTable()
        {
            if (DataTable != null)
            {
                DataTable.Dispose();
            }

            DataTable = new System.Data.DataTable();

            var query = GetQuery();
            if (query.IsEmpty()) return;

            // Open the query, fill the datatable
            try
            {
                var adapter = new SqlDataAdapter(query, Parameters.Database.ConnectionString)
                {
                    SelectCommand = { CommandTimeout = 600 }
                };
                adapter.Fill(DataTable);

                if (Parameters.Database.HasIasiArchiveStructure && Parameters.Database.QueryIasiArchiveStructure)
                {
                    adapter.FillLoadOption = LoadOption.Upsert;
                    adapter = new SqlDataAdapter(query, Parameters.Database.IasiArchiveDatabase.ConnectionString)
                    {
                        SelectCommand = { CommandTimeout = 600 }
                    };
                    adapter.Fill(DataTable);
                }

                if (Parameters.Database.HasSudburyArchiveStructure && Parameters.Database.QuerySudburyArchiveStructure)
                {
                    adapter.FillLoadOption = LoadOption.Upsert;
                    adapter = new SqlDataAdapter(query, Parameters.Database.SudburyArchiveDatabase.ConnectionString)
                    {
                        SelectCommand = { CommandTimeout = 600 }
                    };
                    adapter.Fill(DataTable);
                }

                DataTable = DataTable.DefaultView.ToTable( /*distinct*/ true);

                DataView dataView = DataTable.DefaultView;
                dataView.Sort = "[PDate] desc";
                DataTable = dataView.ToTable();
            }
            catch (SqlException exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        /// <summary>
        /// Returns a list of all the unique test ID rows.
        /// </summary>
        /// <returns>A list of all the unique test IDs.</returns>
        public abstract List<T> GetUniqueTestIDs();

        /// <summary>
        /// Creates complicated SQL string to return summarised data for the list of unique test IDs.
        /// </summary>
        /// <returns>SQL string to return summarised data.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        public string GetQuery()
        {
            var query = new StringBuilder();
            var uniqueTestIDs = GetUniqueTestIDs();

            if (uniqueTestIDs.Count > 0)
            {
                var keys = GetKeys(uniqueTestIDs);

                // Create columns for each "Name" in FTNamesIDs for FTResults
                query.AppendLines(
                    "DECLARE @resultsnamecols VARCHAR(MAX)",
                    "    SELECT  @resultsnamecols = STUFF(( SELECT DISTINCT TOP 100 PERCENT",
                    "                          '],[' + name.Name + '_gr'",
                    "                          FROM [", QueryParameters.ResultsTable, "] AS re",
                    "                          INNER JOIN [", QueryParameters.NameIDsTable, "] AS name ON (", QueryParameters.EqualNames.Args("name", "re"), ")",
                    "                          INNER JOIN [", QueryParameters.HeaderTable, "] AS he ON (he.UTID = re.UTID AND he.Plantnumber = re.Plantnumber)",
                    "                          " + QueryBaseSubQueries.GetFilterOnKeys("he", keys),
                    "                          ORDER BY '],[' + name.Name + '_gr'",
                    "                          FOR XML PATH('')",
                    "                          ), 1, 2, '') + ']'",
                    "    IF (select @resultsnamecols) is null",
                    "    SET @resultsnamecols = 'NoResults'");

                // Create columns for each "Name" in FTNamesIDs for FTTrims
                query.AppendLines(
                    "DECLARE @trimsnamecols VARCHAR(MAX)",
                    "    SELECT  @trimsnamecols = STUFF(( SELECT DISTINCT TOP 100 PERCENT",
                    "                          '],[' + name.Name",
                    "                          FROM [", QueryParameters.TrimsTable, "] AS tr",
                    "                          INNER JOIN [", QueryParameters.NameIDsTable, "] AS name ON (", QueryParameters.EqualNames.Args("name", "tr"), ")",
                    "                          INNER JOIN [", QueryParameters.HeaderTable, "] AS he ON (he.UTID = tr.UTID AND he.Plantnumber = tr.Plantnumber)",
                    "                          " + QueryBaseSubQueries.GetFilterOnKeys("he", keys),
                    "                          ORDER BY '],[' + name.Name",
                    "                          FOR XML PATH('')",
                    "                          ), 1, 2, '') + ']'",
                    "    IF (select @trimsnamecols) is null",
                    "    SET @trimsnamecols = 'NoTrims'");

                // Completed tests query           
                if (Parameters.Database.FtStructure)
                {
                    //jz5w78
                    // Include the OID as Testlist Test #
                    query.AppendLines(
                        "",
                        "    DECLARE @query VARCHAR(MAX)",
                        "    SET @query = 'SELECT fthe.UTID, fthe.ISN, fthe.PDate, fthe.PlantNumber, fthe.LineNumber, fthe.InjectorType, ftnt.Notes, fthe.LOP, fthe.", QueryParameters.HeaderTableIDENTColumn, ", fthe.TestPlan, fthe.TestType,",
                        "    fthe.ForcedTrim, fthe.TestPlanID, fthe.OID as [TestList Test#], fthe.TestPlanAndOffsetReference as [Test List], fthe.TrimCodeType, fttc.TrimCode, fthe.", QueryParameters.HeaderTableLuaSVNColumn, ", ftee.Reason, ftld.shortdelta AS DeltaPressure, fttr.*, ftre.*");

                }
                else
                {
                    //jz5w78
                    // Include the OID as Testlist Test #
                    query.AppendLines(
                        "",
                        "    DECLARE @query VARCHAR(MAX)",
                        "    SET @query = 'SELECT fthe.UTID, fthe.ISN, fthe.PDate, fthe.PlantNumber, fthe.LineNumber, fthe.InjectorType, ftnt.Notes, fthe.LOP, fthe.", QueryParameters.HeaderTableIDENTColumn, ", fthe.TestPlan,",
                        "    fthe.ForcedTrim, fthe.TestPlanID, fthe.OID as [TestList Test#], fthe.TestPlanAndOffsetReference as [Test List], fthe.TrimCodeType, fttc.TrimCode, fthe.", QueryParameters.HeaderTableLuaSVNColumn, ", ftee.Reason, ftld.DeltaPressure, fttr.*, ftre.*");

                }

                query.AppendLines(
                    "",
                    "    FROM (",
                    "          SELECT *",
                    "          FROM [", QueryParameters.HeaderTable, "] AS he",
                    "          " + QueryBaseSubQueries.GetFilterOnKeys("he", keys),
                    "         ) AS fthe",
                    "    LEFT JOIN [", QueryParameters.TrimCodeTable, "] AS fttc ON (fttc.UTID = fthe.UTID AND fttc.Plantnumber = fthe.Plantnumber)",
                    "    LEFT JOIN [", QueryParameters.EarlyEndTable, "] AS ftee ON (ftee.UTID = fthe.UTID AND ftee.Plantnumber = fthe.Plantnumber)",
                    "");

                // jz5w78
                // Notes 
                if (Parameters.Database.FtStructure)
                {
                    query.AppendLines("LEFT JOIN [FTNotes] AS ftnt ON (ftnt.UTID = fthe.UTID AND ftnt.Plantnumber = fthe.Plantnumber)");
                }
                else
                {
                    query.AppendLines("LEFT JOIN [Notes] AS ftnt ON (ftnt.UTID = fthe.UTID AND ftnt.Plantnumber = fthe.Plantnumber)");
                }

                // Leakdown linked by UTID on old db, URID on new, so this is required...
                // Leakdown values
                if (Parameters.Database.FtStructure)
                {
                    query.AppendLines("LEFT JOIN [", QueryParameters.LeakDownTable, "] AS ftld ON (fthe.UTID = ftld.UTID AND fthe.Plantnumber = ftld.Plantnumber)");
                }
                else
                {
                    query.AppendLines(
                        "LEFT JOIN (",
                        "              SELECT he.UTID, ld.PlantNumber, ld.DeltaPressure",
                        "              FROM [", QueryParameters.HeaderTable, "] AS he",
                        "              INNER JOIN [", QueryParameters.DataHeaderTable, "] AS dh ON (dh.UTID = he.UTID AND dh.PlantNumber = he.PlantNumber)",
                        "              INNER JOIN [", QueryParameters.LeakDownTable, "] AS ld ON (ld.URID = dh.URID AND ld.PlantNumber = he.PlantNumber)",
                        "              " + QueryBaseSubQueries.GetFilterOnKeys("he", keys),
                        "              ) AS ftld",
                        "    ON (ftld.UTID = fthe.UTID AND ftld.Plantnumber = fthe.Plantnumber)");
                }

                // Trim values
                query.AppendLines(
                    "",
                    "    LEFT JOIN (",
                    "               SELECT *",
                    "               FROM (",
                    "                     SELECT tr.PlantNumber, tr.UTID, tr.Value, name.Name",
                    "                     FROM [", QueryParameters.HeaderTable, "] AS he",
                    "                     LEFT JOIN [", QueryParameters.TrimsTable, "] AS tr ON (he.UTID = tr.UTID AND he.Plantnumber = tr.Plantnumber)",
                    "                     LEFT JOIN [", QueryParameters.NameIDsTable, "] AS name ON (", QueryParameters.EqualNames.Args("name", "tr"), ")",
                    "                     " + QueryBaseSubQueries.GetFilterOnKeys("he", keys),
                    "                    ) SourceTable",
                    "               PIVOT (",
                    "                      MAX(Value)",
                    "                      FOR Name IN (' + @trimsnamecols + ')",
                    "                     ) AS PivotTable",
                    "              ) AS fttr",
                    "    ON (fthe.UTID = fttr.UTID AND fthe.Plantnumber = fttr.Plantnumber)",
                    "    LEFT JOIN (",
                    "               SELECT *",
                    "               FROM (",
                    "                     SELECT re.PlantNumber, re.UTID, re.Grade, (name.Name + ''_gr'') AS NewName",
                    "                     FROM [", QueryParameters.HeaderTable, "] AS he",
                    "                     LEFT JOIN [", QueryParameters.ResultsTable, "] AS re ON (he.UTID = re.UTID AND he.Plantnumber = re.Plantnumber)",
                    "                     LEFT JOIN [", QueryParameters.NameIDsTable, "] AS name ON (", QueryParameters.EqualNames.Args("name", "re"), ")",
                    "                     " + QueryBaseSubQueries.GetFilterOnKeys("he", keys),
                    "                    ) SourceTable",
                    "               PIVOT (",
                    "                      MAX(Grade)",
                    "                      FOR NewName IN (' + @resultsnamecols + ')",
                    "                     ) AS PivotTable",
                    "              ) AS ftre",
                    "    ON (fthe.UTID = ftre.UTID AND fthe.Plantnumber = ftre.Plantnumber)",
                    "    ORDER BY fthe.PDate desc, fthe.LineNumber desc;'",
                    "    EXECUTE(@query)");

                // If on Non-FT db structure, run function to find and replace table and column names which have changed
                QueryModifier.ModifyOLQueryForNonFT(query);
            }

            string q = query.ToString(); // Just so i can extrct query before it runs
            return q;
        }

        /// <summary>
        /// Returns a list of keys taken from the UTID and plant number columns of the rows.
        /// </summary>
        /// <param name="rows">The rows to get the keys from.</param>
        /// <returns>A list of keys.</returns>
        private UTIDPlantNumberKeys GetKeys(IEnumerable<T> rows)
        {
            var keys = new UTIDPlantNumberKeys();

            foreach (var row in rows)
            {
                keys.Add(row.UTID, row.PlantNumber);
            }

            return keys;
        }
    }
}
