﻿using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace SudburyDataAnalyser.Queries.DataTable
{
    /// <summary>
    /// Base class for complete unique test ID queries.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class CompleteTestUTIDQuery : Query<CompleteRow>
    {
        /// <summary>
        /// Gets or set the rig type.
        /// </summary>
        private readonly string rigType;

        /// <summary>
        /// Initialises a new instance of the <see cref="CompleteTestUTIDQuery"/> class.
        /// </summary>
        /// <param name="parameters">The parameters used in the query.</param>
        /// <param name="rigType">Rig type string (i.e. calibration or pumping).</param>
        protected CompleteTestUTIDQuery(DataTableQueryParameters parameters, string rigType)
            : base(parameters)
        {
            this.rigType = rigType;
        }

        /// <summary>
        /// Returns the SQL query based on the parameters and rig type.
        /// </summary>
        /// <returns>The SQL query.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        protected override string GetQuery()
        {
            // Generate list of utids from selected parameter criteria
            var query = new StringBuilder();
            query.AppendLines(
                "SELECT he.UTID, he.PlantNumber, he.ISN",
                "FROM [", HeaderTable, "] AS he",
                GenerateSqlStringForRigType(rigType),
                "INNER JOIN [", ResultsTable, "] AS re ON (re.UTID = he.UTID AND re.PlantNumber = he.PlantNumber)",
                "INNER JOIN [", NameIDsTable, "] AS name ON (", EqualNames.Args("name", "re"), ")",
                "WHERE he.PDate > {0} AND he.PDate < {1}".Args(Parameters.DateFrom, Parameters.DateTo),
                "AND (name.Name = 'Global' AND re.Grade NOT IN ({0}))".Args(GetIncompleteCodesList()),
                GenerateSqlStringFromList("LOP", Parameters.LOPs),
                GenerateSqlStringFromList("Testplan", Parameters.TestPlans),
                GenerateSqlStringFromList("TestType", Parameters.TestTypes),  // Note, will not exist on new db structure
                GenerateSqlStringForTestType(rigType),   // For old db, this will ensure the data goes in the correct datagrid
                GenerateSqlStringFromList("PlantNumber", Parameters.PlantNumbers),
                GenerateSqlStringFromList("LineNumber", Parameters.LineNumbers),
                GenerateSqlStringFromList("ISN", Parameters.ISNs),
                GenerateSqlStringForIsNumeric("ISN", Parameters.NumericIsns),
                GenerateSqlStringFromList("UTID", Parameters.UTIDs),
                GenerateSqlStringFromUtidPlantNumberList("UTID", "PlantNumber", Parameters.Keys),
                GenerateSqlStringForTestNumber(Parameters.TestNumber, rigType),
                GenerateSqlStringForResult(Parameters.Result));

            QueryModifier.ModifyOLQueryForNonFT(query);
            string q = query.ToString();
            return q;
        }

        /// <summary>
        /// Returns a csv string of valid incomplete test codes
        /// </summary>
        /// <returns>Valid incomplete test codes for pumping Rigs</returns>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "csv = Comma seperated Values.")]
        protected virtual string GetIncompleteCodesList()
        {
            return IncompleteGradeValue;
        }
    }
}
