﻿using System;
using System.Data.SqlClient;

namespace SudburyDataAnalyser.Queries.DataTable
{
    /// <summary>
    /// Class used to represent a incomplete unique test ID row.
    /// </summary>
    public class IncompleteRow : CompleteRow
    {
        /// <summary>Gets the PDate.</summary>
        public DateTime PDate { get; private set; }

        /// <summary>Gets the ISN.</summary>
        // ReSharper disable once InconsistentNaming
        public string ISN { get; private set; }

        /// <summary>Gets the early end reason.</summary>
        public string Reason { get; private set; }

        /// <summary>
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        public override void Initialise(SqlDataReader reader)
        {
            UTID = ReadInt(reader, "UTID");
            PlantNumber = ReadShort(reader, "PlantNumber");

            PDate = ReadDateTime(reader, "PDate");
            ISN = ReadString(reader, "ISN");
            Reason = ReadString(reader, "Reason");
        }
    }
}
