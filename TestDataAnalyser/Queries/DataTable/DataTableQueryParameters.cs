﻿using System.Collections.Generic;

namespace SudburyDataAnalyser.Queries.DataTable
{
    /// <summary>
    /// Class used to store the parameters used in a data table query.
    /// </summary>
    public class DataTableQueryParameters
    {
        /// <summary>
        /// Gets the start date used in the query.
        /// </summary>
        public string DateFrom { get; private set; }

        /// <summary>
        /// Gets the end date used in the query.
        /// </summary>
        public string DateTo { get; private set; }

        /// <summary>
        /// Gets the list of LOPs used in the query.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public List<string> LOPs { get; private set; }

        /// <summary>
        /// Gets the list of test plans used in the query.
        /// </summary>
        public List<string> TestPlans { get; private set; }

        /// <summary>
        /// Gets the list of test types used in the query.
        /// </summary>
        public List<string> TestTypes { get; private set; }

        /// <summary>
        /// Gets the list of plant numbers used in the query.
        /// </summary>
        public List<string> PlantNumbers { get; private set; }

        /// <summary>
        /// Gets the list of line numbers used in the query.
        /// </summary>
        public List<string> LineNumbers { get; private set; }

        /// <summary>
        /// Gets or sets the list of ISNs used in the query.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public List<string> ISNs { get; set; }

        /// <summary>
        /// Gets a value indicating whether the ISNs must be numeric.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public bool NumericIsns { get; private set; }

        /// <summary>
        /// Gets the list of UITDs used in the query.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public List<string> UTIDs { get; private set; }

        /// <summary>
        /// Gets the list of keys used in the query.
        /// </summary>
        public UTIDPlantNumberKeys Keys { get; private set; }

        /// <summary>
        /// Gets the test number used in the query.
        /// </summary>
        public string TestNumber { get; private set; }

        /// <summary>
        /// Gets the result used in the query.
        /// </summary>
        public string Result { get; private set; }

        /// <summary>
        /// Gets the database.
        /// </summary>
        public Database.DatabaseBase Database { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="DataTableQueryParameters"/> class.
        /// </summary>
        /// <param name="dateFrom">The start date used in the query.</param>
        /// <param name="dateTo">The end date used in the query.</param>
        /// <param name="lops">The list of LOPs used in the query.</param>
        /// <param name="testPlans">The list of test plans used in the query.</param>
        /// <param name="testTypes">The list of test types used in the query.</param>
        /// <param name="plantNumbers">The list of plant numbers used in the query.</param>
        /// <param name="lineNumbers">The list of line numbers used in the query.</param>
        /// <param name="isns">The list of ISNs used in the query.</param>
        /// <param name="numericIsns">Value indicating whether the ISNs must be numeric</param>
        /// <param name="utids">The list of UTIDs used in the query.</param>
        /// <param name="keys">The keys used in the query.</param>
        /// <param name="testNumber">The test number used in the query.</param>
        /// <param name="result">The result used in the query.</param>
        /// <param name="database">The database.</param>
        public DataTableQueryParameters(
            string dateFrom, 
            string dateTo, 
            List<string> lops, 
            List<string> testPlans, 
            List<string> testTypes,
            List<string> plantNumbers, 
            List<string> lineNumbers, 
            List<string> isns,
            bool numericIsns,
            List<string> utids,
            UTIDPlantNumberKeys keys,
            string testNumber, 
            string result, 
            Database.DatabaseBase database)
        {
            DateFrom = dateFrom;
            DateTo = dateTo;
            LOPs = lops;
            TestPlans = testPlans;
            TestTypes = testTypes;
            if (keys.Count == 0)
            {
                PlantNumbers = plantNumbers;
            }
            else
            {
                PlantNumbers = new List<string>();
            }
            LineNumbers = lineNumbers;
            ISNs = isns;
            NumericIsns = numericIsns;
            UTIDs = utids;
            Keys = keys;
            TestNumber = testNumber;
            Result = result;
            Database = database;
        }
    }
}
