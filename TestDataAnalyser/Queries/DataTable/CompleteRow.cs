﻿using System.Data.SqlClient;

namespace SudburyDataAnalyser.Queries.DataTable
{
    /// <summary>
    /// Class used to represent a complete unique test ID row.
    /// </summary>
    public class CompleteRow : RowBase
    {
        /// <summary>Gets or sets the UTID.</summary>
        // ReSharper disable once InconsistentNaming
        public int UTID { get; protected set; }

        /// <summary>Gets or sets the PlantNumber.</summary>
        public short PlantNumber { get; protected set; }

        /// <summary>Gets or sets the Injector Serial Number</summary>
        public string ISN { get; set; }

        /// <summary>
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        public override void Initialise(SqlDataReader reader)
        {
            UTID = ReadInt(reader, "UTID");
            PlantNumber = ReadShort(reader, "PlantNumber");
            ISN = ReadString(reader, "ISN");
        }
    }
}
