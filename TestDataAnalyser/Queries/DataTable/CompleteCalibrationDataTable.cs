﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace SudburyDataAnalyser.Queries.DataTable
{
    /// <summary>
    /// The complete calibration data table.
    /// </summary>
    public class CompleteCalibrationDataTable : SummarisedData<CompleteRow>
    {
        /// <summary>
        /// Returns a list of all the unique test ID rows.
        /// </summary>
        /// <returns>A list of all the unique test IDs.</returns>
        public override List<CompleteRow> GetUniqueTestIDs()
        {
            return new CompleteCalibrationTestUTIDQuery(Parameters).Execute();
        }

        /// <summary>
        /// Returns a collection of all the fail rows.
        /// </summary>
        public EnumerableRowCollection<DataRow> FailRows()
        {
            return from x in this.DataTable.AsEnumerable()
                   where (x.Field<float>("Global_gr") > 0)
                   select x;
        }

        /// <summary>
        /// Returns a count of all the fail rows.
        /// </summary>
        public int FailCount()
        {
            return (from x in this.DataTable.AsEnumerable()
                    where (x.Field<float>("Global_gr") > 0)
                    select x).Count();
        }

        /// <summary>
        /// Returns a count of all the production fail rows.
        /// </summary>
        /// <returns>A count of all the production fail rows.</returns>
        public int ProductionFailCount()
        {
            return (from x in this.DataTable.AsEnumerable()
                    where ((x.Field<float>("Global_gr") > 0) && (x.Field<string>("TestPlan") == "Production"))
                    select x).Count();
        }

        /// <summary>
        /// Returns a count of all the non production fail rows.
        /// </summary>
        /// <returns>A count of all the non production fail rows.</returns>
        public int NonProductionFailCount()
        {
            return (from x in this.DataTable.AsEnumerable()
                    where ((x.Field<float>("Global_gr") > 0) && (x.Field<string>("TestPlan") != "Production"))
                    select x).Count();
        }

        /// <summary>
        /// Returns all the fail rows between the start and end date
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        public EnumerableRowCollection<UTIDPlantNumberKey> Failures(DateTime startDate, DateTime endDate)
        {
            return from x in this.DataTable.AsEnumerable()
                   where (x.Field<float>("Global_gr") > 0 &&
                          x.Field<DateTime>("PDate") >= startDate &&
                          x.Field<DateTime>("PDate") < endDate)
                   select new UTIDPlantNumberKey(x.Field<int>("UTID"), x.Field<short>("PlantNumber"));
        }

        /// <summary>
        /// Returns all the fail rows between the start and end date
        /// for the specified rig.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="plantNumber">The plant number.</param>
        public EnumerableRowCollection<UTIDPlantNumberKey> Failures(DateTime startDate, DateTime endDate, short plantNumber)
        {
            return from x in this.DataTable.AsEnumerable()
                   where (x.Field<float>("Global_gr") > 0 &&
                          x.Field<short>("PlantNumber") == plantNumber &&
                          x.Field<DateTime>("PDate") >= startDate &&
                          x.Field<DateTime>("PDate") < endDate)
                   select new UTIDPlantNumberKey(x.Field<int>("UTID"), x.Field<short>("PlantNumber"));
        }

        /// <summary>
        /// Returns a count of all the pass rows between the start and end date.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        public int PassCount(DateTime startDate, DateTime endDate)
        {
            return (from x in this.DataTable.AsEnumerable()
                    where ((x.Field<float>("Global_gr").Approx(0) || x.Field<float>("Global_gr").Approx(-2) || x.Field<float>("Global_gr").Approx(-3) || x.Field<float>("Global_gr").Approx(-4)) &&
                           (x.Field<DateTime>("PDate") >= startDate) &&
                           (x.Field<DateTime>("PDate") < endDate))
                    select x).Count();
        }

        /// <summary>
        /// Returns a count of all the pass rows between the start and end date
        /// for the specified rig.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="plantNumber">The plant number.</param>
        public int PassCount(DateTime startDate, DateTime endDate, short plantNumber)
        {
            return (from x in this.DataTable.AsEnumerable()
                    where ((x.Field<float>("Global_gr").Approx(0) || x.Field<float>("Global_gr").Approx(-2) || x.Field<float>("Global_gr").Approx(-3) || x.Field<float>("Global_gr").Approx(-4)) &&
                           (x.Field<short>("PlantNumber") == plantNumber) &&
                           (x.Field<DateTime>("PDate") >= startDate) &&
                           (x.Field<DateTime>("PDate") < endDate))
                    select x).Count();
        }

        /// <summary>
        /// Returns a count of all the production pass rows.
        /// </summary>
        /// <returns>A count of all the production pass rows.</returns>
        public int ProductionPassCount()
        {
            return (from x in this.DataTable.AsEnumerable()
                    where (x.Field<float>("Global_gr").Approx(0) || x.Field<float>("Global_gr").Approx(-2))
                    select x).Count();
        }

        /// <summary>
        /// Returns a count of all the non production pass rows.
        /// </summary>
        /// <returns>A count of all the non production pass rows.</returns>
        public int NonProductionPassCount()
        {
            return (from x in this.DataTable.AsEnumerable()
                    where (x.Field<float>("Global_gr").Approx(-3) || x.Field<float>("Global_gr").Approx(-4))
                    select x).Count();
        }

        /// <summary>
        /// Returns a count of all the plant numbers.
        /// </summary>
        /// <returns>The count of all the plant numbers</returns>
        public List<short> PlantNumbers()
        {
            return (from x in this.DataTable.AsEnumerable() group x by x.Field<short>("PlantNumber")).Keys();
        }

        /// <summary>
        /// Class used represent a plant number and line number.
        /// </summary>
        public class PlantLineNumbers : IEquatable<PlantLineNumbers>
        {
            /// <summary>
            /// Gets or sets the plant number.
            /// </summary>
            public string PlantNumber { get; set; }

            /// <summary>
            /// Gets or sets the line number.
            /// </summary>
            public string LineNumber { get; set; }

            /// <summary>
            /// Returns true if the object equals this object.
            /// </summary>
            /// <param name="obj">The object to compare.</param>
            /// <returns>true if the object equals this object; false otherwise.</returns>
            public override bool Equals(object obj)
            {
                return obj != null && Equals(obj as PlantLineNumbers);
            }

            /// <summary>
            /// Returns the objects hash code.
            /// </summary>
            /// <returns>The objects hash code.</returns>
            public override int GetHashCode()
            {
                return PlantNumber.GetHashCode() ^ LineNumber.GetHashCode();
            }

            /// <summary>
            /// Returns true if the object equals this object.
            /// </summary>
            /// <param name="other">The object to compare.</param>
            /// <returns>true if the object equals this object; false otherwise.</returns>
            public bool Equals(PlantLineNumbers other)
            {
                return (other.PlantNumber == PlantNumber) && (other.LineNumber == LineNumber);
            }
        }

        /// <summary>
        /// Returns a collection of all the distinct plant number and line number from the rows.
        /// </summary>
        /// <returns>A collection of all the distinct plant number and line numbers.</returns>
        public IEnumerable<PlantLineNumbers> DistinctPlantLineNumberRows()
        {
            return (from x in this.DataTable.AsEnumerable()
                    select new PlantLineNumbers
                    {
                        PlantNumber = x["PlantNumber"].ToString(),
                        LineNumber = x["LineNumber"].ToString()
                    }).Distinct();
        }
    }
}
