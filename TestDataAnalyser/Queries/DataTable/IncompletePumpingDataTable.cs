﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace SudburyDataAnalyser.Queries.DataTable
{
    /// <summary>
    /// The incomplete pumping data table.
    /// </summary>
    public class IncompletePumpingDataTable : SummarisedData<IncompleteRow>
    {
        /// <summary>
        /// Returns a list of all the unique test ID rows.
        /// </summary>
        /// <returns>A list of all the unique test IDs.</returns>
        public override List<IncompleteRow> GetUniqueTestIDs()
        {
            var query = new IncompletePumpingTestUTIDQuery(Parameters);
            return query.Execute();
        }

        /// <summary>
        /// Returns all the rows between the start and end date.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        public EnumerableRowCollection<UTIDPlantNumberKey> All(DateTime startDate, DateTime endDate)
        {
            return from x in this.DataTable.AsEnumerable()
                   where (x.Field<DateTime>("PDate") >= startDate &&
                          x.Field<DateTime>("PDate") < endDate)
                   select new UTIDPlantNumberKey(x.Field<int>("UTID"), x.Field<short>("PlantNumber"));
        }

        /// <summary>
        /// Returns all the rows between the start and end date
        /// for the specified rig.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="plantNumber">The plant number.</param>
        public EnumerableRowCollection<UTIDPlantNumberKey> All(DateTime startDate, DateTime endDate, short plantNumber)
        {
            return from x in this.DataTable.AsEnumerable()
                   where (x.Field<short>("PlantNumber") == plantNumber &&
                          x.Field<DateTime>("PDate") >= startDate &&
                          x.Field<DateTime>("PDate") < endDate)
                    select new UTIDPlantNumberKey(x.Field<int>("UTID"), x.Field<short>("PlantNumber"));
        }

        /// <summary>
        /// Returns a count of all the rows between the start and end date.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        public int Count(DateTime startDate, DateTime endDate)
        {
            return (from x in this.DataTable.AsEnumerable()
                    where ((x.Field<DateTime>("PDate") >= startDate.Date) && (x.Field<DateTime>("PDate") <= endDate.Date))
                    select x).Count();
        }

        /// <summary>
        /// Returns a count of all the rows between the start and end date
        /// for the specified rig.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="plantNumber">The plant number.</param>
        public int Count(DateTime startDate, DateTime endDate, short plantNumber)
        {
            return (from x in this.DataTable.AsEnumerable()
                    where ((x.Field<short>("PlantNumber") == plantNumber) && (x.Field<DateTime>("PDate") >= startDate.Date) && (x.Field<DateTime>("PDate") <= endDate.Date))
                    select x).Count();
        }
    }
}
