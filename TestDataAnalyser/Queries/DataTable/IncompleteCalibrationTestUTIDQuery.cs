﻿namespace SudburyDataAnalyser.Queries.DataTable
{
    /// <summary>
    /// Incomplete calibration unique test ID query.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class IncompleteCalibrationTestUTIDQuery : IncompleteTestUTIDQuery
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="IncompleteCalibrationTestUTIDQuery"/> class.
        /// </summary>
        /// <param name="parameters">The parameters used in the query.</param>
        public IncompleteCalibrationTestUTIDQuery(DataTableQueryParameters parameters)
            : base(parameters, "C")
        {
        }
    }
}
