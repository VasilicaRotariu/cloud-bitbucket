﻿using System.Diagnostics.CodeAnalysis;

namespace SudburyDataAnalyser.Queries.DataTable
{
    /// <summary>
    /// Incomplete pumping unique test ID query.
    /// </summary>
// ReSharper disable once InconsistentNaming
    public class IncompletePumpingTestUTIDQuery : IncompleteTestUTIDQuery
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="IncompletePumpingTestUTIDQuery"/> class.
        /// </summary>
        /// <param name="parameters">The parameters used in the query.</param>
        public IncompletePumpingTestUTIDQuery(DataTableQueryParameters parameters)
            : base(parameters, "P")
        {
        }

        /// <summary>
        /// Returns a csv string of valid incomplete test codes
        /// </summary>
        /// <returns>Valid incomplete test codes for pumping Rigs</returns>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "csv = Comma seperated Values.")]
        protected override string GetIncompleteCodesList()
        {
            return "-5, -6, -7";
        }
    }
}
