﻿namespace SudburyDataAnalyser.Queries.DataTable
{
    /// <summary>
    /// Complete calibration unique test ID query.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class CompleteCalibrationTestUTIDQuery : CompleteTestUTIDQuery
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="CompleteCalibrationTestUTIDQuery"/> class.
        /// </summary>
        /// <param name="parameters">The parameters used in the query.</param>
        public CompleteCalibrationTestUTIDQuery(DataTableQueryParameters parameters)
            : base(parameters, "C")
        {
        }
    }
}
