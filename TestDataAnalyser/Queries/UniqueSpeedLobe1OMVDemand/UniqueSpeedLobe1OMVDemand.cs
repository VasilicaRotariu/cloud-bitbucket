﻿using System.Diagnostics.CodeAnalysis;
using System.Text;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.UniqueSpeedLobe1OMVDemandBase
{
    /// <summary>
    /// Base class for a unique speed lobe1OMV demand classes.
    /// </summary>
    public class Query : QueryBase<UniqueSpeedLobe1OMVDemand.Row>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="UniqueSpeedLobe1OMVDemandBase.Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="keys">List of keys used to filter the returned rows.</param>
        /// <param name="table">The name of the test point header table.</param>
        /// <param name="column">The name of the test point header table URID column.</param>
        public Query(Database.DatabaseBase database, UTIDPlantNumberKeys keys, Queries.Parameters.HeaderTable table, Queries.Parameters.HeaderTableColumn column)
            : base(database)
        {
            Keys = keys;
            TestPointHeaderTable = table;
            TestPointHeaderTableUridColumn = column;
        }

        /// <summary>
        /// Gets or sets the filter keys.
        /// </summary>
        private UTIDPlantNumberKeys Keys { get; set; }

        /// <summary>
        /// Gets or sets the header table name.
        /// </summary>
        private Queries.Parameters.HeaderTable TestPointHeaderTable { get; set; }

        /// <summary>
        /// Gets or sets the header table URID column name.
        /// </summary>
        private Queries.Parameters.HeaderTableColumn TestPointHeaderTableUridColumn { get; set; }

        /// <summary>
        /// Returns the query to get all the unique speed lobe1OMV demands.
        /// </summary>
        /// <returns>The query to execute.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        protected override string GetQuery()
        {
            var query = new StringBuilder();
            query.AppendLines(
                GetDeclareGlobalData(Keys, !Database.FtStructure),
                "SELECT DISTINCT ('s' + CAST(hn.SpeedReq as VARCHAR(4)) + '_p' + CAST(hn.PressureReq as VARCHAR(5))) AS SpeedLobe1OMVDemand",
                "FROM [", HeaderTable, "] AS he",
                GetJoinGlobalData("he"),
                "INNER JOIN [", DataHeaderTable, "] AS dh ON (dh.UTID = he.UTID AND dh.PlantNumber = he.PlantNumber)",
                "INNER JOIN [", TestPointHeaderTable, "] AS hn ON (hn.URID = dh.URID AND hn.PlantNumber = dh.PlantNumber)",
                "INNER JOIN [", TracesTable, "] AS ct ON (ct.URID = hn.URID AND ct.PlantNumber = hn.PlantNumber)",
                "ORDER BY SpeedLobe1OMVDemand DESC");
            QueryModifier.ModifyOLQueryForNonFT(query);

            return query.ToString();
        }
    }
}