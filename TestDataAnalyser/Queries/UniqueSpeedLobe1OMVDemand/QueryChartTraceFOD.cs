﻿namespace SudburyDataAnalyser.ChartTraceFOD
{
    /// <summary>
    /// Unique speed lobe1OMV demand query for chart trace FOD.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class QueryUniqueLobe1OMVPressureDemand : UniqueSpeedLobe1OMVDemandBase.Query
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ChartTraceFOD.QueryUniqueLobe1OMVPressureDemand"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="keys">List of keys used to filter the returned rows.</param>
        public QueryUniqueLobe1OMVPressureDemand(Database.DatabaseBase database, UTIDPlantNumberKeys keys)
            : base(database, keys, HeaderFODTable, HeaderFODTableURIDColumn)
        {
        }
    }
}
