﻿using System.Text;
using System.Diagnostics.CodeAnalysis;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.Trend
{
    /// <summary>
    /// Default query for Trend.
    /// See Trend.<see cref="Row"/> for the rows returned from Execute.
    /// </summary>
    public class Query : QueryBase<Row>
    {
        /// <summary>
        /// Gets or sets the UTID's used in the SQL query.
        /// </summary>
        private string Utid { get; set; }

        /// <summary>
        /// Gets or sets the plant number used in the SQL query.
        /// </summary>
        private string PlantNumber { get; set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="Query"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="utid">The UTID used to filter the returned rows.</param>
        /// <param name="plantNumber">The plant number used to filter the returned rows.</param>
        public Query(Database.DatabaseBase database, string utid, string plantNumber)
            : base(database)
        {
            Utid = utid;
            PlantNumber = plantNumber;
        }

        /// <summary>
        /// Returns the query used to get the rows.
        /// </summary>
        /// <returns>The query used to get the rows.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        protected override string GetQuery()
        {
            var query = new StringBuilder();
            query.AppendLines(
                "SELECT tr.PlantNumber, tr.UTID, tr.Trace",
                "FROM [", TrendChartTable, "] as tr",
                "WHERE tr.UTID = {0}".Args(Utid),
                "AND tr.PlantNumber = {0}".Args(PlantNumber));

            return query.ToString();
        }
    }
}
