﻿using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace SudburyDataAnalyser.Queries.XYQueries
{
    /// <summary>
    /// Class used to return the alignment Y query.
    /// </summary>
    public class AlignmentYQuery
    {
        /// <summary>
        /// Returns the alignment Y query.
        /// </summary>
        /// <param name="keys">The keys to filter the rows by.</param>
        /// <param name="plantNumber">The plant number to filter the rows by.</param>
        /// <param name="line">The line number to filter the rows by.</param>
        /// <param name="yQuery">The Y data query embedded in the query.</param>
        /// <returns>The alignment Y query.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation",
                         Justification = "Variable yQuery is a query for the Y axis")]
        public string GetQuery(UTIDPlantNumberKeys keys, string plantNumber, string line, IXYQuery yQuery)
        {
            var query = new StringBuilder();

            query.AppendLines(
                "DECLARE @ydata Table (UTID int, PlantNumber smallint, LineNumber tinyint, Y real)",
                 QueryBaseSubQueries.GetFilterOnKeys_V2(keys),
                //// Get y data
                "INSERT @ydata",
                yQuery.GetQuery(keys, "Y"),
                "AND he.PlantNumber = {0}".Args(plantNumber),
                "AND he.LineNumber = {0}".Args(line),
                //// Select x and y data, joined by UTID
                "SELECT Y FROM @ydata AS ydata"
                ,"DROP TABLE #temp;");

            return query.ToString();
        }
    }
}
