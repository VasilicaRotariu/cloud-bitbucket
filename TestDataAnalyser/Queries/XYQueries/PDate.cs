﻿using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace SudburyDataAnalyser.Queries.XYQueries
{
    /// <summary>
    /// PDate XY query class. 
    /// </summary>
    public class PDate : QueryBaseSubQueries, IXYQuery
    {
        /// <summary>
        /// Returns the query used to retrieve median values from the test point table.
        /// </summary>
        /// <param name="keys">List of keys used to filter the returned rows.</param>
        /// <param name="alias">X or Y value alias name.</param>
        /// <returns>The query used to retrieve the rows.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        public string GetQuery(UTIDPlantNumberKeys keys, string alias)
        {
            var query = new StringBuilder();
            query.AppendLines(
                //GetFilterOnKeys_V2(keys),
                "SELECT he.UTID, he.PlantNumber, he.LineNumber, he.PDate AS {0}".Args(alias),
                "FROM [", HeaderTable, "] AS he",
                GetFilterOnKeys_V3("he")
               // , " DROP TABLE #temp;"
                );

            return query.ToString();
        }
    }
}
