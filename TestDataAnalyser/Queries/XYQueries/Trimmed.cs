﻿namespace SudburyDataAnalyser.Queries.XYQueries
{
    /// <summary>
    /// Trimmed XY query class. 
    /// </summary>
    public class Trimmed : XYPressureReqQueryBase
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="SudburyDataAnalyser.Queries.XYQueries.Trimmed"/> class.
        /// Returns the query used to retrieve rows from the trimmed tables.
        /// </summary>
        /// <param name="name">The name to filter the rows by.</param>
        /// <param name="duration">The duration to filter the rows by.</param>
        /// <param name="pressureReq">The pressure request to filter the rows by.</param>
        public Trimmed(string name, string duration, string pressureReq)
            : base(
            HeaderTrimmedTable, 
            HeaderTrimmedTableURIDColumn, 
            DataTrimmedTable, 
            DataTrimmedTableURIDColumn, 
            HeaderTrimmedTableMedianColumn,
            HeaderTrimmedTableDurationColumn, 
            name, 
            duration, 
            pressureReq)
        {
        }
    }
}
