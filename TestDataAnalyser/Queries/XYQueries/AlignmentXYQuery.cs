﻿using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace SudburyDataAnalyser.Queries.XYQueries
{
    /// <summary>
    /// Class used to return the alignment XY query.
    /// </summary>
// ReSharper disable once InconsistentNaming
    public class AlignmentXYQuery
    {
        /// <summary>
        /// Returns the alignment XY query.
        /// </summary>
        /// <param name="topNodeName">Top parent node of the tree.</param>
        /// <param name="keys">The keys to filter the rows by.</param>
        /// <param name="plantNumber">The plant number to filter the rows by.</param>
        /// <param name="line">The line number to filter the rows by.</param>
        /// <param name="xQuery">The X data query embedded in the query.</param>
        /// <param name="yQuery">The Y data query embedded in the query.</param>
        /// <returns>The alignment XY query.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation",
                         Justification = "Variables xQuery and yQuery are queries for the X and Y axis")]
        public string GetQuery(string topNodeName, UTIDPlantNumberKeys keys, string plantNumber, string line, IXYQuery xQuery, IXYQuery yQuery)
        {
            var query = new StringBuilder();

            query.Append("DECLARE @xdata Table (UTID int, PlantNumber smallint, LineNumber tinyint, X ");
            switch (topNodeName)
            {
                case "PDate":
                    query.AppendLine("DateTime)");
                    break;
                case "Chronological":
                    query.AppendLine("int)");
                    break;
                default:
                    query.AppendLine("real)");
                    break;
            }

            query.AppendLines(
                "DECLARE @ydata Table (UTID int, PlantNumber smallint, LineNumber tinyint, Y real)",
                //added V2
                 QueryBaseSubQueries.GetFilterOnKeys_V2(keys),
                //// Get x data
                   "INSERT @xdata",
                   xQuery.GetQuery(keys, "X"),
                   "AND he.PlantNumber = {0}".Args(plantNumber),
                   "AND he.LineNumber = {0}".Args(line),
                //// Get y data
                   "INSERT @ydata",
                   yQuery.GetQuery(keys, "Y"),
                   "AND he.PlantNumber = {0}".Args(plantNumber),
                   "AND he.LineNumber = {0}".Args(line),
                //// Select x and y data, joined by UTID
                   "SELECT X, Y FROM @xdata AS xdata",
                   "INNER JOIN @ydata As ydata ON (ydata.UTID = xdata.UTID AND ydata.PlantNumber = xdata.PlantNumber)"
                   , " DROP TABLE #temp;");

            string q = query.ToString(); // exposed so i can extract
            return query.ToString();
        }
    }
}
