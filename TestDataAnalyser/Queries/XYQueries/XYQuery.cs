﻿using System.Windows.Forms;

namespace SudburyDataAnalyser.Queries.XYQueries
{
    /// <summary>
    /// Utility class used to get the alignment, trend or XY sub query.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class XYQuery
    {
        /// <summary>
        /// Generates and returns SQL query to return data for selected test point/parameter (TreeNode).
        /// </summary>
        /// <param name="currentNode">Selected tree node.</param>
        /// <param name="topNode">The top node of the selected tree node.</param>
        /// <param name="xy">String that is either "X" or "Y", depending on which axis the data is to be used for.</param>
        /// <returns>SQL Query to return data for selected TreeNode.</returns>
        // ReSharper disable once InconsistentNaming
        public static IXYQuery GetAlignmentOrTrendOrXYQuery(TreeNode currentNode, TreeNode topNode, string xy)
        {
            string name = "", duration = "", pressureReq = "";

            try
            {
                name = currentNode.Text;
                duration = currentNode.Parent.Text;
                pressureReq = currentNode.Parent.Parent.Text;
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch
            {
            }

            switch (topNode.Text)
            {
                case "OpenLoop":
                    return new NCV(name, duration, pressureReq);

                case "Trimmed":
                    return new Trimmed(name, duration, pressureReq);

                case "ClosedLoop":
                    return new CLT(name, duration, pressureReq);

                case "Trims":
                    return new Trims(name);

                case "Grades":
                    return new Grades(name);

                case "PDate":
                    return new PDate();

                case "Chronological":
                    // Need to come up with a way of creating a series from 1 to "number of records" in date order to plot against
                    // OR if no x-points set against y points, will it just do it against index?
                    return new EmptyQuery();
            }

            return new EmptyQuery();
        }
    }
}
