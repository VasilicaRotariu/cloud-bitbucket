﻿namespace SudburyDataAnalyser.Queries.XYQueries
{
    /// <summary>
    /// Grades XY query class.
    /// </summary>
    public class Grades : XYNameQueryBase
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="SudburyDataAnalyser.Queries.XYQueries.Grades"/> class.
        /// Returns the query used to retrieve rows from the results table.
        /// </summary>
        /// <param name="name">The name to filter the rows by.</param>
        public Grades(string name)
            : base(ResultsTable, GradesTableValueColumn, name)
        {
        }
    }
}
