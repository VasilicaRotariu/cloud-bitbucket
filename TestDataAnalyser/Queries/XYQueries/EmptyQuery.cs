﻿namespace SudburyDataAnalyser.Queries.XYQueries
{
    /// <summary>
    /// Empty XY query.
    /// </summary>
    public class EmptyQuery : IXYQuery
    {
        /// <summary>
        /// Returns an empty query.
        /// </summary>
        /// <param name="keys">List of keys used to filter the returned rows.</param>
        /// <param name="alias">X or Y value alias name.</param>
        /// <returns>An empty query.</returns>
        public string GetQuery(UTIDPlantNumberKeys keys, string alias)
        {
            return string.Empty;
        }
    }
}
