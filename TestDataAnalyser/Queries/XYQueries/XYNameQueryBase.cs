﻿using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace SudburyDataAnalyser.Queries.XYQueries
{
    /// <summary>
    /// Base class for all XY name queries. Used to retrieve rows including a median
    /// value from a test point table. 
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class XYNameQueryBase : QueryBaseSubQueries, IXYQuery
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="XYNameQueryBase"/> class.
        /// </summary>
        /// <param name="table">The test point table to retrieve the values from.</param>
        /// <param name="resultColumn">The name of the results column.</param>
        /// <param name="name">The name to filter the rows by.</param>
        public XYNameQueryBase(Parameters.QueryTable table, Parameters.QueryColumn resultColumn, string name)
        {
            TestPointTable = table;
            ResultColumn = resultColumn;
            Name = name;
        }

        /// <summary>
        /// Returns the query used to retrieve median values from the test point table.
        /// </summary>
        /// <param name="keys">List of keys used to filter the returned rows.</param>
        /// <param name="alias">X or Y value alias name.</param>
        /// <returns>The query used to retrieve the rows.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        public string GetQuery(UTIDPlantNumberKeys keys, string alias)
        {
            var query = new StringBuilder();
            query.AppendLines(
              //  GetFilterOnKeys_V2(keys),
                "SELECT he.UTID, he.PlantNumber, he.LineNumber, tpt.", ResultColumn, " AS {0}".Args(alias),
                "FROM [", HeaderTable, "] AS he",
                "INNER JOIN [", TestPointTable, "] AS tpt ON (tpt.UTID = he.UTID AND tpt.PlantNumber = he.PlantNumber)",
                "INNER JOIN [", NameIDsTable, "] AS name ON (name.NameID = tpt.NameID)",
                GetFilterOnKeys_V3("he"),
                "AND name.Name = '{0}'".Args(Name)
              //  , " DROP TABLE #temp;"
                );

            return query.ToString();
        }

        /// <summary>
        /// Gets or sets the name of the table to return median values from.
        /// </summary>
        private Parameters.QueryTable TestPointTable { get; set; }

        /// <summary>
        /// Gets or sets the name of the results column.
        /// </summary>
        private Parameters.QueryColumn ResultColumn { get; set; }

        /// <summary>
        /// Gets or sets the name to filter the rows by.
        /// </summary>
        private string Name { get; set; }
    }
}
