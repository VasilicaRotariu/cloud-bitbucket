﻿namespace SudburyDataAnalyser.Queries.XYQueries
{
    /// <summary>
    /// Interface for all XYQueries.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public interface IXYQuery
    {
        /// <summary>
        /// Returns the XY query for the UTIDs and XY parameter supplied.
        /// </summary>
        /// <param name="keys">List of keys used to filter the returned rows.</param>
        /// <param name="xy">The XY parameter used in the query.</param>
        /// <returns>The XY query.</returns>
        string GetQuery(UTIDPlantNumberKeys keys, string xy);
    }
}
