﻿using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace SudburyDataAnalyser.Queries.XYQueries
{
    /// <summary>
    /// Base class for all XY pressure request queries. Used to retrieve rows including a median
    /// value from a test point table. 
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class XYPressureReqQueryBase : QueryBaseSubQueries, IXYQuery
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="XYPressureReqQueryBase"/> class.
        /// </summary>
        /// <param name="headerTable">The test point header table.</param>
        /// <param name="headerTableUridColumn">The test point header table URID column.</param>
        /// <param name="dataTable">The test point data table.</param>
        /// <param name="dataTableUridColumn">The test point data table URID column.</param>
        /// <param name="medianColumn">The test point header table median column.</param>
        /// <param name="durationColumn">The test point header table duration column.</param>
        /// <param name="name">The name to filter the rows by.</param>
        /// <param name="duration">The duration to filter the rows by.</param>
        /// <param name="pressureReq">The pressure request to filter the rows by.</param>
        public XYPressureReqQueryBase(
            Parameters.HeaderTable headerTable,
            Parameters.HeaderTableColumn headerTableUridColumn,
            Parameters.DataTable dataTable,
            Parameters.DataTableColumn dataTableUridColumn,
            Parameters.QueryColumn medianColumn,
            Parameters.QueryColumn durationColumn,
            string name, 
            string duration, 
            string pressureReq)
        {
            TestPointHeaderTable = headerTable;
            TestPointHeaderTableUridColumn = headerTableUridColumn;
            TestPointDataTable = dataTable;
            TestPointDataTableUridColumn = dataTableUridColumn;
            MedianColumn = medianColumn;
            DurationColumn = durationColumn;
            Name = name;
            Duration = duration;
            PressureReq = pressureReq;
        }

        /// <summary>
        /// Returns the query used to retrieve median values from the test point table.
        /// </summary>
        /// <param name="keys">List of keys used to filter the returned rows.</param>
        /// <param name="alias">X or Y value alias name.</param>
        /// <returns>The query used to retrieve the rows.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1117:ParametersMustBeOnSameLineOrSeparateLines",
                         Justification = "Disabled rule to improve readability of SQL.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.ReadabilityRules", "SA1115:ParameterMustFollowComma",
                         Justification = "Disabled rule to improve readability of SQL.")]
        public string GetQuery(UTIDPlantNumberKeys keys, string alias)
        {
            var query = new StringBuilder();
            query.AppendLines(
               // GetFilterOnKeys_V2(keys),
                "SELECT he.UTID, he.PlantNumber, he.LineNumber, tpda.", MedianColumn, " AS {0}".Args(alias),
                "FROM [", HeaderTable, "] AS he",
                "INNER JOIN [", DataHeaderTable, "] AS dh ON (dh.UTID = he.UTID AND dh.PlantNumber = he.PlantNumber)",
                "INNER JOIN [", TestPointHeaderTable, "] AS tphe ON (tphe.URID = dh.URID AND tphe.PlantNumber = dh.PlantNumber)",
                "INNER JOIN [", TestPointDataTable, "] AS tpda ON (tpda.", TestPointDataTableUridColumn, " = tphe.", TestPointHeaderTableUridColumn, 
                " AND tpda.PlantNumber = tphe.PlantNumber)",
                "INNER JOIN [", NameIDsTable, "] AS name ON (name.NameID = tpda.NameID AND name.PlantNumber = tpda.PlantNumber)",
                GetFilterOnKeys_V3("he"),
                "AND tphe.PressureReq = {0}".Args(PressureReq),
                "AND abs(tphe.", DurationColumn, " - {0}) < 0.0001".Args(Duration),
                "AND name.Name = '{0}'".Args(Name)
              //  , " DROP TABLE #temp;"
                );

            return query.ToString();
        }

        /// <summary>
        /// Gets or sets the name of the test point header table.
        /// </summary>
        private Parameters.HeaderTable TestPointHeaderTable { get; set; }

        /// <summary>
        /// Gets or sets the name of the test point header table URID column.
        /// </summary>
        private Parameters.HeaderTableColumn TestPointHeaderTableUridColumn { get; set; }

        /// <summary>
        /// Gets or sets the name of the test point data table.
        /// </summary>
        private Parameters.DataTable TestPointDataTable { get; set; }

        /// <summary>
        /// Gets or sets the name of the test point data table URID column.
        /// </summary>
        private Parameters.DataTableColumn TestPointDataTableUridColumn { get; set; }

        /// <summary>
        /// Gets or sets the name of the test point header table median column.
        /// </summary>
        private Parameters.QueryColumn MedianColumn { get; set; }

        /// <summary>
        /// Gets or sets the name of the test point header table duration column.
        /// </summary>
        private Parameters.QueryColumn DurationColumn { get; set; }

        /// <summary>
        /// Gets or sets the name to filter the rows by.
        /// </summary>
        private string Name { get; set; }

        /// <summary>
        /// Gets or sets the duration to filter the rows by.
        /// </summary>
        private string Duration { get; set; }

        /// <summary>
        /// Gets or sets the pressure request to filter the rows by.
        /// </summary>
        private string PressureReq { get; set; }
    }
}
