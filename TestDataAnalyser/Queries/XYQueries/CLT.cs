﻿namespace SudburyDataAnalyser.Queries.XYQueries
{
    /// <summary>
    /// Closed loop XY query class. 
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class CLT : XYPressureReqQueryBase
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="SudburyDataAnalyser.Queries.XYQueries.CLT"/> class.
        /// Returns the query used to retrieve rows from the closed loop tables.
        /// </summary>
        /// <param name="name">The name to filter the rows by.</param>
        /// <param name="duration">The duration to filter the rows by.</param>
        /// <param name="pressureReq">The pressure request to filter the rows by.</param>
        public CLT(string name, string duration, string pressureReq)
            : base(
            HeaderCLTable, 
            HeaderCLTableURIDColumn, 
            DataCLTable, 
            DataCLTableURIDColumn, 
            HeaderCLTableMedianColumn,
            HeaderCLTableDurationColumn, 
            name, 
            duration, 
            pressureReq)
        {
        }
    }
}
