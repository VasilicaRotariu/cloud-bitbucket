﻿namespace SudburyDataAnalyser.Queries.XYQueries
{
    /// <summary>
    /// Trims XY query class. 
    /// </summary>
    public class Trims : XYNameQueryBase
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="SudburyDataAnalyser.Queries.XYQueries.Trims"/> class.
        /// Returns the query used to retrieve rows from the trims tables.
        /// </summary>
        /// <param name="name">The name to filter the rows by.</param>
        public Trims(string name)
            : base(TrimsTable, TrimsTableValueColumn, name)
        {
        }
    }
}
