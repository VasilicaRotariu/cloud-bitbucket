﻿using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace SudburyDataAnalyser.Queries.XYQueries
{
    /// <summary>
    /// Class used to return the alignment XY query.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class TrendXYQuery
    {
        /// <summary>
        /// Returns the trend XY query.
        /// </summary>
        /// <param name="topNodeName">Top parent node of the tree.</param>
        /// <param name="keys">The keys to filter the rows by.</param>
        /// <param name="xQuery">The X data query embedded in the query.</param>
        /// <param name="yQuery">The Y data query embedded in the query.</param>
        /// <returns>The trend XY query.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation",
                         Justification = "Variables xQuery and yQuery are queries for the X and Y axis")]
        public string GetQuery(string topNodeName, UTIDPlantNumberKeys keys, IXYQuery xQuery, IXYQuery yQuery)
        {
            var query = new StringBuilder();

            query.Append("DECLARE @xdata Table (UTID int, PlantNumber smallint, LineNumber tinyint, X ");
            if (topNodeName == "PDate")
            {
                query.AppendLine("DateTime)");
            }
            else if (topNodeName == "Chronological")
            {
                query.AppendLine("int)");
            }
            else
            {
                query.AppendLine("real)");
            }

            query.AppendLines(
                "DECLARE @ydata Table (UTID int, PlantNumber smallint, LineNumber tinyint, Y real)",
                 QueryBaseSubQueries.GetFilterOnKeys_V2(keys),
                   //// Get x data
                   "INSERT @xdata",
                   xQuery.GetQuery(keys, "X"),
                //// Get y data
                   "INSERT @ydata",
                   yQuery.GetQuery(keys, "Y"),
                //// Select x and y data, joined by UTID
                   "SELECT X, Y FROM @xdata AS xdata",
                   "INNER JOIN @ydata As ydata ON (ydata.UTID = xdata.UTID AND ydata.PlantNumber = xdata.PlantNumber)"
                   ," DROP TABLE #temp;");

            return query.ToString();
        }
    }
}
