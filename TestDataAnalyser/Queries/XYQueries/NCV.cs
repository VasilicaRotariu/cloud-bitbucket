﻿namespace SudburyDataAnalyser.Queries.XYQueries
{
    /// <summary>
    /// Open loop XY query class. 
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class NCV : XYPressureReqQueryBase
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="SudburyDataAnalyser.Queries.XYQueries.NCV"/> class.
        /// Returns the query used to retrieve rows from the open loop tables.
        /// </summary>
        /// <param name="name">The name to filter the rows by.</param>
        /// <param name="duration">The duration to filter the rows by.</param>
        /// <param name="pressureReq">The pressure request to filter the rows by.</param>
        public NCV(string name, string duration, string pressureReq)
            : base(
            HeaderOLTable, 
            HeaderOLTableURIDColumn, 
            DataOLTable, 
            DataOLTableURIDColumn,
            HeaderOLTableMedianColumn,
            HeaderOLTableDurationColumn, 
            name, 
            duration, 
            pressureReq)
        {
        }
    }
}
