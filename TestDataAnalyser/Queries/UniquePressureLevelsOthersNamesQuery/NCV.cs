﻿namespace SudburyDataAnalyser.NCV
{
    /// <summary>
    /// Unique pressure level, duration and names query for open loop.
    /// </summary>
    public class QueryUniquePressureLevelsDurationsNames : SudburyDataAnalyser.UniquePressureLevelsOthersNamesBase.Query<UniquePressureLevelsOthersNamesBase.Row>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="QueryUniquePressureLevelsDurationsNames"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="keys">List of keys used to filter the returned rows.</param>
        public QueryUniquePressureLevelsDurationsNames(Database.DatabaseBase database, UTIDPlantNumberKeys keys)
            : base(database, keys, DataOLTable, DataOLTableURIDColumn, HeaderOLTableDurationColumn, HeaderOLTable, HeaderOLTableURIDColumn)
        {
        }
    }
}
