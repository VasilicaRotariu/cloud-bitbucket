﻿using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.UniquePressureLevelsOthersNamesBase
{
    /// <summary>
    /// Base class for rows returned from a unique pressure levels, other and names queries.
    /// </summary>
    public class Row : RowBase
    {
        /// <summary>
        /// Gets or sets the pressure level column in the row.
        /// </summary>
        public string PressureReq { get; protected set; }

        /// <summary>
        /// Gets or sets the name column in the row.
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a row.
        /// </summary>
        /// <param name="reader">SQLDataReader used to read row from.</param>
        public override void Initialise(SqlDataReader reader)
        {
            PressureReq = ReadString(reader, "PressureReq");
            Name = ReadString(reader, "Name");
        }
    }
}
