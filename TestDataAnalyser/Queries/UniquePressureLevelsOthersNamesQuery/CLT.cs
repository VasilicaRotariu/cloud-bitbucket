﻿namespace SudburyDataAnalyser.CLT
{
    /// <summary>
    /// Unique pressure level, fuel demand and names query for closed loop.
    /// </summary>
    public class QueryUniquePressureLevelsFuelDemandsNames : SudburyDataAnalyser.UniquePressureLevelsOthersNamesBase.Query<UniquePressureLevelsOthersNamesBase.Row>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="QueryUniquePressureLevelsFuelDemandsNames"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="keys">List of keys used to filter the returned rows.</param>
        public QueryUniquePressureLevelsFuelDemandsNames(Database.DatabaseBase database, UTIDPlantNumberKeys keys)
            : base(database, keys, DataCLTable, DataCLTableURIDColumn, FuelDemandColumn, HeaderCLTable, HeaderCLTableURIDColumn)
        {
        }
    }
}
