﻿using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.CLT.UniquePressureLevelsOthersNamesBase
{
    /// <summary>
    /// Rows returned from a unique pressure level, fuel demand and names query.
    /// Based on the unique pressure level, other and names row.
    /// </summary>
    public class Row : SudburyDataAnalyser.UniquePressureLevelsOthersNamesBase.Row
    {
        /// <summary>
        /// Gets or sets the fuel demand column in the row.
        /// </summary>
        public string FuelDemand { get; protected set; }

        /// <summary>
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a row.
        /// </summary>
        /// <param name="reader">SQLDataReader used to read row from.</param>
        public override void Initialise(SqlDataReader reader)
        {
            base.Initialise(reader);
            FuelDemand = ReadString(reader, Queries.Parameters.QueryParameters.FuelDemandColumn);
        }
    }
}
