﻿using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.NCV.UniquePressureLevelsOthersNamesBase
{
    /// <summary>
    /// Rows returned from a unique pressure level, duration and names query.
    /// Based on the unique pressure level, other and names row.
    /// </summary>
    public class Row : SudburyDataAnalyser.UniquePressureLevelsOthersNamesBase.Row
    {
        /// <summary>
        /// Gets or sets the duration column in the row.
        /// </summary>
        public string Duration { get; protected set; }

        /// <summary>
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a row.
        /// </summary>
        /// <param name="reader">SQLDataReader used to read row from.</param>
        public override void Initialise(SqlDataReader reader)
        {
            base.Initialise(reader);
            Duration = ReadString(reader, Queries.Parameters.QueryParameters.HeaderOLTableDurationColumn);
        }
    }
}
