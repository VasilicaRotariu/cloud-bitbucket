﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SudburyDataAnalyser.Queries.Parameters;

namespace SudburyDataAnalyser.Database
{
    /// <summary>
    /// Database class for MDEG virgin components database.
    /// </summary>
    public class MdegInjectorComponentDatabase : InjectorComponentDatabase
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="MdegInjectorComponentDatabase"/> class.
        /// </summary>
        public MdegInjectorComponentDatabase()
        {
        }
    }
}
