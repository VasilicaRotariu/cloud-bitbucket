﻿using SudburyDataAnalyser.Queries.Parameters;

namespace SudburyDataAnalyser.Database
{
    /// <summary>
    /// Base class for all non FT structured databases.
    /// </summary>
    public class NonFtDatabase : DatabaseBase
    {
        /// <summary>
        /// Gets a value indicating whether the database is an FT structured database or not.
        /// </summary>
        public override bool FtStructure
        {
            get { return false; }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="NonFtDatabase"/> class.
        /// </summary>
        /// <param name="connectionString">The database connection string.</param>
        public NonFtDatabase(string connectionString)
        {
            QueryParameters.HeaderTable.Value = "Header";

            ConnectionString = connectionString;

            QueryParameters.EarlyEndTable.Value = "EarlyEnd";
            QueryParameters.LeakDownTable.Value = "LeakDown";
            QueryParameters.NameIDsTable.Value = "NameID";
            QueryParameters.NominalDatasetTable.Value = "NominalDataset";
            QueryParameters.ResultsTable.Value = "Results";
            QueryParameters.RigTypeTable.Value = "RigType";
            QueryParameters.TracesTable.Value = "ChartTrace";
            QueryParameters.TrendChartTable.Value = "TrendChart";
            QueryParameters.TrimCodeTable.Value = "TrimCode";
            QueryParameters.TrimsTable.Value = "Trims";
            QueryParameters.TrimmedIdentityTable.Value = "PassOffTrimmedIdentity";
            QueryParameters.TrimmedPassOffTable.Value = "PassOffTrimmed";

            QueryParameters.DataHeaderTable.Value = "DataHeader";
            QueryParameters.DataCLTable.Value = "DataCL";
            QueryParameters.DataFODTable.Value = null;
            QueryParameters.DataLeakTable.Value = null;
            QueryParameters.DataOLTable.Value = "DataOL";
            QueryParameters.DataOMVTable.Value = null;
            QueryParameters.DataStabiliseCLTable.Value = "DataStabiliseCL";
            QueryParameters.DataStabiliseOLTable.Value = "DataStabiliseOL";
            QueryParameters.DataTrimmedTable.Value = "DataTrimmed";

            QueryParameters.HeaderTable.Value = "Header";
            QueryParameters.HeaderCLTable.Value = "HeaderCL";
            QueryParameters.HeaderFODTable.Value = "HeaderOMVFT";
            QueryParameters.HeaderLeakTable.Value = null;
            QueryParameters.HeaderOLTable.Value = "HeaderOL";
            QueryParameters.HeaderOMVTable.Value = "HeaderOMV";
            QueryParameters.HeaderStabiliseCLTable.Value = "HeaderStabiliseCL";
            QueryParameters.HeaderStabiliseOLTable.Value = "HeaderStabiliseOL";
            QueryParameters.HeaderTrimmedTable.Value = "HeaderTrimmed";

            QueryParameters.DataCLTableURIDColumn.Value = "URIDCL";
            QueryParameters.DataFODTableURIDColumn.Value = null;
            QueryParameters.DataLeakTableURIDColumn.Value = null;
            QueryParameters.DataOLTableURIDColumn.Value = "URIDOL";
            QueryParameters.DataOMVTableURIDColumn.Value = null;
            QueryParameters.DataStabiliseCLTableURIDColumn.Value = "URIDStabiliseCL";
            QueryParameters.DataStabiliseOLTableURIDColumn.Value = "URIDStabiliseOL";
            QueryParameters.DataTrimmedTableURIDColumn.Value = "URIDTrimmed";

            QueryParameters.HeaderTableIDENTColumn.Value = "PassOffSpec";
            QueryParameters.HeaderTableTestTypeColumn.Value = "";
            QueryParameters.NominalDatasetTableIDENTColumn.Value = "PassOffSpec";
            QueryParameters.HeaderTableLuaSVNColumn.Value = "LuaSVN";
            QueryParameters.HeaderCLTableURIDColumn.Value = "URIDCL";
            QueryParameters.HeaderFODTableURIDColumn.Value = null;
            QueryParameters.HeaderLeakTableURIDColumn.Value = null;
            QueryParameters.HeaderOLTableURIDColumn.Value = "URIDOL";
            QueryParameters.HeaderOMVTableURIDColumn.Value = null;
            QueryParameters.HeaderStabiliseCLTableURIDColumn.Value = "URIDStabiliseCL";
            QueryParameters.HeaderStabiliseOLTableURIDColumn.Value = "URIDStabiliseOL";
            QueryParameters.HeaderTrimmedTableURIDColumn.Value = "URIDTrimmed";

            QueryParameters.HeaderCLTableDurationColumn.Value = "Logic1";
            QueryParameters.HeaderOLTableDurationColumn.Value = "Logic";
            QueryParameters.HeaderTrimmedTableDurationColumn.Value = "NominalFuel";
            QueryParameters.HeaderOLTableMedianColumn.Value = "Median";
            QueryParameters.HeaderCLTableMedianColumn.Value = "Median";
            QueryParameters.HeaderTrimmedTableMedianColumn.Value = "Data";
            QueryParameters.FuelDemandColumn.Value = "FuelDemand1";
            QueryParameters.NominalFuelColumn.Value = "NominalFuel";

            QueryParameters.TraceTableIdentColumn.Value = "PassOffSpec";
            QueryParameters.TraceTableBinaryTraceDataColumn.Value = "Trace";

            QueryParameters.TrimsTableValueColumn.Value = "Value";
            QueryParameters.GradesTableValueColumn.Value = "Grade";

            QueryParameters.EqualNames.Value = "{0}.NameID = {1}.NameID AND {0}.PlantNumber = {1}.PlantNumber";
            QueryParameters.IncompleteGradeValue.Value = "-5";
        }
    }
}