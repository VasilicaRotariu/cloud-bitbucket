﻿namespace SudburyDataAnalyser.Database
{
    /// <summary>
    /// Database class for DAF DB01 database.
    /// </summary>
    public class DafSudburyDatabaseArchive : NonFtDatabase
    {
        /// <summary>
        /// Gets or sets the Databases Location
        /// </summary>
        public override DatabaseLocation Location
        {
            get { return DatabaseLocation.Sudbury; }
        }

        /// <summary>
        /// Gets or sets the Databases Product Type
        /// </summary>
        public override DatabaseProductType ProductType
        {
            get { return DatabaseProductType.DAF; }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DafSudburyDatabaseArchive"/> class.
        /// </summary>
        public DafSudburyDatabaseArchive()
            : base(Properties.Settings.Default.DAFSudburyArchiveConnectionString)
        {
        }
    }
}
