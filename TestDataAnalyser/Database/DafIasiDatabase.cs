﻿namespace SudburyDataAnalyser.Database
{
    class DafIasiDatabase : NonFtDatabase
    {
        /// <summary>
        /// Gets or sets the Databases Location
        /// </summary>
        public override DatabaseLocation Location
        {
            get { return DatabaseLocation.Iasi; }
        }

        /// <summary>
        /// Gets or sets the Databases Product Type
        /// </summary>
        public override DatabaseProductType ProductType
        {
            get { return DatabaseProductType.DAF; }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DafSudburyDatabaseArchive"/> class.
        /// </summary>
        public DafIasiDatabase()
            : base(Properties.Settings.Default.DAFIasiConnectionString)
        {
        }
    }
}
