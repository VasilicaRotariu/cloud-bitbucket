﻿using System;

namespace SudburyDataAnalyser.Database
{
    /// <summary>
    /// The Location of the Database
    /// </summary>
    public enum DatabaseLocation
    {
        Stonehouse = 1,
        Sudbury = 2,
        Iasi = 3
    }

    /// <summary>
    /// The Type of Product produced by the machine
    /// </summary>
    public enum DatabaseProductType
    {
        DAF = 1,
        MDEG = 2,
        Pump = 3,
        Other = 4,
    }

    /// <summary>
    /// Base class for all databases.
    /// </summary>
    public class DatabaseBase
    {
        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        public string ConnectionString { get; protected set; }

        /// <summary>
        /// Gets a value indicating whether the database is an FT structured database or not.
        /// </summary>
        public virtual bool FtStructure
        {
            get { throw new NotSupportedException(); }
        }

        /// <summary>
        /// Gets a value indicating whether the database is an Engineering database or not.
        /// </summary>
        public virtual bool EngineeringDatabase
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the Database's Location
        /// </summary>
        public virtual DatabaseLocation Location
        {
            get { throw new NotSupportedException(); }
        }

        /// <summary>
        /// Gets the Databases Product Type
        /// </summary>
        public virtual DatabaseProductType ProductType
        {
            get { throw new NotSupportedException(); }
        }

        public virtual DatabaseBase IasiArchiveDatabase { get; set; }
        public virtual DatabaseBase SudburyArchiveDatabase { get; set; }

        public DateTime MinArchiveTime {
            get { return DateTime.Now.AddMonths(-Properties.Settings.Default.MinAchiveQueryTime); } }

        public bool HasSudburyArchiveStructure { get { return IasiArchiveDatabase != null; } }
        public bool HasIasiArchiveStructure { get { return IasiArchiveDatabase != null; } }

        public bool QueryIasiArchiveStructure { get; set; }
        public bool QuerySudburyArchiveStructure { get; set; }
    }
}
