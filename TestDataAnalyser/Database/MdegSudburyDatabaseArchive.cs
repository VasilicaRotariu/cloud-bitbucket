﻿namespace SudburyDataAnalyser.Database
{
    /// <summary>
    /// Database class for MDEG DB01 database.
    /// </summary>
    public class MdegSudburyDatabaseArchive : NonFtDatabase
    {
        /// <summary>
        /// Gets or sets the Databases Location
        /// </summary>
        public override DatabaseLocation Location
        {
            get { return DatabaseLocation.Sudbury; }
        }

        /// <summary>
        /// Gets or sets the Databases Product Type
        /// </summary>
        public override DatabaseProductType ProductType
        {
            get { return DatabaseProductType.MDEG; }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MdegSudburyDatabaseArchive"/> class.
        /// </summary>
        public MdegSudburyDatabaseArchive()
            : base(Properties.Settings.Default.MDEGSudburyArchiveConnectionString)
        {
        }
    }
}
