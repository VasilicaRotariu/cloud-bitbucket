﻿namespace SudburyDataAnalyser.Database
{
    /// <summary>
    /// Database class for DAF DB01 database.
    /// </summary>
    public class DafIasiDatabaseArchive : NonFtDatabase
    {
        /// <summary>
        /// Gets or sets the Databases Location
        /// </summary>
        public override DatabaseLocation Location
        {
            get { return DatabaseLocation.Iasi; }
        }

        /// <summary>
        /// Gets or sets the Databases Product Type
        /// </summary>
        public override DatabaseProductType ProductType
        {
            get { return DatabaseProductType.DAF; }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DafSudburyDatabaseArchive"/> class.
        /// </summary>
        public DafIasiDatabaseArchive()
            : base(Properties.Settings.Default.DAFIasiArchiveConnectionString)
        {
        }
    }
}
