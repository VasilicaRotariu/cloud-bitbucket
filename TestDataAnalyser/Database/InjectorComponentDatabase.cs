﻿namespace SudburyDataAnalyser.Database
{
    /// <summary>
    /// Database class for cache 0675 database.
    /// </summary>
    public class InjectorComponentDatabase : DatabaseBase
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="InjectorComponentDatabase"/> class.
        /// </summary>
        public InjectorComponentDatabase()
        {
            ConnectionString = Properties.Settings.Default.Euro6ConnectionString;
        }
    }
}
