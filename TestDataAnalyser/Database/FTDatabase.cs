﻿using SudburyDataAnalyser.Queries.Parameters;

namespace SudburyDataAnalyser.Database
{
    /// <summary>
    /// Base class for all FT structured databases.
    /// </summary>
    public class FtDatabase : DatabaseBase
    {
        /// <summary>
        /// Gets a value indicating whether the database is an FT structured database or not.
        /// </summary>
        public override bool FtStructure
        {
            get { return true; }
        } 

        /// <summary>
        /// Gets or sets the Databases Location
        /// </summary>
        public override DatabaseLocation Location
        {
            get { return DatabaseLocation.Stonehouse; }
        }

        /// <summary>
        /// Gets or sets the Databases Product Type
        /// </summary>
        public override DatabaseProductType ProductType
        {
            get { return DatabaseProductType.Pump; }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="FtDatabase"/> class.
        /// </summary>
        /// <param name="connectionString">The database connection string.</param>
        public FtDatabase(string connectionString)
        {
            ConnectionString = connectionString;

            QueryParameters.EarlyEndTable.Value = "FTEarlyEnd";
            QueryParameters.LeakDownTable.Value = "FTLeakDown";
            QueryParameters.NameIDsTable.Value = "FTNameIDs";
            QueryParameters.NominalDatasetTable.Value = "FTNominalDataset";
            QueryParameters.ResultsTable.Value = "FTResults";
            QueryParameters.RigTypeTable.Value = "RigType";
            QueryParameters.TracesTable.Value = "FTTraces";
            QueryParameters.TrendChartTable.Value = "FTTrend";
            QueryParameters.TrimCodeTable.Value = "FTTrimCode";
            QueryParameters.TrimsTable.Value = "FTTrims";
            QueryParameters.TrimmedIdentityTable.Value = null;
            QueryParameters.TrimmedPassOffTable.Value = null;

            QueryParameters.DataHeaderTable.Value = "FTDataHeader";
            QueryParameters.DataCLTable.Value = "FTDataCLT";
            QueryParameters.DataFODTable.Value = "FTDataFOD";
            QueryParameters.DataLeakTable.Value = "FTDataLEAK";
            QueryParameters.DataOLTable.Value = "FTDataNCV";
            QueryParameters.DataOMVTable.Value = "FTDataOMV";
            QueryParameters.DataStabiliseCLTable.Value = "FTDataStabiliseCL";
            QueryParameters.DataStabiliseOLTable.Value = "FTDataStabiliseOL";
            QueryParameters.DataTrimmedTable.Value = null;

            QueryParameters.HeaderTable.Value = "FTHeader";
            QueryParameters.HeaderCLTable.Value = "FTHeaderCLT";
            QueryParameters.HeaderFODTable.Value = "FTHeaderFOD";
            QueryParameters.HeaderLeakTable.Value = "FTHeaderLEAK";
            QueryParameters.HeaderOLTable.Value = "FTHeaderNCV";
            QueryParameters.HeaderOMVTable.Value = "FTHeaderOMV";
            QueryParameters.HeaderStabiliseCLTable.Value = "FTHeaderStabiliseCL";
            QueryParameters.HeaderStabiliseOLTable.Value = "FTHeaderStabiliseOL";
            QueryParameters.HeaderTrimmedTable.Value = null;

            QueryParameters.DataCLTableURIDColumn.Value = "URIDCLT";
            QueryParameters.DataFODTableURIDColumn.Value = "URIDFOD";
            QueryParameters.DataLeakTableURIDColumn.Value = "URIDLEAK";
            QueryParameters.DataOLTableURIDColumn.Value = "URIDNCV";
            QueryParameters.DataOMVTableURIDColumn.Value = "URIDOMV";
            QueryParameters.DataStabiliseCLTableURIDColumn.Value = "URIDStabiliseCL";
            QueryParameters.DataStabiliseOLTableURIDColumn.Value = "URIDStabiliseOL";
            QueryParameters.DataTrimmedTableURIDColumn.Value = null;

            QueryParameters.HeaderTableIDENTColumn.Value = "IDENT";
            QueryParameters.HeaderTableTestTypeColumn.Value = "TestType";
            QueryParameters.HeaderTableLuaSVNColumn.Value = "LuaSVNVersion";
            QueryParameters.NominalDatasetTableIDENTColumn.Value = "IDENT";
            QueryParameters.HeaderCLTableURIDColumn.Value = "URIDCLT";
            QueryParameters.HeaderFODTableURIDColumn.Value = "URIDFOD";
            QueryParameters.HeaderLeakTableURIDColumn.Value = "URIDLEAK";
            QueryParameters.HeaderOLTableURIDColumn.Value = "URIDNCV";
            QueryParameters.HeaderOMVTableURIDColumn.Value = "URIDOMV";
            QueryParameters.HeaderStabiliseCLTableURIDColumn.Value = "URIDStabiliseCL";
            QueryParameters.HeaderStabiliseOLTableURIDColumn.Value = "URIDStabiliseOL";
            QueryParameters.HeaderTrimmedTableURIDColumn.Value = null;

            QueryParameters.HeaderCLTableDurationColumn.Value = "Duration";
            QueryParameters.HeaderOLTableDurationColumn.Value = "Duration";
            QueryParameters.HeaderTrimmedTableDurationColumn.Value = null;
            QueryParameters.HeaderOLTableMedianColumn.Value = "Median";
            QueryParameters.HeaderCLTableMedianColumn.Value = "Median";
            QueryParameters.HeaderTrimmedTableMedianColumn.Value = null;
            QueryParameters.FuelDemandColumn.Value = "FuelDemand";
            QueryParameters.NominalFuelColumn.Value = "NominalFuel";
            QueryParameters.TraceTableIdentColumn.Value = "IDENT";
            QueryParameters.TraceTableBinaryTraceDataColumn.Value = "BinaryTraceData";

            QueryParameters.TrimsTableValueColumn.Value = "Value";
            QueryParameters.GradesTableValueColumn.Value = "Grade";

            QueryParameters.EqualNames.Value = "{0}.NameID = {1}.NameID";
            QueryParameters.IncompleteGradeValue.Value = "-1";
        }
    }
}
