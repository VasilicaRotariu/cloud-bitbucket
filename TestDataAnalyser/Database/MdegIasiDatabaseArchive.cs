﻿namespace SudburyDataAnalyser.Database
{
    /// <summary>
    /// Database class for MDEG DB01 database.
    /// </summary>
    public class MdegIasiDatabaseArchive : NonFtDatabase
    {
        /// <summary>
        /// Gets or sets the Databases Location
        /// </summary>
        public override DatabaseLocation Location
        {
            get { return DatabaseLocation.Iasi; }
        }

        /// <summary>
        /// Gets or sets the Databases Product Type
        /// </summary>
        public override DatabaseProductType ProductType
        {
            get { return DatabaseProductType.MDEG; }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MdegIasiDatabaseArchive"/> class.
        /// </summary>
        public MdegIasiDatabaseArchive()
            : base(Properties.Settings.Default.MDEGIasiArchiveConnectionString)
        {
        }
    }
}
