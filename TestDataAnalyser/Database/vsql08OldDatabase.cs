﻿namespace SudburyDataAnalyser.Database
{
    /// <summary>
    /// Database class for the old VSQL 08 database.
    /// </summary>
    public class Vsql08OldDatabase : FtDatabase
    {
        /// <summary>
        /// Gets or sets the Databases Location
        /// </summary>
        public override DatabaseLocation Location
        {
            get { return DatabaseLocation.Stonehouse; }
        }

        /// <summary>
        /// Gets or sets the Databases Product Type
        /// </summary>
        public override DatabaseProductType ProductType
        {
            get { return DatabaseProductType.Pump; }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Vsql08OldDatabase"/> class.
        /// </summary>
        public Vsql08OldDatabase()
            : base(Properties.Settings.Default.DAFIasiConnectionString)//F2Testvsql08ConnectionString)
        {
        }
    }
}