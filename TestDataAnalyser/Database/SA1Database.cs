﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SudburyDataAnalyser.Database
{
    /// <summary>
    /// Represents a Connection to the Selective Assembly 1 Database
    /// </summary>
    public class SA1Database : DatabaseBase
    {
        /// <summary>
        /// Gets a value indicating whether the database in an engineering database
        /// </summary>
        public override bool EngineeringDatabase
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the database follows an FtStructure
        /// </summary>
        public override bool FtStructure
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the Location of the Database
        /// </summary>
        public override DatabaseLocation Location
        {
            get
            {
                return DatabaseLocation.Sudbury;
            }
        }

        /// <summary>
        /// Gets the product type of the Database
        /// </summary>
        public override DatabaseProductType ProductType
        {
            get
            {
                return DatabaseProductType.Other;
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SA1Database" /> class
        /// </summary>
        public SA1Database()
        {
            this.ConnectionString = @"server=selassembly\selassy;user id=StatsRO;password=StatsRO;persist security info=True;database=SA_Stats;Connection Timeout=5;Application Name=Test Data Analyser";
        }
    }
}
