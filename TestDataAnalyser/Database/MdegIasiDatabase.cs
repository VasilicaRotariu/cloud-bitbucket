﻿namespace SudburyDataAnalyser.Database
{
    class MdegIasiDatabase : NonFtDatabase
    {
        /// <summary>
        /// Gets or sets the Databases Location
        /// </summary>
        public override DatabaseLocation Location
        {
            get { return DatabaseLocation.Iasi; }
        }

        /// <summary>
        /// Gets or sets the Databases Product Type
        /// </summary>
        public override DatabaseProductType ProductType
        {
            get { return DatabaseProductType.MDEG; }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MdegIasiDatabase"/> class.
        /// </summary>
        public MdegIasiDatabase()
            : base(Properties.Settings.Default.MDEGIasiConnectionString)
        {
        }
    }
}
