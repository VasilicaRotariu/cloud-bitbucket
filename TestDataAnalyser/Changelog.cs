﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SudburyDataAnalyser
{
    /// <summary>
    /// Changelog, difference since the last version
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed.")]
    public sealed class Changelog
    {
        /// <summary> Constant file name to the changelog </summary>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed.")]
        public const string ChangeLogFile = "changelog.txt";

        /// <summary> Gets the changelog Text </summary>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed.")]
        public string ChangeLogText { get; private set; }

        /// <summary> Gets the version number </summary>
        public string Version
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Changelog" /> class
        /// </summary>
        public Changelog()
        {
            ReadFromFile();
        }

        /// <summary>
        /// Reads the contents of the changelog from the file
        /// </summary>
        private void ReadFromFile()
        {
            try
            {
                using (StreamReader read = new StreamReader(File.OpenRead("changelog.txt")))
                {
                    StringBuilder build = new StringBuilder();
                    while (!read.EndOfStream)
                    {
                        string line = read.ReadLine();

                        //replace $versioncode$ with the version number
                        line = line.Replace("$versioncode$", Version);
                        build.AppendLine(line);
                    }

                    ChangeLogText = build.ToString();
                    read.Close();
                }
            }
            catch (Exception e)
            {
                ChangeLogText = "Error reading '{0}' varify file exists".Args(ChangeLogFile);
            }
        }
    }
}
