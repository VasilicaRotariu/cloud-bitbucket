﻿using System.IO;

namespace SudburyDataAnalyser
{
    /// <summary>
    /// Class used for logging debug information.
    /// </summary>
    public static class DebugLog
    { 
        /// <summary>
        /// The file name the log messages are saved too.
        /// </summary>
        private static string fileName = "";

        /// <summary>
        /// Gets or sets the name and path of the log file.
        /// </summary>
        public static string FileName
        {
            get 
            { 
                return fileName; 
            }

            set 
            {
                fileName = value;
                if (fileName == null)
                {
                    Enabled = false;
                }
                else
                {
                    var directoryName = Path.GetDirectoryName(fileName);
                    if (directoryName == null)
                    {
                        Enabled = false;
                    }
                    else
                    {
                        Enabled = Directory.Exists(directoryName);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether logging is enabled or not.
        /// </summary>
        public static bool Enabled { get; set; }

        /// <summary>
        /// Empties any existing log file.
        /// </summary>
        public static void Clear()
        {
            try
            {
                File.Delete(fileName);
            }
            catch
            {
                Enabled = false;
            }
        }

        /// <summary>
        /// Writes the specified text to the log file.
        /// </summary>
        /// <param name="logText">The text to write to the log file.</param>
        public static void Write(string logText)
        {
            if (Enabled)
            {
                try
                {
                    using (var writer = new StreamWriter(File.Open(fileName, FileMode.Append)))
                    {
                        writer.Write(logText);
                    }
                }
                catch
                {
                    Enabled = false;
                }
            }
        }

        /// <summary>
        /// Writes the specified line of text to the log file.
        /// </summary>
        /// <param name="logText">The text to write to the log file.</param>
        public static void WriteLine(string logText)
        {
            if (Enabled)
            {
                try
                {
                    using (var writer = new StreamWriter(File.Open(fileName, FileMode.Append)))
                    {
                        writer.WriteLine(logText);
                    }
                }
                catch
                {
                    Enabled = false;
                }
            }
        }

        /// <summary>
        /// Writes the start of a block to the log file.
        /// </summary>
        /// <param name="logTitle">The block title.</param>
        public static void WriteStart(string logTitle)
        {
            if (!Enabled) return;
            if (fileName == null) return;

            var highlight = new string('*', logTitle.Length + "Start of ".Length);

            using (var writer = new StreamWriter(File.Open(fileName, FileMode.Append)))
            {
                writer.WriteLine(highlight);
                writer.Write("Start of ");
                writer.WriteLine(logTitle);
                writer.WriteLine(highlight);
            }
        }

        /// <summary>
        /// Writes the end of a block to the log file.
        /// </summary>
        /// <param name="logTitle">The block title.</param>
        public static void WriteEnd(string logTitle)
        {
            if (Enabled)
            {
                var highlight = new string('*', logTitle.Length + "End of ".Length);

                using (var writer = new StreamWriter(File.Open(fileName, FileMode.Append)))
                {
                    writer.WriteLine(highlight);
                    writer.Write("End of ");
                    writer.WriteLine(logTitle);
                    writer.WriteLine(highlight);
                }
            }
        }
    }
}
