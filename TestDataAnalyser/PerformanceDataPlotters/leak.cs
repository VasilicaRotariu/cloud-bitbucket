﻿using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.Leak
{
    /// <summary>
    /// Performance plotter class for the leak down data.
    /// </summary>
    public class Plotter : PerformanceDataPlotter<Row>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Leak.Plotter"/> class.
        /// </summary>
        /// <param name="seriesSelection">Array of flags specifying the series selection.</param>
        /// <param name="selectedKeys">List of selected keys used to select the data to plot.</param>
        /// <param name="selectedParameters">List of selected parameters used to select the data to plot.</param>
        /// <param name="chart">The chart and legend to plot the data on.</param>
        /// <param name="gridView">The grid view to add the filtered rows to.</param>
        /// <param name="data">The rows of data used for the plot.</param>
        public Plotter(
            IEnumerable<bool> seriesSelection,
            UTIDPlantNumberKeys selectedKeys,
            List<string> selectedParameters,
            ChartSplitLegend chart,
            DataGridView gridView,
            List<Row> data)
            : base(seriesSelection, selectedKeys, null, selectedParameters, null, chart, false, gridView, data)
        {
        }

        /// <summary>
        /// Plots the leak down chart and populates the grid view.
        /// </summary>
        public void Plot()
        {
            GridViewColumns = new[] 
            {
                "UTID", "ISN", "PDate", "Global", "IDENT", "Testplan", "URID", "TestPoint", "PressureReq", "OffTime"
            };

            PlotPerformanceData(
                Data,
                Data.GetUTIDPressureLevelStatisticTree(NoOrder),
                ChartSplitLegend,
                delegate(AddGridViewRowParameters parameters)
                {
                    var row = parameters.Row;

                    // Add data points to datagridview (separate from creating series to prevent duplication of records in datgridview
                    GridView.Rows.Add(
                        row.UTID.ToString(CultureInfo.InvariantCulture),
                        row.ISN,
                        row.PDate.ToString(CultureInfo.InvariantCulture),
                        row.Global.ToString(CultureInfo.InvariantCulture),
                        row.IDENT,
                        row.TestPlan,
                        row.URID.ToString(CultureInfo.InvariantCulture),
                        row.TestPlan,
                        row.PressureReq.ToString(CultureInfo.InvariantCulture),
                        row.OffTime.ToString(CultureInfo.InvariantCulture));
                },
                parameters => parameters.Series.Points.AddXY(parameters.Row.OffTime, parameters.Row.Pressure));
        }
    }
}
