﻿using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.Trimmed
{
    /// <summary>
    /// Performance plotter class for the log of the trimmed data.
    /// </summary>
    public class LogPlotter : PerformanceDataPlotter<Row>
    {
        /// <summary>
        /// Gets or sets a value indicating whether the fuel as error should be shown.
        /// </summary>
        private bool ShowFuelAsError { get; set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="Trimmed.LogPlotter"/> class.
        /// </summary>
        /// <param name="seriesSelection">Array of flags specifying the series selection.</param>
        /// <param name="selectedKeys">List of selected keys used to select the data to plot.</param>
        /// <param name="selectedPressureLevels">List of selected pressure levels used to select the data to plot.</param>
        /// <param name="selectedParameters">List of selected parameters used to select the data to plot.</param>
        /// <param name="selectedStatistics">List of selected statistics used to select the data to plot.</param>
        /// <param name="chart">The chart and legend to plot the data on.</param>
        /// <param name="gridView">The grid view to add the filtered rows to.</param>
        /// <param name="data">The rows of data used for the plot.</param>
        /// <param name="showFuelAsError">Flag specifying whether fuel should be shown as an error.</param>
        public LogPlotter(
            IEnumerable<bool> seriesSelection,
            UTIDPlantNumberKeys selectedKeys,
            List<string> selectedPressureLevels,
            List<string> selectedParameters,
            List<string> selectedStatistics,
            ChartSplitLegend chart,
            DataGridView gridView,
            List<Row> data,
            bool showFuelAsError)
            : base(seriesSelection, selectedKeys, selectedPressureLevels, selectedParameters, selectedStatistics, chart, true, gridView, data)
        {
            ShowFuelAsError = showFuelAsError;
        }

        /// <summary>
        /// Plots the log trimmed chart and populates the grid view.
        /// </summary>
        public void Plot()
        {
            GridViewColumns = new[] 
            {
                "UTID", "ISN", "PDate", "Global", "LOP", "IDENT", "Testplan",
                "PressureReq", "Name", "NominalLogic", "NominalFuel", "Data"
            };

            PlotPerformanceData(
                Data,
                Data.GetUTIDPressureLevelStatisticTree(RowsOrderer.OrderRowsByNominalFuel),
                ChartSplitLegend,
                delegate(AddGridViewRowParameters parameters)
                {
                    var row = parameters.Row;

                    // Add data points to datagridview (separate from creating series to prevent duplication of records in datgridview
                    GridView.Rows.Add(
                        row.UTID.ToString(CultureInfo.InvariantCulture), 
                        row.ISN,
                        row.PDate.ToString(CultureInfo.InvariantCulture),
                        row.Global.ToString(CultureInfo.InvariantCulture), 
                        row.LOP, 
                        row.IDENT, 
                        row.TestPlan,
                        row.PressureReq.ToString(CultureInfo.InvariantCulture),
                        row.Name,
                        row.NominalLogic.ToString(CultureInfo.InvariantCulture),
                        row.NominalFuel.ToString(CultureInfo.InvariantCulture),
                        row.Data.ToString(CultureInfo.InvariantCulture));
                },
                delegate(AddSeriesPointParameters parameters)
                {
                    if (parameters.Statistic != "Data") return;

                    // Can't plot 0's on log plots so check > 0
                    if (parameters.Row.NominalFuel <= 0) return;

                    if (ShowFuelAsError && (parameters.Parameter == "Fuel"))
                    {
                        parameters.Series.Points.AddXY(parameters.Row.NominalFuel, parameters.Row.Data - parameters.Row.NominalFuel);
                    }
                    else
                    {
                        parameters.Series.Points.AddXY(parameters.Row.NominalFuel, parameters.Row.Data);
                    }
                });
        }
    }
}
