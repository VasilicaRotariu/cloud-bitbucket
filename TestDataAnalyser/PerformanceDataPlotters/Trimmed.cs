﻿using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Linq;
using System.Drawing;
using System.ComponentModel;
using System;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.Trimmed
{
    /// <summary>
    /// Performance plotter class for the trimmed data.
    /// </summary>
    public class Plotter : PerformanceDataPlotter<Row>
    {
        /// <summary>Gets or sets a value indicating whether or not to plot the Mean Error Curve</summary>
        public bool PlotMeanCurve { get; set; }

        /// <summary> Gets the Mean Data points used for plotting </summary>
        public List<Tuple<double, double>> MeanData { get; private set; }
         
        /// <summary>Gets or sets a value indicating whether or not to plot the Median Error Curve</summary>
        public bool PlotMedianCurve { get; set; }

        /// <summary>Gets the Median Data points used for plotting </summary>
        public List<Tuple<double, double>> MedianData { get; private set; }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="Trimmed.Plotter"/> class.
        /// </summary>
        /// <param name="seriesSelection">Array of flags specifying the series selection.</param>
        /// <param name="selectedKeys">List of selected keys used to select the data to plot.</param>
        /// <param name="limitsQuery">The query used to determine the limits.</param>
        /// <param name="selectedPressureLevels">List of selected pressure levels used to select the data to plot.</param>
        /// <param name="selectedParameters">List of selected parameters used to select the data to plot.</param>
        /// <param name="selectedStatistics">List of selected statistics used to select the data to plot.</param>
        /// <param name="chart">The chart and legend to plot the data on.</param>
        /// <param name="gridView">The grid view to add the filtered rows to.</param>
        /// <param name="data">The rows of data used for the plot.</param>
        public Plotter(
            IEnumerable<bool> seriesSelection,
            UTIDPlantNumberKeys selectedKeys,
            LimitsBase.Query limitsQuery,
            List<string> selectedPressureLevels,
            List<string> selectedParameters,
            List<string> selectedStatistics,
            ChartSplitLegend chart,
            DataGridView gridView,
            List<Row> data)
            : base(seriesSelection, selectedKeys, selectedPressureLevels, selectedParameters, selectedStatistics, chart, false, gridView, data)
        {
            LimitsQuery = limitsQuery;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Trimmed.Plotter"/> class.
        /// </summary>
        /// <param name="seriesSelection">Array of flags specifying the series selection.</param>
        /// <param name="selectedKeys">List of selected keys used to select the data to plot.</param>
        /// <param name="limitsQuery">The query used to determine the limits.</param>
        /// <param name="selectedPressureLevels">List of selected pressure levels used to select the data to plot.</param>
        /// <param name="selectedParameters">List of selected parameters used to select the data to plot.</param>
        /// <param name="selectedStatistics">List of selected statistics used to select the data to plot.</param>
        /// <param name="chart">The chart to plot the data on.</param>
        /// <param name="legend">The legend.</param>
        /// <param name="gridView">The grid view to add the filtered rows to.</param>
        /// <param name="data">The rows of data used for the plot.</param>
        public Plotter(
            IEnumerable<bool> seriesSelection,
            UTIDPlantNumberKeys selectedKeys,
            LimitsBase.Query limitsQuery,
            List<string> selectedPressureLevels,
            List<string> selectedParameters,
            List<string> selectedStatistics,
            Chart chart,
            ChartLegend legend,
            DataGridView gridView,
            List<Row> data)
            : base(seriesSelection, selectedKeys, selectedPressureLevels, selectedParameters, selectedStatistics, chart, legend, false, gridView, data)
        {
            LimitsQuery = limitsQuery;
        }

        /// <summary>
        /// Gets or sets the query to determine the limits.
        /// </summary>
        private LimitsBase.Query LimitsQuery { get; set; }

        /// <summary>
        /// Plots the trimmed chart and populates the grid view.
        /// </summary>
        public void Plot()
        {
            GridViewColumns = new[] 
            {
                "UTID", "ISN", "PDate", "Global", "LOP", "IDENT", "Testplan",
                "PressureReq", "Name", "NominalLogic", "NominalFuel", "Data"
            };

            if (ChartSplitLegend != null)
            {
                PlotPerformanceData(
                    Data,
                    Data.GetUTIDPressureLevelStatisticTree(RowsOrderer.OrderRowsByNominalFuel),
                    LimitsQuery,
                    ChartSplitLegend,
                    delegate(AddGridViewRowParameters parameters)
                    {
                        var row = parameters.Row;

                        // Add data points to datagridview (separate from creating series to prevent duplication of records in datgridview
                        GridView.Rows.Add(
                            row.UTID.ToString(CultureInfo.InvariantCulture),
                            row.ISN,
                            row.PDate.ToString(CultureInfo.InvariantCulture),
                            row.Global.ToString(CultureInfo.InvariantCulture),
                            row.LOP,
                            row.IDENT,
                            row.TestPlan,
                            row.PressureReq.ToString(CultureInfo.InvariantCulture),
                            row.Name,
                            row.NominalLogic.ToString(CultureInfo.InvariantCulture),
                            row.NominalFuel.ToString(CultureInfo.InvariantCulture),
                            row.Data.ToString(CultureInfo.InvariantCulture));
                    },
                    delegate(AddSeriesPointParameters parameters)
                    {
                        if (parameters.Statistic == "Data")
                        {
                            parameters.Series.Points.AddXY(parameters.Row.NominalFuel, parameters.Row.Data);
                        }
                    });

                //Add Stats Series Stats Curves
                if (PlotMeanCurve)
                {
                    MeanData = new List<Tuple<double, double>>();
                    foreach (Series s in GetPlotCurve(Data, "Mean", CalcMean))
                        ChartSplitLegend.Add(s);
                }

                if (PlotMedianCurve)
                {
                    MedianData = new List<Tuple<double, double>>();
                    foreach (Series s in GetPlotCurve(Data, "Median", CalcMedian))
                        ChartSplitLegend.Add(s);
                }
            }
            else if (Chart != null)
            {
                PlotPerformanceData(
                    Data,
                    Data.GetUTIDPressureLevelStatisticTree(RowsOrderer.OrderRowsByNominalFuel),
                    LimitsQuery,
                    Chart,
                    Legend,
                    delegate(AddGridViewRowParameters parameters)
                    {
                        var row = parameters.Row;

                        // Add data points to datagridview (separate from creating series to prevent duplication of records in datgridview
                        GridView.Rows.Add(
                            row.UTID.ToString(CultureInfo.InvariantCulture),
                            row.ISN,
                            row.PDate.ToString(CultureInfo.InvariantCulture),
                            row.Global.ToString(CultureInfo.InvariantCulture),
                            row.LOP,
                            row.IDENT,
                            row.TestPlan,
                            row.PressureReq.ToString(CultureInfo.InvariantCulture),
                            row.Name,
                            row.NominalLogic.ToString(CultureInfo.InvariantCulture),
                            row.NominalFuel.ToString(CultureInfo.InvariantCulture),
                            row.Data.ToString(CultureInfo.InvariantCulture));
                    },
                    delegate(AddSeriesPointParameters parameters)
                    {
                        if (parameters.Statistic == "Data")
                        {
                            parameters.Series.Points.AddXY(parameters.Row.NominalFuel, parameters.Row.Data);
                        }
                    });

                //Add Stats Series
                //Stats Curves
                if (PlotMeanCurve)
                {
                    MeanData = new List<Tuple<double, double>>();
                    foreach (Series s in GetPlotCurve(Data, "Mean", CalcMean))
                    {
                        Chart.Series.Add(s);
                        Legend.Add(s);
                    }
                }

                if (PlotMedianCurve)
                {
                    MedianData = new List<Tuple<double, double>>();
                    foreach (Series s in GetPlotCurve(Data, "Median", CalcMedian))
                    {
                        Chart.Series.Add(s);
                        Legend.Add(s);
                    }
                }
            }
        }
         
        /// <summary>
        /// Get a Plot curve for a statistic over a given dataset
        /// </summary>
        /// <param name="data">The dataset</param>
        /// <param name="statistic">The header for the Legend</param>
        /// <param name="function">The function to calculate the statistic from data</param>
        /// <returns>Collection of Average curve series</returns>
        private IEnumerable<Series> GetPlotCurve(List<Row> data, string statistic, Func<IEnumerable<Row>, double> function)
        {
            // Split into Pressure groups
            var groups = SplitData(data); 

            foreach (string param in SelectedParameters)
            {
                foreach (IGrouping<Tuple<short, short, int>, Row> groupData in groups)
                {
                    Tuple<short, short, int> key = groupData.Key;

                    Series keySeries = new Series()
                    {
                        //20439 L1 1800bar FuelError Mean
                        Name = "{0} L{1} {2}bar {3} {4}".Args(key.Item2, key.Item3, key.Item1, param, statistic),
                        ChartType = SeriesChartType.Line,
                        Color = Color.CornflowerBlue, //TODO:
                    };

                    //Select all the data for this Pressure group & paramter
                    var nomPoints = (from d in groupData
                                     where d.Name == param
                                     orderby d.NominalFuel ascending
                                     select d.NominalFuel).Distinct();

                    foreach (double nfp in nomPoints)
                    {
                        var d = from p in groupData
                                where p.NominalFuel == nfp
                                && p.Name == param
                                orderby p.Data
                                select p;

                        var result = function(d);
                        keySeries.Points.AddXY(nfp, result);

                        if (statistic == "Mean")
                            MeanData.Add(new Tuple<double, double>(nfp, result));
                        else
                            MedianData.Add(new Tuple<double, double>(nfp, result));
                    }

                    yield return keySeries;
                }
            }
        } 

        /// <summary>
        /// Split data by Pressure, then into Plant / Line Numbers
        /// </summary>
        /// <param name="data">The data to split</param>
        /// <returns>Data grouped by Pressure, Plant and Line numbers</returns>
        private IEnumerable<IGrouping<Tuple<short, short, int>, Row>> SplitData(IEnumerable<Row> data)
        {
            return from x in data
                   where
                       SelectedPressureLevels.Contains(x.PressureReq.ToString())
                   group x by new Tuple<short, short, int>(x.PressureReq, x.PlantNumber, x.LineNumber) into plantGroups
                   select plantGroups;
        }

        /// <summary>
        /// Calculates the mean of a dataset
        /// </summary>
        /// <param name="data">The Dataset</param>
        /// <returns>the mean of the dataset</returns>
        private double CalcMean(IEnumerable<Row> data)
        {
            double sum = data.Sum(x => x.Data);
            return sum / data.Count();
        }

        /// <summary>
        /// Calculates the median of a dataset
        /// </summary>
        /// <param name="data">The Dataset</param>
        /// <returns>the median of the dataset</returns>
        private double CalcMedian(IEnumerable<Row> data)
        {
            double median = 0.0d;
            if (data.Count() % 2 == 0)
            {
                median = (
                    data.ElementAt((data.Count() / 2) - 1).Data +
                    data.ElementAt((data.Count() / 2) - 1).Data) / 2.0d;
            }
            else
                median = data.ElementAt(data.Count() / 2).Data;

            return median;
        }
    }
}
