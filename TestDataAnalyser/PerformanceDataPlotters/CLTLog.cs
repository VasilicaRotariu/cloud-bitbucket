﻿using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.CLT
{
    /// <summary>
    /// Performance plotter class for the log of the closed loop data.
    /// </summary>
    public class LogPlotter : PerformanceDataPlotter<Row>
    {
        /// <summary>
        /// Gets or sets a value indicating whether the fuel as error should be shown.
        /// </summary>
        private bool ShowFuelAsError { get; set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="CLT.LogPlotter"/> class.
        /// </summary>
        /// <param name="seriesSelection">Array of flags specifying the series selection.</param>
        /// <param name="selectedKeys">List of selected keys used to select the data to plot.</param>
        /// <param name="selectedPressureLevels">List of selected pressure levels used to select the data to plot.</param>
        /// <param name="selectedParameters">List of selected parameters used to select the data to plot.</param>
        /// <param name="selectedStatistics">List of selected statistics used to select the data to plot.</param>
        /// <param name="chart">The chart and legend to plot the data on.</param>
        /// <param name="gridView">The grid view to add the filtered rows to.</param>
        /// <param name="data">The rows of data used for the plot.</param>
        /// <param name="showFuelAsError">Flag specifying whether fuel should be shown as an error.</param>
        public LogPlotter(
            IEnumerable<bool> seriesSelection,
            UTIDPlantNumberKeys selectedKeys,
            List<string> selectedPressureLevels,
            List<string> selectedParameters,
            List<string> selectedStatistics,
            ChartSplitLegend chart,
            DataGridView gridView,
            List<Row> data,
            bool showFuelAsError)
            : base(seriesSelection, selectedKeys, selectedPressureLevels, selectedParameters, selectedStatistics, chart, true, gridView, data)
        {
            ShowFuelAsError = showFuelAsError;
        }

        /// <summary>
        /// Plots the log closed loop chart and populates the grid view.
        /// </summary>
        public void Plot()
        {
            GridViewColumns = new[] 
            {
                "UTID", "ISN", "PDate", "Global", "LOP", "IDENT", "Testplan", "PressureReq",
                "Name", "Duration", "FuelDemand", "Mean", "Median", "Min", "Max", "SD"
            };

            PlotPerformanceData(
                Data,
                Data.GetUTIDPressureLevelStatisticTree(NoOrder),
                ChartSplitLegend,
                delegate(AddGridViewRowParameters parameters)
                {
                    var row = parameters.Row;

                    // Add data points to datagridview (separate from creating series to prevent duplication of records in datgridview
                    GridView.Rows.Add(
                        row.UTID.ToString(CultureInfo.InvariantCulture), 
                        row.ISN,
                        row.PDate.ToString(CultureInfo.InvariantCulture),
                        row.Global.ToString(CultureInfo.InvariantCulture), 
                        row.LOP, 
                        row.IDENT,
                        row.TestPlan,
                        row.PressureReq.ToString(CultureInfo.InvariantCulture), 
                        row.Name,
                        row.Duration.ToString(CultureInfo.InvariantCulture),
                        row.FuelDemand.ToString(CultureInfo.InvariantCulture),
                        row.Mean.ToString(CultureInfo.InvariantCulture),
                        row.Median.ToString(CultureInfo.InvariantCulture),
                        row.Min.ToString(CultureInfo.InvariantCulture),
                        row.Max.ToString(CultureInfo.InvariantCulture),
                        row.SD.ToString(CultureInfo.InvariantCulture));
                },
                delegate(AddSeriesPointParameters parameters)
                {
                    double plotValue = 0.0;
                    switch (parameters.Statistic)
                    {
                        case "Mean":
                            {
                                plotValue = parameters.Row.Mean;
                            }
                            break;
                        case "Median":
                            {
                                plotValue = parameters.Row.Median;
                            }
                            break;
                        case "Min":
                            {
                                plotValue = parameters.Row.Min;
                            }
                            break;
                        case "Max":
                            {
                                plotValue = parameters.Row.Max;
                            }
                            break;
                    }

                    if (parameters.Statistic == "SD")
                    {
                        parameters.Series.Points.AddXY(parameters.Row.FuelDemand, parameters.Row.SD);
                    }
                    else
                    {
                        if (ShowFuelAsError && ((parameters.Parameter == "Fuel") || (parameters.Parameter == "FuelExpected")))
                        {
                            parameters.Series.Points.AddXY(parameters.Row.FuelDemand, plotValue - parameters.Row.FuelDemand);
                        }
                        else
                        {
                            parameters.Series.Points.AddXY(parameters.Row.FuelDemand, plotValue);
                        }
                    }
                });
        }
    }
}
