﻿using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using System.Linq;
using System.Windows.Forms.DataVisualization.Charting;
using System.Drawing;

namespace SudburyDataAnalyser.Untrimmed
{
    /// <summary>
    /// Plotter for the Untrimmed Data graphs
    /// </summary>
    public sealed class Plotter : PerformanceDataPlotter<Row>
    {
        /// <summary>
        /// Gets the List of pressure Levels
        /// </summary>
        public List<string> PressureLevels { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="Untrimmed.Plotter"/> class.
        /// </summary>
        /// <param name="seriesSelection">Array of flags specifying the series selection.</param>
        /// <param name="selectedKeys">List of selected keys used to select the data to plot.</param>
        /// <param name="selectedPressureLevels">List of selected pressure levels used to select the data to plot.</param>
        /// <param name="selectedParameters">List of selected parameters used to select the data to plot.</param>
        /// <param name="selectedStatistics">List of selected statistics used to select the data to plot.</param>
        /// <param name="chart">The chart and legend to plot the data on.</param>
        /// <param name="gridView">The grid view to add the filtered rows to.</param>
        /// <param name="data">The rows of data used for the plot.</param>
        public Plotter(
            IEnumerable<bool> seriesSelection,
            UTIDPlantNumberKeys selectedKeys,
            List<string> selectedPressureLevels,
            List<string> selectedParameters,
            List<string> selectedStatistics,
            ChartSplitLegend chart,
            DataGridView gridView,
            List<Row> data)
            : base(seriesSelection, selectedKeys, selectedPressureLevels, selectedParameters, selectedStatistics, chart, false, gridView, data)
            
        {
            PressureLevels = selectedPressureLevels;
        }
        
        /// <summary>
        /// Plots the Untrimmed chart and populates the grid view.
        /// </summary>
        public void Plot()
        {
            GridViewColumns = new[] 
            {
                "UTID", "PlantNumber", "Name", "PressureReq", "Logic", "Nominal Value", "Offset", "Untrimmed Value"
            };
            
            PlotPerformanceData(
                Data,
                Data.GetUTIDPressureLevelStatisticTree(NoOrder),
                ChartSplitLegend,
                addRowParam =>
                {
                    var row = addRowParam.Row;

                    //Add data points to the datagridView
                    GridView.Rows.Add(
                        row.UTID,
                        row.PlantNumber,
                        row.Name,
                        row.PressureReq,
                        row.Logic,
                        row.NominalValue,
                        row.Offset,
                        row.UntrimmedValue);
                },
                addPointParam =>
                {
                    //Add point to the graph
                    addPointParam.Series.Points.AddXY(addPointParam.Row.Logic, addPointParam.Row.UntrimmedValue);
                });

            if (Data.Count == 0)
                return; //No Data, so nothing to draw

            //Create a nominal series for each pressure level foreach UTID
            var utid = (from d in Data.ToList<Row>() select d.UTID).Distinct().First();

            //foreach(var utid in uniqueUTID)
           // {
                var uniquepressureLevels = (from d in Data
                                           where d.UTID == utid && PressureLevels.Contains(d.PressureReq.ToString())
                                           select d.PressureReq).Distinct();

                foreach (var p in uniquepressureLevels)
                {
                    Series nominalSeries = new Series("{0} {1}bar Nominal".Args(utid, p));
                    nominalSeries.Color = Color.Black;

                    var points = from d in Data
                                 where d.UTID == utid && d.PressureReq == p
                                 select d;

                    foreach (var point in points)
                    {
                        nominalSeries.Points.AddXY(point.Logic, point.NominalValue);
                    }

                    nominalSeries.ChartType = SeriesChartType.Line;
                    ChartSplitLegend.Add(nominalSeries);
                } 
 
            //}
        }
    }
}
