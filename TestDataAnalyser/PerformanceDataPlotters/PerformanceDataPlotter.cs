﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using SudburyDataAnalyser.LimitsBase;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Base class for all performance data plotters.
    /// Performance data plotters 
    /// </summary>
    /// <typeparam name="T">Type of row data to be plotted.</typeparam>
    public class PerformanceDataPlotter<T> where T : UTIDRow
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="PerformanceDataPlotter{T}"/> class.
        /// </summary>
        /// <param name="seriesSelection">Array of flags specifying the series selection.</param>
        /// <param name="selectedKeys">List of selected keys used to select the data to plot.</param>
        /// <param name="selectedPressureLevels">List of selected pressure levels used to select the data to plot.</param>
        /// <param name="selectedParameters">List of selected parameters used to select the data to plot.</param>
        /// <param name="selectedStatistics">List of selected statistics used to select the data to plot.</param>
        /// <param name="chart">The chart to plot the data on.</param>
        /// <param name="legend">The legend chart to display the legend on.</param>
        /// <param name="logarithmic">Flag indicating if the plot is logarithmic.</param>
        /// <param name="gridView">The grid view to add the filtered rows to.</param>
        /// <param name="data">The rows of data used for the plot.</param>
        public PerformanceDataPlotter(
            IEnumerable<bool> seriesSelection,
            UTIDPlantNumberKeys selectedKeys, 
            List<string> selectedPressureLevels,
            List<string> selectedParameters,
            List<string> selectedStatistics,
            Chart chart,
            ChartLegend legend,
            bool logarithmic,
            DataGridView gridView,
            List<T> data)
        {
            SelectedKeys = selectedKeys;
            SelectedPressureLevels = selectedPressureLevels;
            SelectedParameters = selectedParameters;
            SelectedStatistics = selectedStatistics;

            Chart = chart;
            Legend = legend;
            Logarithmic = logarithmic;
            GridView = gridView;
            Data = data;

            var seriesColourSelection = SeriesColourSelection.passFail;
            foreach (var val in seriesSelection)
            {
                if (val)
                {
                    ColourSelection = seriesColourSelection;
                    return;
                }

                seriesColourSelection++;
            }
            ColourSelection = SeriesColourSelection.none;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="PerformanceDataPlotter{T}"/> class.
        /// </summary>
        /// <param name="seriesSelection">Array of flags specifying the series selection.</param>
        /// <param name="selectedKeys">List of selected keys used to select the data to plot.</param>
        /// <param name="selectedPressureLevels">List of selected pressure levels used to select the data to plot.</param>
        /// <param name="selectedParameters">List of selected parameters used to select the data to plot.</param>
        /// <param name="selectedStatistics">List of selected statistics used to select the data to plot.</param>
        /// <param name="chart">The chart and legend to plot the data on.</param>
        /// <param name="logarithmic">Flag indicating if the plot is logarithmic.</param>
        /// <param name="gridView">The grid view to add the filtered rows to.</param>
        /// <param name="data">The rows of data used for the plot.</param>
        public PerformanceDataPlotter(
            IEnumerable<bool> seriesSelection,
            UTIDPlantNumberKeys selectedKeys,
            List<string> selectedPressureLevels,
            List<string> selectedParameters,
            List<string> selectedStatistics,
            ChartSplitLegend chart,
            bool logarithmic,
            DataGridView gridView,
            List<T> data)
        {
            SelectedKeys = selectedKeys;
            SelectedPressureLevels = selectedPressureLevels;
            SelectedParameters = selectedParameters;
            SelectedStatistics = selectedStatistics;

            AddDummyIfEmpty(SelectedPressureLevels);
            AddDummyIfEmpty(SelectedStatistics);

            ChartSplitLegend = chart;
            Logarithmic = logarithmic;
            GridView = gridView;
            Data = data;

            var seriesColourSelection = SeriesColourSelection.passFail;
            foreach (var val in seriesSelection)
            {
                if (val)
                {
                    ColourSelection = seriesColourSelection;
                    return;
                }

                seriesColourSelection++;
            }
            ColourSelection = SeriesColourSelection.none;
        }

        /// <summary>
        /// Gets the chart split legend control.
        /// </summary>
        protected ChartSplitLegend ChartSplitLegend { get; private set; }

        /// <summary>
        /// Gets the chart.
        /// </summary>
        protected Chart Chart { get; private set; }

        /// <summary>
        /// Gets the legend chart.
        /// </summary>
        protected ChartLegend Legend { get; private set; }

        /// <summary>
        /// Gets the data grid view.
        /// </summary>
        protected DataGridView GridView { get; private set; }

        /// <summary>
        /// Gets or sets the rows of data.
        /// </summary>
        protected List<T> Data { get; set; }

        /// <summary>
        /// Gets or sets the grid view column titles.
        /// </summary>
        protected string[] GridViewColumns { get; set; }

        /// <summary>
        /// Gets or sets the selected pressure levels.
        /// </summary>
        protected List<string> SelectedPressureLevels { get; set; }

        /// <summary>
        /// Gets or sets the selected parameters.
        /// </summary>
        protected List<string> SelectedParameters { get; set; }

        /// <summary>
        /// Gets or sets the selected statistics.
        /// </summary>
        protected List<string> SelectedStatistics { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the plot is logarithmic.
        /// </summary>
        private bool Logarithmic { get; set; }

        /// <summary>
        /// Gets or sets the selected keys.
        /// </summary>
        private UTIDPlantNumberKeys SelectedKeys { get; set; }

        /// <summary>
        /// Enumeration used for a series colour.
        /// </summary>
        private enum SeriesColourSelection
        {
            // ReSharper disable InconsistentNaming
            passFail,
            UTID,
            LOP,
            ISN,
            IDENT,
            testPlan,
            Line,
            PlantNumber,
            none
            // ReSharper restore InconsistentNaming
        }

        /// <summary>
        /// Gets or sets the series colour.
        /// </summary>
        private SeriesColourSelection ColourSelection { get; set; }

        /// <summary>
        /// Class used to encapsulate a row so that a row can be passed as a
        /// parameter to an action. Passing an anonymous row is not allowed.
        /// </summary>
        protected class AddGridViewRowParameters
        {
            /// <summary>
            /// Initialises a new instance of the <see cref="AddGridViewRowParameters"/> class.
            /// </summary>
            /// <param name="row">The row to initialise the instance with.</param>
            public AddGridViewRowParameters(T row)
            {
                Row = row;
            }

            /// <summary>
            /// Gets the row.
            /// </summary>
            public T Row { get; private set; }
        }

        /// <summary>
        /// Class used to encapsulate the create series parameters.
        /// </summary>
        protected class SeriesParameters
        {
            /// <summary>
            /// Initialises a new instance of the <see cref="SeriesParameters"/> class.
            /// </summary>
            /// <param name="utid">The UTID value used to colour the series.</param>
            /// <param name="pressureLevel">The pressure level value used to colour the series.</param>
            /// <param name="parameter">The parameter value used to colour the series.</param>
            /// <param name="statistic">The statistic value used to colour the series.</param>
            /// <param name="global">The global value used to colour the series.</param>
            /// <param name="isn">The ISN value used to colour the series.</param>
            /// <param name="lop">The LOP value used to colour the series.</param>
            /// <param name="ident">The IDENT value used to colour the series.</param>
            /// <param name="testPlan">The test plan value used to colour the series.</param>
            /// <param name="line">The line value used to colour the series.</param>
            /// <param name="plantNumber">The plant number value used to colour the series.</param>
            /// <param name="utidIndex">The UTID index value used to colour the series.</param>
            /// <param name="isnIndex">The ISN index value used to colour the series.</param>
            /// <param name="lopIndex">The LOP index value used to colour the series.</param>
            /// <param name="identIndex">The IDENT index value used to colour the series.</param>
            /// <param name="testPlanIndex">The test plan index value used to colour the series.</param>
            /// <param name="lineIndex">The line index value used to colour the series.</param>
            /// <param name="plantNumberIndex">The plant number index used to colour the series.</param>
            public SeriesParameters(
                int utid, 
                short pressureLevel, 
                string parameter, 
                string statistic, 
                int global, 
                string isn, 
                string lop,
                string ident, 
                string testPlan,
                Tuple<int, short> line,
                short plantNumber,
                int utidIndex, 
                int isnIndex, 
                int lopIndex, 
                int identIndex, 
                int testPlanIndex,
                int lineIndex,
                int plantNumberIndex)
            {
                UTID = utid;
                PressureLevel = pressureLevel;
                Parameter  = parameter;
                Statistic = statistic;
                Global  = global;
                ISN = isn;
                LOP = lop;
                IDENT = ident;
                TestPlan = testPlan;
                Line = line;
                PlantNumber = plantNumber;
                UTIDIndex = utidIndex;
                ISNIndex = isnIndex;
                LOPIndex = lopIndex;
                IDENTIndex = identIndex;
                TestPlanIndex = testPlanIndex;
                LineIndex = lineIndex;
                PlantNumberIndex = plantNumberIndex;
            }

            /// <summary>
            /// Gets the UTID value used to create the series.
            /// </summary>
            // ReSharper disable once InconsistentNaming
            public int UTID { get; private set; }

            /// <summary>
            /// Gets the pressure level value used to create the series.
            /// </summary>
            public short PressureLevel { get; private set; }

            /// <summary>
            /// Gets the parameter value used to create the series.
            /// </summary>
            public string Parameter { get; private set; }

            /// <summary>
            /// Gets the statistic value used to create the series.
            /// </summary>
            public string Statistic { get; private set; }

            /// <summary>
            /// Gets the global value used to create the series.
            /// </summary>
            public int Global { get; private set; }

            /// <summary>
            /// Gets the ISN value used to create the series.
            /// </summary>
            // ReSharper disable once InconsistentNaming
            public string ISN { get; private set; }

            /// <summary>
            /// Gets the LOP value used to create the series.
            /// </summary>
            // ReSharper disable once InconsistentNaming
            public string LOP { get; private set; }

            /// <summary>
            /// Gets the IDENT value used to create the series.
            /// </summary>
            // ReSharper disable once InconsistentNaming
            public string IDENT { get; private set; }

            /// <summary>
            /// Gets the test plan value used to create the series.
            /// </summary>
            public string TestPlan { get; private set; }

            /// <summary>
            /// Gets the line value used to create the series.
            /// </summary>
            public Tuple<int, short> Line { get; private set; }

            /// <summary>
            /// Gets the rig value used to create the series.
            /// </summary>
            public int PlantNumber { get; private set; }

            /// <summary>
            /// Gets the UTID index value used to create the series.
            /// </summary>
            // ReSharper disable once InconsistentNaming
            public int UTIDIndex { get; private set; }

            /// <summary>
            /// Gets the ISN index value used to create the series.
            /// </summary>
            // ReSharper disable once InconsistentNaming
            public int ISNIndex { get; private set; }

            /// <summary>
            /// Gets the LOP index value used to create the series.
            /// </summary>
            // ReSharper disable once InconsistentNaming
            public int LOPIndex { get; private set; }

            /// <summary>
            /// Gets the IDENT index value used to create the series.
            /// </summary>
            // ReSharper disable once InconsistentNaming
            public int IDENTIndex { get; private set; }

            /// <summary>
            /// Gets the test plan index value used to create the series.
            /// </summary>
            public int TestPlanIndex { get; private set; }

            /// <summary>
            /// Gets the line index value used to create the series.
            /// </summary>
            public int LineIndex { get; private set; }

            /// <summary>
            /// Gets the plant number index value used to create the series.
            /// </summary>
            public int PlantNumberIndex { get; private set; }
        }

        /// <summary>
        /// Class used to encapsulate the series point parameters.
        /// </summary>
        protected class AddSeriesPointParameters
        {
            /// <summary>
            /// Initialises a new instance of the <see cref="AddSeriesPointParameters"/> class.
            /// </summary>
            /// <param name="row">The row to add to the series.</param>
            /// <param name="utid">The UTID value used when adding the row to the series.</param>
            /// <param name="pressureLevel">The pressure level used when add the row to the series.</param>
            /// <param name="parameter">The parameter used when add the row to the series.</param>
            /// <param name="statistic">The statistic used when add the row to the series.</param>
            /// <param name="index">The index used when add the row to the series.</param>
            /// <param name="series">The series to add the row too.</param>
            public AddSeriesPointParameters(T row, string utid, string pressureLevel, string parameter, string statistic, int index, Series series)
            {
                Row = row;
                UTID = utid;
                PressureLevel = pressureLevel;
                Parameter = parameter;
                Statistic = statistic;
                Index = index;
                Series = series;
            }

            /// <summary>
            /// Gets the row to add to the series.
            /// </summary>
            public T Row { get; private set; }

            /// <summary>
            /// Gets the UTID value used when adding the row to the series.
            /// </summary>
            // ReSharper disable once InconsistentNaming
            public string UTID { get; private set; }

            /// <summary>
            /// Gets the pressure level value used when adding the row to the series.
            /// </summary>
            public string PressureLevel { get; private set; }

            /// <summary>
            /// Gets the parameter value used when adding the row to the series.
            /// </summary>
            public string Parameter { get; private set; }

            /// <summary>
            /// Gets the statistic value used when adding the row to the series.
            /// </summary>
            public string Statistic { get; private set; }

            /// <summary>
            /// Gets the index value used when adding the row to the series.
            /// </summary>
            public int Index { get; private set; }

            /// <summary>
            /// Gets the series the row is to be added too.
            /// </summary>
            public Series Series { get; private set; }
        }

        /// <summary>
        /// Plots the performance data onto the chart by adding series and adding these series
        /// to the chart.
        /// </summary>
        /// <param name="rows">List of rows of data to be added to the chart.</param>
        /// <param name="tree">The rows of data in a tree structure for fast access.</param>
        /// <param name="limitsQuery">The query used to determine the limits.</param>
        /// <param name="chart">The chart to add the series too.</param>
        /// <param name="legend">The legend chart to display the legend on.</param>
        /// <param name="addGridViewRow">The method to call to add rows to the grid view.</param>
        /// <param name="createSeries">The method to call to create a new series.</param>
        /// <param name="addSeriesData">The method to call to add the points to the series based on the parameters.</param>
        protected void PlotPerformanceData(
            List<T> rows,
            RowsTree<T, UTIDPlantNumberKey, string, string> tree,
            Query limitsQuery,
            Chart chart,
            ChartLegend legend,
            Action<AddGridViewRowParameters> addGridViewRow,
            Func<SeriesParameters, Series> createSeries,
            Action<AddSeriesPointParameters> addSeriesData)
        {
            // Clear existing data from the chart and grid view
            chart.ChartAreas[0].AxisX.IsLogarithmic = false;
            chart.Series.Clear();

            if (GridView != null)
            {
                GridView.Rows.Clear();
                GridView.Columns.Clear();

                // Add the columns to the grid view
                GridView.Columns.AddMultipleColumns(GridViewColumns);
            }

            // ReSharper disable InconsistentNaming
            // Get list of unique utids, ISNs, LOPs & Testplans for grouping series later
            var uniqueutids = rows.Uniqueutids(SelectedKeys, SelectedPressureLevels, SelectedParameters);
            var uniqueISNs = rows.UniqueISNs(SelectedKeys, SelectedPressureLevels, SelectedParameters);
            var uniqueLOPs = rows.UniqueLOPs(SelectedKeys, SelectedPressureLevels, SelectedParameters);
            var uniqueIDENTs = rows.UniqueIDENTs(SelectedKeys, SelectedPressureLevels, SelectedParameters);
            var uniqueTestplans = rows.UniqueTestPlans(SelectedKeys, SelectedPressureLevels, SelectedParameters);
            var uniqueLines = rows.UniqueLines(SelectedKeys, SelectedPressureLevels, SelectedParameters);
            var uniqueRigs = rows.UniqueRigs(SelectedKeys, SelectedPressureLevels, SelectedParameters);

            //// ReSharper enable InconsistentNaming

            foreach (var key in SelectedKeys)
            {
                foreach (var pressureLevel in SelectedPressureLevels)
                {
                    foreach (var parameter in SelectedParameters)
                    {
                        var newSeriesData = tree.Get(key, pressureLevel, parameter);

                        // If there is no data for given injector for this pressure level & parameter, skip to the next, otherwise carry on plotting
                        if (!newSeriesData.Any()) continue;

                        var firstRow = newSeriesData.First();

                        if (GridView != null)
                        {
                            foreach (var row in newSeriesData)
                            {
                                addGridViewRow(new AddGridViewRowParameters(row));
                            }
                        }

                        // Generate series for each statistic, parameter, pressure level, utid
                        var index = 0;
                        foreach (var statistic in SelectedStatistics)
                        {
                            var seriesParameters = new SeriesParameters(
                                key.UTID,
                                (short)pressureLevel.ToInt(),
                                parameter,
                                statistic,
                                firstRow.Global,
                                firstRow.ISN,
                                firstRow.LOP,
                                firstRow.IDENT,
                                firstRow.TestPlan,
                                new Tuple<int, short>(firstRow.LineNumber, firstRow.PlantNumber),
                                firstRow.PlantNumber,
                                uniqueutids.IndexOf(firstRow.UTID),
                                uniqueISNs.IndexOf(firstRow.ISN),
                                uniqueLOPs.IndexOf(firstRow.LOP),
                                uniqueIDENTs.IndexOf(firstRow.IDENT),
                                uniqueTestplans.IndexOf(firstRow.TestPlan), //uniqueLines.IndexOf(new Tuple<int, short>(firstRow.LineNumber, firstRow.PlantNumber)),                                
                                uniqueLines.IndexOf(firstRow.LineNumber),
                                uniqueRigs.IndexOf(firstRow.PlantNumber));

                            var series = createSeries(seriesParameters);

                            // Add data points to series
                            foreach (var row in newSeriesData)
                            {
                                addSeriesData(new AddSeriesPointParameters(row, key.UTID.ToString(CultureInfo.InvariantCulture), pressureLevel, parameter, statistic, index, series));
                                ++index;
                            }

                            chart.Series.Add(series);
                            if (legend != null)
                            {
                                legend.Add(createSeries(seriesParameters));
                            }
                        }
                    }
                }
            }

            if ((limitsQuery != null) && (SelectedPressureLevels.Count == 1) && (uniqueLOPs.Count == 1) && (SelectedParameters.Count == 1) && (SelectedParameters.First() == "FuelError"))
            {
                limitsQuery.Pressure = SelectedPressureLevels.First();
                limitsQuery.LOP = uniqueLOPs.First();

                var limitsRows = limitsQuery.Execute();

                if (limitsRows.Count > 0)
                {
                    limitsRows.Sort();

                    chart.Series.Add(CreateLowerLimitsSeries(limitsRows));
                    chart.Series.Add(CreateUpperLimitsSeries(limitsRows));
                }
            }

            if (Logarithmic)
            {
                chart.ChartAreas[0].AxisX.IsLogarithmic = true;
                chart.ChartAreas[0].AxisX.MinorGrid.Interval = 1;
                chart.ChartAreas[0].AxisX.MinorTickMark.Interval = 1;
            }
            else
            {
                ChartAxisScaling.AxisScaler.ScaleAxis(Chart.ChartAreas[0], Chart.Name, uniqueLOPs);
            }

            chart.ChartAreas[0].CursorX.IsUserSelectionEnabled = chart.ChartAreas[0].CursorY.IsUserSelectionEnabled = true;
            chart.Visible = true;
        }

        /// <summary>
        /// Plots the performance data onto the chart by adding series and adding these series
        /// to the chart.
        /// </summary>
        /// <param name="rows">List of rows of data to be added to the chart.</param>
        /// <param name="tree">The rows of data in a tree structure for fast access.</param>
        /// <param name="limitsQuery">The query used to determine the limits.</param>
        /// <param name="chart">The chart to add the series too.</param>
        /// <param name="legend">The legend chart to display the legend on.</param>
        /// <param name="addGridViewRow">The method to call to add rows to the grid view.</param>
        /// <param name="addSeriesData">The method to call to add the points to the series based on the parameters.</param>
        protected void PlotPerformanceData(
            List<T> rows,
            RowsTree<T, UTIDPlantNumberKey, string, string> tree,
            Query limitsQuery,
            Chart chart,
            ChartLegend legend,
            Action<AddGridViewRowParameters> addGridViewRow,
            Action<AddSeriesPointParameters> addSeriesData)
        {
            PlotPerformanceData(rows, tree, limitsQuery, chart, legend, addGridViewRow, CreateIndividualChartsSeriesDefault, addSeriesData);
        }

        /// <summary>
        /// Plots the performance data onto the chart by adding series and adding these series
        /// to the chart.
        /// </summary>
        /// <param name="rows">List of rows of data to be added to the chart.</param>
        /// <param name="tree">The rows of data in a tree structure for fast access.</param>
        /// <param name="limitsQuery">The query used to determine the limits.</param>
        /// <param name="chart">The chart and legend to add the series too.</param>
        /// <param name="addGridViewRow">The method to call to add rows to the grid view.</param>
        /// <param name="createSeries">The method to call to create a new series.</param>
        /// <param name="addSeriesData">The method to call to add the points to the series based on the parameters.</param>
        protected void PlotPerformanceData(
            List<T> rows,
            RowsTree<T, UTIDPlantNumberKey, string, string> tree,
            Query limitsQuery,
            ChartSplitLegend chart,
            Action<AddGridViewRowParameters> addGridViewRow,
            Func<SeriesParameters, Series> createSeries,
            Action<AddSeriesPointParameters> addSeriesData)
        {
            // Clear existing data from the chart and grid view
            chart.Clear();

            if (GridView != null)
            {
                GridView.Rows.Clear();
                GridView.Columns.Clear();

                // Add the columns to the grid view
                GridView.Columns.AddMultipleColumns(GridViewColumns);
            }

            // Get list of unique utids, ISNs, LOPs & Testplans for grouping series later
            var uniqueutids = rows.Uniqueutids(SelectedKeys, SelectedPressureLevels, SelectedParameters);
            var uniqueISNs = rows.UniqueISNs(SelectedKeys, SelectedPressureLevels, SelectedParameters);
            var uniqueLOPs = rows.UniqueLOPs(SelectedKeys, SelectedPressureLevels, SelectedParameters);
            var uniqueIDENTs = rows.UniqueIDENTs(SelectedKeys, SelectedPressureLevels, SelectedParameters);
            var uniqueTestplans = rows.UniqueTestPlans(SelectedKeys, SelectedPressureLevels, SelectedParameters);
            var uniqueLines = rows.UniqueLines(SelectedKeys, SelectedPressureLevels, SelectedParameters);
            var uniqueRigs = rows.UniqueRigs(SelectedKeys, SelectedPressureLevels, SelectedParameters);

            foreach (var key in SelectedKeys)
            {
                foreach (var pressureLevel in SelectedPressureLevels)
                {
                    foreach (var parameter in SelectedParameters)
                    {
                        var newSeriesData = tree.Get(key, pressureLevel, parameter);

                        // If there is no data for given injector for this pressure level & parameter, skip to the next, otherwise carry on plotting
                        if (!newSeriesData.Any()) continue;

                        var firstRow = newSeriesData.First();

                        if (GridView != null)
                        {
                            foreach (var row in newSeriesData)
                            {
                                addGridViewRow(new AddGridViewRowParameters(row));
                            }
                        }

                        // Generate series for each statistic, parameter, pressure level, utid
                        var index = 0;
                        foreach (string statistic in SelectedStatistics)
                        {
                            var seriesParameters = new SeriesParameters(
                                key.UTID,
                                (short)pressureLevel.ToInt(),
                                parameter,
                                statistic,
                                firstRow.Global,
                                firstRow.ISN,
                                firstRow.LOP,
                                firstRow.IDENT,
                                firstRow.TestPlan,
                                new Tuple<int, short>(firstRow.LineNumber, firstRow.PlantNumber),
                                firstRow.PlantNumber,
                                uniqueutids.IndexOf(firstRow.UTID),
                                uniqueISNs.IndexOf(firstRow.ISN),
                                uniqueLOPs.IndexOf(firstRow.LOP),
                                uniqueIDENTs.IndexOf(firstRow.IDENT),
                                uniqueTestplans.IndexOf(firstRow.TestPlan), //uniqueLines.IndexOf(new Tuple<int, short>(firstRow.LineNumber, firstRow.PlantNumber)),
                                uniqueLines.IndexOf(firstRow.LineNumber),
                                uniqueRigs.IndexOf(firstRow.PlantNumber));

                            var series = createSeries(seriesParameters);

                            // Add data points to series
                            foreach (var row in newSeriesData)
                            {
                                addSeriesData(new AddSeriesPointParameters(row, key.UTID.ToString(CultureInfo.InvariantCulture), pressureLevel, parameter, statistic, index, series));
                                ++index;
                            }

                            chart.Add(series);
                        }
                    }
                }
            }

            if ((limitsQuery != null) && (SelectedPressureLevels.Count == 1) && (uniqueLOPs.Count == 1) && (SelectedParameters.Count == 1) && (SelectedParameters.First() == "FuelError"))
            {
                limitsQuery.Pressure = SelectedPressureLevels.First();
                limitsQuery.LOP = uniqueLOPs.First();

                List<Row> limitsRows = limitsQuery.Execute();

                if (limitsRows.Count > 0)
                {
                    limitsRows.Sort();

                    chart.Add(CreateLowerLimitsSeries(limitsRows));
                    chart.Add(CreateUpperLimitsSeries(limitsRows));
                }
            }

            if (Logarithmic)
            {
                chart.Logarithmic = true;
            }
            else
            {
                chart.ScaleAxis(uniqueLOPs.Union(SelectedPressureLevels).ToList());
            }
        }

        /// <summary>
        /// Plots the performance data onto the chart by adding series and adding these series
        /// to the chart.
        /// </summary>
        /// <param name="rows">List of rows of data to be added to the chart.</param>
        /// <param name="tree">The rows of data in a tree structure for fast access.</param>
        /// <param name="limitsQuery">The query used to determine the limits.</param>
        /// <param name="chart">The chart and legend to add the series too.</param>
        /// <param name="addGridViewRow">The method to call to add rows to the grid view.</param>
        /// <param name="addSeriesData">The method to call to add the points to the series based on the parameters.</param>
        protected void PlotPerformanceData(
            List<T> rows,
            RowsTree<T, UTIDPlantNumberKey, string, string> tree,
            Query limitsQuery,
            ChartSplitLegend chart,
            Action<AddGridViewRowParameters> addGridViewRow,
            Action<AddSeriesPointParameters> addSeriesData)
        {
            PlotPerformanceData(rows, tree, limitsQuery, chart, addGridViewRow, CreateChartSeriesDefault, addSeriesData);
        }

        /// <summary>
        /// Plots the performance data onto the chart by adding series and adding these series
        /// to the chart.
        /// </summary>
        /// <param name="rows">List of rows of data to be added to the chart.</param>
        /// <param name="tree">The rows of data in a tree structure for fast access.</param>
        /// <param name="chart">The chart and legend to add the series too.</param>
        /// <param name="addGridViewRow">The method to call to add rows to the grid view.</param>
        /// <param name="createSeries">The method to call to create a new series.</param>
        /// <param name="addSeriesData">The method to call to add the points to the series based on the parameters.</param>
        protected void PlotPerformanceData(
            List<T> rows,
            RowsTree<T, UTIDPlantNumberKey, string, string> tree,
            ChartSplitLegend chart,
            Action<AddGridViewRowParameters> addGridViewRow,
            Func<SeriesParameters, Series> createSeries,
            Action<AddSeriesPointParameters> addSeriesData)
        {
            PlotPerformanceData(rows, tree, null, chart, addGridViewRow, createSeries, addSeriesData);
        }

        /// <summary>
        /// Plots the performance data onto the chart by adding series and adding these series
        /// to the chart.
        /// </summary>
        /// <param name="rows">List of rows of data to be added to the chart.</param>
        /// <param name="tree">The rows of data in a tree structure for fast access.</param>
        /// <param name="chart">The chart and legend to add the series too.</param>
        /// <param name="addGridViewRow">The method to call to add rows to the grid view.</param>
        /// <param name="addSeriesData">The method to call to add the points to the series based on the parameters.</param>
        protected void PlotPerformanceData(
            List<T> rows,
            RowsTree<T, UTIDPlantNumberKey, string, string> tree,
            ChartSplitLegend chart,
            Action<AddGridViewRowParameters> addGridViewRow,
            Action<AddSeriesPointParameters> addSeriesData)
        {
            PlotPerformanceData(rows, tree, chart, addGridViewRow, CreateChartSeriesDefault, addSeriesData);
        }

        /// <summary>
        /// Default method for creating a series.
        /// </summary>
        /// <param name="param">Parameters required to create a series.</param>
        /// <returns>A newly created series.</returns>
        private Series CreateChartSeriesDefault(SeriesParameters param)
        {
            var series = new Series(string.Format("{0} {1}bar {2} {3}", param.UTID, param.PressureLevel, param.Parameter, param.Statistic))
            {
                ChartType = SeriesChartType.Line,
                Color = GetSeriesColour(param)
            };

            series.Name += GetSeriesNamePostFix(param.ISN, param.LOP, param.IDENT, param.TestPlan, param.Line, param.PlantNumber);
            series.BorderWidth = 2;
            return series;
        }

        /// <summary>
        /// Default method for creating a series for an individual charts legend.
        /// </summary>
        /// <param name="param">Parameters required to create a series.</param>
        /// <returns>A newly created series.</returns>
        private Series CreateIndividualChartsSeriesDefault(SeriesParameters param)
        {
            var series = new Series(string.Format("{0} {1} {2}", param.UTID, param.Parameter, param.Statistic))
            {
                ChartType = SeriesChartType.Line,
                Color = GetSeriesColour(param)
            };

            series.Name += GetSeriesNamePostFix(param.ISN, param.LOP, param.IDENT, param.TestPlan, param.Line, param.PlantNumber);
            series.BorderWidth = 2;
            return series;
        }

        /// <summary>
        /// Creates a series to show the lower limits.
        /// </summary>
        /// <param name="rows">List of limits rows.</param>
        /// <returns>The series showing the lower limits.</returns>
        private static Series CreateLowerLimitsSeries(IEnumerable<Row> rows)
        {
            var series = new Series
            {
                ChartType = SeriesChartType.Line,
                Color = Color.Black,
                IsVisibleInLegend = false,
                BorderWidth = 2
            }; 

            foreach (var row in rows)
            {
                series.Points.AddXY(row.FuelRange.Min, row.Min.Min);
                if (!row.FuelRange.Min.Approx(row.FuelRange.Max) || !row.Min.Min.Approx(row.Min.Max))
                {
                    series.Points.AddXY(row.FuelRange.Max, row.Min.Max);
                }
            }

            return series;
        }

        /// <summary>
        /// Creates a series to show the upper limits.
        /// </summary>
        /// <param name="rows">List of limits rows.</param>
        /// <returns>The series showing the upper limits.</returns>
        private static Series CreateUpperLimitsSeries(IEnumerable<Row> rows)
        {
            var series = new Series
            {
                ChartType = SeriesChartType.Line,
                Color = Color.Black,
                IsVisibleInLegend = false,
                BorderWidth = 2
            };

            foreach (var row in rows)
            {
                series.Points.AddXY(row.FuelRange.Min, row.Max.Min);
                if (!row.FuelRange.Min.Approx(row.FuelRange.Max) || !row.Max.Min.Approx(row.Max.Max))
                {
                    series.Points.AddXY(row.FuelRange.Max, row.Max.Max);
                }
            }

            return series;
        }

        /// <summary>
        /// No order method used for not ordering a list of rows.
        /// </summary>
        /// <param name="rows">The rows that will not be reordered.</param>
        /// <returns>The unordered rows.</returns>
        protected List<T> NoOrder(List<T> rows)
        {
            return rows;
        }

        /// <summary>
        /// Returns a series colour based on the current colour selection.
        /// </summary>
        /// <param name="param">The create chart series parameters used to determine the series colour.</param>
        /// <returns>The series colour.</returns>
        protected Color GetSeriesColour(SeriesParameters param)
        {
            switch (ColourSelection)
            {
                case SeriesColourSelection.passFail: return SeriesColour.PassFailCustomPalette(param.Global);
                case SeriesColourSelection.UTID: return SeriesColour.BetterColours(param.UTIDIndex);
                case SeriesColourSelection.ISN: return SeriesColour.BetterColours(param.ISNIndex);
                case SeriesColourSelection.LOP: return SeriesColour.BetterColours(param.LOPIndex);
                case SeriesColourSelection.IDENT: return SeriesColour.BetterColours(param.IDENTIndex);
                case SeriesColourSelection.testPlan: return SeriesColour.BetterColours(param.TestPlanIndex);
                case SeriesColourSelection.Line: return SeriesColour.BetterColours(param.LineIndex);
                case SeriesColourSelection.PlantNumber: return SeriesColour.BetterColours(param.PlantNumberIndex);
            }

            return new Color();
        }

        /// <summary>
        /// Returns a series name post fix.
        /// </summary>
        /// <param name="isn">The ISN.</param>
        /// <param name="lop">The LOP.</param>
        /// <param name="ident">The IDENT.</param>
        /// <param name="testPlan">The test plan.</param>
        /// <param name="line">The line.</param>
        /// <param name="plantNumber">The plant number.</param>
        /// <returns>The series name post fix.</returns>
        protected string GetSeriesNamePostFix(string isn, string lop, string ident, string testPlan, Tuple<int, short> line, int plantNumber)
        {
            switch (ColourSelection)
            {
                case SeriesColourSelection.ISN: return string.Format(" ({0})", isn);
                case SeriesColourSelection.LOP: return string.Format(" ({0})", lop);
                case SeriesColourSelection.IDENT: return string.Format(" ({0})", ident);
                case SeriesColourSelection.testPlan: return string.Format(" ({0})", testPlan);
                case SeriesColourSelection.Line: return string.Format(" ({0} {1})", line.Item1, line.Item2);
                case SeriesColourSelection.PlantNumber: return string.Format(" ({0})", plantNumber);
            }

            return string.Empty;
        }

        /// <summary>
        /// Adds a dummy item to a list if the list is null or empty.
        /// </summary>
        /// <param name="list">The list.</param>
        private static void AddDummyIfEmpty(List<string> list)
        {
            if (list == null)
            {
                list = new List<string>();
            }

            if (list.Count == 0)
            {
                list.Add("dummy");
            }
        }
    }
}
