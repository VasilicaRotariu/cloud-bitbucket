﻿using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Provides methods to extend the functionality of the Chart class.
    /// </summary>
    public static class ChartExt
    {
        /// <summary>
        /// Clears the series data of the supplied charts.
        /// </summary>
        /// <param name="charts">An array of charts to clear.</param>
        public static void ClearCharts(params Chart[] charts)
        {
            foreach (var chart in charts)
            {
                chart.Series.Clear();
            }
        }

        /// <summary>
        /// Adds a legend to the chart if the chart does not already contain a legend.
        /// </summary>
        /// <param name="chart">The chart to add a legend too.</param>
        /// <returns>The legend added to the chart or null.</returns>
        public static Legend AddLegendIfAbsent(this Chart chart)
        {
            if (chart.Legends.Count > 0) return null;

            var legend = new Legend("temp");
            chart.Legends.Add(legend);

            return legend;
        }

        /// <summary>
        /// Removes a legend from a chart. If the legend is null, nothing happens.
        /// </summary>
        /// <param name="chart">The chart to remove the legend from.</param>
        /// <param name="legend">The legend to remove from the chart.</param>
        public static void RemoveLegend(this Chart chart, Legend legend)
        {
            if (legend != null)
            {
                chart.Legends.Remove(legend);
            }
        }

        /// <summary>
        /// Copies the image of a chart to the clipboard.
        /// </summary>
        /// <param name="chart">The chart to copy to the clipboard.</param>
        public static void CopyToClipboard(this Chart chart)
        {
            using (var ms = new MemoryStream())
            {
                chart.SaveImage(ms, ChartImageFormat.Bmp);
                Clipboard.SetImage(new Bitmap(ms));
            }
        }

        /// <summary>
        /// Opens the chart in a big chart.
        /// </summary>
        /// <param name="chart">The chart to open in a big chart.</param>
        public static void OpenInBigChart(this Chart chart)
        {
            // Save to and load from a .NET Stream object.
            var memoryStream = new MemoryStream();

            var legend = chart.AddLegendIfAbsent();
            chart.Serializer.Save(memoryStream);
            chart.RemoveLegend(legend);

            // Generate new Big chart and copy serialized chart data to it.
            var bigChartForm = new BigChart.frmBigChart();
            bigChartForm.copyChartToBigWindow(memoryStream);

            bigChartForm.Show();

            memoryStream.Close();
        }

        /// <summary>
        /// Opens the chart in a custom big chart implementation with legend hiding
        ///  and power-point export functionality (incomplete)
        /// </summary>
        /// <param name="chart">The chart to open in a big chart.</param>
        public static void OpenIsCustomBigChart(this Chart chart)
        {
            var memoryStream = new MemoryStream();
            chart.Serializer.Save(memoryStream);

            var chartForm = new ChartForm();
            chartForm.CopyChartToWindow(memoryStream);
            chartForm.Show();
        } 
    }
}
