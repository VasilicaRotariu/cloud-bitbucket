﻿using System.Collections.Generic;
using System.Windows.Forms;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Provides methods to extend the functionality of the DataGridView class.
    /// </summary>
    public static class DataGridViewExt
    {
        /// <summary>
        /// Sets the data grid view selection by selecting the rows that have a cell 
        /// value (indexed by the cell index) that matches one of the specified selection strings.
        /// </summary>
        /// <param name="dataGridView">The data grid view.</param>
        /// <param name="selection">The selection strings.</param>
        /// <param name="cellIndex">The cell index.</param>
        public static void SetSelection(this DataGridView dataGridView, List<string> selection, int cellIndex = 0)
        {
            if (dataGridView.Rows.Count > 0)
            {
                bool nothingSelected = true;

                for (int index = 0; index < dataGridView.Rows.Count; ++index)
                {
                    var row = dataGridView.Rows[index];
                    bool select = selection.Contains(row.Cells[cellIndex].Value.ToString());
                    row.Selected = select;
                    nothingSelected &= !select;
                }

                if (nothingSelected)
                {
                    dataGridView.Rows[0].Selected = true;
                }
            }
        }
    }
}
