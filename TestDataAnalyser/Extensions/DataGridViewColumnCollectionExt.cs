﻿using System.Windows.Forms;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Provides methods to extend the functionality of the DataGridViewColumnCollection class.
    /// </summary>
    public static class DataGridViewColumnCollectionExt
    {
        /// <summary>
        /// Adds multiple columns to the collection.
        /// </summary>
        /// <param name="collection">The collection to add the columns to.</param>
        /// <param name="columns">An array of column names to add to the collection.</param>
        public static void AddMultipleColumns(this DataGridViewColumnCollection collection, params string[] columns)
        {
            foreach (string column in columns)
            {
                collection.Add(column, column);
            }
        }
    }
}
