﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Provides methods to extend the functionality of the DataGridViewSelectedRowCollection class.
    /// </summary>
    public static class DataGridViewSelectedRowCollectionExt
    {
        /// <summary>
        /// Returns the values from the named cell found in the rows of the collection.
        /// </summary>
        /// <param name="collection">The collection if rows to get the values from.</param>
        /// <param name="cell">The name of the cell to get the values from.</param>
        /// <returns>The list of values.</returns>
        public static List<string> CellValues(this DataGridViewSelectedRowCollection collection, string cell)
        {
            return (from DataGridViewRow row in collection select row.Cells[cell].Value.ToString()).ToList();
        }

        /// <summary>
        /// Returns the values from the named cell found in the rows of the collection.
        /// </summary>
        /// <param name="collection">The collection if rows to get the values from.</param>
        /// <param name="cellIndex">The index of the cell to get the values from.</param>
        /// <returns>The list of values.</returns>
        public static List<string> CellValues(this DataGridViewSelectedRowCollection collection, int cellIndex)
        {
            return (from DataGridViewRow row in collection select row.Cells[cellIndex].Value.ToString()).ToList();
        }
    }
}
