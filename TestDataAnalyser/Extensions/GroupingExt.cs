﻿using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Provides methods to extend the functionality of the grouping class.
    /// </summary>
    public static class GroupingExt
    {
        /// <summary>
        /// Returns the keys as a list from the grouping.
        /// </summary>
        /// <typeparam name="TKey">The grouping key type.</typeparam>
        /// <typeparam name="TElement">The grouping element type.</typeparam>
        /// <param name="values">The grouping.</param>
        /// <returns>The list of keys.</returns>
        public static List<TKey> Keys<TKey, TElement>(this IEnumerable<IGrouping<TKey, TElement>> values)
        {
            return values.Select(value => value.Key).ToList();
        }
    }
}
