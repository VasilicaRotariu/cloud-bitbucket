﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Provides methods to extend the functionality of the Chart class.
    /// </summary>
    public static class StringExt
    {
        /// <summary>
        /// Returns true if a string is empty. Returns false otherwise.
        /// </summary>
        /// <param name="val">The string to check.</param>
        /// <returns>true if a string is empty, false otherwise.</returns>
        public static bool IsEmpty(this string val)
        {
            return val.Length == 0;
        }

        /// <summary>
        /// Returns false if a string is empty. Returns true otherwise.
        /// </summary>
        /// <param name="val">The string to check.</param>
        /// <returns>false if a string is empty, true otherwise.</returns>
        public static bool IsNotEmpty(this string val)
        {
            return val.Length > 0;
        }

        /// <summary>
        /// Converts a string into a double value.
        /// </summary>
        /// <param name="val">The string to convert.</param>
        /// <returns>A double value. double.Nan if the conversion was not possible.</returns>
        public static double ToDouble(this string val)
        {
            double value;
            if (!double.TryParse(val, out value))
            {
                value = double.NaN;
            }
            return value;
        }

        /// <summary>
        /// Converts a string into a float value.
        /// </summary>
        /// <param name="val">The string to convert.</param>
        /// <returns>A float value. double.Nan if the conversion was not possible.</returns>
        public static float ToFloat(this string val)
        {
            float value;
            if (!float.TryParse(val, out value))
            {
                value = float.NaN;
            }
            return value;
        }

        /// <summary>
        /// Converts a string into an integer value.
        /// </summary>
        /// <param name="val">The string to convert.</param>
        /// <returns>An integer value. <c>int.Minimum</c> if the conversion was not possible.</returns>
        public static int ToInt(this string val)
        {
            int value;
            if (!int.TryParse(val, out value))
            {
                value = int.MinValue;
            }
            return value;
        }

        /// <summary>
        /// Uses the supplied arguments to format the format string.
        /// </summary>
        /// <param name="format">The string format to complete.</param>
        /// <param name="args">The arguments.</param>
        /// <returns>The formatted string.</returns>
        public static string Args(this string format, params object[] args)
        {
            return string.Format(format, args);
        }

        /// <summary>
        /// returns 'c' characters from the right of the string
        /// </summary>
        /// <param name="s">the string</param>
        /// <param name="c">Number of characters to return</param>
        /// <returns>c characters from the right of the string</returns>
        public static string Right(this string s, int c)
        {
            return s.Substring(s.Length - c, c);
        }

        /// <summary>
        /// returns 'c' characters from the left of the string
        /// </summary>
        /// <param name="s">the string</param>
        /// <param name="c">Number of characters to return</param>
        /// <returns>c characters from the right of the string</returns>
        public static string Left(this string s, int c)
        {
            return s.Substring(0, c);
        }

        /// <summary>
        /// Converts a character array into a string, removing any line termination characters.
        /// </summary>
        /// <param name="charArray">The character array to convert.</param>
        /// <returns>The converted string.</returns>
        public static string FromCharArray(char[] charArray)
        {
            //string String = new string(charArray);
            return (new string(charArray)).Trim('\0');
        }

        /// <summary>
        /// Joins a list of strings into a single string. Each string is quoted using the quote string and
        /// then joined into a single string, each sub string separated using the separator.
        /// </summary>
        /// <param name="values">The string values to join.</param>
        /// <param name="quote">The quote string to place around each string.</param>
        /// <param name="separator">The separator string to place between each string.</param>
        /// <returns>The joined string.</returns>
        public static string QuotedJoin(List<string> values, string quote = "'", string separator = ",")
        {
            var sb = new StringBuilder();
            bool first = true;

            foreach (var value in values)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    sb.Append(separator);
                }

                sb.Append(quote);
                sb.Append(value);
                sb.Append(quote);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Converts the text from the list of strings into a list of integers.
        /// </summary>
        /// <param name="values">The list of strings.</param>
        /// <returns>A list of integer values from the list of strings.</returns>
        public static List<int> ToIntList(this List<string> values)
        {
            return values.Select(value => value.ToInt()).ToList();
        }
    }
}
