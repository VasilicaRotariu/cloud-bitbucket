﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SudburyDataAnalyser.ChartTraces;

namespace SudburyDataAnalyser.Extensions
{
    /// <summary>
    /// Provides methods to extend the functionality of the ChartBLOB
    /// </summary>
    public static class ChartBLOBExt
    {
        /// <summary>
        /// Returns a Collection of unique series names from a collection of blobs
        /// </summary>
        /// <param name="blobs">Collection of Blobs</param>
        /// <returns>A collection of unique series names</returns>
        public static IEnumerable<string> GetUniqueSeriesNames(IEnumerable<ChartBLOB> blobs)
        { 
            return (from b in blobs
                         from s in b.SeriesNames()
                         select s).Distinct();
        }
    }
}
