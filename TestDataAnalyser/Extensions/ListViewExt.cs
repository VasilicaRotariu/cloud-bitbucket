﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Provides methods to extend the functionality of the ListView class.
    /// </summary>
    public static class ListViewExt
    {
        /// <summary>
        /// Returns a list of the text of all the checked items.
        /// </summary>
        /// <param name="listView">A list view.</param>
        /// <returns>A list of the text of the checked list view items.</returns>
        public static List<string> CheckedItems(this ListView listView)
        {
            return (from ListViewItem item in listView.Items where item.Checked select item.Text).ToList();
        }

        /// <summary>
        /// Clears all of the supplied list views.
        /// </summary>
        /// <param name="listViews">A list of list views to clear.</param>
        public static void ClearListViews(params ListView[] listViews)
        {
            foreach (var listView in listViews)
            {
                listView.Items.Clear();
            }
        }

        /// <summary>
        /// Creates a new list view item and sub items with the values supplied.
        /// </summary>
        /// <param name="tag">The list view item tag.</param>
        /// <param name="values">The list view item and sub item text values.</param>
        /// <returns>The parent list view item.</returns>
        public static ListViewItem CreateListViewItem(object tag, params object[] values)
        {
            return new ListViewItem(values.Select(value => value.ToString()).ToArray()) { Tag = tag };
        }

        /// <summary>
        /// Creates a new checked list view item and sub items with the values supplied.
        /// </summary>
        /// <param name="tag">The list view item tag.</param>
        /// <param name="values">The list view item and sub item text values.</param>
        /// <returns>The parent list view item.</returns>
        public static ListViewItem CreateCheckedListViewItem(object tag, params object[] values)
        {
            var item = CreateListViewItem(tag, values);
            item.Checked = true;

            return item;
        }
    }
}
