﻿using System.Collections.Generic;
using System.Windows.Forms;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Provides methods to extend the functionality of the TreeNode class.
    /// </summary>
    public static class TreeNodeExt
    {
        /// <summary>
        /// Returns the top parent tree node from a child tree node.
        /// </summary>
        /// <param name="treeNode">A node in a tree.</param>
        /// <returns>The nodes top parent node.</returns>
        public static TreeNode TopNode(this TreeNode treeNode)
        {
            var node = treeNode;

            while (node.Parent != null) node = node.Parent;
            return node;
        }

        /// <summary>
        /// Returns all the nodes children.
        /// </summary>
        /// <param name="treeNode">A node in a tree.</param>
        /// <returns>The nodes children.</returns>
        public static List<TreeNode> AllChildren(this TreeNode treeNode)
        {
            var nodes = new List<TreeNode>();

            foreach (TreeNode childNode in treeNode.Nodes)
            {
                nodes.Add(childNode);

                if (childNode.Nodes.Count > 0)
                {
                    nodes.AddRange(childNode.AllChildren());
                }
            }

            return nodes;
        }
    }
}
