﻿using System;
using System.Text;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Provides methods to extend the functionality of the StringBuilder class.
    /// </summary>
    public static class StringBuilderExt
    {
        /// <summary>
        /// Appends the supplied strings to the string builder instance.
        /// </summary>
        /// <param name="sb">The string builder instance.</param>
        /// <param name="values">The string values to add to the string builder instance.</param>
        /// <returns>The appended string builder instance.</returns>
        public static StringBuilder Append(this StringBuilder sb, params string[] values)
        {
            foreach (var value in values)
            {
                sb.Append(value);
            }

            return sb;
        }

        /// <summary>
        /// Appends the supplied strings as lines of text to the string builder instance.
        /// </summary>
        /// <param name="sb">The string builder instance.</param>
        /// <param name="values">The values to add to the string builder instance.</param>
        /// <returns>The appended string builder instance.</returns>
        public static StringBuilder AppendLines(this StringBuilder sb, params object[] values)
        {
            bool addEndOfLine = false;

            foreach (object value in values)
            {
                if (value is string)
                {
                    if (addEndOfLine)
                    {
                        sb.AppendLine();
                    }

                    var stringValue = value as string;
                    if (stringValue.IsNotEmpty())
                    {
                        sb.Append(stringValue);
                    }
                    addEndOfLine = true;
                }
                else if (value is Queries.Parameters.QueryParameter)
                {
                    string stringValue = value as Queries.Parameters.QueryParameter;
                    if (stringValue.IsNotEmpty())
                    {
                        sb.Append(stringValue);
                    }
                    addEndOfLine = false;
                }
                else
                {
                    throw new NotSupportedException(value.GetType().ToString());
                }
            }

            if (addEndOfLine)
            {
                sb.AppendLine();
            }

            return sb;
        }
    }
}
