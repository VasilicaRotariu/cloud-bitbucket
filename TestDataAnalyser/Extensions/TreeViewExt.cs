﻿using System.Collections.Generic;
using System.Windows.Forms;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Provides methods to extend the functionality of the TreeView class.
    /// </summary>
    public static class TreeViewExt
    {
        /// <summary>
        /// Returns all the children.
        /// </summary>
        /// <param name="treeView">A tree view.</param>
        /// <returns>The tree view nodes.</returns>
        public static List<TreeNode> AllNodes(this TreeView treeView)
        {
            var nodes = new List<TreeNode>();

            foreach (TreeNode node in treeView.Nodes)
            {
                nodes.Add(node);
                nodes.AddRange(node.AllChildren());
            }

            return nodes;
        }
    }
}
