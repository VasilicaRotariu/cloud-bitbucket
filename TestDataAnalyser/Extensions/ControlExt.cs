﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Provides helper methods to controls generally.
    /// </summary>
    public class ControlExt
    {
        /// <summary>
        /// Shows or hides the array of controls.
        /// </summary>
        /// <param name="show">Flag set true to show the controls and false to hide the controls.</param>
        /// <param name="controls">Array of controls to show or hide.</param>
        public static void ShowControls(bool show, params Control[] controls)
        {
            foreach (Control control in controls)
            {
                if (control != null)
                {
                    control.Visible = show;
                }
            }
        }

        /// <summary>
        /// Shows the array of controls.
        /// </summary>
        /// <param name="controls">The controls to show.</param>
        public static void ShowControls(params Control[] controls)
        {
            ShowControls(true, controls);
        }

        /// <summary>
        /// Hides the array of controls.
        /// </summary>
        /// <param name="controls">The controls to hide.</param>
        public static void HideControls(params Control[] controls)
        {
            ShowControls(false, controls);
        }

        /// <summary>
        /// Gets all the controls with the specified name that are on the main form.
        /// </summary>
        /// <param name="name">The name of the control.</param>
        /// <returns>All the controls with the specified name.</returns>
        public static List<Control> GetControls(string name)
        {
            if (name == null) return new List<Control>();
            if (Application.OpenForms.Count == 0) return new List<Control>();

            return Application.OpenForms[0].Controls.Find(name, true).ToList();
        }

        /// <summary>
        /// Gets the control with the specified name of type T.
        /// </summary>
        /// <typeparam name="T">The type of control.</typeparam>
        /// <param name="name">The name of the control.</param>
        /// <returns>The control or null.</returns>
        public static T GetControl<T>(string name) where T : Control
        {
            return GetControls(name).OfType<T>().Select(control => control).FirstOrDefault();
        }

        /// <summary>
        /// Gets the controls with the specified name prefix of type T.
        /// </summary>
        /// <typeparam name="T">The type of control.</typeparam>
        /// <param name="prefix">The name prefix of the controls.</param>
        /// <returns>The control or null.</returns>
        public static List<T> GetControls<T>(string prefix) where T : Control
        {
            var controls = new List<T>();

            for (var postfix = 1;; ++postfix)
            {
                var control = GetControl<T>("{0}_{1}".Args(prefix, postfix));
                if (control == null) break;

                controls.Add(control);
            }

            return controls;
        }
    }
}
