﻿using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Generic tree class. Represents a tree of T objects.
    /// </summary>
    /// <typeparam name="T">The type of object in the tree.</typeparam>
    public class Tree<T>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Tree{T}"/> class.
        /// </summary>
        /// <param name="value">The objects value for the tree, branch or leaf.</param>
        public Tree(T value)
        {
            Value = value;
        }

        /// <summary>
        /// Gets the tree, branch or leaf value.
        /// </summary>
        public T Value { get; private set; }

        /// <summary>
        /// Adds a branch or leaf to a tree.
        /// </summary>
        /// <param name="child">The branch or leaf to add to the tree.</param>
        public Tree<T> AddChild(T child)
        {
            Tree<T> branch;
            if (!branches.TryGetValue(child, out branch))
            {
                branch = new Tree<T>(child);
                branches[child] = branch;
                orderedKeys.Add(child);
            }

            return branch;
        }

        /// <summary>
        /// Gets the child branches and leaves in the order added to the tree.
        /// </summary>
        public List<Tree<T>> Branches
        {
            get
            {
                return orderedKeys.Select(key => branches[key]).ToList();
            }
        }

        /// <summary>
        /// The tree branches in a dictionary for fast lookup.
        /// </summary>
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private Dictionary<T, Tree<T>> branches = new Dictionary<T, Tree<T>>();

        /// <summary>
        /// The keys in the order added to the tree.
        /// </summary>
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private List<T> orderedKeys = new List<T>();
    }
}
