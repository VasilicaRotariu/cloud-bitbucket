﻿using System;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Provides methods to extend the functionality of the float class.
    /// </summary>
    public static class FloatExt
    {
        /// <summary>
        /// Returns true if the other value is very close to this value.
        /// </summary>
        /// <param name="value">This float value.</param>
        /// <param name="otherValue">The float value to compare.</param>
        /// <returns>True if the other value is very close to this value.</returns>
        public static bool Approx(this float value, float otherValue)
        {
            return Math.Abs(value - otherValue) < FloatTolerance;
        }

        /// <summary>
        /// Returns true if the other value is very close to this value.
        /// </summary>
        /// <param name="value">This float value.</param>
        /// <param name="otherValue">The float value to compare.</param>
        /// <returns>True if the other value is very close to this value.</returns>
        public static bool Approx(this double value, double otherValue)
        {
            return Math.Abs(value - otherValue) < DoubleTolerance;
        }

        /// <summary>Tolerance used to compare floats.</summary>
        private const float FloatTolerance = 0.0001f;

        /// <summary>Tolerance used to compare doubles.</summary>
        private const double DoubleTolerance = 0.0000001f;
    }
}
