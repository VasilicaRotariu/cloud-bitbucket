﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Provides methods to extend the functionality of the ListView.ListViewItemCollection class.
    /// </summary>
    public static class ListViewItemCollectionExt
    {
        /// <summary>
        /// Concatenates the text from the list view items in the collection into a single string.
        /// </summary>
        /// <param name="collection">The list view item collection.</param>
        /// <param name="separator">The text placed between each list view item text.</param>
        /// <returns>A concatenated string of list view item text.</returns>
        public static string Join(this ListView.ListViewItemCollection collection, string separator)
        {
            return string.Join(separator, (from ListViewItem item in collection select item.Text).ToList());
        }

        /// <summary>
        /// Concatenates the text from the list view items in the collection into a single string.
        /// </summary>
        /// <param name="collection">The list view item collection.</param>
        /// <param name="separator">The text placed between each list view item text.</param>
        /// <returns>A concatenated string of list view item text.</returns>
        public static string Join(this ListView.CheckedListViewItemCollection collection, string separator)
        {
            return string.Join(separator, (from ListViewItem item in collection select item.Text).ToList());
        }

        /// <summary>
        /// Converts the text from the list view items in the collection into a list of integers.
        /// </summary>
        /// <param name="collection">The list view item collection.</param>
        /// <returns>A list of integer values from the list view item collection.</returns>
        public static List<int> ToIntList(this ListView.ListViewItemCollection collection)
        {
            return (from ListViewItem item in collection select item.Text.ToInt()).ToList();
        }

        /// <summary>
        /// Converts the text from the list view items in the collection into a list of integers.
        /// </summary>
        /// <param name="collection">The list view item collection.</param>
        /// <returns>A list of integer values from the list view item collection.</returns>
        public static List<int> ToIntList(this ListView.SelectedListViewItemCollection collection)
        {
            return (from ListViewItem item in collection select item.Text.ToInt()).ToList();
        }

        /// <summary>
        /// Converts the text from the list view items in the collection into a list of strings.
        /// </summary>
        /// <param name="collection">The list view item collection.</param>
        /// <returns>A list of string values from the list view item collection.</returns>
        public static List<string> ToList(this ListView.CheckedListViewItemCollection collection)
        {
            return (from ListViewItem item in collection select item.Text).ToList();
        }
    }
}
