﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Provides methods to extend the functionality of the date time class.
    /// </summary>
    public class DateTimeExt
    {
        /// <summary>
        /// Returns all the date times, starting with the start date time and ending with the 
        /// end date time and including all the date times between that have a time specified
        /// by the times.
        /// </summary>
        /// <param name="start">The start date time.</param>
        /// <param name="end">The end date time.</param>
        /// <param name="times">List of times.</param>
        /// <returns>List of date times.</returns>
        public static List<DateTime> GetAllDateTimes(DateTime start, DateTime end, List<TimeSpan> times)
        {
            var splitDateTimes = new List<DateTime>();

            if (times.Count > 0)
            {
                DateTime splitDateTime = start;

                while (true)
                {
                    splitDateTimes.Add(splitDateTime);

                    splitDateTime = NextDateTime(splitDateTime, times);
                    if (splitDateTime >= end)
                    {
                        splitDateTimes.Add(end);
                        break;
                    }
                }
            }

            return splitDateTimes;
        }

        /// <summary>
        /// Returns the next date time after the specified date time that has a time in times.
        /// </summary>
        /// <param name="dateTime">The initial date time.</param>
        /// <param name="times">List of times.</param>
        /// <returns>The next date time.</returns>
        private static DateTime NextDateTime(DateTime dateTime, List<TimeSpan> times)
        {
            var timeOfDay = dateTime.TimeOfDay;
            var date = dateTime.Date;

            if (times.Count == 1)
            {
                return date.AddDays(1);
            }

            foreach (var splitTime in times.Where(splitTime => timeOfDay < splitTime))
            {
                return date + splitTime;
            }

            return date.AddDays(1) + times[0];
        }
    }
}
