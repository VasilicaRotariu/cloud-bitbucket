﻿using System;
using System.ComponentModel;
using System.Threading;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Provides methods to extend the functionality of the Thread class.
    /// </summary>
    public static class ThreadExt
    {
        /// <summary>
        /// Starts a new thread and calls the method in the new thread.
        /// </summary>
        /// <param name="start">The method to execute in the thread.</param>
        public static void StartInNewThread(ThreadStart start)
        {
            var thread = new Thread(start);
            thread.Start();
        }

        /// <summary>
        /// Executes the action specified, either directly or through an invoke if an invoke is
        /// required (because the call is made from an non UI thread).
        /// </summary>
        /// <typeparam name="T">The control type.</typeparam>
        /// <param name="control">The control that is going to be modified by the action.</param>
        /// <param name="action">The action to apply to the control.</param>
        public static void InvokeIfRequired<T>(this T control, Action<T> action) where T : ISynchronizeInvoke
        {
            try
            {
                if (control.InvokeRequired)
                {
                    control.Invoke(new Action(() => action(control)), null);
                }
                else
                {
                    action(control);
                }
            }
            catch (ObjectDisposedException)
            {
                // Catch object disposed exceptions (which can occur if the application exits whilst
                // the thread is running) and abort the thread.
                Thread.CurrentThread.Abort();
            }
        }
    }
}
