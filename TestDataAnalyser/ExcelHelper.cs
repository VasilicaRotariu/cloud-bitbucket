﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Excel = Microsoft.Office.Interop.Excel;

namespace SudburyDataAnalyser
{
    /// <summary>
    /// Helper class used to copy data grid data to an Excel spread sheet.
    /// </summary>
    public class ExcelHelper
    {
        /// <summary>  The Excel application. </summary>
        private Excel.Application application;

        /// <summary> The Excel work book.  </summary>
        private Excel.Workbook workbook;

        /// <summary>
        /// Copies the data found in the data grid view to an Excel work sheet.
        /// Opens Excel if required and adds a new work sheet.
        /// </summary>
        /// <param name="dataGrid">The data grid to get the values from.</param>
        public void Export(DataGridView dataGrid)
        {
            // ReSharper disable once UnusedVariable
            using (var waitCursor = new WaitCursor())
            {
                if (!OpenExcelWorkbook())
                {
                    return;
                }

                var worksheet = AddWorkSheet();
                if (worksheet != null)
                {
                    var cellValues = GetTitleCells(dataGrid);
                    CopyToExcel(worksheet, cellValues, 0);

                    var rowIndex = 1;
                    foreach (DataGridViewRow row in dataGrid.Rows)
                    {
                        cellValues = GetRowCells(row);
                        CopyToExcel(worksheet, cellValues, rowIndex);

                        ++rowIndex;
                    }
                }

                this.application.Visible = true;
                this.workbook.Activate();
            }
        }

        /// <summary>
        /// Copies the data found in the chart to an Excel work sheet.
        /// Opens Excel if required and adds a new work sheet.
        /// </summary>
        /// <param name="chart">The chart to get the values from.</param>
        public void Export(Chart chart)
        {
            // ReSharper disable once UnusedVariable
            using (var waitCursor = new WaitCursor())
            {
                if (!OpenExcelWorkbook())
                {
                    return;
                }

                var worksheet = AddWorkSheet();
                if (worksheet != null)
                {
                    var cellValues = GetTitleCells(chart);
                    CopyToExcel(worksheet, cellValues, 0);

                    var rowIndex = 1;
                    while (GetRowCells(chart, rowIndex - 1, out cellValues))
                    {
                        CopyToExcel(worksheet, cellValues, rowIndex);
                        ++rowIndex;
                    }
                }

                application.Visible = true;
                workbook.Activate();
            }
        }

        /// <summary>
        /// Adds a row of values to anExcel work sheet.
        /// Because of issues with setting the cell values, this is done, first per row and
        /// secondly, in blocks of 26 cells at a time.
        /// </summary>
        /// <param name="worksheet">The work sheet to add the values too.</param>
        /// <param name="values">The values.</param>
        /// <param name="rowIndex">The zero based index of the row.</param>
        private void CopyToExcel(Excel.Worksheet worksheet, string[] values, int rowIndex)
        {
            var valueCount = values.Length;
            var start = 0;

            while (start < valueCount)
            {
                var end = Math.Min(valueCount - 1, start + 25);

                var valuesToCopy = new string[end - start + 1];
                Array.Copy(values, start, valuesToCopy, 0, end - start + 1);

                var range = worksheet.Range[Cell(start, rowIndex), Cell(end, rowIndex)];
                range.Value = valuesToCopy;

                start = end + 1;
            }
        }

        /// <summary>
        /// Gets the title values from the data grid.
        /// </summary>
        /// <param name="dataGridView">The data grid.</param>
        /// <returns>The title values.</returns>
        private string[] GetTitleCells(DataGridView dataGridView)
        {
            var values = new string[dataGridView.Columns.Count];

            var columnIndex = 0;
            foreach (DataGridViewColumn column in dataGridView.Columns)
            {
                values[columnIndex] = column.HeaderText;
                ++columnIndex;
            }

            return values;
        }

        /// <summary>
        /// Gets the title values from the chart.
        /// </summary>
        /// <param name="chart">The chart.</param>
        /// <returns>The title values.</returns>
        private string[] GetTitleCells(Chart chart)
        {
            var values = new string[chart.Series.Count * 2];

            var columnIndex = 0;
            foreach (var series in chart.Series)
            {
                values[columnIndex] = series.Name;
                columnIndex += 2;
            }

            return values;
        }

        /// <summary>
        /// Gets the cell values for a single row.
        /// </summary>
        /// <param name="dataGridViewRow">The row to get the cell values from.</param>
        /// <returns>The cell values.</returns>
        private string[] GetRowCells(DataGridViewRow dataGridViewRow)
        {
            var values = new string[dataGridViewRow.Cells.Count];

            var columnIndex = 0;
            foreach (DataGridViewCell cell in dataGridViewRow.Cells)
            {
                values[columnIndex] = cell.Value.ToString();
                ++columnIndex;
            }

            return values;
        }

        /// <summary>
        /// Gets the cell values for the index row of the chart series data.
        /// </summary>
        /// <param name="chart">The chart.</param>
        /// <param name="index">The row index.</param>
        /// <param name="values">The cell values.</param>
        /// <returns>True if there were any cell values; false otherwise.</returns>
        private bool GetRowCells(Chart chart, int index, out string[] values)
        {
            values = new string[chart.Series.Count * 2];

            var addedValue = false;
            var seriesIndex = 0;
            foreach (var series in chart.Series)
            {
                if (series.Points.Count > index)
                {
                    values[seriesIndex] = series.Points[index].XValue.ToString(CultureInfo.InvariantCulture);
                    values[seriesIndex + 1] = series.Points[index].YValues.First().ToString(CultureInfo.InvariantCulture);
                    addedValue = true;
                }

                seriesIndex += 2;
            }

            return addedValue;
        }

        /// <summary>
        /// Returns an Excel cell using the zero based indices provides.
        /// Does not check for illegal column or row indices.
        /// </summary>
        /// <param name="column">The column index.</param>
        /// <param name="row">The row index.</param>
        /// <returns>The Excel cell.</returns>
        private string Cell(int column, int row)
        {
            string cell;
            if (column < 26)
            {
                cell = "{0}{1}".Args((char)('A' + column), row + 1);
            }
            else
            {
                cell = "{0}{1}{2}".Args((char)(('A' + (column / 26)) - 1), (char)('A' + (column % 26)), row + 1);
            }

            return cell;
        }

        /// <summary>
        /// Opens an Excel work book.
        /// </summary>
        /// <returns>True if the work book was opened; false otherwise.</returns>
        private bool OpenExcelWorkbook()
        {
            try
            {
                if (application == null)
                {
                    application = new Excel.Application();
                    workbook = application.Workbooks.Add();
                }

                if (workbook == null)
                {
                    workbook = application.Workbooks.Add();
                }

                return true;
            }
            catch
            {
                application = null;
                workbook = null;

                return false;
            }
        }

        /// <summary>
        /// Adds and returns a new worksheet. Opens excel etc. if required.
        /// </summary>
        /// <returns>The new worksheet.</returns>
        private Excel.Worksheet AddWorkSheet()
        {
            try
            {
                return workbook.Sheets.Add();
            }
            catch
            {
                application = null;
                workbook = null;

                OpenExcelWorkbook();

                try
                {
                    if (workbook == null)
                    {
                        return null;
                    }

                    return workbook.Sheets.Add();
                }
                catch
                {
                    return null;
                }
            }
        }
    }
}
