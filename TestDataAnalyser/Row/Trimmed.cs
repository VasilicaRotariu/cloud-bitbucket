﻿using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.Trimmed
{
    /// <summary>
    /// Represents a single record (one test point for one injector) of Trimmed data
    /// </summary>
    public class Row : UTIDRow
    {
        /// <summary>Gets test point duration.</summary>
        public short NominalLogic { get; private set; }

        /// <summary>Gets test point fuel demand.</summary>
        public double NominalFuel { get; private set; }

        /// <summary>Gets measured parameter standard deviation.</summary>
        public double Data { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="Trimmed.Row"/> class.
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        public override void Initialise(SqlDataReader reader)
        {
            Initialise(
                reader,
                UTIDRowParameters.PlantNumber,
                UTIDRowParameters.LineNumber,
                UTIDRowParameters.UTID,
                UTIDRowParameters.PDate,
                UTIDRowParameters.ISN,
                UTIDRowParameters.LOP,
                UTIDRowParameters.TestPlan,
                UTIDRowParameters.Global,
                UTIDRowParameters.URID,
                UTIDRowParameters.SpeedReq);
            Initialise(reader, NameRowParameters.All);

            Data = ReadDoubleFromString(reader, "Data");
            NominalFuel = ReadDoubleFromString(reader, "NominalFuel");
            NominalLogic = ReadShort(reader, "NominalLogic");

            TestType = ReadString(reader, Queries.Parameters.QueryParameters.HeaderTableTestTypeColumn);
        }
    }
}
