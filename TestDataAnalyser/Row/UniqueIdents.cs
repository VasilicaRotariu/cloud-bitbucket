﻿using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.UniqueIdents
{
    /// <summary>
    /// Represents a single unique IDENT record.
    /// </summary>
    public class Row : RowBase
    {
        /// <summary>Gets or sets IDENT.</summary>
        // ReSharper disable once InconsistentNaming
        public string IDENT { get; protected set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="UniqueIdents.Row"/> class.
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        public override void Initialise(SqlDataReader reader)
        {
            IDENT = ReadString(reader, Queries.Parameters.QueryParameters.HeaderTableIDENTColumn);
        }
    }
}
