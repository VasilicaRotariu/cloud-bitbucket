﻿using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Static class used for retrieving information from lists of rows.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public static class ChartTraceOMVRowsExt
    {
        /// <summary>
        /// From the rows provided, returns a list of rows that match the UTID and speed pressure
        /// duration.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="key">The key to filter the rows by.</param>
        /// <param name="speedPressureDuration">The speed pressure duration to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<ChartTraceOMV.Row> FilterOnUTIDAndSpeedPressureDuration(this List<ChartTraceOMV.Row> rows, UTIDPlantNumberKey key, string speedPressureDuration)
        {
            return (from x in rows
                    where (x.UTID == key.UTID) && (x.PlantNumber == key.PlantNumber) && (x.SpeedPressureDuration == speedPressureDuration)
                    orderby x.SpeedPressureDuration
                    select x).ToList();
        }

        /// <summary>
        /// From the rows provided, returns the rows grouped by speed pressure duration.
        /// </summary>
        /// <param name="rows">The rows to group.</param>
        /// <returns>A list of grouped rows.</returns>
        public static List<string> GroupBySpeedPressureDuration(this List<ChartTraceOMV.Row> rows)
        {
            return (from x in rows group x by x.SpeedPressureDuration).Keys();
        }

        /// <summary>
        /// From the rows provided filtered by the keys and speed pressure duration,
        /// returns the unique speed pressure duration.
        /// </summary>
        /// <param name="rows">The rows to group.</param>
        /// <param name="keys">The keys to filter the rows by.</param>
        /// <param name="speedPressureDuration">The speed pressure duration to filter the rows by.</param>
        /// <returns>A list of unique speed pressure duration.</returns>
        public static List<string> UniqueSpeedPressureDurations(this List<ChartTraceOMV.Row> rows, UTIDPlantNumberKeys keys, List<string> speedPressureDuration)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber)) && speedPressureDuration.Contains(x.SpeedPressureDuration)
                    group x by x.SpeedPressureDuration).Keys();
        }

        /// <summary>
        /// From the rows provided filtered by the keys and speed pressure duration,
        /// returns the unique identifiers.
        /// </summary>
        /// <param name="rows">The rows to group.</param>
        /// <param name="keys">The keys to filter the rows by.</param>
        /// <param name="speedPressureDuration">The speed pressure duration to filter the rows by.</param>
        /// <returns>A list of unique identifiers.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueIDENTS(this List<ChartTraceOMV.Row> rows, UTIDPlantNumberKeys keys, List<string> speedPressureDuration)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber)) && speedPressureDuration.Contains(x.SpeedPressureDuration)
                    group x by x.IDENT).Keys();
        }
    }
}
