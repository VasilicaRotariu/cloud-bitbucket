﻿using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.Trimmed
{
    /// <summary>
    /// Static class used for ordering rows in a tree.
    /// </summary>
    public static class RowsOrderer
    {
        /// <summary>
        /// Orders a list of rows by nominal fuel.
        /// </summary>
        /// <param name="rows">List of rows to order.</param>
        /// <returns>The ordered rows.</returns>
        public static List<Row> OrderRowsByNominalFuel(this List<Row> rows)
        {
            return rows.OrderBy(o => o.NominalFuel).ToList();
        }
    }
}
