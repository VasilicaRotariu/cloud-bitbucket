﻿using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.UniqueSpeedLobe1OMVDemand
{
    /// <summary>
    /// Rows returned from a unique speed lobe1OMV duration query.
    /// </summary>
    public class Row : RowBase
    {
        /// <summary>
        /// Gets or sets the speed pressure lobe1 OMV column in the row.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public string SpeedLobe1OMVDemand { get; protected set; }

        /// <summary>
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a row.
        /// </summary>
        /// <param name="reader">SQLDataReader used to read row from.</param>
        public override void Initialise(SqlDataReader reader)
        {
            SpeedLobe1OMVDemand = ReadString(reader, "SpeedLobe1OMVDemand");
        }
    }
}
