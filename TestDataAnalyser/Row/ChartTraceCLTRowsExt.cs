﻿using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Static class used for retrieving information from lists of rows.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public static class ChartTraceCLTRowsExt
    {
        /// <summary>
        /// From the rows provided, returns a list of rows that match the UTID and speed pressure
        /// demand.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="key">The key to filter the rows by.</param>
        /// <param name="speedPressureDemand">The speed pressure demand to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<ChartTraceCLT.Row> FilterOnUTIDAndSpeedPressureDemand(this List<ChartTraceCLT.Row> rows, UTIDPlantNumberKey key, string speedPressureDemand)
        {
            return (from x in rows
                    where (x.UTID == key.UTID) && (x.PlantNumber == key.PlantNumber) && (x.SpeedPressureDemand == speedPressureDemand)
                    orderby x.SpeedPressureDemand
                    select x).ToList();
        }

        /// <summary>
        /// From the rows provided, returns a list of the unique speed pressure demands.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <returns>A list of unique speed pressure demands.</returns>
        public static List<string> UniqueSpeedPressureDemands(this List<ChartTraceCLT.Row> rows)
        {
            return (from x in rows group x by x.SpeedPressureDemand).Keys();
        }

        /// <summary>
        /// From the rows provided and filtered by the keys and selected speed pressure demands,
        /// returns a list of the unique speed pressure demands.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">List of keys to filter the rows by.</param>
        /// <param name="selectedSpeedPressureDemand">List of speed pressure demands to filter the rows by.</param>
        /// <returns>A list of unique speed pressure demands.</returns>
        public static List<string> UniqueSpeedPressureDemands(this List<ChartTraceCLT.Row> rows, UTIDPlantNumberKeys keys, List<string> selectedSpeedPressureDemand)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber)) && selectedSpeedPressureDemand.Contains(x.SpeedPressureDemand)
                    group x by x.SpeedPressureDemand).Keys();
        }

        /// <summary>
        /// From the rows provided and filtered by the keys and selected speed pressure demands,
        /// returns a list of the unique identifiers.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">List of keys to filter the rows by.</param>
        /// <param name="speedPressureDemands">List of speed pressure demands to filter the rows by.</param>
        /// <returns>A list of unique identifiers.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueIDENTS(this List<ChartTraceCLT.Row> rows, UTIDPlantNumberKeys keys, List<string> speedPressureDemands)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber)) && speedPressureDemands.Contains(x.SpeedPressureDemand)
                    group x by x.IDENT).Keys();
        }
    }
}
