﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SudburyDataAnalyser.Untrimmed
{
    /// <summary>
    /// A Row containing untrimmed data points
    /// </summary>
    public class Row : UTIDRow
    {
        /// <summary>
        /// Gets the Logic
        /// </summary>
        public int Logic { get; private set; }

        /// <summary>
        /// Gets the Nominal Value
        /// </summary>
        public float NominalValue { get; private set; }

        /// <summary>
        /// Gets the Offset Value
        /// </summary>
        public float Offset { get; private set; }

        /// <summary>
        /// Gets the Untrimmed value (Nominal + Offset)
        /// </summary>
        public float UntrimmedValue { get; private set; }
        
        /// <summary>
        /// Used to initialise the Row using a reader
        /// </summary>
        /// <param name="reader">the SQL data reader to read from</param>
        public override void Initialise(System.Data.SqlClient.SqlDataReader reader)
        {
            //Init UTIDRow
            Initialise(
                reader,
                UTIDRowParameters.PlantNumber, 
                UTIDRowParameters.UTID);

            //Init Name Row
            Initialise(reader, NameRowParameters.Name, NameRowParameters.PressureReq);
            
            Logic = (int)ReadShort(reader, "Logic");
            NominalValue = ReadFloat(reader, "NomValue");
            Offset = ReadFloat(reader, "Offset");
            UntrimmedValue = ReadFloat(reader, "CP");
        }
    }
}
