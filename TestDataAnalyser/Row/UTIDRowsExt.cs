﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Static class used for retrieving information from lists of rows.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public static class UTIDRowsExt
    {
        /// <summary>
        /// From the rows provided, returns the list of unique UTID's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified, the
        /// pressure level is one of the pressure levels specified and the name is one of the
        /// names specified.
        /// </summary>
        /// <typeparam name="T">The type of the rows to filter.</typeparam>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="pressureLevels">A list of pressure levels to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        public static List<int> Uniqueutids<T>(this List<T> rows, UTIDPlantNumberKeys keys, List<string> pressureLevels, List<string> names) where T : UTIDRow
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && pressureLevels.Contains(x.PressureReq.ToString(CultureInfo.InvariantCulture))
                    && names.Contains(x.Name)
                    group x by x.UTID).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique UTID's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <typeparam name="T">The type of the rows to filter.</typeparam>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        public static List<int> Uniqueutids<T>(this List<T> rows, UTIDPlantNumberKeys keys, List<string> names) where T : UTIDRow
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.Name)
                    group x by x.UTID).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique UTID's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        public static List<int> Uniqueutids(this List<Leak.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.TestPoint.ToString(CultureInfo.InvariantCulture))
                    group x by x.UTID).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique UTID's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        public static List<int> Uniqueutids(this List<ChartTraceCLT.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDemand)
                    group x by x.UTID).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique UTID's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        public static List<int> Uniqueutids(this List<ChartTraceCLS.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDemand)
                    group x by x.UTID).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique UTID's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        public static List<int> Uniqueutids(this List<ChartTraceFOD.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedLobe1OMVDemand)
                    group x by x.UTID).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique UTID's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        public static List<int> Uniqueutids(this List<ChartTraceNCV.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDuration)
                    group x by x.UTID).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique UTID's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        public static List<int> Uniqueutids(this List<ChartTraceOMV.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDuration)
                    group x by x.UTID).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique ISN's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified, the
        /// pressure level is one of the pressure levels specified and the name is one of the
        /// names specified.
        /// </summary>
        /// <typeparam name="T">The type of the rows to filter.</typeparam>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="pressureLevels">A list of pressure levels to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueISNs<T>(this List<T> rows, UTIDPlantNumberKeys keys, List<string> pressureLevels, List<string> names) where T : UTIDRow
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && pressureLevels.Contains(x.PressureReq.ToString(CultureInfo.InvariantCulture))
                    && names.Contains(x.Name)
                    group x by x.ISN).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique ISN's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <typeparam name="T">The type of the rows to filter.</typeparam>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueISNs<T>(this List<T> rows, UTIDPlantNumberKeys keys, List<string> names) where T : UTIDRow
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.Name)
                    group x by x.ISN).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique ISN's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueISNs(this List<Leak.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.TestPoint.ToString(CultureInfo.InvariantCulture))
                    group x by x.ISN).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique ISN's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueISNs(this List<ChartTraceCLT.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDemand)
                    group x by x.ISN).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique ISN's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueISNs(this List<ChartTraceCLS.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDemand)
                    group x by x.ISN).Keys();
        } 

        /// <summary>
        /// From the rows provided, returns the list of unique ISN's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueISNs(this List<ChartTraceFOD.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedLobe1OMVDemand)
                    group x by x.ISN).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique ISN's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueISNs(this List<ChartTraceNCV.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDuration)
                    group x by x.ISN).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique ISN's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueISNs(this List<ChartTraceOMV.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDuration)
                    group x by x.ISN).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique LOP's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified, the
        /// pressure level is one of the pressure levels specified and the name is one of the
        /// names specified.
        /// </summary>
        /// <typeparam name="T">The type of the rows to filter.</typeparam>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="pressureLevels">A list of pressure levels to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueLOPs<T>(this List<T> rows, UTIDPlantNumberKeys keys, List<string> pressureLevels, List<string> names) where T : UTIDRow
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && pressureLevels.Contains(x.PressureReq.ToString(CultureInfo.InvariantCulture))
                    && names.Contains(x.Name)
                    group x by x.LOP).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique LOP's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <typeparam name="T">The type of the rows to filter.</typeparam>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueLOPs<T>(this List<T> rows, UTIDPlantNumberKeys keys, List<string> names) where T : UTIDRow
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.Name)
                    group x by x.LOP).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique LOP's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueLOPs(this List<Leak.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                         where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                         && names.Contains(x.TestPoint.ToString(CultureInfo.InvariantCulture))
                         group x by x.LOP).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique LOP's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueLOPs(this List<ChartTraceCLT.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDemand)
                    group x by x.LOP).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique LOP's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueLOPs(this List<ChartTraceCLS.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDemand)
                    group x by x.LOP).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique LOP's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueLOPs(this List<ChartTraceFOD.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedLobe1OMVDemand)
                    group x by x.LOP).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique LOP's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueLOPs(this List<ChartTraceNCV.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDuration)
                    group x by x.LOP).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique LOP's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueLOPs(this List<ChartTraceOMV.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDuration)
                    group x by x.LOP).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique IDENT's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified, the
        /// pressure level is one of the pressure levels specified and the name is one of the
        /// names specified.
        /// </summary>
        /// <typeparam name="T">The type of the rows to filter.</typeparam>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="pressureLevels">A list of pressure levels to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueIDENTs<T>(this List<T> rows, UTIDPlantNumberKeys keys, List<string> pressureLevels, List<string> names) where T : UTIDRow
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && pressureLevels.Contains(x.PressureReq.ToString(CultureInfo.InvariantCulture))
                    && names.Contains(x.Name)
                    group x by x.IDENT).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique IDENT's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <typeparam name="T">The type of the rows to filter.</typeparam>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueIDENTs<T>(this List<T> rows, UTIDPlantNumberKeys keys, List<string> names) where T : UTIDRow
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.Name)
                    group x by x.IDENT).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique IDENT's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueIDENTs(this List<Leak.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.TestPoint.ToString(CultureInfo.InvariantCulture))
                    group x by x.IDENT).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique IDENT's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueIDENTs(this List<ChartTraceCLT.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDemand)
                    group x by x.IDENT).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique IDENT's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueIDENTs(this List<ChartTraceCLS.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDemand)
                    group x by x.IDENT).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique IDENT's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueIDENTs(this List<ChartTraceFOD.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedLobe1OMVDemand)
                    group x by x.IDENT).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique IDENT's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueIDENTs(this List<ChartTraceNCV.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDuration)
                    group x by x.IDENT).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique IDENT's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueIDENTs(this List<ChartTraceOMV.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDuration)
                    group x by x.IDENT).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique test plans from the rows that
        /// match the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified, the
        /// pressure level is one of the pressure levels specified and the name is one of the
        /// names specified.
        /// </summary>
        /// <typeparam name="T">The row type.</typeparam>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="pressureLevels">A list of pressure levels to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        public static List<string> UniqueTestPlans<T>(this List<T> rows, UTIDPlantNumberKeys keys, List<string> pressureLevels, List<string> names) where T : UTIDRow
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && pressureLevels.Contains(x.PressureReq.ToString(CultureInfo.InvariantCulture))
                    && names.Contains(x.Name)
                    group x by x.TestPlan).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique test plans from the rows that
        /// match the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <typeparam name="T">The row type.</typeparam>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        public static List<string> UniqueTestPlans<T>(this List<T> rows, UTIDPlantNumberKeys keys, List<string> names) where T : UTIDRow
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.Name)
                    group x by x.TestPlan).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique test plans from the rows that
        /// match the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        public static List<string> UniqueTestPlans(this List<Leak.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.TestPoint.ToString(CultureInfo.InvariantCulture))
                    group x by x.TestPlan).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique test plans from the rows that
        /// match the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        public static List<string> UniqueTestPlans(this List<ChartTraceCLT.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDemand)
                    group x by x.TestPlan).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique test plans from the rows that
        /// match the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        public static List<string> UniqueTestPlans(this List<ChartTraceCLS.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDemand)
                    group x by x.TestPlan).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique test plans from the rows that
        /// match the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        public static List<string> UniqueTestPlans(this List<ChartTraceFOD.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedLobe1OMVDemand)
                    group x by x.TestPlan).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique test plans from the rows that
        /// match the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        public static List<string> UniqueTestPlans(this List<ChartTraceNCV.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDuration)
                    group x by x.TestPlan).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique test plans from the rows that
        /// match the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        public static List<string> UniqueTestPlans(this List<ChartTraceOMV.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDuration)
                    group x by x.TestPlan).Keys();
        }
                
        /// <summary>
        /// From the rows provided, returns the list of unique lines from the rows that
        /// match the filter criteria. Lines on different rigs are considered different even
        /// if they have the same line number.
        /// The filtered list includes rows where the UTID is one of the UTID's specified, the
        /// pressure level is one of the pressure levels specified and the name is one of the
        /// names specified.
        /// </summary>
        /// <typeparam name="T">The row type.</typeparam>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="pressureLevels">A list of pressure levels to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        public static List<int> UniqueLines<T>(this List<T> rows, UTIDPlantNumberKeys keys, List<string> pressureLevels, List<string> names) where T : UTIDRow
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && pressureLevels.Contains(x.PressureReq.ToString(CultureInfo.InvariantCulture))
                    && names.Contains(x.Name)
                    group x by x.LineNumber).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique LineNumber's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <typeparam name="T">The type of the rows to filter.</typeparam>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<int> UniqueLines<T>(this List<T> rows, UTIDPlantNumberKeys keys, List<string> names) where T : UTIDRow
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.Name)
                    group x by x.LineNumber).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique LineNumber's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<int> UniqueLines(this List<Leak.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.TestPoint.ToString(CultureInfo.InvariantCulture))
                    group x by x.LineNumber).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique LineNumber's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<int> UniqueLines(this List<ChartTraceCLT.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDemand)
                    group x by x.LineNumber).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique LineNumber's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<int> UniqueLines(this List<ChartTraceCLS.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDemand)
                    group x by x.LineNumber).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique LineNumber's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<int> UniqueLines(this List<ChartTraceFOD.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedLobe1OMVDemand)
                    group x by x.LineNumber).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique LineNumber's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<int> UniqueLines(this List<ChartTraceNCV.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDuration)
                    group x by x.LineNumber).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique LineNumber's from the rows that match 
        /// the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified and
        /// and the name is one of the names specified.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<int> UniqueLines(this List<ChartTraceOMV.Row> rows, UTIDPlantNumberKeys keys, List<string> names)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && names.Contains(x.SpeedPressureDuration)
                    group x by x.LineNumber).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the list of unique rigs from the rows that
        /// match the filter criteria.
        /// The filtered list includes rows where the UTID is one of the UTID's specified, the
        /// pressure level is one of the pressure levels specified and the name is one of the
        /// names specified.
        /// </summary>
        /// <typeparam name="T">The row type.</typeparam>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="keys">A list of keys to filter the rows by.</param>
        /// <param name="pressureLevels">A list of pressure levels to filter the rows by.</param>
        /// <param name="names">A list of names to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        public static List<short> UniqueRigs<T>(this List<T> rows, UTIDPlantNumberKeys keys, List<string> pressureLevels, List<string> names) where T : UTIDRow
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber))
                    && pressureLevels.Contains(x.PressureReq.ToString(CultureInfo.InvariantCulture))
                    && names.Contains(x.Name)
                    group x by x.PlantNumber).Keys();
        }

        /// <summary>
        /// From the rows provided, creates and returns a tree. The first level of the tree is 
        /// indexed by UTID. The second level of the tree is indexed by pressure level. The third
        /// level of the tree (leaves) is indexed by name.
        /// </summary>
        /// <typeparam name="T">The row type.</typeparam>
        /// <param name="rows">The rows to add to the tree.</param>
        /// <param name="orderRows">The method used to order the rows at each leaf.</param>
        /// <returns>The rows structures as a tree.</returns>
        // ReSharper disable once InconsistentNaming
        public static RowsTree<T, UTIDPlantNumberKey, string, string> GetUTIDPressureLevelStatisticTree<T>(this List<T> rows, Func<List<T>, List<T>> orderRows) where T : UTIDRow
        {
            var tree = new RowsTree<T, UTIDPlantNumberKey, string, string>();

            foreach (T row in rows)
            {
                tree.Add(new UTIDPlantNumberKey(row.UTID, row.PlantNumber), row.PressureReq.ToString(CultureInfo.InvariantCulture), row.Name, row);
            }

            tree.OrderLeaves(orderRows);

            return tree;
        }

        /// <summary>
        /// From the rows provided, creates and returns a tree. The first level of the tree is 
        /// indexed by UTID. The second level of the tree is indexed by pressure level. However,
        /// the pressure level is not supplied and so will always be an empty string. The third
        /// level of the tree (leaves) is indexed by name.
        /// </summary>
        /// <typeparam name="T">The row type.</typeparam>
        /// <param name="rows">The rows to add to the tree.</param>
        /// <param name="orderRows">The method used to order the rows at each leaf.</param>
        /// <returns>The rows structures as a tree.</returns>
        // ReSharper disable once InconsistentNaming
        public static RowsTree<T, UTIDPlantNumberKey, string, string> GetUTIDStatisticTree<T>(this List<T> rows, Func<List<T>, List<T>> orderRows) where T : UTIDRow
        {
            var tree = new RowsTree<T, UTIDPlantNumberKey, string, string>();

            foreach (T row in rows)
            {
                tree.Add(new UTIDPlantNumberKey(row.UTID, row.PlantNumber), string.Empty, row.Name, row);
            }

            tree.OrderLeaves(orderRows);

            return tree;
        }

        /// <summary>
        /// Adds a list of rows to another list of rows.
        /// </summary>
        /// <typeparam name="T">The row type.</typeparam>
        /// <param name="rows">The list of rows to add to.</param>
        /// <param name="moreRows">The list of rows to add.</param>
        public static void AddRows<T>(this List<T> rows, List<T> moreRows)
        {
            rows.AddRange(moreRows);
        }

        /// <summary>
        /// Returns a list of unique pressure levels from a list of rows.
        /// </summary>
        /// <typeparam name="T">The row type.</typeparam>
        /// <param name="rows">The list of rows.</param>
        /// <returns>List of unique pressure levels.</returns>
        public static List<short> UniquePressureLevels<T>(this List<T> rows) where T : NameRow
        {
            return (from x in rows group x by x.PressureReq).Keys();
        }

        /// <summary>
        /// Returns a list of unique names from a list of rows.
        /// </summary>
        /// <typeparam name="T">The row type.</typeparam>
        /// <param name="rows">The list of rows.</param>
        /// <returns>List of unique names.</returns>
        public static List<string> UniqueNames<T>(this List<T> rows) where T : NameRow
        {
            return (from x in rows group x by x.Name).Keys();
        }

        /// <summary>
        /// Returns a list of unique IDENTs from a list of rows.
        /// </summary>
        /// <typeparam name="T">The row type.</typeparam>
        /// <param name="rows">The list of rows.</param>
        /// <returns>List of unique IDENTs.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueIDENTs<T>(this List<T> rows) where T : NameRow
        {
            return (from x in rows group x by x.IDENT).Keys();
        }
    }
}
