﻿using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Static class used for retrieving information from lists of rows.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public static class ChartTraceFODRowsExt
    {
        /// <summary>
        /// From the rows provided, returns a list of rows that match the UTID and speed Lobe10MV
        /// demand.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="key">The key to filter the rows by.</param>
        /// <param name="speedLobe1OMVDemand">The speed Love10MV demand to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable InconsistentNaming
        public static List<ChartTraceFOD.Row> FilterOnUTIDAndSpeedLobe1OMVDemand(this List<ChartTraceFOD.Row> rows, UTIDPlantNumberKey key, string speedLobe1OMVDemand)
        // ReSharper restore InconsistentNaming
        {
            return (from x in rows
                    where (x.UTID == key.UTID) && (x.PlantNumber == key.PlantNumber) && (x.SpeedLobe1OMVDemand == speedLobe1OMVDemand)
                    orderby x.SpeedLobe1OMVDemand
                    select x).ToList();
        }

        /// <summary>
        /// From the rows provided, returns the rows grouped by speed Lobe10MV demand.
        /// </summary>
        /// <param name="rows">The rows to group.</param>
        /// <returns>A list of grouped rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> GroupBySpeedLobe1OMVDemand(this List<ChartTraceFOD.Row> rows)
        {
            return (from x in rows group x by x.SpeedLobe1OMVDemand).Keys();
        }

        /// <summary>
        /// From the rows provided, returns the unique speed Lobe10MV demands.
        /// </summary>
        /// <param name="rows">The rows to group.</param>
        /// <returns>A list of unique speed Lobe10MV demands.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueSpeedLobe1OMVDemands(this List<ChartTraceFOD.Row> rows)
        {
            return (from x in rows group x by x.SpeedLobe1OMVDemand).Keys();
        }

        /// <summary>
        /// From the rows provided filtered by the keys and speed Lobe10MV demands,
        /// returns the unique speed Lobe10MV demands.
        /// </summary>
        /// <param name="rows">The rows to group.</param>
        /// <param name="keys">The keys to filter the rows by.</param>
        /// <param name="speedLobe1OMVDemand">The speed Lobe10MV demand to filter the rows by.</param>
        /// <returns>A list of unique speed Lobe10MV demands.</returns>
        // ReSharper disable InconsistentNaming
        public static List<string> UniqueSpeedLobe1OMVDemands(this List<ChartTraceFOD.Row> rows, UTIDPlantNumberKeys keys, List<string> speedLobe1OMVDemand)
        // ReSharper restore InconsistentNaming
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber)) && speedLobe1OMVDemand.Contains(x.SpeedLobe1OMVDemand)
                    group x by x.SpeedLobe1OMVDemand).Keys();
        }

        /// <summary>
        /// From the rows provided filtered by the keys and speed pressure duration,
        /// returns the unique identifiers.
        /// </summary>
        /// <param name="rows">The rows to group.</param>
        /// <param name="keys">The keys to filter the rows by.</param>
        /// <param name="selectedSpeedPressureDuration">The speed pressure duration to filter the rows by.</param>
        /// <returns>A list of unique identifiers.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<string> UniqueIDENTS(this List<ChartTraceFOD.Row> rows, UTIDPlantNumberKeys keys, List<string> selectedSpeedPressureDuration)
        {
            return (from x in rows
                    where keys.Contains(new UTIDPlantNumberKey(x.UTID, x.PlantNumber)) && selectedSpeedPressureDuration.Contains(x.SpeedLobe1OMVDemand)
                    group x by x.IDENT).Keys();
        }
    }
}
