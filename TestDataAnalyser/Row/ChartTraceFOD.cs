﻿using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.ChartTraceFOD
{
    /// <summary>
    /// Represents a single record (one test point for one injector) of fixed OMV demand chart trace data.
    /// </summary>
    public class Row : ChartTrace.Row
    {
        /// <summary>Gets SpeedLobe1OMVDemand.</summary>
// ReSharper disable once InconsistentNaming
        public string SpeedLobe1OMVDemand { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="ChartTraceFOD.Row"/> class.
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        public override void Initialise(SqlDataReader reader)
        {
            Initialise(
                reader, 
                UTIDRowParameters.PlantNumber, 
                UTIDRowParameters.LineNumber, 
                UTIDRowParameters.UTID, 
                UTIDRowParameters.ISN,
                UTIDRowParameters.LOP, 
                UTIDRowParameters.TestPlan, 
                UTIDRowParameters.Global, 
                UTIDRowParameters.URID);
            Initialise(reader, ChartTraceRowParameters.All);

            SpeedLobe1OMVDemand = ReadString(reader, "SpeedLobe1OMVDemand");
        }
    }
}
