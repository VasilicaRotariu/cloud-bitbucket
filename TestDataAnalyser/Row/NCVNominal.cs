﻿using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.NCVNominal
{
    /// <summary>
    /// Represents a single record (one test point for one injector) of nominal open loop data.
    /// </summary>
    public class Row : NameRow
    {
        /// <summary>Gets test point duration.</summary>
        public short Duration { get; private set; }

        /// <summary>Gets measured parameter median.</summary>
        public double Median { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="NCVNominal.Row"/> class.
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        public override void Initialise(SqlDataReader reader)
        {
            Initialise(reader, NameRowParameters.PressureReq, NameRowParameters.Name);

            Median = ReadDoubleFromString(reader, "Median");
            Duration = ReadShort(reader, Queries.Parameters.QueryParameters.HeaderOLTableDurationColumn);
        }
    }
}
