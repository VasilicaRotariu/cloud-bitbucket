﻿using System;
using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Base class for all row classes.
    /// </summary>
    public abstract class RowBase
    {
        /// <summary>
        /// Initialises a row by reading data from the SQL data reader and storing the
        /// data in the rows parameters.
        /// </summary>
        /// <param name="reader">SQL data reader used to read a row from.</param>
        public abstract void Initialise(SqlDataReader reader);

        /// <summary>
        /// Reads the named value from the SQL data reader and returns the value
        /// as a string.
        /// </summary>
        /// <param name="reader">SQL data reader used to read the data from.</param>
        /// <param name="key">The name of the parameter to read</param>
        /// <returns>The string value or an empty string if a value could not be read.</returns>
        public string ReadString(SqlDataReader reader, string key)
        {
            if (key.IsEmpty()) return string.Empty;

            try
            {
                return reader[key].ToString();
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Reads the named value from the SQL data reader and returns the value
        /// as an integer.
        /// </summary>
        /// <param name="reader">SQL data reader used to read the data from.</param>
        /// <param name="key">The name of the parameter to read</param>
        /// <returns>The integer value or zero if a value could not be read.</returns>
        public int ReadInt(SqlDataReader reader, string key)
        {
            if (key.IsEmpty()) return 0;

            if (reader[key] is int)
            {
                return (int)reader[key];
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Reads the named value from the SQL data reader and returns the string
        /// value converted to an integer.
        /// </summary>
        /// <param name="reader">SQL data reader used to read the data from.</param>
        /// <param name="key">The name of the parameter to read</param>
        /// <returns>The integer value or zero if a value could not be read.</returns>
        public int ReadIntFromString(SqlDataReader reader, string key)
        {
            if (key.IsEmpty()) return 0;

            try
            {
                return ReadString(reader, key).ToInt();
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Reads the named value from the SQL data reader and returns the value
        /// as a short.
        /// </summary>
        /// <param name="reader">SQL data reader used to read the data from.</param>
        /// <param name="key">The name of the parameter to read</param>
        /// <returns>The short value or zero if a value could not be read.</returns>
        public short ReadShort(SqlDataReader reader, string key)
        {
            if (key.IsEmpty()) return 0;

            if (reader[key] is short)
            {
                return (short)reader[key];
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Reads the named value from the SQL data reader and returns the value
        /// as a long.
        /// </summary>
        /// <param name="reader">SQL data reader used to read the data from.</param>
        /// <param name="key">The name of the parameter to read</param>
        /// <returns>The long value or zero if a value could not be read.</returns>
        public long ReadLong(SqlDataReader reader, string key)
        {
            if (key.IsEmpty()) return 0;

            if (reader[key] is long)
            {
                return (long)reader[key];
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Reads the named value from the SQL data reader and returns the value
        /// as a long.
        /// </summary>
        /// <param name="reader">SQL data reader used to read the data from.</param>
        /// <param name="key">The name of the parameter to read</param>
        /// <returns>The long value or zero if a value could not be read.</returns>
        public double ReadDouble(SqlDataReader reader, string key)
        {
            if (key.IsEmpty()) return 0.0;

            if (reader[key] is double)
            {
                return (double)reader[key];
            }
            else
            {
                return 0.0;
            }
        }

        /// <summary>
        /// Reads the named value from the SQL data reader and returns the string
        /// value converted to a double.
        /// </summary>
        /// <param name="reader">SQL data reader used to read the data from.</param>
        /// <param name="key">The name of the parameter to read</param>
        /// <returns>The double value or zero if a value could not be read.</returns>
        public double ReadDoubleFromString(SqlDataReader reader, string key)
        {
            if (key.IsEmpty()) return 0.0;

            try
            {
                return ReadString(reader, key).ToDouble();
            }
            catch
            {
                return 0.0;
            }
        }

        /// <summary>
        /// Reads the named value from the SQL data reader and returns the string
        /// value converted to a float.
        /// </summary>
        /// <param name="reader">SQL data reader used to read the data from.</param>
        /// <param name="key">The name of the parameter to read</param>
        /// <returns>The double value or zero if a value could not be read.</returns>
        public float ReadFloat(SqlDataReader reader, string key)
        {
            if (key.IsEmpty()) return 0.0f;

            try
            {
                return ReadString(reader, key).ToFloat();
            }
            catch
            {
                return 0.0f;
            }
        }

        /// <summary>
        /// Reads the named value from the SQL data reader and returns the value
        /// as a DateTime.
        /// </summary>
        /// <param name="reader">SQL data reader used to read the data from.</param>
        /// <param name="key">The name of the parameter to read</param>
        /// <returns>The DateTime value or a new DateTime if a value could not be read.</returns>
        public DateTime ReadDateTime(SqlDataReader reader, string key)
        {
            if (key.IsEmpty()) return new DateTime();

            if (reader[key] is DateTime)
            {
                return (DateTime)reader[key];
            }
            else
            {
                return new DateTime();
            }
        }

        /// <summary>
        /// Reads the named value from the SQL data reader and returns the string
        /// value converted to a bool. (0 = false; 1 = true)
        /// </summary>
        /// <param name="reader">SQL data reader used to read the data from.</param>
        /// <param name="key">The name of the parameter to read</param>
        /// <returns>The bool value or false if a value could not be read.</returns>
        public bool ReadBoolFromString(SqlDataReader reader, string key)
        {
            if (key.IsEmpty()) return false;

            try
            {
                return ReadString(reader, key) == "True" ? true : false;
            }
            catch
            {
                return false;
            }
        }
    }
}
