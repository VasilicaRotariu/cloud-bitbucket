﻿using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.Trend
{
    /// <summary>
    /// Represents a single trend trace record.
    /// </summary>
    public class Row : RowBase
    {
        /// <summary>Gets PlantNumber.</summary>
        public short PlantNumber { get; private set; }

        /// <summary>Gets UTID.</summary>
        // ReSharper disable once InconsistentNaming
        public int UTID { get; private set; }

        /// <summary>Gets byte array (BLOB) for trend trace data.</summary>
        public byte[] Trace { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="Trend.Row"/> class.
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        public override void Initialise(SqlDataReader reader)
        {
            PlantNumber = ReadShort(reader, "PlantNumber");
            UTID = ReadInt(reader, "UTID");
            Trace = (byte[])reader["Trace"];
        }
    }
}
