﻿using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Static class used for retrieving information from lists of rows.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public static class NCVNominalRowsExt
    {
        /// <summary>
        /// From the rows provided, returns a list of rows that match the IDENT and pressure
        /// and name.
        /// </summary>
        /// <param name="rows">The rows to filter.</param>
        /// <param name="ident">The IDENT to filter the rows by.</param>
        /// <param name="pressure">The pressure to filter the rows by.</param>
        /// <param name="name">The name to filter the rows by.</param>
        /// <returns>A list of the filtered rows.</returns>
        // ReSharper disable once InconsistentNaming
        public static List<NCVNominal.Row> FilterOnIDENTPressureAndName(this List<NCVNominal.Row> rows, string ident, string pressure, string name)
        {
            return (from x in rows
                    where (x.IDENT == ident) && (x.PressureReq == int.Parse(pressure)) && (x.Name == name)
                    orderby x.Duration
                    select x).ToList();
        }
    }
}
