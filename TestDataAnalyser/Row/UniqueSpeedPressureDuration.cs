﻿using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.UniqueSpeedPressureDuration
{
    /// <summary>
    /// Rows returned from a unique speed pressure duration query.
    /// </summary>
    public class Row : RowBase
    {
        /// <summary>
        /// Gets or sets the speed pressure duration column in the row.
        /// </summary>
        public string SpeedPressureDuration { get; protected set; }

        /// <summary>
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a row.
        /// </summary>
        /// <param name="reader">SQLDataReader used to read row from.</param>
        public override void Initialise(SqlDataReader reader)
        {
            SpeedPressureDuration = ReadString(reader, "SpeedPressureDuration");
        }
    }
}
