﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SudburyDataAnalyser.ChartTraceCLS
{
    /// <summary>
    /// Data row for CLS Chart Trace
    /// </summary>
    public class Row : ChartTrace.Row
    {
        /// <summary>Gets speed pressure demand.</summary>
        public string SpeedPressureDemand { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="ChartTraceCLT.Row"/> class.
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        public override void Initialise(System.Data.SqlClient.SqlDataReader reader)
        {
            Initialise(
                reader,
                UTIDRowParameters.PlantNumber,
                UTIDRowParameters.LineNumber,
                UTIDRowParameters.UTID,
                UTIDRowParameters.ISN,
                UTIDRowParameters.LOP,
                UTIDRowParameters.TestPlan,
                UTIDRowParameters.Global,
                UTIDRowParameters.URID);
            Initialise(reader, ChartTraceRowParameters.All);

            SpeedPressureDemand = ReadString(reader, "SpeedPressureDemand");
        }
    }
}
