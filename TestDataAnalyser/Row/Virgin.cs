﻿using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.Virgin
{
    /// <summary>
    /// Represents a single record from a Virgin query.
    /// </summary>
    public class Row : RowBase
    {
        /// <summary>Gets the row injector serial number.</summary>
        public string InjectorSerial { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="Virgin.Row"/> class.
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        public override void Initialise(SqlDataReader reader)
        {
            InjectorSerial = ReadString(reader, "ISN");
        }
    }
}
