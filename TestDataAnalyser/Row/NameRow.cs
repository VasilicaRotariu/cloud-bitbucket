﻿using System.Data.SqlClient;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Base class for all named row classes.
    /// </summary>
    public abstract class NameRow : RowBase
    {
        /// <summary>
        /// Enumeration used to identify the fields to be initialised.
        /// </summary>
        public enum NameRowParameters
        {
            // ReSharper disable once InconsistentNaming
            IDENT,
            PressureReq,
            Name,
            All,
        }

        /// <summary>Gets or sets IDENT.</summary>
        // ReSharper disable once InconsistentNaming
        public string IDENT { get; set; }

        /// <summary>Gets or sets test point pressure.</summary>
        public short PressureReq { get; protected set; }

        /// <summary>Gets or sets measured parameter name.</summary>
        public string Name { get; protected set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="NameRow"/> class.
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        /// <param name="parameters">List of parameters to initialise</param>
        public void Initialise(SqlDataReader reader, params NameRowParameters[] parameters)
        {
            if (parameters.First() == NameRowParameters.All)
            {
                IDENT = ReadString(reader, Queries.Parameters.QueryParameters.HeaderTableIDENTColumn);
                PressureReq = ReadShort(reader, "PressureReq");
                Name = ReadString(reader, "Name");
            }
            else
            {
                foreach (var parameter in parameters)
                {
                    switch (parameter)
                    {
                        case NameRowParameters.IDENT:
                            {
                                IDENT = ReadString(reader, Queries.Parameters.QueryParameters.HeaderTableIDENTColumn);
                            }
                            break;
                        case NameRowParameters.PressureReq:
                            {
                                PressureReq = ReadShort(reader, "PressureReq");
                            }
                            break;
                        case NameRowParameters.Name:
                            {
                                Name = ReadString(reader, "Name");
                            }
                            break;
                    }
                }
            }
        }
    }
}
