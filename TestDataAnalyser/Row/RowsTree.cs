﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Class used to create and represent a tree structure of rows.
    /// The tree is fixed in depth at three levels.
    /// The types for the keys to the tree are K1 (for the first level in the tree),
    /// K2 (for the second level in the tree) and K3 (for the third level in the tree).
    /// The leaves are rows of type R.
    /// </summary>
    /// <typeparam name="T">The row type.</typeparam>
    /// <typeparam name="TK1">The type of the key at the first level of the tree.</typeparam>
    /// <typeparam name="TK2">The type of the key at the second level of the tree.</typeparam>
    /// <typeparam name="TK3">The type of the key at the third level of the tree.</typeparam>
    public class RowsTree<T, TK1, TK2, TK3> where T : UTIDRow
    {
        /// <summary>
        /// The top of the tree.
        /// </summary>
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private Dictionary<TK1, Dictionary<TK2, Dictionary<TK3, List<T>>>> tree = new Dictionary<TK1, Dictionary<TK2, Dictionary<TK3, List<T>>>>();

        /// <summary>
        /// Add a row to the tree.
        /// </summary>
        /// <param name="k1">Level one key.</param>
        /// <param name="k2">Level two key.</param>
        /// <param name="k3">Level three key.</param>
        /// <param name="row">The row to add to the tree.</param>
        public void Add(TK1 k1, TK2 k2, TK3 k3, T row)
        {
            // Get or add the level one key
            Dictionary<TK2, Dictionary<TK3, List<T>>> level1Tree;
            if (!tree.TryGetValue(k1, out level1Tree))
            {
                level1Tree = new Dictionary<TK2, Dictionary<TK3, List<T>>>();
                tree[k1] = level1Tree;
            }

            // Get or add the level two key
            Dictionary<TK3, List<T>> level2Tree;
            if (!level1Tree.TryGetValue(k2, out level2Tree))
            {
                level2Tree = new Dictionary<TK3, List<T>>();
                level1Tree[k2] = level2Tree;
            }

            // Get or add the level three key
            List<T> rows;
            if (!level2Tree.TryGetValue(k3, out rows))
            {
                rows = new List<T>();
                level2Tree[k3] = rows;
            }

            // Add the row
            rows.Add(row);
        }

        /// <summary>
        /// Get a row from the tree.
        /// </summary>
        /// <param name="k1">Level one key.</param>
        /// <param name="k2">Level two key.</param>
        /// <param name="k3">Level three key.</param>
        /// <returns>The row from the tree or a default row if one of the keys was not in the tree.</returns>
        public List<T> Get(TK1 k1, TK2 k2, TK3 k3)
        {
            Dictionary<TK2, Dictionary<TK3, List<T>>> level1Tree;
            if (tree.TryGetValue(k1, out level1Tree))
            {
                Dictionary<TK3, List<T>> level2Tree;
                if (level1Tree.TryGetValue(k2, out level2Tree))
                {
                    List<T> rows;
                    if (level2Tree.TryGetValue(k3, out rows))
                    {
                        return rows;
                    }
                }
            }

            return new List<T>();
        }

        /// <summary>
        /// Gets a list of level three keys.
        /// </summary>
        /// <param name="k1">Level one key.</param>
        /// <param name="k2">Level two key.</param>
        /// <returns>The list of level three keys or an empty list if the level one key or level two key was not in the tree.</returns>
        public List<TK3> Get(TK1 k1, TK2 k2)
        {
            Dictionary<TK2, Dictionary<TK3, List<T>>> level1Tree;
            if (tree.TryGetValue(k1, out level1Tree))
            {
                Dictionary<TK3, List<T>> level2Tree;
                if (level1Tree.TryGetValue(k2, out level2Tree))
                {
                    return level2Tree.Keys.ToList();
                }
            }

            return new List<TK3>();
        }

        /// <summary>
        /// Orders the rows at the leaves.
        /// </summary>
        /// <param name="orderRows">Method used to order the rows.</param>
        public void OrderLeaves(Func<List<T>, List<T>> orderRows)
        {
            var treeStructure = new Dictionary<TK1, Dictionary<TK2, List<TK3>>>();

            foreach (var k1 in tree.Keys)
            {
                treeStructure[k1] = new Dictionary<TK2, List<TK3>>();
                foreach (var k2 in tree[k1].Keys)
                {
                    treeStructure[k1][k2] = new List<TK3>();
                    foreach (var k3 in tree[k1][k2].Keys)
                    {
                        treeStructure[k1][k2].Add(k3);
                    }
                }
            }

            foreach (var k1 in treeStructure.Keys)
            {
                foreach (var k2 in treeStructure[k1].Keys)
                {
                    foreach (var k3 in treeStructure[k1][k2])
                    {
                        tree[k1][k2][k3] = orderRows(tree[k1][k2][k3]);
                    }
                }
            }
        }
    }
}
