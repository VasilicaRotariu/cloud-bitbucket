﻿using System;
using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.LimitsBase
{
    /// <summary>
    /// Represents a single record of limit data.
    /// </summary>
    public class Row : RowBase, IComparable
    {
        /// <summary>
        /// Represents a minimum and maximum value, initialised as a string in the form
        /// "min:max" (e.g. "3:6") or "min max" (e.g. "4").
        /// </summary>
        public class MinMaxValues
        {
            /// <summary>
            /// Gets or sets the minimum value.
            /// </summary>
            public float Min { get; set; }

            /// <summary>
            /// Gets or sets the maximum value.
            /// </summary>
            public float Max { get; set; }

            /// <summary>
            /// Initialises a new instance of the <see cref="MinMaxValues"/> class.
            /// </summary>
            /// <param name="values">String value to initialise the min and max values.</param>
            public MinMaxValues(string values)
            {
                string[] splitValues = values.Split(':');
                if (splitValues.Length > 1)
                {
                    float min, max;

                    float.TryParse(splitValues[0], out min);
                    float.TryParse(splitValues[1], out max);

                    Min = min;
                    Max = max;
                }
                else
                {
                    float value;

                    float.TryParse(splitValues[0], out value);

                    Min = Max = value;
                }
            }
        }

        /// <summary>
        /// Compares this row with another row.
        /// </summary>
        /// <param name="obj">Another row.</param>
        /// <returns>A value that indicates the relative order of the rows being compared.</returns>
        public int CompareTo(object obj)
        {
            var other = obj as Row;
            if (other == null) return -1;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (FuelRange.Min == other.FuelRange.Min)
            {
                return FuelRange.Max.CompareTo(other.FuelRange.Max);
            }

            return FuelRange.Min.CompareTo(other.FuelRange.Min);
        }

        /// <summary>
        /// Gets the fuel range.
        /// </summary>
        public MinMaxValues FuelRange { get; private set; }

        /// <summary>
        /// Gets or sets the minimum values.
        /// </summary>
        public MinMaxValues Min { get; set; }

        /// <summary>
        /// Gets or sets the maximum value.
        /// </summary>
        public MinMaxValues Max { get; set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="LimitsBase.Row"/> class.
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        public override void Initialise(SqlDataReader reader)
        {
            FuelRange = new MinMaxValues(ReadString(reader, "FuelRange"));
            Min = new MinMaxValues(ReadString(reader, "Min"));
            Max = new MinMaxValues(ReadString(reader, "Max"));
        }
    }
}
