﻿using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.ChartTrace
{
    /// <summary>
    /// Base class for all chart trace row classes.
    /// </summary>
    public abstract class Row : UTIDRow
    {
        /// <summary>
        /// Enumeration used to identify the fields to be initialised.
        /// </summary>
        public enum ChartTraceRowParameters
        {
            All,
        }

        /// <summary>Gets or sets TestPlan.</summary>
        public string Testplan { get; protected set; }

        /// <summary>Gets or sets the BinaryTraceData.</summary>
        public byte[] BinaryTraceData { get; protected set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="ChartTrace.Row"/> class.
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        /// <param name="parameters">List of parameters to initialise</param>
        public void Initialise(SqlDataReader reader, params ChartTraceRowParameters[] parameters)
        {
            IDENT = ReadString(reader, Queries.Parameters.QueryParameters.TraceTableIdentColumn);
            BinaryTraceData = (byte[])reader[Queries.Parameters.QueryParameters.TraceTableBinaryTraceDataColumn];
        }
    }
}
