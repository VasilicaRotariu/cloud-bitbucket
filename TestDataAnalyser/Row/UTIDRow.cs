﻿using System;
using System.Data.SqlClient;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Base class for all UTID row classes.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public abstract class UTIDRow : NameRow
    {
        /// <summary>
        /// Enumeration used to identify the fields to be initialised.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public enum UTIDRowParameters
        {
            // ReSharper disable InconsistentNaming
            PlantNumber,
            LineNumber,
            UTID,
            PDate,
            ISN,
            LOP,
            TestPlan,
            TestType,
            Global,
            URID,
            SpeedReq,
            All
            // ReSharper restore InconsistentNaming
        }

        /// <summary>Gets or sets PlantNumber.</summary>
        public short PlantNumber { get; protected set; }

        /// <summary>Gets or sets LineNumber.</summary>
        public int LineNumber { get; protected set; }

        /// <summary>Gets or sets UTID.</summary>
        // ReSharper disable once InconsistentNaming
        public int UTID { get; protected set; }

        /// <summary>Gets or sets PDate.</summary>
        public DateTime PDate { get; protected set; }

        /// <summary>Gets or sets ISN.</summary>
        // ReSharper disable once InconsistentNaming
        public string ISN { get; protected set; }

        /// <summary>Gets or sets LOP.</summary>
        // ReSharper disable once InconsistentNaming
        public string LOP { get; protected set; }

        /// <summary>Gets or sets TestPlan.</summary>
        public string TestPlan { get; protected set; }

        /// <summary>Gets or sets TestType.</summary>
        public string TestType { get; protected set; }

        /// <summary>Gets or sets Global pass/fail flag.</summary>
        public int Global { get; protected set; }

        /// <summary>Gets or sets URID.</summary>
        // ReSharper disable once InconsistentNaming
        public long URID { get; protected set; }

        /// <summary>Gets or sets test point speed.</summary>
        public short SpeedReq { get; protected set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="NameRow"/> class.
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        /// <param name="parameters">List of parameters to initialise</param>
        public void Initialise(SqlDataReader reader, params UTIDRowParameters[] parameters)
        {
            if (parameters.First() == UTIDRowParameters.All)
            {
                PlantNumber = ReadShort(reader, "PlantNumber");
                LineNumber = ReadIntFromString(reader, "LineNumber");
                UTID = ReadInt(reader, "UTID");
                PDate = ReadDateTime(reader, "PDate");
                ISN = ReadString(reader, "ISN");
                LOP = ReadString(reader, "LOP");
                TestPlan = ReadString(reader, "Testplan");
                TestType = ReadString(reader, "TestType");
                Global = ReadInt(reader, "Glob");
                URID = ReadLong(reader, "URID");
                SpeedReq = ReadShort(reader, "SpeedReq");
            }
            else
            {
                foreach (var parameter in parameters)
                {
                    switch (parameter)
                    {
                        case UTIDRowParameters.PlantNumber:
                            {
                                PlantNumber = ReadShort(reader, "PlantNumber");
                            }
                            break;
                        case UTIDRowParameters.LineNumber:
                            {
                                LineNumber = ReadIntFromString(reader, "LineNumber");
                            }
                            break;
                        case UTIDRowParameters.UTID:
                            {
                                UTID = ReadInt(reader, "UTID");
                            }
                            break;
                        case UTIDRowParameters.PDate:
                            {
                                PDate = ReadDateTime(reader, "PDate");
                            }
                            break;
                        case UTIDRowParameters.ISN:
                            {
                                ISN = ReadString(reader, "ISN");
                            }
                            break;
                        case UTIDRowParameters.LOP:
                            {
                                LOP = ReadString(reader, "LOP");
                            }
                            break;
                        case UTIDRowParameters.TestPlan:
                            {
                                TestPlan = ReadString(reader, "Testplan");
                            }
                            break;
                        case UTIDRowParameters.TestType:
                            {
                                TestType = ReadString(reader, "TestType");
                            }
                            break;
                        case UTIDRowParameters.Global:
                            {
                                Global = ReadInt(reader, "Glob");
                            }
                            break;
                        case UTIDRowParameters.URID:
                            {
                                URID = ReadLong(reader, "URID");
                            }
                            break;
                        case UTIDRowParameters.SpeedReq:
                            {
                                SpeedReq = ReadShort(reader, "SpeedReq");
                            }
                            break;
                    }
                }
            }
        }
    }
}
