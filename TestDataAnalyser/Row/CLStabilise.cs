﻿using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.CLStabilise
{
    /// <summary>
    /// Represents a single record (one test point for one injector) of closed loop stabilise data.
    /// Note that this is only for new (non-FT) database structure as stabilise doesn't exist in old database.
    /// </summary>
    public class Row : UTIDRow
    {
        /// <summary>Gets Pass off specification.</summary>
        public string PassOffSpec { get; private set; }

        /// <summary>Gets fuel demand 1.</summary>
        public short FuelDemand1 { get; private set; }

        /// <summary>Gets fuel demand 2.</summary>
        public short FuelDemand2 { get; private set; }

        /// <summary>Gets fuel demand 3.</summary>
        public short FuelDemand3 { get; private set; }

        /// <summary>Gets logic 1.</summary>
        public short Logic1 { get; private set; }

        /// <summary>Gets logic 2.</summary>
        public short Logic2 { get; private set; }

        /// <summary>Gets logic 3.</summary>
        public short Logic3 { get; private set; }

        /// <summary>Gets separation 1.</summary>
        public short Separation1 { get; private set; }

        /// <summary>Gets separation 2.</summary>
        public short Separation2 { get; private set; }

        /// <summary>Gets test point shot count.</summary>
        public int Count { get; private set; }

        /// <summary>Gets measured parameter median.</summary>
        public double Median { get; private set; }

        /// <summary>Gets measured parameter mean.</summary>
        public double Mean { get; private set; }

        /// <summary>Gets measured parameter minimum.</summary>
        public double Min { get; private set; }

        /// <summary>Gets measured parameter maximum.</summary>
        public double Max { get; private set; }

        /// <summary>Gets measured parameter standard deviation.</summary>
        // ReSharper disable once InconsistentNaming
        public double SD { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="CLStabilise.Row"/> class.
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        public override void Initialise(SqlDataReader reader)
        {
            Initialise(
                reader, 
                UTIDRowParameters.PlantNumber, 
                UTIDRowParameters.LineNumber, 
                UTIDRowParameters.UTID, 
                UTIDRowParameters.PDate,
                UTIDRowParameters.ISN, 
                UTIDRowParameters.LOP, 
                UTIDRowParameters.TestPlan, 
                UTIDRowParameters.Global, 
                UTIDRowParameters.URID, 
                UTIDRowParameters.SpeedReq);
            Initialise(reader, NameRowParameters.PressureReq, NameRowParameters.Name);

            PassOffSpec = ReadString(reader, "PassOffSpec");
            FuelDemand1 = ReadShort(reader, "FuelDemand1");
            FuelDemand2 = ReadShort(reader, "FuelDemand2");
            FuelDemand3 = ReadShort(reader, "FuelDemand3");
            Logic1 = ReadShort(reader, "Logic1");
            Logic2 = ReadShort(reader, "Logic2");
            Logic3 = ReadShort(reader, "Logic3");
            Separation1 = ReadShort(reader, "Separation1");
            Separation2 = ReadShort(reader, "Separation2");
            Count = ReadIntFromString(reader, "Count");
            Median = ReadDoubleFromString(reader, "Median");
            Mean = ReadDoubleFromString(reader, "Mean");
            Min = ReadDoubleFromString(reader, "Min");
            Max = ReadDoubleFromString(reader, "Max");
            SD = ReadDoubleFromString(reader, "SD");
        }
    }
}
