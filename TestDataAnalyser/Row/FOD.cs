﻿using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.FOD
{
    /// <summary>
    /// Represents a single record (one test point for one injector) of fixed OMV demand data.
    /// </summary>
    public class Row : UTIDRow
    {
        /// <summary>Gets test point cam lobe 1 OMV demand.</summary>
        // ReSharper disable once InconsistentNaming
        public int Lobe1OMVDemand { get; private set; }

        /// <summary>Gets test point cam lobe 2 OMV demand.</summary>
        // ReSharper disable once InconsistentNaming
        public int Lobe2OMVDemand { get; private set; }

        /// <summary>Gets test point shot count.</summary>
        public int Count { get; private set; }

        /// <summary>Gets measured parameter median.</summary>
        public double Median { get; private set; }

        /// <summary>Gets measured parameter mean.</summary>
        public double Mean { get; private set; }

        /// <summary>Gets measured parameter minimum.</summary>
        public double Min { get; private set; }

        /// <summary>Gets measured parameter maximum.</summary>
        public double Max { get; private set; }

        /// <summary>Gets measured parameter standard deviation.</summary>
        // ReSharper disable once InconsistentNaming
        public double SD { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="FOD.Row"/> class.
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        public override void Initialise(SqlDataReader reader)
        {
            Initialise(reader, UTIDRowParameters.All);
            Initialise(reader, NameRowParameters.IDENT, NameRowParameters.Name);

            Lobe1OMVDemand = ReadIntFromString(reader, "Lobe1OMVDemand");
            Lobe2OMVDemand = ReadIntFromString(reader, "Lobe2OMVDemand");
            Count = ReadIntFromString(reader, "Count");
            Median = ReadDoubleFromString(reader, "Median");
            Mean = ReadDoubleFromString(reader, "Mean");
            Min = ReadDoubleFromString(reader, "Min");
            Max = ReadDoubleFromString(reader, "Max");
            SD = ReadDoubleFromString(reader, "SD");
        }
    }
}
