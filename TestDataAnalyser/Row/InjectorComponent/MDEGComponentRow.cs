﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SudburyDataAnalyser.MDEGInjectorComponentData
{
    /// <summary>
    /// Represents a single record from an InjectorComponentData query.
    /// </summary>
    public class Row : RowBase
    {
        /// <summary>Gets the row the date of the assembly.</summary>
        public DateTime AssemblyDate { get; private set; }

        /// <summary>Gets the row injector serial number.</summary>
        public string InjectorSerial { get; private set; }

        /// <summary>Gets the nozzle serial number.</summary>
        public string NozzleSerial { get; private set; }

        /// <summary>Gets the NCV serial number.</summary>
        public string NCVSerial { get; private set; }

        /// <summary>Gets or sets the piston guide serial number.</summary>
        public string PistonGuideSerial { get; set; }

        /// <summary>Gets or sets the nozzle count.</summary>
        public int NozzleAssemblyCount { get; set; }

        /// <summary>Gets or sets the NCV count.</summary>
        public int NCVAssemblyCount { get; set; }

        /// <summary>Gets or sets the piston guide count.</summary>
        public int PGAssemblyCount { get; set; }

        /// <summary>Gets or sets the Fag</summary>
        public double Fag { get; set; }

        /// <summary>Gets or sets the Tilt</summary>
        public double Tilt { get; set; }

        /// <summary>Gets or sets the NeedleInFlow</summary>
        public double NeedleInFlow { get; set; }

        /// <summary>Gets or sets the NeedleOutFlow</summary>
        public double NeedleOutFlow { get; set; }

        /// <summary>Gets or sets the NeedleLift</summary>
        public double NeedleLift { get; set; }

        /// <summary>Gets or sets or sets the NCVClearance</summary>
        public double NCVClearance { get; set; }

        /// <summary>Gets or sets or sets the NCVLift</summary>
        public double NCVLift { get; set; }

        /// <summary>Gets the RdO</summary>
        public double RdO { get; private set; }

        /// <summary>Gets the InO</summary>
        public double InO { get; private set; }

        /// <summary>Gets the RDFlow</summary>
        public double RDFlow { get; private set; }

        /// <summary>Gets the INOFlow</summary>
        public double INOFlow { get; private set; }

        /// <summary>Gets the SPO</summary>
        public double SPO { get; private set; } 

        /// <summary>
        /// Initialises a new instance of the <see cref="VirginData.Row"/> class.
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        public override void Initialise(SqlDataReader reader)
        {
            AssemblyDate = ReadDateTime(reader, "AssemblyDate");
            InjectorSerial = ReadString(reader, "ISN");
            NozzleSerial = ReadString(reader, "NozzleSerial");
            NCVSerial = ReadString(reader, "NCVSerial");
            PistonGuideSerial = ReadString(reader, "PistonGuideSerial");
            NozzleAssemblyCount = ReadInt(reader, "NozzleCount");
            NCVAssemblyCount = ReadInt(reader, "NCVCount");
            PGAssemblyCount = ReadInt(reader, "PistonGuideCount");

            Fag = ReadDouble(reader, "Fag");
            Tilt = ReadDouble(reader, "Tilt");
            NeedleInFlow = ReadDouble(reader, "NeedleInFlow");
            NeedleOutFlow = ReadDouble(reader, "NeedleOutFlow");
            NeedleLift = ReadDouble(reader, "NeedleLift");
            NCVClearance = ReadDouble(reader, "NCVClearance");
            NCVLift = ReadDouble(reader, "NCVLift");
            RdO = ReadDouble(reader, "RdO");
            InO = ReadDouble(reader, "InO");

            RDFlow = ReadDouble(reader, "RDFlow");
            INOFlow = ReadDouble(reader, "INOFlow");
            SPO = ReadDouble(reader, "SPO");
        }
    } 
}
