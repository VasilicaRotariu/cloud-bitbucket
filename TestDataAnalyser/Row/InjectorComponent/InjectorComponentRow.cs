﻿using System;
using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.InjectorComponentData
{
    /// <summary>
    /// Represents a single record from an InjectorComponentData query.
    /// </summary>
    public class Row : RowBase
    {
        /// <summary>Gets the row the date of the assembly.</summary>
        public DateTime AssemblyDate { get; private set; }

        /// <summary>Gets the row injector serial number.</summary>
        public string InjectorSerial { get; private set; }

        /// <summary>Gets the nozzle serial number.</summary>
        public string NozzleSerial { get; private set; }

        /// <summary>Gets the NCV serial number.</summary>
        public string NCVSerial { get; private set; }

        /// <summary>Gets the piston guide serial number.</summary>
        public string PistonGuideSerial { get; private set; }

        /// <summary>Gets the nozzle count.</summary>
        public int NozzleAssemblyCount { get; private set; }

        /// <summary>Gets the NCV count.</summary>
        public int NCVAssemblyCount { get; private set; }

        /// <summary>Gets the piston guide count.</summary>
        public int PGAssemblyCount { get; private set; }

        /// <summary>Gets the Fag</summary>
        public double Fag { get; private set; }

        /// <summary>Gets the Tilt</summary>
        public double Tilt { get; private set; }

        /// <summary>Gets the NeedleInFlow</summary>
        public double NeedleInFlow { get; private set; }

        /// <summary>Gets the NeedleOutFlow</summary>
        public double NeedleOutFlow { get; private set; }

        /// <summary>Gets the NeedleLift</summary>
        public double NeedleLift { get; private set; }

        /// <summary>Gets the NCVClearance</summary>
        public double NCVClearance { get; private set; }

        /// <summary>Gets the NCVLift</summary>
        public double NCVLift { get; private set; }

        /// <summary>Gets the RdO</summary>
        public double RdO { get; private set; }

        /// <summary>Gets the InO</summary>
        public double InO { get; private set; } 
        
        /// <summary>
        /// Initialises a new instance of the <see cref="VirginData.Row"/> class.
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        public override void Initialise(SqlDataReader reader)
        {
            AssemblyDate = ReadDateTime(reader, "AssemblyDate");
            InjectorSerial = ReadString(reader, "ISN");
            NozzleSerial = ReadString(reader, "NozzleSerial");
            NCVSerial = ReadString(reader, "NCVSerial");
            PistonGuideSerial = ReadString(reader, "PistonGuideSerial");
            NozzleAssemblyCount = ReadInt(reader, "NozzleCount");
            NCVAssemblyCount = ReadInt(reader, "NCVCount");
            PGAssemblyCount = ReadInt(reader, "PistonGuideCount");

            Fag = ReadDouble(reader, "Fag");
            Tilt = ReadDouble(reader, "Tilt");
            NeedleInFlow = ReadDouble(reader, "NeedleInFlow");
            NeedleOutFlow = ReadDouble(reader, "NeedleOutFlow");
            NeedleLift = ReadDouble(reader, "NeedleLift");
            NCVClearance = ReadDouble(reader, "NCVClearance");
            NCVLift = ReadDouble(reader, "NCVLift");
            RdO = ReadDouble(reader, "RdO");
            InO = ReadDouble(reader, "InO"); 
        } 
    }
}
