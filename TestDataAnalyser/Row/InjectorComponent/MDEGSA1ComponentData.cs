﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SudburyDataAnalyser.MDEGSA1ComponentData
{
    /// <summary>
    /// A Row of component data from SA1
    /// </summary>
    public class Row : RowBase
    {
        /// <summary>
        /// Gets or sets the Serial
        /// </summary>
        public string Serial { get; set; }

        /// <summary>
        /// Gets or sets the NCV Clearance
        /// </summary>
        public double Clearance { get; set; }

        /// <summary>
        /// Gets or sets the NCV Lift
        /// </summary>
        public double Lift { get; set; }

        /// <summary>
        /// Initialise the Row
        /// </summary>
        /// <param name="reader">The SQL Reader</param>
        public override void Initialise(System.Data.SqlClient.SqlDataReader reader)
        {
            Serial = ReadString(reader, "serial");
            Clearance = ReadDouble(reader, "Clearance");
            Lift = ReadDouble(reader, "Lift");
        }
    }
}
