﻿using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.NCV
{
    /// <summary>
    /// Represents a single record (one test point for one injector) of open loop data.
    /// </summary>
    public class Row : UTIDRow
    {
        /// <summary>Gets test point duration.</summary>
        public short Duration { get; private set; }

        /// <summary>Gets test point shot count.</summary>
        public int Count { get; private set; }

        /// <summary>Gets measured parameter median.</summary>
        public double Median { get; private set; }

        /// <summary>Gets measured parameter mean.</summary>
        public double Mean { get; private set; }

        /// <summary>Gets measured parameter minimum.</summary>
        public double Min { get; private set; }

        /// <summary>Gets measured parameter maximum.</summary>
        public double Max { get; private set; }

        /// <summary>Gets measured parameter standard deviation.</summary>
        // ReSharper disable once InconsistentNaming
        public double SD { get; private set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="NCV.Row"/> class.
        /// Converts SQL record being read with SQLDataReader into parameters to be stored in a class instance.
        /// </summary>
        /// <param name="reader">Single record from SQLDataReader</param>
        public override void Initialise(SqlDataReader reader)
        {
            Initialise(
                reader, 
                UTIDRowParameters.PlantNumber, 
                UTIDRowParameters.LineNumber, 
                UTIDRowParameters.UTID, 
                UTIDRowParameters.PDate,
                UTIDRowParameters.ISN,
                UTIDRowParameters.LOP,
                UTIDRowParameters.TestPlan,
                UTIDRowParameters.Global,
                UTIDRowParameters.URID,
                UTIDRowParameters.SpeedReq);
            Initialise(reader, NameRowParameters.All);

            Count = ReadIntFromString(reader, "Count");
            Median = ReadDoubleFromString(reader, "Median");
            Mean = ReadDoubleFromString(reader, "Mean");
            Min = ReadDoubleFromString(reader, "Min");
            Max = ReadDoubleFromString(reader, "Max");
            SD = ReadDoubleFromString(reader, "SD");

            Duration = ReadShort(reader, Queries.Parameters.QueryParameters.HeaderOLTableDurationColumn);
            TestType = ReadString(reader, Queries.Parameters.QueryParameters.HeaderTableTestTypeColumn);
        }

        /// <summary>Gets a value indicating whether if the median, mean, min, max and SD are all zero.</summary>
        public bool AllZero
        {
            get { return (Median < 0.00001) && (Mean < 0.00001) && (Min < 0.00001) && (Max < 0.00001) && (SD < 0.00001); }
        }
    }
}
