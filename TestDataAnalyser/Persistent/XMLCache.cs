﻿namespace SudburyDataAnalyser.Persistent
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml.Serialization;

    public static class XMLCache
    {
        private const string fileName = "\\{0}.xml";
        private static string directory = "\\DataCache";
        private static string filePath;

        static XMLCache()
        {

            string assemblyPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            directory = Path.GetDirectoryName(assemblyPath) + directory;
            filePath = directory + fileName;
        }

        public static void WriteToXMLFile<T>(object objectToWrite, string uniqueName)
        {
            Directory.CreateDirectory(directory);

            TextWriter writer = null;
            try
            {
                Directory.CreateDirectory(@"C:\MP_Upload");
                var serializer = new XmlSerializer(typeof(T));
                writer = new StreamWriter(filePath.Args(uniqueName));
                serializer.Serialize(writer, objectToWrite);
            }
            catch (Exception ex)
            {
                //This is only a quality of life feature. So swallow this exception
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }

        public static T ReadFromXmlFile<T>(string uniqueName)
        {
            TextReader reader = null;
            try
            {
                if (File.Exists(filePath.Args(uniqueName)))
                {
                    var serializer = new XmlSerializer(typeof(T));
                    reader = new StreamReader(filePath.Args(uniqueName));
                    return (T)serializer.Deserialize(reader);
                }
            }
            catch (Exception ex)
            {
                //This is only a quality of life feature. So swallow this exception
                return default(T);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return default(T);
        }
    }
}
