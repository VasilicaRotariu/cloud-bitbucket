﻿using System.Windows.Forms;

namespace SudburyDataAnalyser.Persistent
{
    /// <summary>
    /// Class used for persisting the state of the detailed data list view controls.
    /// </summary>
    public class ChartTraces
    {
        /// <summary>
        /// Gets or sets the open loop settings.
        /// </summary>
        public CheckedListView OpenLoop { get; set; }

        /// <summary>
        /// Gets or sets the closed loop settings.
        /// </summary>
        public CheckedListView ClosedLoop { get; set; }

        /// <summary>
        /// Gets or sets the Closed Loop Stabilise settings.
        /// </summary> 
        public CheckedListView CLStabilise { get; set; }

        /// <summary>
        /// Gets or sets the FOD settings.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public CheckedListView FOD { get; set; }
               
        /// <summary>
        /// Gets or sets the OMV settings.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public CheckedListView OMV { get; set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="ChartTraces"/> class.
        /// </summary>
        public ChartTraces()
        {
            OpenLoop = new CheckedListView();
            ClosedLoop = new CheckedListView();
            CLStabilise = new CheckedListView();
            FOD = new CheckedListView();
            OMV = new CheckedListView();
        }

        /// <summary>
        /// Initialises the checked state for the open loop, closed loop, FOD and OMV list view controls.
        /// </summary>
        /// <param name="openLoopListView">The open loop list view.</param>
        /// <param name="closedLoopListView">The closed loop list view.</param>
        /// <param name="clstabiliseListView">The closed loop Stabilise list view.</param>
        /// <param name="fodListView">The FOD list view.</param>
        /// <param name="omvListView">The OMV list view.</param>
        public void InitialiseCheckedItems(
            ListView openLoopListView,
            ListView closedLoopListView,
            ListView clstabiliseListView,
            ListView fodListView,
            ListView omvListView)
        {
            OpenLoop.InitialiseCheckedItems(openLoopListView);
            ClosedLoop.InitialiseCheckedItems(closedLoopListView);
            CLStabilise.InitialiseCheckedItems(clstabiliseListView);
            FOD.InitialiseCheckedItems(fodListView);
            OMV.InitialiseCheckedItems(omvListView);
        }

        /// <summary>
        /// Initialises the list view controls using the recorded settings.
        /// </summary>
        /// <param name="openLoopListView">The open loop list view.</param>
        /// <param name="closedLoopListView">The closed loop list view.</param>
        /// <param name="clstabiliseListView">The closed loop Stabilise list view.</param>
        /// <param name="fodListView">The FOD list view.</param>
        /// <param name="omvListView">The OMV list view.</param>
        public void InitialiseListView(
            ListView openLoopListView,
            ListView closedLoopListView,
            ListView clstabiliseListView,
            ListView fodListView,
            ListView omvListView)
        {
            OpenLoop.InitialiseListView(openLoopListView);
            ClosedLoop.InitialiseListView(closedLoopListView);
            CLStabilise.InitialiseListView(clstabiliseListView);
            FOD.InitialiseListView(fodListView);
            OMV.InitialiseListView(omvListView);
        }
    }
}
