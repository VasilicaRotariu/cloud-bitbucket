﻿using System.Windows.Forms;

namespace SudburyDataAnalyser.Persistent
{
    /// <summary>
    /// Class used for persisting the state of the detailed data list view controls.
    /// </summary>
    public class DetailedData
    {
        /// <summary>
        /// Class used for persisting the state of the detailed data tab list view controls.
        /// </summary>
        public class Settings
        {
            /// <summary>
            /// Gets or sets the checked pressure levels.
            /// </summary>
            public CheckedListView CheckedPressureLevels { get; set; }

            /// <summary>
            /// Gets or sets the checked parameters.
            /// </summary>
            public CheckedListView CheckedParameters { get; set; }

            /// <summary>
            /// Gets or sets the checked statistics.
            /// </summary>
            public CheckedListView CheckedStatistics { get; set; }

            /// <summary>
            /// Initialises a new instance of the <see cref="DetailedData.Settings"/> class.
            /// </summary>
            public Settings()
            {
                CheckedPressureLevels = new CheckedListView();
                CheckedParameters = new CheckedListView();
                CheckedStatistics = new CheckedListView();
            }

            /// <summary>
            /// Initialises the checked state for the pressure level, parameters and statistics list view controls.
            /// </summary>
            /// <param name="pressureLevelsListView">The pressure levels list view.</param>
            /// <param name="parametersListView">The parameters list view.</param>
            /// <param name="statisticsListView">The statistics list view.</param>
            public void InitialiseSettings(ListView pressureLevelsListView, ListView parametersListView, ListView statisticsListView)
            {
                CheckedPressureLevels.InitialiseCheckedItems(pressureLevelsListView);
                CheckedParameters.InitialiseCheckedItems(parametersListView);
                CheckedStatistics.InitialiseCheckedItems(statisticsListView);
            }

            /// <summary>
            /// Initialises the list view controls using the recorded settings.
            /// </summary>
            /// <param name="pressureLevelListView">The pressure levels list view.</param>
            /// <param name="parametersListView">The parameters list view.</param>
            /// <param name="statisticsListView">The statistics list view.</param>
            public void InitialiseListViews(ListView pressureLevelListView, ListView parametersListView, ListView statisticsListView)
            {
                CheckedPressureLevels.InitialiseListView(pressureLevelListView);
                CheckedParameters.InitialiseListView(parametersListView);
                CheckedStatistics.InitialiseListView(statisticsListView);
            }
        }

        /// <summary>
        /// Gets or sets the open loop settings.
        /// </summary>
        public Settings OpenLoop { get; set; }

        /// <summary>
        /// Gets or sets the closed loop settings.
        /// </summary>
        public Settings ClosedLoop { get; set; }

        /// <summary>
        /// Gets or sets the closed loop log settings.
        /// </summary>
        public Settings ClosedLoopLog { get; set; }

        /// <summary>
        /// Gets or sets the open loop stabilise settings.
        /// </summary>
        public Settings OpenLoopStabilise { get; set; }

        /// <summary>
        /// Gets or sets the closed loop stabilise settings.
        /// </summary>
        public Settings ClosedLoopStabilise { get; set; }

        /// <summary>
        /// Gets or sets the FOD settings.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public Settings FOD { get; set; }

        /// <summary>
        /// Gets or sets the OMV settings.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public Settings OMV { get; set; }

        /// <summary>
        /// Gets or sets the trimmed settings.
        /// </summary>
        public Settings Trimmed { get; set; }

        /// <summary>
        /// Gets or sets the trimmed log settings.
        /// </summary>
        public Settings TrimmedLog { get; set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="DetailedData"/> class.
        /// </summary>
        public DetailedData()
        {
            OpenLoop = new Settings();
            ClosedLoop = new Settings();
            ClosedLoopLog = new Settings();
            OpenLoopStabilise = new Settings();
            ClosedLoopStabilise = new Settings();
            FOD = new Settings();
            OMV = new Settings();
            Trimmed = new Settings();
            TrimmedLog = new Settings();
        }
    }
}
