﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace SudburyDataAnalyser.Persistent
{
    /// <summary>
    /// Class used for persisting the state of a check list view control.
    /// </summary>
    public class CheckedListView
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="CheckedListView"/> class.
        /// </summary>
        public CheckedListView()
        {
            CheckedItems = new HashSet<string>();
        }

        /// <summary>
        /// Records all the checked items in the list view.
        /// </summary>
        /// <param name="listView">The list view to record checked items from.</param>
        public void InitialiseCheckedItems(ListView listView)
        {
            if (listView != null)
            {
                if (listView.Items.Count > 0)
                {
                    CheckedItems = new HashSet<string>(listView.CheckedItems());
                }
            }
        }

        /// <summary>
        /// Initialises the checked state of a list view using the recorded settings.
        /// </summary>
        /// <param name="listView">The list view to initialise.</param>
        public void InitialiseListView(ListView listView)
        {
            if (listView != null)
            {
                foreach (ListViewItem item in listView.Items)
                {
                    if (CheckedItems.Contains(item.Text))
                    {
                        item.Checked = true;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the checked items.
        /// </summary>
        private HashSet<string> CheckedItems { get; set; }
    }
}
