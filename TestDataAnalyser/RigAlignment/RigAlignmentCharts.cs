﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Class used to contain multiple rig alignment charts.
    /// </summary>
    public class RigAlignmentCharts
    {
        /// <summary>The height of each row (containing one chart).</summary>
        private const int RowHeight = 400;

        /// <summary>
        /// Class used to contain all the controls for a chart.
        /// </summary>
        private class Controls
        {
            /// <summary>
            /// Initialises a new instance of the <see cref="Controls"/> class.
            /// </summary>
            /// <param name="rigAlignmentChart">The rig alignment class.</param>
            public Controls(RigAlignmentChart rigAlignmentChart)
            {
                chart = rigAlignmentChart;
                powerPointButton = new PowerPointButton(chart.Parent);
                excelButton = new ExcelButton(chart.Parent);
                copyButton = new CopyButton(chart.Parent);

                powerPointButton.Click += PowerPointButton_Click;
                excelButton.Click += ExcelButton_Click;
                copyButton.Click += CopyButton_Click;
            }

            /// <summary>
            /// Gets the rig alignment chart.
            /// </summary>
            public RigAlignmentChart Chart
            {
                get
                {
                    return chart;
                }
            }

            /// <summary>
            /// Adds the controls to their parent panel.
            /// </summary>
            public void AddToPanel()
            {
                Chart.Parent.Controls.Add(Chart);
                powerPointButton.Parent.Controls.Add(powerPointButton);
                excelButton.Parent.Controls.Add(excelButton);
                copyButton.Parent.Controls.Add(copyButton);
            }

            /// <summary>
            /// Removes the controls from their parent panel.
            /// </summary>
            public void RemoveFromPanel()
            {
                Chart.Parent.Controls.Remove(Chart);
                powerPointButton.Parent.Controls.Remove(powerPointButton);
                excelButton.Parent.Controls.Remove(excelButton);
                copyButton.Parent.Controls.Remove(copyButton);
            }

            /// <summary>
            /// Locates the controls at the indexed position.
            /// </summary>
            public void Locate(int position)
            {
                Chart.Location = new Point(0, 24 + (position * RowHeight));
                powerPointButton.Location = new Point(powerPointButton.Parent.Width - 150, position * RowHeight);
                excelButton.Location = new Point(excelButton.Parent.Width - 130, position * RowHeight);
                copyButton.Location = new Point(copyButton.Parent.Width - 110, position * RowHeight);
            }

            /// <summary>
            /// Button click event handler causing the chart to be exported to Power Point.
            /// </summary>
            /// <param name="sender">The event sender</param>
            /// <param name="e">The event arguments.</param>
            private void PowerPointButton_Click(object sender, EventArgs e)
            {
                Singleton<PowerPointHelper>.Instance.Export(Chart, false);
            }

            /// <summary>
            /// Button click event handler causing the chart to be exported to Excel.
            /// </summary>
            /// <param name="sender">The event sender</param>
            /// <param name="e">The event arguments.</param>
            private void ExcelButton_Click(object sender, EventArgs e)
            {
                Singleton<ExcelHelper>.Instance.Export(Chart);
            }

            /// <summary>
            /// Button click event handler causing the chart to be copied to the clipboard.
            /// </summary>
            /// <param name="sender">The event sender</param>
            /// <param name="e">The event arguments.</param>
            private void CopyButton_Click(object sender, EventArgs e)
            {
                using (var ms = new MemoryStream())
                {
                    Chart.SaveImage(ms, ChartImageFormat.Bmp);
                    var bm = new Bitmap(ms);
                    Clipboard.SetImage(bm);
                }
            }

            /// <summary>
            /// The rig alignment chart.
            /// </summary>
            // ReSharper disable once FieldCanBeMadeReadOnly.Local
            private RigAlignmentChart chart;

            /// <summary>
            /// The export to Power Point button.
            /// </summary>
            // ReSharper disable once FieldCanBeMadeReadOnly.Local
            private PowerPointButton powerPointButton;

            /// <summary>
            /// The export to Excel button.
            /// </summary>
            // ReSharper disable once FieldCanBeMadeReadOnly.Local
            private ExcelButton excelButton;

            /// <summary>
            /// The copy to clipboard button.
            /// </summary>
            // ReSharper disable once FieldCanBeMadeReadOnly.Local
            private CopyButton copyButton;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RigAlignmentCharts"/> class.
        /// </summary>
        /// <param name="parent">The parent panel the charts will be a children of.</param>
        public RigAlignmentCharts(Panel parent)
        {
            parentPanel = parent;
        }

        /// <summary>
        /// Add a new chart to the charts and display it on the parent panel.
        /// </summary>
        /// <param name="checkedNode">The node checked indicating the new chart to add.</param>
        /// <param name="allNodes">All the nodes, used to locate the charts in the correct order.</param>
        /// <param name="lines">The lines used to get the chart data.</param>
        /// <param name="chronological">Flag indicating if the date/time or chronological order is required.</param>
        /// <param name="showLegend">Flag indicating whether the legend should be displayed or not.</param>
        /// <param name="completeCalibrationData">The data used to populate the chart.</param>
        /// <param name="connectionString">The connection string used to get data for the chart.</param>
        public void AddChart(
            TreeNode checkedNode,
            List<TreeNode> allNodes,
            List<string> lines,
            bool chronological,
            bool showLegend,
            Queries.DataTable.CompleteCalibrationDataTable completeCalibrationData,
            string connectionString)
        {
            if (!rigAlignmentControls.ContainsKey(checkedNode.FullPath))
            {
                var chart = new RigAlignmentChart(parentPanel, checkedNode, completeCalibrationData, connectionString);
                chart.Initialise(lines, chronological, showLegend);

                var controls = new Controls(chart);

                rigAlignmentControls[checkedNode.FullPath] = controls;
                controls.AddToPanel();
            }

            LocateCharts(allNodes);
            parentPanel.ScrollControlIntoView(rigAlignmentControls[checkedNode.FullPath].Chart);
        }

        /// <summary>
        /// Remove a chart from the charts and remove it from the parent panel.
        /// </summary>
        /// <param name="uncheckedNode">The node unchecked indicating the new chart to remove.</param>
        /// <param name="allNodes">All the nodes, used to locate the charts in the correct order.</param>
        public void RemoveChart(TreeNode uncheckedNode, List<TreeNode> allNodes)
        {
            var fullPath = uncheckedNode.FullPath;

            if (!rigAlignmentControls.ContainsKey(fullPath)) return;

            var scrollNode = PreviousCheckedNode(uncheckedNode, allNodes);

            rigAlignmentControls[fullPath].RemoveFromPanel();
            rigAlignmentControls.Remove(fullPath);

            LocateCharts(allNodes);

            if (scrollNode != null)
            {
                parentPanel.ScrollControlIntoView(rigAlignmentControls[scrollNode.FullPath].Chart);
            }
        }

        /// <summary>
        /// Gets the chart count.
        /// </summary>
        public int Count
        {
            get
            {
                return rigAlignmentControls.Count;
            }
        }

        /// <summary>
        /// Removes all the charts and removes the charts from the parent panel.
        /// </summary>
        public void Clear()
        {
            parentPanel.InvokeIfRequired(c => 
            {
                var checkBox = c.Controls[0] as CheckBox;
                c.Controls.Clear();
                if (checkBox != null)
                {
                    c.Controls.Add(checkBox);
                }
            });
            rigAlignmentControls.Clear();
        }

        /// <summary>
        /// Updates the charts to display in the correct format.
        /// </summary>
        /// <param name="lines">The lines used to get the chart data.</param>
        /// <param name="chronological">Flag indicating if the date/time or chronological order is required.</param>
        /// <param name="showLegend">Value indicating if the legend should be shown.</param>
        public void UpdateCharts(List<string> lines, bool chronological, bool showLegend)
        {
            foreach (var controls in rigAlignmentControls.Values)
            {
                controls.Chart.Initialise(lines, chronological, showLegend);
            }
        }

        /// <summary>
        /// Returns the previous checked node before the node specified.
        /// </summary>
        /// <param name="searchNode">The node to search for a return the previous checked node.</param>
        /// <param name="nodes">The nodes to search.</param>
        /// <returns>The previous checked node before the specified node.</returns>
        private static TreeNode PreviousCheckedNode(TreeNode searchNode, IEnumerable<TreeNode> nodes)
        {
            TreeNode previousNode = null;

            foreach (var node in nodes)
            {
                if (node == searchNode)
                {
                    return previousNode;
                }
                if (node.Checked)
                {
                    previousNode = node;
                }
            }

            return previousNode;
        }

        /// <summary>
        /// Locates the charts on the parent panel in the order found in the nodes.
        /// Scrolls to the start of the charts to ensure there correct location.
        /// </summary>
        /// <param name="allNodes">All the nodes, used to determine the chart sequence.</param>
        private void LocateCharts(IEnumerable<TreeNode> allNodes)
        {
            using (var control = new Control { Parent = parentPanel, Dock = DockStyle.Top })
            {
                parentPanel.ScrollControlIntoView(control);
                control.Parent = null;
            }

            var pos = 0;
            foreach (var node in allNodes.Where(node => node.Checked))
            {
                Controls controls;
                if (!rigAlignmentControls.TryGetValue(node.FullPath, out controls)) continue;

                controls.Locate(pos);
                ++pos;
            }
        }

        /// <summary>
        /// The rig alignment charts.
        /// </summary>
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private Dictionary<string, Controls> rigAlignmentControls = new Dictionary<string, Controls>();

        /// <summary>
        /// The parent panel the charts are added too.
        /// </summary>
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private Panel parentPanel;
    }
}
