﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Class used for the rig alignment charts.
    /// </summary>
    public class RigAlignmentChart : Chart
    {
        /// <summary>Default height of each rig alignment chart.</summary>
        public const int ChartHeight = 366;

        /// <summary>
        /// Initialises a new instance of the <see cref="RigAlignmentChart"/> class.
        /// </summary>
        /// <param name="parent">The parent panel the chart will be a child of.</param>
        /// <param name="node">The tree node used to get the chart data.</param>
        /// <param name="completeCalibrationData">The data used to populate the chart.</param>
        /// <param name="connectionString">The connection string used to get data for the chart.</param>
        public RigAlignmentChart(
            Panel parent,
            TreeNode node,
            Queries.DataTable.CompleteCalibrationDataTable completeCalibrationData,
            string connectionString)
        {
            Parent = parent;
            Node = node;
            Data = completeCalibrationData;
            ConnectionString = connectionString;

            Width = Parent.Width - 20;
            Left = 3;
            Height = ChartHeight;

            Titles.Add(Node.FullPath);
            Name = "RigAlignmentChart_{0}".Args(Node.FullPath);

            var trackingChartArea = new ChartArea(TrackingChartArea) { Position = { Auto = false, Height = 90, X = 0, Y = 10 } };
            var frequencyChartArea = new ChartArea(FrequencyChartArea) { Position = { Auto = true, Height = 90, Y = 10 } };

            ChartAreas.Add(trackingChartArea);
            ChartAreas.Add(frequencyChartArea);

            MouseDoubleClick += RigAlignmentChart_MouseDoubleClick;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RigAlignmentChart"/> class.
        /// </summary>
        /// <param name="lines">The lines used to get the chart data.</param>
        /// <param name="chronological">Flag indicating if the date/time or chronological order is required.</param>
        /// <param name="showLegend">Value indicating if the legend should be shown.</param>
        public void Initialise(List<string> lines, bool chronological, bool showLegend)
        {
            Lines = lines;
            Chronological = chronological;

            Series.Clear();

            InitialiseTrackingChart(ChartAreas[TrackingChartArea], Series);
            InitialiseFrequencyChart(ChartAreas[FrequencyChartArea], Series);

            ChartAreas[TrackingChartArea].Position.Width = showLegend ? TrackingChartWidthWithLegend : TrackingChartWidthWithoutLegend;
            ChartAreas[FrequencyChartArea].Position.Width = showLegend ? FrequenceChartWidthWithLegend : FrequenceChartWidthWithoutLegend;
            ChartAreas[FrequencyChartArea].Position.X = ChartAreas[TrackingChartArea].Position.Width + 2;

            ChartAreas[FrequencyChartArea].InnerPlotPosition.Height = ChartAreas[TrackingChartArea].InnerPlotPosition.Height = 85;
            ChartAreas[FrequencyChartArea].InnerPlotPosition.Width = 100;
            ChartAreas[TrackingChartArea].InnerPlotPosition.Width = 90;
            ChartAreas[FrequencyChartArea].InnerPlotPosition.X = ChartAreas[TrackingChartArea].InnerPlotPosition.X = 10;

            ChartAreas[FrequencyChartArea].AxisY.LabelStyle.Enabled = false;
             
            if (showLegend)
            {
                if (Legends.Count == 0)
                {
                    Legends.Add(new Legend());
                }
            }
            else
            {
                Legends.Clear();
            }
        }

        /// <summary>
        /// Adds the tracking data to the chart series data and initialises the chart area.
        /// </summary>
        /// <param name="chartArea">The chart area to configure.</param>
        /// <param name="chartSeries">The chart series to add series data too.</param>
        /// <param name="seriesNamePrefix">The prefix for the series name.</param>
        /// <param name="showInLegend">Flag indicating whether the series should be shown in the legend.</param>
        private void InitialiseTrackingChart(ChartArea chartArea, SeriesCollection chartSeries, string seriesNamePrefix = "Tracking_", bool showInLegend = false)
        {
            if (Lines.Count > 0)
            {
                var seriesIndex = 0;
                foreach (var plantLineNumber in Lines)
                {
                    // Tracker chart
                    chartSeries.Add(GetTrackingSeries(plantLineNumber, seriesIndex, seriesNamePrefix, showInLegend));
                    ++seriesIndex;
                }

                ChartAxisScaling.AxisScaler.ScaleAxis(chartArea, "RigAlignmentTracking", Node.TopNode().Text);

                if (Chronological)
                {
                    chartArea.AxisX.LabelStyle.Enabled = false;
                    chartArea.AxisX.MajorGrid.Enabled = false;
                }
                else
                {
                    chartArea.AxisX.LabelStyle.Enabled = true;
                    chartArea.AxisX.LabelStyle.Format = "yyyy-MM-dd HH:mm";
                    chartArea.AxisX.MajorGrid.Enabled = true;
                }
            }
        }

        /// <summary>
        /// Adds the frequency data to the chart series data and initialises the chart area.
        /// </summary>
        /// <param name="chartArea">The chart area to configure.</param>
        /// <param name="chartSeries">The chart series to add series data too.</param>
        private void InitialiseFrequencyChart(ChartArea chartArea, SeriesCollection chartSeries)
        {
            if (Lines.Count > 0)
            {
                var seriesIndex = 0;
                var maxPercentage = 0.0;

                ChartAxisScaling.AxisScaler.ScaleAxis(chartArea, "RigAlignmentFrequency", Node.TopNode().Text);

                foreach (string plantLineNumber in Lines)
                {
                    chartSeries.Add(GetFrequencySeries(plantLineNumber, seriesIndex, chartArea.AxisY.Minimum, chartArea.AxisY.Maximum, ref maxPercentage));
                    ++seriesIndex;
                }

                chartArea.AxisX.Maximum = Math.Min(100, (double)(((int)(maxPercentage / 10) + 1) * 10));
                chartArea.AxisX.LabelStyle.Format = "{#'%'}";
            }
        }

        /// <summary>
        /// Generates and runs full query to generate XY data series for the selected TreeNode and plant 
        /// number and line number.
        /// </summary>
        /// <param name="plantNumberAndLine">The plant number and line number combined as a single string.</param>
        /// <returns>The data table for the plant number and line number.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation",
                         Justification = "Variables xTreeNode, yTreeNode, xQuery, yQuery are X and Y axis specific.")]
        private DataTable GetData(string plantNumberAndLine)
        {
            if (DataTables == null)
            {
                DataTables = new Dictionary<string, DataTable>();
            }

            DataTable dataTable;
            if (DataTables.TryGetValue(plantNumberAndLine, out dataTable)) return dataTable;

            var splitPlantNumberAndLine = plantNumberAndLine.Split('_');
            var plantNumber = splitPlantNumberAndLine[0];
            var line = splitPlantNumberAndLine[1];
            var keys = UTIDPlantNumberKeys.GetKeys(Data.DataTable);

            var xTreeNode = new TreeNode { Text = @"PDate" };

            var xQuery = Queries.XYQueries.XYQuery.GetAlignmentOrTrendOrXYQuery(xTreeNode, xTreeNode, "X");
            var yQuery = Queries.XYQueries.XYQuery.GetAlignmentOrTrendOrXYQuery(Node, Node.TopNode(), "Y");

            var query = new Queries.XYQueries.AlignmentXYQuery();

            dataTable = new DataTable();

            try
            {
                var da = new SqlDataAdapter(query.GetQuery(xTreeNode.Text, keys, plantNumber, line, xQuery, yQuery), ConnectionString);
                da.Fill(dataTable);
            }
            catch (SqlException exception)
            {
                MessageBox.Show(exception.Message);
            }

            DataTables[plantNumberAndLine] = dataTable;

            return dataTable;
        }

        /// <summary>
        /// Creates a series for the tracking data for the plant number and line number.
        /// </summary>
        /// <param name="plantNumberAndLine">The plant number and line number combined as a single string.</param>
        /// <param name="seriesIndex">Series index used to colour the series.</param>
        /// <param name="seriesNamePrefix">The prefix for the series name.</param>
        /// <param name="showInLegend">Flag indicating whether the series should be shown in the legend.</param>
        /// <returns>A series for the tracking data.</returns>
        private Series GetTrackingSeries(string plantNumberAndLine, int seriesIndex, string seriesNamePrefix, bool showInLegend)
        {
            var dataTable = GetData(plantNumberAndLine);

            if (dataTable.Rows.Count <= 0) return new Series();

            var seriesColour = SeriesColour.FrequencyPlotColours(seriesIndex);

            // Add x and y points to series
            var series = new Series(seriesNamePrefix + plantNumberAndLine)
            {
                ChartType = SeriesChartType.Point,
                MarkerStyle = MarkerStyle.Circle,
                MarkerSize = 4,
                Color = seriesColour,
                MarkerColor = seriesColour,
                IsVisibleInLegend = showInLegend
            };

            if (Chronological)
            {
                var index = dataTable.Rows.Count;
                foreach (DataRow dr in dataTable.Rows)
                {
                    series.Points.AddXY(index, dr["Y"]);
                    --index;
                }
            }
            else
            {
                foreach (DataRow dr in dataTable.Rows)
                {
                    series.Points.AddXY(dr["X"], dr["Y"]);
                }
            }

            return series;
        }

        /// <summary>
        /// Creates a series for the frequency data for the plant number and line number.
        /// </summary>
        /// <param name="plantNumberAndLine">The plant number and line number combined as a single string.</param>
        /// <param name="seriesIndex">Series index used to colour the series.</param>
        /// <param name="minValue">A minimum value to plot.</param>
        /// <param name="maxValue">A maximum value to plot.</param>
        /// <param name="maxPercentage">Reference to the maximum percentage calculated.</param>
        /// <returns>A series for the frequency data.</returns>
        private Series GetFrequencySeries(string plantNumberAndLine, int seriesIndex, double minValue, double maxValue, ref double maxPercentage)
        {
            var dataTable = GetData(plantNumberAndLine);

            if (dataTable.Rows.Count <= 0) return new Series();

            var values = new List<double>();
            foreach (DataRow dr in dataTable.Rows)
            {
                try 
                {
                    values.Add((float)dr["Y"]);
                }
                // ReSharper disable once EmptyGeneralCatchClause
                catch 
                { 
                }
            }

            double segmentIntervalWidth = (maxValue - minValue) / 15;
            segmentIntervalWidth = RoundInterval(segmentIntervalWidth);

            // Round minimum and maximum values
            minValue = Math.Floor(minValue / segmentIntervalWidth) * segmentIntervalWidth;
            maxValue = Math.Ceiling(maxValue / segmentIntervalWidth) * segmentIntervalWidth;

            var series = new Series(plantNumberAndLine);

            // Create histogram series points
            double currentPosition;
            for (currentPosition = minValue; currentPosition <= maxValue; currentPosition += segmentIntervalWidth)
            {
                // Count all points from data series that are in current interval
                var count = 0;
                foreach (var value in values)
                {
                    var endPosition = currentPosition + segmentIntervalWidth;
                    if (value >= currentPosition &&
                        value < endPosition)
                    {
                        ++count;
                    }

                        // Last segment includes point values on both segment boundaries
                    else if (endPosition >= maxValue)
                    {
                        if (value >= currentPosition &&
                            value <= endPosition)
                        {
                            ++count;
                        }
                    }
                }

                // Add data point into the histogram series
                var percentage = (double)count * 100 / values.Count;
                series.Points.AddXY(percentage, currentPosition + (segmentIntervalWidth / 2.0));
                maxPercentage = Math.Max(maxPercentage, percentage);
            }

            // Adjust series attributes
            series["PointWidth"] = "1";
            series.ChartType = SeriesChartType.Spline;
            series.Color = series.BorderColor = Color.FromArgb(255, SeriesColour.FrequencyPlotColours(seriesIndex));

            series.ChartArea = FrequencyChartArea;

            return series;
        }

        /// <summary>
        /// Converts selected chart information to a memory stream and runs BigChart to generate a new
        /// separate window cloning the chart.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void RigAlignmentChart_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var chart = new Chart();
            chart.Legends.Add(new Legend());

            // Determine which plot was double clicked
            if (e.X * 100 / Width > (Legends.Any() ? TrackingChartWidthWithLegend : TrackingChartWidthWithoutLegend))
            {
                chart.ChartAreas.Add(new ChartArea(FrequencyChartArea));
                InitialiseFrequencyChart(chart.ChartAreas[0], chart.Series);
            }
            else
            {
                chart.ChartAreas.Add(new ChartArea(TrackingChartArea));
                InitialiseTrackingChart(chart.ChartAreas[0], chart.Series, "", true);
            }

            var memoryStream = new MemoryStream();
            chart.Serializer.Save(memoryStream);

            // Generate new Big chart and copy serialized chart data to it.
            var bigChartForm = new BigChart.frmBigChart();
            bigChartForm.copyChartToBigWindow(memoryStream);

            bigChartForm.Show();

            memoryStream.Close();
        }

        /// <summary>
        /// Helper method which rounds specified an axis interval.
        /// </summary>
        /// <param name="interval">Calculated axis interval.</param>
        /// <returns>Rounded axis interval.</returns>
        private double RoundInterval(double interval)
        {
            // If the interval is zero return error
            if (interval.Approx(0.0))
            {
                return 1.0;
            }

            // If the real interval is > 1.0
            var step = -1;
            var tempValue = interval;
            while (tempValue > 1.0)
            {
                step++;
                tempValue = tempValue / 10.0;
                if (step > 1000)
                {
                    throw new InvalidOperationException("Auto interval error due to invalid point values or axis minimum/maximum.");
                }
            }

            // If the real interval is < 1.0
            tempValue = interval;
            if (tempValue < 1.0)
            {
                step = 0;
            }

            while (tempValue < 1.0)
            {
                step--;
                tempValue = tempValue * 10.0;
                if (step < -1000)
                {
                    throw new InvalidOperationException("Auto interval error due to invalid point values or axis minimum/maximum.");
                }
            }

            var tempDiff = interval / Math.Pow(10.0, step);
            if (tempDiff < 3.0)
            {
                tempDiff = 2.0;
            }
            else if (tempDiff < 7.0)
            {
                tempDiff = 5.0;
            }
            else
            {
                tempDiff = 10.0;
            }

            // Make a correction of the real interval
            return tempDiff * Math.Pow(10.0, step);
        }

        /// <summary>The name of the tracking chart area.</summary>
        private const string TrackingChartArea = "TrackingChartArea";

        /// <summary>The name of the frequency chart area.</summary>
        private const string FrequencyChartArea = "FrequencyChartArea";

        /// <summary>Chart area width of the tracking chart when a legend is being shown.</summary>
        private const int TrackingChartWidthWithLegend = 51;

        /// <summary>Chart area width of the tracking chart when a legend is not being shown.</summary>
        private const int TrackingChartWidthWithoutLegend = 58;

        /// <summary>Chart area width of the frequency chart when a legend is being shown.</summary>
        private const int FrequenceChartWidthWithLegend = 31;

        /// <summary>Chart area width of the frequency chart when a legend is not being shown.</summary>
        private const int FrequenceChartWidthWithoutLegend = 38;

        /// <summary>Gets or sets the tree node the chart is displaying data for.</summary>
        private TreeNode Node { get; set; }

        /// <summary>Gets or sets the plant numbers and line numbers.</summary>
        private List<string> Lines { get; set; }

        /// <summary>Gets or sets a value indicating whether the tracking chart is chronological.</summary>
        private bool Chronological { get; set; }

        /// <summary>Gets or sets the calibration data used to populate the chart.</summary>
        private Queries.DataTable.CompleteCalibrationDataTable Data { get; set; }

        /// <summary>Gets or sets the data tables for each line.</summary>
        private Dictionary<string, DataTable> DataTables { get; set; }

        /// <summary>Gets or sets the connection string used to access the database.</summary>
        private string ConnectionString { get; set; }
    }
}
