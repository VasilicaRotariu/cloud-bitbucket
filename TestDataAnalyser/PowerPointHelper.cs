﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.IO;
using SudburyDataAnalyser.Properties;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;

namespace SudburyDataAnalyser
{
    /// <summary>
    /// Helper class used to add chart images to a power point presentation.
    /// </summary>
    public class PowerPointHelper
    {
        /// <summary>
        /// Finalises an instance of the <see cref="PowerPointHelper"/> class.
        /// Deletes any temporary files created.
        /// </summary>
        ~PowerPointHelper()
        {
            foreach (string file in filesToDelete)
            {
                try
                {
                    File.Delete(file);
                }
                // ReSharper disable once EmptyGeneralCatchClause
                catch
                {
                }
            }
        }

        /// <summary>
        /// Exports a chart image to a power point presentation on a new slide.
        /// </summary>
        /// <param name="chart">The chart to add to the power point presentation.</param>
        /// <param name="showLegend">Value indicating if the legend should be shown.</param>
        public void Export(Chart chart, bool showLegend)
        {
            // ReSharper disable once UnusedVariable
            using (var waitCursor = new WaitCursor())
            {
                if (!OpenPowerPointPresentation()) return;

                var slide = AddSlide(chart.Titles.First().Text);
                if (slide == null) return;

                var titleText = chart.Titles.First().Text;
                chart.Titles.First().Text = string.Empty;
                slide.Shapes.AddPicture(ChartImageFile(chart, showLegend), Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoCTrue, 50, 100, 600, 400);
                chart.Titles.First().Text = titleText;
                Save();
            }
        }

        /// <summary>
        /// Exports the charts as images to a power point presentation on a new slide.
        /// </summary>
        /// <param name="title">The slide title.</param>
        /// <param name="charts">The charts to add to the power point presentation.</param>
        /// <param name="showLegend">Value indicating if the legend should be shown.</param>
        public void Export(string title, TableOfCharts charts, bool showLegend)
        {
            if (charts.Count == 0) return;

            // ReSharper disable once UnusedVariable
            using (var waitCursor = new WaitCursor())
            {
                if (!OpenPowerPointPresentation()) return;

                // The top left coordinate for each chart.
                var chartPosition = new[,] { { 25, 100 }, { 25, 250 }, { 25, 400 }, { 375, 100 }, { 375, 250 }, { 375, 400 } };

                var slide = AddSlide(title);
                if (slide == null) return;

                var index = 0;
                foreach (var chart in charts.Charts)
                {
                    slide.Shapes.AddPicture(ChartImageFile(chart, showLegend), Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoCTrue, chartPosition[index, 0], chartPosition[index, 1], 325, 125);

                    ++index;
                    if (index == chartPosition.Length)
                    {
                        break;
                    }
                }

                Save();
            }
        }

        /// <summary>
        /// Opens a power point presentation ready to add slides.
        /// </summary>
        /// <returns>True if the presentation was opened. False otherwise.</returns>
        private bool OpenPowerPointPresentation()
        {
            try
            {
                if (application == null)
                {
                    application = new PowerPoint.Application();
                }

                if (presentations == null)
                {
                    presentations = application.Presentations;
                }

                if (presentation == null)
                {
                    var openFile = new OpenFileDialog();
                    if (openFile.ShowDialog() == DialogResult.OK && openFile.CheckPathExists)
                    {
                        presentation = presentations.Open(openFile.FileName);
                        saveAfterAddingSlide = true;
                    }
                    else
                    {
                        presentation = presentations.Add();
                        saveAfterAddingSlide = false;
                    }

                    application.Visible = Microsoft.Office.Core.MsoTriState.msoTrue;
                }
            }
            catch
            {
                MessageBox.Show(Resources.Unable_to_start_PowerPoint__Please_check_Office_has_been_installed, Resources.PowerPoint_error);

                application = null;
                presentations = null;
                presentation = null;
            }

            return presentation != null;
        }

        /// <summary>
        /// Adds a new slide to the power point presentation and returns the slide.
        /// </summary>
        /// <param name="title">The slide title.</param>
        /// <param name="layout">The slide layout.</param>
        /// <returns>A new slide or null.</returns>
        private PowerPoint._Slide AddSlide(string title, PowerPoint.PpSlideLayout layout = PowerPoint.PpSlideLayout.ppLayoutTitleOnly)
        {
            PowerPoint.Slides slides;

            try
            {
                slides = presentation.Slides;
            }
            catch
            {
                application = null;
                presentations = null;
                presentation = null;

                OpenPowerPointPresentation();
                if (presentation == null) return null;

                slides = presentation.Slides;
            }

            PowerPoint._Slide slide = slides.Add(slides.Count + 1, layout);

            if (layout == PowerPoint.PpSlideLayout.ppLayoutTitleOnly) slide.Shapes[1].TextFrame.TextRange.Text = title;

            return slide;
        }

        /// <summary>
        /// Converts the chart to an image, saved in a file and returns the file name.
        /// </summary>
        /// <param name="chart">The chart to convert.</param>
        /// <param name="showLegend">Value indicating if the legend should be shown.</param>
        /// <returns>The name of the image file.</returns>
        private string ChartImageFile(Chart chart, bool showLegend)
        {
            Legend legend = null;
            if (showLegend)
            {
                legend = chart.AddLegendIfAbsent();
            }

            string fileName = Path.GetTempFileName();
            chart.SaveImage(fileName, System.Drawing.Imaging.ImageFormat.Bmp);

            chart.RemoveLegend(legend);

            filesToDelete.Add(fileName);

            return fileName;
        }

        /// <summary>
        /// Save the power point presentation to disk.
        /// </summary>
        private void Save()
        {
            if (saveAfterAddingSlide)
            {
                try
                {
                    presentation.Save();
                }
                catch
                {
                    MessageBox.Show(Resources.Failed_to_save_the_presentation__Please_save_it_manually_, Resources.PowerPoint_error);
                    saveAfterAddingSlide = false;
                }
            }
        }

        /// <summary>
        /// A list of files to be deleted during finalisation.
        /// </summary>
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private List<string> filesToDelete = new List<string>();

        /// <summary>
        /// The power point application.
        /// </summary>
        private PowerPoint.Application application;

        /// <summary>
        /// The power point presentations/
        /// </summary>
        private PowerPoint.Presentations presentations;

        /// <summary>
        /// The power point presentation.
        /// </summary>
        private PowerPoint.Presentation presentation;

        /// <summary>
        /// Flag and
        /// </summary>
        private bool saveAfterAddingSlide = true;
    }
}
