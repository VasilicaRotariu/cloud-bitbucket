﻿using System.Drawing;
using System.Windows.Forms;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.VirtualDataGridView
{
    /// <summary>
    /// User control derived from VirtualDataGridView. Implements the required cell
    /// formatting for completed data grid views.
    /// </summary>
    public class InjectorComponentDataGridView : VirtualDataGridView
    {
        /// <summary>
        /// Event handler to format a cell in the data grid view. Overrides the default
        /// implementation to provide the specific formatting required for incomplete
        /// data grid views.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event parameters.</param>
        protected override void CellFormattingHandler(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.Value == null) return;
            if (DataTable.Rows.Count <= e.RowIndex) return;

            if (((Columns[e.ColumnIndex].HeaderText == "NCV Count") && (DataTable.Rows[e.RowIndex]["NCV Count"].ToString() != "1")) ||
                ((Columns[e.ColumnIndex].HeaderText == "PG Count") && (DataTable.Rows[e.RowIndex]["PG Count"].ToString() != "1")) ||
                ((Columns[e.ColumnIndex].HeaderText == "Nozzle Count") && (DataTable.Rows[e.RowIndex]["Nozzle Count"].ToString() != "1")))
            {
                e.CellStyle.Font = new Font(Font, FontStyle.Bold);
                return;
            }

            if (DataTable.Rows[e.RowIndex]["NCV Count"].ToString() != "1") return;
            if (DataTable.Rows[e.RowIndex]["PG Count"].ToString() != "1") return;
            if (DataTable.Rows[e.RowIndex]["Nozzle Count"].ToString() != "1") return;

            e.CellStyle.BackColor = Color.PaleGreen;
            e.CellStyle.ForeColor = Color.Black;
            e.CellStyle.SelectionBackColor = Color.DarkGreen;
            e.CellStyle.SelectionForeColor = Color.White;
        }
    }
}
