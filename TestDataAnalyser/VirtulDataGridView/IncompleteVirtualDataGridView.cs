﻿using System;
using System.Drawing;
using System.Windows.Forms;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.VirtualDataGridView
{
    /// <summary>
    /// User control derived from VirtualDataGridView. Implements the required cell
    /// formatting for incomplete data grid views.
    /// </summary>
    public class IncompleteVirtualDataGridView : VirtualDataGridView
    {
        /// <summary>
        /// Event handler to format a cell in the data grid view. Overrides the default
        /// implementation to provide the specific formatting required for incomplete
        /// data grid views.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event parameters.</param>
        protected override void CellFormattingHandler(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.Value == null) return;
            if (DataTable.Columns.Count <= e.ColumnIndex) return;

            var column = DataTable.Columns[e.ColumnIndex].Caption;

            if (!column.EndsWith("_gr") || !(e.Value is float)) return;

            var value = (float)e.Value;
            if (Math.Abs(value) >= 1.0)
            {
                e.CellStyle.Font = new Font(Font, FontStyle.Bold);
            }
        }
    }
}
