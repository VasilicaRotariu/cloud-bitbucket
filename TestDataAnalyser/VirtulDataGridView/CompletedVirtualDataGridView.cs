﻿using System;
using System.Drawing;
using System.Windows.Forms;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.VirtualDataGridView
{
    /// <summary>
    /// User control derived from VirtualDataGridView. Implements the required cell
    /// formatting for completed data grid views.
    /// </summary>
    public class CompletedVirtualDataGridView : VirtualDataGridView
    {
        /// <summary>
        /// Event handler to format a cell in the data grid view. Overrides the default
        /// implementation to provide the specific formatting required for completed
        /// data grid views.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event parameters.</param>
        protected override void CellFormattingHandler(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.Value == null) return;
            if (DataTable.Columns.Count <= e.ColumnIndex) return;
            if (DataTable.Rows.Count <= e.RowIndex) return;

            var column = DataTable.Columns[e.ColumnIndex].Caption;
            var globalGrValue = (int)(float)DataTable.Rows[e.RowIndex]["Global_gr"];

            // Production injector pass - '0' = NPI pass, '-2' = PI pass
            if (globalGrValue == 0 || globalGrValue == -2)
            {
                e.CellStyle.BackColor = Color.PaleGreen;
                e.CellStyle.ForeColor = Color.Black;
                e.CellStyle.SelectionBackColor = Color.DarkGreen;
                e.CellStyle.SelectionForeColor = Color.White;
            }
                //// Master, ref, samples, develop etc. pass - '-3' on calibration, '-4' on pumping
            else if (globalGrValue == -3 || globalGrValue == -4)
            {
                e.CellStyle.BackColor = Color.LightBlue;
                e.CellStyle.ForeColor = Color.Black;
                e.CellStyle.SelectionBackColor = Color.Blue;
                e.CellStyle.SelectionForeColor = Color.White;
            }
                //// Production fail - > 0
            else if (globalGrValue > 0 && DataTable.Rows[e.RowIndex]["TestPlan"].ToString().Equals("Production"))
            {
                e.CellStyle.BackColor = Color.LightCoral;
                e.CellStyle.ForeColor = Color.Black;
                e.CellStyle.SelectionBackColor = Color.Maroon;
                e.CellStyle.SelectionForeColor = Color.White;
            }
                //// Master, ref, samples, develop etc. fail = '>0'
            else
            {
                e.CellStyle.BackColor = Color.LightGray;
                e.CellStyle.ForeColor = Color.Black;
                e.CellStyle.SelectionBackColor = Color.DarkGray;
                e.CellStyle.SelectionForeColor = Color.White;
            }

            if (!column.EndsWith("_gr") || (!(e.Value is float))) return;

            var value = (float)e.Value;
            if (Math.Abs(value) >= 1.0)
            {
                e.CellStyle.Font = new Font(Font, FontStyle.Bold);
            }
        }
    }
}
