﻿using System.Data;
using System.Windows.Forms;
using Microsoft.Office.Interop.PowerPoint;
using DataTable = System.Data.DataTable;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser.VirtualDataGridView
{
    /// <summary>
    /// User control derived from DataGridView. Implements a virtual data grid view
    /// to improve the performance of setting and formatting the grid view data.
    /// </summary>
    public partial class VirtualDataGridView : DataGridView
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="VirtualDataGridView"/> class.
        /// </summary>
        public VirtualDataGridView()
        {
            InitializeComponent();

            AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
        }

        /// <summary>
        /// Sets the data table to be displayed in the data grid view.
        /// </summary>
        /// <param name="data">The data table to display.</param>
        public void SetDataTable(DataTable data)
        {
            DataTable = data;

            Rows.Clear();
            Columns.Clear();

            foreach (DataColumn column in DataTable.Columns)
            {
                Columns.Add(column.Caption, column.Caption);
            }

            CellValueNeeded += CellValueNeededHandler;
            CellFormatting += CellFormattingHandler;

            VirtualMode = true;
            RowCount = DataTable.Rows.Count;
        }

        /// <summary>
        /// Sets the data tables columns. Clears any existing columns and rows.
        /// </summary>
        /// <param name="columns">The names of the columns.</param>
        public void SetColumns(params string[] columns)
        {
            DataTable = new DataTable();
            Rows.Clear();
            Columns.Clear();

            foreach (var column in columns)
            {
                Columns.Add(column, column);
                DataTable.Columns.Add(column);
            }
        }

        /// <summary>
        /// Adds a row of values to the table. There must be one value per column.
        /// </summary>
        /// <param name="values">The values to add to the row.</param>
        public void AddRow(params object[] values)
        {
            DataTable.Rows.Add(values);
        }

        /// <summary>
        /// Called to update the table when all the rows have been added.
        /// </summary>
        public void RowsComplete()
        {
            CellValueNeeded += CellValueNeededHandler;
            CellFormatting += CellFormattingHandler;

            VirtualMode = true;
            RowCount = DataTable.Rows.Count;
        }

        /// <summary>
        /// Sorts the data grid view according to the values in the indexed column.
        /// </summary>
        /// <param name="columnIndex">The index of the column to sort the data on.</param>
        public void SortOnColumn(int columnIndex)
        {
            if (Columns.Count <= columnIndex) return;

            var column = Columns[columnIndex].Name;
            var sort = column + " desc";
            if (lastSort == sort)
            {
                sort = column + " asc";
            }
            lastSort = sort;

            Sort(sort);
        }

        /// <summary>
        /// Sorts the data grid view using the sort string.
        /// </summary>
        /// <param name="sort">The sort string to apply.</param>
        public void Sort(string sort)
        {
            if (sort == string.Empty) return;

            var dataView = DataTable.DefaultView;
            dataView.Sort = sort;

            DataTable = dataView.ToTable();
            Invalidate();
        }

        /// <summary>
        /// Event handler to add data to the cells in the data grid view.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event parameters.</param>
        protected virtual void CellValueNeededHandler(object sender, DataGridViewCellValueEventArgs e)
        {
            if (DataTable.Rows.Count > e.RowIndex)
            {
                // Use the store if the e value has been modified  
                // and stored.            
                e.Value = DataTable.Rows[e.RowIndex][e.ColumnIndex];
            }
            else
            {
                e.Value = e.RowIndex;
            }
        }

        /// <summary>
        /// Event handler to format a cell in the data grid view.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event parameters.</param>
        protected virtual void CellFormattingHandler(object sender, DataGridViewCellFormattingEventArgs e)
        {
        }

        /// <summary>
        /// Gets or sets the data table to get the data from.
        /// </summary>
        protected DataTable DataTable { get; set; }

        /// <summary>
        /// The last sort applied used to implement ascending and descending sort order.
        /// </summary>
        private string lastSort = "";
    }
}
