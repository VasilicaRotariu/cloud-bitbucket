﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SudburyDataAnalyser
{
    /// <summary>
    /// Represents an instance of the modified DateTimePicker with additional controls over background colour.
    /// </summary>
    public partial class ExtendedDateTimePicker : DateTimePicker
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ExtendedDateTimePicker"/> class.
        /// </summary>
        public ExtendedDateTimePicker()
        {
            InitializeComponent();

            DropDown += dateTimePicker_DropDown;
            CloseUp += dateTimePicker_CloseUp;
        }

        /// <summary>
        /// Override of the OnPaint event.
        /// </summary>
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);

            // Insert code to do custom painting.
            // If you want to completely change the appearance of your control,
            // do not call base.OnPaint(pe).
        }

        /// <summary>
        /// Override of the background colour fill.
        /// </summary>
        /// <param name="m">Windows message.</param>
        protected override void WndProc(ref Message m)
        {
            // ReSharper disable once InconsistentNaming
            const int WM_ERASEBKGND = 0X14;
            if (m.Msg == WM_ERASEBKGND)
            {
                var g = Graphics.FromHdc(m.WParam);
                if (solidbrushBackBrush == null)
                {
                    solidbrushBackBrush = new SolidBrush(BackColor);
                }
                g.FillRectangle(solidbrushBackBrush, ClientRectangle);
                g.Dispose();
            }
            else
            {
                base.WndProc(ref m);
            }
        }

        /// <summary>Brush of a single colour.</summary>
        private SolidBrush solidbrushBackBrush;

        /// <summary>
        /// Gets or sets the background colour.
        /// </summary>
        [Browsable(true), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public override Color BackColor
        {
            get
            {
                return base.BackColor;
            }

            set
            {
                if (solidbrushBackBrush != null)
                {
                    solidbrushBackBrush.Dispose();
                }
                base.BackColor = value;
                solidbrushBackBrush = new SolidBrush(BackColor);
                Invalidate();
            }
        }

        /// <summary>
        /// Gets the date time in human readable format.
        /// </summary>
        /// <returns>The date time in human readable format.</returns>
        public string DateTimeString
        {
            get { return Value.ToString("dd-MMM-yyyy HH:mm"); }
        }

        /// <summary>
        /// Gets the date time in a format used by SQL.
        /// </summary>
        /// <returns>The date time in a format used by SQL.</returns>
        // ReSharper disable once InconsistentNaming
        public string SQLDateTime
        {
            get { return "CONVERT(DATETIME2, \'{0}\', 126)".Args(Value.ToString("o")); }
        }

        /// <summary>Gets a value indicating whether the SQL date time has changed since the last time it was obtained.</summary>
        public bool SqlValueChanged
        {
            get
            {
                return Value != previousSqlDateTime;
            }
        }

        /// <summary>Gets a value indicating whether the calendar is dropped down.</summary>
        public bool IsDroppedDown { get; private set; }

        /// <summary>
        /// Called when the calendar is dropped down.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void dateTimePicker_DropDown(object sender, EventArgs e)
        {
            IsDroppedDown = true;
        }

        /// <summary>
        /// Called when the calendar is closed.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void dateTimePicker_CloseUp(object sender, EventArgs e)
        {
            IsDroppedDown = false;
        }

        /// <summary>The date time value when SQL date time was last obtained.</summary>
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private DateTime previousSqlDateTime = new DateTime();
    }
}
