﻿namespace SudburyDataAnalyser
{
    /// <summary>
    /// Key based on UTID and plant number used to identify rows in queries.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class UTIDPlantNumberKey
    {
        /// <summary>
        /// Gets the UTID.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public int UTID { get; private set; }

        /// <summary>
        /// Gets the plant number.
        /// </summary>
        public short PlantNumber { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the key is valid.
        /// </summary>
        public bool Valid
        {
            get
            {
                return PlantNumber > 0;
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="UTIDPlantNumberKey"/> class.
        /// </summary>
        /// <param name="utid">The UTUD.</param>
        /// <param name="plantNumber">The plant number.</param>
        public UTIDPlantNumberKey(int utid, short plantNumber)
        {
            UTID = utid;
            PlantNumber = plantNumber;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="UTIDPlantNumberKey"/> class.
        /// </summary>
        /// <param name="utid">The UTUD.</param>
        /// <param name="plantNumber">The plant number.</param>
        public UTIDPlantNumberKey(string utid, string plantNumber)
        {
            UTID = utid.ToInt();
            PlantNumber = (short)plantNumber.ToInt();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="UTIDPlantNumberKey"/> class.
        /// </summary>
        /// <param name="value">The UTUD and plant number.</param>
        public UTIDPlantNumberKey(string value)
        {
            int utid = 0;
            short plantNumber = 0;
            bool foundSeparator = false;

            foreach (var character in value)
            {
                if ((character < '0') || (character > '9'))
                {
                    if (foundSeparator)
                    {
                        break;
                    }
                    foundSeparator = true;
                }
                else
                {
                    if (foundSeparator)
                    {
                        plantNumber *= 10;
                        plantNumber += (short)(character - '0');
                    }
                    else
                    {
                        utid *= 10;
                        utid += character - '0';
                    }
                }
            }

            UTID = utid;
            PlantNumber = plantNumber;
        }

        /// <summary>
        /// Overrides the equals method. Returns true if the specified object is the same
        /// as this key.
        /// </summary>
        /// <param name="obj">The object to compare with this key.</param>
        /// <returns>True if the object is the same as this key; false otherwise.</returns>
        public override bool Equals(object obj)
        {
            var key = obj as UTIDPlantNumberKey;
            if (key == null) return false;

            return (key.UTID == UTID) && (key.PlantNumber == PlantNumber);
        }

        /// <summary>
        /// Returns a hash code for this key.
        /// </summary>
        /// <returns>Hash code for the key.</returns>
        public override int GetHashCode()
        {
            return UTID ^ PlantNumber << 16;
        }

        /// <summary>
        /// Returns the key as a string in the format "(UTID, plant number)"
        /// </summary>
        /// <returns>The key as a string.</returns>
        public override string ToString()
        {
            return "({0},{1})".Args(UTID, PlantNumber);
        }
    }
}
