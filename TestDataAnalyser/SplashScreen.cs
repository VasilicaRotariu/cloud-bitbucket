﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SudburyDataAnalyser
{
    /// <summary>
    /// The Splash screen for loading big data
    /// </summary>
    public partial class SplashScreen : Form
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="SplashScreen" /> class
        /// </summary>
        public SplashScreen()
        {
            InitializeComponent();
        }

        #region ************************* Form Control Methods **************************

        /// <summary>
        /// Sets the Loading Text
        /// </summary>
        /// <param name="headerText">The Loading Text</param>
        public void SetLoadingHeader(string headerText)
        {
            loadingHeaderText.Text = headerText;
        }

        /// <summary>
        /// Sets the description Text
        /// </summary>
        /// <param name="text">The description Text</param>
        public void SetLoadingDescription(string text)
        {
            loadingDescriptionText.Text = text;
        }  
        #endregion
    }
}
