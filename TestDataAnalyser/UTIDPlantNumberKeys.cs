﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace SudburyDataAnalyser
{
    /// <summary>
    /// Class representing a list of UTID plant number keys.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class UTIDPlantNumberKeys : HashSet<UTIDPlantNumberKey>
    {
        /// <summary>
        /// Returns a list of keys from the rows in the data grid view.
        /// </summary>
        /// <param name="data">The data table to get the keys from.</param>
        /// <returns>A list of keys taken from the data grid view.</returns>
        public static UTIDPlantNumberKeys GetKeys(DataTable data)
        {
            var keys = new UTIDPlantNumberKeys();
            foreach (DataRow dr in data.Rows)
            {
                // Grab utids from the selected rows
                var utid = (int)dr["UTID"];
                var plantNumber = (short)dr["PlantNumber"];

                var key = new UTIDPlantNumberKey(utid, plantNumber);
                keys.Add(key);
            }

            return keys;
        }

        /// <summary>
        /// Add a new UTID and plant number key to the list.
        /// </summary>
        /// <param name="utid">The UTID.</param>
        /// <param name="plantNumber">The plant number.</param>
        public void Add(int utid, short plantNumber)
        {
            Add(new UTIDPlantNumberKey(utid, plantNumber));
        }

        /// <summary>
        /// Adds a list of keys to this list of keys.
        /// </summary>
        /// <param name="keys">The keys to add.</param>
        public void Add(UTIDPlantNumberKeys keys)
        {
            foreach (var key in keys)
            {
                Add(key);
            }
        }

        /// <summary>
        /// Adds a list of UTIDs with the same plant number to this list of keys.
        /// </summary>
        /// <param name="utids">The list of UTIDs to add.</param>
        /// <param name="plantNumber">The plant number.</param>
        public void Add(IEnumerable<int> utids, short plantNumber)
        {
            foreach (var utid in utids)
            {
                Add(utid, plantNumber);
            }
        }

        /// <summary>
        /// Returns the list of keys as a string.
        /// </summary>
        public override string ToString()
        {
            string ret = string.Join(",", this);
            return ret;
        }

        /// <summary>
        /// Gets a list of the plant numbers.
        /// </summary>
        public List<short> PlantNumbers
        {
            get
            {
                var plantNumbers = new HashSet<short>();

                foreach (var key in this)
                {
                    if (!plantNumbers.Contains(key.PlantNumber))
                    {
                        plantNumbers.Add(key.PlantNumber);
                    }
                }

                return plantNumbers.ToList();
            }
        }
    }
}
