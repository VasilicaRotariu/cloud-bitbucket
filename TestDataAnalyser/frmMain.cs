﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.IO;
using System.Reflection;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Eventing.Reader;
using System.Security.Cryptography.X509Certificates;
using SudburyDataAnalyser.Database;
using SudburyDataAnalyser.Properties;
using SudburyDataAnalyser.Queries.DataTable;
using SudburyDataAnalyser.UniqueIdents;
using NAppUpdate.Framework;
using TeamCityUpdater;
using SudburyDataAnalyser.ChartTraces;
using Timer = System.Windows.Forms.Timer;

namespace SudburyDataAnalyser
{
    #region Completed tasks

    // COMPLETE:- Pump test - update so tracker charts and detailed data tab
    // COMPLETE:- Add leakdown data - need to distinguish between long and short leak test.
    // COMPLETE:- Make first word of query have to be "SELECT" to prevent unwanted deletions!  Eventually change to readonly access connection.
    // COMPLETE:- Give option for mean, median, max, min, sd on charts, not just mean.
    // COMPLETE:- Add ISNs & PDates to utids on detailed data page
    // COMPLETE:- Fix making big charts when double-clicking - static methods may help?
    // COMPLETE:- Work out how/when to bring back results for injectors that aborted early - required for true FTPR/failure mode analysis
    // COMPLETE:- Pass rate tracker table - count records retrieved, count "Global = 0" and "Global = -1" to sort table.
    // COMPLETE:- On pass rate table, only populate rows if there are any records.
    // COMPLETE:- Make front page query builder work.
    // COMPLETE:- Colour code trimtrackerdatagrids red/green by global value (0 = Pass = green, >0 = Fail = Red)
    // COMPLETE:- Closed loop log chart for fuel error against demand.
    // COMPLETE:- Get dataset series splitter to work to try to colour code charts better.
    // COMPLETE:- Add opening and closing delay to trim tracker charts - remove pressure level 6 and move pass rate tracker to different tab
    // COMPLETE:- Add TestType and Testplan to front page options
    // COMPLETE:- Put titles and axes titles on charts.  Partly done, needs checking and tracker charts need looking at.
    // COMPLETE:- Make sure failed open loop tests get put in trimTrackerDataGridView so can be plotted - ask Andy to assign these with a different flag, (-2 maybe).
    // COMPLETE:- Get TestNumber selection to work - maybe with set options of "All", "1st" or "Latest", default to All.
    // COMPLETE:- Open loop nominal curves on plots
    // COMPLETE:- Convert for new database structure - DON'T DO ANYTHING ELSE ON THIS UNTIL THIS WORKS!!!  Working backup held in %Testrig
    // COMPLETE:- Update connection string to VSQL01
    // COMPLETE:- CLT changes in db - see Andy's email
    // COMPLETE:- Get LineNumber selection on front screen to work when available in data
    // COMPLETE:- Option for passes/fails only
    // COMPLETE:- Change OMV chart to show a marker - only one pressure level now, so no line showing
    // COMPLETE:- Create FOD charts - angle versus parameters (similar to OMV)
    // COMPLETE:- Assign SORate a group (new PO criterion)
    // COMPLETE:- Add row numbers to datagrids (for Andy)
    // COMPLETE:- Return trim codes in trim tracker data
    // COMPLETE:- Implement Failure analysis page - Build in rework decisions so it can generate a pareto
    // COMPLETE:- Make sure trim tracker datagrids repaint on sorting columns - currently loses red/green
    // COMPLETE:- Release Memory Streams from BigCharts?  Reduce memory usage once series are populated?
    // COMPLETE:- Implement chart traces
    // COMPLETE:- Chart legends also need sorting.
    // COMPLETE:- Also need chart colours to be sorted in a slightly better way
    // COMPLETE:- Also add "LOP" to "Group data by"
    // Tebbo's list
    // 1.COMPLETE:- Still getting the problem where if I leave it for a while with a query, then try to rerun it fails.
    // 2.COMPLETE:- Want a check box to allow incomplete tests to show in the main results.  
    // 3.COMPLETE:- OMV and FOD don’t work anymore in detailed results. I think this is down to there being no NCV results so it crashes there, look at any pumpcheck testplan to see this. 
    // 4.COMPLETE:- Need to call back forced trim in the results window. 
    // 5.COMPLETE:- Need to be able to select whether you want forced trim, pumpcheck will always run with forced trim so maybe auto selected for testtype pumpcheck, and auto not selected for all other testtypes.  
    // 6.COMPLETE:- Would be useful to have the actual point which caused the failure of the injector highlighted, even if it’s just bold. 
    // 7.COMPLETE:- In detailed data, group by ident could be useful for comparing testplan changes. 
    // 8.COMPLETE:- In chart traces too.
    // 9.COMPLETE:- Also in chart traces group by isn?
    // 10.COMPLETE:- Trim tracker needs to be different for pumpcheck tests. OMVT3a at 3000b, TotalAngle at 3000b, Pressure at fixed angle, omvt3a at fixed angle, etc. etc. All in grade format. -22 or something. 
    // 11.COMPLETE:- Trim tracker should maybe have sortime grade, otherwise we are likely to miss rate shape changes until it’s way out and failing. 
    // 12.COMPLETE:- Thinking it could be helpful to have trimtracker always work from grade values anyway, so any way off scale won’t make it look so odd?
    // COMPLETE:- Give option not return nominal dataset (default not to?) - to speed things up.
    // COMPLETE:- Consider removing hidden datagrids on detailed data and just plotting from datatables
    // COMPLETE:- Trimtracker formatting - optimise as seems very slow at the moment.
    // COMPLETE:- Chart trace to csv button
    // COMPLETE:- When choosing date from or to, don't re-query until date selected properly - if scrolling back months, it re-queries each time - stop this!
    // COMPLETE:- Investigate slow FailureAnalysis query & slow detailed data queries - optimise queries using SQL Management Studio
    // COMPLETE:- As the analyser opens, it populates the boxes and probably runs loads of queries, which may not be necessary - look into this.
    // COMPLETE:- Just changed event type to CellMouseClick from SelectionChanged on each of the datagridviews
    // COMPLETE:- Window sizing/resizing options - fit for laptop screen and if maximised etc, get components to shrink/strecth as they should.  
    // COMPLETE:- Change start and end date to 06:00 default
    // COMPLETE:- Remove datatables for detailed data and chart trace data so that data is transferred directly into the class lists
    // COMPLETE:- Make sure queries which have "WHERE UTID IN"... also makes sure the plantnumber is listed.  Also matters for UTID specified by user.
    // COMPLETE:- FAILURE ANALYSIS database tables need to be moved onto F2Test db or cross-db query required in group query
    // COMPLETE:- Separate BigChart into dll
    // COMPLETE:- Fix all queries to run from either database - add bool switch
    // COMPLETE:- Update ParseBlob with new version which deals with x-scaling correctly
    // COMPLETE:- Anywhere that known columns are joined (global, early end etc.) add a temp table and join that way instead - seems much quicker.
    // COMPLETE:- Fix Global values for chart series colours - no longer just 0 for pass, anything else for fail
    // COMPLETE:- Add stabilisation tab to detailed data for new DB.
    // COMPLETE:- Fix bug where UTID list not updated if query already run.
    // COMPLETE:- Get additional selection working properly for new db structure
    //               - COMPLETE:- 1st time tests, most recent tests.
    //               - COMPLETE:- Passes, fails
    // COMPLETE:- Specific ISNs are applied even when checkbox is disabled.
    // COMPLETE:- Add Trend chart for new DB structure only - held in BLOBs
    // COMPLETE:- Update ChartTraces for new DB structure as links are different
    // COMPLETE:- If you select open loop and closed loop chart traces, you get duplicate series names which causes an exception
    // COMPLETE:- First and latest test queries not working correctly still - add rigtype/testtype (new/old db) to test number select statement.
    // COMPLETE:- Only run fails through FailureAnalysis checks (takes ages otherwise)
    // COMPLETE:- Update nominal data for new database structure
    #endregion

    #region Outstanding tasks

    // BUGS
    // Treeviews has a checkbox where they shouldn't - first pressure level on open loop only.  Can't understand it.

    // FOR NEW DATABASE STRUCTURE:-
    // IN PROGRESS:- Add switch to detailed data classes
    //              - COMPLETE:- OL, CL, OLNom
    //              - IN PROGRESS:- Remove Leakdown for old rig (no longer used) and put them in for new rig, maybe as part of chart trace tab?  Links ChartTrace by URID to DataHeader.
    //              - FOD/OMV

    // NEW FEATURES:-
    // Add plantnumber / line number colour/point type coding to charts
    //              - Add to detailed data & chart trace in "Group data by:" options.
    //              - IN PROGRESS:- Also add a "Rig Alignment" tab as probably too difficult to see on TrimTrackerCharts to include:-
    //                  - IN PROGRESS:- Frequency plots for line-to-line.
    //                          - Needs colours sorting.
    //                  - Summary table of rig+line to line on all parameters (trims first?)
    // Failure analysis drill down to component level data, including build data.
    // Group data in trimTracker dataset so that they can be plotted as different series - useful for fast tracks
    // IN PROGRESS:- Generic trend chart implementation
    //              - Pumping to be added
    //              - Chronological order feature to be added
    //              - Ability to select certain datapoints for utids (useful for highlighting certain engine sets for example)
    //              - Colour coding - allow splitting data by testplan, ident etc... as per detailed data and chart traces
    //              - Add a "Deselect all" button on y-axis at least
    //              - Allow series to be placed onto secondary y-axis
    //              - Add ability to change chart to line chart
    //              - If legend clicked, expand treeview to that node - easier to find if needed to deselect, or allow a right click and delete series option which then un-checks given series
    // IN PROGRESS:- X-Y chart implementation
    //              - Pumping to be added
    //              - Colour coding - allow splitting data by testplan, ident etc... as per detailed data and chart traces
    //              - Allow series to be placed onto secondary y-axis                    
    //              - Add a "Deselect all" button on y-axis at least
    //              - Add trend line option - maybe as final child node, along with secondary axis option
    // IN PROGRESS:- Plot CL grades in a way that illustrates where injectors are failing at each test point & distribution at each
    // Parameter correlation generator, like Desmo's colour coded triangular grid
    // Add test number option on main query   

    // DATABASE QUERY IMPROVEMENTS/FIXES:-
    // On Main screen, Specific utids will need to also have a plantnumber reference - maybe make a list with 2 columns?
    // On detailed data and chart trace data, when pulling back list of utids from trimtracker, also need to pull back plant numbers to ensure the correct data is retrieved and plotted.    

    // PERFORMANCE IMPROVEMENTS:-
    // Consider removing datatables from x-y charts, generic trend charts, trim tracker, incomplete test tracker etc...

    // EXTERNAL PROBLEMS
    // Add SQL exception capturing to FailureAnalysis (within FTypeRework.dll)
    // BigChart needs putting in Externals and referencing in libs (like FTypeRework)
    // IN PROGRESS:- Add axis min/max/interval setting to BigCharts - Ensure only valid values can be added (i.e. min < max)
    // IN PROGRESS:- BigCharts - allow secondary axis, set better colours, marker types...
    // Big charts - investigate speed of serialization of data - slow on Chart traces; is there anything that can be done to speed it up or reduce memory usage?
    // Multi highlight for same ISN? (Woodley's request) 

    // MISC:-
    // IN PROGRESS:- Add error catching in all relevant places (try catches put around sql queries - need to look at program flow (bools for "ok"?)
    // Chart Traces - give option to choose which series types to plot - i.e. rate/drate/pressure only for example to reduce data & keep charts tidy
    // Put limits on plots 
    // Tidy up UpdateCLPlots() method - it's horrible.
    // Implement Andy's offsets - see email
    // Put nominal/helpful lines on closed loop charts
    // Detailed data splitting by Date (day/week/month/batch?)    
    // Add extra chart showing MDP shifted open loop against closed loop curve - may help with understanding divergence.
    // IN PROGRESS:- Add status bar to show what stage the program is at.  Need to put in different thread http://msdn.microsoft.com/en-us/library/aa645740(v=vs.71).aspx
    // Improve the way the csv data is saved - duration as fields to help with analysis, rather than just one column.
    // Look at ways of speeding up csv writing & fix problem where a cell with a blank first character makes a new line
    // Check data format in csv files - Sion says it saves some stuff as text which should be values.
    // Change icon to something more suitable/unique
    #endregion
    ///Dawid
    /// <summary>
    /// Represents the main analyser form.
    /// Contains most of the specific functionality of the main form.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1123:DoNotPlaceRegionsWithinElements", Justification = "Becuase it looks Ugly otherwise.")]
    public partial class FrmMain : Form
    {
        #region *********************** Global variables ***********************
        /// <summary>The Statics Data Window object </summary>
        private MeanMedianData statisticDataWindow;

        /// <summary>The current database to query.</summary>
        private DatabaseBase currentDatabase = new DatabaseBase();

        /// <summary>The virgin database to query.</summary>
        private InjectorComponentDatabase injectorComponentsDatabase;

        /// <summary> Gets or sets a value indicating whether the next UI update event should be supressed </summary>
        private bool SilentUIUpdate { get; set; }

        /// <summary> Gets or sets a set of missing ISNs from the Injector Component Query</summary>
        private List<string> ComponentMissingISNS { get; set; }

        /// <summary>Cache of FOD data.</summary>        
        private FOD.DataCache fodData = new FOD.DataCache();

        /// <summary>Cache of OMV data.</summary> 
        private OMV.DataCache omvData = new OMV.DataCache();

        /// <summary>Cache of open loop data.</summary> 
        private NCV.DataCache openLoopData = new NCV.DataCache();

        /// <summary>Cache of Untrimmed data.</summary> 
        private Untrimmed.DataCache untrimmedData = new Untrimmed.DataCache();

        /// <summary>Cache of open loop nominal data.</summary> 
        private List<NCVNominal.Row> openLoopNominalData = new List<NCVNominal.Row>();

        /// <summary>Cache of closed loop data.</summary> 
        private CLT.DataCache closedLoopData = new CLT.DataCache();

        /// <summary>Cache of trimmed data.</summary> 
        private Trimmed.DataCache trimmedData = new Trimmed.DataCache();

        /// <summary>Cache of leak data.</summary> 
        private Leak.DataCache leakData = new Leak.DataCache();

        /// <summary>Cache of open loop stabilise data.</summary> 
        private OLStabilise.DataCache openLoopStabiliseData = new OLStabilise.DataCache();

        /// <summary>Cache of closed loop stabilise data.</summary> 
        private CLStabilise.DataCache closedLoopStabiliseData = new CLStabilise.DataCache();

        /// <summary>Cache of open loop chart trace data.</summary>
        private ChartTraceNCV.DataCache chartTraceDataOL = new ChartTraceNCV.DataCache();

        /// <summary>Cache of closed loop chart trace data.</summary>
        private ChartTraceCLT.DataCache chartTraceDataCL = new ChartTraceCLT.DataCache();

        /// <summary>Cache of closed loop stabilise chart trace data.</summary>
        private ChartTraceCLS.DataCache chartTraceDataCLS = new ChartTraceCLS.DataCache();

        /// <summary>Cache of FOD chart trace data.</summary>
        private ChartTraceFOD.DataCache chartTraceDataFOD = new ChartTraceFOD.DataCache();

        /// <summary>Cache of OMV chart trace data.</summary>
        private ChartTraceOMV.DataCache chartTraceDataOMV = new ChartTraceOMV.DataCache();

        // ReSharper restore FieldCanBeMadeReadOnly.Local

        /// <summary>Data table to store the summarised completed calibration test tracker data.</summary>
        private CompleteCalibrationDataTable completeCalibrationData = new CompleteCalibrationDataTable();

        /// <summary>Data table to store the summarised incomplete calibration test tracker data.</summary>
        private IncompleteCalibrationDataTable incompleteCalibrationData = new IncompleteCalibrationDataTable();

        /// <summary>Data table to store the summarised completed pumping test tracker data.</summary>
        private CompletePumpingDataTable completePumpingData = new CompletePumpingDataTable();

        /// <summary>Data table to store the summarised incomplete pumping test tracker data.</summary>
        private IncompletePumpingDataTable incompletePumpingData = new IncompletePumpingDataTable();

        /// <summary>List of the keys selected in the detailed list view.</summary>
        private UTIDPlantNumberKeys detailedListViewSelectedKeys = new UTIDPlantNumberKeys();

        /// <summary>Persistent storage of the chart traces settings.</summary>
        private Persistent.ChartTraces chartTracesSettings = new Persistent.ChartTraces();

        /// <summary>Persistent storage of the detailed data settings.</summary>
        private Persistent.DetailedData detailedDataSettings = new Persistent.DetailedData();

        /// <summary>The rig alignment charts.</summary>
        private RigAlignmentCharts rigAlignmentCharts;

        /// <summary>Flag used to disable updating performance plots when updating data.</summary>
        private bool disablePlotPerformanceData;

        /// <summary>
        /// The available update types.
        /// </summary>
        private enum UpdateType
        {
            UpdateLopList,
            UpdateTestPlanList,
            UpdateTestTypeList,
            UpdatePlantNumberList,
            UpdateLineNumberList,
            UpdateNothing
        }

        /// <summary>The next data grid view update to apply.</summary>
        private UpdateType queryUpdateType = UpdateType.UpdateNothing;

        /// <summary>The update counter. When zero, the net update is applied.</summary>
        private int queryUpdateTimerCounter = -1;

        #endregion

        /// <summary>
        /// Initialises a new instance of the <see cref="FrmMain"/> class.
        /// Sets DateTimePickers on Query tab to default values and runs GetDatabase() to populate form.
        /// </summary>
        ///

        public static DateTime DateFrom;
        public static DateTime DateTo;
        //proced data variables
        private SqlDataAdapter dataAdapter = new SqlDataAdapter();
        private BindingSource bindingSource1 = new BindingSource();
        private BindingSource bindingSource2 = new BindingSource();
        private BindingSource bindingSource3 = new BindingSource();

        public FrmMain()
        {
            DebugLog.Enabled = true;
            DebugLog.FileName = "c:\\DataAnalyser\\DebugLog.txt";
            DebugLog.Clear();
            SilentUIUpdate = false;
            InitializeComponent();

            rigAlignmentCharts = new RigAlignmentCharts(rigAlignmentPanel);

            // ReSharper disable once DoNotCallOverridableMethodsInConstructor
            Text += " : build {0}".Args(Assembly.GetExecutingAssembly().GetName().Version.ToString());

            // Set the date time pickers for the last day & update public date variables
            var dateTo = new TimeSpan(1, 6, 0, 0);
            var dateFrom = new TimeSpan(18, 0, 0);
            //dateFromExtDateTimePicker.Value = DateTime.Today.Subtract(dateFrom);
            //dateToExtDateTimePicker.Value = DateTime.Today.Add(dateTo);

            // ReSharper disable once UnusedVariable
            var axisScale = new ChartAxisScaling.AxisScaler(new MdegIasiDatabase(), "AnalyserChartAxisSettings.xml");

            // Get selected database
            // Wrapped in a try catch now due to connecting to a default database,
            // if this database is unavalaible it would crash the application for any user.
            // this fix should still run the app if connection fails
            // Alex - jz5w78
            try
            {
                //Hide the Loading screen
                //dawid 25/03
                GetDatabase();
            }
            catch (SqlException)
            {
                MessageBox.Show("An Error occured while connecting to the database '{0}'.\n" +
                    "Please check its availability.".Args(currentDatabase.ConnectionString));
            }

            dateFromExtDateTimePicker.TextChanged += dateTimePicker_DateChanged;
            dateToExtDateTimePicker.TextChanged += dateTimePicker_DateChanged;
          

            trimmedCharts.ChartLegend = trimmedChartIndividualChartsLegend;
            statisticDataWindow = new MeanMedianData();

            //Check for Updates
#if !DEBUG
            Thread thread = new Thread(CheckForUpdates) { IsBackground = true };
            thread.Start();
            CheckForUpdates();
#endif
        }

        /// <summary>
        /// Method upon loading of main analyser form.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            dgvProcessData.DataSource = bindingSource1;
            dgvProcessDataPack.DataSource = bindingSource2;
            dgvDaimlerISN.DataSource = bindingSource3;
            //GetData("EXEC	[Euro6].[dbo].[sp_ProcessFlow]	@SerialNumber ='" + this.tbProcessDataSerial.Text + "'");
            // Nothing added to this event yet.
        }

        /// <summary>
        /// Sets the status text, allowing for threading.
        /// </summary>
        /// <param name="status">The new status text.</param>
        private void SetStatus(string status)
        {
            mainStatusStrip.InvokeIfRequired(c =>
            {
                mainStatusStripLabel.Text = status;
                //dawid 25/03
                c.Update();
            });
        }

        /// <summary>
        /// Gets the start date from the from date time picker.
        /// </summary>
        /// <returns>The start date in SQL date time format.</returns>
        private string StartDate
        {
            get { return dateFromExtDateTimePicker.SQLDateTime; }
        }

        /// <summary>
        /// Gets the start date from the from date time picker.
        /// </summary>
        /// <returns>The start date in SQL date time format.</returns>
        private string EndDate
        {
            get { return dateToExtDateTimePicker.SQLDateTime; }
        }

        /// <summary>
        /// Sets database connection strings based on current selection on Query tab,
        /// sets database type flag (old "FT" style or new "non-FT" style),
        /// runs initial queries to populate form
        /// and builds tracking queries based on selection.
        /// </summary>
        private void GetDatabase()
        {
            injectorComponentsDatabase = null;

            // -----
            // Stonehouse Databases
            if (vsql08OldDBRadioButton.Checked)
            {
                currentDatabase = new Vsql08OldDatabase();
            }
            else if (vsql08NewDBRadioButton.Checked)
            {
                currentDatabase = new Vsql08NewDatabase();
            }
            else if (mdegDB01RadioButton.Checked)
            {
                currentDatabase = new MdegIasiDatabase();
                currentDatabase.IasiArchiveDatabase = new MdegIasiDatabaseArchive();
                currentDatabase.SudburyArchiveDatabase = new MdegSudburyDatabaseArchive();
                currentDatabase.QueryIasiArchiveStructure = checkBoxUseIasiArchive.Checked;
                currentDatabase.QuerySudburyArchiveStructure = checkBoxUseSudburyArchive.Checked;

            }
            else if (dafDB01RadioButton.Checked)
            {
                currentDatabase = new DafIasiDatabase();
                currentDatabase.IasiArchiveDatabase = new DafIasiDatabaseArchive();
                currentDatabase.SudburyArchiveDatabase = new DafSudburyDatabaseArchive();
                currentDatabase.QueryIasiArchiveStructure = checkBoxUseIasiArchive.Checked;
                currentDatabase.QuerySudburyArchiveStructure = checkBoxUseSudburyArchive.Checked;
            }
            else
            {
                MessageBox.Show(Resources.No_valid_database_selected);
            }

            if (currentDatabase.ProductType == DatabaseProductType.MDEG)
                injectorComponentsDatabase = new MdegInjectorComponentDatabase();
            else
                injectorComponentsDatabase = new InjectorComponentDatabase();

            QueryModifier.FTDBStructure = currentDatabase.FtStructure;

            // Update front screen for current selected dates
            UpdateLOPListDataGridView();
            BuildPivotQuery();
        }

        /// <summary>
        /// List view key press handler used to select all items on a control A
        /// key press.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void ListView_CtrlAHandler(object sender, KeyPressEventArgs e)
        {
            var listView = sender as ListView;

            if (e.KeyChar != 1) return;
            if (listView == null) return;

            foreach (ListViewItem item in listView.Items)
            {
                item.Selected = true;
            }

            e.Handled = true;
        }

        #region *********************** Main Query ***********************

        /// <summary>
        /// Captures database selection change and fires GetDatabase().
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void database_CheckedChanged(object sender, EventArgs e)
        {
           //dawid GetDatabase();

            // If we are Mdeg, disable the PG Filter and enable virgin filter
            if (currentDatabase.ProductType == DatabaseProductType.MDEG)
            {
                //change chart scaling to MDEG
                var axisScale = new ChartAxisScaling.AxisScaler(currentDatabase, "AnalyserChartAxisSettings.xml");
            }
            else //Else Set its Enabled to the virgin filter
            {
                //change chart scaling to DAF
                var axisScale = new ChartAxisScaling.AxisScaler(currentDatabase, "AnalyserChartAxisSettings.xml");
            }

            // Clear any stored information from previous queries
            ClearPreviousQueryData();
        }

        /// <summary>
        /// Gets the main query parameters.
        /// </summary>
        /// <returns>The main query parameters.</returns>
        private DataTableQueryParameters GetMainQueryParameters()
        {
            var listIsNs = new List<string>();
            if (specificISNsCheckBox.Checked)
            {
                listIsNs = specificISNsTextBox.Lines.ToList();
            }

            var listutids = new List<string>();
            var listutidPlantNumbers = new UTIDPlantNumberKeys();
            if (specificutidsCheckBox.Checked)
            {
                foreach (string line in specificutidsTextBox.Lines)
                {
                    if (line.IsEmpty())
                        continue;

                    var key = new UTIDPlantNumberKey(line);
                    if (key.Valid)
                    {
                        listutidPlantNumbers.Add(key);
                    }
                    else
                    {
                        listutids.Add(line);
                    }
                }
            }

            string testNumber;
            if (allTestsRadioButton.Checked)
            {
                testNumber = "All";
            }
            else if (firstTimeTestsRadioButton.Checked)
            {
                testNumber = "First";
            }
            else if (latestTestsRadioButton.Checked)
            {
                testNumber = "Latest";
            }
            else
            {
                testNumber = "Unknown";
            }

            string result;
            if (passesAndFailsRadioButton.Checked)
            {
                result = "All";
            }
            else if (passesOnlyRadioButton.Checked)
            {
                result = "Passes";
            }
            else if (failsOnlyRadioButton.Checked)
            {
                result = "Fails";
            }
            else
            {
                result = "Unknown";
            }

           
            return new DataTableQueryParameters(
                dateFromExtDateTimePicker.SQLDateTime,
                dateToExtDateTimePicker.SQLDateTime,
                lopListDataGridView.SelectedRows.CellValues("LOP"),
                testplanListDataGridView.SelectedRows.CellValues("TestPlan"),
                testTypeListDataGridView.SelectedRows.CellValues("TestType"),
                plantNumberListDataGridView.SelectedRows.CellValues("PlantNumber"),
                lineNumberListDataGridView.SelectedRows.CellValues("LineNumber"),
                listIsNs,
                NumericISNCheckBox.Checked,
                listutids,
                listutidPlantNumbers,
                testNumber,
                result,
                currentDatabase);
        }

        /// <summary>
        /// Visually enables the plant number data grid view.
        /// </summary>
        private void EnablePlantNumberGridView()
        {
            if (plantNumberListDataGridView.BackgroundColor == SystemColors.Window) return;

            plantNumberListDataGridView.BackgroundColor = SystemColors.Window;
            foreach (DataGridViewRow row in plantNumberListDataGridView.Rows)
            {
                row.DefaultCellStyle.BackColor = SystemColors.Window;
            }

            if (plantNumberListDataGridView.Rows.Count > 0)
            {
                plantNumberListDataGridView.Rows[0].Selected = true;
            }
        }

        /// <summary>
        /// Visually disables the plant number data grid view.
        /// </summary>
        private void DisablePlantNumberGridView()
        {
            if (plantNumberListDataGridView.BackgroundColor != SystemColors.Window) return;

            plantNumberListDataGridView.BackgroundColor = Color.LightGray;
            foreach (DataGridViewRow row in plantNumberListDataGridView.Rows)
            {
                row.DefaultCellStyle.BackColor = Color.LightGray;
            }

            plantNumberListDataGridView.ClearSelection();
        }

        /// <summary>
        /// Shows the UTIDs list text box in an error state when a mixture of UTID and 
        /// UTID, plant numbers pairs are entered.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void ShowUTIDTextBoxError()
        {
            specificutidsTextBox.BackColor = Color.LightCoral;
            toolTip.SetToolTip(specificutidsTextBox, "The list must contain a list of UTIDs or a list of UTID Plant Number pairs, not a mix of both.");
        }

        /// <summary>
        /// Shows the UTIDs list text box in an normal state.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void HideUTIDTextBoxError()
        {
            specificutidsTextBox.BackColor = specificutidsCheckBox.Checked ? SystemColors.Window : Color.LightGray;
            toolTip.SetToolTip(specificutidsTextBox, string.Empty);
        }

        /// <summary>
        /// Retrieves data from user selection on Query tab.
        /// </summary>
        private void BuildPivotQuery()
        {
            DebugLog.WriteStart("BuildPivotQuery");

            // ReSharper disable once UnusedVariable
            using (var waitCursor = new WaitCursor())
            {
                //Calcualte new Parameters for the queries
                var parameters = completeCalibrationData.Parameters = completePumpingData.Parameters =
                    incompleteCalibrationData.Parameters = incompletePumpingData.Parameters = GetMainQueryParameters();

                //Populate the query boxes on the interface
                completeCalibrationQueryTextBox.Text = completeCalibrationData.GetQuery();
                incompleteCalibrationQueryTextBox.Text = incompleteCalibrationData.GetQuery();
                completePumpingQueryTextBox.Text = completePumpingData.GetQuery();
                incompletePumpingQueryTextBox.Text = incompletePumpingData.GetQuery();

                //Get all ISNs
                List<string> returnedISNs = new List<string>();
                returnedISNs.AddRange(completeCalibrationData.GetUniqueTestIDs().Select(s => s.ISN));
                returnedISNs.AddRange(completePumpingData.GetUniqueTestIDs().Select(s => s.ISN));
                returnedISNs.AddRange(incompleteCalibrationData.GetUniqueTestIDs().Select(s => s.ISN));
                returnedISNs.AddRange(incompletePumpingData.GetUniqueTestIDs().Select(s => s.ISN));

                injCountLabel.Text = returnedISNs.Count.ToString(CultureInfo.InvariantCulture);

               

                //Specific UTIDS
                if (specificutidsCheckBox.Checked)
                {
                    if (parameters.Keys.Count == 0)
                    {
                        EnablePlantNumberGridView();
                        HideUTIDTextBoxError();
                    }
                    else
                    {
                        DisablePlantNumberGridView();
                        if (parameters.UTIDs.Count > 0)
                            ShowUTIDTextBoxError();
                        else
                            HideUTIDTextBoxError();
                    }
                }
                else
                {
                    EnablePlantNumberGridView();
                    HideUTIDTextBoxError();
                }
            }

            DebugLog.WriteEnd("BuildPivotQuery");
        }

        /// <summary>
        /// Captures run query button click event and runs each of relevant queries from the textboxes
        /// on the Query tab.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void runMainQueryButton_Click(object sender, EventArgs e)
        {
            // Clear any stored information from previous queries
            ClearPreviousQueryData();

            // Set status strip label
            SetStatus("Running query...");

            // Build main query - required to rebuild UTID list in case nothing new selected but button re-clicked when new data exists
            BuildPivotQuery();

            // Complete calibration tests
            completeCalibrationData.SetDataTable();
            completedCalibrationDataGridView.SetDataTable(completeCalibrationData.DataTable);

            // Incomplete calibration tests
            incompleteCalibrationData.SetDataTable();
            incompleteCalibrationDataGridView.SetDataTable(incompleteCalibrationData.DataTable);

            // Complete pumping tests
            completePumpingData.SetDataTable();
            completedPumpingDataGridView.SetDataTable(completePumpingData.DataTable);

            // Incomplete pumping tests
            incompletePumpingData.SetDataTable();
            incompletePumpingDataGridView.SetDataTable(incompletePumpingData.DataTable);

            // Set status strip label
            SetStatus("Updating datasets...");

            // Move to dataset tab and format datagrids
            mainTabControl.SelectedTab = datasetTabPage;

            UpdateCountsLabels(
                completeCalibrationData.ProductionPassCount(),
                completeCalibrationData.NonProductionPassCount(),
                completeCalibrationData.ProductionFailCount(),
                completeCalibrationData.NonProductionFailCount(),
                completeCalibrationData.DataTable.Rows.Count,
                CompletedCalibrationCount0,
                CompletedCalibrationCount1,
                CompletedCalibrationCount2,
                CompletedCalibrationCount3,
                CompletedCalibrationCount4);

            UpdateCountsLabels(
                completePumpingData.ProductionPassCount(),
                completePumpingData.NonProductionPassCount(),
                completePumpingData.ProductionFailCount(),
                completePumpingData.NonProductionFailCount(),
                completePumpingData.DataTable.Rows.Count,
                CompletePumpingCount0,
                CompletePumpingCount1,
                CompletePumpingCount2,
                CompletePumpingCount3,
                CompletePumpingCount4);

            DatasetTableLayoutPanel.RowStyles[1].Height = completeCalibrationData.DataTable.Rows.Count == 0 ? 5 : 35;
            DatasetTableLayoutPanel.RowStyles[3].Height = completePumpingData.DataTable.Rows.Count == 0 ? 5 : 35;
            DatasetTableLayoutPanel.RowStyles[5].Height = incompleteCalibrationData.DataTable.Rows.Count == 0 ? 5 : 25;
            DatasetTableLayoutPanel.RowStyles[7].Height = incompletePumpingData.DataTable.Rows.Count == 0 ? 5 : 25;
            CompleteCalibrationExcelButton.Visible = completeCalibrationData.DataTable.Rows.Count > 0;
            CompletePumpingExcelButton.Visible = completePumpingData.DataTable.Rows.Count > 0;
            IncompleteCalibrationExcelButton.Visible = incompleteCalibrationData.DataTable.Rows.Count > 0;
            IncompletePumpingExcelButton.Visible = incompletePumpingData.DataTable.Rows.Count > 0;

            // Update charts
            UpdateTrimTrackerCharts();
            UpdatePassRateTable();
            UpdateCLCharts();

            // Populate generic trend and x-y treeviews
            ThreadExt.StartInNewThread(PopulateParameterTreeViews);

            PopulateRigAlignmentPNandLineListView();

            // Populate trend chart
            PopulateTrendChartUTIDList(incompleteCalibrationData.GetUniqueTestIDs(), incompletePumpingData.GetUniqueTestIDs());

            // Set status strip label
            SetStatus("Ready.");
        }

        /// <summary>
        /// Colours the DateTimePicker background yellow whilst being edited.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void dateTimePickers_Enter(object sender, EventArgs e)
        {
            // Make background yellow
            var extendedDateTimePicker = sender as ExtendedDateTimePicker;
            if (extendedDateTimePicker == null) return;

            extendedDateTimePicker.BackColor = Color.Yellow;
        }

        /// <summary>
        /// Colours the DateTimePicker background white when editing complete.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void dateTimePicker_Leave(object sender, EventArgs e)
        {
            // Make background yellow
            var extendedDateTimePicker = sender as ExtendedDateTimePicker;
            if (extendedDateTimePicker == null) return;
            

            extendedDateTimePicker.BackColor = Color.White;
        }

        /// <summary>
        /// Gets the dates from the DateTimePickers and re-populates the form.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void dateTimePicker_CloseUp(object sender, EventArgs e)
        {
            var extendedDateTimePicker = sender as ExtendedDateTimePicker;
            if (extendedDateTimePicker == null) return;

            if (extendedDateTimePicker.SqlValueChanged)
            {
              //dawid  UpdateListDataGridView(UpdateType.UpdateLopList);
                currentDatabase.QueryIasiArchiveStructure = checkBoxUseIasiArchive.Checked;
                currentDatabase.QuerySudburyArchiveStructure = checkBoxUseSudburyArchive.Checked;
            }
            DateTo = dateFromExtDateTimePicker.Value;
            DateFrom = dateToExtDateTimePicker.Value;
           
        }

        /// <summary>
        /// Gets the dates from the DateTimePickers and re-populates the form.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        

        /// <summary>
        /// Gets the dates from the DateTimePickers and re-populates the form.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void dateTimePicker_DateChanged(object sender, EventArgs e)
        {
            var extendedDateTimePicker = sender as ExtendedDateTimePicker;
            if (extendedDateTimePicker == null) return;

            if (extendedDateTimePicker.SqlValueChanged && !extendedDateTimePicker.IsDroppedDown)
            {
                //dawid 25/03
                ///UpdateListDataGridView(UpdateType.UpdateLopList);
            }
        }

        /// <summary>
        /// Called every tenth of a second, decrements the update counter until zero and if now
        /// set to zero, applies the next update.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void queryUpdateTimer_Tick(object sender, EventArgs e)
        {
            if (queryUpdateTimerCounter <= 0) return;

            --queryUpdateTimerCounter;
            if ((queryUpdateTimerCounter > 0) || (queryUpdateType >= UpdateType.UpdateNothing)) return;

            // ReSharper disable once UnusedVariable
            using (var waitCursor = new WaitCursor())
            {
                switch (queryUpdateType)
                {
                    case UpdateType.UpdateLopList:
                        {
                            UpdateLOPListDataGridView();
                        }
                        break;

                    case UpdateType.UpdateTestPlanList:
                        {
                            UpdateTestplanDataGridView();
                        }
                        break;

                    case UpdateType.UpdateTestTypeList:
                        {
                            UpdateTestTypeDataGridView();
                        }
                        break;

                    case UpdateType.UpdatePlantNumberList:
                        {
                            UpdatePlantNumberListDataGridView();
                        }
                        break;

                    case UpdateType.UpdateLineNumberList:
                        {
                            UpdateLineNumberListDataGridView();
                        }
                        break;
                }
            }

            queryUpdateType = UpdateType.UpdateNothing;
        }

        /// <summary>
        /// Sets the next update. If the next update would occur anyway, nothing changes.
        /// </summary>
        /// <param name="updateType">The update type to apply.</param>
        private void UpdateListDataGridView(UpdateType updateType)
        {
            if (updateType < queryUpdateType)
            {
                queryUpdateType = updateType;
            }
            queryUpdateTimerCounter = 10;
        }

        /// <summary>
        /// Generates and runs query to get relevant LOPs and populates LOP DataGridView.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void UpdateLOPListDataGridView()
        {
            var listQuery = new LOPListQuery.Query(currentDatabase, StartDate, EndDate);

            // Set up the data source and display
            var previousSelection = lopListDataGridView.SelectedRows.CellValues(0);

            lopListDataGridView.DataSource = listQuery.GetDataTable();
            lopListDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            lopListDataGridView.SetSelection(previousSelection);

            // Update Testplan list
            UpdateTestplanDataGridView();
        }

        /// <summary>
        /// Captures user LOP selection and runs subsequent methods to re-populate form.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void lopListDataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // Update test plan list
            UpdateListDataGridView(UpdateType.UpdateTestPlanList);
            GetDatabase();
        }

        /// <summary>
        /// Generates and runs query to get relevant test plans and populates test plans DataGridView.
        /// </summary>
        private void UpdateTestplanDataGridView()
        {
            if (lopListDataGridView.SelectedRows.Count > 0)
            {
                var listQuery = new TestPlanListQuery.Query(
                    currentDatabase,
                    StartDate,
                    EndDate,
                    lopListDataGridView.SelectedRows.CellValues("LOP"));

                // Set up the data source and display
                var previousSelection = testplanListDataGridView.SelectedRows.CellValues(0);

                testplanListDataGridView.Enabled = true;
                testplanListDataGridView.DataSource = listQuery.GetDataTable();
                testplanListDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                testplanListDataGridView.SetSelection(previousSelection);

                // Update Test type list
                UpdateTestTypeDataGridView();
            }
            else
            {
                testplanListDataGridView.Enabled = false;
            }
        }

        /// <summary>
        /// Captures user test plan selection and runs subsequent methods to re-populate form.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void testplanListDataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // Update test type list
            UpdateListDataGridView(UpdateType.UpdateTestTypeList);
            GetDatabase();
        }

        /// <summary>
        /// Generates and runs query to get relevant TestTypes and populates TestType DataGridView.
        /// </summary>
        private void UpdateTestTypeDataGridView()
        {
            if (currentDatabase.FtStructure)
            {
                if (testplanListDataGridView.SelectedRows.Count > 0)
                {
                    var listQuery = new TestTypeListQuery.Query(
                        currentDatabase,
                        StartDate,
                        EndDate,
                        lopListDataGridView.SelectedRows.CellValues("LOP"),
                        testplanListDataGridView.SelectedRows.CellValues("TestPlan"));

                    // Set up the data source and display
                    var previousSelection = testTypeListDataGridView.SelectedRows.CellValues(0);

                    testTypeListDataGridView.Enabled = true;
                    testTypeListDataGridView.DataSource = listQuery.GetDataTable();
                    testTypeListDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                    testTypeListDataGridView.SetSelection(previousSelection);

                    // Update plant number list
                    UpdatePlantNumberListDataGridView();
                }
                else
                {
                    testTypeListDataGridView.Enabled = false;
                }
            }
            //// New db structure doesn't have TestType, so jump to UpdatePlantNumberListDataGridView()
            else
            {
                // Disable testtype dgv and update plant number list
                testTypeListDataGridView.ClearSelection();
                testTypeListDataGridView.DataSource = "";
                testTypeListDataGridView.Enabled = false;
                UpdatePlantNumberListDataGridView();
            }
        }

        /// <summary>
        /// Captures user TestType selection and runs subsequent methods to re-populate form.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void testTypeListDataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // Update plant number list
            UpdateListDataGridView(UpdateType.UpdatePlantNumberList);
            GetDatabase();
        }

        /// <summary>
        /// Generates and runs query to get relevant PlantNumbers and populates PlantNumbers DataGridView.
        /// </summary>
        private void UpdatePlantNumberListDataGridView()
        {
            if ((testTypeListDataGridView.SelectedRows.Count > 0) || (!currentDatabase.FtStructure))
            {
                var listQuery = new PlantNumberListQuery.Query(
                    currentDatabase,
                    StartDate,
                    EndDate,
                    lopListDataGridView.SelectedRows.CellValues("LOP"),
                    testplanListDataGridView.SelectedRows.CellValues("TestPlan"),
                    testTypeListDataGridView.SelectedRows.CellValues("TestType"));

                // Set up the data source and display
                var previousSelection = plantNumberListDataGridView.SelectedRows.CellValues(0);

                plantNumberListDataGridView.Enabled = true;
                plantNumberListDataGridView.DataSource = listQuery.GetDataTable();
                plantNumberListDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                plantNumberListDataGridView.SetSelection(previousSelection);

                // Select top row
                if (plantNumberListDataGridView.Rows.Count > 0)
                {
                    plantNumberListDataGridView.Rows[0].Selected = true;
                }

                // Update line number list
                UpdateLineNumberListDataGridView();
            }
            else
            {
                plantNumberListDataGridView.Enabled = false;
            }
        }

        /// <summary>
        /// Captures user PlantNumber selection and runs subsequent methods to re-populate form.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void plantNumberListDataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // Update line number list
            //UpdateListDataGridView(UpdateType.UpdateLineNumberList);
           // GetDatabase();
        }

      
        /// <summary>
        /// Captures user Plant Number Selection and runs subsequent methods to re-populate the form.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void plantNumberListDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            // Update line number list
          //  UpdateListDataGridView(UpdateType.UpdateLineNumberList);
        }

        /// <summary>
        /// Generates and runs query to get relevant LineNumbers and populates LineNumbers DataGridView.
        /// </summary>
        private void UpdateLineNumberListDataGridView()
        {
            if (plantNumberListDataGridView.SelectedRows.Count > 0)
            {
                var listQuery = new LineNumberListQuery.Query(
                    currentDatabase,
                    StartDate,
                    EndDate,
                    lopListDataGridView.SelectedRows.CellValues("LOP"),
                    testplanListDataGridView.SelectedRows.CellValues("TestPlan"),
                    testTypeListDataGridView.SelectedRows.CellValues("TestType"),
                    plantNumberListDataGridView.SelectedRows.CellValues("PlantNumber"));

                // Set up the data source and display
                var previousSelection = lineNumberListDataGridView.SelectedRows.CellValues(0);

                lineNumberListDataGridView.Enabled = true;
                lineNumberListDataGridView.DataSource = listQuery.GetDataTable();
                lineNumberListDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                lineNumberListDataGridView.SetSelection(previousSelection);

                // Select top row
                if (lineNumberListDataGridView.Rows.Count > 0)
                {
                    lineNumberListDataGridView.Rows[0].Selected = true;
                }

                // Build main query
                BuildPivotQuery();
            }
            else
            {
                lineNumberListDataGridView.Enabled = false;
            }
        }

        /// <summary>
        /// Captures user LineNumber selection and runs subsequent methods to re-populate form.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void lineNumberListDataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (lineNumberListDataGridView.SelectedRows.Count > 0)
            {
                BuildPivotQuery();
            }
        }

        /// <summary>
        /// Enables/disables TextBox to input specific ISNs.
        /// Disables TextBox to input specific UTIDs if enabled.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void specificISNsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (SilentUIUpdate)
                return; //Silent Update - ignore.

            if (specificISNsCheckBox.Checked)
            {
                specificISNsTextBox.Enabled = true;
                specificutidsCheckBox.Checked = false;
            }
            else
            {
                specificISNsTextBox.Enabled = false;
            }
            BuildPivotQuery();
        }

        /// <summary>
        /// Rebuilds main queries based on new ISNs entered by user.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void specificISNsTextBox_TextChanged(object sender, EventArgs e)
        {
            if (SilentUIUpdate)
                return; //Silent Update - ignore.

            BuildPivotQuery();
        }

        /// <summary>
        /// Enables/disables TextBox to input specific UTIDs.
        /// Disables TextBox to input specific ISNs if enabled.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void specificutidsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (specificutidsCheckBox.Checked)
            {
                specificutidsTextBox.Enabled = true;
                specificISNsCheckBox.Checked = false;
            }
            else
            {
                specificutidsTextBox.Enabled = false;
            }
            BuildPivotQuery();
        }

        /// <summary>
        /// Rebuilds main queries based on new UTIDs entered by user.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void specificutidsTextBox_TextChanged(object sender, EventArgs e)
        {
            BuildPivotQuery();
        }

        

        /// <summary>
        /// Rebuilds main queries allowing for virgin components being checked.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void VirginsNCVCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            BuildPivotQuery();
        }

        /// <summary>
        /// Rebuilds main queries allowing for virgin components being checked.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void VirginsNozzleCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            BuildPivotQuery();
        }

        /// <summary>
        /// Rebuilds main queries allowing for virgin components being checked.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void VirginsPGCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            BuildPivotQuery();
        }

        /// <summary>
        /// Captures selection change for type of tests (All tests, 1st time, latest) selected
        /// and rebuilds main queries.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void testsForEachISNRadioButtons_CheckedChanged(object sender, EventArgs e)
        {
            BuildPivotQuery();
        }

        /// <summary>
        /// Captures selection change for results of tests (All tests, passes only, fails only) selected
        /// and rebuilds main queries.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void passesAndFailsRadioButtons_CheckedChanged(object sender, EventArgs e)
        {
            BuildPivotQuery();
        }

        /// <summary>
        /// Captures selection change for allowing forced trim results to appear in data to be returned
        /// and rebuilds main queries.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void allowForcedTrimCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            BuildPivotQuery();
        }

        /// <summary>
        /// Captures selection change for allowing incomplete tests in complete test queries (and hence DataGridViews) 
        /// and rebuilds main queries.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void allowIncompleteTestsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            BuildPivotQuery();
        }

        /// <summary>
        /// Called when the numeric ISN check box changes state. Rebuilds the main queries.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void NumericISNCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            BuildPivotQuery();
        }

        /// <summary>
        /// Called when a virtual data grid view column header is clicked to sort the column.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event arguments.</param>
        private void VirtualDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var dataGridView = sender as VirtualDataGridView.VirtualDataGridView;
            if (dataGridView == null) return;

            dataGridView.SortOnColumn(e.ColumnIndex);
        }

        /// <summary>
        /// Updates the count labels with counts greater than zero and colours appropriately.
        /// </summary>
        /// <param name="productionPass">The production pass count.</param>
        /// <param name="otherPass">The non production pass count.</param>
        /// <param name="productionFail">The production fail count.</param>
        /// <param name="otherFail">The non production fail count.</param>
        /// <param name="total">The total test count.</param>
        /// <param name="countsLabel0">The first label control.</param>
        /// <param name="countsLabel1">The second label control.</param>
        /// <param name="countsLabel2">The third label control.</param>
        /// <param name="countsLabel3">The fourth label control.</param>
        /// <param name="countsLabel4">The fifth label control.</param>
        private void UpdateCountsLabels(int productionPass, int otherPass, int productionFail, int otherFail, int total, Label countsLabel0, Label countsLabel1, Label countsLabel2, Label countsLabel3, Label countsLabel4)
        {
            var counts = new[] { otherFail, productionFail, otherPass, productionPass, total };
            var colours = new[] { Color.LightGray, Color.LightCoral, Color.LightBlue, Color.PaleGreen, SystemColors.Control };
            var labels = new[] { countsLabel0, countsLabel1, countsLabel2, countsLabel3, countsLabel4 };
            var sort = new[] { "", "Global_gr desc", "", "Global_gr asc", "UTID desc" };

            var labelIndex = 0;
            for (var index = 0; index < 5; ++index)
            {
                if (counts[index] > 0)
                {
                    var label = labels[labelIndex];
                    ++labelIndex;

                    label.BackColor = colours[index];
                    label.Text = counts[index].ToString(CultureInfo.InvariantCulture);
                    label.Tag = sort[index];
                    label.Visible = true;
                }
            }

            while (labelIndex < 5)
            {
                labels[labelIndex].Visible = false;
                ++labelIndex;
            }
        }

        /// <summary>
        /// Called when a counts label is clicked. Applies the sort string associated with
        /// the label to the completed calibration grid view.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void CompletedCalibrationCounts_Click(object sender, EventArgs e)
        {
            var label = sender as Label;
            if (label == null) return;

            var tag = label.Tag as string;
            if (tag == null) return;

            completedCalibrationDataGridView.Sort(tag);
        }

        /// <summary>
        /// Called when a counts label is clicked. Applies the sort string associated with
        /// the label to the completed pumping grid view.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void CompletedPumpingCounts_Click(object sender, EventArgs e)
        {
            var label = sender as Label;
            if (label != null)
            {
                if (label.Tag is string)
                {
                    completedPumpingDataGridView.Sort(label.Tag as string);
                }
            }
        }

        /// <summary>
        /// Clears existing data from DataGridViews, charts, lists etc.
        /// </summary>
        private void ClearPreviousQueryData()
        {
            // Clear trimtracker charts
            ChartExt.ClearCharts(
                trimTrackerMDPChart,
                trimTrackerP1Chart,
                trimTrackerP2Chart,
                trimTrackerP3Chart,
                trimTrackerP4Chart,
                trimTrackerP5Chart,
                trimTrackerODChart,
                trimTrackerCDChart,
                trimTrackerLeakdownChart);

            // Clear pumping tracker charts
            ChartExt.ClearCharts(pumpingTrackerOMVT3FixedPressureChart, pumpingTrackerOMVT3FixedAngleChart, pumpingTrackerPressureFixedAngleChart, pumpingTrackerAngleFixedPressureChart);

            // Clear CLT tracker charts
            ChartExt.ClearCharts(cltP1TrackerChart, cltP2TrackerChart, cltP3TrackerChart, cltP4TrackerChart, cltP5TrackerChart);

            // Clear pass rate table and failure pareto chart
            passRateGridView.Rows.Clear();
            overallPassRateGridView.Rows.Clear();
            failureParetoChart.Series.Clear();

            // Clear trimtracker data grids
            completedCalibrationDataGridView.DataSource = null;
            incompleteCalibrationDataGridView.DataSource = null;
            completedPumpingDataGridView.DataSource = null;
            incompletePumpingDataGridView.DataSource = null;
            failureFullDataGridView.DataSource = null;

            // Dispose of datatables and set new so that pass rate table will calculate based on latest query (can keep old data otherwise)
            completeCalibrationData.DataTable.Dispose();
            incompleteCalibrationData.DataTable.Dispose();
            completePumpingData.DataTable.Dispose();
            incompletePumpingData.DataTable.Dispose();

            completeCalibrationData = new CompleteCalibrationDataTable();
            incompleteCalibrationData = new IncompleteCalibrationDataTable();
            completePumpingData = new CompletePumpingDataTable();
            incompletePumpingData = new IncompletePumpingDataTable();

            // Clear detailed list views & data grids
            ClearPreviousDetailedData();

            // Clear chart trace
            ClearPreviousChartTraceData();

            // Clear rig alignment, generic trend and XY data
            ClearPreviousRigAlignmentAndTrendAndXYData();

            // Clear trend chart
            ClearPreviousTrendChartData();
        }

        ///////////////////////////////////////////////////////////////////////
        // Update tracking charts
        ///////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Creates a new chart series.
        /// </summary>
        /// <param name="name">The name of the series.</param>
        /// <param name="index">An index used to set chart colours.</param>
        /// <returns>The created chart series.</returns>
        private Series CreateSeries(string name, int index)
        {
            return new Series
            {
                Name = name,
                ChartType = SeriesChartType.Line,
                MarkerStyle = MarkerStyle.Circle,
                MarkerSize = 4,
                MarkerColor = SeriesColour.BetterColours(index),
                Color = SeriesColour.BetterColours(index)
            };
        }

        /// <summary>
        /// Add series data from the rows, taking the X value from the xFieldName and the y value from the yFieldName
        /// </summary>
        /// <param name="series">The series to add data too.</param>
        /// <param name="rows">The rows of data to add to the series.</param>
        /// <param name="xFieldName">The name of the field used for the x axis value.</param>
        /// <param name="yFieldName">The name of the field used for the y axis value.</param>
        /// <param name="plotChronological">Flag indicating if the data should be plot chronologically or not.</param>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation",
                         Justification = "Variables xFieldName and yFieldName are X and Y axis specific.")]
        private void AddSeriesData(Series series, DataRowCollection rows, string xFieldName, string yFieldName, bool plotChronological)
        {
            var index = 0;
            foreach (DataRow row in rows)
            {
                if (plotChronological)
                {
                    series.Points.AddXY(index, row[yFieldName]);
                }
                else
                {
                    series.Points.AddXY(row[xFieldName], row[yFieldName]);
                }
                ++index;
            }
        }

        /// <summary>
        /// Add series data from the rows, taking the y value from the yFieldName
        /// </summary>
        /// <param name="series">The series to add data to.</param>
        /// <param name="rows">The rows of data to add to the series.</param>
        /// <param name="xValue">The x axis value.</param>
        /// <param name="yFieldName">The name of the field used for the y axis value.</param>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation",
                         Justification = "Variables xValue and yFieldName are X and Y axis specific.")]
        private void AddSeriesData(Series series, DataRowCollection rows, int xValue, string yFieldName)
        {
            foreach (DataRow row in rows)
            {
                series.Points.AddXY(xValue, row[yFieldName]);
            }
        }

        /// <summary>
        /// Adds a new series to a chart.
        /// </summary>
        /// <param name="seriesName">The name of the series to add.</param>
        /// <param name="colourIndex">An index used to set the colour of the series.</param>
        /// <param name="table">The table used to get the data for the series.</param>
        /// <param name="xFieldName">The name of the field used to get the x axis data.</param>
        /// <param name="yFieldName">The name of the field used to get the y axis data.</param>
        /// <param name="chart">The chart to add the series to.</param>
        /// <param name="plotChronological">Flag indicating if the data should be plot chronologically or not.</param>
        /// <param name="plotGrades">Flag indicating if grades are being plotted.</param>
        /// <returns>True if the series was added to the chart, false otherwise.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation",
                         Justification = "Variables xFieldName and yFieldName are X and Y axis specific.")]
        private bool AddSeriesToChart(string seriesName, int colourIndex, DataTable table, string xFieldName, string yFieldName, Chart chart, bool plotChronological, bool plotGrades)
        {
            // Check to see if the table contains the named field
            if (!table.Columns.Contains(xFieldName) || !table.Columns.Contains(yFieldName)) return false;

            var gradesPostfix = plotGrades ? "_gr" : "";

            if (!table.Columns.Contains(xFieldName) || !table.Columns.Contains(yFieldName + gradesPostfix)) return true;

            var series = CreateSeries(seriesName, colourIndex);
            AddSeriesData(series, table.Rows, xFieldName, yFieldName + gradesPostfix, plotChronological);

            chart.InvokeIfRequired(c =>
            {
                chart.ChartAreas.Clear();
                chart.ChartAreas.Add("");

                // setup axes
                ChartAxisScaling.AxisScaler.ScaleAxis(c.ChartAreas[0], c.Name + gradesPostfix, seriesName);

                // Add new series to chart
                c.Series.Add(series);

                if (!plotChronological) return;

                chart.ChartAreas[0].AxisX.Minimum = 0;
                chart.ChartAreas[0].AxisX.Maximum = series.Points.Count;
            });

            return true;
        }

        /// <summary>
        /// Adds a new series to a chart.
        /// </summary>
        /// <param name="seriesName">The name of the series to add.</param>
        /// <param name="colourIndex">An index used to set the colour of the series.</param>
        /// <param name="table">The table used to get the data for the series.</param>
        /// <param name="xFieldName">The name of the field used to get the x axis data.</param>
        /// <param name="chart">The chart to add the series to.</param>
        /// <param name="plotChronological">Flag indicating if the data should be plot chronologically or not.</param>
        /// <param name="plotGrades">Flag indicating if grades are being plotted.</param>
        /// <returns>True if the series was added to the chart, false otherwise.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation",
                         Justification = "Variable xFieldName is X axis specific.")]
        private bool AddSeriesToChart(string seriesName, int colourIndex, DataTable table, string xFieldName, Chart chart, bool plotChronological, bool plotGrades)
        {
            return AddSeriesToChart(seriesName, colourIndex, table, xFieldName, seriesName, chart, plotChronological, plotGrades);
        }

        /// <summary>
        /// Configures a chart by recalculating the axis scales.
        /// </summary>
        /// <param name="chart">The chart to configure.</param>
        /// <param name="showXAxisLabel">Flag indicating if the X axis label should be shown.</param>
        /// <param name="userSelectionEnable">The user selection enabled flag.</param>
        private void ConfigureChart(Chart chart, bool showXAxisLabel, bool userSelectionEnable = false)
        {
            chart.InvokeIfRequired(c =>
            {
                c.ChartAreas[0].CursorX.IsUserSelectionEnabled = userSelectionEnable;
                c.ChartAreas[0].CursorY.IsUserSelectionEnabled = userSelectionEnable;
                c.ChartAreas[0].AxisX.LabelStyle.Enabled = showXAxisLabel;
            });
        }

        /// <summary>
        /// Clears all the trim tracker charts.
        /// </summary>
        private void ClearTrimTrackerCharts()
        {
            // Clear trimtracker charts
            ChartExt.ClearCharts(
                trimTrackerMDPChart,
                trimTrackerP1Chart,
                trimTrackerP2Chart,
                trimTrackerP3Chart,
                trimTrackerP4Chart,
                trimTrackerP5Chart,
                trimTrackerODChart,
                trimTrackerCDChart,
                trimTrackerLeakdownChart);
        }

        /// <summary>
        /// Updates all the trim tracker charts.
        /// </summary>
        private void UpdateTrimTrackerCharts()
        {
            if (currentDatabase.ProductType == DatabaseProductType.MDEG)
            {
                UpdateMDPChartAlternative();
                UpdateQTrimChartsAlternative();
            }
            else
            {
                UpdateMDPChart();
                UpdateQTrimCharts();
            }

            UpdateODCDCharts();
            UpdateLeakageCharts();
            UpdatePumpingCharts();
        }

        /// <summary>
        /// Event handler called when the trim tracker chart type radio buttons are checked.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void trimTrackerPlotRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            ClearTrimTrackerCharts();
            UpdateTrimTrackerCharts();
        }

        /// <summary>
        /// Updates MDP chart on "Trim Tracker Charts" tab.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void UpdateMDPChart()
        {
            // Update single point passoff criteria charts
            // MDP chart first          
            for (var index = 0; index < 5; index++)
            {
                var seriesName = string.Format("p{0}", index + 1);
                var fieldName = "MDP" + seriesName;

                AddSeriesToChart(seriesName, index, completeCalibrationData.DataTable, "PDate", fieldName, trimTrackerMDPChart, trimTrackerPlotChronologicalRadioButton.Checked, trimTrackerPlotGradesCheckBox.Checked);
            }

            ConfigureChart(trimTrackerMDPChart, !trimTrackerPlotChronologicalRadioButton.Checked);
        }

        /// <summary>
        /// Updates MDP chart on "Trim Tracker Charts" tab.
        /// //TEBBO! Alt MDP on MDEG doesn't have pressure, and pressure other way up, 1 or other or neither might exist, not both.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void UpdateMDPChartAlternative()
        {
            // Update single point passoff criteria charts
            // MDP chart first          
            for (var index = 0; index < 5; index++)
            {
                var seriesName = string.Format("{0}", index + 1);
                var fieldName = "MDP" + seriesName;

                AddSeriesToChart(seriesName, index, completeCalibrationData.DataTable, "PDate", fieldName, trimTrackerMDPChart, trimTrackerPlotChronologicalRadioButton.Checked, trimTrackerPlotGradesCheckBox.Checked);
            }

            ConfigureChart(trimTrackerMDPChart, !trimTrackerPlotChronologicalRadioButton.Checked);
        }

        /// <summary>
        /// Updates Q-trim charts on "Trim Tracker Charts" tab.
        /// </summary>
        private void UpdateQTrimCharts()
        {
            // Set an array for charts to reduce repetition when adding data to q-trim plots
            var charts = new[] { trimTrackerP1Chart, trimTrackerP2Chart, trimTrackerP3Chart, trimTrackerP4Chart, trimTrackerP5Chart };

            if (trimTrackerMDPChart.Series.Count > 0)
            {
                foreach (var chart in charts)
                {
                    chart.Series.Clear();
                }
            }

            // Loop for 6 pressure levels
            for (var pressureLevelIndex = 0; pressureLevelIndex < charts.Length; pressureLevelIndex++)
            {
                // Loop for 3 Q-trims per pressure level
                for (var trimIndex = 0; trimIndex < 3; trimIndex++)
                {
                    var seriesName = string.Format("Q{0}", trimIndex + 1);
                    var fieldName = string.Format("{0}p{1}", seriesName, pressureLevelIndex + 1);

                    AddSeriesToChart(seriesName, trimIndex, completeCalibrationData.DataTable, "PDate", fieldName, charts[pressureLevelIndex], trimTrackerPlotChronologicalRadioButton.Checked, trimTrackerPlotGradesCheckBox.Checked);
                }

                ConfigureChart(charts[pressureLevelIndex], !trimTrackerPlotChronologicalRadioButton.Checked, true);
            }
        }

        /// <summary>
        /// TEBBO! Alternative MDEG Q trims don't contain pressure levels. OK to run both methods as shouldn't have both MDEG notation and HITH in same query results! 
        /// Updates Q-trim charts on "Trim Tracker Charts" tab.
        /// </summary>
        private void UpdateQTrimChartsAlternative()
        {
            // Set an array for charts to reduce repetition when adding data to q-trim plots
            var charts = new[] { trimTrackerP1Chart, trimTrackerP2Chart, trimTrackerP3Chart, trimTrackerP4Chart, trimTrackerP5Chart };

            if (trimTrackerMDPChart.Series.Count > 0)
            {
                for (var index = 0; index < 5; index++)
                {
                    charts[index].Series.Clear();
                }
            }

            for (var pressureLevelIndex = 0; pressureLevelIndex < charts.Length; pressureLevelIndex++)
            {
                var seriesName = string.Format("Q{0}", pressureLevelIndex + 1);
                var fieldName = string.Concat("Q" + (pressureLevelIndex + 1), trimTrackerPlotGradesCheckBox.Checked ? "_gr" : "");

                // Check to see if column exists first
                if (completeCalibrationData.DataTable.Columns.Contains(fieldName))
                {
                    // If option to plot by grade checked, look at grade fields on data grid
                    AddSeriesToChart(seriesName, pressureLevelIndex, completeCalibrationData.DataTable, "PDate", seriesName, charts[pressureLevelIndex], trimTrackerPlotChronologicalRadioButton.Checked, trimTrackerPlotGradesCheckBox.Checked);
                }

                ConfigureChart(charts[pressureLevelIndex], !trimTrackerPlotChronologicalRadioButton.Checked, true);
            }
        }

        /// <summary>
        /// Updates OD and CD charts on "Trim Tracker Charts" tab.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void UpdateODCDCharts()
        {
            // Update single point passoff criteria charts
            // Opening delay chart first          
            for (var index = 0; index < 5; index++)
            {
                var seriesName = string.Format("p{0}", index + 1);
                var fieldName = string.Format("ODp{0}", index + 1);

                // If option to plot by grade checked, look at grade fields on data grid
                AddSeriesToChart(seriesName, index, completeCalibrationData.DataTable, "PDate", fieldName, trimTrackerODChart, trimTrackerPlotChronologicalRadioButton.Checked, trimTrackerPlotGradesCheckBox.Checked);
            }

            ConfigureChart(trimTrackerODChart, !trimTrackerPlotChronologicalRadioButton.Checked, true);

            // Now Closing delay chart       
            for (var index = 0; index < 5; index++)
            {
                var seriesName = string.Format("p{0}", index + 1);
                var fieldName = string.Format("CDp{0}", index + 1);

                // If option to plot by grade checked, look at grade fields on data grid
                AddSeriesToChart(seriesName, index, completeCalibrationData.DataTable, "PDate", fieldName, trimTrackerCDChart, trimTrackerPlotChronologicalRadioButton.Checked, trimTrackerPlotGradesCheckBox.Checked);
            }

            ConfigureChart(trimTrackerCDChart, !trimTrackerPlotChronologicalRadioButton.Checked, true);
        }

        /// <summary>
        /// Updates leakage chart on "Trim Tracker Charts" tab.
        /// </summary>
        private void UpdateLeakageCharts()
        {
            AddSeriesToChart("DeltaPressure", 0, completeCalibrationData.DataTable, "PDate", trimTrackerLeakdownChart, trimTrackerPlotChronologicalRadioButton.Checked, false);
            AddSeriesToChart("SORtimep1_gr", 0, completeCalibrationData.DataTable, "PDate", trimTrackerLeakdownChart, trimTrackerPlotChronologicalRadioButton.Checked, false);

            ConfigureChart(trimTrackerLeakdownChart, !trimTrackerPlotChronologicalRadioButton.Checked, true);
        }

        /// <summary>
        /// Updates a pumping chart on "Pumping Tracker Charts" tab.
        /// </summary>
        /// <param name="seriesName">The name of the series to add.</param>
        /// <param name="chart">The chart to add the series too.</param>
        private void UpdatePumpingChart(string seriesName, Chart chart)
        {
            if (AddSeriesToChart(seriesName, 0, completePumpingData.DataTable, "PDate", chart, trimTrackerPlotChronologicalRadioButton.Checked, false))
            {
                ConfigureChart(chart, true, true);
            }
        }

        /// <summary>
        /// Updates Pumping charts on "Pumping Tracker Charts" tab.
        /// </summary>
        private void UpdatePumpingCharts()
        {
            UpdatePumpingChart("OMVT3aMax_gr", pumpingTrackerOMVT3FixedPressureChart);
            UpdatePumpingChart("OMVT3aFOD22_gr", pumpingTrackerOMVT3FixedAngleChart);
            UpdatePumpingChart("FOCSOutputTMax_gr", pumpingTrackerAngleFixedPressureChart);
            UpdatePumpingChart("PressureFOD22_gr", pumpingTrackerPressureFixedAngleChart);
        }

        /// <summary>
        /// Adds a series to a close loop chart.
        /// </summary>
        /// <param name="chart">The closed loop chart.</param>
        /// <param name="columnName">Name of the column in the rows to get the y values from.</param>
        /// <param name="prefix">Prefix to the displayed name of the series.</param>
        /// <param name="index">Index used to determine the series colour.</param>
        /// <param name="rows">Row of data to create the series from.</param>
        /// <param name="seriesNameEx">Postfix to the series name</param>
        private void AddClosedLoopChart(Chart chart, string columnName, string prefix, int index, DataRowCollection rows, string seriesNameEx = "")
        {
            var expectedPrefix = string.Format("{0}{1}f", prefix, index + 1);

            if (!columnName.StartsWith(expectedPrefix)) return;

            var seriesName = columnName.Substring(3) + seriesNameEx;
            var series = CreateSeries(seriesName, index);

            var nameIntOffset = prefix.Length + 2;
            var value = int.Parse(columnName.Substring(nameIntOffset, columnName.Length - nameIntOffset - 3));

            AddSeriesData(series, rows, value, columnName);

            // Add new series to chart & put in correct chart area
            chart.InvokeIfRequired(c => c.Series.Add(series));
        }

        /// <summary>
        /// Updates closed loop charts on "CLT Tracker" tab.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void UpdateCLCharts()
        {
            // Set an array for charts to reduce repetition when adding data to q-trim plots
            var charts = new[] { cltP1TrackerChart, cltP2TrackerChart, cltP3TrackerChart, cltP4TrackerChart, cltP5TrackerChart };

            for (var index = 0; index < charts.Length; index++)
            {
                // Set scales & grids
                charts[index].InvokeIfRequired(c => ChartAxisScaling.AxisScaler.ScaleAxis(c.ChartAreas[0], c.Name));

                // Find all columns relevant to current pressure level
                foreach (DataColumn column in completeCalibrationData.DataTable.Columns)
                {
                    // Old DB, with names like CLTrp and CLTep
                    if (currentDatabase.FtStructure)
                    {
                        if (cltTrackerShowRealCheckBox.Checked)
                        {
                            AddClosedLoopChart(charts[index], column.ColumnName, "CLTrp", index, completeCalibrationData.DataTable.Rows);
                        }

                        if (cltTrackerShowExpectedCheckBox.Checked)
                        {
                            AddClosedLoopChart(charts[index], column.ColumnName, "CLTep", index, completeCalibrationData.DataTable.Rows);
                        }
                    }
                    //// New DB, with names like CLrp and CLep
                    else
                    {
                        if (cltTrackerShowRealCheckBox.Checked)
                        {
                            AddClosedLoopChart(charts[index], column.ColumnName, "CLrp", index, completeCalibrationData.DataTable.Rows);
                        }

                        if (cltTrackerShowExpectedCheckBox.Checked)
                        {
                            AddClosedLoopChart(charts[index], column.ColumnName, "CLep", index, completeCalibrationData.DataTable.Rows);
                        }
                    }
                }

                const string PumpCheckPostFix = "_pumpcheck";

                // Find all columns in pumping tests relevant to current pressure level
                foreach (DataColumn column in completePumpingData.DataTable.Columns)
                {
                    // Old DB, with names like CLTrp and CLTep
                    if (currentDatabase.FtStructure)
                    {
                        if (cltTrackerShowRealCheckBox.Checked)
                        {
                            AddClosedLoopChart(charts[index], column.ColumnName, "CLTrp", index, completePumpingData.DataTable.Rows, PumpCheckPostFix);
                        }

                        if (cltTrackerShowExpectedCheckBox.Checked)
                        {
                            AddClosedLoopChart(charts[index], column.ColumnName, "CLTep", index, completePumpingData.DataTable.Rows, PumpCheckPostFix);
                        }
                    }
                    //// New DB, with names like CLrp and CLep
                    else
                    {
                        if (cltTrackerShowRealCheckBox.Checked)
                        {
                            AddClosedLoopChart(charts[index], column.ColumnName, "CLrp", index, completePumpingData.DataTable.Rows, PumpCheckPostFix);
                        }

                        if (cltTrackerShowExpectedCheckBox.Checked)
                        {
                            AddClosedLoopChart(charts[index], column.ColumnName, "CLep", index, completePumpingData.DataTable.Rows, PumpCheckPostFix);
                        }
                    }
                }

                ConfigureChart(charts[index], true);
            }
        }

        /// <summary>
        /// Returns a list of date times which are the split times, including the start and 
        /// end date time between the date from and date to date times.
        /// </summary>
        /// <returns>A List of split date times.</returns>
        private List<DateTime> GetTimePeriodSplitDateTimes()
        {
            if (SplitResultsDaily.Checked)
            {
                return DateTimeExt.GetAllDateTimes(
                    dateFromExtDateTimePicker.Value.Date,
                    dateToExtDateTimePicker.Value.Date.AddDays(1),
                    new List<TimeSpan> { new TimeSpan() });
            }
            if (SplitResults6Till6.Checked)
            {
                return DateTimeExt.GetAllDateTimes(
                    dateFromExtDateTimePicker.Value.Date,
                    dateToExtDateTimePicker.Value.Date.AddHours(12),
                    new List<TimeSpan> { new TimeSpan(6, 0, 0), new TimeSpan(18, 0, 0) });
            }
            if (SplitResults10Till10.Checked)
            {
                return DateTimeExt.GetAllDateTimes(
                    dateFromExtDateTimePicker.Value.Date,
                    dateToExtDateTimePicker.Value.Date.AddHours(12),
                    new List<TimeSpan> { new TimeSpan(10, 0, 0), new TimeSpan(22, 0, 0) });
            }
            if (SplitResultsByShift.Checked)
            {
                return DateTimeExt.GetAllDateTimes(
                    dateFromExtDateTimePicker.Value.Date,
                    dateToExtDateTimePicker.Value.Date.AddHours(8),
                    new List<TimeSpan> { new TimeSpan(6, 0, 0), new TimeSpan(14, 0, 0), new TimeSpan(22, 0, 0) });
            }

            return new List<DateTime>();
        }

        /// <summary>
        /// Called when a failure analysis parameter is changed. Updates the data grids.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void FailureAnalysis_ParametersChanged(object sender, EventArgs e)
        {
            passRateGridView.Rows.Clear();
            overallPassRateGridView.Rows.Clear();

            UpdatePassRateTable();
        }

        /// <summary>
        /// Returns all the failures as the date time and plant number, indexed by OEE Flag.
        /// </summary>
        /// <returns>The failures as the date time and plant number, indexed by OEE Flag.</returns>
        private Dictionary<bool, List<Tuple<DateTime, short>>> GetFailures()
        {
            var testResults = completeCalibrationData.FailCount() + incompleteCalibrationDataGridView.Rows.Count +
                              completePumpingData.FailCount() + incompletePumpingData.DataTable.Rows.Count;
            var utidPlantNumberList = new string[testResults, 2];

            // Set count of no of fails in list variable to 0
            var listLength = 0;

            // Create list of utids and Plantnumbers and put into list
            foreach (var row in completeCalibrationData.FailRows())
            {
                utidPlantNumberList[listLength, 0] = row["UTID"].ToString();
                utidPlantNumberList[listLength, 1] = row["PlantNumber"].ToString();
                listLength++;
            }
            foreach (var row in completePumpingData.FailRows())
            {
                utidPlantNumberList[listLength, 0] = row["UTID"].ToString();
                utidPlantNumberList[listLength, 1] = row["PlantNumber"].ToString();
                listLength++;
            }

            // Also add EarlyEndFails - always fails, so no need for if statement
            foreach (DataRow row in incompleteCalibrationData.DataTable.Rows)
            {
                utidPlantNumberList[listLength, 0] = row["UTID"].ToString();
                utidPlantNumberList[listLength, 1] = row["PlantNumber"].ToString();
                listLength++;
            }
            foreach (DataRow row in incompletePumpingData.DataTable.Rows)
            {
                utidPlantNumberList[listLength, 0] = row["UTID"].ToString();
                utidPlantNumberList[listLength, 1] = row["PlantNumber"].ToString();
                listLength++;
            }

            var res = new Dictionary<bool, List<Tuple<DateTime, short>>>();
            res[true] = new List<Tuple<DateTime, short>>();
            res[false] = new List<Tuple<DateTime, short>>();

            if (!currentDatabase.FtStructure)
            {
                // Tempoarary - this gives us a temporary aggregation of all the fails returned by the Inital query
                // All are OEE negative, becuase i cannot determine this at this point in time without using FTypeRework
                //  This takes out the OEE Filter - but gives a Pass rate which may be a good enough indicator at the moment.
                foreach (var row in completeCalibrationData.FailRows())
                    res[false].Add(new Tuple<DateTime, short>((DateTime)row["Pdate"], (short)row["PlantNumber"]));
                foreach (var row in completePumpingData.FailRows())
                    res[false].Add(new Tuple<DateTime, short>((DateTime)row["Pdate"], (short)row["PlantNumber"]));
            }
            else
            {
                // Send required variables (fails only)
                var newRework = new FTypeRework.FTypeRework(
                    utidPlantNumberList,
                    dateFromExtDateTimePicker.DateTimeString,
                    dateToExtDateTimePicker.DateTimeString,
                    currentDatabase.ConnectionString);

                try
                {
                    // Return datatable of analysed results
                    foreach (var row in newRework.ReturnReworkDataTable().AsEnumerable())
                    {
                        res[row["OEEFlag"].ToString() == "True"].Add(new Tuple<DateTime, short>((DateTime)row["Pdate"], (short)row["PlantNumber"]));
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
            return res;
        }

        /// <summary>
        /// Updates Pass rate summary table for calibration tests on "Failure Analysis" tab.
        /// </summary>
        private void UpdatePassRateTable()
        {
            {
                using (new WaitCursor())
                {
                    passRateGridView.Rows.Clear();

                    var timePeriods = GetTimePeriodSplitDateTimes();

                    if (timePeriods.Count <= 1) return;

                    List<short> plantNumbers;
                    if (SplitResultsByRig.Checked)
                    {
                        plantNumbers = completeCalibrationData.PlantNumbers();
                    }
                    else
                    {
                        plantNumbers = new List<short> { 0 };
                    }

                    int totalPasses = 0, totalFails = 0, totalIncompletes = 0;
                    var failures = GetFailures();

                    foreach (var plantNumber in plantNumbers)
                    {
                        var plantNumberHeading = plantNumber.ToString(CultureInfo.InvariantCulture);
                        var addPlantNumberHeading = SplitResultsByRig.Checked;

                        var start = new DateTime();
                        foreach (var end in timePeriods)
                        {
                            if (start.Ticks > 0)
                            {
                                int fails, passes, incomplete = 0;

                                // Get total passes and fails from completed tests & total incomplete tests, for each day
                                if (SplitResultsByRig.Checked)
                                {
                                    if (filterAllRadioButton.Checked)
                                    {
                                        fails = failures[true].Count(x => x.Item2 == plantNumber && x.Item1 >= start && x.Item1 < end) +
                                                failures[false].Count(x => x.Item2 == plantNumber && x.Item1 >= start && x.Item1 < end);
                                    }
                                    else
                                    {
                                        fails = failures[filterOEEFailsRadioButton.Checked].Count(x => x.Item2 == plantNumber && x.Item1 >= start && x.Item1 < end);
                                    }

                                    passes = completeCalibrationData.PassCount(start, end, plantNumber);
                                    if (!IgnoreIncompleteResults.Checked)
                                    {
                                        incomplete = incompleteCalibrationData.Count(start, end, plantNumber);
                                    }
                                }
                                else
                                {
                                    if (filterAllRadioButton.Checked)
                                    {
                                        fails = failures[true].Count(x => x.Item1 >= start && x.Item1 < end) +
                                                failures[false].Count(x => x.Item1 >= start && x.Item1 < end);
                                    }
                                    else
                                    {
                                        fails = failures[filterOEEFailsRadioButton.Checked].Count(x => x.Item1 >= start && x.Item1 < end);
                                    }

                                    passes = completeCalibrationData.PassCount(start, end) + completePumpingData.PassCount(start, end, plantNumber);
                                    if (!IgnoreIncompleteResults.Checked)
                                    {
                                        incomplete = incompleteCalibrationData.Count(start, end) + incompletePumpingData.Count(start, end, plantNumber);
                                    }
                                }

                                var total = fails + passes + incomplete;
                                var passRate = 100 * passes / (float)total;

                                // Add row to pass rate table if any tests have been completed on the given day
                                if (total > 0)
                                {
                                    DateTime startCopy = start;
                                    passRateGridView.InvokeIfRequired(c =>
                                    {
                                        if (addPlantNumberHeading)
                                        {
                                            // ReSharper disable once PossiblyMistakenUseOfParamsMethod
                                            c.Rows.Add(plantNumberHeading);
                                            addPlantNumberHeading = false;
                                        }
                                        c.Rows.Add(startCopy, passes, fails, incomplete, total, passRate);
                                    });
                                }

                                totalPasses += passes;
                                totalFails += fails;
                                totalIncompletes += incomplete;
                            }

                            start = end;
                        }
                    }
                    overallPassRateGridView.Rows.Clear();
                    var totalTotal = totalPasses + totalFails + totalIncompletes;
                    var totalPassRate = 100 * totalPasses / (float)totalTotal;
                    overallPassRateGridView.Rows.Add(totalPasses, totalFails, totalIncompletes, totalTotal, totalPassRate);

                    // Format columns
                    // ReSharper disable PossibleNullReferenceException
                    if (SplitResultsDaily.Checked)
                    {
                        passRateGridView.Columns["calDay"].DefaultCellStyle.Format = "dd-MMM-yyyy";
                    }
                    else
                    {
                        passRateGridView.Columns["calDay"].DefaultCellStyle.Format = "dd-MMM-yyyy HH:mm";
                    }
                    passRateGridView.Columns["calPassRate"].DefaultCellStyle.Format = "0.00";
                }
            }
        }

        /// <summary>
        /// Event handler called when one of the failure filter radio buttons changes state.
        /// </summary>
        private void FailureFilterRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            var button = sender as RadioButton;
            if (button == null) return;
            if (!button.Checked) return;

            UpdatePassRateTable();
        }

        /// <summary>
        /// Captures plot by grades checkbox selection change, clears and updates charts on "Trim Tracker Charts" tab.
        /// </summary>
        private void trimTrackerPlotGradesCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            // Clear existing charts and re-populate
            ChartExt.ClearCharts(trimTrackerMDPChart, trimTrackerP1Chart, trimTrackerP2Chart, trimTrackerP3Chart, trimTrackerP4Chart, trimTrackerP5Chart, trimTrackerODChart, trimTrackerCDChart);

            //TEBBO! alt MDP Q charts for MDEG notation
            if (currentDatabase.ProductType == DatabaseProductType.MDEG)
            {
                UpdateMDPChartAlternative();
                UpdateQTrimChartsAlternative();
            }
            else
            {
                UpdateMDPChart();
                UpdateQTrimCharts();
            }

            UpdateODCDCharts();
        }

        /// <summary>
        /// Captures show real closed loop fuel checkbox selection change, clears and updates charts on "CLT Tracker" tab.
        /// </summary>
        private void cltTrackerShowRealCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            // Clear CLT tracker charts
            ChartExt.ClearCharts(cltP1TrackerChart, cltP2TrackerChart, cltP3TrackerChart, cltP4TrackerChart, cltP5TrackerChart);

            UpdateCLCharts();
        }

        /// <summary>
        /// Captures show expected closed loop fuel checkbox selection change, clears and updates charts on "CLT Tracker" tab.
        /// </summary>
        private void cltTrackerShowExpectedCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            // Clear CLT tracker charts
            ChartExt.ClearCharts(cltP1TrackerChart, cltP2TrackerChart, cltP3TrackerChart, cltP4TrackerChart, cltP5TrackerChart);

            UpdateCLCharts();
        }

        #endregion

        #region *********************** Failure Analysis ***********************

        // Failure analysis tab

        /// <summary>
        /// Gets UTIDs for fails from "Dataset" tab and runs them through the FTypeRework
        /// to assign a failure mode to each.
        /// </summary>
        private void analyseFailedInjectorsButton_Click(object sender, EventArgs e)
        {
            // Clear any existing data in datatable and chart
            failureFullDataGridView.DataSource = null;
            failureParetoChart.Series.Clear();

            string[,] utidPlantNumberList = new string[completeCalibrationData.FailCount() + incompleteCalibrationDataGridView.Rows.Count, 2];
            int listLength = 0;

            if (currentDatabase.Location == DatabaseLocation.Sudbury)
                utidPlantNumberList = new string[completeCalibrationData.FailCount() + incompleteCalibrationDataGridView.Rows.Count, 4];
            else if (currentDatabase.Location == DatabaseLocation.Stonehouse)
                utidPlantNumberList = new string[completeCalibrationData.FailCount() + incompleteCalibrationDataGridView.Rows.Count, 2];

            // Create list of utids and Plantnumbers and put into list
            foreach (var row in completeCalibrationData.FailRows())
            {
                string plantNumber = row["PlantNumber"].ToString();

                utidPlantNumberList[listLength, 0] = row["UTID"].ToString();
                utidPlantNumberList[listLength, 1] = plantNumber;
                if (currentDatabase.Location == DatabaseLocation.Sudbury)
                {
                    utidPlantNumberList[listLength, 2] = row["ISN"].ToString();
                    utidPlantNumberList[listLength, 3] = row["TestList Test#"].ToString();
                }

                listLength++;
            }

            // Also add EarlyEndFails - always fails, so no need for if statement
            foreach (DataRow row in incompleteCalibrationData.DataTable.Rows)
            {
                string plantNumber = row["PlantNumber"].ToString();

                utidPlantNumberList[listLength, 0] = row["UTID"].ToString();
                utidPlantNumberList[listLength, 1] = plantNumber;
                if (currentDatabase.Location == DatabaseLocation.Sudbury)
                {
                    utidPlantNumberList[listLength, 2] = row["ISN"].ToString();
                    utidPlantNumberList[listLength, 3] = row["TestList Test#"].ToString();
                }
                listLength++;
            }

            // If any fails, call failure analysis routines
            if (listLength > 0)
            {
                // ReSharper disable once UnusedVariable
                using (var waitCursor = new WaitCursor())
                {
                    //jz5w78
                    if (currentDatabase.Location == DatabaseLocation.Sudbury)
                    {
                        //Use the new Analysis  
                        var newFailureMode = new FailureMode.FailureMode(
                            utidPlantNumberList,
                            dateFromExtDateTimePicker.DateTimeString,
                            dateToExtDateTimePicker.DateTimeString,
                            currentDatabase.ConnectionString);
                        try
                        {
                            DoSudburyFailureAnalysis(newFailureMode);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                    }
                    else
                    {
                        // Send required variables (fails only)
                        var newRework = new FTypeRework.FTypeRework(
                            utidPlantNumberList,
                            dateFromExtDateTimePicker.DateTimeString,
                            dateToExtDateTimePicker.DateTimeString,
                            currentDatabase.ConnectionString);

                        // Return datatable of analysed results
                        try
                        {
                            var analysedResults = newRework.ReturnReworkDataTable();
                            failureFullDataGridView.DataSource = analysedResults;
                            failureFullDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

                            // Plot data
                            PlotFailurePareto(analysedResults, completeCalibrationData.DataTable.Rows.Count + incompleteCalibrationData.DataTable.Rows.Count);
                        }
                        // ReSharper disable once EmptyGeneralCatchClause
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                    }
                }
            }
            else
            {
                // No fails to analyse
                MessageBox.Show(Resources.No_failures_within_current_dataset_to_analyse);
            }
        }

        /// <summary>
        /// Retrieves current Failure Analysis and determines injector failure modes
        ///  uses a Background Worker.
        /// </summary>
        /// <param name="newFailureMode">The Failure Mode Instance</param>
        private void DoSudburyFailureAnalysis(FailureMode.FailureMode newFailureMode)
        {
            BackgroundWorker failureAnalyser = new BackgroundWorker();
            failureAnalyser.WorkerSupportsCancellation = false;

            analyseFailedInjectorsButton.Text = "Analysing please wait";
            analyseFailedInjectorsButton.Enabled = false;

            Timer callbackTimer = new Timer();
            callbackTimer.Interval = 1000;
            int state = 3;

            callbackTimer.Tick += (x, y) =>
            {
                //called every 1s
                state++;
                state = state % 4;
                StringBuilder text = new StringBuilder();
                text.Append("Analysing please wait ");
                for (int i = 0; i < state; i++)
                    text.Append(".");

                analyseFailedInjectorsButton.Text = text.ToString();
            };

            //Do Work
            failureAnalyser.DoWork += (s, evt) =>
            {
                evt.Result = newFailureMode.GetResults();
            };

            //Completed event
            failureAnalyser.RunWorkerCompleted += (s, evt) =>
            {
                try
                {
                    Tuple<Dictionary<string, int>, DataTable> analysedResults = (Tuple<Dictionary<string, int>, DataTable>)evt.Result;
                    PlotSudburyFailurePareto(analysedResults.Item1, completeCalibrationData.DataTable.Rows.Count + incompleteCalibrationData.DataTable.Rows.Count);

                    //Create a Datatable to show the results. 
                    failureFullDataGridView.InvokeIfRequired(x =>
                    {
                        failureFullDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                        failureFullDataGridView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                        failureFullDataGridView.DataSource = analysedResults.Item2;
                    });

                    MessageBox.Show("Failure Analysis has been complete.", "Failure Analysis Complete");
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
                finally
                {
                    callbackTimer.Stop();
                    analyseFailedInjectorsButton.Text = "Analyse failed injectors";
                    analyseFailedInjectorsButton.Enabled = true;
                }
            };

            callbackTimer.Start();
            failureAnalyser.RunWorkerAsync();
        }

        /// <summary>
        /// Plots the data received from FailureModeDLL into a pareto
        /// </summary> 
        /// <param name="analysedResults">The results from the failureMode DLL</param> 
        /// <param name="totalTests">The total test count</param> 
        private void PlotSudburyFailurePareto(Dictionary<string, int> analysedResults, int totalTests)
        {
            // Analyser results contains ALL possible failsures, so filter out the ones with 0
            var acctualFailureModes = from result in analysedResults
                                      where result.Value > 0
                                      orderby result.Value descending
                                      select result;

            //jz5w78 - clear to fix issue #3334
            failureParetoChart.ChartAreas[0].AxisX.CustomLabels.Clear();
            failureParetoChart.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            failureParetoChart.ChartAreas[0].AxisX.LabelStyle.Enabled = true;

            failureParetoChart.ChartAreas[0].AxisY.Minimum = 0;
            failureParetoChart.ChartAreas[0].AxisY.Interval = 5;
            failureParetoChart.ChartAreas[0].AxisY.Title = "% of Tests";

            failureParetoChart.ChartAreas[0].AxisY2.Enabled = AxisEnabled.True;
            failureParetoChart.ChartAreas[0].AxisY2.MajorGrid.LineWidth = 0;
            failureParetoChart.ChartAreas[0].AxisY2.Minimum = 0;
            failureParetoChart.ChartAreas[0].AxisY2.Maximum = 100;
            failureParetoChart.ChartAreas[0].AxisY2.Interval = 10;

            Series colSeries = new Series()
            {
                Name = "Categories",
                ChartType = SeriesChartType.Column,

                Color = Color.CornflowerBlue,
            };
            Series paretoLine = new Series()
            {
                Name = "Percentage",
                ChartType = SeriesChartType.Line,
                YAxisType = AxisType.Secondary,
                Color = Color.Red,
            };

            //create a series for each pair 
            int totalFails = acctualFailureModes.Sum(x => x.Value);
            double cumulativeTotal = 0;
            int i = 0;
            float axisInterval = 1;

            foreach (KeyValuePair<string, int> result in acctualFailureModes)
            {
                cumulativeTotal += result.Value;

                float percentOfTotalTests = (result.Value / (float)totalTests) * 100f;
                colSeries.Points.AddXY(i, percentOfTotalTests);

                //Add a Label
                failureParetoChart.ChartAreas[0].AxisX.CustomLabels.Add(
                    (i * axisInterval) - 0.5f,
                    ((i + 1) * axisInterval) - 0.5f,
                    "{1} ({0})".Args(result.Value, result.Key),
                    0,
                    LabelMarkStyle.None);

                paretoLine.Points.AddXY(i++, (cumulativeTotal / (float)totalFails) * 100f);
            }

            //Add the Pareto line
            failureParetoChart.Series.Add(colSeries);
            failureParetoChart.Series.Add(paretoLine);

            //Remove the Legend 
            failureParetoChart.Legends.Clear();
        }

        /// <summary>
        /// Groups fails by failure mode and generates a Pareto.
        /// </summary>
        /// <param name="analysedResults">DataTable containing analysed failure mode data for selected injectors.</param>
        /// <param name="totalTests">The total number of test results.</param>
        private void PlotFailurePareto(DataTable analysedResults, int totalTests)
        {
            // Need to extract list of unique FailureModeIDs with associated FailureModeDescription
            // and count the number of fails for each
            var uniqueFailureModes = from row in analysedResults.AsEnumerable()
                                     group row by new
                                     {
                                         fmID = row["FailureModeID"],
                                         fmDesc = row["FailureModeDescription"],
                                         fmoee = row["OEEFlag"]
                                     }
                                     into grp
                                     select new { grp.Key.fmID, grp.Key.fmDesc, fmCount = grp.Count(), grp.Key.fmoee }
                                     into ordgrp
                                     orderby ordgrp.fmCount descending, ordgrp.fmID ascending
                                     select ordgrp;

            var uniquePareto = from rowB in analysedResults.AsEnumerable()
                               group rowB by new
                               {
                                   fpID = rowB["ParetoID"],
                                   fpDesc = rowB["ParetoDescription"],
                                   fpoee = rowB["OEEFlag"]
                               }
                               into grpB
                               select new { grpB.Key.fpID, grpB.Key.fpDesc, fpCount = grpB.Count(), grpB.Key.fpoee }
                               into ordgrpB
                               orderby ordgrpB.fpCount descending, ordgrpB.fpID ascending
                               select ordgrpB;

            if (graphFailureModesRadioButton.Checked)
            {
                foreach (var id in uniqueFailureModes)
                {
                    if (Convert.ToString(id.fmoee).IsEmpty()) continue;

                    // Makes a decision based on filters selected whether to add the row to the graph.
                    if ((Convert.ToBoolean(id.fmoee) && (filterOEEFailsRadioButton.Checked || filterAllRadioButton.Checked)) ||
                        (!Convert.ToBoolean(id.fmoee) && (filterTestFailsRadioButton.Checked || filterAllRadioButton.Checked)))
                    {
                        var series = new Series
                        {
                            Name = "{0:N2}% - {1} ({2})".Args((float)id.fmCount * 100 / totalTests, id.fmDesc.ToString(), id.fmCount)
                        };

                        // Add this point to series
                        series.Points.AddY(id.fmCount);
                        failureParetoChart.Series.Add(series);

                        // Format & add new series to chart
                        series.ChartType = SeriesChartType.Column;
                    }
                }
                failureParetoChart.Update();

                // Make sure y-axis scale minimum major intervals is 1 (can't have half a fail!)
                if (failureParetoChart.ChartAreas[0].AxisY.MajorGrid.Interval < 1)
                {
                    failureParetoChart.ChartAreas[0].AxisY.MajorGrid.Interval = 1;
                    failureParetoChart.ChartAreas[0].AxisY.MajorTickMark.Interval = 1;
                    failureParetoChart.ChartAreas[0].AxisY.LabelStyle.Interval = 1;
                }
            }

            if (graphParetoRadioButton.Checked)
            {
                // Get count of different failure modes & Paretos
                foreach (var id in uniquePareto)
                {
                    // Makes a decision based on filters selected whether to add the row to the graph.
                    if (Convert.ToString(id.fpoee).IsEmpty()) continue;

                    if ((Convert.ToBoolean(id.fpoee) && (filterOEEFailsRadioButton.Checked || filterAllRadioButton.Checked)) ||
                        (!Convert.ToBoolean(id.fpoee) && (filterTestFailsRadioButton.Checked || filterAllRadioButton.Checked)))
                    {
                        var series = new Series
                        {
                            Name = "{0:N2}% - {1} ({2})".Args((float)id.fpCount * 100 / totalTests, id.fpDesc.ToString(), id.fpCount)
                        };

                        // Add this point to series
                        series.Points.AddY(id.fpCount);
                        failureParetoChart.Series.Add(series);

                        // Format & add new series to chart
                        series.ChartType = SeriesChartType.Column;
                    }
                }

                failureParetoChart.Update();

                // Make sure y-axis scale minimum major intervals is 1 (can't have half a fail!)
                if (failureParetoChart.ChartAreas[0].AxisY.MajorGrid.Interval < 1)
                {
                    failureParetoChart.ChartAreas[0].AxisY.MajorGrid.Interval = 1;
                    failureParetoChart.ChartAreas[0].AxisY.MajorTickMark.Interval = 1;
                    failureParetoChart.ChartAreas[0].AxisY.LabelStyle.Interval = 1;
                }
            }

            var paretoChartTitle = new StringBuilder();

            if (graphFailureModesRadioButton.Checked) paretoChartTitle.Append("Failure Modes - ");
            if (graphParetoRadioButton.Checked) paretoChartTitle.Append("Pareto - ");

            if (filterTestFailsRadioButton.Checked) paretoChartTitle.AppendLine("Test Fails");
            if (filterOEEFailsRadioButton.Checked) paretoChartTitle.AppendLine("OEE Fails");
            if (filterAllRadioButton.Checked) paretoChartTitle.AppendLine("All Fails");

            if (firstTimeTestsRadioButton.Checked) paretoChartTitle.Append("First Tests -");
            if (latestTestsRadioButton.Checked) paretoChartTitle.Append("Latest Tests -");
            if (allTestsRadioButton.Checked) paretoChartTitle.Append("All Tests - ");

            paretoChartTitle.Append("{0} to {1}".Args(dateFromExtDateTimePicker.DateTimeString, dateToExtDateTimePicker.DateTimeString));

            failureParetoChart.Titles.Clear();
            failureParetoChart.Titles.Add(
                new Title(
                    paretoChartTitle.ToString(),
                    Docking.Top,
                    new Font("Microsoft Sans Serif", 12f, FontStyle.Bold),
                    Color.Black));

            failureParetoChart.Update();
        }

        #endregion

        #region *********************** Detailed Data ***********************

        // Retrieve detailed data (FOD, OMV, NCV, CLT and leakdown) for selected injectors & NCV for nominal set(s)
        //TEBBO! Adding trimmed data 

        /// <summary>
        /// Gets UTIDs selected in "Dataset" tab, adds them to the "Detailed Data" tab list
        /// and runs methods to collect detailed test data.
        /// </summary>
        private void detailedDataUpdateButton_Click(object sender, EventArgs e)
        {
            // Set status strip label
            SetStatus("Getting selected items...");

            // Clear any current items from listboxes
            ClearPreviousDetailedData();

            // Check at least one row has been selected in the tracker datagridview
            if (completedCalibrationDataGridView.SelectedRows.Count + completedPumpingDataGridView.SelectedRows.Count <= 0)
                return;

            detailedListViewSelectedKeys.Clear();

            foreach (DataGridViewRow row in completedCalibrationDataGridView.SelectedRows)
            {
                var utid = (int)row.Cells["UTID"].Value;
                var plantNumber = (short)row.Cells["PlantNumber"].Value;

                var key = new UTIDPlantNumberKey(utid, plantNumber);
                detailedListViewSelectedKeys.Add(key);

                DetailedUTIDsListView.Items.Add(ListViewExt.CreateCheckedListViewItem(key, utid, plantNumber, row.Cells["ISN"].Value, row.Cells["PDate"].Value));
            }

            foreach (DataGridViewRow row in completedPumpingDataGridView.SelectedRows)
            {
                var utid = (int)row.Cells["UTID"].Value;
                var plantNumber = (short)row.Cells["PlantNumber"].Value;

                var key = new UTIDPlantNumberKey(utid, plantNumber);
                detailedListViewSelectedKeys.Add(key);

                DetailedUTIDsListView.Items.Add(ListViewExt.CreateCheckedListViewItem(key, utid, plantNumber, row.Cells["ISN"].Value, row.Cells["PDate"].Value));
            }

            DetailedUTIDsListView.Sorting = System.Windows.Forms.SortOrder.Descending;
            DetailedUTIDsListView.Sort();

            disablePlotPerformanceData = true;

            // Need to get detailed data (performance and leak) for all utids selected in tracker datagridview, 
            // in preparation for charting if selected.  Also put into datagridviews for easy export.
            //GetOLInitialiseDataForSelectedutids(detailedutidsListView.Items);
            GetOpenLoopDataForSelectedUTIDs(detailedListViewSelectedKeys);
            GetClosedLoopDataForSelectedUTIDs(detailedListViewSelectedKeys);
            if (!currentDatabase.FtStructure)
            {
                GetOLStabiliseDataForSelectedUTIDs(detailedListViewSelectedKeys);
                GetCLStabiliseDataForSelectedUTIDs(detailedListViewSelectedKeys);
                GetTrimmedDataForSelectedUTIDs(detailedListViewSelectedKeys);
                GetTrimmedPlantNumbers(detailedListViewSelectedKeys);
            }
            GetFODDataForSelectedUTIDs(detailedListViewSelectedKeys);
            GetOMVDataForSelectedUTIDs(detailedListViewSelectedKeys);
            ////GetLeakDataForSelectedutids(utidsListView.Items);  // This is now done differently - not available on the old rig and a chart trace on the new rig
            GetUntrimmedDataForSelectedUTIDs(detailedListViewSelectedKeys);

            disablePlotPerformanceData = false;
            PlotAllPerformanceData();
        }

        /// <summary>
        /// Gets the open loop detailed data.
        /// </summary>
        /// <param name="keys">List of selected UTIDs.</param>
        // ReSharper disable once InconsistentNaming
        private void GetOpenLoopDataForSelectedUTIDs(UTIDPlantNumberKeys keys)
        {
            // Add pressure levels to the list box
            foreach (var row in new NCV.QueryUniquePressureLevels(currentDatabase, keys).Execute())
            {
                olPressureLevelsListView.Items.Add(row.PressureReq.ToString(CultureInfo.InvariantCulture));
            }

            // Add parameters to the list box
            foreach (var row in new NCV.QueryUniqueNames(currentDatabase, keys).Execute())
            {
                olParametersListView.Items.Add(row.Name);
            }

            var uniqueIdents = new Query(currentDatabase, keys).Execute();

            // Get nominal set data if any NCV data returned & if checkbox ticked
            if (returnNominalDataCheckBox.Checked && uniqueIdents.Any())
            {
                GetOLDataForNominalSet(uniqueIdents);
                showOLNominalCheckBox.Enabled = true;
            }
            else
            {
                showOLNominalCheckBox.Enabled = false;
            }

            detailedDataSettings.OpenLoop.InitialiseListViews(olPressureLevelsListView, olParametersListView, olStatisticsListView);

            // Set status strip label
            SetStatus("OL data returned...");
        }

        /// <summary>
        /// Gets the open loop detailed data for nominal dataset(s).
        /// </summary>
        /// <param name="uniqueIdents">List of unique IDENTS to return nominal data for.</param>
        // ReSharper disable once InconsistentNaming
        private void GetOLDataForNominalSet(IEnumerable<Row> uniqueIdents)
        {
            // Set status strip label
            SetStatus("Running open loop query for nominal data set(s)...");

            var idents = uniqueIdents.Select(uniqueIdent => uniqueIdent.IDENT).ToList();

            var nominalUtiDs = new NominalUTIDQuery.Query(currentDatabase, StringExt.QuotedJoin(idents)).GetDataTable();
            foreach (DataRow row in nominalUtiDs.Rows)
            {
                var utids = row["UTIDS"].ToString();
                var plantNumber = (short)row["PlantNumber"];
                var ident = row[Queries.Parameters.QueryParameters.NominalDatasetTableIDENTColumn].ToString();

                var query = new NCVNominal.Query(currentDatabase, utids, plantNumber);
                foreach (var queryRow in query.Execute())
                {
                    queryRow.IDENT = ident;
                    openLoopNominalData.Add(queryRow);
                }
            }

            // Dispose of datatable
            nominalUtiDs.Dispose();

            // Set status strip label
            SetStatus("Nominal set NCV data returned...");
        }

        /// <summary>
        /// Gets the closed loop detailed data for selected UTIDs.
        /// </summary>
        /// <param name="keys">List of selected UTIDs.</param>
        // ReSharper disable once InconsistentNaming
        private void GetClosedLoopDataForSelectedUTIDs(UTIDPlantNumberKeys keys)
        {
            // Add pressure levels to the list box
            foreach (var row in new CLT.QueryUniquePressureLevels(currentDatabase, keys).Execute())
            {
                clPressureLevelsListView.Items.Add(row.PressureReq.ToString(CultureInfo.InvariantCulture));
                clLogPlotPressureLevelsListView.Items.Add(row.PressureReq.ToString(CultureInfo.InvariantCulture));
            }

            // Add pressure levels to the list box
            foreach (var row in new CLT.QueryUniqueNames(currentDatabase, keys).Execute())
            {
                clParametersListView.Items.Add(row.Name);
                clLogPlotParametersListView.Items.Add(row.Name);
            }

            detailedDataSettings.ClosedLoop.InitialiseListViews(clPressureLevelsListView, clParametersListView, clStatisticsListView);

            // Set status strip label to "Detailed performance data returned..."
            SetStatus("CLT data returned...");
        }

        /// <summary>
        /// Gets the trimmed detailed data for selected UTIDs.
        /// </summary>
        /// <param name="keys">List of selected UTIDs.</param>
        // ReSharper disable once InconsistentNaming
        private void GetTrimmedDataForSelectedUTIDs(UTIDPlantNumberKeys keys)
        {
            // Add pressure levels to the list box
            foreach (var row in new Trimmed.QueryUniquePressureLevels(currentDatabase, keys).Execute(true))
            {
                trimmedPressureLevelsListView.Items.Add(row.PressureReq.ToString(CultureInfo.InvariantCulture));
                trimmedLogPlotPressureLevelsListView.Items.Add(row.PressureReq.ToString(CultureInfo.InvariantCulture));
            }

            // Add parameters to the list box
            foreach (var row in new Trimmed.QueryUniqueNames(currentDatabase, keys).Execute(true))
            {
                trimmedParametersListView.Items.Add(row.Name);
                trimmedLogPlotParametersListView.Items.Add(row.Name);
            }

            detailedDataSettings.Trimmed.InitialiseListViews(trimmedPressureLevelsListView, trimmedParametersListView, trimmedStatisticsListView);
            detailedDataSettings.TrimmedLog.InitialiseListViews(trimmedLogPlotPressureLevelsListView, trimmedLogPlotParametersListView, trimmedLogPlotStatisticsListView);
            CheckTrimmedCurveToggles();

            // Set status strip label to "Detailed performance data returned..."
            SetStatus("Trimmed data returned...");
        }

        /// <summary>
        /// Adds the plant numbers to the trimmed limits combo box.
        /// </summary>
        /// <param name="keys">The keys used to get the plant numbers.</param>
        private void GetTrimmedPlantNumbers(UTIDPlantNumberKeys keys)
        {
            trimmedChart_Limits.Items.Clear();
            trimmedChart_Limits.Items.Add("None");

            foreach (var plantNumber in keys.PlantNumbers)
            {
                trimmedChart_Limits.Items.Add(plantNumber.ToString(CultureInfo.InvariantCulture));
            }

            trimmedChart_Limits.SelectedIndex = trimmedChart_Limits.Items.Count - 1;
        }

        /// <summary>
        /// Gets the open loop stabilisation detailed data for selected UTIDs.
        /// </summary>
        /// <param name="keys">List of selected UTIDs.</param>
        // ReSharper disable once InconsistentNaming
        private void GetOLStabiliseDataForSelectedUTIDs(UTIDPlantNumberKeys keys)
        {
            // Add pressure levels to the list box
            foreach (var row in new OLStabilise.QueryUniquePressureLevels(currentDatabase, keys).Execute())
            {
                olStabilisePressureLevelsListView.Items.Add(row.PressureReq.ToString(CultureInfo.InvariantCulture));
            }

            // Add parameters to the list box
            foreach (var row in new OLStabilise.QueryUniqueNames(currentDatabase, keys).Execute())
            {
                olStabiliseParametersListView.Items.Add(row.Name);
            }

            detailedDataSettings.OpenLoopStabilise.InitialiseListViews(olStabilisePressureLevelsListView, olStabiliseParametersListView, olStabiliseStatisticsListView);

            // Set status strip label
            SetStatus("Stabilise OL data returned...");
        }

        /// <summary>
        /// Gets the closed loop stabilisation detailed data for selected UTIDs.
        /// </summary>
        /// <param name="keys">List of selected UTIDs.</param>
        // ReSharper disable once InconsistentNaming
        private void GetCLStabiliseDataForSelectedUTIDs(UTIDPlantNumberKeys keys)
        {
            // Add pressure levels to the list box
            foreach (var row in new CLStabilise.QueryUniquePressureLevels(currentDatabase, keys).Execute())
            {
                clStabilisePressureLevelsListView.Items.Add(row.PressureReq.ToString(CultureInfo.InvariantCulture));
            }

            // Add parameters to the list box
            foreach (var row in new CLStabilise.QueryUniqueNames(currentDatabase, keys).Execute())
            {
                clStabiliseParametersListView.Items.Add(row.Name);
            }

            detailedDataSettings.ClosedLoopStabilise.InitialiseListViews(clStabilisePressureLevelsListView, clStabiliseParametersListView, clStabiliseStatisticsListView);

            // Set status strip label
            SetStatus("Stabilise CL data returned...");
        }

        /// <summary>
        /// Gets Fixed OMV Demand (FOD) detailed data for selected UTIDs.
        /// </summary>
        /// <param name="keys">List of selected UTIDs.</param>
        // ReSharper disable once InconsistentNaming
        private void GetFODDataForSelectedUTIDs(UTIDPlantNumberKeys keys)
        {
            try
            {
                // Add pressure levels to the list box
                foreach (var row in new FOD.QueryUniqueNames(currentDatabase, keys).Execute())
                {
                    fodParametersListView.Items.Add(row.Name);
                }

                detailedDataSettings.FOD.InitialiseListViews(null, fodParametersListView, fodStatisticsListView);
            }
            catch (NotSupportedException)
            {
            }

            // Set status strip label
            SetStatus("FOD data returned...");
        }

        /// <summary>
        /// Gets the OMV detailed data for selected UTIDs.
        /// </summary>
        /// <param name="keys">List of selected UTIDs.</param>
        // ReSharper disable once InconsistentNaming
        private void GetOMVDataForSelectedUTIDs(UTIDPlantNumberKeys keys)
        {
            try
            {
                // Add pressure levels to the list box
                foreach (var row in new OMV.QueryUniqueNames(currentDatabase, keys).Execute())
                {
                    omvParametersListView.Items.Add(row.Name);
                }

                detailedDataSettings.OMV.InitialiseListViews(null, omvParametersListView, omvStatisticsListView);
            }
            catch (NotSupportedException)
            {
            }

            // Set status strip label
            SetStatus("OMV data returned...");
        }

        /// <summary>
        /// Clears lists, class lists, charts etc. for detailed data.
        /// </summary>
        private void ClearPreviousDetailedData()
        {
            detailedDataSettings.OpenLoop.InitialiseSettings(olPressureLevelsListView, olParametersListView, olStatisticsListView);
            detailedDataSettings.ClosedLoop.InitialiseSettings(clPressureLevelsListView, clParametersListView, clStatisticsListView);
            detailedDataSettings.ClosedLoopLog.InitialiseSettings(clLogPlotPressureLevelsListView, clLogPlotParametersListView, clLogPlotStatisticsListView);
            detailedDataSettings.OpenLoopStabilise.InitialiseSettings(olStabilisePressureLevelsListView, olStabiliseParametersListView, olStabiliseStatisticsListView);
            detailedDataSettings.ClosedLoopStabilise.InitialiseSettings(clStabilisePressureLevelsListView, clStabiliseParametersListView, clStabiliseStatisticsListView);
            detailedDataSettings.FOD.InitialiseSettings(null, fodParametersListView, fodStatisticsListView);
            detailedDataSettings.OMV.InitialiseSettings(null, omvParametersListView, omvStatisticsListView);
            detailedDataSettings.Trimmed.InitialiseSettings(trimmedPressureLevelsListView, trimmedParametersListView, trimmedStatisticsListView);
            detailedDataSettings.TrimmedLog.InitialiseSettings(trimmedLogPlotPressureLevelsListView, trimmedLogPlotParametersListView, trimmedLogPlotStatisticsListView);

            // Listviews and datagrids from detailed data used for all chart tabs
            DetailedUTIDsListView.Items.Clear();

            // List views from detailed data chart tabs
            olPressureLevelsListView.Items.Clear();
            olParametersListView.Items.Clear();

            clPressureLevelsListView.Items.Clear();
            clParametersListView.Items.Clear();

            clLogPlotPressureLevelsListView.Items.Clear();
            clLogPlotParametersListView.Items.Clear();

            trimmedPressureLevelsListView.Items.Clear();
            trimmedParametersListView.Items.Clear();

            trimmedLogPlotPressureLevelsListView.Items.Clear();
            trimmedLogPlotParametersListView.Items.Clear();

            olStabilisePressureLevelsListView.Items.Clear();
            olStabiliseParametersListView.Items.Clear();

            clStabilisePressureLevelsListView.Items.Clear();
            clStabiliseParametersListView.Items.Clear();

            fodParametersListView.Items.Clear();

            omvParametersListView.Items.Clear();

            leakdownParametersListView.Items.Clear();

            utPressureLevelsListView.Items.Clear();

            // Clear current chart data & charted datagridview 
            ClearPreviousOLChartedData();
            ClearPreviousCLChartedData();
            ClearPreviousCLLogChartedData();
            ClearPreviousTrimmedChartedData();
            ClearPreviousTrimmedLogChartedData();
            ClearPreviousOLStabiliseChartedData();
            ClearPreviousCLStabiliseChartedData();
            ClearPreviousFODChartedData();
            ClearPreviousOMVChartedData();
            ClearPreviousLeakdownChartedData();
            ClearPreviousUntrimmedChartedData();

            // Clear detailed data lists
            openLoopData.Clear();
            openLoopNominalData.Clear();
            closedLoopData.Clear();
            openLoopStabiliseData.Clear();
            closedLoopStabiliseData.Clear();
            fodData.Clear();
            omvData.Clear();
            leakData.Clear();
            trimmedData.Clear();
        }

        /// <summary>
        /// Clears open loop charts and DataGridViews.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void ClearPreviousOLChartedData()
        {
            olChart.Clear();
            olChartedDataGridView.Rows.Clear();
            olChartedDataGridView.Columns.Clear();

            olNominalChartedDataGridView.Rows.Clear();
            olNominalChartedDataGridView.Columns.Clear();
        }

        /// <summary>
        /// Clears closed loop charts and DataGridViews.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void ClearPreviousCLChartedData()
        {
            clChart.Clear();
            clChartedDataGridView.Rows.Clear();
            clChartedDataGridView.Columns.Clear();
        }

        /// <summary>
        /// Clears closed loop log charts and DataGridViews.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void ClearPreviousCLLogChartedData()
        {
            clLogPlotChart.Clear();
            clLogPlotChartedDataGridView.Rows.Clear();
            clLogPlotChartedDataGridView.Columns.Clear();
        }

        /// <summary>
        /// Clears trimmed charts and DataGridViews.
        /// </summary>
        private void ClearPreviousTrimmedChartedData()
        {
            trimmedChart.Clear();
            trimmedChartedDataGridView.Rows.Clear();
            trimmedChartedDataGridView.Columns.Clear();
        }

        /// <summary>
        /// Clears trimmed log charts and DataGridViews.
        /// </summary>
        private void ClearPreviousTrimmedLogChartedData()
        {
            trimmedLogPlotChart.Clear();
            trimmedLogPlotChartedDataGridView.Rows.Clear();
            trimmedLogPlotChartedDataGridView.Columns.Clear();
        }

        /// <summary>
        /// Clears open loop stabilisation charts and DataGridViews.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void ClearPreviousOLStabiliseChartedData()
        {
            olStabiliseChart.Clear();
            olStabiliseChartedDataGridView.Rows.Clear();
            olStabiliseChartedDataGridView.Columns.Clear();

            olStabiliseNominalChartedDataGridView.Rows.Clear();
            olStabiliseNominalChartedDataGridView.Columns.Clear();
        }

        /// <summary>
        /// Clears closed loop stabilisation charts and DataGridViews.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void ClearPreviousCLStabiliseChartedData()
        {
            clStabiliseChart.Clear();
            clStabiliseChartedDataGridView.Rows.Clear();
            clStabiliseChartedDataGridView.Columns.Clear();

            clStabiliseNominalChartedDataGridView.Rows.Clear();
            clStabiliseNominalChartedDataGridView.Columns.Clear();
        }

        /// <summary>
        /// Clears Fixed OMV Demand (FOD) charts and DataGridViews.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void ClearPreviousFODChartedData()
        {
            fodChart.Clear();
            fodChartedDataGridView.Rows.Clear();
            fodChartedDataGridView.Columns.Clear();
        }

        /// <summary>
        /// Clears OMV charts and DataGridViews.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void ClearPreviousOMVChartedData()
        {
            omvChart.Clear();
            omvChartedDataGridView.Rows.Clear();
            omvChartedDataGridView.Columns.Clear();
        }

        /// <summary>
        /// Clears Leak charts and DataGridViews.
        /// </summary>
        private void ClearPreviousLeakdownChartedData()
        {
            leakdownChart.Clear();
            leakdownChartedDataGridView.Rows.Clear();
            leakdownChartedDataGridView.Columns.Clear();
        }

        /// <summary>
        /// Shows a Pop-out window with Statistical Data
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Parameters - usually null</param>
        private void viewStatsWindow_Click(object sender, EventArgs e)
        {
            if (statisticDataWindow == null)
                statisticDataWindow = new MeanMedianData();

            statisticDataWindow.Show();
        }

        ///////////////////////////////////////////////////////////////////////
        // Plot detailed data methods
        ///////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Returns an array of flags indicating the selected series to be plotted.
        /// </summary>
        /// <returns>Array of flags indicating the selected series to be plotted.</returns>
        private IEnumerable<bool> GetDetailedChartSeriesSelection()
        {
            return new[]
            {
                passFailGroupRadioButton.Checked,
                utidGroupRadioButton.Checked,
                lopGroupRadioButton.Checked,
                isnGroupRadioButton.Checked,
                identGroupRadioButton.Checked,
                testplanGroupRadioButton.Checked,
                lineGroupRadioButton.Checked,
                rigGroupRadioButton.Checked
            };
        }

        /// <summary>
        /// Returns the checked UTID's (and plant numbers) as a list of keys.
        /// </summary>
        /// <param name="listView">The list view with the checked items.</param>
        /// <returns>List of checked keys.</returns>
        private UTIDPlantNumberKeys GetCheckedItemsAsKeys(ListView listView)
        {
            var keys = new UTIDPlantNumberKeys();

            foreach (var item in listView.Items.Cast<ListViewItem>().Where(item => item.Checked))
            {
                keys.Add(item.Tag as UTIDPlantNumberKey);
            }

            return keys;
        }

        /// <summary>
        /// Updates all the performance data plots.
        /// </summary>
        private void PlotAllPerformanceData()
        {
            if (disablePlotPerformanceData) return;

            if (AutoUpdateDetailedChartsCheckBox.Checked)
            {
                PlotOpenLoopPerformanceData();
                PlotClosedLoopPerformanceData();
                PlotClosedLoopLogPlotPerformanceData();
                PlotClosedLoopStabilisePerformanceData();
                PlotOpenLoopStabilisePerformanceData();
                PlotFODPerformanceData();
                PlotOMVPerformanceData();
                PlotTrimmedPerformanceData();
                PlotTrimmedLogPlotPerformanceData();
            }
        }

        /// <summary>
        /// Updates the performance data plots specified by the tag.
        /// </summary>
        private void PlotPerformanceData(object tag)
        {
            if (disablePlotPerformanceData) return;
            if (!AutoUpdateDetailedChartsCheckBox.Checked) return;

            switch ((string)tag)
            {
                case "olChart":
                    {
                        PlotOpenLoopPerformanceData();
                    }
                    break;

                case "clChart":
                    {
                        PlotClosedLoopPerformanceData();
                    }
                    break;

                case "clLogPlotChart":
                    {
                        PlotClosedLoopLogPlotPerformanceData();
                    }
                    break;

                case "clStabiliseChart":
                    {
                        PlotClosedLoopStabilisePerformanceData();
                    }
                    break;

                case "olStabiliseChart":
                    {
                        PlotOpenLoopStabilisePerformanceData();
                    }
                    break;

                case "fodChart":
                    {
                        PlotFODPerformanceData();
                    }
                    break;

                case "omvChart":
                    {
                        PlotOMVPerformanceData();
                    }
                    break;

                case "trimmedChart":
                    {
                        PlotTrimmedPerformanceData();
                    }
                    break;

                case "trimmedLogPlotChart":
                    {
                        PlotTrimmedLogPlotPerformanceData();
                    }
                    break;
            }
        }

        /// <summary>
        /// Plot open loop charts based on user selection.
        /// </summary>
        private void PlotOpenLoopPerformanceData()
        {
            // Set status strip label
            SetStatus("Plotting open loop data...");

            var oloopData = openLoopData.Get(currentDatabase, detailedListViewSelectedKeys);

            var plotter = new NCV.Plotter(
                GetDetailedChartSeriesSelection(),
                GetCheckedItemsAsKeys(DetailedUTIDsListView),
                olPressureLevelsListView.CheckedItems(),
                olParametersListView.CheckedItems(),
                olStatisticsListView.CheckedItems(),
                olChart,
                olChartedDataGridView,
                oloopData);

            plotter.Plot();

            // Plot nominal gain curves if checkbox option selected
            if (showOLNominalCheckBox.Checked)
            {
                PlotOpenLoopNominalPerformanceData();
            }

            // Set status strip label
            SetStatus("Open loop data plotted. Ready.");
        }

        /// <summary>
        /// Plot open loop charts for nominal dataset(s) based on user selection.
        /// </summary>
        private void PlotOpenLoopNominalPerformanceData()
        {
            // Update chart with data from uniqueIdents, pressureLevels & parameters                       
            // Get the unique IDENTs 
            var uniqueIdents = openLoopNominalData.UniqueIDENTs();
            var selectedPressureLevels = olPressureLevelsListView.CheckedItems();
            var selectedParameters = olParametersListView.CheckedItems();
            var selectedStatistics = olStatisticsListView.CheckedItems();

            olNominalChartedDataGridView.Columns.AddMultipleColumns("IDENT", "PressureReq", "Name", "Duration", "Median");

            // Peform LINQ median query here to generate reduced dataset, based on median for each IDENT, and put into new table
            foreach (var id in uniqueIdents)
            {
                foreach (var pressure in selectedPressureLevels)
                {
                    foreach (var parameter in selectedParameters)
                    {
                        var newSeriesData = openLoopNominalData.FilterOnIDENTPressureAndName(id, pressure, parameter);

                        // Add data points to datagridview (separate from creating series to prevent duplication of records in datgridview)
                        foreach (var row in newSeriesData)
                        {
                            olNominalChartedDataGridView.Rows.Add(
                                row.IDENT,
                                row.PressureReq.ToString(CultureInfo.InvariantCulture),
                                row.Name,
                                row.Duration.ToString(CultureInfo.InvariantCulture),
                                row.Median.ToString(CultureInfo.InvariantCulture));
                        }

                        // Generate series for each statistic, parameter, pressure level, utid
                        foreach (string statistic in selectedStatistics)
                        {
                            // Generate new series
                            var series = new Series(string.Format("{0} {1}bar {2} median", id, pressure, parameter))
                            {
                                ChartType = SeriesChartType.Line,
                                Color = Color.Black,
                                BorderWidth = 2
                            };

                            // Add data points to series & rows to datagridview
                            foreach (var row in newSeriesData)
                            {
                                switch (statistic)
                                {
                                    case "Mean":
                                        {
                                            series.Points.AddXY(row.Duration, row.Median);
                                        }
                                        break;
                                    case "Median":
                                        {
                                            series.Points.AddXY(row.Duration, row.Median);
                                        }
                                        break;
                                }
                            }

                            olChart.Add(series);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Plot closed loop charts based on user selection.
        /// </summary>
        private void PlotClosedLoopPerformanceData()
        {
            // Set status strip label
            SetStatus("Plotting closed loop data...");

            var plotter = new CLT.Plotter(
                GetDetailedChartSeriesSelection(),
                GetCheckedItemsAsKeys(DetailedUTIDsListView),
                clPressureLevelsListView.CheckedItems(),
                clParametersListView.CheckedItems(),
                clStatisticsListView.CheckedItems(),
                clChart,
                clChartedDataGridView,
                closedLoopData.Get(currentDatabase, detailedListViewSelectedKeys));

            plotter.Plot();

            // Set status strip label
            SetStatus("Closed loop data plotted. Ready.");
        }

        /// <summary>
        /// Plot closed loop log charts based on user selection.
        /// </summary>
        private void PlotClosedLoopLogPlotPerformanceData()
        {
            // Set status strip label
            SetStatus("Plotting closed loop log plot data...");

            var plotter = new CLT.LogPlotter(
                GetDetailedChartSeriesSelection(),
                GetCheckedItemsAsKeys(DetailedUTIDsListView),
                clLogPlotPressureLevelsListView.CheckedItems(),
                clLogPlotParametersListView.CheckedItems(),
                clLogPlotStatisticsListView.CheckedItems(),
                clLogPlotChart,
                clLogPlotChartedDataGridView,
                closedLoopData.Get(currentDatabase, detailedListViewSelectedKeys),
                showFuelAsErrorCheckBox.Checked);

            plotter.Plot();

            // Set status strip label
            SetStatus("Closed loop log plot data plotted. Ready.");
        }

        /// <summary>
        /// Plot trimmed charts based on user selection.
        /// </summary>
        private void PlotTrimmedPerformanceData()
        {
            // Set status strip label
            statisticDataWindow.ClearData();

            SetStatus("Plotting trimmed data...");

            var selectedKeys = GetCheckedItemsAsKeys(DetailedUTIDsListView);

            var data = trimmedData.Get(currentDatabase, selectedKeys);
            var plotter = new Trimmed.Plotter(
                GetDetailedChartSeriesSelection(),
                selectedKeys,
                new Trimmed.QueryLimits(currentDatabase, (string)trimmedChart_Limits.SelectedItem),
                trimmedPressureLevelsListView.CheckedItems(),
                trimmedParametersListView.CheckedItems(),
                trimmedStatisticsListView.CheckedItems(),
                trimmedChart,
                trimmedChartedDataGridView,
                data);

            plotter.PlotMeanCurve = trimmedMeanToggle.Checked;
            plotter.PlotMedianCurve = trimmedMedianToggle.Checked;
            plotter.Plot();

            PlotIndividualTrimmedPerformanceData();

            // Set status strip label
            SetStatus("Trimmed data plotted. Ready.");
        }

        /// <summary>
        /// Plot trimmed charts based on user selection.
        /// </summary>
        private void PlotIndividualTrimmedPerformanceData()
        {
            // Set status strip label
            SetStatus("Plotting trimmed data...");

            var chartIndex = 0;

            UTIDPlantNumberKeys selectedKeys = GetCheckedItemsAsKeys(DetailedUTIDsListView);

            trimmedCharts.Clear();
            trimmedChartIndividualChartsLegend.Clear();

            foreach (var pressureLevel in trimmedPressureLevelsListView.CheckedItems())
            {
                var data = trimmedData.Get(currentDatabase, selectedKeys);

                var plotter = new Trimmed.Plotter(
                    GetDetailedChartSeriesSelection(),
                    selectedKeys,
                    new Trimmed.QueryLimits(currentDatabase, (string)trimmedChart_Limits.SelectedItem),
                    new List<string> { pressureLevel },
                    trimmedParametersListView.CheckedItems(),
                    trimmedStatisticsListView.CheckedItems(),
                    trimmedCharts[chartIndex],
                    trimmedChartIndividualChartsLegend,
                    null,
                    data);

                trimmedCharts.FormatTitle(chartIndex, pressureLevel);

                plotter.PlotMeanCurve = trimmedMeanToggle.Checked;
                plotter.PlotMedianCurve = trimmedMedianToggle.Checked;
                plotter.Plot();

                if (plotter.PlotMeanCurve)
                    statisticDataWindow.AddDataSet(pressureLevel + " - Mean", plotter.MeanData);

                if (plotter.PlotMedianCurve)
                    statisticDataWindow.AddDataSet(pressureLevel + " - Median", plotter.MedianData);

                ++chartIndex;
            }

            // Set status strip label
            SetStatus("Trimmed data plotted. Ready.");
        }

        /// <summary>
        /// Plot closed loop log charts based on user selection.
        /// </summary>
        private void PlotTrimmedLogPlotPerformanceData()
        {
            // Set status strip label
            SetStatus("Plotting trimmed log plot data...");

            var selectedKeys = GetCheckedItemsAsKeys(DetailedUTIDsListView);

            var plotter = new Trimmed.LogPlotter(
                GetDetailedChartSeriesSelection(),
                selectedKeys,
                trimmedLogPlotPressureLevelsListView.CheckedItems(),
                trimmedLogPlotParametersListView.CheckedItems(),
                trimmedLogPlotStatisticsListView.CheckedItems(),
                trimmedLogPlotChart,
                trimmedLogPlotChartedDataGridView,
                trimmedData.Get(currentDatabase, selectedKeys),
                trimmedShowFuelAsErrorCheckBox.Checked);

            plotter.Plot();

            // Set status strip label
            SetStatus("Trimmed log plot data plotted. Ready.");
        }

        /// <summary>
        /// Plot open loop stabilisation charts based on user selection.
        /// </summary>
        private void PlotOpenLoopStabilisePerformanceData()
        {
            // Set status strip label
            SetStatus("Plotting stabilise open loop data...");

            var plotter = new OLStabilise.Plotter(
                GetDetailedChartSeriesSelection(),
                GetCheckedItemsAsKeys(DetailedUTIDsListView),
                olStabilisePressureLevelsListView.CheckedItems(),
                olStabiliseParametersListView.CheckedItems(),
                olStabiliseStatisticsListView.CheckedItems(),
                olStabiliseChart,
                olStabiliseChartedDataGridView,
                openLoopStabiliseData.Get(currentDatabase, detailedListViewSelectedKeys));

            plotter.Plot();

            // Set status strip label
            SetStatus("Stabilise open loop data plotted. Ready.");
        }

        /// <summary>
        /// Plot closed loop stabilisation charts based on user selection.
        /// </summary>
        private void PlotClosedLoopStabilisePerformanceData()
        {
            // Set status strip label
            SetStatus("Plotting stabilise closed loop data...");

            var plotter = new CLStabilise.Plotter(
                GetDetailedChartSeriesSelection(),
                GetCheckedItemsAsKeys(DetailedUTIDsListView),
                clStabilisePressureLevelsListView.CheckedItems(),
                clStabiliseParametersListView.CheckedItems(),
                clStabiliseStatisticsListView.CheckedItems(),
                clStabiliseChart,
                clStabiliseChartedDataGridView,
                closedLoopStabiliseData.Get(currentDatabase, detailedListViewSelectedKeys));

            plotter.Plot();

            // Set status strip label
            SetStatus("Stabilise closed loop data plotted. Ready.");
        }

        /// <summary>
        /// Plot Fixed OMV Demand (FOD) charts based on user selection.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void PlotFODPerformanceData()
        {
            // Set status strip label
            SetStatus("Plotting FOD data...");

            var plotter = new FOD.Plotter(
                GetDetailedChartSeriesSelection(),
                GetCheckedItemsAsKeys(DetailedUTIDsListView),
                fodParametersListView.CheckedItems(),
                fodStatisticsListView.CheckedItems(),
                fodChart,
                fodChartedDataGridView,
                fodData.Get(currentDatabase, detailedListViewSelectedKeys));

            plotter.Plot();

            // Set status strip label
            SetStatus("FOD data plotted. Ready.");
        }

        /// <summary>
        /// Plot OMV charts based on user selection.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void PlotOMVPerformanceData()
        {
            // Set status strip label
            SetStatus("Plotting omv data...");

            var plotter = new OMV.Plotter(
                GetDetailedChartSeriesSelection(),
                GetCheckedItemsAsKeys(DetailedUTIDsListView),
                omvParametersListView.CheckedItems(),
                omvStatisticsListView.CheckedItems(),
                omvChart,
                omvChartedDataGridView,
                omvData.Get(currentDatabase, detailedListViewSelectedKeys));

            plotter.Plot();

            // Set status strip label
            SetStatus("OMV data plotted. Ready.");
        }

        /// <summary>
        /// Plot Leak charts based on user selection.
        /// </summary>
        private void PlotLeakdownPerformanceData()
        {
            // Set status strip label
            SetStatus("Plotting leak data...");

            var plotter = new Leak.Plotter(
                GetDetailedChartSeriesSelection(),
                GetCheckedItemsAsKeys(DetailedUTIDsListView),
                leakdownParametersListView.CheckedItems(),
                leakdownChart,
                leakdownChartedDataGridView,
                leakData.Get(currentDatabase, detailedListViewSelectedKeys));

            plotter.Plot();

            // Set status strip label
            SetStatus("Leak data plotted. Ready.");
        }

        /// <summary>
        /// Runs method to update open loop chart.
        /// </summary>
        private void olUpdateChartButton_Click(object sender, EventArgs e)
        {
            PlotOpenLoopPerformanceData();
        }

        /// <summary>
        /// Runs method to update closed loop chart.
        /// </summary>
        private void clUpdateChartButton_Click(object sender, EventArgs e)
        {
            PlotClosedLoopPerformanceData();
        }

        /// <summary>
        /// Runs method to update closed loop log chart.
        /// </summary>
        private void clLogPlotUpdateChartButton_Click(object sender, EventArgs e)
        {
            PlotClosedLoopLogPlotPerformanceData();
        }

        /// <summary>
        /// Runs method to update trimmed chart.
        /// </summary>
        private void trimmedUpdateChartButton_Click(object sender, EventArgs e)
        {
            PlotTrimmedPerformanceData();
        }

        /// <summary>
        /// Runs method to update trimmed log chart.
        /// </summary>
        private void trimmedLogPlotUpdateChartButton_Click(object sender, EventArgs e)
        {
            PlotTrimmedLogPlotPerformanceData();
        }

        /// <summary>
        /// Runs method to update open loop stabilisation chart.
        /// </summary>
        private void olStabiliseUpdateChartButton_Click(object sender, EventArgs e)
        {
            PlotOpenLoopStabilisePerformanceData();
        }

        /// <summary>
        /// Runs method to update closed loop stabilisation chart.
        /// </summary>
        private void clStabiliseUpdateChartButton_Click(object sender, EventArgs e)
        {
            PlotClosedLoopStabilisePerformanceData();
        }

        /// <summary>
        /// Runs method to update Fixed OMV Demand (FOD) chart.
        /// </summary>
        private void fodUpdateChartButton_Click(object sender, EventArgs e)
        {
            PlotFODPerformanceData();
        }

        /// <summary>
        /// Runs method to update OMV chart.
        /// </summary>
        private void omvUpdateChartButton_Click(object sender, EventArgs e)
        {
            PlotOMVPerformanceData();
        }

        /// <summary>
        /// Runs method to update leak chart.
        /// </summary>
        private void leakdownUpdateChartButton_Click(object sender, EventArgs e)
        {
            PlotLeakdownPerformanceData();
        }

        /// <summary>
        /// Event handler for the Untrimmed chart update action
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">the senders arguments</param>
        private void utChartUpdateButton_Click(object sender, EventArgs e)
        {
            PlotUntrimmedPerformanceData();
        }

        /// <summary>
        /// Clears all previous entries on the untrimmed chart tab
        /// </summary>
        private void ClearPreviousUntrimmedChartedData()
        {
            utChart.Clear();
            utChartedDataGridView.Columns.Clear();
            utChartedDataGridView.Rows.Clear();
        }

        /// <summary>
        /// Retrieves the Untrimmed data for selected UTID plant number pairs
        /// </summary>
        /// <param name="keys">UTID plant number pairs</param>
        private void GetUntrimmedDataForSelectedUTIDs(UTIDPlantNumberKeys keys)
        {
            // Add pressure levels to the list box
            foreach (var row in new NCV.QueryUniquePressureLevels(currentDatabase, keys).Execute())
            {
                utPressureLevelsListView.Items.Add(row.PressureReq.ToString(CultureInfo.InvariantCulture));
            }

            // Set status strip label
            SetStatus("Untrimmed data returned...");
        }

        /// <summary>
        /// Plots the untrimmed Performance graphs
        /// </summary>
        private void PlotUntrimmedPerformanceData()
        {
            // Set status strip label
            SetStatus("Plotting Untrimmed data...");

            var plotter = new Untrimmed.Plotter(
                GetDetailedChartSeriesSelection(),
                GetCheckedItemsAsKeys(DetailedUTIDsListView),
                utPressureLevelsListView.CheckedItems(),
                new string[] { "Fuel" }.ToList(),
                new string[] { "UntrimmedValue" }.ToList(),
                utChart,
                utChartedDataGridView,
                untrimmedData.Get(currentDatabase, detailedListViewSelectedKeys));

            utChartedDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            utChartedDataGridView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

            plotter.Plot();

            // Set status strip label
            SetStatus("Untrimmed data plotted. Ready.");
        }

        /// <summary>
        /// Event Handler for the check changing on either median or mean Curves
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">the senders arguments</param>
        private void trimmedMeanToggle_CheckedChanged(object sender, EventArgs e)
        {
            //replot data
            PlotPerformanceData(detailedDataTabControl.SelectedTab.Tag);
        }

        /// <summary>
        /// Event handler for the Trimmed Median toggle is changed
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">the senders arguments</param>
        private void trimmedMedianToggle_CheckedChanged(object sender, EventArgs e)
        {
            //replot data
            PlotPerformanceData(detailedDataTabControl.SelectedTab.Tag);
        }

        /// <summary>
        /// Checks and sets states of the Median and mean curve displays
        /// </summary>
        private void CheckTrimmedCurveToggles()
        {
            int count = DetailedUTIDsListView.Items.Count;

            //Enable / disable mean/median curves if there is more than one selected 
            trimmedMeanToggle.Enabled = count > 1;
            if (!trimmedMeanToggle.Enabled)
                trimmedMeanToggle.Checked = false;

            trimmedMedianToggle.Enabled = count > 1;
            if (!trimmedMedianToggle.Enabled)
                trimmedMedianToggle.Checked = false;
        }

        #endregion

        #region *********************** Chart Traces ***********************

        /// <summary>
        /// Returns a list of selected series names
        /// </summary>
        /// <returns>List of selected series names</returns>
        private IEnumerable<string> GetSelectedSeries()
        {
            throw new NotImplementedException("Feature not yet implemented, blame bad developer. ^_^");
        }

        /// <summary>
        /// Gets selected UTIDs from "Dataset" tab, populates UTID list
        /// and runs methods to return trace data.
        /// </summary>
        private void getTraceDataButton_Click(object sender, EventArgs e)
        {
            // Set status strip label
            SetStatus("Getting selected items...");

            // Clear any current items from listboxes
            ClearPreviousChartTraceData();

            // Check at least one row has been selected in the tracker datagridview
            if (completedCalibrationDataGridView.Rows.GetRowCount(DataGridViewElementStates.Selected) > 0)
            {
                var keys = new UTIDPlantNumberKeys();

                foreach (DataGridViewRow dr in completedCalibrationDataGridView.SelectedRows)
                {
                    // Grab utids from the selected rows
                    var utid = (int)dr.Cells["UTID"].Value;
                    var plantNumber = (short)dr.Cells["PlantNumber"].Value;
                    var isn = dr.Cells["ISN"].Value.ToString();
                    var date = dr.Cells["PDate"].Value.ToString();

                    var key = new UTIDPlantNumberKey(utid, plantNumber);
                    keys.Add(key);

                    // Add to listbox, ready for selection
                    traceCalibrationutidsListView.Items.Add(ListViewExt.CreateCheckedListViewItem(
                        key,
                        utid.ToString(CultureInfo.InvariantCulture),
                        plantNumber.ToString(CultureInfo.InvariantCulture),
                        isn,
                        date));
                }

                traceCalibrationutidsListView.Sorting = System.Windows.Forms.SortOrder.Descending;
                traceCalibrationutidsListView.Sort();

                // Now get list of available parameters from selected injectors and test points (URIDs, but by running condition)
                GetOpenLoopChartTraceData(keys);
                GetClosedLoopChartTraceData(keys);
            }

            // Check at least one row has been selected in the tracker datagridview
            int selectedPumpCount = completedPumpingDataGridView.Rows.GetRowCount(DataGridViewElementStates.Selected) +
                incompletePumpingDataGridView.Rows.GetRowCount(DataGridViewElementStates.Selected);

            if (selectedPumpCount > 0)
            {
                var keys = new UTIDPlantNumberKeys();

                foreach (DataGridViewRow dr in completedPumpingDataGridView.SelectedRows)
                {
                    // Grab utids from the selected rows
                    var utid = (int)dr.Cells["UTID"].Value;
                    var plantNumber = (short)dr.Cells["PlantNumber"].Value;
                    var isn = dr.Cells["ISN"].Value.ToString();
                    var pate = dr.Cells["PDate"].Value.ToString();

                    var key = new UTIDPlantNumberKey(utid, plantNumber);
                    keys.Add(key);

                    // Add to listbox, ready for selection
                    tracePumpingutidsListView.Items.Add(ListViewExt.CreateCheckedListViewItem(key, utid.ToString(CultureInfo.InvariantCulture), plantNumber.ToString(CultureInfo.InvariantCulture), isn, pate));
                }

                foreach (DataGridViewRow dr in incompletePumpingDataGridView.SelectedRows)
                {
                    // Grab utids from the selected rows
                    var utid = (int)dr.Cells["UTID"].Value;
                    var plantNumber = (short)dr.Cells["PlantNumber"].Value;
                    var isn = dr.Cells["ISN"].Value.ToString();
                    var pate = dr.Cells["PDate"].Value.ToString();

                    var key = new UTIDPlantNumberKey(utid, plantNumber);
                    keys.Add(key);

                    // Add to listbox, ready for selection
                    tracePumpingutidsListView.Items.Add(ListViewExt.CreateCheckedListViewItem(key, utid.ToString(CultureInfo.InvariantCulture), plantNumber.ToString(CultureInfo.InvariantCulture), isn, pate));
                }

                tracePumpingutidsListView.Sorting = System.Windows.Forms.SortOrder.Descending;
                tracePumpingutidsListView.Sort();

                // Now get list of available parameters from selected injectors and test points (URIDs, but by running condition)
                GetOpenLoopChartTraceData(keys);
                GetClosedLoopChartTraceData(keys);
                GetCLStabiliseChartTraceData(keys);
                GetFODChartTraceData(keys);
                GetOMVChartTraceData(keys);
            }

            chartTracesSettings.InitialiseListView(
                traceNCVTestPointListView,
                traceCLTTestPointListView,
                traceCLSTestPointListView,
                traceFODTestPointListView,
                traceOMVTestPointListView);

            AutoUpdateTraceCharts();
        }

        /// <summary>
        /// Retrieves open loop chart trace data for selected UTIDs.
        /// </summary>
        /// <param name="keys">List of selected UTIDs.</param>
        private void GetOpenLoopChartTraceData(UTIDPlantNumberKeys keys)
        {
            var query = new ChartTraceNCV.QueryUniqueSpeedPressureDuration(currentDatabase, keys);
            var rows = query.Execute();

            // Add pressure levels to the list box
            foreach (var row in rows)
            {
                traceNCVTestPointListView.Items.Add(row.SpeedPressureDuration);
            }
        }

        /// <summary>
        /// Retrieves closed loop chart trace data for selected UTIDs.
        /// </summary>
        /// <param name="keys">List of selected UTIDs.</param>
        private void GetClosedLoopChartTraceData(UTIDPlantNumberKeys keys)
        {
            var query = new ChartTraceCLT.QueryUniqueSpeedPressureDemand(currentDatabase, keys);
            var rows = query.Execute();

            // Add pressure levels to the list box
            foreach (var row in rows)
            {
                traceCLTTestPointListView.Items.Add(row.SpeedPressureDemand);
            }
        }

        /// <summary>
        /// Retrieves closed loop stabilise chart trace data for selected UTIDs.
        /// </summary>
        /// <param name="keys">List of selected UTIDs.</param>
        private void GetCLStabiliseChartTraceData(UTIDPlantNumberKeys keys)
        {
            var query = new ChartTraceCLS.UniqueSpeedPressureDemand(currentDatabase, keys);
            var rows = query.Execute();

            var uniqueRowNames = rows.GroupBy(x => x.SpeedPressureDemand, (key, p) => p.FirstOrDefault()).ToList();

            foreach (var row in uniqueRowNames)
            {
                traceCLSTestPointListView.Items.Add(row.SpeedPressureDemand);
            }
        }

        /// <summary>
        /// Retrieves Fixed OMV Demand (FOD) chart trace data for selected UTIDs.
        /// </summary>
        /// <param name="keys">List of selected UTIDs.</param> 
        private void GetFODChartTraceData(UTIDPlantNumberKeys keys)
        {
            // Add pressure levels to the list box
            foreach (var row in new ChartTraceFOD.QueryUniqueLobe1OMVPressureDemand(currentDatabase, keys).Execute())
            {
                traceFODTestPointListView.Items.Add(row.SpeedLobe1OMVDemand);
            }
        }

        /// <summary>
        /// Retrieves OMV chart trace data for selected UTIDs.
        /// </summary>
        /// <param name="keys">List of selected UTIDs.</param> 
        private void GetOMVChartTraceData(UTIDPlantNumberKeys keys)
        {
            // Add pressure levels to the list box
            foreach (var row in new ChartTraceOMV.QueryUniqueSpeedPressureDuration(currentDatabase, keys).Execute())
            {
                traceOMVTestPointListView.Items.Add(row.SpeedPressureDuration);
            }
        }

        /// <summary>
        /// Clears chart trace lists, class lists and chart.
        /// </summary>
        private void ClearPreviousChartTraceData()
        {
            chartTraceDataOL.Clear();
            chartTraceDataCL.Clear();
            chartTraceDataCLS.Clear();
            chartTraceDataFOD.Clear();
            chartTraceDataOMV.Clear();

            chartTracesSettings.InitialiseCheckedItems(
                traceNCVTestPointListView,
                traceCLTTestPointListView,
                traceCLSTestPointListView,
                traceFODTestPointListView,
                traceOMVTestPointListView);

            ListViewExt.ClearListViews(
                traceCalibrationutidsListView,
                tracePumpingutidsListView,
                traceNCVTestPointListView,
                traceCLTTestPointListView,
                traceCLSTestPointListView,
                traceFODTestPointListView,
                traceOMVTestPointListView);

            chartTraceChart.Series.Clear();
        }

        /// <summary>
        /// Called when the auto update trace charts check box is changed state.
        /// Updates the charts and disables the manual update button if checked.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void AutoUpdateTraceChartsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            updateChartTraceChartButton.Enabled = !AutoUpdateTraceChartsCheckBox.Checked;
        }

        /// <summary>
        /// Runs methods to generate chart series and updates chart trace chart for selected UTIDs.
        /// </summary>
        private void chartTraceUpdateChartButton_Click(object sender, EventArgs e)
        {
            UpdateTraceCharts();
        }

        /// <summary>
        /// Called when the checked traces UTIDs change. Updates the chart.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void TraceUTIDListView_CheckedChanged(object sender, ItemCheckedEventArgs e)
        {
            AutoUpdateTraceCharts();
        }

        /// <summary>
        /// Called when the checked traces open loop change. Updates the chart.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void TraceOpenLoopListView_CheckedChanged(object sender, ItemCheckedEventArgs e)
        {
            AutoUpdateTraceCharts();
        }

        /// <summary>
        /// Called when the checked traces closed loop change. Updates the chart.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void TraceClosedLoopListView_CheckedChanged(object sender, ItemCheckedEventArgs e)
        {
            AutoUpdateTraceCharts();
        }

        /// <summary>
        /// Called when the checked traces FOD change. Updates the chart.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void TraceFODListView_CheckedChanged(object sender, ItemCheckedEventArgs e)
        {
            AutoUpdateTraceCharts();
        }

        /// <summary>
        /// Called when the checked traces OMV change. Updates the chart.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void TraceOMVListView_CheckedChanged(object sender, ItemCheckedEventArgs e)
        {
            AutoUpdateTraceCharts();
        }

        /// <summary>
        /// Called when the checked series changes. Updates the chart.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void traceSeriesListView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            AutoUpdateTraceCharts();
        }

        /// <summary>
        /// Updates the traces chart if the auto update check box is checked.
        /// </summary>
        private void AutoUpdateTraceCharts()
        {
            if (AutoUpdateTraceChartsCheckBox.Checked)
            {
                UpdateTraceCharts();
            }
        }

        /// <summary>
        /// Updates the traces chart.
        /// </summary>
        private void UpdateTraceCharts()
        {
            ClearPreviousChartTraceChartedData();

            // Get selected utids
            var selectedKeys = GetCheckedItemsAsKeys(traceCalibrationutidsListView);
            var selectedPumpingutids = GetCheckedItemsAsKeys(tracePumpingutidsListView);

            // Plot chart traces for each test section
            PlotSelectedOpenLoopTraces(selectedKeys);
            PlotSelectedCLSTraces(selectedPumpingutids);

            selectedKeys.Add(selectedPumpingutids);
            PlotSelectedClosedLoopTraces(selectedKeys);
            PlotSelectedFODTraces(selectedPumpingutids);
            PlotSelectedOMVTraces(selectedPumpingutids);

            chartTraceChart.InvokeIfRequired(c => ChartAxisScaling.AxisScaler.ScaleAxis(c.ChartAreas[0], c.Name));
            ConfigureChart(chartTraceChart, true, true);
        }

        /// <summary>
        /// Configure the chart series.
        /// </summary>
        /// <param name="series">The series to configure.</param>
        /// <param name="utidIndex">The index of the UTID.</param>
        /// <param name="testPointIndex">The index of the test point.</param>
        /// <param name="seriesNamePostFix">The postfix for the name of the series.</param>
        private void ConfigureChartSeries(Series series, int utidIndex, int testPointIndex, string seriesNamePostFix)
        {
            // For first row & first testpoint ONLY add single-point legends (OD, CD, LERP etc.)
            // Add multi-point legends for ALL rows & testpoints after (line series, such as rate, pressure etc.)
            if ((utidIndex != 0 || testPointIndex != 0) && series.Points.Count() == 1)
            {
                series.IsVisibleInLegend = false;
            }
            //// If first UTID & testpoint and of single point series, add to separate legend.
            else if (series.Points.Count() == 1)
            {
                // Shorten legend name for point series so doesn't include UTID & URID
                var param = series.Name.Split(' ');
                if (param.Count() <= 1) return;

                series.Name = param[2] + string.Format(" ({0})", seriesNamePostFix);
                series.Legend = "legendPointSeries";
            }
            //// All other series are line series, so put these in RHS legend
            else
            {
                series.Name = series.Name + string.Format(" ({0})", seriesNamePostFix);
                series.Legend = "legendLineSeries";
            }
        }

        /// <summary>
        /// Generates open loop chart trace series for selected UTIDs and test points.
        /// </summary>
        /// <param name="keys">List of selected UTIDs.</param>
        private void PlotSelectedOpenLoopTraces(UTIDPlantNumberKeys keys)
        {
            // Get selected TestPoints
            List<string> selectedTestPoints = traceNCVTestPointListView.CheckedItems();

            var rows = chartTraceDataOL.Get(currentDatabase, keys);

            // Generate list of unique utids and TestPoints from those selected
            var uniqueLoPs = rows.UniqueLOPs(keys, selectedTestPoints);
            var uniqueutids = rows.Uniqueutids(keys, selectedTestPoints);
            var uniqueTestPoints = rows.UniqueSpeedPressureDurations(keys, selectedTestPoints);
            var uniqueIdenTs = rows.UniqueIDENTS(keys, selectedTestPoints);
            var uniqueTestplans = rows.UniqueTestPlans(keys, selectedTestPoints);
            var uniqueIsNs = rows.UniqueISNs(keys, selectedTestPoints);
            var uniqueLiNes = rows.UniqueLines(keys, selectedTestPoints);

            List<ChartBLOB> blobs = new List<ChartBLOB>();

            foreach (var key in keys)
            {
                foreach (var testPoint in selectedTestPoints)
                {
                    var newSeriesData = rows.FilterOnUTIDAndSpeedPressureDuration(key, testPoint);

                    foreach (var row in newSeriesData)
                    {
                        // Work through BLOB for given UTID, URID
                        ChartBLOB blob = new ChartBLOB(row.BinaryTraceData, row.UTID, row.URID);
                        blobs.Add(blob);

                        //Draw each Blobl series
                        // Get index of current UTID within uniqueUTID
                        var lopIndex = uniqueLoPs.IndexOf(row.LOP);
                        var utidIndex = uniqueutids.IndexOf(row.UTID);
                        var testPointIndex = uniqueTestPoints.IndexOf(row.SpeedPressureDuration);
                        var identIndex = uniqueIdenTs.IndexOf(row.IDENT);
                        var testplanIndex = uniqueTestplans.IndexOf(row.Testplan);
                        var isnIndex = uniqueIsNs.IndexOf(row.ISN);
                        var lineIndex = uniqueLiNes.IndexOf(row.LineNumber);

                        // Add all series for current datarow
                        foreach (var series in blob.Series)
                        {
                            SetChartTraceChartSeriesColours(series, utidIndex, testPointIndex, lopIndex, identIndex, testplanIndex, isnIndex, lineIndex, row.LOP, row.IDENT, row.Testplan, row.ISN, row.LineNumber, row.Global);
                            ConfigureChartSeries(series, utidIndex, testPointIndex, "OL");
                            chartTraceChart.Series.Add(series);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Generates closed loop chart trace series for selected UTIDs and test points.
        /// </summary>
        /// <param name="keys">List of selected UTIDs.</param>
        private void PlotSelectedClosedLoopTraces(UTIDPlantNumberKeys keys)
        {
            // Get selected TestPoints
            List<string> selectedTestPoints = traceCLTTestPointListView.CheckedItems();

            var rows = chartTraceDataCL.Get(currentDatabase, keys);

            // Generate list of unique utids and TestPoints from those selected
            var uniqueLoPs = rows.UniqueLOPs(keys, selectedTestPoints);
            var uniqueutids = rows.Uniqueutids(keys, selectedTestPoints);
            var uniqueTestPoints = rows.UniqueSpeedPressureDemands(keys, selectedTestPoints);
            var uniqueIdenTs = rows.UniqueIDENTS(keys, selectedTestPoints);
            var uniqueTestplans = rows.UniqueTestPlans(keys, selectedTestPoints);
            var uniqueIsNs = rows.UniqueISNs(keys, selectedTestPoints);
            var uniqueLiNes = rows.UniqueLines(keys, selectedTestPoints);

            foreach (var key in keys)
            {
                foreach (var testPoint in selectedTestPoints)
                {
                    var newSeriesData = rows.FilterOnUTIDAndSpeedPressureDemand(key, testPoint);

                    foreach (var row in newSeriesData)
                    {
                        // Work through BLOB for given UTID, URID, TestPoint
                        ChartBLOB blob = new ChartBLOB(row.BinaryTraceData, row.UTID, row.URID);

                        // Get index of current UTID within uniqueUTID
                        var lopIndex = uniqueLoPs.IndexOf(row.LOP);
                        var utidIndex = uniqueutids.IndexOf(row.UTID);
                        var testPointIndex = uniqueTestPoints.IndexOf(row.SpeedPressureDemand);
                        var identIndex = uniqueIdenTs.IndexOf(row.IDENT);
                        var testplanIndex = uniqueTestplans.IndexOf(row.Testplan);
                        var isnIndex = uniqueIsNs.IndexOf(row.ISN);
                        var lineIndex = uniqueLiNes.IndexOf(row.LineNumber);

                        // Add all series for current datarow
                        foreach (var series in blob.Series)
                        {
                            SetChartTraceChartSeriesColours(series, utidIndex, testPointIndex, lopIndex, identIndex, testplanIndex, isnIndex, lineIndex, row.LOP, row.IDENT, row.Testplan, row.ISN, row.LineNumber, row.Global);
                            ConfigureChartSeries(series, utidIndex, testPointIndex, "CL");
                            chartTraceChart.Series.Add(series);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Generates closed loop stabilise chart trace series for selected UTIDs and test points.
        /// </summary>
        /// <param name="keys">List of selected UTIDs.</param>
        private void PlotSelectedCLSTraces(UTIDPlantNumberKeys keys)
        {
            // Get selected TestPoints
            List<string> selectedTestPoints = traceCLSTestPointListView.CheckedItems();

            var rows = chartTraceDataCLS.Get(currentDatabase, keys);

            // Generate list of unique utids and TestPoints from those selected
            var uniqueLoPs = rows.UniqueLOPs(keys, selectedTestPoints);
            var uniqueutids = rows.Uniqueutids(keys, selectedTestPoints);
            var uniqueTestPoints = rows.UniqueSpeedPressureDemands(keys, selectedTestPoints);
            var uniqueIdenTs = rows.UniqueIDENTS(keys, selectedTestPoints);
            var uniqueTestplans = rows.UniqueTestPlans(keys, selectedTestPoints);
            var uniqueIsNs = rows.UniqueISNs(keys, selectedTestPoints);
            var uniqueLiNes = rows.UniqueLines(keys, selectedTestPoints);

            foreach (var key in keys)
            {
                foreach (var testPoint in selectedTestPoints)
                {
                    var newSeriesData = rows.FilterOnUTIDAndSpeedPressureDemand(key, testPoint);

                    foreach (var row in newSeriesData)
                    {
                        // Work through BLOB for given UTID, URID, TestPoint
                        ChartBLOB blob = new ChartBLOB(row.BinaryTraceData, row.UTID, row.URID);

                        // Get index of current UTID within uniqueUTID
                        var lopIndex = uniqueLoPs.IndexOf(row.LOP);
                        var utidIndex = uniqueutids.IndexOf(row.UTID);
                        var testPointIndex = uniqueTestPoints.IndexOf(row.SpeedPressureDemand);
                        var identIndex = uniqueIdenTs.IndexOf(row.IDENT);
                        var testplanIndex = uniqueTestplans.IndexOf(row.Testplan);
                        var isnIndex = uniqueIsNs.IndexOf(row.ISN);
                        var lineIndex = uniqueLiNes.IndexOf(row.LineNumber);

                        // Add all series for current datarow
                        foreach (var series in blob.Series)
                        {
                            SetChartTraceChartSeriesColours(series, utidIndex, testPointIndex, lopIndex, identIndex, testplanIndex, isnIndex, lineIndex, row.LOP, row.IDENT, row.Testplan, row.ISN, row.LineNumber, row.Global);
                            ConfigureChartSeries(series, utidIndex, testPointIndex, "CL");
                            chartTraceChart.Series.Add(series);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Generates Fixed OMV Demand (FOD) chart trace series for selected UTIDs and test points.
        /// </summary>
        /// <param name="keys">List of selected UTIDs.</param>
        private void PlotSelectedFODTraces(UTIDPlantNumberKeys keys)
        {
            // Get selected TestPoints
            List<string> selectedTestPoints = traceFODTestPointListView.CheckedItems();

            var rows = chartTraceDataFOD.Get(currentDatabase, keys);

            // Generate list of unique utids and TestPoints from those selected
            var uniqueLoPs = rows.UniqueLOPs(keys, selectedTestPoints);
            var uniqueutids = rows.Uniqueutids(keys, selectedTestPoints);
            var uniqueTestPoints = rows.UniqueSpeedLobe1OMVDemands(keys, selectedTestPoints);
            var uniqueIdenTs = rows.UniqueIDENTS(keys, selectedTestPoints);
            var uniqueTestplans = rows.UniqueTestPlans(keys, selectedTestPoints);
            var uniqueIsNs = rows.UniqueISNs(keys, selectedTestPoints);
            var uniqueLiNes = rows.UniqueLines(keys, selectedTestPoints);

            foreach (var key in keys)
            {
                foreach (var testPoint in selectedTestPoints)
                {
                    var newSeriesData = rows.FilterOnUTIDAndSpeedLobe1OMVDemand(key, testPoint);

                    foreach (var row in newSeriesData)
                    {
                        // Work through BLOB for given UTID, URID, TestPoint
                        ChartBLOB blob = new ChartBLOB(row.BinaryTraceData, row.UTID, row.URID);

                        // Get index of current UTID within uniqueUTID
                        var lopIndex = uniqueLoPs.IndexOf(row.LOP);
                        var utidIndex = uniqueutids.IndexOf(row.UTID);
                        var testPointIndex = uniqueTestPoints.IndexOf(row.SpeedLobe1OMVDemand);
                        var identIndex = uniqueIdenTs.IndexOf(row.IDENT);
                        var testplanIndex = uniqueTestplans.IndexOf(row.Testplan);
                        var isnIndex = uniqueIsNs.IndexOf(row.ISN);
                        var lineIndex = uniqueLiNes.IndexOf(row.LineNumber);

                        // Add all series for current datarow
                        foreach (var series in blob.Series)
                        {
                            SetChartTraceChartSeriesColours(series, utidIndex, testPointIndex, lopIndex, identIndex, testplanIndex, isnIndex, lineIndex, row.LOP, row.IDENT, row.Testplan, row.ISN, row.LineNumber, row.Global);
                            ConfigureChartSeries(series, utidIndex, testPointIndex, "FOD");
                            chartTraceChart.Series.Add(series);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Generates OMV chart trace series for selected UTIDs and test points.
        /// </summary>
        /// <param name="keys">List of selected UTIDs.</param>
        private void PlotSelectedOMVTraces(UTIDPlantNumberKeys keys)
        {
            // Get selected TestPoints
            List<string> selectedTestPoints = traceOMVTestPointListView.CheckedItems();

            var rows = chartTraceDataOMV.Get(currentDatabase, keys);

            // Generate list of unique utids and TestPoints from those selected
            var uniqueLoPs = rows.UniqueLOPs(keys, selectedTestPoints);
            var uniqueutids = rows.Uniqueutids(keys, selectedTestPoints);
            var uniqueTestPoints = rows.UniqueSpeedPressureDurations(keys, selectedTestPoints);
            var uniqueIdenTs = rows.UniqueIDENTS(keys, selectedTestPoints);
            var uniqueTestplans = rows.UniqueTestPlans(keys, selectedTestPoints);
            var uniqueIsNs = rows.UniqueISNs(keys, selectedTestPoints);
            var uniqueLiNes = rows.UniqueLines(keys, selectedTestPoints);

            foreach (var key in keys)
            {
                foreach (var testPoint in selectedTestPoints)
                {
                    var newSeriesData = rows.FilterOnUTIDAndSpeedPressureDuration(key, testPoint);

                    foreach (var row in newSeriesData)
                    {
                        // Work through BLOB for given UTID, URID, TestPoint
                        ChartBLOB blob = new ChartBLOB(row.BinaryTraceData, row.UTID, row.URID);

                        // Get index of current UTID within uniqueUTID
                        var lopIndex = uniqueLoPs.IndexOf(row.LOP);
                        var utidIndex = uniqueutids.IndexOf(row.UTID);
                        var testPointIndex = uniqueTestPoints.IndexOf(row.SpeedPressureDuration);
                        var identIndex = uniqueIdenTs.IndexOf(row.IDENT);
                        var testplanIndex = uniqueTestplans.IndexOf(row.Testplan);
                        var isnIndex = uniqueIsNs.IndexOf(row.ISN);
                        var lineIndex = uniqueLiNes.IndexOf(row.LineNumber);

                        // Add all series for current datarow
                        foreach (var series in blob.Series)
                        {
                            SetChartTraceChartSeriesColours(series, utidIndex, testPointIndex, lopIndex, identIndex, testplanIndex, isnIndex, lineIndex, row.LOP, row.IDENT, row.Testplan, row.ISN, row.LineNumber, row.Global);
                            ConfigureChartSeries(series, utidIndex, testPointIndex, "OMV");
                            chartTraceChart.Series.Add(series);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Clears chart trace chart.
        /// </summary>
        private void ClearPreviousChartTraceChartedData()
        {
            chartTraceChart.Series.Clear();
        }

        /// <summary>
        /// Called when the auto update charts check box is changed state.
        /// Updates the charts and disables the manual update buttons if checked.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void AutoUpdateDetailedChartsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            olUpdateChartButton.Enabled =
            clUpdateChartButton.Enabled =
            clLogPlotUpdateChartButton.Enabled =
            olStabiliseUpdateChartButton.Enabled =
            clStabiliseUpdateChartButton.Enabled =
            fodUpdateChartButton.Enabled =
            omvUpdateChartButton.Enabled =
            trimmedUpdateChartButton.Enabled =
            trimmedLogPlotUpdateChartButton.Enabled = !AutoUpdateDetailedChartsCheckBox.Checked;

            if (AutoUpdateDetailedChartsCheckBox.Checked)
            {
                PlotAllPerformanceData();
            }
        }

        /// <summary>
        /// Called when a legend checkbox check state changes. Shows or hides the appropriate
        /// chart legend.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void DetailedChartLegendCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            var checkBox = sender as CheckBox;
            if (checkBox == null) return;

            var individualCharts = GetIndividualChartsCheckBox(checkBox.Tag);
            if (individualCharts != null)
            {
                var legend = GetControl<ChartLegend>(checkBox.Tag + "IndividualChartsLegend");
                if (legend != null)
                {
                    legend.Visible = individualCharts.Checked && checkBox.Checked;
                }
            }

            if ((individualCharts == null) || (!individualCharts.Checked))
            {
                var chartScrollableLegend = GetControl<ChartSplitLegend>(checkBox.Tag);
                if (chartScrollableLegend != null)
                {
                    chartScrollableLegend.ShowLegend = checkBox.Checked;
                }

                foreach (var legend in GetAllCharts(checkBox.Tag).Where(chart => chart != null).SelectMany(chart => chart.Legends))
                {
                    legend.Enabled = checkBox.Checked;
                }
            }
        }

        /// <summary>
        /// Called when the checked detailed UTIDs change. Updates the chart.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void DetailedUTIDListView_CheckedChanged(object sender, ItemCheckedEventArgs e)
        {
            CheckTrimmedCurveToggles();
            PlotPerformanceData(detailedDataTabControl.SelectedTab.Tag);
        }

        /// <summary>
        /// Called when the checked state of the pressure levels changes. Shows or hides
        /// the individual charts check box (if it exists) and shows or hides the charts
        /// appropriately. Updates the chart.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void PressureLevelsListView_CheckedChanged(object sender, ItemCheckedEventArgs e)
        {
            var listView = sender as ListView;
            if (listView == null) return;

            // Get the individual charts check box (if it exists)
            var individualCheckBox = GetIndividualChartsCheckBox(listView.Tag);
            if (individualCheckBox != null)
            {
                // If more than one pressure level checked
                if (listView.CheckedItems.Count > 1)
                {
                    ControlExt.ShowControls(individualCheckBox);

                    IndividualCharts_CheckedChanged(individualCheckBox, null);
                }
                else
                {
                    ControlExt.ShowControls(GetControl<Chart>(listView.Tag));
                    ControlExt.HideControls(individualCheckBox, GetTableLayoutPanel(listView.Tag));
                }
            }

            PlotPerformanceData(listView.Tag);
        }

        private void checkBoxCheckSudbury_CheckedChanged(object sender, EventArgs e)
        {
            if (currentDatabase != null)
            {
                currentDatabase.QueryIasiArchiveStructure = checkBoxUseIasiArchive.Checked;
                currentDatabase.QuerySudburyArchiveStructure = checkBoxUseSudburyArchive.Checked;
            }

           //dawid 25/03
           //UpdateListDataGridView(UpdateType.UpdateLopList);
        }


        private void checkBoxFastQueries_CheckedChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Called when the checked state of the parameters changes. Updates the charts.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void ParametersListView_CheckChanged(object sender, ItemCheckedEventArgs e)
        {
            var listView = sender as ListView;
            if (listView != null)
            {
                PlotPerformanceData(listView.Tag);
            }
        }

        /// <summary>
        /// Called when the checked state of the statistics changes. Updates the charts.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void StatisticsListView_CheckChanged(object sender, ItemCheckedEventArgs e)
        {
            var listView = sender as ListView;
            if (listView != null)
            {
                PlotPerformanceData(listView.Tag);
            }
        }

        /// <summary>
        /// Called when a group by check box is changed state. Updates the charts.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void GroupDataBy_CheckedChanged(object sender, EventArgs e)
        {
            PlotPerformanceData(detailedDataTabControl.SelectedTab.Tag);
        }

        /// <summary>
        /// Called when the checked state of the individual charts check box changes.
        /// Shows the main chart or the individual charts and shows or hides the
        /// copy to clipboard button.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void IndividualCharts_CheckedChanged(object sender, EventArgs e)
        {
            var individualCheckBox = sender as CheckBox;
            if (individualCheckBox == null) return;

            var tag = (string)individualCheckBox.Tag;

            var copyButton = GetCopyToClipboardButton(tag);
            var chart = GetControl<ChartSplitLegend>(tag);
            var charts = GetControl<TableOfCharts>(individualCheckBox.Tag + "s");

            ControlExt.ShowControls(trimmedChartIndividualCharts.Checked, charts);
            ControlExt.ShowControls(!trimmedChartIndividualCharts.Checked, chart, copyButton);

            DetailedChartLegendCheckBox_CheckedChanged(GetControl<CheckBox>(individualCheckBox.Tag + "LegendCheckBox"), e);
        }

        /// <summary>
        /// Gets a control of type T with the name equal to the tag string value.
        /// </summary>
        /// <typeparam name="T">The control type.</typeparam>
        /// <param name="tag">The tag used to specify the control name.</param>
        /// <returns>The control if it exists or null.</returns>
        private T GetControl<T>(object tag) where T : Control
        {
            var controls = Controls.Find(tag.ToString(), true);
            if (controls.Length <= 0) return null;

            return controls.OfType<T>().Select(control => control).FirstOrDefault();
        }

        /// <summary>
        /// Returns a list of all the charts.
        /// </summary>
        /// <param name="tag">The detailed charts type.</param>
        /// <returns>A list of all the charts.</returns>
        private IEnumerable<Chart> GetAllCharts(object tag)
        {
            var charts = ControlExt.GetControls<Chart>(tag.ToString());
            charts.Add(GetControl<Chart>(tag));

            return charts;
        }

        /// <summary>
        /// Returns the copy to clipboard button.
        /// </summary>
        /// <param name="tag">The detailed charts type.</param>
        /// <returns>The copy to clipboard button.</returns>
        private PictureBox GetCopyToClipboardButton(object tag)
        {
            return GetControl<PictureBox>("{0}_{1}".Args(tag.ToString(), "CopyToClipboard"));
        }

        /// <summary>
        /// Returns the individual charts check box.
        /// </summary>
        /// <param name="tag">The detailed charts type.</param>
        /// <returns>The individual charts check box.</returns>
        private CheckBox GetIndividualChartsCheckBox(object tag)
        {
            return GetControl<CheckBox>("{0}{1}".Args(tag.ToString(), "IndividualCharts"));
        }

        /// <summary>
        /// Returns the individual charts table layout control.
        /// </summary>
        /// <param name="tag">The detailed charts type.</param>
        /// <returns>The individual charts table layout control.</returns>
        private TableLayoutPanel GetTableLayoutPanel(object tag)
        {
            return GetControl<TableLayoutPanel>("{0}_{1}".Args(tag.ToString(), "TableLayout"));
        }

        #endregion

        #region *********************** Rig Alignment, Generic trend & X-Y charts ***********************

        // Rig alignment tab

        /// <summary>
        /// Populates rig alignment plant and line number list view.
        /// </summary>
        private void PopulateRigAlignmentPNandLineListView()
        {
            // Clear any existing plant or line numbers from list
            rigAlignmentLineSelectionListView.Items.Clear();

            // Check which type of test is to be plotted
            if (rigAlignmentCalibrationRadioButton.Checked)
            {
                // Check that there are some results to plot in complete calibration tests
                if (completeCalibrationData.DataTable.Rows.Count != 0)
                {
                    // Get distinct plant and line number combinations
                    var rows = completeCalibrationData.DistinctPlantLineNumberRows();

                    // Generate concatenated plant number and line number items in list view
                    foreach (var row in rows)
                    {
                        // Generate new ListViewItem
                        var item = new ListViewItem(row.PlantNumber + "_" + row.LineNumber) { Checked = true };
                        rigAlignmentLineSelectionListView.Items.Add(item);
                    }

                    // Order the list
                    rigAlignmentLineSelectionListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
                    rigAlignmentLineSelectionListView.Sort();
                }
            }

            // Check that there are some results to plot in complete pumping tests
            // NOT YET IN USE
        }

        /// <summary>
        /// Retrieves data for selected parameter and plots rig alignment chart.
        /// </summary>
        private void rigAlignmentParameterTreeView_AfterCheck(object sender, TreeViewEventArgs e)
        {
            // If checked, add new series to chart against selected x-Axis parameter
            if (e.Node.Checked)
            {
                rigAlignmentCharts.AddChart(
                    e.Node,
                    rigAlignmentParameterTreeView.AllNodes(),
                    rigAlignmentLineSelectionListView.CheckedItems.ToList(),
                    rigAlignmentChronologicalRadioButton.Checked,
                    RigAlignmentShowLegend.Checked,
                    completeCalibrationData,
                    currentDatabase.ConnectionString);

                RigAlignmentShowLegend.Visible = true;
            }
            else
            {
                rigAlignmentCharts.RemoveChart(e.Node, rigAlignmentParameterTreeView.AllNodes());
                RigAlignmentShowLegend.Visible = rigAlignmentCharts.Count > 0;
            }
        }

        /// <summary>
        /// Called when the chart type is changed. Updates the charts.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void rigAlignmentChartType_CheckedChanged(object sender, EventArgs e)
        {
            rigAlignmentCharts.UpdateCharts(rigAlignmentLineSelectionListView.CheckedItems.ToList(), rigAlignmentChronologicalRadioButton.Checked, RigAlignmentShowLegend.Checked);
        }

        /// <summary>
        /// Called when the line selection is changed. Updates the charts.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void rigAlignmentLineSelection_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            rigAlignmentCharts.UpdateCharts(rigAlignmentLineSelectionListView.CheckedItems.ToList(), rigAlignmentChronologicalRadioButton.Checked, RigAlignmentShowLegend.Checked);
        }

        /// <summary>
        /// Called when the show legend check box check state is changed. Updates the charts.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void RigAlignmentShowLegend_CheckedChanged(object sender, EventArgs e)
        {
            rigAlignmentCharts.UpdateCharts(rigAlignmentLineSelectionListView.CheckedItems.ToList(), rigAlignmentChronologicalRadioButton.Checked, RigAlignmentShowLegend.Checked);
        }

        // Generic Trend Chart

        /// <summary>
        /// Retrieves data for selected parameter(s) and plots generic trend chart.
        /// </summary>
        private void genericTrendChartTreeView_AfterCheck(object sender, TreeViewEventArgs e)
        {
            // If checked, add new series to chart against selected x-Axis parameter
            if (e.Node.Checked)
            {
                var tnX = new TreeNode();

                // Check if by date or plotChronological order for x points & create treenode
                if (genericTrendDateTimeRadioButton.Checked)
                {
                    tnX.Text = @"PDate";
                }
                else if (genericTrendChronologicalRadioButton.Checked)
                {
                    //tnX.Text = "Chronological";
                    tnX.Text = @"PDate";
                    MessageBox.Show(Resources.Chronological_option_not_yet_implemented);
                }

                var series = RetrieveTrendOrXYSeries(tnX, e.Node);
                genericTrendChart.ChartAreas[0].AxisX.Title = tnX.Text;
                genericTrendChart.Series.Add(series);
                genericTrendChart.ChartAreas[0].RecalculateAxesScale();
                genericTrendChart.ChartAreas[0].AxisX.RoundAxisValues();
                genericTrendChart.ChartAreas[0].AxisY.RoundAxisValues();
            }
            else
            {
                var series = genericTrendChart.Series[e.Node.FullPath];
                genericTrendChart.Series.Remove(series);
                genericTrendChart.ChartAreas[0].RecalculateAxesScale();
                genericTrendChart.ChartAreas[0].AxisX.RoundAxisValues();
                genericTrendChart.ChartAreas[0].AxisY.RoundAxisValues();
            }
        }

        // X-Y Chart

        /// <summary>
        /// Retrieves data for selected x-axis parameter and plots XY chart.
        /// </summary>
        private void xAxisXYChartTreeView_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Checked)
            {
                // Uncheck all nodes within tree which are not the one that's just been checked
                foreach (TreeNode tnodeTable in e.Node.TreeView.Nodes)
                {
                    foreach (TreeNode tnodeCol1 in tnodeTable.Nodes)
                    {
                        // If no child nodes, uncheck these
                        if (tnodeCol1.Nodes.Count == 0)
                        {
                            if (tnodeCol1 != e.Node)
                            {
                                tnodeCol1.Checked = false;
                            }
                        }
                        //// Else, go to child nodes
                        else
                        {
                            foreach (TreeNode tnodeCol2 in tnodeCol1.Nodes)
                            {
                                // If no child nodes, uncheck these
                                if (tnodeCol2.Nodes.Count == 0)
                                {
                                    if (tnodeCol2 != e.Node)
                                    {
                                        tnodeCol2.Checked = false;
                                    }
                                }
                                //// Else, go to child nodes
                                else
                                {
                                    foreach (var tnodeCol3 in tnodeCol2.Nodes.Cast<TreeNode>().Where(tnodeCol3 => tnodeCol3 != e.Node))
                                    {
                                        tnodeCol3.Checked = false;
                                    }
                                }
                            }
                        }
                    }
                }

                // Enable y-axis treeview
                yAxisXYChartTreeView.Enabled = true;

                // If y-axis treeview has any selected nodes, generate series data
                foreach (TreeNode tnodeTable in yAxisXYChartTreeView.Nodes)
                {
                    foreach (TreeNode tnodeCol1 in tnodeTable.Nodes)
                    {
                        // If no child nodes, generate series for checked node
                        if (tnodeCol1.Nodes.Count == 0)
                        {
                            if (!tnodeCol1.Checked) continue;

                            var series = RetrieveTrendOrXYSeries(e.Node, tnodeCol1);
                            xyChart.ChartAreas[0].AxisX.Title = e.Node.FullPath;
                            xyChart.Series.Add(series);
                            xyChart.ChartAreas[0].RecalculateAxesScale();
                            xyChart.ChartAreas[0].AxisX.RoundAxisValues();
                            xyChart.ChartAreas[0].AxisY.RoundAxisValues();
                        }
                        //// Else go to child nodes
                        else
                        {
                            foreach (TreeNode tnodeCol2 in tnodeCol1.Nodes)
                            {
                                // If no child nodes, generate series for checked node
                                if (tnodeCol2.Nodes.Count == 0)
                                {
                                    if (!tnodeCol2.Checked) continue;

                                    var series = RetrieveTrendOrXYSeries(e.Node, tnodeCol2);
                                    xyChart.ChartAreas[0].AxisX.Title = e.Node.FullPath;
                                    xyChart.Series.Add(series);
                                    xyChart.ChartAreas[0].RecalculateAxesScale();
                                    xyChart.ChartAreas[0].AxisX.RoundAxisValues();
                                    xyChart.ChartAreas[0].AxisY.RoundAxisValues();
                                }
                                //// Else go to child nodes
                                else
                                {
                                    foreach (TreeNode tnodeCol3 in tnodeCol2.Nodes)
                                    {
                                        if (!tnodeCol3.Checked) continue;

                                        var series = RetrieveTrendOrXYSeries(e.Node, tnodeCol3);
                                        xyChart.ChartAreas[0].AxisX.Title = e.Node.FullPath;
                                        xyChart.Series.Add(series);
                                        xyChart.ChartAreas[0].RecalculateAxesScale();
                                        xyChart.ChartAreas[0].AxisX.RoundAxisValues();
                                        xyChart.ChartAreas[0].AxisY.RoundAxisValues();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // If unchecked, then none will be checked, so disable y-axis treeview and clear chart
            else
            {
                // Clear chart
                xyChart.Series.Clear();

                // Disable y-axis chart
                yAxisXYChartTreeView.Enabled = false;
            }
        }

        /// <summary>
        /// Retrieves data for selected y-axis parameter and plots XY chart.
        /// </summary>
        private void yAxisXYChartTreeView_AfterCheck(object sender, TreeViewEventArgs e)
        {
            // If checked, add new series to chart against selected x-Axis parameter
            if (e.Node.Checked)
            {
                // If x-axis treeview has any selected nodes, generate series data
                foreach (TreeNode tnodeTable in xAxisXYChartTreeView.Nodes)
                {
                    foreach (TreeNode tnodeCol1 in tnodeTable.Nodes)
                    {
                        // If no child nodes, generate series for checked node
                        if (tnodeCol1.Nodes.Count == 0)
                        {
                            if (!tnodeCol1.Checked) continue;

                            var series = RetrieveTrendOrXYSeries(tnodeCol1, e.Node);
                            xyChart.ChartAreas[0].AxisX.Title = tnodeCol1.FullPath;
                            xyChart.Series.Add(series);
                            xyChart.ChartAreas[0].RecalculateAxesScale();
                            xyChart.ChartAreas[0].AxisX.RoundAxisValues();
                            xyChart.ChartAreas[0].AxisY.RoundAxisValues();
                        }
                        //// Else go to child nodes
                        else
                        {
                            foreach (TreeNode tnodeCol2 in tnodeCol1.Nodes)
                            {
                                // If no child nodes, generate series for checked node
                                if (tnodeCol2.Nodes.Count == 0)
                                {
                                    if (!tnodeCol2.Checked) continue;

                                    var series = RetrieveTrendOrXYSeries(tnodeCol2, e.Node);
                                    xyChart.ChartAreas[0].AxisX.Title = tnodeCol2.FullPath;
                                    xyChart.Series.Add(series);
                                    xyChart.ChartAreas[0].RecalculateAxesScale();
                                    xyChart.ChartAreas[0].AxisX.RoundAxisValues();
                                    xyChart.ChartAreas[0].AxisY.RoundAxisValues();
                                }
                                //// Else go to child nodes
                                else
                                {
                                    foreach (TreeNode tnodeCol3 in tnodeCol2.Nodes)
                                    {
                                        if (!tnodeCol3.Checked) continue;

                                        var series = RetrieveTrendOrXYSeries(tnodeCol3, e.Node);
                                        xyChart.ChartAreas[0].AxisX.Title = tnodeCol3.FullPath;
                                        xyChart.Series.Add(series);
                                        xyChart.ChartAreas[0].RecalculateAxesScale();
                                        xyChart.ChartAreas[0].AxisX.RoundAxisValues();
                                        xyChart.ChartAreas[0].AxisY.RoundAxisValues();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                var series = xyChart.Series[e.Node.FullPath];
                xyChart.Series.Remove(series);
                xyChart.ChartAreas[0].RecalculateAxesScale();
                xyChart.ChartAreas[0].AxisX.RoundAxisValues();
                xyChart.ChartAreas[0].AxisY.RoundAxisValues();
            }
        }

        // Shared methods for rig alignment, trend and x-y charts

        /// <summary>
        /// Retrieves distinct parameters for all completed tests
        /// and runs methods to generate TreeViews.
        /// </summary>
        private void PopulateParameterTreeViews()
        {
            // Clear charts & treeviews
            ClearPreviousRigAlignmentAndTrendAndXYData();

            // Check for results
            if (completeCalibrationData.DataTable.Rows.Count > 0)
            {
                /* CALIBRATION TEST */

                UTIDPlantNumberKeys keys = UTIDPlantNumberKeys.GetKeys(completeCalibrationData.DataTable);

                var tree = new Tree<string>("root");
                var trimsBranch = tree.AddChild("Trims");
                foreach (var row in new Trims.QueryUniqueNames(currentDatabase, keys).Execute())
                {
                    trimsBranch.AddChild(row.Name);
                }
                Tree<string> gradesBranch = tree.AddChild("Grades");
                foreach (var row in new Grades.QueryUniqueNames(currentDatabase, keys).Execute())
                {
                    gradesBranch.AddChild(row.Name);
                }
                Tree<string> openLoopBranch = tree.AddChild("OpenLoop");
                foreach (var row in new NCV.QueryUniquePressureLevelsDurationsNames(currentDatabase, keys).Execute())
                {
                    openLoopBranch.AddChild(row.PressureReq).AddChild(row.Duration).AddChild(row.Name);
                }
                Tree<string> closedLoopBranch = tree.AddChild("ClosedLoop");
                foreach (var row in new CLT.QueryUniquePressureLevelsFuelDemandsNames(currentDatabase, keys).Execute())
                {
                    closedLoopBranch.AddChild(row.PressureReq).AddChild(row.FuelDemand).AddChild(row.Name);
                }
                if (mdegDB01RadioButton.Checked)
                {
                    Tree<string> trimmedBranch = tree.AddChild("Trimmed");
                    foreach (var row in new Trimmed.QueryUniquePressureLevelsNominalFuelsNames(currentDatabase, keys).Execute())
                    {
                        trimmedBranch.AddChild(row.PressureReq).AddChild(row.NominalFuel).AddChild(row.Name);
                    }
                }

                // TODO: Add pumping query here and return to a dsPumping dataset

                // TODO: Separate this and save as a public treeview node collection for pumping and non-pumping to save future re-querying
                // Rig alignment treeview
                if (rigAlignmentCalibrationRadioButton.Checked)
                {
                    // Update with new nodes
                    PopulateParameterTreeViewsNodes(tree, rigAlignmentParameterTreeView);
                }
                else if (rigAlignmentCalibrationRadioButton.Checked)
                {
                    MessageBox.Show(Resources.Not_yet_implemented);
                }

                // Generic trend chart treeview
                if (genericTrendCalibrationRadioButton.Checked)
                {
                    // Update with new nodes
                    PopulateParameterTreeViewsNodes(tree, genericTrendChartTreeView);
                }
                else if (genericTrendPumpingRadioButton.Checked)
                {
                    MessageBox.Show(Resources.Not_yet_implemented);
                }

                // XY chart treeview
                if (xyCalibrationRadioButton.Checked)
                {
                    // Update with new nodes
                    PopulateParameterTreeViewsNodes(tree, xAxisXYChartTreeView);
                    PopulateParameterTreeViewsNodes(tree, yAxisXYChartTreeView);
                }
                else if (xyPumpingRadioButton.Checked)
                {
                    MessageBox.Show(Resources.Not_yet_implemented);
                }

                // Allow zooming/panning
                genericTrendChart.InvokeIfRequired(c =>
                {
                    genericTrendChart.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
                    genericTrendChart.ChartAreas[0].CursorY.IsUserSelectionEnabled = true;
                });
                xyChart.InvokeIfRequired(c =>
                {
                    xyChart.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
                    xyChart.ChartAreas[0].CursorY.IsUserSelectionEnabled = true;
                });
            }
        }

        /// <summary>
        /// Clears any previous data from the rig alignment, Generic trend and XY tabs
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void ClearPreviousRigAlignmentAndTrendAndXYData()
        {
            // Clear treeviews, listviews and charts
            rigAlignmentCharts.Clear();
            rigAlignmentParameterTreeView.InvokeIfRequired(c => c.Nodes.Clear());

            genericTrendChart.InvokeIfRequired(c => c.Series.Clear());
            genericTrendChartTreeView.InvokeIfRequired(c => c.Nodes.Clear());

            xyChart.InvokeIfRequired(c => c.Series.Clear());
            xAxisXYChartTreeView.InvokeIfRequired(c => c.Nodes.Clear());
            yAxisXYChartTreeView.InvokeIfRequired(c => c.Nodes.Clear());
        }

        /// <summary>
        /// Generates TreeViews for distinct parameters, sorting by pressure level and test point.
        /// </summary>
        /// <param name="tree">The tree data used to generate the TreeView.</param>
        /// <param name="tv">TreeView to be populated.</param>
        private void PopulateParameterTreeViewsNodes(Tree<string> tree, TreeView tv)
        {
            tv.InvokeIfRequired(c =>
            {
                tv.BeginUpdate();

                // Loop through tables within dataset
                foreach (var tableNode in tree.Branches)
                {
                    // Name base treenode and add
                    var tnodeTable = new TreeNode(tableNode.Value);
                    tv.Nodes.Add(tnodeTable);

                    foreach (var col1Node in tableNode.Branches)
                    {
                        // Add nodes for first column
                        var tnodeFirstCol = new TreeNode(col1Node.Value);
                        tnodeTable.Nodes.Add(tnodeFirstCol);

                        if (col1Node.Branches.Count <= 0) continue;

                        // If second column exists, get distinct values for it, filtered for current first row
                        foreach (var col2Node in col1Node.Branches)
                        {
                            // Add nodes for second column
                            var tnodeSecondCol = new TreeNode(col2Node.Value);
                            tnodeFirstCol.Nodes.Add(tnodeSecondCol);

                            if (col2Node.Branches.Count <= 0) continue;

                            foreach (var col3Node in col2Node.Branches)
                            {
                                // Add nodes for third column
                                var tnodeThirdCol = new TreeNode(col3Node.Value);
                                tnodeSecondCol.Nodes.Add(tnodeThirdCol);
                            }

                            // Hide second column node checkboxes
                            var hideCheckBoxCol2 = new HideCheckBoxOnTreeView();
                            hideCheckBoxCol2.HideCheckBox(tv, tnodeSecondCol);
                        }

                        // Hide first column node checkboxes
                        var hideCheckBoxCol1 = new HideCheckBoxOnTreeView();
                        hideCheckBoxCol1.HideCheckBox(tv, tnodeFirstCol);
                    }
                }

                // Hide all table node checkboxes (done in a loop here otherwise leaves first node checkbox)
                foreach (TreeNode tn in tv.Nodes)
                {
                    var hideCheckBoxTable = new HideCheckBoxOnTreeView();
                    hideCheckBoxTable.HideCheckBox(tv, tn);
                }

                tv.EndUpdate();
                tv.Refresh();
            });
        }

        /// <summary>
        /// Generates and runs full query to generate XY data series for the selected TreeNodes.
        /// </summary>
        /// <param name="xTreeNode">"X" axis selected TreeNode.</param>
        /// <param name="yTreeNode">"Y" axis selected TreeNode.</param>
        /// <returns>XY chart series for selected TreeNodes.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation",
                         Justification = "Variables xTreeNode, yTreeNode, xQuery and yQuery are X and Y axis specific.")]
        // ReSharper disable once InconsistentNaming
        private Series RetrieveTrendOrXYSeries(TreeNode xTreeNode, TreeNode yTreeNode)
        {
            var keys = UTIDPlantNumberKeys.GetKeys(completeCalibrationData.DataTable);

            TreeNode topXTreeNode = xTreeNode.TopNode();
            TreeNode topYTreeNode = yTreeNode.TopNode();

            // Call routine to return correct query section for x and y
            Queries.XYQueries.IXYQuery xQuery = Queries.XYQueries.XYQuery.GetAlignmentOrTrendOrXYQuery(xTreeNode, topXTreeNode, "X");
            Queries.XYQueries.IXYQuery yQuery = Queries.XYQueries.XYQuery.GetAlignmentOrTrendOrXYQuery(yTreeNode, topYTreeNode, "Y");
            var trendQuery = new Queries.XYQueries.TrendXYQuery();

            var dtMedianXy = new DataTable();

            try
            {
                var da = new SqlDataAdapter(trendQuery.GetQuery(topXTreeNode.Text, keys, xQuery, yQuery), currentDatabase.ConnectionString);
                da.Fill(dtMedianXy);
            }
            catch (SqlException exception)
            {
                MessageBox.Show(exception.Message);
            }

            var colourIndex = 0;

            // Find out which treeview is being used so count series from correct chart
            if (yTreeNode.TreeView == rigAlignmentParameterTreeView)
            {
                // Count number of series on chart
                colourIndex = 0; // rigAlignmentChart.Series.Count;
            }
            else if (yTreeNode.TreeView == genericTrendChartTreeView)
            {
                // Count number of series on chart
                colourIndex = genericTrendChart.Series.Count;
            }
            else if (yTreeNode.TreeView == yAxisXYChartTreeView)
            {
                // Count number of series on chart
                colourIndex = xyChart.Series.Count;
            }

            var series = new Series(yTreeNode.FullPath)
            {
                ChartType = SeriesChartType.Point,
                MarkerStyle = MarkerStyle.Circle,
                MarkerSize = 4,
                MarkerColor = SeriesColour.BetterColours(colourIndex),
                Color = SeriesColour.BetterColours(colourIndex)
            };

            // Add x and y points to series
            foreach (DataRow dr in dtMedianXy.Rows)
            {
                series.Points.AddXY(dr["X"], dr["Y"]);
            }

            // Return new data series
            return series;
        }

        /// <summary>
        /// Captures test type selection change and re-populates TreeViews.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void rigAlignmentOrtrendOrXYTestTypeChanged(object sender, EventArgs e)
        {
            // Clear chart and repopulate the treeviews
            PopulateParameterTreeViews();
        }

        #endregion

        #region *********************** Trend chart ***********************

        /// <summary>
        /// Populates UTID ListView with UTIDs from incomplete tests.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void PopulateTrendChartUTIDList(IEnumerable<IncompleteRow> uniquetestIDsIncompleteCalibration, IEnumerable<IncompleteRow> uniquetestIDsIncompletePumping)
        {
            // Add unique test IDs to ListView
            foreach (var id in uniquetestIDsIncompleteCalibration)
            {
                trendutidsListView.Items.Add(ListViewExt.CreateListViewItem(
                    null,
                    id.UTID.ToString(CultureInfo.InvariantCulture),
                    id.PlantNumber.ToString(CultureInfo.InvariantCulture),
                    id.ISN,
                    id.PDate.ToString("dd-MMM-yyyy HH:mm:ss"),
                    "C",
                    id.Reason));
            }

            // Add unique test IDs to ListView
            foreach (var id in uniquetestIDsIncompletePumping)
            {
                trendutidsListView.Items.Add(ListViewExt.CreateListViewItem(
                    null,
                    id.UTID.ToString(CultureInfo.InvariantCulture),
                    id.PlantNumber.ToString(CultureInfo.InvariantCulture),
                    id.ISN,
                    id.PDate.ToString("dd-MMM-yyyy HH:mm:ss"),
                    "P",
                    id.Reason));
            }

            // Sort by utid desc
            trendutidsListView.Sorting = System.Windows.Forms.SortOrder.Descending;
            trendutidsListView.Sort();

            // Resize columns to fit data
            trendutidsListView.Columns[5].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
        }

        /// <summary>
        /// Captures injector selection in ListView and runs method to retrieve data and generate chart.
        /// </summary>
        private void trendutidsListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            trendChart.Series.Clear();
            trendChartDataGridView.Rows.Clear();
            trendChartDataGridView.Columns.Clear();

            if (trendutidsListView.SelectedItems.Count > 0)
            {
                // Get UTID and Plant Number
                var item = trendutidsListView.SelectedItems[0];

                var query = new Trend.Query(currentDatabase, item.Text, item.SubItems[1].Text);
                var trend = query.Execute();

                // Plot trend chart if there is any data
                if (trend.Any())
                {
                    PlotTrendChart(trend, item.Text);
                }
            }
        }

        /// <summary>
        /// Runs parse BLOB method for current trend/UTID and plots on trend chart.
        /// </summary>
        private void PlotTrendChart(IList<Trend.Row> trend, string utids)
        {
            // Get trace for selected row
            var selectedTrace = trend[0].Trace;

            // Parse Trace
            ChartBLOB blob = new ChartBLOB(selectedTrace, utids, "");

            // Add all series for current datarow
            if (blob.Count != 0)
            {
                trendChartDataGridView.Columns.Add("Time", "Time");

                foreach (var s in blob.Series)
                {
                    trendChart.Series.Add(s);

                    if (s.Points.Count() == 1)
                    {
                        // Shorten legend name for point series so doesn't include UTID & URID
                        var param = s.Name.Split(' ');
                        s.Name = param[2];
                        s.Legend = "legendLineSeries";
                    }
                    //// All other series are line series, so put these in RHS legend
                    else
                    {
                        s.Legend = "legendLineSeries";
                    }

                    // Populate datagrid
                    // Add rows and time data if this is the first series
                    if (trendChartDataGridView.Rows.Count == 0)
                    {
                        trendChartDataGridView.Rows.Add(s.Points.Count());
                        int rowIndex = 0;

                        foreach (var dp in s.Points)
                        {
                            // Fill in time for this latest row
                            trendChartDataGridView.Rows[rowIndex].Cells["Time"].Value = dp.XValue;
                            rowIndex++;
                        }
                    }

                    trendChartDataGridView.Columns.Add(s.Name, s.Name);

                    foreach (var dp in s.Points)
                    {
                        // Fill in current series data - but make sure the xvalue matches the xvalue in the "Time" column
                        var rowIndex = (from DataGridViewRow row in trendChartDataGridView.Rows
                                        where row.Cells["Time"].Value.ToString().Equals(dp.XValue.ToString(CultureInfo.InvariantCulture))
                                        select row.Index).DefaultIfEmpty(-1).FirstOrDefault();

                        if (rowIndex != -1) // So there is a match to the time setting
                        {
                            trendChartDataGridView.Rows[rowIndex].Cells[s.Name].Value = dp.YValues[0];
                        }
                        else  // Must be a new X value, so add it and yvalue data in this row
                        {
                            var newRow = (DataGridViewRow)trendChartDataGridView.Rows[0].Clone();
                            if (newRow == null) continue;

                            // ReSharper disable PossibleNullReferenceException
                            newRow.Cells[trendChartDataGridView.Columns["Time"].Index].Value = dp.XValue;
                            newRow.Cells[trendChartDataGridView.Columns[s.Name].Index].Value = dp.YValues[0];
                            // ReSharper restore PossibleNullReferenceException
                            trendChartDataGridView.Rows.Add(newRow);
                        }
                    }
                }

                // re-order datagrid by time
                // ReSharper disable once AssignNullToNotNullAttribute
                trendChartDataGridView.Sort(trendChartDataGridView.Columns["Time"], ListSortDirection.Ascending);

                // Re-calculate axis
                trendChart.ChartAreas[0].RecalculateAxesScale();
            }
        }

        /// <summary>
        /// Clears trend chart list, DataGridView and chart of any existing data.
        /// </summary>
        private void ClearPreviousTrendChartData()
        {
            trendutidsListView.Items.Clear();
            trendChart.Series.Clear();
            trendChartDataGridView.Rows.Clear();
            trendChartDataGridView.Columns.Clear();
        }

        #endregion

        #region *********************** Injector Components ***********************

        /// <summary>
        /// Called when the checked state of the individual charts check box changes.
        /// Shows the main chart or the individual charts and shows or hides the
        /// copy to clipboard button.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void UpdateInjectorComponentsDataButton_Click(object sender, EventArgs e)
        {
            if (injectorComponentsDatabase == null) return;

            var injectorSerialNumbers = (from DataGridViewRow row in completedCalibrationDataGridView.SelectedRows select row.Cells["ISN"].Value.ToString()).ToList();
            injectorSerialNumbers.AddRange(from DataGridViewRow row in completedPumpingDataGridView.SelectedRows select row.Cells["ISN"].Value.ToString());
            injectorSerialNumbers.AddRange(from DataGridViewRow row in incompleteCalibrationDataGridView.SelectedRows select row.Cells["ISN"].Value.ToString());
            injectorSerialNumbers.AddRange(from DataGridViewRow row in incompletePumpingDataGridView.SelectedRows select row.Cells["ISN"].Value.ToString());

            selectedComponentsLabel.Text = "{0} Selected".Args(injectorSerialNumbers.Count);

            using (new WaitCursor())
            {
                int injectorsMIA = 0;
                if (currentDatabase.ProductType == DatabaseProductType.MDEG)
                {
                    #region MDEG Stuff
                    var query = new MDEGInjectorComponentData.Query(injectorComponentsDatabase, StartDate, EndDate, injectorSerialNumbers);
                    var latestRow = new Dictionary<string, DateTime>();
                    var rows = query.Execute();

                    if (rows.Any(x => x.NCVClearance == 0d || x.NCVLift == 0d))
                    {
                        var sa1Query = new MDEGSA1ComponentData.Query(new SA1Database(), rows.Where(x => x.NCVLift == 0d || x.NCVClearance == 0d).Select(x => x.NCVSerial));
                        var sa1Rows = sa1Query.Execute();

                        foreach (var row in sa1Rows)
                        {
                            var r = rows.Where(x => x.NCVSerial == row.Serial).Single();

                            r.NeedleLift = row.Lift;
                            r.NCVClearance = row.Clearance;
                        }
                    }

                    injectorsMIA = injectorSerialNumbers.Distinct().Count() - rows.Distinct().Count();
                    ComponentMissingISNS = injectorSerialNumbers.Select(s => s.Right(9))
                        .Except(rows.Select(r => r.InjectorSerial.Right(9))).ToList();

                    // Get the latest date for all injectors
                    foreach (var row in rows)
                    {
                        DateTime currentPDate;
                        if (!latestRow.TryGetValue(row.InjectorSerial, out currentPDate) || (currentPDate < row.AssemblyDate))
                        {
                            latestRow[row.InjectorSerial] = row.AssemblyDate;
                        }
                    }

                    InjectorComponentsGridView.SetColumns(
                       "Assembly Date",
                       "Injector Serial",
                       "Nozzle Serial",
                       "NCV Serial",
                       "Piston Guide Serial",
                       "Nozzle Count",
                       "NCV Count",
                       "PG Count",
                       "Fag",
                       "Tilt",
                       "Needle In Flow",
                       "Needle Out Flow",
                       "Needle Lift",
                       "NCV Clearance",
                       "NCV  Lift",
                       "RdO",
                       "InO",
                       "RD Flow",
                       "INO Flow",
                       "SPO");

                    // Iterate through all the latest rows for each injector
                    foreach (var row in rows.Where(row => latestRow[row.InjectorSerial] == row.AssemblyDate))
                    {
                        InjectorComponentsGridView.AddRow(
                            row.AssemblyDate,
                            row.InjectorSerial,
                            row.NozzleSerial,
                            row.NCVSerial,
                            row.PistonGuideSerial,
                            row.NozzleAssemblyCount,
                            row.NCVAssemblyCount,
                            row.PGAssemblyCount,
                            row.Fag,
                            row.Tilt,
                            row.NeedleInFlow,
                            row.NeedleOutFlow,
                            row.NeedleLift,
                            row.NCVClearance,
                            row.NCVLift,
                            row.RdO,
                            row.InO,
                            row.RDFlow,
                            row.INOFlow,
                            row.SPO);
                    }
                    #endregion
                }
                else
                {
                    #region DAF Stuff
                    //DAF
                    QueryBase<InjectorComponentData.Row> query = new InjectorComponentData.Query(injectorComponentsDatabase, StartDate, EndDate, injectorSerialNumbers);
                    var latestRow = new Dictionary<string, DateTime>();
                    var rows = query.Execute();

                    injectorsMIA = injectorSerialNumbers.Distinct().Count() - rows.Distinct().Count();
                    ComponentMissingISNS = injectorSerialNumbers.Select(s => s.Right(9))
                        .Except(rows.Select(r => r.InjectorSerial.Right(9))).ToList();

                    // Get the latest date for all injectors
                    foreach (var row in rows)
                    {
                        DateTime currentPDate;
                        if (!latestRow.TryGetValue(row.InjectorSerial, out currentPDate) || (currentPDate < row.AssemblyDate))
                        {
                            latestRow[row.InjectorSerial] = row.AssemblyDate;
                        }
                    }

                    InjectorComponentsGridView.SetColumns(
                       "Assembly Date",
                       "Injector Serial",
                       "Nozzle Serial",
                       "NCV Serial",
                       "Piston Guide Serial",
                       "Nozzle Count",
                       "NCV Count",
                       "PG Count",
                       "Fag",
                       "Tilt",
                       "Needle In Flow",
                       "Needle Out Flow",
                       "Needle Lift",
                       "NCV Clearance",
                       "NCV  Lift",
                       "RdO",
                       "InO");

                    // Iterate through all the latest rows for each injector
                    foreach (var row in rows.Where(row => latestRow[row.InjectorSerial] == row.AssemblyDate))
                    {
                        InjectorComponentsGridView.AddRow(
                            row.AssemblyDate,
                            row.InjectorSerial,
                            row.NozzleSerial,
                            row.NCVSerial,
                            row.PistonGuideSerial,
                            row.NozzleAssemblyCount,
                            row.NCVAssemblyCount,
                            row.PGAssemblyCount,
                            row.Fag,
                            row.Tilt,
                            row.NeedleInFlow,
                            row.NeedleOutFlow,
                            row.NeedleLift,
                            row.NCVClearance,
                            row.NCVLift,
                            row.RdO,
                            row.InO);
                    }

                    #endregion
                }

                //Set MIA Text
                missingComponentsLabel.Text = "{0} missing".Args(injectorsMIA);
                missingComponentsLabel.Visible = injectorsMIA > 0;

                if (injectorsMIA > 0)
                {
                    //Show some informationa bout out missing injectors
                    //Only if we need to.
                    // 9 chars from the right removes the line number 
                    MissingInjectorInformation infoBox = new MissingInjectorInformation(ComponentMissingISNS);
                    infoBox.Show();
                }

                InjectorComponentsGridView.RowsComplete();
            }
        }

        /// <summary>
        /// Called when the missing components label is clicked,
        ///  Used to show the user a list of ISNs which have been missed from the Components Query
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void missingComponentsLabel_Click(object sender, EventArgs e)
        {
            if (ComponentMissingISNS.Count <= 0)
                return;

            MissingInjectorInformation infoBox = new MissingInjectorInformation(ComponentMissingISNS);
            infoBox.Show();
        }

        #endregion

        #region *********************** Miscellaneous ***********************

        /// <summary>
        /// Converts selected chart information to a memory stream and runs BigChart to generate a new
        /// separate window cloning the chart.
        /// </summary>
        private void ChartMouseDblClick(object sender, MouseEventArgs e)
        {
            // Get chart that was clicked
            var currentChart = sender as Chart;
            if (currentChart == null) return;

            currentChart.OpenInBigChart();
        }

        /// <summary>
        /// Sets series colour for chart trace charts, grouping series by user selection (e.g. Pass/fail, LOP, ISN etc.).
        /// </summary>
        /// <param name="series">Series to be formatted.</param>
        /// <param name="utidIndex">UTID for current series.</param>
        /// <param name="testPointIndex">Test point index - not the actual test point number.</param>
        /// <param name="lopIndex">LOP index - not the actual LOP number.</param>
        /// <param name="identIndex">IDENT index - not the actual IDENT.</param>
        /// <param name="testPlanIndex">Test plan index - not the actual Test plan.</param>
        /// <param name="isnIndex">ISN index - not the actual ISN.</param>
        /// <param name="lineIndex">Line index</param>
        /// <param name="lop">LOP for current series.</param>
        /// <param name="ident">IDENT for current series.</param>
        /// <param name="testPlan">Test plan for current series.</param>
        /// <param name="isn">ISN for current series.</param>
        /// <param name="line">The Line </param>
        /// <param name="global">Global pass/fail flag.</param>
        private void SetChartTraceChartSeriesColours(
            Series series,
            int utidIndex,
            int testPointIndex,
            int lopIndex,
            int identIndex,
            int testPlanIndex,
            int isnIndex,
            int lineIndex,
            string lop,
            string ident,
            string testPlan,
            string isn,
            int line,
            int global)
        {
            // Set format & add new series to chart
            var seriesColour = new Color();

            // Default is by UTID
            if (traceUTIDRadioButton.Checked)
            {
                seriesColour = SeriesColour.BetterColours(utidIndex);
            }

            // Options to split series colour in different ways - also add to series name grouping if required.
            if (tracePassFailRadioButton.Checked)
            {
                seriesColour = SeriesColour.PassFailCustomPalette(global);
            }

            if (traceTestPointRadioButton.Checked)
            {
                seriesColour = SeriesColour.BetterColours(testPointIndex);
            }

            if (traceLOPRadioButton.Checked)
            {
                seriesColour = SeriesColour.BetterColours(lopIndex);
                series.Name += " (" + lop + ")";
            }

            if (traceIDENTRadioButton.Checked)
            {
                seriesColour = SeriesColour.BetterColours(identIndex);
                series.Name += " (" + ident + ")";
            }

            if (traceISNRadioButton.Checked)
            {
                seriesColour = SeriesColour.BetterColours(isnIndex);
                series.Name += " (" + isn + ")";
            }

            if (traceTestplanRadioButton.Checked)
            {
                seriesColour = SeriesColour.BetterColours(testPlanIndex);
                series.Name += " (" + testPlan + ")";
            }

            if (traceLineNumberRadioButton.Checked)
            {
                seriesColour = SeriesColour.BetterColours(lineIndex);
                series.Name += " (" + line.ToString() + ")";
            }

            series.Color = seriesColour;
        }

        #endregion

        /// <summary>
        /// Checks for Automatic Updates
        /// </summary>
        private void CheckForUpdates()
        {
            UpdateManager manager = UpdateManager.Instance;
            manager.Config.TempFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System).Substring(0, 3), "Temp\\Delphi");
            manager.CleanUp();

            manager.UpdateSource = new TeamCityUpdateSource(
                "Test_TestDataAnalyser_TestDataAnalyser",
                "Setup/SetupDataAnalyser.msi");

            manager.UpdateFeedReader = new TeamCityFeedReader(true);

            manager.Config.UpdateExecutableName = null;
            manager.ReinstateIfRestarted();

            manager.CheckForUpdateAsync(updateAvaliable =>
            {
                if (updateAvaliable)
                {
                    var result = MessageBox.Show("An Update is avaliable, would you like to apply?", "Update Avaliable", MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        manager.PrepareUpdatesAsync(b =>
                        {
                            if (!b)
                            {
                                MessageBox.Show("An error occured downloading the update. Please try again", "Oops!", MessageBoxButtons.OK);
                                return;
                            }

                            result = MessageBox.Show("Update has been downloaded and is ready to be applied.", "Applying Update", MessageBoxButtons.OK);
                            UpdateManager.Instance.ApplyUpdates(true);
                        });
                    }
                    else
                        return;
                }
            });
        }

        /// <summary>
        /// Called when the View change log button is clicked. Displays the change log dialog (modal).
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void viewChangelogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ChangelogReport().ShowDialog();
        }

        /// <summary>
        /// Update button
        /// </summary>
        /// <param name="sender">On click</param>
        /// <param name="e">On click</param>
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //dawid
            GetDatabase();
          //  UpdateListDataGridView(UpdateType.UpdateLopList);
        }

        /// <summary>
        /// GetData for Process Flow
        /// </summary>
        /// <param name="selectCommand"></param>
        private void GetData(string selectCommand, BindingSource varBindingSource, String connectionString)
        {
            try
            {
                
                   
                // Specify a connection string.
                // Replace <SQL Server> with the SQL Server for your Northwind sample database.
                // Replace "Integrated Security=True" with user login information if necessary.
               // String connectionString =
                 //   Properties.Settings.Default.Euro6ConnectionString;

                // Create a new data adapter based on the specified query.
                dataAdapter = new SqlDataAdapter(selectCommand, connectionString);
                
                // Create a command builder to generate SQL update, insert, and
                // delete commands based on selectCommand.
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);

                // Populate a new data table and bind it to the BindingSource.
             
                DataTable table = new DataTable
                {
                    Locale = CultureInfo.InvariantCulture
                };
                dataAdapter.Fill(table);
                varBindingSource.DataSource = table;

                // Resize the DataGridView columns to fit the newly loaded content.
                dgvProcessData.AutoResizeColumns(
                    DataGridViewAutoSizeColumnsMode.DisplayedCells);
            }
            catch (SqlException)
            {
                MessageBox.Show("Process Data: Error in connecting to Database");
            }
        }

        /// <summary>
        /// Button click - refresh data with input
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnProcessData_MouseClick(object sender, MouseEventArgs e)
        {
            
            GetData("EXEC	[Euro6].[dbo].[sp_ProcessFlow_test]	@SerialNumber ='" + this.tbProcessDataSerial.Text + "'",bindingSource1, Properties.Settings.Default.Euro6ConnectionString);
            dataAdapter.Update((DataTable)bindingSource1.DataSource);

            int count = 0;

            foreach (DataGridViewRow row in dgvProcessData.Rows)
            {
                
                String errorCode = Convert.ToString(row.Cells[4].Value);
                
                if (errorCode != "PASS")
                {
                    dgvProcessData[4, count].Style.BackColor = Color.Red;//to color the row
                  

                    //dgvProcessData[0, count].ReadOnly = true;//qty should not be enter for 0 inventory                       
                } 
                else 
                {
                    dgvProcessData[4, count].Style.BackColor = Color.Green;//to color the row

                    //dgvProcessData[0, count].ReadOnly = true;
                }
                //dgvProcessData[0, count].Value = "0";//assign a default value to quantity enter
                count++;
            }

        }

        /// <summary>
        /// Packing Data search
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPackingData_MouseClick(object sender, MouseEventArgs e)
        {
            String injectorSerials = "";

            foreach (string s in tbPackingData.Lines)
            {
                injectorSerials += "('" + s + "'),";
            }

            injectorSerials = injectorSerials.Left(injectorSerials.Length - 1);

            //MessageBox.Show(injectorSerials);
            // GetData("SELECT * FROM [SmartPack].[dbo].[vw_InjectorTraceability] WHERE Serial IN (" + injectorSerials + ")", bindingSource2);
            GetData("SELECT DISTINCT isNull(s.Serial,v.Serial) as [Serial],[Time],[Crate ID],[Sap Serial],[Trim Code],[Body Type],[User] FROM [SmartPack].[dbo].[vw_InjectorTraceability] s right join  (values " + injectorSerials + ") v(Serial) on v.Serial = s.Serial or v.Serial = s.[Sap Serial] or v.Serial= s.[Crate ID] ", bindingSource2, Properties.Settings.Default.Euro6ConnectionString);

            dataAdapter.Update((DataTable)bindingSource2.DataSource);

            lPackingDataOutput.Visible = false;
            int count = 0;
            int missingDataCount = 0;

            foreach (DataGridViewRow row in dgvProcessDataPack.Rows)
            {

                String Time = Convert.ToString(row.Cells[1].Value);

                if (Time == "" )
                {
                    dgvProcessDataPack[0, count].Style.BackColor = Color.Red;//to color the row

                    missingDataCount++;

                    //dgvProcessData[0, count].ReadOnly = true;//qty should not be enter for 0 inventory                       
                }
                else
                {
                    dgvProcessDataPack[0, count].Style.BackColor = Color.Green;//to color the row

                    //dgvProcessData[0, count].ReadOnly = true;
                }
                //dgvProcessData[0, count].Value = "0";//assign a default value to quantity enter
                count++;
            }

            if (missingDataCount > 0)
            {
                lPackingDataOutput.Text = "Missing Data for " + missingDataCount + " injector/s.";
                lPackingDataOutput.Visible = true;
            }
        }

        private void Export2Excel(DataGridView dgv)
        {
            // creating Excel Application  
            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
            // creating new WorkBook within Excel application  
            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
            // creating new Excelsheet in workbook  
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
            // see the excel sheet behind the program  
            app.Visible = true;
            // get the reference of first sheet. By default its name is Sheet1.  
            // store its reference to worksheet  
            worksheet = workbook.Sheets["Sheet1"];
            worksheet = workbook.ActiveSheet;
            // changing the name of active sheet  
            worksheet.Name = "Exported from gridview";
            // storing header part in Excel  
            for (int i = 1; i < dgv.Columns.Count + 1; i++)
            {
                worksheet.Cells[1, i] = dgv.Columns[i - 1].HeaderText;
            }
            // storing Each row and column value to excel sheet  
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                for (int j = 0; j < dgv.Columns.Count; j++)
                {
                    try
                    {
                        worksheet.Cells[i + 2, j + 1] = dgv.Rows[i].Cells[j].Value.ToString();
                    }
                    catch (Exception e)
                    {
                        break;
                    }
                }
            }
        }

        private void PackingDataExcel_Click(object sender, EventArgs e)
        {
            Export2Excel(dgvProcessDataPack);
        }

        private void ProcessDataExcel_Click(object sender, EventArgs e)
        {
            Export2Excel(dgvProcessData);
        }

        private void btnDaimlerISN_Click(object sender, EventArgs e)
        {
            String injectorSerials = "";

            foreach (string s in tbDaimlerISN.Lines)
            {
                injectorSerials += "('" + s + "'),";
            }

            injectorSerials = injectorSerials.Left(injectorSerials.Length - 1);

            //MessageBox.Show(injectorSerials);
            // GetData("SELECT * FROM [SmartPack].[dbo].[vw_InjectorTraceability] WHERE Serial IN (" + injectorSerials + ")", bindingSource2);
            GetData(@"SELECT DISTINCT isNull(v.Serial,[Daimler ISN]) as [Daimler ISN],[PlantNumber] ,[UTID]      ,[Delphi_ISN] ,[TDate] , MAX([Location]) as Location from ( 
                    SELECT  Daimler_ISN as [Daimler ISN],[PlantNumber]      ,[UTID]      ,[Delphi_ISN] ,[TDate], 'Primary' as Location FROM [Euro6MDEGTest].[dbo].[vw_TestInjectorStatus]
                    UNION ALL SELECT  Daimler_ISN as [Daimler ISN],[PlantNumber]      ,[UTID]      ,[Delphi_ISN] ,[TDate], 'Archive' as Location FROM [ROIAS-LDB03\ISArchive].[Euro6MDEGTest].[dbo].[vw_TestInjectorStatus]
                    UNION ALL SELECT  Daimler_ISN as [Daimler ISN],[PlantNumber]      ,[UTID]      ,[Delphi_ISN] ,[TDate],'Sudbury Archive' as Location FROM [ROIAS-LDB03\Archive].[Euro6MDEGTest].[dbo].[vw_TestInjectorStatus]
					) as  s right join  (values " + injectorSerials + ") v(Serial) on v.Serial = s.[Daimler ISN]" +
                    " GROUP BY  isNull(v.Serial,[Daimler ISN]),[PlantNumber] ,[UTID],[Delphi_ISN] ,[TDate]", bindingSource3, Properties.Settings.Default.MDEGIasiConnectionString);
           
            dataAdapter.Update((DataTable)bindingSource3.DataSource);

            lDaimlerISN.Visible = false;
            int count = 0;
            int missingDataCount = 0;

            foreach (DataGridViewRow row in dgvDaimlerISN.Rows)
            {

                String Time = Convert.ToString(row.Cells[1].Value);

                if (Time == "")
                {
                    dgvDaimlerISN[0, count].Style.BackColor = Color.Red;//to color the row

                    missingDataCount++;

                    //dgvProcessData[0, count].ReadOnly = true;//qty should not be enter for 0 inventory                       
                }
                else
                {
                    dgvDaimlerISN[0, count].Style.BackColor = Color.Green;//to color the row

                    //dgvProcessData[0, count].ReadOnly = true;
                }
                //dgvProcessData[0, count].Value = "0";//assign a default value to quantity enter
                count++;
            }

            if (missingDataCount > 0)
            {
                lDaimlerISN.Text = "Missing Data for " + missingDataCount + " injector/s.";
                lDaimlerISN.Visible = true;
            }
        }

        private void btnDaimlerISNExcel_Click(object sender, EventArgs e)
        {
            Export2Excel(dgvDaimlerISN);
        }
    }
}