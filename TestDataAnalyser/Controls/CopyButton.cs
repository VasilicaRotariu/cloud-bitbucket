﻿using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// The Copy button class, based on the picture box button.
    /// </summary>
    public class CopyButton : PictureBoxButton
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="CopyButton"/> class.
        /// </summary>
        public CopyButton()
            : base(null, Properties.Resources.Copy)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CopyButton"/> class.
        /// </summary>
        /// <param name="parent">The buttons parent.</param>
        public CopyButton(Control parent)
            : base(parent, Properties.Resources.Copy)
        {
        }

        /// <summary>
        /// Copies a chart to the clipboard. Adds a legend if required.
        /// </summary>
        protected override void OnMouseClick()
        {
            var tag = Tag as string;

            var chart = ControlExt.GetControl<Chart>(tag);
            if (chart != null)
            {
                Legend legend = null;

                var legendCheckBox = ControlExt.GetControl<CheckBox>(tag + "LegendCheckBox");
                if (legendCheckBox != null)
                {
                    if (legendCheckBox.Checked)
                    {
                        legend = chart.AddLegendIfAbsent();
                    }
                }

                chart.CopyToClipboard();

                chart.RemoveLegend(legend);
            }
            else
            {
                var chartScrollableLegend = ControlExt.GetControl<ChartSplitLegend>(tag);
                if (chartScrollableLegend != null)
                {
                    chartScrollableLegend.CopyToClipboard();
                }
            }
        }
    }
}
