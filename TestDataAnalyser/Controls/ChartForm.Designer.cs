﻿namespace SudburyDataAnalyser
{
    partial class ChartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChartForm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.chartHideLegendCheckBox = new System.Windows.Forms.CheckBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.axisEditorTableView = new System.Windows.Forms.TableLayoutPanel();
            this.closeEditorButton = new System.Windows.Forms.Button();
            this.axisEditorGroupBox = new System.Windows.Forms.GroupBox();
            this.x2AxisGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.x2TickMinIntTextBox = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.x2TickMajIntTextBox = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.x2GridMinIntTextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.x2GridMajIntTextBox = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.x2AxisIntTextBox = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.x2AxisMaxTextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.x2AxisMinTextBox = new System.Windows.Forms.TextBox();
            this.x2AutoScaleCheckBox = new System.Windows.Forms.CheckBox();
            this.y2AxisGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.y2AxisIntTextBox = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.y2AxisMinTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.y2AxisMaxTextBox = new System.Windows.Forms.TextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.y2GridMajIntTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.y2GridMinIntTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.y2TickMinIntTextBox = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.y2TickMajIntTextBox = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.y2AutoScaleCheckBox = new System.Windows.Forms.CheckBox();
            this.y1AxisGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.y1GridMajIntTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.y1GridMinIntTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.y1AxisIntTextBox = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.y1AxisMaxTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.y1AxisMinTextBox = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.y1TickMinIntTextBox = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.y1TickMajIntTextBox = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.y1AutoScaleCheckBox = new System.Windows.Forms.CheckBox();
            this.x1AxisGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.x1TickMinIntTextBox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.x1TickMajIntTextBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.x1AxisIntTextBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.x1AxisMinTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.x1AxisMaxTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.x1GridMinIntTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.x1GridMajIntTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.x1AutoScaleCheckBox = new System.Windows.Forms.CheckBox();
            this.chartExportPowerPoint = new SudburyDataAnalyser.PowerPointButton();
            this.ChartLegend = new SudburyDataAnalyser.ChartSplitLegend();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.axisEditorTableView.SuspendLayout();
            this.axisEditorGroupBox.SuspendLayout();
            this.x2AxisGroupBox.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.y2AxisGroupBox.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.y1AxisGroupBox.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.x1AxisGroupBox.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartExportPowerPoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChartLegend)).BeginInit();
            this.ChartLegend.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.chartExportPowerPoint, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.chartHideLegendCheckBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.splitContainer1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(984, 732);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // chartHideLegendCheckBox
            // 
            this.chartHideLegendCheckBox.AutoSize = true;
            this.chartHideLegendCheckBox.Dock = System.Windows.Forms.DockStyle.Right;
            this.chartHideLegendCheckBox.Location = new System.Drawing.Point(894, 3);
            this.chartHideLegendCheckBox.Name = "chartHideLegendCheckBox";
            this.chartHideLegendCheckBox.Size = new System.Drawing.Size(87, 18);
            this.chartHideLegendCheckBox.TabIndex = 1;
            this.chartHideLegendCheckBox.Text = "Hide Legend";
            this.chartHideLegendCheckBox.UseVisualStyleBackColor = true;
            this.chartHideLegendCheckBox.CheckedChanged += new System.EventHandler(this.chartHideLegendCheckBox_CheckedChanged);
            // 
            // splitContainer1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.splitContainer1, 2);
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 27);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ChartLegend);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.axisEditorTableView);
            this.splitContainer1.Size = new System.Drawing.Size(978, 702);
            this.splitContainer1.SplitterDistance = 622;
            this.splitContainer1.TabIndex = 2;
            // 
            // axisEditorTableView
            // 
            this.axisEditorTableView.ColumnCount = 1;
            this.axisEditorTableView.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.axisEditorTableView.Controls.Add(this.closeEditorButton, 0, 1);
            this.axisEditorTableView.Controls.Add(this.axisEditorGroupBox, 0, 0);
            this.axisEditorTableView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axisEditorTableView.Location = new System.Drawing.Point(0, 0);
            this.axisEditorTableView.Name = "axisEditorTableView";
            this.axisEditorTableView.RowCount = 2;
            this.axisEditorTableView.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.axisEditorTableView.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.axisEditorTableView.Size = new System.Drawing.Size(352, 702);
            this.axisEditorTableView.TabIndex = 0;
            // 
            // closeEditorButton
            // 
            this.closeEditorButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.closeEditorButton.Location = new System.Drawing.Point(3, 636);
            this.closeEditorButton.Name = "closeEditorButton";
            this.closeEditorButton.Size = new System.Drawing.Size(346, 63);
            this.closeEditorButton.TabIndex = 5;
            this.closeEditorButton.Text = "Close editor";
            this.closeEditorButton.UseVisualStyleBackColor = true;
            this.closeEditorButton.Click += new System.EventHandler(this.closeEditorButton_Click);
            // 
            // axisEditorGroupBox
            // 
            this.axisEditorGroupBox.Controls.Add(this.x2AxisGroupBox);
            this.axisEditorGroupBox.Controls.Add(this.y2AxisGroupBox);
            this.axisEditorGroupBox.Controls.Add(this.y1AxisGroupBox);
            this.axisEditorGroupBox.Controls.Add(this.x1AxisGroupBox);
            this.axisEditorGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axisEditorGroupBox.Location = new System.Drawing.Point(3, 3);
            this.axisEditorGroupBox.Name = "axisEditorGroupBox";
            this.axisEditorGroupBox.Size = new System.Drawing.Size(346, 627);
            this.axisEditorGroupBox.TabIndex = 4;
            this.axisEditorGroupBox.TabStop = false;
            this.axisEditorGroupBox.Text = "Axis Editor";
            // 
            // x2AxisGroupBox
            // 
            this.x2AxisGroupBox.Controls.Add(this.groupBox6);
            this.x2AxisGroupBox.Controls.Add(this.groupBox5);
            this.x2AxisGroupBox.Controls.Add(this.groupBox4);
            this.x2AxisGroupBox.Controls.Add(this.x2AutoScaleCheckBox);
            this.x2AxisGroupBox.Location = new System.Drawing.Point(176, 23);
            this.x2AxisGroupBox.Name = "x2AxisGroupBox";
            this.x2AxisGroupBox.Size = new System.Drawing.Size(164, 299);
            this.x2AxisGroupBox.TabIndex = 5;
            this.x2AxisGroupBox.TabStop = false;
            this.x2AxisGroupBox.Text = "Secondary X axis";
            this.x2AxisGroupBox.Visible = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.x2TickMinIntTextBox);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.x2TickMajIntTextBox);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Location = new System.Drawing.Point(7, 217);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(151, 73);
            this.groupBox6.TabIndex = 12;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Tick marks";
            // 
            // x2TickMinIntTextBox
            // 
            this.x2TickMinIntTextBox.Enabled = false;
            this.x2TickMinIntTextBox.Location = new System.Drawing.Point(84, 44);
            this.x2TickMinIntTextBox.Name = "x2TickMinIntTextBox";
            this.x2TickMinIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.x2TickMinIntTextBox.TabIndex = 8;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(5, 24);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(73, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "Major interval:";
            // 
            // x2TickMajIntTextBox
            // 
            this.x2TickMajIntTextBox.Enabled = false;
            this.x2TickMajIntTextBox.Location = new System.Drawing.Point(84, 21);
            this.x2TickMajIntTextBox.Name = "x2TickMajIntTextBox";
            this.x2TickMajIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.x2TickMajIntTextBox.TabIndex = 7;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(5, 47);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(73, 13);
            this.label21.TabIndex = 4;
            this.label21.Text = "Minor interval:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.x2GridMinIntTextBox);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.x2GridMajIntTextBox);
            this.groupBox5.Location = new System.Drawing.Point(6, 138);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(152, 73);
            this.groupBox5.TabIndex = 10;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Grid";
            // 
            // x2GridMinIntTextBox
            // 
            this.x2GridMinIntTextBox.Enabled = false;
            this.x2GridMinIntTextBox.Location = new System.Drawing.Point(85, 44);
            this.x2GridMinIntTextBox.Name = "x2GridMinIntTextBox";
            this.x2GridMinIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.x2GridMinIntTextBox.TabIndex = 8;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 24);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Major interval:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "Minor interval:";
            // 
            // x2GridMajIntTextBox
            // 
            this.x2GridMajIntTextBox.Enabled = false;
            this.x2GridMajIntTextBox.Location = new System.Drawing.Point(85, 21);
            this.x2GridMajIntTextBox.Name = "x2GridMajIntTextBox";
            this.x2GridMajIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.x2GridMajIntTextBox.TabIndex = 7;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.x2AxisIntTextBox);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this.x2AxisMaxTextBox);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.x2AxisMinTextBox);
            this.groupBox4.Location = new System.Drawing.Point(6, 43);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(152, 92);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Axis range";
            // 
            // x2AxisIntTextBox
            // 
            this.x2AxisIntTextBox.Enabled = false;
            this.x2AxisIntTextBox.Location = new System.Drawing.Point(85, 63);
            this.x2AxisIntTextBox.Name = "x2AxisIntTextBox";
            this.x2AxisIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.x2AxisIntTextBox.TabIndex = 10;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 66);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(42, 13);
            this.label26.TabIndex = 9;
            this.label26.Text = "Interval";
            // 
            // x2AxisMaxTextBox
            // 
            this.x2AxisMaxTextBox.Enabled = false;
            this.x2AxisMaxTextBox.Location = new System.Drawing.Point(85, 41);
            this.x2AxisMaxTextBox.Name = "x2AxisMaxTextBox";
            this.x2AxisMaxTextBox.Size = new System.Drawing.Size(61, 20);
            this.x2AxisMaxTextBox.TabIndex = 6;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(27, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Min:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 44);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(30, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Max:";
            // 
            // x2AxisMinTextBox
            // 
            this.x2AxisMinTextBox.Enabled = false;
            this.x2AxisMinTextBox.Location = new System.Drawing.Point(85, 19);
            this.x2AxisMinTextBox.Name = "x2AxisMinTextBox";
            this.x2AxisMinTextBox.Size = new System.Drawing.Size(61, 20);
            this.x2AxisMinTextBox.TabIndex = 5;
            // 
            // x2AutoScaleCheckBox
            // 
            this.x2AutoScaleCheckBox.AutoSize = true;
            this.x2AutoScaleCheckBox.Checked = true;
            this.x2AutoScaleCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.x2AutoScaleCheckBox.Location = new System.Drawing.Point(7, 20);
            this.x2AutoScaleCheckBox.Name = "x2AutoScaleCheckBox";
            this.x2AutoScaleCheckBox.Size = new System.Drawing.Size(78, 17);
            this.x2AutoScaleCheckBox.TabIndex = 0;
            this.x2AutoScaleCheckBox.Text = "Auto Scale";
            this.x2AutoScaleCheckBox.UseVisualStyleBackColor = true;
            this.x2AutoScaleCheckBox.CheckedChanged += new System.EventHandler(this.x2AutoScaleCheckBox_CheckedChanged);
            // 
            // y2AxisGroupBox
            // 
            this.y2AxisGroupBox.Controls.Add(this.groupBox12);
            this.y2AxisGroupBox.Controls.Add(this.groupBox10);
            this.y2AxisGroupBox.Controls.Add(this.groupBox8);
            this.y2AxisGroupBox.Controls.Add(this.y2AutoScaleCheckBox);
            this.y2AxisGroupBox.Location = new System.Drawing.Point(176, 328);
            this.y2AxisGroupBox.Name = "y2AxisGroupBox";
            this.y2AxisGroupBox.Size = new System.Drawing.Size(164, 299);
            this.y2AxisGroupBox.TabIndex = 4;
            this.y2AxisGroupBox.TabStop = false;
            this.y2AxisGroupBox.Text = "Secondary Y axis";
            this.y2AxisGroupBox.Visible = false;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.y2AxisIntTextBox);
            this.groupBox12.Controls.Add(this.label28);
            this.groupBox12.Controls.Add(this.y2AxisMinTextBox);
            this.groupBox12.Controls.Add(this.label12);
            this.groupBox12.Controls.Add(this.label10);
            this.groupBox12.Controls.Add(this.y2AxisMaxTextBox);
            this.groupBox12.Location = new System.Drawing.Point(7, 43);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(152, 92);
            this.groupBox12.TabIndex = 14;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Axis range";
            // 
            // y2AxisIntTextBox
            // 
            this.y2AxisIntTextBox.Enabled = false;
            this.y2AxisIntTextBox.Location = new System.Drawing.Point(84, 62);
            this.y2AxisIntTextBox.Name = "y2AxisIntTextBox";
            this.y2AxisIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.y2AxisIntTextBox.TabIndex = 12;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(5, 65);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(42, 13);
            this.label28.TabIndex = 11;
            this.label28.Text = "Interval";
            // 
            // y2AxisMinTextBox
            // 
            this.y2AxisMinTextBox.Enabled = false;
            this.y2AxisMinTextBox.Location = new System.Drawing.Point(84, 18);
            this.y2AxisMinTextBox.Name = "y2AxisMinTextBox";
            this.y2AxisMinTextBox.Size = new System.Drawing.Size(61, 20);
            this.y2AxisMinTextBox.TabIndex = 5;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(5, 21);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Min:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 43);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Max:";
            // 
            // y2AxisMaxTextBox
            // 
            this.y2AxisMaxTextBox.Enabled = false;
            this.y2AxisMaxTextBox.Location = new System.Drawing.Point(84, 40);
            this.y2AxisMaxTextBox.Name = "y2AxisMaxTextBox";
            this.y2AxisMaxTextBox.Size = new System.Drawing.Size(61, 20);
            this.y2AxisMaxTextBox.TabIndex = 6;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.y2GridMajIntTextBox);
            this.groupBox10.Controls.Add(this.label11);
            this.groupBox10.Controls.Add(this.y2GridMinIntTextBox);
            this.groupBox10.Controls.Add(this.label9);
            this.groupBox10.Location = new System.Drawing.Point(7, 141);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(151, 73);
            this.groupBox10.TabIndex = 13;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Grid";
            // 
            // y2GridMajIntTextBox
            // 
            this.y2GridMajIntTextBox.Enabled = false;
            this.y2GridMajIntTextBox.Location = new System.Drawing.Point(84, 19);
            this.y2GridMajIntTextBox.Name = "y2GridMajIntTextBox";
            this.y2GridMajIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.y2GridMajIntTextBox.TabIndex = 7;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Major interval:";
            // 
            // y2GridMinIntTextBox
            // 
            this.y2GridMinIntTextBox.Enabled = false;
            this.y2GridMinIntTextBox.Location = new System.Drawing.Point(84, 42);
            this.y2GridMinIntTextBox.Name = "y2GridMinIntTextBox";
            this.y2GridMinIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.y2GridMinIntTextBox.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 45);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Minor interval:";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.y2TickMinIntTextBox);
            this.groupBox8.Controls.Add(this.label24);
            this.groupBox8.Controls.Add(this.y2TickMajIntTextBox);
            this.groupBox8.Controls.Add(this.label25);
            this.groupBox8.Location = new System.Drawing.Point(6, 226);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(151, 73);
            this.groupBox8.TabIndex = 12;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Tick marks";
            // 
            // y2TickMinIntTextBox
            // 
            this.y2TickMinIntTextBox.Enabled = false;
            this.y2TickMinIntTextBox.Location = new System.Drawing.Point(84, 44);
            this.y2TickMinIntTextBox.Name = "y2TickMinIntTextBox";
            this.y2TickMinIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.y2TickMinIntTextBox.TabIndex = 8;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(5, 24);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(73, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Major interval:";
            // 
            // y2TickMajIntTextBox
            // 
            this.y2TickMajIntTextBox.Enabled = false;
            this.y2TickMajIntTextBox.Location = new System.Drawing.Point(84, 21);
            this.y2TickMajIntTextBox.Name = "y2TickMajIntTextBox";
            this.y2TickMajIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.y2TickMajIntTextBox.TabIndex = 7;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(5, 47);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(73, 13);
            this.label25.TabIndex = 4;
            this.label25.Text = "Minor interval:";
            // 
            // y2AutoScaleCheckBox
            // 
            this.y2AutoScaleCheckBox.AutoSize = true;
            this.y2AutoScaleCheckBox.Checked = true;
            this.y2AutoScaleCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.y2AutoScaleCheckBox.Location = new System.Drawing.Point(7, 20);
            this.y2AutoScaleCheckBox.Name = "y2AutoScaleCheckBox";
            this.y2AutoScaleCheckBox.Size = new System.Drawing.Size(78, 17);
            this.y2AutoScaleCheckBox.TabIndex = 0;
            this.y2AutoScaleCheckBox.Text = "Auto Scale";
            this.y2AutoScaleCheckBox.UseVisualStyleBackColor = true;
            this.y2AutoScaleCheckBox.CheckedChanged += new System.EventHandler(this.y2AutoScaleCheckBox_CheckedChanged);
            // 
            // y1AxisGroupBox
            // 
            this.y1AxisGroupBox.Controls.Add(this.groupBox11);
            this.y1AxisGroupBox.Controls.Add(this.groupBox9);
            this.y1AxisGroupBox.Controls.Add(this.groupBox7);
            this.y1AxisGroupBox.Controls.Add(this.y1AutoScaleCheckBox);
            this.y1AxisGroupBox.Location = new System.Drawing.Point(6, 328);
            this.y1AxisGroupBox.Name = "y1AxisGroupBox";
            this.y1AxisGroupBox.Size = new System.Drawing.Size(164, 299);
            this.y1AxisGroupBox.TabIndex = 3;
            this.y1AxisGroupBox.TabStop = false;
            this.y1AxisGroupBox.Text = "Primary Y axis";
            this.y1AxisGroupBox.Visible = false;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.y1GridMajIntTextBox);
            this.groupBox11.Controls.Add(this.label7);
            this.groupBox11.Controls.Add(this.y1GridMinIntTextBox);
            this.groupBox11.Controls.Add(this.label5);
            this.groupBox11.Location = new System.Drawing.Point(6, 141);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(151, 73);
            this.groupBox11.TabIndex = 14;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Grid";
            // 
            // y1GridMajIntTextBox
            // 
            this.y1GridMajIntTextBox.Enabled = false;
            this.y1GridMajIntTextBox.Location = new System.Drawing.Point(83, 19);
            this.y1GridMajIntTextBox.Name = "y1GridMajIntTextBox";
            this.y1GridMajIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.y1GridMajIntTextBox.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Major interval:";
            // 
            // y1GridMinIntTextBox
            // 
            this.y1GridMinIntTextBox.Enabled = false;
            this.y1GridMinIntTextBox.Location = new System.Drawing.Point(83, 42);
            this.y1GridMinIntTextBox.Name = "y1GridMinIntTextBox";
            this.y1GridMinIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.y1GridMinIntTextBox.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Minor interval:";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.y1AxisIntTextBox);
            this.groupBox9.Controls.Add(this.label27);
            this.groupBox9.Controls.Add(this.y1AxisMaxTextBox);
            this.groupBox9.Controls.Add(this.label8);
            this.groupBox9.Controls.Add(this.label6);
            this.groupBox9.Controls.Add(this.y1AxisMinTextBox);
            this.groupBox9.Location = new System.Drawing.Point(6, 43);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(152, 92);
            this.groupBox9.TabIndex = 13;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Axis range";
            // 
            // y1AxisIntTextBox
            // 
            this.y1AxisIntTextBox.Enabled = false;
            this.y1AxisIntTextBox.Location = new System.Drawing.Point(84, 62);
            this.y1AxisIntTextBox.Name = "y1AxisIntTextBox";
            this.y1AxisIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.y1AxisIntTextBox.TabIndex = 10;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(5, 65);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(42, 13);
            this.label27.TabIndex = 9;
            this.label27.Text = "Interval";
            // 
            // y1AxisMaxTextBox
            // 
            this.y1AxisMaxTextBox.Enabled = false;
            this.y1AxisMaxTextBox.Location = new System.Drawing.Point(84, 40);
            this.y1AxisMaxTextBox.Name = "y1AxisMaxTextBox";
            this.y1AxisMaxTextBox.Size = new System.Drawing.Size(61, 20);
            this.y1AxisMaxTextBox.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Min:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Max:";
            // 
            // y1AxisMinTextBox
            // 
            this.y1AxisMinTextBox.Enabled = false;
            this.y1AxisMinTextBox.Location = new System.Drawing.Point(84, 18);
            this.y1AxisMinTextBox.Name = "y1AxisMinTextBox";
            this.y1AxisMinTextBox.Size = new System.Drawing.Size(61, 20);
            this.y1AxisMinTextBox.TabIndex = 5;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.y1TickMinIntTextBox);
            this.groupBox7.Controls.Add(this.label22);
            this.groupBox7.Controls.Add(this.y1TickMajIntTextBox);
            this.groupBox7.Controls.Add(this.label23);
            this.groupBox7.Location = new System.Drawing.Point(6, 226);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(151, 73);
            this.groupBox7.TabIndex = 12;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Tick marks";
            // 
            // y1TickMinIntTextBox
            // 
            this.y1TickMinIntTextBox.Enabled = false;
            this.y1TickMinIntTextBox.Location = new System.Drawing.Point(84, 44);
            this.y1TickMinIntTextBox.Name = "y1TickMinIntTextBox";
            this.y1TickMinIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.y1TickMinIntTextBox.TabIndex = 8;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(5, 24);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(73, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "Major interval:";
            // 
            // y1TickMajIntTextBox
            // 
            this.y1TickMajIntTextBox.Enabled = false;
            this.y1TickMajIntTextBox.Location = new System.Drawing.Point(84, 21);
            this.y1TickMajIntTextBox.Name = "y1TickMajIntTextBox";
            this.y1TickMajIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.y1TickMajIntTextBox.TabIndex = 7;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(5, 47);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(73, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "Minor interval:";
            // 
            // y1AutoScaleCheckBox
            // 
            this.y1AutoScaleCheckBox.AutoSize = true;
            this.y1AutoScaleCheckBox.Checked = true;
            this.y1AutoScaleCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.y1AutoScaleCheckBox.Location = new System.Drawing.Point(7, 20);
            this.y1AutoScaleCheckBox.Name = "y1AutoScaleCheckBox";
            this.y1AutoScaleCheckBox.Size = new System.Drawing.Size(78, 17);
            this.y1AutoScaleCheckBox.TabIndex = 0;
            this.y1AutoScaleCheckBox.Text = "Auto Scale";
            this.y1AutoScaleCheckBox.UseVisualStyleBackColor = true;
            this.y1AutoScaleCheckBox.CheckedChanged += new System.EventHandler(this.y1AutoScaleCheckBox_CheckedChanged);
            // 
            // x1AxisGroupBox
            // 
            this.x1AxisGroupBox.Controls.Add(this.groupBox3);
            this.x1AxisGroupBox.Controls.Add(this.groupBox2);
            this.x1AxisGroupBox.Controls.Add(this.groupBox1);
            this.x1AxisGroupBox.Controls.Add(this.x1AutoScaleCheckBox);
            this.x1AxisGroupBox.Location = new System.Drawing.Point(6, 23);
            this.x1AxisGroupBox.Name = "x1AxisGroupBox";
            this.x1AxisGroupBox.Size = new System.Drawing.Size(164, 299);
            this.x1AxisGroupBox.TabIndex = 2;
            this.x1AxisGroupBox.TabStop = false;
            this.x1AxisGroupBox.Text = "Primary X axis";
            this.x1AxisGroupBox.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.x1TickMinIntTextBox);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.x1TickMajIntTextBox);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Location = new System.Drawing.Point(6, 217);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(151, 73);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tick marks";
            // 
            // x1TickMinIntTextBox
            // 
            this.x1TickMinIntTextBox.Enabled = false;
            this.x1TickMinIntTextBox.Location = new System.Drawing.Point(84, 44);
            this.x1TickMinIntTextBox.Name = "x1TickMinIntTextBox";
            this.x1TickMinIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.x1TickMinIntTextBox.TabIndex = 8;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(5, 24);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Major interval:";
            // 
            // x1TickMajIntTextBox
            // 
            this.x1TickMajIntTextBox.Enabled = false;
            this.x1TickMajIntTextBox.Location = new System.Drawing.Point(84, 21);
            this.x1TickMajIntTextBox.Name = "x1TickMajIntTextBox";
            this.x1TickMajIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.x1TickMajIntTextBox.TabIndex = 7;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(5, 47);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(73, 13);
            this.label19.TabIndex = 4;
            this.label19.Text = "Minor interval:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.x1AxisIntTextBox);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.x1AxisMinTextBox);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.x1AxisMaxTextBox);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(6, 43);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(152, 92);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Axis range";
            // 
            // x1AxisIntTextBox
            // 
            this.x1AxisIntTextBox.Enabled = false;
            this.x1AxisIntTextBox.Location = new System.Drawing.Point(83, 63);
            this.x1AxisIntTextBox.Name = "x1AxisIntTextBox";
            this.x1AxisIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.x1AxisIntTextBox.TabIndex = 8;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(4, 66);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(42, 13);
            this.label17.TabIndex = 7;
            this.label17.Text = "Interval";
            // 
            // x1AxisMinTextBox
            // 
            this.x1AxisMinTextBox.Enabled = false;
            this.x1AxisMinTextBox.Location = new System.Drawing.Point(83, 19);
            this.x1AxisMinTextBox.Name = "x1AxisMinTextBox";
            this.x1AxisMinTextBox.Size = new System.Drawing.Size(61, 20);
            this.x1AxisMinTextBox.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Min:";
            // 
            // x1AxisMaxTextBox
            // 
            this.x1AxisMaxTextBox.Enabled = false;
            this.x1AxisMaxTextBox.Location = new System.Drawing.Point(83, 41);
            this.x1AxisMaxTextBox.Name = "x1AxisMaxTextBox";
            this.x1AxisMaxTextBox.Size = new System.Drawing.Size(61, 20);
            this.x1AxisMaxTextBox.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Max:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.x1GridMinIntTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.x1GridMajIntTextBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(6, 138);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(151, 73);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Grid";
            // 
            // x1GridMinIntTextBox
            // 
            this.x1GridMinIntTextBox.Enabled = false;
            this.x1GridMinIntTextBox.Location = new System.Drawing.Point(84, 44);
            this.x1GridMinIntTextBox.Name = "x1GridMinIntTextBox";
            this.x1GridMinIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.x1GridMinIntTextBox.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Major interval:";
            // 
            // x1GridMajIntTextBox
            // 
            this.x1GridMajIntTextBox.Enabled = false;
            this.x1GridMajIntTextBox.Location = new System.Drawing.Point(84, 21);
            this.x1GridMajIntTextBox.Name = "x1GridMajIntTextBox";
            this.x1GridMajIntTextBox.Size = new System.Drawing.Size(61, 20);
            this.x1GridMajIntTextBox.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Minor interval:";
            // 
            // x1AutoScaleCheckBox
            // 
            this.x1AutoScaleCheckBox.AutoSize = true;
            this.x1AutoScaleCheckBox.Checked = true;
            this.x1AutoScaleCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.x1AutoScaleCheckBox.Location = new System.Drawing.Point(7, 20);
            this.x1AutoScaleCheckBox.Name = "x1AutoScaleCheckBox";
            this.x1AutoScaleCheckBox.Size = new System.Drawing.Size(78, 17);
            this.x1AutoScaleCheckBox.TabIndex = 0;
            this.x1AutoScaleCheckBox.Text = "Auto Scale";
            this.x1AutoScaleCheckBox.UseVisualStyleBackColor = true;
            this.x1AutoScaleCheckBox.CheckedChanged += new System.EventHandler(this.x1AutoScaleCheckBox_CheckedChanged);
            // 
            // chartExportPowerPoint
            // 
            this.chartExportPowerPoint.Dock = System.Windows.Forms.DockStyle.Left;
            this.chartExportPowerPoint.Image = ((System.Drawing.Image)(resources.GetObject("chartExportPowerPoint.Image")));
            this.chartExportPowerPoint.Location = new System.Drawing.Point(3, 3);
            this.chartExportPowerPoint.Name = "chartExportPowerPoint";
            this.chartExportPowerPoint.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.chartExportPowerPoint.Size = new System.Drawing.Size(18, 18);
            this.chartExportPowerPoint.TabIndex = 0;
            this.chartExportPowerPoint.TabStop = false;
            this.chartExportPowerPoint.Click += new System.EventHandler(this.chartExportPowerPoint_Click);
            // 
            // ChartLegend
            // 
            this.ChartLegend.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChartLegend.Location = new System.Drawing.Point(0, 0);
            this.ChartLegend.Name = "ChartLegend";
            this.ChartLegend.ShowLegend = true;
            this.ChartLegend.Size = new System.Drawing.Size(622, 702);
            this.ChartLegend.SplitterDistance = 417;
            this.ChartLegend.TabIndex = 0;
            this.ChartLegend.Title = "";
            // 
            // ChartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 732);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimizeBox = false;
            this.Name = "ChartForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Chart";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.axisEditorTableView.ResumeLayout(false);
            this.axisEditorGroupBox.ResumeLayout(false);
            this.x2AxisGroupBox.ResumeLayout(false);
            this.x2AxisGroupBox.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.y2AxisGroupBox.ResumeLayout(false);
            this.y2AxisGroupBox.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.y1AxisGroupBox.ResumeLayout(false);
            this.y1AxisGroupBox.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.x1AxisGroupBox.ResumeLayout(false);
            this.x1AxisGroupBox.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartExportPowerPoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChartLegend)).EndInit();
            this.ChartLegend.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private PowerPointButton chartExportPowerPoint;
        private System.Windows.Forms.CheckBox chartHideLegendCheckBox;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TableLayoutPanel axisEditorTableView;
        private System.Windows.Forms.Button closeEditorButton;
        private System.Windows.Forms.GroupBox axisEditorGroupBox;
        private System.Windows.Forms.GroupBox x2AxisGroupBox;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox x2TickMinIntTextBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox x2TickMajIntTextBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox x2GridMinIntTextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox x2GridMajIntTextBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox x2AxisIntTextBox;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox x2AxisMaxTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox x2AxisMinTextBox;
        private System.Windows.Forms.CheckBox x2AutoScaleCheckBox;
        private System.Windows.Forms.GroupBox y2AxisGroupBox;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.TextBox y2AxisIntTextBox;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox y2AxisMinTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox y2AxisMaxTextBox;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox y2GridMajIntTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox y2GridMinIntTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox y2TickMinIntTextBox;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox y2TickMajIntTextBox;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.CheckBox y2AutoScaleCheckBox;
        private System.Windows.Forms.GroupBox y1AxisGroupBox;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox y1GridMajIntTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox y1GridMinIntTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox y1AxisIntTextBox;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox y1AxisMaxTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox y1AxisMinTextBox;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox y1TickMinIntTextBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox y1TickMajIntTextBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.CheckBox y1AutoScaleCheckBox;
        private System.Windows.Forms.GroupBox x1AxisGroupBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox x1TickMinIntTextBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox x1TickMajIntTextBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox x1AxisIntTextBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox x1AxisMinTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox x1AxisMaxTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox x1GridMinIntTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox x1GridMajIntTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox x1AutoScaleCheckBox;
        private ChartSplitLegend ChartLegend;
    }
}