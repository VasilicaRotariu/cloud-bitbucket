﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Class for the label button.
    /// </summary>
    public class LabelButton : Label
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="LabelButton"/> class.
        /// </summary>
        public LabelButton()
        {
            MouseEnter += MouseEnterHandler;
            MouseLeave += MouseLeaveHandler;
            Click += MouseClickHandler;
        }

        /// <summary>
        /// Event called when the label button is clicked.
        /// </summary>
        public event EventHandler ButtonClick;

        /// <summary>
        /// Event handler handling the mouse enter event. Used to highlight the button as the mouse
        /// passed over it.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void MouseEnterHandler(object sender, EventArgs e)
        {
            var backgroundLookup = new Dictionary<Color, Color>
            {
                { Color.PaleGreen, Color.DarkGreen },
                { Color.LightBlue, Color.Blue },
                { Color.LightCoral, Color.Maroon },
                { Color.LightGray, Color.DarkGray },
                { SystemColors.Control, Color.DarkGray }
            };

            this.InvokeIfRequired(c =>
            {
                Color color;
                if (backgroundLookup.TryGetValue(c.BackColor, out color))
                {
                    oldBackgroundColour = c.BackColor;
                    c.BackColor = color;
                }
            });
        }

        /// <summary>
        /// Event handler handling the mouse leave event. Used to highlight the button as the mouse
        /// passed over it.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void MouseLeaveHandler(object sender, EventArgs e)
        {
            this.InvokeIfRequired(c => { c.BackColor = oldBackgroundColour; });
        }

        /// <summary>
        /// Event handler handling the mouse click event.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void MouseClickHandler(object sender, EventArgs e)
        {
            this.InvokeIfRequired(c => { c.BorderStyle = BorderStyle.Fixed3D; });
            if (ButtonClick != null)
            {
                ButtonClick(sender, e);
            }
            this.InvokeIfRequired(c => { c.BorderStyle = BorderStyle.None; });
        }

        /// <summary>
        /// The previous background colour, before the image is highlighted.
        /// </summary>
        private Color oldBackgroundColour;
    }
}
