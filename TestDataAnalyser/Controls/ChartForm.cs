﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Diagnostics.CodeAnalysis;
using SudburyDataAnalyser;

namespace SudburyDataAnalyser
{
    /// <summary>
    /// Pop-out chart form
    /// </summary>
    public partial class ChartForm : Form
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ChartForm" /> class
        /// </summary> 
        public ChartForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Copies a chart from a Memory Stream
        /// </summary>
        /// <param name="stream">the memory stream to read from</param>
        public void CopyChartToWindow(MemoryStream stream)
        {
            ChartLegend.LoadFromStream(stream);            
            stream.Close();

            ChartSplitLegend csl = (ChartSplitLegend)ChartLegend;
            csl.OnChartMouseDown += (s, e) =>
            {
                HitTestResult result = csl.Chart.HitTest(e.X, e.Y);

                if (result != null && result.Object != null)
                {
                    if (result.Object is CustomLabel && e.Button == MouseButtons.Left)
                    {
                        //Clicked on an Axis label
                        SetAxisEditorVisible(true);
                    }
                }
                return;
            };

            csl.OnLegendItemChange += (s, e) =>
            {
                //Show Series Editor?
                //Todo:
            };

            SetAxisEditorVisible(false);
        }        

        /// <summary>
        /// Event triggered when the Hide Legend box is used
        /// </summary>
        /// <param name="sender">the control sender</param>
        /// <param name="e">the event arguments</param>
        private void chartHideLegendCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (!axisEditorGroupBox.Visible)
            {
                ChartLegend.ShowLegend = !chartHideLegendCheckBox.Checked;
            }
        }

        /// <summary>
        /// Event triggered when power-point export button is clicked
        /// </summary>
        /// <param name="sender">the control sender</param>
        /// <param name="e">the event arguments</param>
        private void chartExportPowerPoint_Click(object sender, EventArgs e)
        {
            ChartLegend.ExportToPowerPoint();
        }

        /// <summary>
        /// Event triggered when the Close Editor is clicked
        /// </summary>
        /// <param name="sender">the control sender</param>
        /// <param name="e">the event arguments</param>
        private void closeEditorButton_Click(object sender, EventArgs e)
        {
            SetAxisEditorVisible(false);
        }

        /// <summary>
        /// Sets the visibility of the Axis Editor Partition
        /// </summary>
        /// <param name="visible">indicates the visibility of the Axis Editor Window</param>
        private void SetAxisEditorVisible(bool visible)
        {
            axisEditorGroupBox.Visible = visible;
            axisEditorTableView.Visible = visible;            
            splitContainer1.Panel2Collapsed = !visible;

            if (visible)
            {
                PopulateAxisEditor(); 
                ChartLegend.ShowLegend = false;
            }
            else
            {
                //If the user has not elected to hide it, show it
                if (!chartHideLegendCheckBox.Checked)
                    ChartLegend.ShowLegend = true;
            }
        }

        /// <summary>
        /// Populates the Axis Editor Window with values using the Chart data
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed.")]
        private void PopulateAxisEditor()
        {
            // Count number of series against each axis type
            // Need to do this as axis enabled property is always set to "Auto" whether the axis is visible or not
            int x1SeriesCount = 0;
            int x2SeriesCount = 0;
            int y1SeriesCount = 0;
            int y2SeriesCount = 0;

            foreach (Series s in ChartLegend.Chart.Series)
            {
                if (s.XAxisType.Equals(AxisType.Primary))
                {
                    x1SeriesCount++;
                }
                else if (s.XAxisType.Equals(AxisType.Secondary))
                {
                    x2SeriesCount++;
                }

                if (s.YAxisType.Equals(AxisType.Primary))
                {
                    y1SeriesCount++;
                }
                else if (s.YAxisType.Equals(AxisType.Secondary))
                {
                    y2SeriesCount++;
                }
            }

            // Bind axis editor boxes, using format that axis values are in (e.g. date, int, 2dp...)
            // X-axis
            if (x1SeriesCount > 0)
            {
                x1AxisGroupBox.Visible = true;

                x1AxisMinTextBox.DataBindings.Clear();
                x1AxisMaxTextBox.DataBindings.Clear();
                x1AxisIntTextBox.DataBindings.Clear();
                
                x1GridMajIntTextBox.DataBindings.Clear();
                x1GridMinIntTextBox.DataBindings.Clear();
                
                x1TickMajIntTextBox.DataBindings.Clear();
                x1TickMinIntTextBox.DataBindings.Clear();

                x1AxisMinTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisX, "Minimum");
                x1AxisMaxTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisX, "Maximum");
                x1AxisIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisX, "Interval");

                x1GridMajIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisX.MajorGrid, "Interval");
                x1GridMinIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisX.MinorGrid, "Interval");
                                
                x1TickMajIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisX.MajorTickMark, "Interval");
                x1TickMinIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisX.MinorTickMark, "Interval");
            }

            // Secondary X-axis
            if (x2SeriesCount > 0)
            {
                x2AxisGroupBox.Visible = true;

                x2AxisMinTextBox.DataBindings.Clear();
                x2AxisMaxTextBox.DataBindings.Clear();
                x2AxisIntTextBox.DataBindings.Clear();

                x2GridMajIntTextBox.DataBindings.Clear();
                x2GridMinIntTextBox.DataBindings.Clear();

                x2TickMajIntTextBox.DataBindings.Clear();
                x2TickMinIntTextBox.DataBindings.Clear();

                x2AxisMinTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisX2, "Minimum");
                x2AxisMaxTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisX2, "Maximum");
                x2AxisIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisX2, "Interval");

                x2GridMajIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisX2.MajorGrid, "Interval");
                x2GridMinIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisX2.MinorGrid, "Interval");

                x2TickMajIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisX2.MajorTickMark, "Interval");
                x2TickMinIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisX2.MinorTickMark, "Interval");
            }

            // Y-axis
            if (y1SeriesCount > 0)
            {
                y1AxisGroupBox.Visible = true;

                y1AxisMinTextBox.DataBindings.Clear();
                y1AxisMaxTextBox.DataBindings.Clear();
                y1AxisIntTextBox.DataBindings.Clear();

                y1GridMajIntTextBox.DataBindings.Clear();
                y1GridMinIntTextBox.DataBindings.Clear();

                y1TickMajIntTextBox.DataBindings.Clear();
                y1TickMinIntTextBox.DataBindings.Clear();

                y1AxisMinTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisY, "Minimum");
                y1AxisMaxTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisY, "Maximum");
                y1AxisIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisY, "Interval");

                y1GridMajIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisY.MajorGrid, "Interval");
                y1GridMinIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisY.MinorGrid, "Interval");

                y1TickMajIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisY.MajorTickMark, "Interval");
                y1TickMinIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisY.MinorTickMark, "Interval");
            }

            // Secondary Y-axis
            if (y2SeriesCount > 0)
            {
                y2AxisGroupBox.Visible = true;

                y2AxisMinTextBox.DataBindings.Clear();
                y2AxisMaxTextBox.DataBindings.Clear();
                y2AxisIntTextBox.DataBindings.Clear();

                y2GridMajIntTextBox.DataBindings.Clear();
                y2GridMinIntTextBox.DataBindings.Clear();

                y2TickMajIntTextBox.DataBindings.Clear();
                y2TickMinIntTextBox.DataBindings.Clear();

                y2AxisMinTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisY2, "Minimum");
                y2AxisMaxTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisY2, "Maximum");
                y2AxisIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisY2, "Interval");

                y2GridMajIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisY2.MajorGrid, "Interval");
                y2GridMinIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisY2.MinorGrid, "Interval");

                y2TickMajIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisY2.MajorTickMark, "Interval");
                y2TickMinIntTextBox.DataBindings.Add("Text", ChartLegend.Chart.ChartAreas[0].AxisY2.MinorTickMark, "Interval");
            }
        }

        /// <summary>
        /// Event called when the checkbox for auto scaling changes
        /// </summary>
        /// <param name="sender">The sender of the event</param>
        /// <param name="e">The event Arguments</param>
        private void x1AutoScaleCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            // If checked, auto scale axis and disable textboxes, If unchecked, enable textboxes
            if (x1AutoScaleCheckBox.Checked == true)
            {
                x1AxisMinTextBox.Enabled = false;
                x1AxisMaxTextBox.Enabled = false;
                x1AxisIntTextBox.Enabled = false;
                x1GridMajIntTextBox.Enabled = false;
                x1GridMinIntTextBox.Enabled = false;
                x1TickMajIntTextBox.Enabled = false;
                x1TickMinIntTextBox.Enabled = false;

                // Auto scale axis
                ChartLegend.Chart.ChartAreas[0].AxisX.Minimum = double.NaN;
                ChartLegend.Chart.ChartAreas[0].AxisX.Maximum = double.NaN;
            }
            else
            {
                // Enable textboxes for editing
                x1AxisMinTextBox.Enabled = true;
                x1AxisMaxTextBox.Enabled = true;
                x1AxisIntTextBox.Enabled = true;
                x1GridMajIntTextBox.Enabled = true;
                x1GridMinIntTextBox.Enabled = true;
                x1TickMajIntTextBox.Enabled = true;
                x1TickMinIntTextBox.Enabled = true;
            }
        }

        /// <summary>
        /// Event called when the checkbox for auto scaling changes
        /// </summary>
        /// <param name="sender">The sender of the event</param>
        /// <param name="e">The event Arguments</param>
        private void x2AutoScaleCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            // If checked, auto scale axis and disable textboxes, If unchecked, enable textboxes
            if (x2AutoScaleCheckBox.Checked == true)
            {
                x2AxisMinTextBox.Enabled = false;
                x2AxisMaxTextBox.Enabled = false;
                x2AxisIntTextBox.Enabled = false;
                x2GridMajIntTextBox.Enabled = false;
                x2GridMinIntTextBox.Enabled = false;
                x2TickMajIntTextBox.Enabled = false;
                x2TickMinIntTextBox.Enabled = false;

                // Auto scale axis
                ChartLegend.Chart.ChartAreas[0].AxisX2.Minimum = double.NaN;
                ChartLegend.Chart.ChartAreas[0].AxisX2.Maximum = double.NaN;
            }
            else
            {
                // Enable textboxes for editing
                x2AxisMinTextBox.Enabled = true;
                x2AxisMaxTextBox.Enabled = true;
                x2AxisIntTextBox.Enabled = true;
                x2GridMajIntTextBox.Enabled = true;
                x2GridMinIntTextBox.Enabled = true;
                x2TickMajIntTextBox.Enabled = true;
                x2TickMinIntTextBox.Enabled = true;
            }
        }

        /// <summary>
        /// Event called when the checkbox for auto scaling changes
        /// </summary>
        /// <param name="sender">The sender of the event</param>
        /// <param name="e">The event Arguments</param>
        private void y1AutoScaleCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            // If checked, auto scale axis and disable textboxes, If unchecked, enable textboxes
            if (y1AutoScaleCheckBox.Checked == true)
            {
                y1AxisMinTextBox.Enabled = false;
                y1AxisMaxTextBox.Enabled = false;
                y1AxisIntTextBox.Enabled = false;
                y1GridMajIntTextBox.Enabled = false;
                y1GridMinIntTextBox.Enabled = false;
                y1TickMajIntTextBox.Enabled = false;
                y1TickMinIntTextBox.Enabled = false;

                // Auto scale axis
                ChartLegend.Chart.ChartAreas[0].AxisY.Minimum = double.NaN;
                ChartLegend.Chart.ChartAreas[0].AxisY.Maximum = double.NaN;
            }
            else
            {
                // Enable textboxes for editing
                // Enable textboxes for editing
                y1AxisMinTextBox.Enabled = true;
                y1AxisMaxTextBox.Enabled = true;
                y1AxisIntTextBox.Enabled = true;
                y1GridMajIntTextBox.Enabled = true;
                y1GridMinIntTextBox.Enabled = true;
                y1TickMajIntTextBox.Enabled = true;
                y1TickMinIntTextBox.Enabled = true;
            }
        }

        /// <summary>
        /// Event called when the checkbox for auto scaling changes
        /// </summary>
        /// <param name="sender">The sender of the event</param>
        /// <param name="e">The event Arguments</param>
        private void y2AutoScaleCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            // If checked, auto scale axis and disable textboxes, If unchecked, enable textboxes
            if (y2AutoScaleCheckBox.Checked == true)
            {
                y2AxisMinTextBox.Enabled = false;
                y2AxisMaxTextBox.Enabled = false;
                y2AxisIntTextBox.Enabled = false;
                y2GridMajIntTextBox.Enabled = false;
                y2GridMinIntTextBox.Enabled = false;
                y2TickMajIntTextBox.Enabled = false;
                y2TickMinIntTextBox.Enabled = false;

                // Auto scale axis
                ChartLegend.Chart.ChartAreas[0].AxisY2.Minimum = double.NaN;
                ChartLegend.Chart.ChartAreas[0].AxisY2.Maximum = double.NaN;
            }
            else
            {
                // Enable textboxes for editing
                y2AxisMinTextBox.Enabled = true;
                y2AxisMaxTextBox.Enabled = true;
                y2AxisIntTextBox.Enabled = true;
                y2GridMajIntTextBox.Enabled = true;
                y2GridMinIntTextBox.Enabled = true;
                y2TickMajIntTextBox.Enabled = true;
                y2TickMinIntTextBox.Enabled = true;
            }
        }
    }
}
