﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Class for the chart legend, based on a list view control
    /// </summary>
    public class ChartLegend : ListView
    {
        /// <summary>The last selection item.</summary>
        private ListViewItem legendLastSelected;

        /// <summary>
        /// Initialises a new instance of the <see cref="ChartLegend"/> class.
        /// </summary>
        public ChartLegend()
        {
            this.View = View.Details;
            this.BorderStyle = BorderStyle.None;
            this.HeaderStyle = ColumnHeaderStyle.None;

            this.ItemSelectionChanged += this.LegendItemSelected; 
            this.MouseDown += this.LegendMouseDown;
        }

        /// <summary>
        /// Delegate for the selection change event.
        /// </summary>
        /// <param name="sender">The ChartLegend that sent the event.</param>
        /// <param name="e">The selection changed event arguments.</param>
        public delegate void SelectionChangedHandler(object sender, SelectionChangedEventArgs e);

        /// <summary>
        /// The selection changed event, called when the legend item selection changes.
        /// </summary>
        public event SelectionChangedHandler SelectionChanged;

        /// <summary>
        /// Gets the current selection.
        /// </summary>
        public string Selection
        {
            get
            {
                if (this.legendLastSelected != null)
                {
                    return this.legendLastSelected.Text;
                }

                return null;
            }
        }

        /// <summary>
        /// Clears the legend of all items.
        /// </summary>
        public new void Clear()
        {
            base.Clear();
            this.legendLastSelected = null;
        }

        /// <summary>
        /// Adds a series to the legend.
        /// </summary>
        /// <param name="series">The series to add to the legend.</param>
        public void Add(Series series)
        {
            if (series.IsVisibleInLegend)
            {
                if (Columns.Count == 0)
                {
                    Columns.Add(string.Empty);
                }

                if (!Items.ContainsKey(series.Name))
                {
                    var item = new ListViewItem(series.Name) { ForeColor = series.Color, Name = series.Name };

                    Items.Add(item);
                    Columns[0].Width = Math.Max(Columns[0].Width, TextRenderer.MeasureText(series.Name, Font).Width + 10);
                }
            }
        }

        /// <summary>
        /// Called when the list view item selection changes. Updates the look of the legend
        /// and calls the selection changed events.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event arguments.</param>
        private void LegendItemSelected(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.Item.Selected)
            {
                if (this.legendLastSelected != e.Item)
                {
                    this.FlipColours(e.Item);
                    this.FlipColours(this.legendLastSelected);
                    this.SetSelectionChanged(e.Item);
                    this.legendLastSelected = e.Item;
                }

                e.Item.Selected = false;
            }
        }

        /// <summary>
        /// Calls the selection changed event handlers if there are any.
        /// </summary>
        /// <param name="selection">The new selection.</param>
        private void SetSelectionChanged(ListViewItem selection = null)
        {
            if (this.SelectionChanged != null)
            {
                this.SelectionChanged(this, new SelectionChangedEventArgs(selection));
            }
        }

        /// <summary>
        /// Handles the mouse down event. If an item is not selected by the mouse click,
        /// the selection is cleared.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event arguments.</param>
        private void LegendMouseDown(object sender, MouseEventArgs e)
        {
            if (HitTest(e.Location).Item == null)
            {
                this.FlipColours(this.legendLastSelected);
                this.SetSelectionChanged();

                this.legendLastSelected = null;
            }
        }

        /// <summary>
        /// Flips the background and foreground colours of the list view item.
        /// </summary>
        /// <param name="item">The list view item.</param>
        private void FlipColours(ListViewItem item)
        {
            if (item != null)
            {
                Color foreColour = item.ForeColor;
                item.ForeColor = item.BackColor;
                item.BackColor = foreColour;
            }
        }

        /// <summary>
        /// Event arguments class used by the selection changed event.
        /// </summary>
        public class SelectionChangedEventArgs : EventArgs
        {
            /// <summary>
            /// Initialises a new instance of the <see cref="SelectionChangedEventArgs"/> class.
            /// </summary>
            public SelectionChangedEventArgs()
            {
            }

            /// <summary>
            /// Initialises a new instance of the <see cref="SelectionChangedEventArgs"/> class.
            /// </summary>
            /// <param name="selection">The selection.</param>
            public SelectionChangedEventArgs(ListViewItem selection)
            {
                if (selection != null)
                {
                    this.Selection = selection.Text;
                }
            }

            /// <summary>
            /// Gets or sets the selection.
            /// </summary>
            public string Selection { get; set; }
        }
    }
}
