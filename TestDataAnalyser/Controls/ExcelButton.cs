﻿using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// The Excel button class, based on the picture box button.
    /// </summary>
    public class ExcelButton : PictureBoxButton
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ExcelButton"/> class.
        /// </summary>
        public ExcelButton()
            : base(null, Properties.Resources.Excel)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ExcelButton"/> class.
        /// </summary>
        /// <param name="parent">The buttons parent.</param>
        public ExcelButton(Control parent)
            : base(parent, Properties.Resources.Excel)
        {
        }

        /// <summary>
        /// Copies a data grid view or a chart into an Excel document.
        /// </summary>
        protected override void OnMouseClick()
        {
            if (!(Tag is string)) return;

            foreach (var control in ControlExt.GetControls(Tag as string))
            {
                if (control is DataGridView)
                {
                    Singleton<ExcelHelper>.Instance.Export(control as DataGridView);
                    break;
                }
                if (control is Chart)
                {
                    Singleton<ExcelHelper>.Instance.Export(control as Chart);
                    break;
                }
            }
        }
    }
}
