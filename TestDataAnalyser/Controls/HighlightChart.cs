﻿using System;
using System.Windows.Forms.DataVisualization.Charting;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Class for the highlight chart, extension of the chart class that adds
    /// series highlighting.
    /// </summary>
    public class HighlightChart : Chart
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="HighlightChart"/> class.
        /// </summary>
        public HighlightChart()
        {
            DoubleClick += HighlightChart_DoubleClick;
        }

        /// <summary>
        /// Highlights a series by name, or if no name is specified, clears any highlights.
        /// </summary>
        /// <param name="seriesName">The name of the series to highlight.</param>
        public void HighlightSeries(string seriesName)
        {
            if (seriesName == null)
            {
                ClearHighlight();
            }
            else
            {
                foreach (Series series in Series)
                {
                    if (series.Name == seriesName)
                    {
                        series.BorderWidth = 4;
                    }
                    else
                    {
                        series.BorderWidth = 1;
                    }
                }
            }
        }

        /// <summary>
        /// Clears all highlights.
        /// </summary>
        public void ClearHighlight()
        {
            foreach (Series series in Series)
            {
                series.BorderWidth = 2;
            }
        }

        /// <summary>
        /// Called when the chart is double clicked. Opens the chart in a big chart.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void HighlightChart_DoubleClick(object sender, EventArgs e)
        {
            this.OpenIsCustomBigChart();
        }
    }
}
