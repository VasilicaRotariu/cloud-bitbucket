﻿using System;
using System.Drawing;
using System.Windows.Forms;

// ReSharper disable once CheckNamespace
namespace SudburyDataAnalyser
{
    /// <summary>
    /// Base class for all picture box buttons.
    /// </summary>
    public class PictureBoxButton : PictureBox
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="PictureBoxButton"/> class.
        /// </summary>
        /// <param name="parent">The buttons parent.</param>
        /// <param name="buttonImage">The buttons image.</param>
        public PictureBoxButton(Control parent, Bitmap buttonImage)
        {
            Parent = parent;
            Image = buttonImage;
            Width = buttonImage.Width + 2;
            Height = buttonImage.Height + 2;
            Padding = new Padding(1, 1, 0, 0);

            MouseEnter += MouseEnterHandler;
            MouseLeave += MouseLeaveHandler;
            Click += MouseClickHandler;
        }

        /// <summary>
        /// Mouse click event handler. Base does nothing.
        /// </summary>
        protected virtual void OnMouseClick()
        {
        }

        /// <summary>
        /// Event handler handling the mouse enter event. Used to highlight the button as the mouse
        /// passed over it.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void MouseEnterHandler(object sender, EventArgs e)
        {
            this.InvokeIfRequired(c => { oldBackgroundColour = c.BackColor; c.BackColor = Color.BlanchedAlmond; });
        }

        /// <summary>
        /// Event handler handling the mouse leave event. Used to highlight the button as the mouse
        /// passed over it.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void MouseLeaveHandler(object sender, EventArgs e)
        {
            this.InvokeIfRequired(c => { c.BackColor = oldBackgroundColour; });
        }

        /// <summary>
        /// Event handler handling the mouse click event.
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments.</param>
        private void MouseClickHandler(object sender, EventArgs e)
        {
            this.InvokeIfRequired(c => { c.BorderStyle = BorderStyle.Fixed3D; });
            OnMouseClick();
            this.InvokeIfRequired(c => { c.BorderStyle = BorderStyle.None; });
        }

        /// <summary>
        /// The previous background colour, before the image is highlighted.
        /// </summary>
        private Color oldBackgroundColour;
    }
}
