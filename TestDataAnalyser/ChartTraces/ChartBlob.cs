﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms.DataVisualization.Charting;

namespace SudburyDataAnalyser.ChartTraces
{
    /// <summary>
    /// Manages the chart trace
    /// </summary>
    public class ChartBLOB
    {
        /// <summary>
        /// Gets the series created from reading the BLOB
        /// </summary>
        public IEnumerable<Series> Series { get; private set; }
        
        /// <summary>
        /// Gets the number of Series contained in the blob
        /// </summary>
        public int Count
        {
            get
            {
                return Series.Count();
            }
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="ChartBLOB" /> class
        /// </summary>
        /// <param name="allByte">Byte array (BLOB) to be parsed.</param>
        /// <param name="utid">UTID that the BLOB belongs to.</param>
        /// <param name="urid">URID that the BLOB belongs to.</param>
        public ChartBLOB(byte[] allByte, int utid, long urid)
            : this(allByte, utid.ToString(CultureInfo.InvariantCulture), urid.ToString(CultureInfo.InvariantCulture))
        { 
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ChartBLOB" /> class
        /// </summary>
        /// <param name="allByte">Byte array (BLOB) to be parsed.</param>
        /// <param name="utid">UTID that the BLOB belongs to.</param>
        /// <param name="urid">URID that the BLOB belongs to.</param>
        public ChartBLOB(byte[] allByte, string utid, string urid)
        {
            Series = ParseBlob(allByte, utid, urid);
        }

        /// <summary>
        /// Yields a collection of series names from the BLOB
        /// </summary>
        /// <returns>A Collection of series names</returns>
        public IEnumerable<string> SeriesNames()
        {
            foreach (Series s in Series)
                yield return s.Name;
        }

        /// <summary>
        /// Converts a binary large object (BLOB) data into a collection of chart series.
        /// Parsing of the BLOB is specific to the way in which the BLOB has been structured in the BITS code.
        /// </summary>
        /// <param name="allByte">Byte array (BLOB) to be parsed.</param>
        /// <param name="utid">UTID that the BLOB belongs to.</param>
        /// <param name="urid">URID that the BLOB belongs to.</param>
        /// <returns>Collection of charts series split out from the BLOB.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation",
                         Justification = "Variables xValue and yValues are X and Y axis specific.")]
        private IEnumerable<Series> ParseBlob(byte[] allByte, string utid, string urid)
        {
            var seriesCollection = new Collection<Series>();

            var fs = new MemoryStream(allByte, true) { Position = 0 };

            using (var reader = new BinaryReader(fs))
            {
                // Note AnsiStrings in C++ are in the format - 4 bytes (int) for length, followed by string
                var dateLength = reader.ReadInt32();
                // ReSharper disable once UnusedVariable
                var charDate = reader.ReadChars(dateLength);

                var isnLength = reader.ReadInt32();
                // ReSharper disable once UnusedVariable
                var charIsn = reader.ReadChars(isnLength);

                var pumpSnLength = reader.ReadInt32();
                // ReSharper disable once UnusedVariable
                var charPumpSn = reader.ReadChars(pumpSnLength);

                // ReSharper disable once UnusedVariable
                var iLineNumber = reader.ReadInt32();
                // ReSharper disable once UnusedVariable
                var iTestPoint = reader.ReadInt32();

                // Now loop for each series
                while (reader.BaseStream.Position < reader.BaseStream.Length)
                {
                    var traceNameLength = reader.ReadInt32();
                    var charTraceName = reader.ReadChars(traceNameLength);
                    var sTraceName = StringExt.FromCharArray(charTraceName);

                    // ReSharper disable once UnusedVariable
                    var colour = reader.ReadInt32();
                    var axis = reader.ReadUInt32();
                    var dataPointCount = reader.ReadUInt32();
                    var verticalScalar = reader.ReadDouble();
                    var verticalOrigin = reader.ReadDouble();
                    var horizontalMin = reader.ReadDouble();
                    var horizontalMax = reader.ReadDouble();
                    var horizontalDiff = horizontalMax - horizontalMin;

                    var typeLength = reader.ReadInt32();
                    var charType = reader.ReadChars(typeLength);
                    var sType = StringExt.FromCharArray(charType);

                    // ReSharper disable once UnusedVariable
                    var width = reader.ReadInt32();
                    // ReSharper disable once UnusedVariable
                    var style = reader.ReadUInt32();

                    var series = new Series(string.Format("{0} {1} {2}", utid, urid, sTraceName));

                    // Scale X & Y values
                    var yValues = new double[dataPointCount];

                    // If the number of data points is < 16 (threshold set in trace generation), then expect an x and y value (ys first, then xs)
                    // Y values as UInt16
                    for (var i = 0; i < dataPointCount; i++)
                    {
                        yValues[i] = (reader.ReadUInt16() / verticalScalar) + verticalOrigin;
                    }

                    var pointsCount = 0;
                    foreach (var yValue in yValues)
                    {
                        double xValue;

                        if (dataPointCount < 16)
                        {
                            xValue = (reader.ReadUInt16() * horizontalDiff / 65535.0) + horizontalMin;
                        }
                        else
                        {
                            xValue = (pointsCount * horizontalDiff / dataPointCount) + horizontalMin;
                        }
                        ++pointsCount;

                        series.Points.AddXY(xValue, yValue);
                    }

                    // Now plot data
                    if ((sType == "TFastLineSeries" && series.Points.Count > 1) || (sType == "TLineSeries" && series.Points.Count > 1))
                    {
                        series.ChartType = SeriesChartType.FastLine;
                    }
                    else
                    {
                        series.ChartType = SeriesChartType.Point;
                        series.MarkerBorderColor = Color.Black;
                        series.MarkerSize = 8;

                        // Set marker type for each single point type
                        MarkerStyle pointMarker;
                        switch (sTraceName)
                        {
                            case "OD R":
                                pointMarker = MarkerStyle.Diamond;
                                break;
                            case "CD L":
                                pointMarker = MarkerStyle.Square;
                                break;
                            case "NCVT4 R":
                                pointMarker = MarkerStyle.Triangle;
                                break;
                            case "LERP L":
                                pointMarker = MarkerStyle.Star6;
                                break;
                            case "EOI R":
                                pointMarker = MarkerStyle.Circle;
                                break;
                            default:
                                pointMarker = MarkerStyle.Cross;
                                break;
                        }
                        series.MarkerStyle = pointMarker;
                    }

                    if (axis != 0)
                    {
                        series.YAxisType = AxisType.Secondary;
                    }

                    // Add current series to return series collection scBLOB
                    seriesCollection.Add(series);
                }
                reader.Close();
            }
            return seriesCollection;
        }
    }
}
