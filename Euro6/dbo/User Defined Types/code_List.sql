﻿CREATE TYPE [dbo].[code_List] AS TABLE (
    [code] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([code] ASC));

