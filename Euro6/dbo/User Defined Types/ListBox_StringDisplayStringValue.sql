﻿CREATE TYPE [dbo].[ListBox_StringDisplayStringValue] AS TABLE (
    [DisplayValue] VARCHAR (80) NULL,
    [Value]        VARCHAR (80) NULL);

