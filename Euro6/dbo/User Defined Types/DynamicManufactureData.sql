﻿CREATE TYPE [dbo].[DynamicManufactureData] AS TABLE (
    [UID]  INT        NOT NULL,
    [Data] FLOAT (53) NULL,
    PRIMARY KEY CLUSTERED ([UID] ASC));

