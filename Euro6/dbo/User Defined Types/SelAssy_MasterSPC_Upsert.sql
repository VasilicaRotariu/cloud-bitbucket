﻿CREATE TYPE [dbo].[SelAssy_MasterSPC_Upsert] AS TABLE (
    [UTC]                        DATETIME2 (7) NULL,
    [PlantNumber]                SMALLINT      NULL,
    [ComponentType]              VARCHAR (20)  NULL,
    [SPCPartNumberAlias]         VARCHAR (50)  NULL,
    [SPCPartDescriptionAlias]    VARCHAR (50)  NULL,
    [Feature]                    VARCHAR (50)  NULL,
    [SPCFeatureDescriptionAlias] VARCHAR (50)  NULL,
    [Value1]                     FLOAT (53)    NULL,
    [Value2]                     FLOAT (53)    NULL,
    [Value3]                     FLOAT (53)    NULL,
    [Value4]                     FLOAT (53)    NULL,
    [Value5]                     FLOAT (53)    NULL);

