﻿
CREATE PROCEDURE [dbo].[DynamicManufacture_SelectiveAssembly]
	@Part1ID varchar(20),
	@Result1 sql_variant OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	-- A value indicating whether a query was executed
	DECLARE @QueryExecuted bit = 0

	-- Hold the result(s) in temporay varaibles to detect NULL value.
	DECLARE @Result1Temp float = NULL

	-- Return a 0 to indicate a sucessful query.
	-- Set to 1 elsewhere in the query to signal an error.
	DECLARE @Ret int = 0

	-- Set a default value for the result(s)
	SET @Result1 = 0

	/*
		LIKE expressions:
		%   Any string of zero or more characters.
		_   Any single character.
		[ ] Any single character within the specified range ([a-f]) or set ([abcdef]).
		[^] Any single character not within the specified range ([^a-f]) or set ([^abcdef]).
	*/

	-- LIKE expression to validate part types.
	-- Note that a type not found is not a result failure.
	IF @Part1ID LIKE '34%'
	BEGIN
		SELECT TOP 1 @Result1Temp = [PM_Flow2]
        FROM [ComponentHegs]
        WHERE PDate = (
                        SELECT MAX(PDate)
                        FROM [ComponentHegs]
                        WHERE [Serial] = @Part1ID
                        GROUP BY [Serial]
                       )
        AND [Serial] = @Part1ID

		SET @QueryExecuted = 1

		SELECT TOP 1 *
        FROM [ComponentHegs]
        WHERE PDate = (
                        SELECT MAX(PDate)
                        FROM [ComponentHegs]
                        WHERE [Serial] = @Part1ID
                        GROUP BY [Serial]
                       )
        AND [Serial] = @Part1ID
		
		SELECT TOP 1 *
        FROM [ComponentHegs]
        WHERE PDate = (
                        SELECT MAX(PDate)
                        FROM [ComponentHegs]
                        WHERE [Serial] = @Part1ID
                        GROUP BY [Serial]
                       )
        AND [Serial] = @Part1ID
	END

	-- Establish valid state.
	-- For example, if @Result1Temp is NULL then there was no record returned
	-- in the query. This could cause error state at the client, where it must
	-- not proceed but instead flag error state for the part (or throw it out).
	IF @QueryExecuted = 1
	BEGIN
		IF @Result1Temp IS NOT NULL
		BEGIN
			SET @Result1 = @Result1Temp
		END 
		ELSE
		BEGIN
			SET @Ret = 1 -- A row for the part was not found.
		END
	END

	RETURN @Ret -- 0 is pass, anything else is fail.
END
