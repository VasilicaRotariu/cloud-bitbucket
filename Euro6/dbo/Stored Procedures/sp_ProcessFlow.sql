﻿

	CREATE PROCEDURE [dbo].[sp_ProcessFlow]
(
	@SerialNumber VARCHAR(MAX),		-- The serial number/matrix to be checked
	@RunSelect TINYINT = 0				-- run a select statement at the end of the procedure if set by the user at 0
)
AS 
	BEGIN
		SET NOCOUNT ON;
	
			--VR, 03/02/2021
			--I will start checking table by table, and after I will be focused to reduce the number of it using views, and also to implement a SP having the input the ISN

			DECLARE @SerialNumNozzle varchar(50)
			DECLARE @SerialNumNCV varchar(50)
			DECLARE @SerialNumPG varchar(50)
			DECLARE @SerialNumINJ varchar(50)
			DECLARE @SerialNumINJMDEG varchar(50)
			DECLARE @AssemblyDate datetime

			SET @SerialNumINJ=@SerialNumber--'210433303'
			--SET @SerialNumINJ='210433738'

			SELECT TOP(1) @AssemblyDate = PDate, 
						  @SerialNumINJ = ISN, 
						  @SerialNumNozzle =  NozzleSerial, 
						  @SerialNumNCV = NCVSerial, 
						  @SerialNumPG = PGSerial
			FROM [Euro6].[dbo].[vw_AssemblyComponentTotals]
			WHERE ISN= @SerialNumber
			ORDER BY PDate DESC

			SELECT TOP(1) @AssemblyDate = PDate, 
						  @SerialNumINJ = ISN, 
						  @SerialNumINJMDEG=Daimler_ISN,
						  @SerialNumNozzle =  NozzleSerial, 
						  @SerialNumNCV = NCVSerial, 
						  @SerialNumPG = PGSerial
			FROM [Euro6MDEGTest].[dbo].[vw_AssemblyComponentTotalsMDEG]	
			WHERE Daimler_ISN=@SerialNumber
			ORDER BY PDate DESC

			--SELECT TOP(1) PDate, 
			--			  ISN, 
			--			  NozzleSerial, 
			--			  NCVSerial, 
			--			  PGSerial
			--FROM [Euro6].[dbo].[vw_AssemblyComponentTotals]
			--WHERE ISN= @SerialNumINJ
			--ORDER BY PDate DESC

			--SELECT TOP(1) PDate, 
			--			  Daimler_ISN, 
			--			  NozzleSerial, 
			--			  NCVSerial, 
			--			  PGSerial
			--FROM [Euro6MDEGTest].[dbo].[vw_AssemblyComponentTotalsMDEG]	
			--WHERE Daimler_ISN=@SerialNumINJ
			--ORDER BY PDate DESC

			--Nozzle MCH OP180
			SELECT  PDate, SerialNum, PlantNumber,'180' AS Operation, FailCode
			FROM [Euro6].[dbo].[NozzleHegsProduction]
			WHERE SerialNum = @SerialNumNozzle
			--Nozzle MCH OP190
			UNION ALL
			SELECT  PDate, SerialNum, PlantNumber,'190' AS Operation, UploadCode
			FROM [Euro6].[dbo].[OldNozzleHegsProduction]
			WHERE SerialNum = @SerialNumNozzle
			--NCV MCH
			UNION ALL
			SELECT PDate, Serial, AssetNumber,'8195' AS Operation, SUBSTRING(FailCode, CHARINDEX('(', FailCode) + 1, ABS(CHARINDEX(')', FailCode) - CHARINDEX('(', FailCode) - 1))
			FROM [Euro6].[dbo].[ComponentHegs]
			WHERE Serial=@SerialNumNCV
			--PG MCH
			UNION ALL
			SELECT PDate, Serial, AssetNumber,'38' AS Operation, SUBSTRING(FailCode, CHARINDEX('(', FailCode) + 1, ABS(CHARINDEX(')', FailCode) - CHARINDEX('(', FailCode) - 1))
			FROM [Euro6].[dbo].[ComponentHegs]
			WHERE Serial=@SerialNumPG
			--NNC Assy
			UNION ALL
			SELECT  PDate, NozzleSerial, AssetNumber,'400' AS Operation, FailCode
			FROM [Euro6].[dbo].[NNCAssembly]
			WHERE NozzleSerial = @SerialNumNozzle
			--NNC Assy
			UNION ALL
			SELECT  PDate, Nozzle_Matrix, 5555,'Manual Assy' AS Operation, '0'
			FROM [Euro6].[dbo].[20604_MatchGrind_Results]
			WHERE Nozzle_Matrix = @SerialNumNozzle
			--NNC Test 
			UNION ALL
			SELECT  PDate, Serial_No_Long collate Latin1_General_CI_AS, PlantNumber,'410' AS Operation, OA_Result
			FROM [NNCNozzleTest].[dbo].[Result]
			WHERE Serial_No_Long = @SerialNumNozzle
		    --NNC Test 
			UNION ALL
			SELECT  PDate, Serial,'20474' AS PlantNumber,'415' AS Operation, FailCode
			FROM [Euro6].[dbo].[NozzleTestWithError_Results]
			WHERE Serial = @SerialNumNozzle
			--NNC Test
			UNION ALL
			SELECT  PDate, SERIAL_NUMBER collate Latin1_General_CI_AS, PlantNumber,'415' AS Operation, SUBSTRING(FAIL_CODE, CHARINDEX('(', FAIL_CODE) + 1, ABS(CHARINDEX(')', FAIL_CODE) - CHARINDEX('(', FAIL_CODE) - 1))
			FROM [NNCNozzleTest].[dbo].[20999_NozzleTest]
			WHERE SERIAL_NUMBER = @SerialNumNozzle
			--Rwk table
			UNION ALL
			SELECT  PDate, Nozzle_Matrix, AssetNumber,'RWK' AS Operation,FailCode
			FROM [Euro6].[dbo].[20603_MatchGrind_Results]
			WHERE Nozzle_Matrix = @SerialNumNozzle
			--SA2 check
			UNION ALL
			SELECT UTC, BodyMatrix, PlantNumber, '4' AS Operation, CAST (CASE WHEN [FailureCode] = 1 THEN 0 ELSE [FailureCode] END AS INT)
			FROM [Euro6].[dbo].[SelAssyAssembly]
			WHERE [BodyMatrix] = @SerialNumNCV
			--SA1 Check
			UNION ALL
			SELECT UTC, Matrix, '4040' AS PlantNumber, '4' AS Operation, Passed 
			--FROM [SELASSEMBLY\SELASSY].[SA_Stats].[dbo].[NCV_MeasureComplete]
			FROM [Euro6].[dbo].[SA1_NCV_OffloadComplete] 
			WHERE [Matrix] = @SerialNumNCV
			--SA3 CHECK 
			UNION ALL
			SELECT UTC, BodyMatrix, PlantNumber, '4' AS Operation, FailureCode
			FROM [Euro6].[dbo].[SA3_SelAssyAssembly]
			WHERE [BodyMatrix]=@SerialNumNCV					
			--NCV Armature Press 
			UNION ALL
			SELECT PDate,  DataMatrix, PlantNumber, '5' AS Operation, Result
			FROM [Euro6].[dbo].[NCVArmaturePress]
			WHERE [DataMatrix] = @SerialNumNCV
			UNION ALL
			SELECT  PDate, DataMatrix, PlantNumber,'5' AS Operation, Press_Result
			FROM [Euro6].[dbo].[ManualArmaturePressResults]
			WHERE [DataMatrix] =@SerialNumNCV
			UNION ALL
			SELECT  PDate, SerialNumber,'20524' AS PlantNumber,'5' AS Operation, Result
			FROM [Euro6].[dbo].[Zygo]
			WHERE [SerialNumber] = @SerialNumNCV
			--Injector Assy L2 & L4 OP10
			UNION ALL
			SELECT  TDate, RIGHT(Injector_ID, 9),(CASE WHEN [LineID] = 2 THEN '20184' WHEN [LineID] = 4 THEN '35824' END) AS AssetNumber, '10' AS Operation, GLOBAL_RESULT
			FROM  [Euro6].[dbo].[InjectorLeakTest]
			WHERE  RIGHT(Injector_ID, 9) = @SerialNumINJ
			--Injector Assy L2 & L4 OP20
			UNION ALL
			SELECT  TDate, RIGHT(Injector_ID, 9),(CASE WHEN [LineID] = 2 THEN '20184' WHEN [LineID] = 4 THEN '35824' END) AS AssetNumber, '20' AS Operation, GLOBAL_RESULT
			FROM  [Euro6].[dbo].[RSPL]
			WHERE  RIGHT(Injector_ID, 9) = @SerialNumINJ
			--Injector Assy L2 & L4 OP30
			UNION ALL
			SELECT  TDate, RIGHT(Injector_ID, 9),(CASE WHEN [LineID] = 2 THEN '20184' WHEN [LineID] = 4 THEN '35824' END) AS AssetNumber, '30' AS Operation, GLOBAL_RESULT
			FROM  [Euro6].[dbo].[NLS]
			WHERE  RIGHT(Injector_ID, 9) = @SerialNumINJ
			--Injector Assy L3
			UNION ALL
			SELECT 	PDate, RIGHT(Serial, 9), '20741' AS AssetNumber, '10' AS Operation, Result
			FROM  [Euro6].[dbo].[MASMEC_SelAsmb_OP10_Results]
			WHERE RIGHT(Serial,9) = @SerialNumINJ
			--Injector Assy L3
			UNION ALL
			SELECT 	Tdate, RIGHT(Serial, 9), '20741' AS AssetNumber, '20' AS Operation, Result
			FROM  [Euro6].[dbo].[MASMEC_SelAsmb_OP20B_Results]
			WHERE RIGHT(Serial,9) = @SerialNumINJ
			--Injector Assy L3
			UNION ALL
			SELECT 	TDate, RIGHT(Serial, 9), '20741' AS AssetNumber, '30' AS Operation, Result
			FROM  [Euro6].[dbo].[MASMEC_SelAsmb_OP30B_Results]
			WHERE RIGHT(Serial,9) = @SerialNumINJ
			--Injector  MDEG Test
			UNION ALL
			SELECT h.PDate, dsn.DaimlerSerialNumber collate Latin1_General_CI_AS, H.PlantNumber,'50' AS Operation, r.Grade
			FROM [Euro6MDEGTest].[dbo].[Header] as h
			INNER JOIN [Euro6MDEGTest].[dbo].[DaimlerSerialNumber] as dsn on h.UTID = dsn.UTID and h.PlantNumber = dsn.PlantNumber
			INNER JOIN [Euro6MDEGTest].[dbo].[Results] AS r on r.UTID=h.UTID
			WHERE dsn.DaimlerSerialNumber collate Latin1_General_CI_AS = @SerialNumINJMDEG
			--Injector  DAF Test
			UNION ALL
			SELECT  h.PDate, h.ISN  collate Latin1_General_CI_AS, H.PlantNumber,'50' AS Operation, r.Grade
			FROM [Euro6DAFTest].[dbo].[Header] as h
			INNER JOIN [Euro6DAFTest].[dbo].[Results] AS r on r.UTID=h.UTID
			WHERE h.ISN collate Latin1_General_CI_AS = @SerialNumINJ


END

GO
GRANT EXECUTE
    ON OBJECT::[dbo].[sp_ProcessFlow] TO [dbcache]
    AS [dbo];

