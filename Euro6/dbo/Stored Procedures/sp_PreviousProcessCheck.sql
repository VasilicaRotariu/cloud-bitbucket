﻿



-- =============================================
-- Author:		
-- Create date: 25/06/2014
-- Description:	Generic Previous Process Procedure 
-- Modified on 30/07/2014 to add MASMEC DAF injector build line
-- Modified on 03/11/2014 to add Selective Assembly 1
-- Modified on 29/01/2015 to add Selective Assembly 2
-- Modified on 16/12/2019 to update Assy L2 from Iasi
-- Modified on 28/01/2020 to add OP354 Nozzle Guide control
-- Modified on 10/02/2020 to add New NFT test - NNC aasy check
-- Modified on 26/03/2020 to add New NFT Test- Assembly check
-- Modified on 18/05/2020 to add Manual Armature Press, Automatic Armature Press - SA3 check
-- Modified on 03/07/2020 to update NNC Assy checks
-- Modified on 30/10/2020 to add Banned Nozzels for assy injectors
-- Modified on 02/12/2020 to activate the PPC at ScanPack
-- Modified on 11/12/2020 to update the PPC at ScanPack
-- =============================================

--================== Previous process calling procedure ==============================


 --use [Euro6] 
 --Declare @ResultCode INT, @Error_Description VARCHAR(MAX)
 
 --EXEC dbo.sp_PreviousProcessCheck
 --@SerialNumber = 'H375065823GMR' ,
 --@PlantNumber = '18932',             -- or @PlantNumber = 'OC3_20707',
 --@Result = @ResultCode OUTPUT,
 --@ErrorDescription = @Error_Description OUTPUT
 
 --SELECT @ResultCode, @Error_Description

-- =====================================================================================
CREATE PROCEDURE [dbo].[sp_PreviousProcessCheck]
(
	@SerialNumber VARCHAR(MAX),		-- The serial number/matrix to be checked
	@PlantNumber VARCHAR(MAX),		-- Plantnumber of the Machine Requesting Previous Result
	@Result VARCHAR(MAX) OUT,			-- Where 0 = PASS
	@ErrorDescription VARCHAR(MAX) OUT,	-- If @Result anything other than 0 then this will explain why 
	@RunSelect TINYINT = 0				-- run a select statement at the end of the procedure if set by the user at 0
)
AS 
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ResultUID INT
		DECLARE @PlantNbr INT
		DECLARE @SeparatorIndex SMALLINT
		DECLARE @Parameter VARCHAR(MAX)
		DECLARE @startTime DATETIME = GETDATE();  -- Set start time	
		DECLARE @ResultBN SMALLINT
		DECLARE @PDateBN DateTime

		---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        -- Separate the plant number and the parameter if any
        ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		SELECT @SeparatorIndex = CHARINDEX('_', @PlantNumber);
		IF @SeparatorIndex != 0
			BEGIN
				SELECT @PlantNbr = CAST( SUBSTRING(@PlantNumber, @SeparatorIndex + 1, 50) AS int) , @Parameter = SUBSTRING(@PlantNumber, 0, @SeparatorIndex) 
			END
		ELSE
			BEGIN 
				SELECT @PlantNbr = CAST (@PlantNumber AS INT)
			END

		---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        -- NNC Test
		-- Test the PPC on OP410.1, OP410.2, OP415.2 and OP415.3 
        ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		IF @PlantNbr IN (20683, 20790, 20999, 20474)
			BEGIN        
				--SELECT TOP 1 @Result=[FailCode] FROM [Euro6].[dbo].[NNCAssembly]
				--WHERE [NozzleSerial] = @SerialNumber
				--ORDER BY [PDate] DESC;
				-- modification 17-03-2021 - RV
				SELECT TOP 1 @Result=[FailCode] 
				FROM (
				      SELECT FailCode, PDate FROM [ZBLN_FTPR].[dbo].ResultsNew
				      WHERE [PartID] = @SerialNumber
				      UNION ALL
				      SELECT FailCode, PDate FROM [Euro6].[dbo].[NNCAssembly]
				      WHERE [NozzleSerial]=@SerialNumber ) as assy2
				ORDER BY [PDate] DESC;
		
				IF @Result = NULL OR @@ROWCOUNT = 0
					BEGIN
					--SELECT @ResultUID=UID FROM [Euro6].[dbo].[20603_MatchGrind_Results]
					--WHERE Nozzle_Matrix = @SerialNumber 
					-- modification 25-08-2020 - RV
					SELECT TOP 1 @ResultUID=UID
					FROM(
						SELECT UID, PDate FROM [Euro6].[dbo].[20603_MatchGrind_Results]
						WHERE Nozzle_Matrix = @SerialNumber 
						UNION ALL
						SELECT UID, PDate FROM [Euro6].[dbo].[20604_MatchGrind_Results]
						WHERE Nozzle_Matrix = @SerialNumber 
						) as assy
						ORDER BY [PDate] DESC;
					--
						IF @ResultUID = NULL OR @@ROWCOUNT = 0
							BEGIN
								SET @Result = 999
								SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[NNCAssembly] or Rework for serial : '  + @SerialNumber
							END
						ELSE
							BEGIN
								SET @Result = 0
								SET @ErrorDescription = 'PASSED'
							END
					END
				ELSE
					BEGIN
						IF @Result != 0
							BEGIN
							--modification 27-09-16
								--IF @Result = 1 
								--	BEGIN
								--		SET @ErrorDescription = 'FAILED'
								--	END 
								--ELSE
								--	BEGIN
								--		SET @ErrorDescription = 'FAILED : Failure code ' + @Result
								--	END       
								SELECT @ResultUID=UID FROM [Euro6].[dbo].[20603_MatchGrind_Results]
								WHERE Nozzle_Matrix = @SerialNumber and PDate  > (
									SELECT TOP 1 [TDate]
    								FROM [Euro6].[dbo].[NNCAssembly] where NozzleSerial = @SerialNumber ORDER BY TDate DESC								
									)
								IF @ResultUID = NULL OR @@ROWCOUNT = 0
									BEGIN
										IF @Result = 1 
											BEGIN
												SET @ErrorDescription = 'FAILED'
											END 
										ELSE
											BEGIN
												SET @ErrorDescription = 'FAILED : Failure code ' + @Result
											END
									END      
									
								ELSE
									BEGIN
										Set @Result = 0
										SET @ErrorDescription = 'PASSED'
									END
						    END

						ELSE                          
						 	BEGIN
								SET @Result = 0
								SET @ErrorDescription = 'PASSED'
							END
					END
			END

		---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        -- NCV MCH
		-- Test the PPC on AXIOME High pressure De-burr
        ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		IF @PlantNbr IN (20688)
			BEGIN        
				SELECT TOP 1 @Result=[Final_Fail_Code] FROM [Euro6].[dbo].[ActF2NCVBoreSeatFace]
				WHERE [data_matrix] = @SerialNumber
				ORDER BY [PDate] DESC;
				IF @Result = NULL OR @@ROWCOUNT = 0
					BEGIN
						SET @Result = 999
						SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[ActF2NCVBoreSeatFace] for serial : '  + @SerialNumber
					END
				ELSE
					BEGIN
						IF @Result != 0
							BEGIN
								IF @Result = 1 
									BEGIN
										SET @ErrorDescription = 'FAILED'
									END 
								ELSE
									BEGIN
										SET @ErrorDescription = 'FAILED : Failure code ' + @Result
									END                                 
								END
							ELSE                          
								BEGIN
									SET @Result = 0
									SET @ErrorDescription = 'PASSED'
								END
					 END
			END
					
		---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        -- Injector Assembly
		-- Test the PPC on 20741 - ASSY MASMEC L3, 35824 - OC3 ASSY PILOT LINE/ L4, 20184 ASSY L2
        ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		IF @PlantNbr IN (20741,35824, 20184)
			BEGIN        
				IF @Parameter IN ('NCV')
				BEGIN
				
					DECLARE @RArmPress INT,/* @SA1BOND INT,*/ @RSelAssy INT
					
					--Banned NCV check START
					SET @ResultBN = 0 

					SELECT TOP 1 @PDateBN = [PDate], @ResultBN=[FailCode]
					FROM [Euro6].[dbo].[BannedNCVs]
					WHERE [SerialNumber] = @SerialNumber

					If @ResultBN = 1
					BEGIN
							SET @Result = 555
							SET @ErrorDescription = 'FAILED : Banned NCV!'
							RETURN;
					END
					--Banned NCV check END

					--block sa1 from next process
					--set @SA1BOND=0
					--SELECT TOP 1 @SA1BOND = 1
					--FROM [Euro6].[dbo].[SA1_NCV_OffloadComplete]  where Matrix =@SerialNumber

					
						SELECT TOP 1 @RArmPress = [Result] 
						FROM(
							SELECT [PDate], [Result] FROM [Euro6].[dbo].[ManualArmatureGaugeResults]--ManualArmatureGougeResults
							WHERE [DataMatrix] = @SerialNumber
							UNION all
							SELECT [PDate], [Result] FROM [ImportExternal].[dbo].[NCVArmaturePress]
							WHERE [DataMatrix] = @SerialNumber
							UNION all
							SELECT [PDate], [Result] FROM [Euro6].[dbo].[NCVArmaturePress]
							WHERE [DataMatrix] = @SerialNumber
							UNION all
							SELECT [PDate], [Result] FROM [Euro6].[dbo].[Zygo]
							WHERE [SerialNumber] = @SerialNumber
						) as armPress
						ORDER BY [PDate] DESC
					
					--END--added

					-- Extra check to make sure the NCVs passed Selective assembly (this should be done on armature press)
					-- SELECT TOP 1 @RSelAssy = [Result]
					-- FROM(
					-- 	SELECT [UTC] AS [PDate], CAST (CASE WHEN [FailureCode] = 1 THEN 0 ELSE [FailureCode] END AS INT) AS [Result] 
					-- 	FROM [Euro6].[dbo].[SelAssyAssembly]
					-- 	WHERE [BodyMatrix] = @SerialNumber
					-- 	UNION all
					-- 	SELECT [UTC] AS [PDate],CAST(CASE WHEN [PassedGuide] = 1 AND [PassedSeat] = 1 THEN 0 ELSE 1 END AS INT) AS [Result]
					-- 	FROM [SELASSEMBLY\SELASSY].[SA_Stats].[dbo].[NCV_MeasureComplete]
					-- 	WHERE [Matrix] = @SerialNumber
					-- ) AS selAssy
					-- ORDER BY [PDate] DESC

					SET @RSelAssy = 0
					
					IF (@RArmPress = 0 and @RSelAssy = 0 /* and @SA1BOND = 0*/)--added
					BEGIN 
						SET @Result = 0
					END
					--added
					--ELSE IF @SA1BOND = 1
					--BEGIN
					--	SET @Result = 999
					--	SET @ErrorDescription = 'BONDED : NCV came from SA1 '  + @SerialNumber
					--END
					--/added end
					ELSE
					BEGIN
						IF @RArmPress IS NULL OR @RSelAssy IS NULL 
						BEGIN
							SET @Result = 999
						END
						ELSE 
						BEGIN
							SET @Result = 1
						END	
					END

					IF @Result = NULL OR @@ROWCOUNT = 0
						BEGIN
							SET @Result = 999
							SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[NCVArmaturePress] or [Euro6].[dbo].[Zygo] for serial : '  + @SerialNumber
						END
					ELSE
						BEGIN
							IF @Result != 0
								BEGIN
									IF @Result = 1 
										BEGIN
											SET @ErrorDescription = 'FAILED'
										END 
									ELSE
										BEGIN
										
											--IF @SA1BOND = 0--added
											--BEGIN --added
											
												SET @ErrorDescription = 'FAILED : Failure code ' + @Result
												
											--END--added
										END                                 
								END
							ELSE                          
								BEGIN
									SET @Result = 0
									SET @ErrorDescription = 'PASSED'
								END
						END
				END
				ELSE
				IF @Parameter IN ('Nozzle')
				BEGIN
					DECLARE @ResultNNCTest SMALLINT 
					DECLARE @PDateNNCTest DateTime
					DECLARE @ResultZBLNTest SMALLINT
					DECLARE @PDateZBLNTest DateTime

					--Banned nozzles check START
					SET @ResultBN = 0 

					SELECT TOP 1 @PDateBN = [PDate], @ResultBN=[FailCode]
					FROM [Euro6].[dbo].[BannedNozzles]
					WHERE [SerialNumber] = @SerialNumber


					If @ResultBN = 1
					BEGIN
							SET @Result = 555
							SET @ErrorDescription = 'FAILED : Banned Nozzle!'
							RETURN;
					END
					----Banned Nozzles check END

					Select TOP 1 @PDateZBLNTest = [PDate], @ResultZBLNTest = [FailCode]
					FROM (
					SELECT   [PDate], [FailCode]
					FROM [Euro6].[dbo].[NozzleTestWithError_Results]
					WHERE [Serial] = @SerialNumber
					UNION ALL 
					SELECT  [PDate],  [FailCode]
					FROM [ImportExternal].[dbo].[NozzleTestWithError_Results]
					WHERE [Serial] = @SerialNumber
					) as a
					ORDER BY PDate DESC;
							
					SELECT TOP 1 @PDateNNCTest = [PDate], @ResultNNCTest=[OA_Result]
					FROM(					
					SELECT  [PDate], [OA_Result]
					FROM [NNCNozzleTest].[dbo].[Result]
					WHERE [Serial_No_Long] = @SerialNumber
					UNION ALL
					SELECT [PDate], [OA_Result]
					FROM [ImportExternal].[dbo].[NNCNozzleTest_Result]
					WHERE [Serial_No_Long] = @SerialNumber
					UNION ALL
					SELECT [PDate], [FailCode]
					FROM [Euro6].[dbo].[20474_SonplasNozzleTest_Results]
					WHERE [Serial] = @SerialNumber
					UNION ALL
					SELECT  [PDate], SUBSTRING(FAIL_CODE, CHARINDEX('(', FAIL_CODE) + 1, ABS(CHARINDEX(')', FAIL_CODE) - CHARINDEX('(', FAIL_CODE) - 1))
					FROM [NNCNozzleTest].[dbo].[20999_NozzleTest]
					WHERE [SERIAL_NUMBER] = @SerialNumber
					) as a
					ORDER BY [PDate] DESC;		

					IF @PDateNNCTest > @PDateZBLNTest or @PDateZBLNTest is null
					BEGIN
						SET @Result = @ResultNNCTest							
					END
					ELSE
					BEGIN
						SET @Result = @ResultZBLNTest
					END

					IF @Result IS NULL OR @@ROWCOUNT = 0
						BEGIN
							SET @Result = 999
							SET @ErrorDescription = 'FAILED : No Record Found for serial : '  + @SerialNumber
						END
					ELSE
						BEGIN
							IF @Result != 0
								BEGIN
									IF @Result = 1 
										BEGIN
											SET @ErrorDescription = 'FAILED'
										END 
									ELSE
										BEGIN
											SET @ErrorDescription = 'FAILED : Failure code ' + @Result
										END                                 
								END
							ELSE                          
								BEGIN
									SET @Result = 0
									SET @ErrorDescription = 'PASSED'
								END
						END
				END
				ELSE
				BEGIN
					SET @Result = 999
					SET @ErrorDescription = 'FAILED : Error in the Previous Process Request'
				END
			
			END
					
		---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        -- NNC Assy
		-- Test the PPC on OP400.1, OP400.2, OP400.3, OP400.4
        ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		--IF @PlantNbr IN (4001, 20751, 20752, 4004)
		--	BEGIN        
		--		DECLARE @EXHResult VARCHAR(MAX)
		--		DECLARE @EXHPdate DateTime

		--		IF @Parameter IN ('Nozzle')
		--		BEGIN
		--			DECLARE @NHResult VARCHAR(MAX)
		--			DECLARE @ONHResult VARCHAR(MAX)
					
		--			DECLARE @NHPDate DateTime
		--			DECLARE @ONHPDate DateTime		

		--			SELECT TOP 1 @NHResult = [FailCode], @NHPDate = [PDate] 
		--				FROM [Euro6].[dbo].[NozzleHegsProduction]
		--				WHERE [SerialNum] = @SerialNumber
		--			ORDER BY PDate desc

		--			SELECT TOP 1 @ONHResult = [UploadCode],  @ONHPDate = [PDate] 
		--				FROM [Euro6].[dbo].[OldNozzleHegsProduction]
		--				WHERE [SerialNum] = @SerialNumber
		--			ORDER BY [PDate] DESC;

		--			SELECT TOP 1 @EXHResult = EXH.[Failcode], @EXHPdate = EXH.[PDate] FROM [Euro6].[dbo].[20445_SonplasHEG_Measure_Results] AS EXH
		--			WHERE EXH.[Serial] = @SerialNumber
		--			or EXH.[Serial] = LOWER(@SerialNumber)
		--			ORDER BY EXH.[PDate] DESC;

		--			--Sudbury checks start

		--			--SELECT TOP 1 @NHResult = [FailCode], @NHPDate = [PDate] 
		--			--FROM [ImportExternal].[dbo].[NozzleHegsProduction]
		--			--WHERE [SerialNum] = @SerialNumber
		--			--ORDER BY PDate desc

		--			--Temporary check
		--			--SELECT TOP 1 @NHResult = [FailCode], @NHPDate = [PDate] 
		--			--FROM [ROIAS-LDB03\ARCHIVE].[Euro6].[dbo].[NozzleHegsProduction]
		--			--WHERE [SerialNum] = @SerialNumber
		--			--ORDER BY PDate desc

		--			--SELECT TOP 1 @ONHResult = [UploadCode],  @ONHPDate = [PDate] 
		--			--FROM [ImportExternal].[dbo].[OldNozzleHegsProduction]
		--			--WHERE [SerialNum] = @SerialNumber
		--			--ORDER BY [PDate] DESC;

		--			--SELECT TOP 1 @EXHResult = EXH.[Failcode], @EXHPdate = EXH.[PDate] FROM [ImportExternal].[dbo].[20445_SonplasHEG_Measure_Results] AS EXH
		--			--WHERE EXH.[Serial] = @SerialNumber
		--			--or EXH.[Serial] = LOWER(@SerialNumber)
		--			--ORDER BY EXH.[PDate] DESC;

		--			-- Sudbury checks end

		--			IF (@NHResult IS NULL AND @ONHResult IS NULL AND @EXHResult IS NULL)  
		--			BEGIN
		--				SET @Result = 999
		--				SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[NozzleHegsData] or [Euro6].[dbo].[OldNozzleHegs] or [Euro6].[dbo].[20445_SonplasHEG_Measure_Results] or Sudbury for serial : '  + @SerialNumber
		--			END
		--			ELSE
		--				BEGIN
		--					IF (@NHResult is NULL and @EXHResult is null) 
		--						BEGIN
		--							SET @Result = @ONHResult
		--						END 
		--					IF (@ONHResult is NULL and @EXHResult is null) 
		--						BEGIN
		--							SET @Result = @NHResult
		--						END
		--					IF (@NHResult is null  and @ONHResult is null) 
		--					BEGIN
		--						SET @Result = @EXHResult
		--					END
							
							
		--					IF (@NHPDate > @ONHPDate)
		--						BEGIN	
		--							if (@NHPDate > @EXHPDate)
		--								Begin						
		--									SET @Result = @NHResult	
		--								end								
		--						END
		--					if (@ONHPDate > @NHPDate)
		--						begin
		--							if (@ONHPDate > @EXHPDate)
		--								begin
		--									set @Result = @ONHResult
		--								end
		--						end

		--					if (@EXHPDate >  @ONHPDate)
		--						begin
		--							if (@EXHPDate > @NHPDate)
		--								begin 
		--									set @Result = @EXHResult
		--								end
		--						end
								
		--					IF (@Result != '0')
		--						BEGIN
		--							IF (@Result = '1') 
		--								BEGIN
		--									SET @ErrorDescription = 'FAILED'
		--								END 
		--							ELSE
		--								BEGIN
		--									IF (@Result = '(0)  IO')
		--										BEGIN
		--											SET @Result = 0
		--											SET @ErrorDescription = 'PASSED'
		--										END
		--									ELSE
		--										BEGIN
		--											SET @ErrorDescription = 'FAILED : Failure code ' + @Result
		--											SET @Result = 1
		--										END						
		--								END                                 
		--						END
		--					ELSE                          
		--						BEGIN
		--							SET @Result = 0
		--							SET @ErrorDescription = 'PASSED'
		--						END
		--				END
		--			END
		--			ELSE
		--			BEGIN
		--				IF @Parameter IN ('Piston Guide')
		--				BEGIN	

		--					DECLARE @CHResult VARCHAR(MAX)
		--					DECLARE @MCHResult VARCHAR(MAX)
						
		--					DECLARE @CHPdate DateTime
		--					DECLARE @MCHdate DateTime
							
											
		--					SELECT TOP 1 @CHResult=[FailCode], @CHPdate = CH.[PDate] FROM [Euro6].[dbo].[ComponentHegsWithIntErrorCode] as CH 
		--					WHERE CH.[Serial] = @SerialNumber
		--					ORDER BY CH.[PDate] DESC;

		--					SELECT TOP 1 @EXHResult =  SUBSTRING(FailCode, CHARINDEX('(', FailCode) + 1, ABS(CHARINDEX(')', FailCode) 
		--					- CHARINDEX('(', FailCode) - 1)),  @EXHPdate = EXH.[PDate] FROM [Euro6].[dbo].[20445_SonplasHEG_Measure_Results] AS EXH
		--					WHERE EXH.[Serial] = @SerialNumber
		--					ORDER BY EXH.[PDate] DESC; 
 

		--					SELECT TOP 1 @MCHResult = MCH.[UnloadCode],  @MCHdate = MCH.[PDate] FROM [Euro6].[dbo].[ManualComponentHeg] AS MCH
		--					WHERE MCH.[Serial] = @SerialNumber
		--					ORDER BY MCH.[PDate] DESC;


		--					DECLARE @TempTable TABLE
		--					(
		--						result VARCHAR(MAX),
		--						pDate datetime
		--					)


		--					INSERT INTO @TempTable (result, pDate) VALUES(@CHResult, @CHPdate)
		--					INSERT INTO @TempTable (result, pDate) VALUES(@EXHResult, @EXHPdate)
		--					INSERT INTO @TempTable (result, pDate) VALUES(@MCHResult, @MCHdate)

		--				   	select @Result = result from @TempTable order by pDate
							
		--					--autopasses
		--					SET @Result = 0
		--					SET @ErrorDescription = 'PASSED'

		--					/*
		--					IF (@Result is null)
		--						BEGIN
		--							SET @Result = '999'
		--							SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[ComponentHegs] or [Euro6].[dbo].[20445_SonplasHEG_Measure_Results] for serial : '  + @SerialNumber
		--						END
		--					*/


		--					/*
		--					IF (@CHResult is NULL and @EXHResult is null and @MCHResult is null)
		--						BEGIN
		--							SET @Result = '999'
		--							SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[ComponentHegs] or [Euro6].[dbo].[20445_SonplasHEG_Measure_Results] for serial : '  + @SerialNumber
		--						END
		--					ELSE
		--						BEGIN

		--						IF (@CHResult is NULL ) 
		--							BEGIN
									
		--								SET @Result = @EXHResult
		--							END 
		--						IF (@EXHResult is NULL) 
		--							BEGIN
		--								SET @Result = @CHResult
		--							END
							

		--						IF (@CHPdate > @EXHPdate)
		--							BEGIN									
		--								SET @Result = @CHResult								
		--							END
		--						ELSE
		--							BEGIN
		--								SET @Result =  @EXHResult
		--							END

		--							IF (@Result != '0')
		--						BEGIN
		--							IF (@Result = '1') 
		--								BEGIN
		--									SET @ErrorDescription = 'FAILED'
		--								END 
		--							ELSE
		--								BEGIN
		--									if (@Result = '(0)  IO')
		--										BEGIN
		--											SET @Result = 0
		--											SET @ErrorDescription = 'PASSED'
		--										END
		--									ELSE
		--										BEGIN
		--											SET @ErrorDescription = 'FAILED : Failure code ' + @Result
		--										END						
		--								END                                 
		--						END
		--					ELSE                          
		--						BEGIN
		--							SET @Result = 0
		--							SET @ErrorDescription = 'PASSED'
		--						END
		--					END
		--					*/
		--			END
		--		END				 
				
		--	END

		IF @PlantNbr IN (4001, 20751, 20752, 4004)
			BEGIN        
				DECLARE @EXHResult VARCHAR(MAX)
				DECLARE @EXHPdate DateTime

				IF @Parameter IN ('Nozzle')
				BEGIN
					DECLARE @NHResult VARCHAR(MAX)
					DECLARE @ONHResult VARCHAR(MAX)
					
					DECLARE @NHPDate DateTime
					DECLARE @ONHPDate DateTime		

					--Banned nozzles check START
					SET @ResultBN = 0 

					SELECT TOP 1 @PDateBN = [PDate], @ResultBN=[FailCode]
					FROM [Euro6].[dbo].[BannedNozzles]
					WHERE [SerialNumber] = @SerialNumber

					If @ResultBN = 1
					BEGIN
							SET @Result = 555
							SET @ErrorDescription = 'FAILED : Banned Nozzle!'
							RETURN;
					END
					--Banned Nozzles check END

					SELECT TOP 1 @NHResult = [FailCode], @NHPDate = [PDate] 
						FROM [Euro6].[dbo].[NozzleHegsProduction]
						WHERE [SerialNum] = @SerialNumber
					ORDER BY PDate desc

					SELECT TOP 1 @ONHResult = [UploadCode],  @ONHPDate = [PDate] 
						FROM [Euro6].[dbo].[OldNozzleHegsProduction]
						WHERE [SerialNum] = @SerialNumber
					ORDER BY [PDate] DESC;

					SELECT TOP 1 @EXHResult = EXH.[Failcode], @EXHPdate = EXH.[PDate] FROM [Euro6].[dbo].[20445_SonplasHEG_Measure_Results] AS EXH
					WHERE EXH.[Serial] = @SerialNumber
					or EXH.[Serial] = LOWER(@SerialNumber)
					ORDER BY EXH.[PDate] DESC;

					--Sudbury checks start

					--SELECT TOP 1 @NHResult = [FailCode], @NHPDate = [PDate] 
					--FROM [ImportExternal].[dbo].[NozzleHegsProduction]
					--WHERE [SerialNum] = @SerialNumber
					--ORDER BY PDate desc

					--Temporary check
					--SELECT TOP 1 @NHResult = [FailCode], @NHPDate = [PDate] 
					--FROM [ROIAS-LDB03\ARCHIVE].[Euro6].[dbo].[NozzleHegsProduction]
					--WHERE [SerialNum] = @SerialNumber
					--ORDER BY PDate desc

					--SELECT TOP 1 @ONHResult = [UploadCode],  @ONHPDate = [PDate] 
					--FROM [ImportExternal].[dbo].[OldNozzleHegsProduction]
					--WHERE [SerialNum] = @SerialNumber
					--ORDER BY [PDate] DESC;

					--SELECT TOP 1 @EXHResult = EXH.[Failcode], @EXHPdate = EXH.[PDate] FROM [ImportExternal].[dbo].[20445_SonplasHEG_Measure_Results] AS EXH
					--WHERE EXH.[Serial] = @SerialNumber
					--or EXH.[Serial] = LOWER(@SerialNumber)
					--ORDER BY EXH.[PDate] DESC;

					-- Sudbury checks end

					IF (@NHResult IS NULL AND @ONHResult IS NULL AND @EXHResult IS NULL)  
					BEGIN
						SET @Result = 999
						SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[NozzleHegsData] or [Euro6].[dbo].[OldNozzleHegs] or [Euro6].[dbo].[20445_SonplasHEG_Measure_Results] or Sudbury for serial : '  + @SerialNumber
					END
					ELSE
						BEGIN
							IF (@NHResult is NULL and @EXHResult is null) 
								BEGIN
									SET @Result = @ONHResult
								END 
							IF (@ONHResult is NULL and @EXHResult is null) 
								BEGIN
									SET @Result = @NHResult
								END
							IF (@NHResult is null  and @ONHResult is null) 
							BEGIN
								SET @Result = @EXHResult
							END
							
							
							IF (@NHPDate > @ONHPDate)
								BEGIN	
									if (@NHPDate > @EXHPDate)
										Begin						
											SET @Result = @NHResult	
										end								
								END
							if (@ONHPDate > @NHPDate)
								begin
									if (@ONHPDate > @EXHPDate)
										begin
											set @Result = @ONHResult
										end
								end

							if (@EXHPDate >  @ONHPDate)
								begin
									if (@EXHPDate > @NHPDate)
										begin 
											set @Result = @EXHResult
										end
								end
								
							IF (@Result != '0')
								BEGIN
									IF (@Result = '1') 
										BEGIN
											SET @ErrorDescription = 'FAILED'
										END 
									ELSE
										BEGIN
											IF (@Result = '(0)  IO')
												BEGIN
													SET @Result = 0
													SET @ErrorDescription = 'PASSED'
												END
											ELSE
												BEGIN
													SET @ErrorDescription = 'FAILED : Failure code ' + @Result
													SET @Result = 1
												END						
										END                                 
								END
							ELSE                          
								BEGIN
									SET @Result = 0
									SET @ErrorDescription = 'PASSED'
								END
						END
					END
					ELSE
					BEGIN
						IF @Parameter IN ('Piston Guide')
						BEGIN	
						   
						    --Remove the MY21 PG PPC check  
							IF LEFT (CAST(@SerialNumber AS VARCHAR(2)),2) IN ('28') 
								BEGIN
									SET @Result = 0
									RETURN;
								END

							DECLARE @CHResult VARCHAR(MAX)
							DECLARE @MCHResult VARCHAR(MAX)
						
							DECLARE @CHPdate DateTime
							DECLARE @MCHdate DateTime

							--Banned PG check START
							SET @ResultBN = 0 

							SELECT TOP 1 @PDateBN = [PDate], @ResultBN=[FailCode]
							FROM [Euro6].[dbo].[BannedPGs]
							WHERE [SerialNumber] = @SerialNumber

							If @ResultBN = 1
							BEGIN
									SET @Result = 555
									SET @ErrorDescription = 'FAILED : Banned PG!'
									RETURN;
							END
							--Banned PG check END
									
							SELECT TOP 1 @Result=[FailCode] FROM [Euro6].[dbo].[ComponentHegsWithIntErrorCode] as CH 
							WHERE [Serial] = @SerialNumber
							ORDER BY [PDate] DESC;

							IF (@Result is NULL)
							BEGIN
								SET @Result = '999'
								SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[ComponentHegs]: ' 
							END
							ELSE
								IF (@Result != '0')
								BEGIN
									IF (@Result = '1') 
										BEGIN
											SET @ErrorDescription = 'FAILED'
										END 
									ELSE
										BEGIN
											IF (@Result = '(0)  IO')
												BEGIN
													SET @Result = 0
													SET @ErrorDescription = 'PASSED'
												END
											ELSE
												BEGIN
													SET @ErrorDescription = 'FAILED : Failure code ' + @Result
												END						
										END                                 
								END
							ELSE                          
							BEGIN
								SET @Result = 0
								SET @ErrorDescription = 'PASSED'
							END
		
							--DECLARE @TempTable TABLE
							--(
							--	result VARCHAR(MAX),
							--	pDate datetime
							--)

							--INSERT INTO @TempTable (result, pDate) VALUES(@CHResult, @CHPdate)
							--INSERT INTO @TempTable (result, pDate) VALUES(@EXHResult, @EXHPdate)
							--INSERT INTO @TempTable (result, pDate) VALUES(@MCHResult, @MCHdate)

						    -- select @Result = result from @TempTable order by pDate
							
							--autopasses
							--SET @Result = 0
							--SET @ErrorDescription = 'PASSED'

							
							--IF (@Result is null)
							--	BEGIN
							--		SET @Result = '999'
							--		SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[ComponentHegs] or [Euro6].[dbo].[20445_SonplasHEG_Measure_Results] for serial : '  + @SerialNumber
							--	END
							
							END
					END
				END

		---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        -- NCV Selective Assembly
		-- Test the PPC on NCV Selective assembly 1- 2051, selective assembly 2- 20707 and selective assembly 3-20848
        ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		IF @PlantNbr IN (20523)
				BEGIN      
					IF (@RunSelect = 1)
					BEGIN        
						DECLARE @PrevTable VARCHAR(max)
				
						IF LEFT (CAST(@SerialNumber AS VARCHAR(2)),2) IN ('39') -- this is an OC3
						BEGIN
							SELECT TOP 1 @Result=[Result] FROM [Euro6].[dbo].[NCVStemholeDeburr]
							WHERE [Serial] = @SerialNumber
							ORDER BY [PDate] DESC;
							SET @PrevTable = '[NCVStemholeDeburr]'
						END
						ELSE IF LEFT (CAST(@SerialNumber AS VARCHAR(2)),2) IN ('30','34') -- this is an C6R or a F2P
						BEGIN
							SELECT TOP 1 @Result=[Result] FROM [Euro6].[dbo].[HighPressureDeburr]
							WHERE [SerialNumber] = @SerialNumber
							ORDER BY [PDate] DESC;
							SET @PrevTable = '[HighPressureDeburr]'
						END

						IF @Result = NULL OR @@ROWCOUNT = 0
						BEGIN
							SET @Result = 999
							SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].' + @PrevTable + ' for serial : '  + @SerialNumber
						END
						ELSE
						BEGIN
							IF (@Result = 0)
							BEGIN
								SET @ErrorDescription = 'PASSED'                       
							END
							ELSE                          
							BEGIN
								IF (@Result = 1)
								BEGIN
									SET @ErrorDescription = 'FAILED'
								END 
								ELSE
								BEGIN
									SET @ErrorDescription = 'FAILED : Failure code ' + @Result
								END 
							END
						END
					END
					ELSE 
					BEGIN
						IF @Parameter IN ('OC3')
						BEGIN
							SELECT TOP 1 @Result=[Result] FROM [Euro6].[dbo].[NCVStemholeDeburr]
							WHERE [Serial] = @SerialNumber
							ORDER BY [PDate] DESC;
							IF @Result = NULL OR @@ROWCOUNT = 0
							BEGIN
								SET @Result = 999
								SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[NCVStemholeDeburr] for serial : '  + @SerialNumber
							END
							ELSE
							BEGIN
								IF (@Result = 0)
								BEGIN
									SET @ErrorDescription = 'PASSED'                       
								END
								ELSE                          
								BEGIN
									IF (@Result = 1)
									BEGIN
										SET @ErrorDescription = 'FAILED'
									END 
									ELSE
									BEGIN
										SET @ErrorDescription = 'FAILED : Failure code ' + @Result
									END 
								END
							END
						END
						ELSE
						BEGIN
							IF @Parameter IN ('C6R')
							BEGIN
								SELECT TOP 1 @Result=[Result] FROM [Euro6].[dbo].[HighPressureDeburr]
								WHERE [SerialNumber] = @SerialNumber
								ORDER BY [PDate] DESC;
								IF @Result = NULL OR @@ROWCOUNT = 0
								BEGIN
									SET @Result = 999
									SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[HighPressureDeburr] for serial : '  + @SerialNumber
								END
								ELSE
								BEGIN
									IF (@Result = 0)
									BEGIN
										SET @ErrorDescription = 'PASSED'                       
									END
									ELSE                          
									BEGIN
										IF (@Result = 1)
										BEGIN
											SET @ErrorDescription = 'FAILED'
										END 
										ELSE
										BEGIN
											SET @ErrorDescription = 'FAILED : Failure code ' + @Result
										END 
									END
								END
							END
							ELSE IF @Parameter IN ('F2P')
							BEGIN
								SELECT TOP 1 @Result=[Result] FROM [Euro6].[dbo].[HighPressureDeburr]
								WHERE [SerialNumber] = @SerialNumber
								ORDER BY [PDate] DESC;
								IF @Result = NULL OR @@ROWCOUNT = 0
								BEGIN
									SET @Result = 999
									SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[HighPressureDeburr] for serial : '  + @SerialNumber
								END
								ELSE
								BEGIN
									IF (@Result = 0)
									BEGIN
										SET @ErrorDescription = 'PASSED'                       
									END
									ELSE                          
									BEGIN
										IF (@Result = 1)
										BEGIN
											SET @ErrorDescription = 'FAILED'
										END 
										ELSE
										BEGIN
											SET @ErrorDescription = 'FAILED : Failure code ' + @Result
										END 
									END
								END
							END
						ELSE
						BEGIN
							SELECT TOP 1 @Result=[Result] FROM [Euro6].[dbo].[HighPressureDeburr]
							WHERE [SerialNumber] = @SerialNumber
							ORDER BY [PDate] DESC;
							IF @Result = NULL OR @@ROWCOUNT = 0
							BEGIN
								SET @Result = 999 -- TODO:: Change this back to 999 when requested. (Adam Cleaver 26/10/2015)
								SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[HighPressureDeburr] for serial : '  + @SerialNumber
							END
							ELSE
							BEGIN
								IF (@Result = 0)
								BEGIN
									SET @ErrorDescription = 'PASSED'                       
								END
								ELSE                          
								BEGIN
									IF (@Result = 1)
									BEGIN
										SET @ErrorDescription = 'FAILED'
									END 
									ELSE
									BEGIN
										SET @ErrorDescription = 'FAILED : Failure code ' + @Result
									END 
								END
							END
						END
					END
				END
				IF (@Result = 0 AND @Parameter IN ('F2P', 'OC3' ))
				BEGIN
					SELECT TOP 1 @Result = FailCode
					FROM
					(
						SELECT [PlantNumber] as AssetNumber
							  ,[PDate]
							  ,[Serial] COLLATE DATABASE_DEFAULT as Serial
							  ,[UnLoadCode] as FailCode
						FROM [Euro6].[dbo].[ManualComponentHeg]
			
						UNION 

						SELECT [AssetNumber]
							,[PDate]
							,[Serial]COLLATE DATABASE_DEFAULT as Serial
							,[FailCode]      
						FROM [Euro6].[dbo].[ComponentHegsWithIntErrorCode]
					) as UT
					WHERE Serial = @SerialNumber
					ORDER BY PDate DESC

					IF @Result = NULL OR @@ROWCOUNT = 0
					BEGIN
						SET @Result = 999
						SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[ComponentHegsWithIntErrorCode] or [Euro6].[dbo].[ManualComponentHeg] for serial : '  + @SerialNumber
					END
					IF (@Result = 1)
					BEGIN
						SET @ErrorDescription = 'FAILED'
					END 
				END 
			END

	    ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        -- NCV Assembly
		-- Test the PPC on Armature Press, Zygo- 20524, Manual Armature Press- 20366, Automatic Armature Press- 20659
        ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		IF @PlantNbr IN (20659, 20366, 20524)
			BEGIN        
				DECLARE @lastSelAssyDate datetime
				begin try
					SELECT TOP 1 @Result = [Result], @lastSelAssyDate = [PDate] 
					FROM(	-- Testing the assembly result table on Selective assembly 2
						SELECT [UTC] AS [PDate],
							CASE CAST (CASE WHEN [FailureCode] = 1 THEN 0 ELSE [FailureCode] END AS INT) -- CASE stateemnt to define wich code to diplay between body and assembly
							WHEN [Result] THEN [Result] -- both the same display any (body)
							WHEN 0 THEN [Result] -- assembly at 0 display body
							WHEN CAST (CASE WHEN [FailureCode] = 1 THEN 0 ELSE [FailureCode] END AS INT) + [Result] THEN [FailureCode] -- add assemby and boddy compare to assembly, that means body is 0 display assembly
							ELSE [FailureCode] -- else dispaly assembly
							END as [Result]
	
						FROM [Euro6].[dbo].[SelAssyAssembly] as Ass
						inner Join (	-- Making sure it passed the body mesurement 
										SELECT TOP 1 [UTC] AS [PDate], CAST (CASE WHEN [FailureCode] = 1 THEN 0 ELSE [FailureCode] END AS INT) AS [Result], [BodyMatrix]	
										FROM [Euro6].[dbo].[SelAssyBody]
										WHERE [BodyMatrix] = @SerialNumber
										ORDER BY PDate desc
										) as Body on Ass.[BodyMatrix] = Body.[BodyMatrix]
						WHERE Ass.[BodyMatrix] = @SerialNumber

					--SA3 CHECK 
					UNION ALL
					SELECT [UTC] AS [PDate],CAST (CASE WHEN [FailureCode] = 1 THEN 0 ELSE [FailureCode] END AS INT) AS [Result]
					FROM [Euro6].[dbo].[SA3_SelAssyAssembly]
					WHERE [BodyMatrix]=@SerialNumber
					
					-- TODO::
					
					-- UNION all
					-- -- Testing the Final measurement on Selective assembly 1
					-- SELECT [UTC] AS [PDate],CAST(CASE WHEN [PassedGuide] = 1 AND [PassedSeat] = 1 THEN 0 ELSE 1 END AS INT) AS [Result]
					-- FROM [SELASSEMBLY\SELASSY].[SA_Stats].[dbo].[NCV_MeasureComplete]
					-- WHERE [Matrix] = @SerialNumber
					

					) AS selAssy
					ORDER BY PDate DESC
				END TRY
					 BEGIN CATCH
							  SELECT TOP 1 @Result = [Result], @lastSelAssyDate = [PDate] 
						FROM(	-- Testing the assembly result table on Selective assembly 2
							SELECT [UTC] AS [PDate],
								CASE CAST (CASE WHEN [FailureCode] = 1 THEN 0 ELSE [FailureCode] END AS INT) -- CASE stateemnt to define wich code to diplay between body and assembly
								WHEN [Result] THEN [Result] -- both the same display any (body)
								WHEN 0 THEN [Result] -- assembly at 0 display body
								WHEN CAST (CASE WHEN [FailureCode] = 1 THEN 0 ELSE [FailureCode] END AS INT) + [Result] THEN [FailureCode] -- add assemby and boddy compare to assembly, that means body is 0 display assembly
								ELSE [FailureCode] -- else dispaly assembly
								END AS [Result]
	
							FROM [Euro6].[dbo].[SelAssyAssembly] AS Ass
							INNER JOIN (	-- Making sure it passed the body mesurement 
											SELECT TOP 1 [UTC] AS [PDate], CAST (CASE WHEN [FailureCode] = 1 THEN 0 ELSE [FailureCode] END AS INT) AS [Result], [BodyMatrix]	
											FROM [Euro6].[dbo].[SelAssyBody]
											WHERE [BodyMatrix] = @SerialNumber
											ORDER BY PDate DESC
											) AS Body ON Ass.[BodyMatrix] = Body.[BodyMatrix]
							WHERE Ass.[BodyMatrix] = @SerialNumber
										) AS selAssy
							ORDER BY PDate DESC
					 END CATCH
				-- Display the Code '111' if failed more than 3 time the armature press.
				IF @Result = '0'
				BEGIN
					DECLARE @failCount INT
					SELECT @failCount = count(*)
					FROM(
						SELECT [PDate] FROM [Euro6].[dbo].[NCVArmaturePress] -- check Armature press DB
						WHERE [DataMatrix] = @SerialNumber
						AND [PDate] > @lastSelAssyDate
						AND [Result] != '0'
						UNION all
						SELECT [PDate] FROM [Euro6].[dbo].[ManualArmaturePressResults] -- check Manual Armature press DB
						WHERE [DataMatrix] = @SerialNumber
						AND [PDate] > @lastSelAssyDate
						AND [Press_Result] != '0'
						UNION all
						SELECT [PDate] FROM [Euro6].[dbo].[Zygo] -- check Zygo (manual assembly) DB
						WHERE [SerialNumber] = @SerialNumber
						AND [PDate] > @lastSelAssyDate
						AND [Result] != '0'
					) as armPress

					IF @failCount > 3 -- compare the count with 3
					BEGIN
						SET @Result = '111'
					END
				END
				IF @Result IS NULL 
					BEGIN
						SET @Result = 999
						SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[SelAssyAssembly] or [SELASSEMBLY\SELASSY].[SA_Stats].[dbo].[NCV_MeasureComplete] for serial : '  + @SerialNumber
					END
				ELSE
					BEGIN
						IF @Result != 0
							BEGIN
								IF @Result = 1 
									BEGIN
										SET @ErrorDescription = 'FAILED'
									END 
								ELSE
									BEGIN
										SET @ErrorDescription = 'FAILED : Failure code ' + @Result
									END                                 
							END
						ELSE                          
							BEGIN
								SET @Result = 0
								SET @ErrorDescription = 'PASSED'
							END
					END
			END

		---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        -- Injector FOFF
		-- Test the PPC on the Scan&Pack
        ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		--IF @PlantNbr IN (20319, 20318)
		--	BEGIN
		--	DECLARE @AssemblyDate DATETIME
		--	DECLARE @ISN VARCHAR(MAX)			
		--	DECLARE @NozzleSerial VARCHAR(MAX)
		--	DECLARE @NCVSerial VARCHAR(MAX)
		--	DECLARE @PistonGuideSerial VARCHAR(MAX)

		--		IF @Parameter IN ('MX11','MX13','MY17','DDi')
		--		BEGIN 
		--			SET @Result = 0
		--			SET @ErrorDescription = 'N/A' 
		--		END
		--		IF @Parameter IN ('MDEG')
		--		BEGIN
				
		--			SET @Result = 0
		--			SET @ErrorDescription = 'N/A' 
		--		END
		--	END

	    IF @PlantNbr IN (20319, 20318)
			BEGIN
				DECLARE @AssemblyDate DATETIME
				DECLARE @ISN VARCHAR(MAX)			
				DECLARE @NozzleSerial VARCHAR(MAX)
				DECLARE @NCVSerial VARCHAR(MAX)
				DECLARE @PistonGuideSerial VARCHAR(MAX)

				DECLARE @ResultBannedNozzle SMALLINT 
				DECLARE @ResultBannedNCV SMALLINT 
				DECLARE @ResultBannedPG SMALLINT 

					IF @Parameter IN ('MX11', 'MX13', 'DDi')
					BEGIN
						SELECT TOP(1)
							@AssemblyDate = PDate, 
							@ISN = ISN, 
							@NozzleSerial =  NozzleSerial, 
							@NCVSerial = NCVSerial, 
							@PistonGuideSerial = PGSerial
						FROM [Euro6].[dbo].[vw_AssemblyComponentTotals]
						WHERE ISN=@SerialNumber
						ORDER BY PDate DESC
					END
					IF @Parameter IN ('MDEG')
					BEGIN
						SELECT TOP(1)
							@AssemblyDate = PDate, 
							@ISN = Daimler_ISN, 
							@NCVSerial = NCVSerial, 
							@NozzleSerial = NozzleSerial, 
							@PistonGuideSerial = PGSerial
						FROM [Euro6MDEGTest].[dbo].[vw_AssemblyComponentTotalsMDEG]	
						WHERE Daimler_ISN=@SerialNumber
						ORDER BY PDate DESC
					END

					SET @ResultBannedNozzle = 0 
					SET @ResultBannedNCV = 0 
					SET @ResultBannedPG = 0 

					SELECT TOP 1  @ResultBannedNozzle=[FailCode]
					FROM [Euro6].[dbo].[BannedNozzles]
					WHERE [SerialNumber] = @NozzleSerial
				
					SELECT TOP 1  @ResultBannedNCV=[FailCode]
					FROM [Euro6].[dbo].[BannedNCVs]
					WHERE [SerialNumber] = @NCVSerial
				
					SELECT TOP 1 @ResultBannedPG=[FailCode]
					FROM [Euro6].[dbo].[BannedPGs]
					WHERE [SerialNumber] = @PistonGuideSerial

					If @ResultBannedNozzle = 1 or @ResultBannedNCV = 1 or @ResultBannedPG = 1
						BEGIN
								SET @Result = 3
								SET @ErrorDescription = 'Injector with  components for AfterMarket!'
								RETURN;
						END
						ELSE
						BEGIN
								SET @Result = 4
								SET @ErrorDescription = 'Injector with  fresh components!'
								RETURN;
						END
				
				END

		---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        -- Nozzle MCH - NFT
		-- Test the PPC on OP370
        ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		IF @PlantNbr IN (18932, 18933)
				
				BEGIN        
					SELECT TOP 1 @Result=a.[Failcode] FROM 
					(select SerialNum,UploadCode as [Failcode], PDate from [Euro6].[dbo].[OldNozzleHegsProduction]  WHERE [SerialNum] = @SerialNumber
					union all select serialnum, failcode as [Failcode], PDate from [Euro6].[dbo].[NozzleHegsProduction] WHERE [SerialNum] = @SerialNumber
					union all select serialnum, failcode as [FailCode], PDate from [ImportExternal].[dbo].[NozzleHegsProduction] WHERE [SerialNum] = @SerialNumber
					) as a
					
					ORDER BY [PDate] DESC;

					IF @Result = NULL OR @@ROWCOUNT = 0
						BEGIN
							SET @Result = 999
							SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[OldNozzleHegsProduction] for serial : '  + @SerialNumber
						END
					ELSE
						BEGIN
							IF @Result != 0
								BEGIN
									IF @Result = 1 
										BEGIN
										
											SET @ErrorDescription = 'FAILED'
										END 
									ELSE
										BEGIN
											SET @ErrorDescription = 'FAILED : Failure code ' + @Result
										END                                 
								END
							ELSE                          
								BEGIN
									SET @Result = 0
									SET @ErrorDescription = 'PASSED'
								END
						END
				END

		---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        -- Nozzle MCH
		-- Test the PPC on OP354- Nozzle Guide Control
        ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
		IF @PlantNbr IN (354000)
				BEGIN        

					SELECT TOP 1 @ONHResult = [UploadCode],  @ONHPDate = [PDate] 
						FROM [Euro6].[dbo].[OldNozzleHegsProduction]
						WHERE [SerialNum] = @SerialNumber
					ORDER BY [PDate] DESC;
					IF ( @ONHResult IS NULL)  
					BEGIN
						SET @Result = 999
						SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[OldNozzleHegs] for serial : '  + @SerialNumber
					END
					ELSE
						BEGIN
							IF (@Result != '0')
								BEGIN
									IF (@Result = '1') 
										BEGIN
											SET @ErrorDescription = 'FAILED'
										END 
									ELSE
										BEGIN
											IF (@Result = '(0)  IO')
												BEGIN
													SET @Result = 0
													SET @ErrorDescription = 'PASSED'
												END
											ELSE
												BEGIN
													SET @ErrorDescription = 'FAILED : Failure code ' + @Result
													SET @Result = 1
												END						
										END                                 
								END
							ELSE                          
								BEGIN
									SET @Result = 0
									SET @ErrorDescription = 'PASSED'
								END
						END
				END

		SET NOCOUNT OFF;
	END


IF (@RunSelect = 1)
BEGIN
	SELECT @Result, @ErrorDescription
END

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- TheEnd
-- Record [_ObjectUsage]
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--BEGIN TRY       
--	DECLARE @Host		VARCHAR(50)		=	LEFT(ISNULL(HOST_NAME(), '<NULL>'), 50);
--	DECLARE @User		VARCHAR(50)		=	LEFT(ISNULL(SUSER_NAME(), '<NULL>'), 50);
--	DECLARE @Object		VARCHAR(100)	=	LEFT('[dbo].[sp_PreviousProcessCheck]', 100);
--	DECLARE @In			VARCHAR(MAX)	=	'@SerialNumber=' + CAST(@SerialNumber AS VARCHAR(MAX)) + ', ' + '@PlantNumber=' + CAST(@PlantNumber AS VARCHAR(MAX));
--	DECLARE @Out		VARCHAR(MAX)	=	'@Result=' + CAST(@Result AS VARCHAR(MAX)) + ', ' + '@ErrorDescription=' + CAST(@ErrorDescription AS VARCHAR(MAX));
--	DECLARE @duration	TIME			=	CAST((GETDATE() - @startTime) AS TIME);
--	INSERT INTO [dbo].[_ObjectUsage] ([HostName], [UserName], [Object], [In], [Out], [Duration]) VALUES (@Host, @User, @Object, @In, @Out, @duration);
--END TRY
--BEGIN CATCH
--	-- Do Nothing      
--END CATCH


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[sp_PreviousProcessCheck] TO [dbcache]
    AS [dbo];

