﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MASMEC_GetWeeklyData] 
	@Start datetime,
	@End datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 
    SET DATEFIRST 6; -- SATURDAY first day of week
	
SELECT  
	--ProductionData.Year,
	ProductionData.Week, 
	Total,
	(Total - ISNULL(MX11, 0)) as MX13,
	ISNULL(MX11, 0) as MX11, 
	ProductionData.WeekStart,
	ROUND((1-(FailureData.FailCount / CAST(Total as FLOAT))) * 100, 1) as PassRate
FROM
(
	SELECT
		Year,		
		Week,  
		COUNT(*) as Total,
		CONVERT(Date, Min (Pdate)) as WeekStart
	FROM (
		SELECT Injector_Type, Result, PDate, DATEPART(YY, DATEADD(N, 150, PDate)) as Year,
			DATEPART(WW, DATEADD(N, 150, PDate)) as Week -- adjusted week -2hr:30 from Sat 00:00 
		FROM MASMEC_SelAsmb_OP30C_Results
		WHERE PDate BETWEEN @Start AND @End AND Result = 0 -- 0 = pass
	) as Data
	GROUP BY Year, Week 
) as ProductionData
FULL JOIN
(
	SELECT Week,  COUNT(*) as MX11, CONVERT(Date, Min (Pdate)) as WeekStart
	FROM (
		SELECT Injector_Type, Result, PDate, DATEPART(WW, DATEADD(N, 150, PDate)) as Week -- adjusted week -2hr:30 from Sat 00:00 
		FROM MASMEC_SelAsmb_OP30C_Results
		WHERE PDate BETWEEN @Start AND @End 
			AND Result = 0 -- 0 = pass
			AND Injector_Type = 2 --MX11
	) as Data
	GROUP BY Week 
) as MX11Data on ( ProductionData.Week = MX11Data.Week) 
LEFT JOIN
(
	SELECT Week, COUNT(*) as FailCount
	FROM 
	(
		SELECT Pdate, DATEPART(WW, DATEADD(N, 150, PDate)) as Week
		FROM MASMEC_SelAsmb_OP10_Results
		WHERE Pdate BETWEEN @Start and @End AND Result = 1
		UNION
		SELECT Pdate, DATEPART(WW, DATEADD(N, 150, PDate)) as Week
		FROM MASMEC_SelAsmb_OP20A_Results
		WHERE Pdate BETWEEN @Start and @End AND Result = 1
		UNION
		SELECT Pdate, DATEPART(WW, DATEADD(N, 150, PDate)) as Week
		FROM MASMEC_SelAsmb_OP20B_Results
		WHERE Pdate BETWEEN @Start and @End AND Result = 1
		UNION
		SELECT Pdate, DATEPART(WW, DATEADD(N, 150, PDate)) as Week
		FROM MASMEC_SelAsmb_OP30A_Results
		WHERE Pdate BETWEEN @Start and @End AND Result = 1
		UNION
		SELECT Pdate, DATEPART(WW, DATEADD(N, 150, PDate)) as Week
		FROM MASMEC_SelAsmb_OP30B_Results
		WHERE Pdate BETWEEN @Start and @End AND Result = 1
		UNION
		SELECT Pdate, DATEPART(WW, DATEADD(N, 150, PDate)) as Week
		FROM MASMEC_SelAsmb_OP30C_Results
		WHERE Pdate BETWEEN @Start and @End AND Result = 1
	) as Data
	GROUP BY Week
) as FailureData on ( ProductionData.Week = FailureData.Week)
ORDER BY 
	ProductionData.Year Asc,
	ProductionData.Week Asc

END

GO
GRANT EXECUTE
    ON OBJECT::[dbo].[sp_MASMEC_GetWeeklyData] TO [dbcache]
    AS [dbo];

