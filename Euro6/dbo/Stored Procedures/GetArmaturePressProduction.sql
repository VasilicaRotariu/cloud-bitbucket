﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetArmaturePressProduction]
	@Start datetime, @End datetime
AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(*) as Total
	FROM NCVArmaturePress
	WHERE Result = 0 
		AND PDate BETWEEN @Start AND @End
END
