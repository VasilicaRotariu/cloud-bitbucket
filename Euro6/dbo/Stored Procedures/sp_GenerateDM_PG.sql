﻿

-- =============================================
-- Author:		<Rotariu,Vasilica>
-- Create date: <10/10/2019>
-- Description:	<Stored procedure used to retrieve the code to engrave the PG part.>
-- =============================================

CREATE PROCEDURE [dbo].[sp_GenerateDM_PG]
(
                @Type_ VARCHAR(MAX) = NULL,                             
                @Val_Out VARCHAR(MAX) OUT, 
                @Status VARCHAR(MAX) OUT
              )
AS 
                BEGIN
						-- SET NOCOUNT ON added to prevent extra result sets from
						-- interfering with SELECT statements.
						SET NOCOUNT ON;

                        DECLARE @Seq_ VARCHAR(5)
                        DECLARE @Week_ VARCHAR(2)
                        DECLARE @Year_ VARCHAR(2)

						DECLARE @Val_Counter_ INT

						SET @Status=1;
						If LEN(@Type_) <> 2
						BEGIN
							SET @Status=-1;
							RETURN;
						END;

						SET @Seq_ = RIGHT(CONCAT('00000',NEXT VALUE FOR [DBO].[SEQ_DM_PG]),5); --incremental number
						SET @Week_= (DATEPART(WEEK,getdate())); --  week number in format WW
						SET @Year_ = RIGHT(YEAR(getdate()), 2); -- year in format YY

						SET @Val_Out = CONCAT(@Type_,@Seq_,@Week_,@Year_);-- DMX content using concatenation 
	
	
	--check if DMX generated is duplicated

	SELECT @Val_Counter_ = COUNT(*)
	FROM [dbo].[LaserMarkingResults]
	WHERE [Serial] = @Val_Out;

	IF @Val_Counter_ > 0
		BEGIN
			SET @Status=-4;
			SET @Val_Out='';
			SET @Val_Counter_=0;
		END
	
	BEGIN TRY  
		SELECT ERROR_NUMBER() AS ErrorNumber		
	END TRY  
	BEGIN CATCH  
		SET @Status=-10; --if we get an SQL error, the status will be -10
	END CATCH; 					
		

END
