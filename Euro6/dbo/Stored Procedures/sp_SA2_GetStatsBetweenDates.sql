﻿-- =============================================
-- Author:		mark boreham
-- Create date: today
-- Description:	gets the pass rate data for SA2 between two dates
-- Dates are in UTC format.
-- =============================================
CREATE PROCEDURE [dbo].[sp_SA2_GetStatsBetweenDates] 
	-- Add the parameters for the stored procedure here
	@DateStart  datetime2(7), 
	@DateEnd  datetime2(7)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT
	(@DateStart) as DateStart,
	(@DateEnd) as DateEnd,
	(datediff (minute, @DateStart, @DateEnd)) as Interval,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_PinReserveLocation] WHERE [PinReserveLocationStatUTC] BETWEEN @DateStart AND @DateEnd) as PinOnload_Count,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_PinReserveLocation] WHERE [PinReserveLocationStatUTC] BETWEEN @DateStart AND @DateEnd AND [PinReserveLocationDetermination] <> 0) as PinOnload_Fail,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_PinGuide] WHERE [PinGuideStatUTC] BETWEEN @DateStart AND @DateEnd) as PinGuide_Count,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_PinGuide] WHERE [PinGuideStatUTC] BETWEEN @DateStart AND @DateEnd AND ([PinGuideDiameterAveDetermination] <> 0 OR [PinGuideDiameterTopDetermination] <> 0 OR [PinGuideDiameterBtmDetermination] <> 0)) as PinGuide_Fail,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_PinGuide] WHERE [PinGuideStatUTC] BETWEEN @DateStart AND @DateEnd AND [PinGuideTaperDetermination] <> 0 ) as PinGuideTaper_Fail,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_PinSeat] WHERE [PinSeatStatUTC] BETWEEN @DateStart AND @DateEnd) as PinSeat_Count,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_PinSeat] WHERE [PinSeatStatUTC] BETWEEN @DateStart AND @DateEnd AND [PinSeatLengthDetermination] <> 0) as PinSeat_Fail,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_PinCollar] WHERE [PinCollarStatUTC] BETWEEN @DateStart AND @DateEnd) as PinCollar_Count,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_PinCollar] WHERE [PinCollarStatUTC] BETWEEN @DateStart AND @DateEnd AND [PinCollarDiameterDetermination] <> 0) as PinCollar_Fail,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_PinCacheFill] WHERE [PinCacheFillStatUTC] BETWEEN @DateStart AND @DateEnd) as PinCacheFill_Count,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_PinCacheFill] WHERE [PinCacheFillStatUTC] BETWEEN @DateStart AND @DateEnd AND [CacheFillDetermination] <> 0) as PinCacheFill_Fail,
	
	(0) as BodyOnload_Count,
	--(0) as BodyOnload_Fail,
	--(SELECT COUNT(*) FROM (SELECT DISTINCT BodyMatrix FROM [Euro6].[dbo].[SelAssyBody] WHERE BodyMatrix <> '#ERR' AND UTC BETWEEN @DateStart AND @DateEnd UNION ALL SELECT BodyMatrix FROM [Euro6].[dbo].[SelAssyBody]  WHERE  BodyMatrix = '#ERR' AND UTC BETWEEN @DateStart AND @DateEnd) A) as BodyOnload_Count,
	(SELECT COUNT(*) FROM [dbo].[SelAssyBody] WHERE BodyMatrix = '#ERR' AND BodySeatLength = 0 AND UTC BETWEEN @DateStart AND @DateEnd) as BodyOnload_Fail,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_BodyGuide] WHERE [BodyGuideStatUTC] BETWEEN @DateStart AND @DateEnd) as BodyGuide_Count,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_BodyGuide] WHERE [BodyGuideStatUTC] BETWEEN @DateStart AND @DateEnd AND ([BodyGuideDiameterAveDetermination] <> 0 OR [BodyGuideDiameterTopDetermination] <> 0 OR [BodyGuideDiameterBtmDetermination] <> 0)) as BodyGuide_Fail,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_BodyGuide] WHERE [BodyGuideStatUTC] BETWEEN @DateStart AND @DateEnd AND [BodyGuideTaperDetermination] <> 0 ) as BodyGuideTaper_Fail,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_BodySeat] WHERE [BodySeatStatUTC] BETWEEN @DateStart AND @DateEnd) as BodySeat_Count,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_BodySeat] WHERE [BodySeatStatUTC] BETWEEN @DateStart AND @DateEnd AND [BodySeatLengthDetermination] <> 0) as BodySeat_Fail,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_BodyCollar] WHERE [BodyCollarStatUTC] BETWEEN @DateStart AND @DateEnd) as BodyCollar_Count,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_BodyCollar] WHERE [BodyCollarStatUTC] BETWEEN @DateStart AND @DateEnd AND [BodyCollarDiameterDetermination] <> 0) as BodyCollar_Fail,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_BodyMatchPin] WHERE [BodyMatchPinStatUTC] BETWEEN @DateStart AND @DateEnd) as BodyMatchPin_Count,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_BodyMatchPin] WHERE [BodyMatchPinStatUTC] BETWEEN @DateStart AND @DateEnd AND [BodyMatchPinDetermination] <> 0) as BodyMatchPin_Fail,

	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_AssemblyInsertion] WHERE [AssemblyInsertionStatUTC] BETWEEN @DateStart AND @DateEnd) as AssyInsertion_Count,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_AssemblyInsertion] WHERE [AssemblyInsertionStatUTC] BETWEEN @DateStart AND @DateEnd AND [AssemblyInsertionLengthDetermination] <> 0) as AssyInsertion_Fail,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_AssemblySeatLift] WHERE [AssemblySeatLiftStatUTC] BETWEEN @DateStart AND @DateEnd) as AssyLift_Count,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_AssemblySeatLift] WHERE [AssemblySeatLiftStatUTC] BETWEEN @DateStart AND @DateEnd AND [AssemblySeatLiftDetermination] <> 0) as AssyLift_Fail,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_AssemblyGuideClearance] WHERE [AssemblyGuideClearanceStatUTC] BETWEEN @DateStart AND @DateEnd) as AssyGuide_Count,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_AssemblyGuideClearance] WHERE [AssemblyGuideClearanceStatUTC] BETWEEN @DateStart AND @DateEnd AND [AssemblyGuideClearanceDetermination] <> 0) as AssyGuide_Fail,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_AssemblyBtmSeatLeak] WHERE [AssemblyBtmSeatLeakStatUTC] BETWEEN @DateStart AND @DateEnd) as AssyBtmSeat_Count,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_AssemblyBtmSeatLeak] WHERE [AssemblyBtmSeatLeakStatUTC] BETWEEN @DateStart AND @DateEnd AND [AssemblyBtmSeatLeakDetermination] <> 0) as AssyBtmSeat_Fail,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_AssemblyTopSeatLeak] WHERE [AssemblyTopSeatLeakStatUTC] BETWEEN @DateStart AND @DateEnd) as AssyTopSeat_Count,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_AssemblyTopSeatLeak] WHERE [AssemblyTopSeatLeakStatUTC] BETWEEN @DateStart AND @DateEnd AND [AssemblyTopSeatLeakDetermination] <> 0) as AssyTopSeat_Fail,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_AssemblyCollarClearance] WHERE [AssemblyCollarClearanceStatUTC] BETWEEN @DateStart AND @DateEnd) as AssyCollar_Count,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_AssemblyCollarClearance] WHERE [AssemblyCollarClearanceStatUTC] BETWEEN @DateStart AND @DateEnd AND [AssemblyCollarClearanceDetermination] <> 0) as AssyCollar_Fail,

	
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_BodyFinalise] WHERE [BodyFinaliseStatUTC] BETWEEN @DateStart AND @DateEnd) as BodyFinalise_Count,
	(SELECT COUNT(*) FROM [dbo].[SA2_Stats_BodyFinalise] WHERE [BodyFinaliseStatUTC] BETWEEN @DateStart AND @DateEnd AND [BodyFinaliseDetermination] <> 0) as BodyFinalise_Fail
END
