﻿/*
SPC...Alias fields are used in SPC destination database to 
correctly locate the feature type.
*/
  CREATE PROCEDURE [dbo].[SA_UpdateSPCFeatureAlias]
	@Feature varchar(50),
	@SPCFeatureDescriptionAlias varchar(50)
  AS
    DECLARE @ret int = 0
	UPDATE [SelAssy_MasterMeasurementFeatureTypes]
	SET [SPCFeatureDescriptionAlias] = @SPCFeatureDescriptionAlias
	WHERE [Feature] = @Feature
	SET @ret = @@ROWCOUNT

	SELECT * 
	FROM [SelAssy_MasterMeasurementFeatureTypes] 
	WHERE [Feature] = @Feature

	RETURN @ret
