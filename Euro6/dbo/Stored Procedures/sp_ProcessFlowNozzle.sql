﻿

CREATE  PROCEDURE [dbo].[sp_ProcessFlowNozzle]
(
	@SerialNumber VARCHAR(MAX),		-- The serial number/matrix to be checked
	@RunSelect TINYINT = 0				-- run a select statement at the end of the procedure if set by the user at 0
)
AS 
	BEGIN
		SET NOCOUNT ON;
	
			DECLARE @SerialNumNozzle varchar(50)

			SET @SerialNumNozzle=@SerialNumber

			--Nozzle MCH OP180
			SELECT  PDate, SerialNum, PlantNumber,'180' AS Operation, FailCode
			FROM [Euro6].[dbo].[NozzleHegsProduction]
			WHERE SerialNum = @SerialNumNozzle
			--Nozzle MCH OP190
			UNION ALL
			SELECT  PDate, SerialNum, PlantNumber,'190' AS Operation, UploadCode
			FROM [Euro6].[dbo].[OldNozzleHegsProduction]
			WHERE SerialNum = @SerialNumNozzle
			--NNC Assy
			UNION ALL
			SELECT  PDate, NozzleSerial, AssetNumber,'400' AS Operation, FailCode
			FROM [Euro6].[dbo].[NNCAssembly]
			WHERE NozzleSerial = @SerialNumNozzle
			--NNC Test 
			UNION ALL
			SELECT  PDate, Serial_No_Long collate Latin1_General_CI_AS, PlantNumber,'410' AS Operation, OA_Result
			FROM [NNCNozzleTest].[dbo].[Result]
			WHERE Serial_No_Long = @SerialNumNozzle
		    --NNC Test 
			UNION ALL
			SELECT  PDate, Serial,'20474' AS PlantNumber,'415' AS Operation, FailCode
			FROM [Euro6].[dbo].[NozzleTestWithError_Results]
			WHERE Serial = @SerialNumNozzle
			--NNC Test
			UNION ALL
			SELECT  PDate, SERIAL_NUMBER collate Latin1_General_CI_AS, PlantNumber,'415' AS Operation, SUBSTRING(FAIL_CODE, CHARINDEX('(', FAIL_CODE) + 1, ABS(CHARINDEX(')', FAIL_CODE) - CHARINDEX('(', FAIL_CODE) - 1))
			FROM [NNCNozzleTest].[dbo].[20999_NozzleTest]
			WHERE SERIAL_NUMBER = @SerialNumNozzle

END
