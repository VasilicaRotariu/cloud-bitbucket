﻿

CREATE PROCEDURE [dbo].[SmartTargeting] 
	@dataString nvarchar(4000) OUTPUT
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRY

		DECLARE @elapsedTime int -- used for timing
		DECLARE @subQueryName varchar(max) -- used for timing

		DECLARE @ret int = 1 -- set to 0 inside each sub procedure if calculation was okay
		DECLARE @data AS dbo.TargetedRawData = @dataString

		-- Log activity to the log table
		INSERT INTO [SmartTargetingLog] ([Input])
		VALUES (@data.ToString())
		DECLARE @logUID bigint = CAST(SCOPE_IDENTITY() AS bigint) 

		BEGIN TRANSACTION

			SET @data.ClearOutputData()

			-- Get the ID from the data, then jump to the corresponding sub procedure.
			DECLARE @ProcessID nvarchar(255) = @data.GetProcessID()

			IF @ProcessID = 'Test'
			BEGIN
				EXEC @ret = [dbo].[SmartTargeting_Test] @data = @data OUTPUT
				SET @subQueryName = 'SmartTargeting_Test'
			END

			IF @ProcessID = 'ArmaturePress' OR  @ProcessID = 'ArmaturePressTest'
			BEGIN
				EXEC @ret = [dbo].[SmartTargeting_ArmaturePress] @data = @data OUTPUT
				SET @subQueryName = 'SmartTargeting_ArmaturePress'
			END
			
			IF @ProcessID = '20184.2.2' OR @ProcessID = 'AEA.2.2'
			BEGIN
				EXEC @ret = [dbo].[SmartTargeting_AEA_Line2_Station2] @data = @data OUTPUT
				SET @subQueryName = 'SmartTargeting_AEA_Line2_Station2'
			END

			IF @ProcessID = '00000' OR @ProcessID = 'SA2'
			BEGIN
				EXEC @ret = [dbo].[SmartTargeting_SA2] @data = @data OUTPUT
				SET @subQueryName = 'SmartTargeting_SA2'
			END
            
			IF @ProcessID = '00000' OR @ProcessID = 'SA3'
			BEGIN
				EXEC @ret = [dbo].[SmartTargeting_SA3] @data = @data OUTPUT
				SET @subQueryName = 'SmartTargeting_SA3'
			END

		COMMIT TRANSACTION
		
		-- Get the elapsed time of the sub procedure
		SELECT @elapsedTime = last_elapsed_time --, DB_NAME(database_id) [db], OBJECT_SCHEMA_NAME(object_id,database_id) [sc], OBJECT_NAME(object_id,database_id) [ob]
		FROM sys.dm_exec_procedure_stats
		WHERE DB_NAME(database_id) = 'Euro6' AND OBJECT_NAME(object_id,database_id) = @subQueryName
		
		-- Log activity to the log table
		UPDATE [SmartTargetingLog] 
		SET [Output] = @data.ToString(), [Elapsed] = (@elapsedTime / 1000000.0)
		WHERE [UID] = @logUID

		SET @dataString = @data.ToString()

		RETURN @ret
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(max)
		DECLARE @ErrorSeverity int
		DECLARE @ErrorState int
		SELECT @ErrorMessage = ERROR_MESSAGE() + ' / Line ' + cast(ERROR_LINE() as nvarchar(5)), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();
		ROLLBACK TRANSACTION
		
		-- Log error to the log table
		UPDATE [SmartTargetingLog] 
		SET [Err] = @ErrorMessage
		WHERE [UID] = @logUID

		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
		RETURN 1
	END CATCH
END




