﻿


-- =============================================
-- Author:	Vasilica Rotariu
-- Create date: 16/11/2020
-- Description:	Generic Previous Process Procedure
-- =============================================

CREATE PROCEDURE [dbo].[sp_PreviousProcessCheck_NNCPack]
(
	@SerialNumber VARCHAR(MAX),		-- The serial number/matrix to be checked
	@PlantNumber VARCHAR(MAX),		-- Plantnumber of the Machine Requesting Previous Result
	@Result VARCHAR(MAX) OUT,			-- Where 0 = PASS
	@ErrorDescription varchar(MAX) OUT,	-- If @Result anything other than 0 then this will explain why 
	@RunSelect TINYINT = 0				-- Run a select statement at the end of the procedure if set by the user at 0
)
AS 
 BEGIN
	SET NOCOUNT ON;

	SET @Result=1;

	---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    -- NNC Packaging- FW422
    -- Check NNC Assy, NNC Test and NNC Rework
    ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	   
	IF @PlantNumber IN (20422)
	   BEGIN   
			DECLARE @ResultNNCTest SMALLINT 
			DECLARE @PDateNNCTest DateTime
			DECLARE @ResultNNCAssy SMALLINT 
			DECLARE @PDateNNCAssy DateTime
			DECLARE @ResultNNCRwk SMALLINT 
			DECLARE @PDateNNCRwk DateTime

			SELECT TOP 1 @ResultNNCAssy=[FailCode] FROM [Euro6].[dbo].[NNCAssembly]
			WHERE [NozzleSerial] = @SerialNumber
			ORDER BY [PDate] DESC;

			IF @ResultNNCAssy = NULL OR @@ROWCOUNT <= 0
				BEGIN
					SELECT @ResultNNCRwk=FailCode, @PDateNNCRwk=PDate FROM [Euro6].[dbo].[20603_MatchGrind_Results]
					WHERE Nozzle_Matrix = @SerialNumber  
					ORDER BY [PDate] DESC;

					IF @ResultNNCRwk = NULL OR @@ROWCOUNT <= 0
						BEGIN	
							SET @Result = 1
							SET @ErrorDescription = 'Lipsa trasabilitate din asamblare sau rework NNC pentru seria: '  + @SerialNumber
							GOTO TheEnd;
						END 
				END
				ELSE
				BEGIN
					IF @ResultNNCAssy != 0
						BEGIN
							SELECT @ResultNNCRwk=FailCode FROM [Euro6].[dbo].[20603_MatchGrind_Results]
									WHERE Nozzle_Matrix = @SerialNumber and PDate  > (
										SELECT TOP 1 [TDate]
    									FROM [Euro6].[dbo].[NNCAssembly] where NozzleSerial = @SerialNumber ORDER BY TDate DESC								
										)
							IF @ResultNNCRwk = NULL OR @@ROWCOUNT <= 0
							BEGIN
								SET @Result = 1
								SET @ErrorDescription = 'Piesa NOK asamblare NNC netrecuta prin rework: '  + @SerialNumber
								GOTO TheEnd;
							END
							ELSE
							BEGIN
							  GOTO NNCTestCheck;
							END
                        END
					 ELSE
						 BEGIN
							GOTO NNCTestCheck;
						 END
				END
	    ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        -- NNCTestCheck
        -- Check NNC Test loop including the rework- TBD
        ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	   
	    NNCTestCheck:
		BEGIN
  
			SELECT TOP 1 @PDateNNCTest = [PDate], @ResultNNCTest=[OA_Result]
			FROM(					
			SELECT  [PDate], [OA_Result]
			FROM [NNCNozzleTest].[dbo].[Result] --OP410.1 & OP410.2
			WHERE [Serial_No_Long] = @SerialNumber
			UNION ALL
			SELECT  [PDate], SUBSTRING(FAIL_CODE, CHARINDEX('(', FAIL_CODE) + 1, ABS(CHARINDEX(')', FAIL_CODE) - CHARINDEX('(', FAIL_CODE) - 1))
			FROM [NNCNozzleTest].[dbo].[20999_NozzleTest] --OP415.2
			WHERE [SERIAL_NUMBER] = @SerialNumber
			UNION ALL
			SELECT   [PDate], [FailCode]
			FROM [Euro6].[dbo].[NozzleTestWithError_Results] --OP415.3
			WHERE [Serial] = @SerialNumber			
			) as a
			ORDER BY [PDate] DESC;	

			SELECT TOP 1 @PDateNNCRWK = [PDate], @ResultNNCRwk = [FailCode]
			FROM(
			SELECT FailCode, PDate FROM [Euro6].[dbo].[20603_MatchGrind_Results]
			WHERE Nozzle_Matrix = @SerialNumber 
			) as b
			ORDER BY [PDate] DESC;
			
			--PRINT @PDateNNCRWK
					
			IF @PDateNNCRWK > @PDateNNCTest
				BEGIN
					SET @Result = 1
				    SET @ErrorDescription = 'Bucla rework testare NNC incorecta: '  + @SerialNumber			
					GOTO TheEnd;			
				END
			
			--PRINT @PDateNNCTest

			IF @ResultNNCTest = 0 
				BEGIN
				   SET @Result = 0
			       SET @ErrorDescription = 'PASSED'
				END

			IF @ResultNNCTest = NULL OR @@ROWCOUNT <= 0
				BEGIN
					SET @Result = 1
					SET @ErrorDescription = 'Lipsa trasabilitate din testare NNC: '  + @SerialNumber
					GOTO TheEnd;
				END

			IF @ResultNNCTest!= 0
				BEGIN	
					SET @Result = 1
					SET @ErrorDescription = 'Piesa NOK la NNC test cu codul de defect:' + @ResultNNCTest
					GOTO TheEnd;
				END 
				
	  END	  
	  
		----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		-- TheEnd
		-- Record SP usage and return
		---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		TheEnd:
        -- Record sp results into [spResults]
        INSERT INTO [dbo].[_spResults]([PDate], [PlantNumber], [SerialNumber], [spName], [Result], [FailReason])
        VALUES
        (
        SYSDATETIME(),                              -- PDate - datetime2(7)
        @PlantNumber,                               -- PlantNumber - smallint
        @SerialNumber,                                  -- SerialNumber - varchar(25)
        'sp_PreviousProcessCheck_NNCPack', @Result,    -- Result - tinyint
        @ErrorDescription                                    -- FailReason - varchar(255)
        );

		-- Record [_ObjectUsage]
		--DECLARE @startTime DATETIME = GETDATE();  -- Set start time
		--BEGIN TRY       
		--	DECLARE @Host		VARCHAR(50)		=	LEFT(ISNULL(HOST_NAME(), '<NULL>'), 50);
		--	DECLARE @User		VARCHAR(50)		=	LEFT(ISNULL(SUSER_NAME(), '<NULL>'), 50);
		--	DECLARE @Object		VARCHAR(100)	=	LEFT('[dbo].[sp_PreviousProcessCheck_NNCPack]', 100);
		--	DECLARE @In			VARCHAR(MAX)	=	'@SerialNumber=' + CAST(@SerialNumber AS VARCHAR(MAX)) + ', ' + '@PlantNumber=' + CAST(@PlantNumber AS VARCHAR(MAX));
		--	DECLARE @Out		VARCHAR(MAX)	=	'@Result=' + CAST(@Result AS VARCHAR(MAX)) + ', ' + '@ErrorDescription=' + CAST(@ErrorDescription AS VARCHAR(MAX));
		--	DECLARE @duration	TIME			=	CAST((GETDATE() - @startTime) AS TIME);
		--	INSERT INTO [dbo].[_ObjectUsage] ([HostName], [UserName], [Object], [In], [Out], [Duration]) VALUES (@Host, @User, @Object, @In, @Out, @duration);
		--END TRY
		--BEGIN CATCH
		--	-- Do Nothing      
		--END CATCH

   END;
   SET NOCOUNT OFF;
 END;


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[sp_PreviousProcessCheck_NNCPack] TO [dbcache]
    AS [dbo];

