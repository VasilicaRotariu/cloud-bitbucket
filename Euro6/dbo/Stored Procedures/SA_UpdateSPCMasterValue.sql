﻿/*
Updates the Selective Assembly data definition which converts
the master data into SPC data (on SudburySPC).
*/
CREATE PROCEDURE [dbo].[SA_UpdateSPCMasterValue]
    @PlantNumber smallint,      -- 20707 (added if it does not exist)
	@ComponentType varchar(20), -- 'PIN' (added if it does not exist)
	@MasterType varchar(10),    -- 'MAX', 'MIN', etc (MUST ALREADY EXIST!)
	@Feature varchar(50),       -- 'MasterValueP.CollarOffset' (MUST ALREADY EXIST!)
	@CalibratedValue float		-- 0.001 (added if it does not exist)
AS
BEGIN
	DECLARE @MasterComponentType_UID int = 0
	DECLARE @Master_UID int = 0
	DECLARE @MasterLimitType_UID int = 0
	DECLARE @SelAssy_MasterMeasurementFeatureType_UID int = 0
	DECLARE @MasterMeasurementFeature_UID int = 0


	BEGIN TRY

		BEGIN TRANSACTION

			IF EXISTS (SELECT * FROM [SelAssy_MasterComponentTypes] WHERE [PlantNumber] = @PlantNumber AND [ComponentType] = @ComponentType)
				BEGIN
					SELECT @MasterComponentType_UID = [MasterComponentType_UID] FROM [SelAssy_MasterComponentTypes] WHERE [PlantNumber] = @PlantNumber AND [ComponentType] = @ComponentType
				END
			ELSE
				BEGIN 
					INSERT INTO [SelAssy_MasterComponentTypes] ([PlantNumber], [ComponentType]) VALUES (@PlantNumber, @ComponentType)
					SET @MasterComponentType_UID = CAST(SCOPE_IDENTITY() AS int) 
				END

			PRINT @MasterComponentType_UID

			SELECT TOP 1 @MasterLimitType_UID = [MasterLimitType_UID] FROM [SelAssy_MasterTypes] WHERE [MasterType] = @MasterType

			IF (@MasterLimitType_UID = 0)
			BEGIN
				raiserror('@MasterType was not found', 15, 1)
			END

			PRINT @MasterLimitType_UID

			SELECT TOP 1 @SelAssy_MasterMeasurementFeatureType_UID = [SelAssy_MasterMeasurementFeatureType_UID] FROM [SelAssy_MasterMeasurementFeatureTypes] WHERE [Feature] = @Feature

			IF (@SelAssy_MasterMeasurementFeatureType_UID = 0)
			BEGIN
				raiserror('@Feature was not found', 15, 1)
			END

			PRINT @MasterLimitType_UID

			IF EXISTS (SELECT * FROM [SelAssy_Masters] WHERE [MasterComponentType_FID] = @MasterComponentType_UID AND [MasterLimitType_FID] = @MasterLimitType_UID)
				BEGIN
					SELECT @Master_UID = [Master_UID] FROM [SelAssy_Masters] WHERE [MasterComponentType_FID] = @MasterComponentType_UID AND [MasterLimitType_FID] = @MasterLimitType_UID
				END
			ELSE
				BEGIN
					INSERT INTO [SelAssy_Masters] ([MasterComponentType_FID], [MasterLimitType_FID]) VALUES (@MasterComponentType_UID, @MasterLimitType_UID)
					SET @Master_UID = CAST(SCOPE_IDENTITY() AS int) 
				END

			PRINT @Master_UID
			
			IF EXISTS (SELECT * FROM [SelAssy_MasterMeasurementFeatures] WHERE [Master_FID] = @Master_UID AND [SelAssy_MasterMeasurementFeatureType_FID] = @SelAssy_MasterMeasurementFeatureType_UID)
				BEGIN
					UPDATE [SelAssy_MasterMeasurementFeatures] SET [CalibratedValue] = @CalibratedValue WHERE [Master_FID] = @Master_UID AND [SelAssy_MasterMeasurementFeatureType_FID] = @SelAssy_MasterMeasurementFeatureType_UID
				END 
			ELSE
				BEGIN
					INSERT INTO [SelAssy_MasterMeasurementFeatures] ([Master_FID], [SelAssy_MasterMeasurementFeatureType_FID], [CalibratedValue]) VALUES (@Master_UID, @SelAssy_MasterMeasurementFeatureType_UID, @CalibratedValue)
				END
			
		COMMIT TRANSACTION

		SELECT *
		FROM [dbo].[SA_GetSPCFeatures]
		WHERE [PlantNumber] = @PlantNumber AND [ComponentType] = @ComponentType AND [MasterType] = @MasterType AND [Feature] = @Feature

	END TRY
	BEGIN CATCH

	    DECLARE @ErrorMessage nvarchar(max)
		DECLARE @ErrorSeverity int
		DECLARE @ErrorState int
		SELECT @ErrorMessage = ERROR_MESSAGE() + ' / Line ' + cast(ERROR_LINE() as nvarchar(5)),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE();
		ROLLBACK TRANSACTION
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

	END CATCH
END
