﻿-- =============================================
-- Author:		<Alex Anderson>
-- Create date: <02/03/2015>
-- Description:	< --- >
-- =============================================
CREATE PROCEDURE [dbo].[sp_MASMEC_GetAllResults] 
	-- Add the parameters for the stored procedure here
	@Start datetime,
	@End datetime 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 1 as StationCode, UID, PDate, Serial, LOP, Result, dbo.CompressLine3InjectorType(Injector_Type) as InjectorType, Fail_UID as FailUID
	FROM dbo.MASMEC_SelAsmb_OP10_Results
	WHERE Pdate BETWEEN @Start AND @End
	UNION
	SELECT 2 as StationCode, UID, PDate, Serial, LOP, Result, dbo.CompressLine3InjectorType(Injector_Type) as InjectorType, Fail_UID as FailUID
	FROM dbo.MASMEC_SelAsmb_OP20A_Results
	WHERE Pdate BETWEEN @Start AND @End
	UNION
	SELECT 3 as StationCode, UID, PDate, Serial, LOP, Result, dbo.CompressLine3InjectorType(Injector_Type) as InjectorType, Fail_UID as FailUID
	FROM dbo.MASMEC_SelAsmb_OP20B_Results
	WHERE Pdate BETWEEN @Start AND @End
	UNION
	--SELECT 4 as StationCode, UID, PDate, Serial, LOP, Result, Injector_Type as InjectorType, Fail_UID as FailUID
	--FROM dbo.MASMEC_SelAsmb_OP20C_Results
	--WHERE Pdate BETWEEN @Start AND @End
	--UNION
	SELECT 5 as StationCode, UID, PDate, Serial, LOP, Result, dbo.CompressLine3InjectorType(Injector_Type) as InjectorType, Fail_UID as FailUID
	FROM dbo.MASMEC_SelAsmb_OP30A_Results
	WHERE Pdate BETWEEN @Start AND @End
	UNION
	SELECT 6 as StationCode, UID, PDate, Serial, LOP, Result, dbo.CompressLine3InjectorType(Injector_Type) as InjectorType, Fail_UID as FailUID
	FROM dbo.MASMEC_SelAsmb_OP30B_Results
	WHERE Pdate BETWEEN @Start AND @End
	UNION
	SELECT 7 as StationCode, UID, PDate, Serial, LOP, Result, dbo.CompressLine3InjectorType(Injector_Type) as InjectorType, Fail_UID as FailUID
	FROM dbo.MASMEC_SelAsmb_OP30C_Results
	WHERE Pdate BETWEEN @Start AND @End

END

GO
GRANT EXECUTE
    ON OBJECT::[dbo].[sp_MASMEC_GetAllResults] TO [dbcache]
    AS [dbo];

