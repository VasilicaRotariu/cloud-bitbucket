﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAEALinesProduction]
	@Start datetime, @End datetime
AS
BEGIN
	 
	SET NOCOUNT ON;

	SELECT COUNT(*) as Total
	FROM NLS
	WHERE LineID IN (1, 2)
		AND GLOBAL_RESULT = 0
	AND PDate BETWEEN @Start AND @End
END
