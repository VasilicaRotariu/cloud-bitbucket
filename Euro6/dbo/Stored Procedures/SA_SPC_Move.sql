﻿/*
Moves data from [Euro6].[dbo].[SelAssy_MasterSPC] and places it into
[SudburySPC].[dbo].[tblProcesslogSPCDatas].
Data older than 2 weeks that has not been moved is deleted.
*/
CREATE PROCEDURE [dbo].[SA_SPC_Move]
AS
	BEGIN TRY
		BEGIN TRANSACTION
			
			-- Create a table variable to hold the data. This is so the
			-- data can be deleted from the source table after transfer.
			DECLARE @SourceTable TABLE 
			(
				[SourceUID] [int] NOT NULL,
				[LineID] [int] NOT NULL,
				[PDate] [datetime] NOT NULL,
				[FeatureID] [int] NOT NULL,
				[UsernameID] [int] NOT NULL,
				[PartNumberID] [int] NOT NULL,
				[MachineID] [int] NOT NULL,
				[ChuckFixture] [int] NOT NULL,
				[ActionCodeID] [int] NULL,
				[Value1] [float] NOT NULL,
				[Value2] [float] NULL,
				[Value3] [float] NULL,
				[Value4] [float] NULL,
				[Value5] [float] NULL,
				[Valid] [int] NOT NULL,
				[MachineGroupID] [int] NOT NULL,
				[Value6] [float] NULL
			)
	
			-- Delete all records where they are more than two weeks old (which have not been moved)
			DELETE [Euro6].[dbo].[SelAssy_MasterSPC]
			WHERE UTC < DATEADD(WEEK, -2, SYSUTCDATETIME())

			-- Fill the table variable
			INSERT INTO @SourceTable
			SELECT 
				SPC_Source.UID [SourceUID]
				,M.LineID [LineID]
				,SPC_Source.UTC [PDate]
				,F.[ID] [FeatureID]
				,209 [UsernameID]
				,P.ID [PartNumberID]
				,M.ID [MachineNumberID]
				,1 [ChuckFixture]
				,NULL [ActionCode]
				,SPC_Source.Value1 [Value1]
				,SPC_Source.Value2 [Value2]
				,SPC_Source.Value3 [Value3]
				,SPC_Source.Value4 [Value4]
				,SPC_Source.Value5 [Value5]
				,1 [Valid]
				,0 [MachineGroupID]
				,NULL [Value6]
			FROM [Euro6].[dbo].[SelAssy_MasterSPC] SPC_Source
			JOIN [SudburySPC].[dbo].[tblProcessLogMachines] M ON M.[PlantNumber] = SPC_Source.PlantNumber
			JOIN [SudburySPC].[dbo].[tblProcessLogSPCFeature] F ON F.[Description] = SPC_Source.[SPCFeatureDescriptionAlias] AND F.[MachineID] = M.[ID]
			JOIN [SudburySPC].[dbo].[tblProcessLogParts] P ON P.[PartNumber] = SPC_Source.[SPCPartNumberAlias] AND P.[MachineID] = M.[ID]
			ORDER BY SPC_Source.UTC

			-- Insert the data into the correct destination table
			INSERT INTO [SudburySPC].[dbo].[tblProcesslogSPCDatas] ([LineID], [PDate], [FeatureID], [UsernameID], [PartNumberID], [MachineID], [ChuckFixture], [ActionCodeID], [Value1], [Value2], [Value3], [Value4], [Value5], [Valid], [MachineGroupID], [Value6])
			SELECT [LineID], [PDate], [FeatureID], [UsernameID], [PartNumberID], [MachineID], [ChuckFixture], [ActionCodeID], [Value1], [Value2], [Value3], [Value4], [Value5], [Valid], [MachineGroupID], [Value6]
			FROM @SourceTable

			-- Delete those records that have been moved.
			DELETE [Euro6].[dbo].[SelAssy_MasterSPC]
			WHERE UID IN 
			(SELECT [SourceUID] FROM @SourceTable)

		COMMIT TRANSACTION
		RETURN 1
	END TRY
	BEGIN CATCH
	    DECLARE @ErrorMessage nvarchar(max)
		DECLARE @ErrorSeverity int
		DECLARE @ErrorState int
		SELECT @ErrorMessage = ERROR_MESSAGE() + ' / Line ' + cast(ERROR_LINE() as nvarchar(5)),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE();
		ROLLBACK TRANSACTION
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	END CATCH


