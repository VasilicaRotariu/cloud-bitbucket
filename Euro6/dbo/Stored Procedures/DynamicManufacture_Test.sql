﻿
CREATE PROCEDURE [dbo].[DynamicManufacture_Test]
	@Part1ID varchar(20),
	@Part2ID varchar(20),
	@Part3ID varchar(20),
	@InputData1 sql_variant,
	@InputData2 sql_variant,
	@InputData3 sql_variant,
	@OutputData1 sql_variant OUTPUT,
	@OutputData2 sql_variant OUTPUT,
	@OutputData3 sql_variant OUTPUT,
	@OutputData4 sql_variant OUTPUT,
	@OutputData5 sql_variant OUTPUT,
	@OutputData6 sql_variant OUTPUT,
	@OutputData7 sql_variant OUTPUT,
	@OutputData8 sql_variant OUTPUT,
	@OutputData9 sql_variant OUTPUT,
	@ProcessData [DynamicManufactureData] READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Binary varbinary(max)
	DECLARE @Ave float

	DECLARE @PartData varchar(4000) = @Part1ID
	IF @Part2ID <> ''
	BEGIN 
		SET @PartData = @PartData + '|' + @Part2ID
	END
	IF @Part3ID <> ''
	BEGIN 
		SET @PartData = @PartData + '|' + @Part3ID
	END

	SELECT @Ave = AVG([Data])
	FROM @ProcessData

	SET @OutputData1 = SYSUTCDATETIME() 
	SET @OutputData2 = @PartData
	SET @OutputData3 = SYSTEM_USER + '|' + CURRENT_USER + '|' + HOST_ID() + '|' + HOST_ID()
	SET @OutputData4 = @Ave
	SELECT @OutputData5 = COUNT(*) FROM information_schema.tables WHERE table_type = 'base table' 
	SET @OutputData6 = convert(varbinary(4000), 'Hello' ,0) 
	SET @OutputData7 = @InputData1
	SET @OutputData8 = @InputData2
	SET @OutputData9 = @InputData3



	RETURN 0 -- 0 is pass, anything else is fail.

END
