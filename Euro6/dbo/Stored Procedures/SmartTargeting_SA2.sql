﻿
-- Returns some test data to the client.
CREATE PROCEDURE [dbo].[SmartTargeting_SA2]
	@data dbo.TargetedRawData OUTPUT
AS
BEGIN

	DECLARE @partID nvarchar(20)
	DECLARE @ret int = 0
	DECLARE @result float
	SET NOCOUNT ON;

	-- Get the input data value
	SET @partID = @data.GetPartID(0)

	-- Do calculation
	SET @result = 0

	-- Set the output data value (this is optional if the result has not been calculated)
	SET @data.SetOutputValue_SqlDouble(0, @result)
	
	RETURN @ret
END




