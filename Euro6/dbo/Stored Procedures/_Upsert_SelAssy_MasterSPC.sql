﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[_Upsert_SelAssy_MasterSPC]
	@Source [dbo].[SelAssy_MasterSPC_Upsert] READONLY
AS
BEGIN

	BEGIN TRANSACTION

	MERGE INTO [dbo].[SelAssy_MasterSPC] target
		USING @Source source
	ON 0 = 1
	WHEN NOT MATCHED THEN
		INSERT VALUES (source.[UTC], source.[PlantNumber], source.[ComponentType], source.[SPCPartNumberAlias], source.[SPCPartDescriptionAlias], source.[Feature], source.[SPCFeatureDescriptionAlias], source.[Value1], source.[Value2], source.[Value3], source.[Value4], source.[Value5]);

	COMMIT TRANSACTION

END
