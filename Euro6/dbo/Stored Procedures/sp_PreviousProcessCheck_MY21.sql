﻿

-- =============================================
-- Author:	Vasilica Rotariu
-- Create date: 13/11/2020
-- Description:	Generic Previous Process Procedure
-- =============================================


CREATE PROCEDURE [dbo].[sp_PreviousProcessCheck_MY21]
(
    @PlantNumber SMALLINT,
    @NozzleNo    VARCHAR(15),
    @ProductID   SMALLINT,
    @Result      TINYINT      OUTPUT,
    @Message     VARCHAR(512) OUTPUT
)
AS
    BEGIN
        SET @Message = '';
        SET @Result = 1;
        DECLARE @RequiredNozzle VARCHAR(3),
                @KPVID          SMALLINT,
                @VIn            DECIMAL(18, 6),
                @VInMin         DECIMAL(18, 9),
                @VInMax         DECIMAL(18, 9),
                @PGClearance    DECIMAL(18, 9),
                @PGClearanceMin DECIMAL(18, 9),
                @PGClearanceMax DECIMAL(18, 9),
                @NeedleLift     DECIMAL(18, 9),
                @NeedleLiftMin  DECIMAL(18, 9),
                @NeedleLiftMax  DECIMAL(18, 9),
                @NeedleTaper    DECIMAL(18, 9),
                @NeedleTaperMin DECIMAL(18, 9),
                @NeedleTaperMax DECIMAL(18, 9),
                @PGTaper        DECIMAL(18, 9),
                @PGTaperMin     DECIMAL(18, 9),
                @PGTaperMax     DECIMAL(18, 9),
                @CountWL        SMALLINT,
                @CountTest      SMALLINT;
     
        ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        GetKPVLimits:
        BEGIN

            -- Get KPV limits
            SELECT      TOP(1)
                        @KPVID          = [ID],
                        @VInMin         = [VInMin],
                        @VInMax         = [VInMax],
                        @PGClearanceMin = [PistonBoreClearanceMin],
                        @PGClearanceMax = [PistonBoreClearanceMax],
                        @NeedleLiftMin  = [NeedleLiftMin],
                        @NeedleLiftMax  = [NeedleLiftMax],
                        @NeedleTaperMin = [NeedleTaperMin],
                        @NeedleTaperMax = [NeedleTaperMax],
                        @PGTaperMin     = [PGBoreTaperMin],
                        @PGTaperMax     = [PGBoreTaperMax]
            FROM        [Euro6].[dbo].[MY21KPVLimits]
            ORDER BY    [IDate] DESC;

            -- Check for rows
            IF @@ROWCOUNT <= 0
                BEGIN
                    SET @Result = 1;
                    SET @Message = 'No KPV Limits found in [Euro6].[dbo].[MY21KPVLimits]';
                    GOTO TheEnd;
                END;

            -- Check limits are valid
            IF  (@VInMin IS NULL)
            OR  (@VInMax IS NULL)
            OR  (@PGClearanceMin IS NULL)
            OR  (@PGClearanceMax IS NULL)
            OR  (@NeedleLiftMin IS NULL)
            OR  (@NeedleLiftMax IS NULL)
            OR  (@NeedleTaperMin IS NULL)
            OR  (@NeedleTaperMax IS NULL)
            OR  (@PGTaperMin IS NULL)
            OR  (@PGTaperMax IS NULL)
                BEGIN
                    SET @Result = 1;
                    SET @Message = 'Invalid KPV Limits for ID ' + ISNULL(CAST(@KPVID AS VARCHAR(10)), '<NULL>') + ' in [Euro6].[dbo].[MY21KPVLimits]';
                    GOTO TheEnd;
                END;
        END;

        ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        -- Nozzle Data Iasi
        -- Check various previous processes to ensure that components are OK
        ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        Nozzle:
        BEGIN
            SET @Result = 1;

            --DECLARE @NozzleManufactureDate DATETIME2(7);
            SELECT      TOP(1)
                        @VIn    = [FLOWTEST_FLOW],
                        @Result = CASE
                                  WHEN LEFT([FAIL_CODE], 7) = '(0)  IO' THEN 0
                                  ELSE 1
                                  END
            FROM        [NNCNozzleTest].[dbo].[20999_NozzleTest]
            WHERE       [SERIAL_NUMBER] = @NozzleNo
            ORDER BY    [PDATE] DESC;

            -- Rows
            IF @@ROWCOUNT <= 0
                BEGIN
                    SET @Result = 1;
                    SET @Message = 'No record in [NNCNozzleTest].[dbo].[20999_NozzleTest]';
                    GOTO TheEnd;
                END;

            -- Result
            IF @Result <> 0
                BEGIN
                    SET @Result = 1;
                    SET @Message = 'Part FAILED in [NNCNozzleTest].[dbo].[20999_NozzleTest]';
                    GOTO TheEnd;
                END;

            --IF @NozzleManufactureDate < '11-SEP-2020 15:00'
            --BEGIN
            -- KPV 
			IF @VIn IS NULL 
			BEGIN 
			 SELECT @Result = 1,
                     @Message = 'Nozzle VIn invalid';
                    GOTO TheEnd;
			    
			END
            IF @VIn NOT BETWEEN @VInMin
                            AND @VInMax
                BEGIN
                    SET @Result = 1;
                    SET @Message = 'Nozzle VIn out of spec (KPV) for ID ' + ISNULL(CAST(@KPVID AS VARCHAR(10)), '<NULL>') + ' in [Euro6].[dbo].[MY21KPVLimits]';
                    GOTO TheEnd;
                END;
            --END
            SET @Result = 1;
            --    SELECT      TOP(1)                      
            --                @PGClearance = [Capsule_Clearance]/1000,
            --				@NeedleLift = [Capsule_NeedleLift]/1000,
            --					   @NeedleTaper = [Needle_Taper]/1000,
            --@PGTaper = [PistonGuide_Taper]/1000,
            --				@Result = CASE WHEN  [Failcode] = 0  THEN 0 ELSE 1 END
            --    FROM        [SDI-VSQL04].[Nozzle].[dbo].[NNCAssembly]
            --    WHERE       [NozzleSerial] = @NozzleNo
            --	ORDER BY    [PDATE] DESC
            SELECT      TOP(1)
                        @PGClearance = [A].[Capsule_Clearance] /1000,
                        @NeedleLift  = [A].[Capsule_NeedleLift] / 1000,
                        @NeedleTaper = [A].[Needle_Taper] / 1000,
                        @PGTaper     = [A].[PistonGuide_Taper] / 1000,
                        @Result      = CASE
                                       WHEN [A].[Failcode] = 0 THEN 0
                                       ELSE 1
                                       END
            FROM
                        (
                            SELECT  [PDate],
                                    [Capsule_Clearance],
                                    [Capsule_NeedleLift],
                                    [Needle_Taper],
                                    [PistonGuide_Taper],
                                    [Failcode]
                            FROM    [Euro6].[dbo].[NNCAssembly]
                            WHERE   [NozzleSerial] = @NozzleNo
                            UNION ALL
                            SELECT  [PDate],
                                    [Clearance],
                                    [Lift] * 1000,
                                    [Taper],
                                    [PG_Taper],
                                    [FailCode]
                            FROM    [Euro6].[dbo].[20603_MatchGrind_Results]
                            WHERE   [Nozzle_Matrix] = @NozzleNo
                        ) AS [A]
            ORDER BY    [A].[PDate] DESC;

            -- Rows
            IF @@ROWCOUNT <= 0
                BEGIN
                    SET @Result = 1;
                    SET @Message = 'No record in [Euro6].[dbo].[NNCAssembly] or [dbo].[NNCRework]';
                    GOTO TheEnd;
                END;

            -- Result
            IF @Result <> 0
                BEGIN
                    SET @Result = 1;
                    SET @Message = 'Part FAILED in [Euro6].[dbo].[NNCAssembly] or [dbo].[NNCRework]';
                    GOTO TheEnd;
                END;
			IF @PGClearance IS NULL 
			BEGIN 
			 SELECT @Result = 1,
                     @Message = 'Nozzle PG Clearance invalid';
                    GOTO TheEnd;
			    
			END
            IF @PGClearance NOT BETWEEN @PGClearanceMin
                                    AND @PGClearanceMax
                BEGIN
                    SET @Result = 1;
                    SET @Message = 'Nozzle PG Clearance out of spec (KPV) for ID ' + ISNULL(CAST(@KPVID AS VARCHAR(10)), '<NULL>') + ' in [Euro6].[dbo].[MY21KPVLimits]';
                    GOTO TheEnd;
                END;
			IF @NeedleLift IS NULL 
			BEGIN 
			 SELECT @Result = 1,
                     @Message = 'Nozzle Needle Lift invalid';
                    GOTO TheEnd;
			    
			END
            IF @NeedleLift NOT BETWEEN @NeedleLiftMin
                                   AND @NeedleLiftMax
                BEGIN
					PRINT  @NeedleLift;
                    SET @Result = 1;
                    SET @Message = 'Nozzle Needle Lift out of spec (KPV) for ID ' + ISNULL(CAST(@KPVID AS VARCHAR(10)), '<NULL>') + ' in [Euro6].[dbo].[MY21KPVLimits]]';
                    GOTO TheEnd;
                END;
				
			IF @NeedleTaper IS NULL 
			BEGIN 
			 SELECT @Result = 1,
                     @Message = 'Nozzle Needle Taper invalid';
                    GOTO TheEnd;
			    
			END
            IF @NeedleTaper NOT BETWEEN @NeedleTaperMin
                                    AND @NeedleTaperMax
                BEGIN
                    SET @Result = 1;
                    SET @Message = 'Nozzle Needle Taper out of spec (KPV) for ID ' + ISNULL(CAST(@KPVID AS VARCHAR(10)), '<NULL>') + ' in [Euro6].[dbo].[MY21KPVLimits]';
                    GOTO TheEnd;
                END;

			IF @PGTaper IS NULL 
			BEGIN 
			 SELECT @Result = 1,
                     @Message = 'PG Bore Taper invalid';
                    GOTO TheEnd;
			    
			END
            IF @PGTaper NOT BETWEEN @PGTaperMin
                                AND @PGTaperMax
                BEGIN
                    SET @Result = 1;
                    SET @Message = 'PG Bore Taper out of spec (KPV) for ID ' + ISNULL(CAST(@KPVID AS VARCHAR(10)), '<NULL>') + ' in [Euro6].[dbo].[MY21KPVLimits]';
                    GOTO TheEnd;
                END;
            GOTO TheEnd;
        END;

        ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        -- TheEnd
        -- Record SP usage and return
        ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
       
	    TheEnd:
        -- Record sp results into [spResults]
        INSERT INTO [dbo].[_spResults]([PDate], [PlantNumber], [SerialNumber], [spName], [Result], [FailReason])
        VALUES
        (
        SYSDATETIME(),                              -- PDate - datetime2(7)
        @PlantNumber,                               -- PlantNumber - smallint
        @NozzleNo,                                  -- SerialNumber - varchar(25)
        'sp_PreviousProcessCheck_MY21', @Result,    -- Result - tinyint
        @Message                                    -- FailReason - varchar(255)
        );

        -- Record [_ObjectUsage]
	 --   DECLARE @startTime DATETIME = GETDATE();  -- Set start time
		--BEGIN TRY       
		--	DECLARE @Host		VARCHAR(50)		=	LEFT(ISNULL(HOST_NAME(), '<NULL>'), 50);
		--	DECLARE @User		VARCHAR(50)		=	LEFT(ISNULL(SUSER_NAME(), '<NULL>'), 50);
		--	DECLARE @Object		VARCHAR(100)	=	LEFT('[dbo].[sp_PreviousProcessCheck_MY21]', 100);
		--	DECLARE @In			VARCHAR(MAX)	=	'@SerialNumber=' + CAST(@NozzleNo AS VARCHAR(MAX)) + ', ' + '@PlantNumber=' + CAST(@PlantNumber AS VARCHAR(MAX));
		--	DECLARE @Out		VARCHAR(MAX)	=	'@Result=' + CAST(@Result AS VARCHAR(MAX)) + ', ' + '@ErrorDescription=' + CAST(@Message AS VARCHAR(MAX));
		--	DECLARE @duration	TIME			=	CAST((GETDATE() - @startTime) AS TIME);
		--	INSERT INTO [dbo].[_ObjectUsage] ([HostName], [UserName], [Object], [In], [Out], [Duration]) VALUES (@Host, @User, @Object, @In, @Out, @duration);
		--END TRY
		--BEGIN CATCH
		--	-- Do Nothing      
		--END CATCH
  END;

GO
GRANT EXECUTE
    ON OBJECT::[dbo].[sp_PreviousProcessCheck_MY21] TO [dbcache]
    AS [dbo];

