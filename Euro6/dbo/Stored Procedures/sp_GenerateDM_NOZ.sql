﻿

-- =============================================
-- Author:		<Rotariu,Vasilica>
-- Create date: <10/10/2019>
-- Description:	<Stored procedure used to retrieve the code to engrave the nozzle part.>
-- =============================================

CREATE PROCEDURE [dbo].[sp_GenerateDM_NOZ] 
(
                @Ligne VARCHAR(MAX)=NULL,                             
                @Ref VARCHAR(MAX)=NULL, 
                @Site VARCHAR(MAX)=NULL,
				@Val_OUT VARCHAR(MAX) OUT,
				@Status INT OUT
              )
AS 
    BEGIN
					-- SET NOCOUNT ON added to prevent extra result sets from
					-- interfering with SELECT statements.
					SET NOCOUNT ON;

					DECLARE @Seq_  VARCHAR(6)  
					DECLARE @Month_ VARCHAR(1)
					DECLARE @Year_ VARCHAR(2)
					DECLARE @Val_OUT_ VARCHAR(13)
					DECLARE @Status_ INT
					DECLARE @Val_Counter_ INT
   
    SET @Status_ =-10;

    IF @Ligne IS NOT NULL
         IF @Ref IS NOT NULL
            IF @Site IS NOT NULL
				BEGIN      

						SET @Seq_ = RIGHT(CONCAT('000000',NEXT VALUE FOR [DBO].[SEQ_DM_PG]),6); --incremental number

                        IF RIGHT(MONTH(getdate()), 2) = '01' --month in format MM
                           SET @Month_ = 'A';
                        ELSE IF RIGHT(MONTH(getdate()), 2) = '02' 
                           SET  @Month_ = 'B';
                        ELSE IF RIGHT(MONTH(getdate()), 2) = '03' 
                           SET @Month_ = 'C';
                        ELSE IF RIGHT(MONTH(getdate()), 2) = '04' 
                           SET @Month_= 'D';
                        ELSE IF RIGHT(MONTH(getdate()), 2) = '05' 
                            SET @Month_ = 'E';
                        ELSE IF RIGHT(MONTH(getdate()), 2) = '06' 
                            SET @Month_ = 'F';
                        ELSE IF RIGHT(MONTH(getdate()), 2) = '07' 
                           SET @Month_= 'G';
                        ELSE IF RIGHT(MONTH(getdate()), 2) = '08' 
                            SET @Month_ = 'H';
                        ELSE IF RIGHT(MONTH(getdate()), 2) = '09' 
                            SET @Month_ = 'J';
                        ELSE IF RIGHT(MONTH(getdate()), 2) = '10' 
                            SET @Month_ = 'K';
                        ELSE IF RIGHT(MONTH(getdate()), 2) = '11' 
                            SET @Month_ = 'L';
                        ELSE IF RIGHT(MONTH(getdate()), 2) = '12' 
                            SET @Month_= 'M';
						--END

						IF RIGHT(YEAR(getdate()), 2) = '11' -- year in format YY
                           SET @Year_ = 'E';
                        ELSE IF RIGHT(YEAR(getdate()), 2) = '12' 
                           SET @Year_ =  'F';
                        ELSE IF RIGHT(YEAR(getdate()), 2) = '13'
                            SET @Year_ =  'G';
                        ELSE IF RIGHT(YEAR(getdate()), 2) = '14' 
                           SET @Year_ =  'H';
                        ELSE IF RIGHT(YEAR(getdate()), 2) = '15'
                           SET @Year_ =  'J';
                        ELSE IF RIGHT(YEAR(getdate()), 2) = '16'
                           SET @Year_ =  'K';
                        ELSE IF RIGHT(YEAR(getdate()), 2) = '17'
                            SET @Year_ = 'L';
                        ELSE IF RIGHT(YEAR(getdate()), 2) = '18'
                           SET @Year_ =  'M';
                        ELSE IF RIGHT(YEAR(getdate()), 2) = '19'
                            SET @Year_ =  'N';
                        ELSE IF RIGHT(YEAR(getdate()), 2) = '20'
                            SET @Year_ =  'P';
                        ELSE IF RIGHT(YEAR(getdate()), 2) = '21'
                           SET @Year_= 'Q';
                        ELSE IF RIGHT(YEAR(getdate()), 2) = '22'
                           SET @Year_ =  'R';
                        ELSE IF RIGHT(YEAR(getdate()), 2) = '23'
                           SET @Year_ =  'S';
                        ELSE IF RIGHT(YEAR(getdate()), 2) = '24'
                           SET @Year_ =  'T';
                        ELSE IF RIGHT(YEAR(getdate()), 2) = '25'
                           SET @Year_ =  'U';
                        ELSE IF RIGHT(YEAR(getdate()), 2) = '26'
                           SET @Year_ =  'V';
                        ELSE IF RIGHT(YEAR(getdate()), 2) = '27'
                          SET @Year_ =  'W';
                        ELSE IF RIGHT(YEAR(getdate()), 2) = '28'
                           SET @Year_ =  'X';
                        ELSE IF RIGHT(YEAR(getdate()), 2) = '29'
                           SET @Year_ =  'Y';
                        ELSE IF RIGHT(YEAR(getdate()), 2) = '30'
                           SET @Year_=  'Z';
						--END

						SET @Val_OUT_ =CONCAT(@Ligne,@Ref,@Seq_,@Month_,@Year_,@Site); -- DMX content using concatenation 
						SET @Status_  = 1;
				END
			 ELSE 
					SET @Status_   = -1;
	        ELSE 
				SET @Status_   = -2;
		ELSE 
			 SET @Status_  = -3;	

	--check if DMX generated is duplicated

	SELECT @Val_Counter_ = COUNT(*)
	FROM [dbo].[LaserMarkingResults]
	WHERE [Serial] = @Val_Out;

	IF @Val_Counter_ > 0
		BEGIN
			SET @Status=-4;
			SET @Val_Counter_=0;
		END

	-- @Status_ :	  -1 = Site value is NULL
    --				  -2 = Reference (RRR) is NULL
    --                -3 = Site is NULL
    --                -4 = DMX duplicated
    --                -10 = SQL error
	
	BEGIN TRY  
		SELECT ERROR_NUMBER() AS ERRORNUMBER		
	END TRY  
	BEGIN CATCH  
		SET @Status=-10; --if we get an SQL error, the status will be -10
	END CATCH; 	

	SET @Val_OUT = @Val_OUT_;
	SET @Status = @Status_;

END
