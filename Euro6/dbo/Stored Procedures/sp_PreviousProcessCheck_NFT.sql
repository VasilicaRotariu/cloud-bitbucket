﻿-- =============================================
-- Author:		
-- Create date: 25/06/2014
-- Description:	Generic Previous Process Procedure 
-- Modified on 30/07/2014 to add MASMEC DAF injector build line
-- Modified on 03/11/2014 to add Selective Assembly 1
-- Modified on 29/01/2015 to add Selective Assembly 2
-- =============================================

--================== Previous process calling procedure ==============================


 --use [Euro6] 
 --Declare @ResultCode INT, @Error_Description VARCHAR(MAX)
 
 --EXEC dbo.sp_PreviousProcessCheck_nft
 --@SerialNumber = 'H375065823GMR' ,
 --@PlantNumber = '18932',             -- or @PlantNumber = 'OC3_20707',
 --@Result = @ResultCode OUTPUT,
 --@ErrorDescription = @Error_Description OUTPUT
 
 --SELECT @ResultCode, @Error_Description

-- =====================================================================================
CREATE PROCEDURE [dbo].[sp_PreviousProcessCheck_NFT]
(
	@SerialNumber VARCHAR(MAX),		-- The serial number/matrix to be checked
	@PlantNumber VARCHAR(MAX),		-- Plantnumber of the Machine Requesting Previous Result
	@Result VARCHAR(MAX) OUT,			-- Where 0 = PASS
	@ErrorDescription varchar(MAX) OUT,	-- If @Result anything other than 0 then this will explain why 
	@RunSelect TINYINT = 0				-- run a select statement at the end of the procedure if set by the user at 0
)
AS 
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ResultUID INT
		DECLARE @PlantNbr INT
		DECLARE @SeparatorIndex SMALLINT
		DECLARE @Parameter VARCHAR(MAX)

		-- separate the plant number and the parameter if any
		SELECT @SeparatorIndex = CHARINDEX('_', @PlantNumber);
		IF @SeparatorIndex != 0
			BEGIN
				SELECT @PlantNbr = CAST( SUBSTRING(@PlantNumber, @SeparatorIndex + 1, 50) AS int) , @Parameter = SUBSTRING(@PlantNumber, 0, @SeparatorIndex) 
			END
		ELSE
			BEGIN 
				SELECT @PlantNbr = CAST (@PlantNumber AS int)
			END

		-- Check for nozzle test previous process
		IF @PlantNbr IN (20683, 20790)
			BEGIN        
				SELECT TOP 1 @Result=[FailCode] FROM [Euro6].[dbo].[NNCAssembly]
				WHERE [NozzleSerial] = @SerialNumber
				ORDER BY [PDate] DESC;
				IF @Result = NULL OR @@ROWCOUNT = 0
					BEGIN
						SELECT @ResultUID=UID FROM [Euro6].[dbo].[20603_MatchGrind_Results]
								WHERE Nozzle_Matrix = @SerialNumber 
								IF @ResultUID = NULL or @@ROWCOUNT = 0
									BEGIN
										SET @Result = 999
										SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[NNCAssembly] for serial : '  + @SerialNumber
									END
								ELSE
									BEGIN
										SET @Result = 0
										SET @ErrorDescription = 'PASSED'
									END
					END
				ELSE
					BEGIN
						IF @Result != 0
							BEGIN
							--modification 27-09-16
								--IF @Result = 1 
								--	BEGIN
								--		SET @ErrorDescription = 'FAILED'
								--	END 
								--ELSE
								--	BEGIN
								--		SET @ErrorDescription = 'FAILED : Failure code ' + @Result
								--	END       
								SELECT @ResultUID=UID FROM [Euro6].[dbo].[20603_MatchGrind_Results]
								WHERE Nozzle_Matrix = @SerialNumber and PDate  > (
									SELECT TOP 1 [TDate]
    								FROM [Euro6].[dbo].[NNCAssembly] where NozzleSerial = @SerialNumber ORDER BY TDate DESC								
									)
								IF @ResultUID = NULL or @@ROWCOUNT = 0
									BEGIN
										IF @Result = 1 
											BEGIN
												SET @ErrorDescription = 'FAILED'
											END 
										ELSE
											BEGIN
												SET @ErrorDescription = 'FAILED : Failure code ' + @Result
											END
									END      
									
								ELSE
									BEGIN
										Set @Result = 0
										SET @ErrorDescription = 'PASSED'
									END
						    END

						ELSE                          
						 	BEGIN
								SET @Result = 0
								SET @ErrorDescription = 'PASSED'
							END
					END
			END

			-- AXIOME High pressure De-burr
			IF @PlantNbr IN (20688)
			BEGIN        
				SELECT TOP 1 @Result=[Final_Fail_Code] FROM [Euro6].[dbo].[ActF2NCVBoreSeatFace]
				WHERE [data_matrix] = @SerialNumber
				ORDER BY [PDate] DESC;
				IF @Result = NULL OR @@ROWCOUNT = 0
					BEGIN
						SET @Result = 999
						SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[ActF2NCVBoreSeatFace] for serial : '  + @SerialNumber
					END
				ELSE
					BEGIN
						IF @Result != 0
							BEGIN
								IF @Result = 1 
									BEGIN
										SET @ErrorDescription = 'FAILED'
									END 
								ELSE
									BEGIN
										SET @ErrorDescription = 'FAILED : Failure code ' + @Result
									END                                 
								END
							ELSE                          
								BEGIN
									SET @Result = 0
									SET @ErrorDescription = 'PASSED'
								END
					 END
			END

			-- MASMEC DAF Build line and Pilot build Line and Line 2
			IF @PlantNbr IN (20741,35824, 20184)
			BEGIN        
				IF @Parameter IN ('NCV')
				BEGIN
				

					DECLARE @RArmPress INT, @RSelAssy INT

					-- Normal previous process on the automatic armature press and manual armature press (Zygo) 
					SELECT TOP 1 @RArmPress = [Result] 
					FROM(
						SELECT [PDate], [Result] FROM [Euro6].[dbo].[NCVArmaturePress]
						WHERE [DataMatrix] = @SerialNumber
						UNION all
						SELECT [PDate], [Result] FROM [Euro6].[dbo].[Zygo]
						WHERE [SerialNumber] = @SerialNumber
					) as armPress
					ORDER BY [PDate] DESC

					-- Extra check to make sure the NCVs passed Selective assembly (this should be done on armature press)
					-- SELECT TOP 1 @RSelAssy = [Result]
					-- FROM(
					-- 	SELECT [UTC] AS [PDate], CAST (CASE WHEN [FailureCode] = 1 THEN 0 ELSE [FailureCode] END AS INT) AS [Result] 
					-- 	FROM [Euro6].[dbo].[SelAssyAssembly]
					-- 	WHERE [BodyMatrix] = @SerialNumber
					-- 	UNION all
					-- 	SELECT [UTC] AS [PDate],CAST(CASE WHEN [PassedGuide] = 1 AND [PassedSeat] = 1 THEN 0 ELSE 1 END AS INT) AS [Result]
					-- 	FROM [SELASSEMBLY\SELASSY].[SA_Stats].[dbo].[NCV_MeasureComplete]
					-- 	WHERE [Matrix] = @SerialNumber
					-- ) AS selAssy
					-- ORDER BY [PDate] DESC

					SET @RSelAssy = 0
					
					IF (@RArmPress = 0 and @RSelAssy = 0)
					BEGIN 
						SET @Result = 0
					END
					ELSE 
					BEGIN
						IF @RArmPress is null or @RSelAssy is null 
						BEGIN
							SET @Result = 999
						END
						ELSE 
						BEGIN
							SET @Result = 1
						END	
					END



					IF @Result = NULL OR @@ROWCOUNT = 0
						BEGIN
							SET @Result = 999
							SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[NCVArmaturePress] or [Euro6].[dbo].[Zygo] for serial : '  + @SerialNumber
						END
					ELSE
						BEGIN
							IF @Result != 0
								BEGIN
									IF @Result = 1 
										BEGIN
											SET @ErrorDescription = 'FAILED'
										END 
									ELSE
										BEGIN
											SET @ErrorDescription = 'FAILED : Failure code ' + @Result
										END                                 
								END
							ELSE                          
								BEGIN
									SET @Result = 0
									SET @ErrorDescription = 'PASSED'
								END
						END
				END
				ELSE
				IF @Parameter IN ('Nozzle')
				BEGIN
					DECLARE @ResultNNCTest SMALLINT 
					DECLARE @PDateNNCTest DateTime
					DECLARE @ResultZBLNTest SMALLINT
					DECLARE @PDateZBLNTest DateTime
					
					SELECT top 1 @PDateZBLNTest = [PDate], @ResultZBLNTest = [FailCode]
					FROM [Euro6].[dbo].[NozzleTestWithError_Results]
					WHERE [Serial] = @SerialNumber
					ORDER BY [PDate] DESC;
					
										
					SELECT top 1 @PDateNNCTest = [PDate], @ResultNNCTest=[OA_Result]
					FROM [NNCNozzleTest].[dbo].[Result]
					WHERE [Serial_No_Long] = @SerialNumber
					ORDER BY [PDate] DESC;	

					IF @PDateNNCTest > @PDateZBLNTest or @PDateZBLNTest is null
					BEGIN
						SET @Result = @ResultNNCTest							
					END
					ELSE
					BEGIN
						SET @Result = @ResultZBLNTest
					END

					IF @Result is NULL OR @@ROWCOUNT = 0
						BEGIN
							SET @Result = 999
							SET @ErrorDescription = 'FAILED : No Record Found in [NNCNozzleTest].[dbo].[Result] for serial : '  + @SerialNumber
						END
					ELSE
						BEGIN
							IF @Result != 0
								BEGIN
									IF @Result = 1 
										BEGIN
											SET @ErrorDescription = 'FAILED'
										END 
									ELSE
										BEGIN
											SET @ErrorDescription = 'FAILED : Failure code ' + @Result
										END                                 
								END
							ELSE                          
								BEGIN
									SET @Result = 0
									SET @ErrorDescription = 'PASSED'
								END
						END
				END
				ELSE
				BEGIN
					SET @Result = 999
					SET @ErrorDescription = 'FAILED : Error in the Previous Process Request'
				END
			
			END

			-- Sonplas NNC Assembly machine 20751 , 20752
			IF @PlantNbr IN (20751, 20752)
			BEGIN        
				DECLARE @EXHResult VARCHAR(MAX)
				DECLARE @EXHPdate DateTime

				IF @Parameter IN ('Nozzle')
				BEGIN
					DECLARE @NHResult VARCHAR(MAX)
					DECLARE @ONHResult VARCHAR(MAX)
					
					DECLARE @NHPDate DateTime
					DECLARE @ONHPDate DateTime		

					SELECT TOP 1 @NHResult = [FailCode], @NHPDate = [PDate] 
						FROM [Euro6].[dbo].[NozzleHegsProduction]
						WHERE [SerialNum] = @SerialNumber
					ORDER BY PDate desc

					SELECT TOP 1 @ONHResult = [UploadCode],  @ONHPDate = [PDate] 
						FROM [Euro6].[dbo].[OldNozzleHegsProduction]
						WHERE [SerialNum] = @SerialNumber
					ORDER BY [PDate] DESC;

					SELECT TOP 1 @EXHResult = EXH.[Failcode], @EXHPdate = EXH.[PDate] FROM [Euro6].[dbo].[20445_SonplasHEG_Measure_Results] AS EXH
					WHERE EXH.[Serial] = @SerialNumber
					or EXH.[Serial] = LOWER(@SerialNumber)
					ORDER BY EXH.[PDate] DESC;

					

					IF (@NHResult is NULL AND @ONHResult is NULL and @EXHResult is null)  
					BEGIN
						SET @Result = 999
						SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[NozzleHegsData] or [Euro6].[dbo].[OldNozzleHegs] or [Euro6].[dbo].[20445_SonplasHEG_Measure_Results] for serial : '  + @SerialNumber
					END
					ELSE
						BEGIN
							IF (@NHResult is NULL and @EXHResult is null) 
								BEGIN
									SET @Result = @ONHResult
								END 
							IF (@ONHResult is NULL and @EXHResult is null) 
								BEGIN
									SET @Result = @NHResult
								END
							IF (@NHResult is null  and @ONHResult is null) 
							BEGIN
								SET @Result = @EXHResult
							END
							

							IF (@NHPDate > @ONHPDate)
								BEGIN	
									if (@NHPDate > @EXHPDate)
										Begin						
											SET @Result = @NHResult	
										end								
								END
							if (@ONHPDate > @NHPDate)
								begin
									if (@ONHPDate > @EXHPDate)
										begin
											set @Result = @ONHResult
										end
								end

							if (@EXHPDate >  @ONHPDate)
								begin
									if (@EXHPDate > @NHPDate)
										begin 
											set @Result = @EXHResult
										end
								end

							IF (@Result != '0')
								BEGIN
									IF (@Result = '1') 
										BEGIN
											SET @ErrorDescription = 'FAILED'
										END 
									ELSE
										BEGIN
											if (@Result = '(0)  IO')
												BEGIN
													SET @Result = 0
													SET @ErrorDescription = 'PASSED'
												END
											ELSE
												BEGIN
													SET @ErrorDescription = 'FAILED : Failure code ' + @Result
													SET @Result = 1
												END						
										END                                 
								END
							ELSE                          
								BEGIN
									SET @Result = 0
									SET @ErrorDescription = 'PASSED'
								END
						END
					END
					ELSE
					BEGIN
						IF @Parameter IN ('Piston Guide')
						BEGIN	

							DECLARE @CHResult VARCHAR(MAX)
							DECLARE @MCHResult VARCHAR(MAX)
						
							DECLARE @CHPdate DateTime
							DECLARE @MCHdate DateTime
							
											
							SELECT TOP 1 @CHResult=[FailCode], @CHPdate = CH.[PDate] FROM [Euro6].[dbo].[ComponentHegsWithIntErrorCode] as CH 
							WHERE CH.[Serial] = @SerialNumber
							ORDER BY CH.[PDate] DESC;
						

							SELECT TOP 1 @EXHResult =  SUBSTRING(FailCode, CHARINDEX('(', FailCode) + 1, ABS(CHARINDEX(')', FailCode) 
                         - CHARINDEX('(', FailCode) - 1)),  @EXHPdate = EXH.[PDate] FROM [Euro6].[dbo].[20445_SonplasHEG_Measure_Results] AS EXH
							WHERE EXH.[Serial] = @SerialNumber
							ORDER BY EXH.[PDate] DESC;

							SELECT TOP 1 @MCHResult = MCH.[UnloadCode],  @MCHdate = MCH.[PDate] FROM [Euro6].[dbo].[ManualComponentHeg] AS MCH
							WHERE MCH.[Serial] = @SerialNumber
							ORDER BY MCH.[PDate] DESC;


							DECLARE @TempTable TABLE
							(
								result VARCHAR(MAX),
								pDate datetime
							)


							INSERT INTO @TempTable (result, pDate) VALUES(@CHResult, @CHPdate)
							INSERT INTO @TempTable (result, pDate) VALUES(@EXHResult, @EXHPdate)
							INSERT INTO @TempTable (result, pDate) VALUES(@MCHResult, @MCHdate)

						   	select @Result = result from @TempTable order by pDate
							
							IF (@Result is null)
								BEGIN
									SET @Result = '999'
									SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[ComponentHegs] or [Euro6].[dbo].[20445_SonplasHEG_Measure_Results] for serial : '  + @SerialNumber
								END

							/*
							IF (@CHResult is NULL and @EXHResult is null and @MCHResult is null)
								BEGIN
									SET @Result = '999'
									SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[ComponentHegs] or [Euro6].[dbo].[20445_SonplasHEG_Measure_Results] for serial : '  + @SerialNumber
								END
							ELSE
								BEGIN

								IF (@CHResult is NULL ) 
									BEGIN
									
										SET @Result = @EXHResult
									END 
								IF (@EXHResult is NULL) 
									BEGIN
										SET @Result = @CHResult
									END
							

								IF (@CHPdate > @EXHPdate)
									BEGIN									
										SET @Result = @CHResult								
									END
								ELSE
									BEGIN
										SET @Result =  @EXHResult
									END

									IF (@Result != '0')
								BEGIN
									IF (@Result = '1') 
										BEGIN
											SET @ErrorDescription = 'FAILED'
										END 
									ELSE
										BEGIN
											if (@Result = '(0)  IO')
												BEGIN
													SET @Result = 0
													SET @ErrorDescription = 'PASSED'
												END
											ELSE
												BEGIN
													SET @ErrorDescription = 'FAILED : Failure code ' + @Result
												END						
										END                                 
								END
							ELSE                          
								BEGIN
									SET @Result = 0
									SET @ErrorDescription = 'PASSED'
								END
							END
							*/
					END
				END				 
				
			END

			-- NCV Selective assembly 1 20513 and selective assembly 2 20707
			IF @PlantNbr IN (20523, 20707)
				BEGIN      
					IF (@RunSelect = 1)
					BEGIN        
						DECLARE @PrevTable VARCHAR(max)
				
						IF LEFT (CAST(@SerialNumber AS VARCHAR(2)),2) IN ('39') -- this is an OC3
						BEGIN
							SELECT TOP 1 @Result=[Result] FROM [Euro6].[dbo].[NCVStemholeDeburr]
							WHERE [Serial] = @SerialNumber
							ORDER BY [PDate] DESC;
							SET @PrevTable = '[NCVStemholeDeburr]'
						END
						ELSE IF LEFT (CAST(@SerialNumber AS VARCHAR(2)),2) IN ('30','34') -- this is an C6R or a F2P
						BEGIN
							SELECT TOP 1 @Result=[Result] FROM [Euro6].[dbo].[HighPressureDeburr]
							WHERE [SerialNumber] = @SerialNumber
							ORDER BY [PDate] DESC;
							SET @PrevTable = '[HighPressureDeburr]'
						END

						IF @Result = NULL OR @@ROWCOUNT = 0
						BEGIN
							SET @Result = 999
							SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].' + @PrevTable + ' for serial : '  + @SerialNumber
						END
						ELSE
						BEGIN
							IF (@Result = 0)
							BEGIN
								SET @ErrorDescription = 'PASSED'                       
							END
							ELSE                          
							BEGIN
								IF (@Result = 1)
								BEGIN
									SET @ErrorDescription = 'FAILED'
								END 
								ELSE
								BEGIN
									SET @ErrorDescription = 'FAILED : Failure code ' + @Result
								END 
							END
						END
					END
					ELSE 
					BEGIN
						IF @Parameter IN ('OC3')
						BEGIN
							SELECT TOP 1 @Result=[Result] FROM [Euro6].[dbo].[NCVStemholeDeburr]
							WHERE [Serial] = @SerialNumber
							ORDER BY [PDate] DESC;
							IF @Result = NULL OR @@ROWCOUNT = 0
							BEGIN
								SET @Result = 999
								SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[NCVStemholeDeburr] for serial : '  + @SerialNumber
							END
							ELSE
							BEGIN
								IF (@Result = 0)
								BEGIN
									SET @ErrorDescription = 'PASSED'                       
								END
								ELSE                          
								BEGIN
									IF (@Result = 1)
									BEGIN
										SET @ErrorDescription = 'FAILED'
									END 
									ELSE
									BEGIN
										SET @ErrorDescription = 'FAILED : Failure code ' + @Result
									END 
								END
							END
						END
						ELSE
						BEGIN
							IF @Parameter IN ('C6R')
							BEGIN
								SELECT TOP 1 @Result=[Result] FROM [Euro6].[dbo].[HighPressureDeburr]
								WHERE [SerialNumber] = @SerialNumber
								ORDER BY [PDate] DESC;
								IF @Result = NULL OR @@ROWCOUNT = 0
								BEGIN
									SET @Result = 999
									SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[HighPressureDeburr] for serial : '  + @SerialNumber
								END
								ELSE
								BEGIN
									IF (@Result = 0)
									BEGIN
										SET @ErrorDescription = 'PASSED'                       
									END
									ELSE                          
									BEGIN
										IF (@Result = 1)
										BEGIN
											SET @ErrorDescription = 'FAILED'
										END 
										ELSE
										BEGIN
											SET @ErrorDescription = 'FAILED : Failure code ' + @Result
										END 
									END
								END
							END
							ELSE IF @Parameter IN ('F2P')
							BEGIN
								SELECT TOP 1 @Result=[Result] FROM [Euro6].[dbo].[HighPressureDeburr]
								WHERE [SerialNumber] = @SerialNumber
								ORDER BY [PDate] DESC;
								IF @Result = NULL OR @@ROWCOUNT = 0
								BEGIN
									SET @Result = 999
									SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[HighPressureDeburr] for serial : '  + @SerialNumber
								END
								ELSE
								BEGIN
									IF (@Result = 0)
									BEGIN
										SET @ErrorDescription = 'PASSED'                       
									END
									ELSE                          
									BEGIN
										IF (@Result = 1)
										BEGIN
											SET @ErrorDescription = 'FAILED'
										END 
										ELSE
										BEGIN
											SET @ErrorDescription = 'FAILED : Failure code ' + @Result
										END 
									END
								END
							END
						ELSE
						BEGIN
							SELECT TOP 1 @Result=[Result] FROM [Euro6].[dbo].[HighPressureDeburr]
							WHERE [SerialNumber] = @SerialNumber
							ORDER BY [PDate] DESC;
							IF @Result = NULL OR @@ROWCOUNT = 0
							BEGIN
								SET @Result = 999 -- TODO:: Change this back to 999 when requested. (Adam Cleaver 26/10/2015)
								SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[HighPressureDeburr] for serial : '  + @SerialNumber
							END
							ELSE
							BEGIN
								IF (@Result = 0)
								BEGIN
									SET @ErrorDescription = 'PASSED'                       
								END
								ELSE                          
								BEGIN
									IF (@Result = 1)
									BEGIN
										SET @ErrorDescription = 'FAILED'
									END 
									ELSE
									BEGIN
										SET @ErrorDescription = 'FAILED : Failure code ' + @Result
									END 
								END
							END
						END
					END
				END
				IF (@Result = 0 AND @Parameter IN ('F2P', 'OC3' ))
				BEGIN
					SELECT TOP 1 @Result = FailCode
					FROM
					(
						SELECT [PlantNumber] as AssetNumber
							  ,[PDate]
							  ,[Serial] COLLATE DATABASE_DEFAULT as Serial
							  ,[UnLoadCode] as FailCode
						FROM [Euro6].[dbo].[ManualComponentHeg]
			
						UNION 

						SELECT [AssetNumber]
							,[PDate]
							,[Serial]COLLATE DATABASE_DEFAULT as Serial
							,[FailCode]      
						FROM [Euro6].[dbo].[ComponentHegsWithIntErrorCode]
					) as UT
					WHERE Serial = @SerialNumber
					ORDER BY PDate DESC

					IF @Result = NULL OR @@ROWCOUNT = 0
					BEGIN
						SET @Result = 999
						SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[ComponentHegsWithIntErrorCode] or [Euro6].[dbo].[ManualComponentHeg] for serial : '  + @SerialNumber
					END
					IF (@Result = 1)
					BEGIN
						SET @ErrorDescription = 'FAILED'
					END 
				END 
			END


			
		-- Check for Armature Press previous process
		IF @PlantNbr IN (20659)
			BEGIN        
				DECLARE @lastSelAssyDate datetime
				begin try
					SELECT TOP 1 @Result = [Result], @lastSelAssyDate = [PDate] 
					FROM(	-- Testing the assembly result table on Selective assembly 2
						SELECT [UTC] AS [PDate],
							CASE CAST (CASE WHEN [FailureCode] = 1 THEN 0 ELSE [FailureCode] END AS INT) -- CASE stateemnt to define wich code to diplay between body and assembly
							WHEN [Result] THEN [Result] -- both the same display any (body)
							WHEN 0 THEN [Result] -- assembly at 0 display body
							WHEN CAST (CASE WHEN [FailureCode] = 1 THEN 0 ELSE [FailureCode] END AS INT) + [Result] THEN [FailureCode] -- add assemby and boddy compare to assembly, that means body is 0 display assembly
							ELSE [FailureCode] -- else dispaly assembly
							END as [Result]
	
						FROM [Euro6].[dbo].[SelAssyAssembly] as Ass
						inner Join (	-- Making sure it passed the body mesurement 
										SELECT TOP 1 [UTC] AS [PDate], CAST (CASE WHEN [FailureCode] = 1 THEN 0 ELSE [FailureCode] END AS INT) AS [Result], [BodyMatrix]	
										FROM [Euro6].[dbo].[SelAssyBody]
										WHERE [BodyMatrix] = @SerialNumber
										ORDER BY PDate desc
										) as Body on Ass.[BodyMatrix] = Body.[BodyMatrix]
						WHERE Ass.[BodyMatrix] = @SerialNumber
					
					-- TODO::
					
					-- UNION all
					-- -- Testing the Final measurement on Selective assembly 1
					-- SELECT [UTC] AS [PDate],CAST(CASE WHEN [PassedGuide] = 1 AND [PassedSeat] = 1 THEN 0 ELSE 1 END AS INT) AS [Result]
					-- FROM [SELASSEMBLY\SELASSY].[SA_Stats].[dbo].[NCV_MeasureComplete]
					-- WHERE [Matrix] = @SerialNumber
					

					) AS selAssy
					ORDER BY PDate DESC
				END TRY
					 BEGIN CATCH
							  SELECT TOP 1 @Result = [Result], @lastSelAssyDate = [PDate] 
						FROM(	-- Testing the assembly result table on Selective assembly 2
							SELECT [UTC] AS [PDate],
								CASE CAST (CASE WHEN [FailureCode] = 1 THEN 0 ELSE [FailureCode] END AS INT) -- CASE stateemnt to define wich code to diplay between body and assembly
								WHEN [Result] THEN [Result] -- both the same display any (body)
								WHEN 0 THEN [Result] -- assembly at 0 display body
								WHEN CAST (CASE WHEN [FailureCode] = 1 THEN 0 ELSE [FailureCode] END AS INT) + [Result] THEN [FailureCode] -- add assemby and boddy compare to assembly, that means body is 0 display assembly
								ELSE [FailureCode] -- else dispaly assembly
								END as [Result]
	
							FROM [Euro6].[dbo].[SelAssyAssembly] as Ass
							inner Join (	-- Making sure it passed the body mesurement 
											SELECT TOP 1 [UTC] AS [PDate], CAST (CASE WHEN [FailureCode] = 1 THEN 0 ELSE [FailureCode] END AS INT) AS [Result], [BodyMatrix]	
											FROM [Euro6].[dbo].[SelAssyBody]
											WHERE [BodyMatrix] = @SerialNumber
											ORDER BY PDate desc
											) as Body on Ass.[BodyMatrix] = Body.[BodyMatrix]
							WHERE Ass.[BodyMatrix] = @SerialNumber
										) AS selAssy
							ORDER BY PDate DESC
					 END CATCH
				-- Display the Code '111' if failed more than 3 time the armature press.
				IF @Result = '0'
				BEGIN
					DECLARE @failCount INT
					SELECT @failCount = count(*)
					FROM(
						SELECT [PDate] FROM [Euro6].[dbo].[NCVArmaturePress] -- check Armature press DB
						WHERE [DataMatrix] = @SerialNumber
						AND [PDate] > @lastSelAssyDate
						AND [Result] != '0'
						UNION all
						SELECT [PDate] FROM [Euro6].[dbo].[Zygo] -- check Zygo (manual assembly) DB
						WHERE [SerialNumber] = @SerialNumber
						AND [PDate] > @lastSelAssyDate
						AND [Result] != '0'
					) as armPress

					IF @failCount > 3 -- compare the count with 3
					BEGIN
						set @Result = '111'
					END
				END
				IF @Result is NULL 
					BEGIN
						SET @Result = 999
						SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[SelAssyAssembly] or [SELASSEMBLY\SELASSY].[SA_Stats].[dbo].[NCV_MeasureComplete] for serial : '  + @SerialNumber
					END
				ELSE
					BEGIN
						IF @Result != 0
							BEGIN
								IF @Result = 1 
									BEGIN
										SET @ErrorDescription = 'FAILED'
									END 
								ELSE
									BEGIN
										SET @ErrorDescription = 'FAILED : Failure code ' + @Result
									END                                 
							END
						ELSE                          
							BEGIN
								SET @Result = 0
								SET @ErrorDescription = 'PASSED'
							END
					END
			END


			-- Scan and Pack
			IF @PlantNbr IN (20319, 20318)
			BEGIN
			DECLARE @AssemblyDate DATETIME
			DECLARE @ISN VARCHAR(MAX)			
			DECLARE @NozzleSerial VARCHAR(MAX)
			DECLARE @NCVSerial VARCHAR(MAX)
			DECLARE @PistonGuideSerial VARCHAR(MAX)

				IF @Parameter IN ('MX11', 'MX13')
				BEGIN
					SELECT 
						@AssemblyDate = AssemblyDate, 
						@ISN = ISN, 
						@NozzleSerial =  nnc.NozzleSerial, 
						@NCVSerial = NCVSerial, --nnc Nozzle Serial, we join on this so doesnt matter if NNC or not
						@PistonGuideSerial = nnc.PistonGuideSerial
					FROM (
						SELECT MAX(AssemblyDate) as AssemblyDate, 
								ISN,
								MAX(NCVSerial) as NCVSerial, 
								MAX(NozzleSerial) as NozzleSerial		 
						FROM (
							SELECT 
								PDate as AssemblyDate, 
								Serial as ISN, 
								NCV_SerialNumber as NCVSerial, 
								Nozzle_SerialNumber as NozzleSerial, 
								NCVCount, 	
								NozzleCount
							FROM MASMEC_SelAsmb_OP20B_Results 
							UNION
							SELECT 
								PDate as AssemblyDate, 
								ISN, 
								NCVSerial as NCVSerial, 
								NozzleSerial as NozzleSerial, 
								NCVSerialCount, 
								NozzleSerialCount
							FROM LowVolumeMX13Line1
						) as DAFResults
						GROUP BY ISN
					) as DAFResultsRefined
					INNER JOIN NNCAssembly as nnc on (nnc.NozzleSerial = DAFResultsRefined.NozzleSerial)
					WHERE RIGHT(ISN, 9) in ( --remove the first character (line #)
						SELECT isn FROM (values (@SerialNumber)) as X(isn) 
						) 
				END
				IF @Parameter IN ('MDEG')
				BEGIN
					SELECT 
						@AssemblyDate = AssemblyDate, 
						@ISN = ISN, 
						@NCVSerial = NCVSerial, 
						@NozzleSerial = nnc.NozzleSerial, 
						@PistonGuideSerial = nnc.PistonGuideSerial
					FROM (
						SELECT 
							MAX(AssemblyDate) as AssemblyDate, 
							ISN,
							MAX(NCVSerial) as NCVSerial, 
							MAX(NozzleSerial) as NozzleSerial
				
						FROM (
							SELECT
								n.Pdate as AssemblyDate, 
								n.Injector_ID as ISN, 
								NCV_DataMatrix as NCVSerial,
								Nozzle_DataMatrix as NozzleSerial 		
							FROM NLS as n 
							FULL JOIN RSPL on (n.Injector_ID = RSPL.Injector_ID) 
							UNION ALL
							SELECT 
								Pdate as AssemblyDate, 
								InjectorSerialNumber as ISN, 
								NCVSerialNumber, 
								NozzleSerialNumber as NozzleSerial	
							FROM MDEGLowVolumeLineResults 
						) as MdegResults	
						GROUP BY ISN
					) as MDEGRefined
					INNER JOIN NNCAssembly as nnc on (nnc.NozzleSerial = MDEGRefined.NozzleSerial)
					WHERE ISN in (
							SELECT h.ISN collate Latin1_General_CI_AS as isn 
							FROM [uksud-db02].[Euro6MDEGTest].[dbo].[Header] as h
							INNER JOIN [uksud-db02].[Euro6MDEGTest].[dbo].[DaimlerSerialNumber] as dsn on h.UTID = dsn.UTID and h.PlantNumber = dsn.PlantNumber
							where dsn.DaimlerSerialNumber collate Latin1_General_CI_AS = @SerialNumber
								)
				END

				IF (@NCVSerial is NOT NULL) 
				BEGIN
					DECLARE @Pl VARCHAR(MAX)
					IF LEFT (CAST(@NCVSerial AS VARCHAR(2)),2) IN ('39')
					BEGIN
						SET @Pl = 'OC3_20707'
					END
					ELSE IF LEFT (CAST(@NCVSerial AS VARCHAR(2)),2) IN ('30','34')
					BEGIN
						SET @Pl = 'F2P_20707'
					END

					Declare @ResultCode INT, @Error_Description VARCHAR(MAX)
					EXEC dbo.sp_PreviousProcessCheck
					@SerialNumber = @NCVSerial ,
					@PlantNumber = @Pl,             
					@Result = @ResultCode OUTPUT,
					@ErrorDescription = @Error_Description OUTPUT

					SELECT @Result = @ResultCode, @ErrorDescription = @Error_Description
				END
				ELSE
				BEGIN
					SET @Result = 404
					SET @ErrorDescription = 'FAIL : No NCV Serial Found' 
				END

				
			END


			-- NFT
			IF @PlantNbr IN (18932, 18933)
				
				BEGIN        
					SELECT TOP 1 @Result=a.[Failcode] FROM 
					(select SerialNum,UploadCode as [Failcode], PDate from [Euro6].[dbo].[OldNozzleHegsProduction]  WHERE [SerialNum] = @SerialNumber
					union all select serialnum, failcode as [Failcode], PDate from [Euro6].[dbo].[NozzleHegsProduction] WHERE [SerialNum] = @SerialNumber
					union all select serialnum, failcode as [FailCode], PDate from [ImportExternal].[dbo].[NozzleHegsProduction] WHERE [SerialNum] = @SerialNumber
					) as a
					
					ORDER BY [PDate] DESC;

					IF @Result = NULL OR @@ROWCOUNT = 0
						BEGIN
							SET @Result = 999
							SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[OldNozzleHegsProduction] for serial : '  + @SerialNumber
						END
					ELSE
						BEGIN
							IF @Result != 0
								BEGIN
									IF @Result = 1 
										BEGIN
										
											SET @ErrorDescription = 'FAILED'
										END 
									ELSE
										BEGIN
											SET @ErrorDescription = 'FAILED : Failure code ' + @Result
										END                                 
								END
							ELSE                          
								BEGIN
									SET @Result = 0
									SET @ErrorDescription = 'PASSED'
								END
						END
				END
				--IF @PlantNbr IN (18932, 18933)
				
			--	BEGIN        
			--		SELECT TOP 1 @Result=a.[Failcode] FROM (select SerialNum,UploadCode as [Failcode], PDate from [Euro6].[dbo].[OldNozzleHegsProduction] union all select serialnum, failcode as [Failcode], PDate from [Euro6].[dbo].[NozzleHegsProduction]) as a
			--		WHERE [SerialNum] = @SerialNumber
			--		ORDER BY [PDate] DESC;

			--		IF @Result = NULL OR @@ROWCOUNT = 0
			--			BEGIN
			--				SET @Result = 999
			--				SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[OldNozzleHegsProduction] for serial : '  + @SerialNumber
			--			END
			--		ELSE
			--			BEGIN
			--				IF @Result != 0
			--					BEGIN
			--						IF @Result = 1 
			--							BEGIN
										
			--								SET @ErrorDescription = 'FAILED'
			--							END 
			--						ELSE
			--							BEGIN
			--								SET @ErrorDescription = 'FAILED : Failure code ' + @Result
			--							END                                 
			--					END
			--				ELSE                          
			--					BEGIN
			--						SET @Result = 0
			--						SET @ErrorDescription = 'PASSED'
			--					END
			--			END
			--	END
		SET NOCOUNT OFF;
	END


IF (@RunSelect = 1)
BEGIN
	SELECT @Result, @ErrorDescription
END
