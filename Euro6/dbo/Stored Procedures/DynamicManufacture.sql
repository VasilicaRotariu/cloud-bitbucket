﻿/*
This is the landing zone for the DynamicManufacture system DLL.
*/
CREATE PROCEDURE [dbo].[DynamicManufacture]
	@ProcessName varchar(255),
	@Part1ID varchar(25),
	@Part2ID varchar(25) = '',
	@Part3ID varchar(25) = '',
	@InputData1 sql_variant = NULL,
	@InputData2 sql_variant = NULL,
	@InputData3 sql_variant = NULL,
	@InputData4 sql_variant = NULL,
	@InputData5 sql_variant = NULL,
	@InputData6 sql_variant = NULL,
	@OutputData1 sql_variant = NULL OUTPUT,
	@OutputData2 sql_variant = NULL OUTPUT,
	@OutputData3 sql_variant = NULL OUTPUT,
	@OutputData4 sql_variant = NULL OUTPUT,
	@OutputData5 sql_variant = NULL OUTPUT,
	@OutputData6 sql_variant = NULL OUTPUT,
	@OutputData7 sql_variant = NULL OUTPUT,
	@OutputData8 sql_variant = NULL OUTPUT,
	@OutputData9 sql_variant = NULL OUTPUT,
	@ProcessData [DynamicManufactureData] READONLY
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @ReturnedValue int = -1

	--- Call sub-prodedure here based on process (the first is a test, second is debug)
    IF @ProcessName = 'Test'
	BEGIN
		EXEC @ReturnedValue = DynamicManufacture_Test @Part1ID = @Part1ID, 
														@Part2ID = @Part2ID, 
														@Part3ID = @Part3ID, 
														@InputData1 = @InputData1, 
														@InputData2 = @InputData2, 
														@InputData3 = @InputData3, 
														@OutputData1 = @OutputData1 OUTPUT, 
														@OutputData2 = @OutputData2 OUTPUT, 
														@OutputData3 = @OutputData3 OUTPUT, 
														@OutputData4 = @OutputData4 OUTPUT, 
														@OutputData5 = @OutputData5 OUTPUT, 
														@OutputData6 = @OutputData6 OUTPUT, 
														@OutputData7 = @OutputData7 OUTPUT, 
														@OutputData8 = @OutputData8 OUTPUT, 
														@OutputData9 = @OutputData9 OUTPUT,
														@ProcessData = @ProcessData
	END
	ELSE IF @ProcessName = 'Debug'
	BEGIN
		--EXEC @ReturnedValue = DynamicManufacture_Test @Part1ID = @Part1ID, @Part2ID = @Part2ID, @Part3ID = @Part3ID, @ProcData1 = @ProcData1, @ProcData2 = @ProcData2, @ProcData3 = @ProcData3, @Result1 = @Result1 OUTPUT, @Result2 = @Result2 OUTPUT, @Result3 = @Result3 OUTPUT, @Result4 = @Result4 OUTPUT, @Result5 = @Result5 OUTPUT, @Result6 = @Result6 OUTPUT
		WAITFOR DELAY '00:00:05' 
	END
    ELSE IF @ProcessName = 'SelectiveAssembly' OR @ProcessName = 'SA' OR @ProcessName = 'SA1' OR @ProcessName = 'SA2' OR @ProcessName = 'SA3'
	BEGIN
		EXEC @ReturnedValue = DynamicManufacture_SelectiveAssembly @Part1ID = @Part1ID, @Result1 = @OutputData1 OUTPUT
	END
	ELSE IF @ProcessName = 'ArmaturePress'
	BEGIN
		EXEC @ReturnedValue = DynamicManufacture_ArmaturePress @Part1ID = @Part1ID, @Result1 = @OutputData1 OUTPUT
	END

	RETURN @ReturnedValue

END
