﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_SAProduction]
	-- Add the parameters for the stored procedure here
	@Start datetime, @End datetime
AS
BEGIN
	SELECT COUNT(*) as Total
	FROM SelAssyAssembly
	WHERE UTC BETWEEN @Start AND @END
		AND FailureCode = 1
END

GO
GRANT EXECUTE
    ON OBJECT::[dbo].[sp_SAProduction] TO [dbcache]
    AS [dbo];

