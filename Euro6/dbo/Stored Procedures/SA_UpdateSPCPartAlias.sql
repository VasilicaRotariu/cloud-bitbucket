﻿/*
SPC...Alias fields are used in SPC destination database to 
correctly locate the part type.
*/
  CREATE PROCEDURE [dbo].[SA_UpdateSPCPartAlias]
	@PlantNumber int,
	@ComponentType varchar(20),
	@SPCPartNumberAlias varchar(50),
	@SPCPartDescriptionAlias varchar(50)
  AS
  DECLARE @ret int = 0
	UPDATE [SelAssy_MasterComponentTypes]
	SET [SPCPartNumberAlias] = @SPCPartNumberAlias, [SPCPartDescriptionAlias] = @SPCPartDescriptionAlias
	WHERE [PlantNumber] = @PlantNumber
	AND [ComponentType] = @ComponentType
	SET @ret = @@ROWCOUNT

	SELECT * 
	FROM [SelAssy_MasterComponentTypes] 
	WHERE [PlantNumber] = @PlantNumber
	AND [ComponentType] = @ComponentType

	RETURN @ret
