﻿-- =============================================
-- Author:		<Alex Anderson,,Name>
-- Create date: <04/03/17,,>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_PreviousProcessCheck2]
	@SerialNumber VARCHAR(MAX),		-- The serial number/matrix to be checked
	@PlantNumber VARCHAR(MAX),		-- Plantnumber of the Machine Requesting Previous Result
	@Result VARCHAR(MAX) OUT,			-- Where 0 = PASS
	@ErrorDescription varchar(MAX) OUT,	-- If @Result anything other than 0 then this will explain why 
	@RunSelect TINYINT = 0				-- run a select statement at the end of the procedure if set by the user at 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @ResultUID INT
	DECLARE @PlantNbr INT
	DECLARE @SeparatorIndex SMALLINT
	DECLARE @Parameter VARCHAR(MAX)

	--
	-- separate the plant number and the parameter if any
	SELECT @SeparatorIndex = CHARINDEX('_', @PlantNumber);
	IF @SeparatorIndex != 0
		BEGIN
			SELECT @PlantNbr = CAST( SUBSTRING(@PlantNumber, @SeparatorIndex + 1, 50) AS int) , @Parameter = SUBSTRING(@PlantNumber, 0, @SeparatorIndex) 
		END
	ELSE
		BEGIN 
			SELECT @PlantNbr = CAST (@PlantNumber AS int)
		END

	-- NNC Nozzle Test Machines
	IF @PlantNbr IN (20683, 20790)
	BEGIN
		EXEC dbo.sp_PreviousProcessCheck_NNCTest 
			@PlantNbr, 		
			@ResultUID, 
			@Parameter,
			@SerialNumber, 
			@Result OUTPUT, 
			@ErrorDescription OUTPUT

			-- If this Failed for reason 999 - this mean no record found
			IF @Result IN ( 999, '999' )
			BEGIN
				--query ARCHIVE Structure
				EXEC dbo.sp_PreviousProcessCheck_NNCTest_Archive 
					@PlantNbr, 		
					@ResultUID, 
					@Parameter,
					@SerialNumber, 
					@Result OUTPUT, 
					@ErrorDescription OUTPUT
			END 
	END

	-- Injector Assy
	ELSE IF @PlantNbr IN (20741, 35824, 20184)
	BEGIN     
		EXEC dbo.sp_PreviousProcessCheck_InjectorAssy 
			@PlantNbr, 		
			@ResultUID, 
			@Parameter,
			@SerialNumber, 
			@Result OUTPUT, 
			@ErrorDescription OUTPUT

			IF @Result IN ( 999, '999' )
			BEGIN
				--query ARCHIVE Structure
				EXEC dbo.sp_PreviousProcessCheck_InjectorAssy 
					@PlantNbr, 		
					@ResultUID, 
					@Parameter,
					@SerialNumber, 
					@Result OUTPUT, 
					@ErrorDescription OUTPUT
			END 
	END   
END
