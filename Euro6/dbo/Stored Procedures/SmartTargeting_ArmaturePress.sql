﻿
CREATE PROCEDURE [dbo].[SmartTargeting_ArmaturePress]
	@data dbo.TargetedRawData OUTPUT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @partID nvarchar(20)
	DECLARE @ret int = 0
	DECLARE @result float = 0
	DECLARE @result1 float = 0

	DECLARE @CollarCl float = null
	DECLARE @Lift float = null

	-- Get the input data value
	SET @partID = @data.GetPartID(0)

	-- Get database data (no matter what type)
	SELECT TOP 1 @Lift = A.[AssemblyLift], @CollarCl = B.[BodyCollarDiameter] - P.[PinCollarDiameter]
	FROM [dbo].[SelAssyAssembly] A
	JOIN [dbo].[SA2_PinHistory] P ON P.Pin_UID = A.PinIdentification
	JOIN [dbo].[SelAssyBody] B ON B.[BodyMatrix] = A.[BodyMatrix]
	WHERE P.[PinHistory_UID] = (SELECT TOP 1 [PinHistory_UID] FROM [dbo].[SA2_PinHistory] WHERE [Pin_UID] = A.[PinIdentification] ORDER BY EventUTC DESC)
	AND B.[UID] = (SELECT TOP 1 UID FROM [dbo].[SelAssyBody] WHERE [BodyMatrix] = A.[BodyMatrix] ORDER BY UTC DESC)
	AND A.[UID] = (SELECT TOP 1 UID FROM [dbo].[SelAssyAssembly] WHERE [BodyMatrix] = A.[BodyMatrix] ORDER BY UTC DESC)
	AND A.[BodyMatrix] = @partID
	AND A.[BodyMatrix] <> '' -- important!

	-- Do calculation only if data is valid, and...
	IF (@CollarCl IS NOT NULL AND @Lift IS NOT NULL)
	BEGIN

		-- Set query result values (enables client calculation - this must not be numeric if the query failed).
		SET @data.SetOutputValue_SqlDouble(1, ROUND(@Lift, 9))
		SET @data.SetOutputValue_SqlDouble(2, ROUND(@CollarCl, 9))

        /*  -- Updated 25th Jul 2017 - no longer needed.

		IF (@partID LIKE '39%') -- is and the correct type!
		BEGIN
			--SET @result = (@Lift - 0.02235) + (@CollarCl - 0.006)
			--SET @result = (2 * (@Lift - 0.02275)) + (2 * (@CollarCl - 0.006))
			--SET @result = (2 * (@Lift - 0.022335)) + (2 * (@CollarCl - 0.006))
			--SET @result = (2 * (@Lift - 0.022335)) + (2 * (@CollarCl - 0.005))
			--SET @result = (@Lift - 0.022335) + (@CollarCl - 0.005)
			--SET @result = (2 * (@Lift - 0.022335)) + (2 * (@CollarCl - 0.005))
			--SET @result = (2 * (@Lift - 0.023)) + (2*(@CollarCl - 0.005))
            SET @result = (2 * (@Lift - 0.024)) + (2*(@CollarCl - 0.005))
			SET @result1 = @result

			-- Sense check - set to 0 if result is outside ±0.005
			IF (@result NOT BETWEEN -0.005 AND 0.005)
			BEGIN
				SET @result = 0
				SET @data.SetOutputValue_SqlBoolean(3, 1) -- indicates the data was rounded - 39* type only
			END
			ELSE
			BEGIN
				SET @data.SetOutputValue_SqlBoolean(3, 0) -- indicates the data was not reset - 39* type only
			END
		END

		-- This sets result to 0 if the hour is an odd hour.
		IF (DATEDIFF(s, SYSDATETIME(), '2017-06-09 12:00:00') > 0)
		BEGIN
			IF (datepart(hour, getdate()) % 2 = 1)
			BEGIN
				SET @result = 0
			END
		END

		*/  -- Updated 25th Jul 2017 - no longer needed.

	END

    /*  -- Updated 25th Jul 2017 - no longer needed.

	-- Set the output result value (does not matter if this is 0)
	SET @data.SetOutputValue_SqlDouble(0, ROUND(@result, 9))
	SET @data.SetOutputValue_SqlDouble(4, @result1)

    */  -- Updated 25th Jul 2017 - no longer needed.

	RETURN @ret
END




