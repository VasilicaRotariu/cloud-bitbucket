﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
 
CREATE PROCEDURE [dbo].[sp_MASMEC_GetAllTelemetry] 
	@Start datetime,
	@End datetime
AS
BEGIN 
	SET NOCOUNT ON;

	SELECT 1 as Station, cnt as Count, Data_Start as Start, Data_End as [End], Status_Code as StatusCode, Fault_Code as FaultCode, Warning_Code as WarningCode
	From MASMEC_SelAsmb_OP10_Telemetry
	WHERE Data_Start BETWEEN @Start and @End
	UNION
	SELECT 2 as Station, cnt as Count, Data_Start as Start, Data_End as [End], Status_Code as StatusCode, Fault_Code as FaultCode, Warning_Code as WarningCode
	From MASMEC_SelAsmb_OP20A_Telemetry
	WHERE Data_Start BETWEEN @Start and @End 
	UNION
	SELECT 3 as Station, cnt as Count, Data_Start as Start, Data_End as [End], Status_Code as StatusCode, Fault_Code as FaultCode, Warning_Code as WarningCode
	From MASMEC_SelAsmb_OP20B_Telemetry
	WHERE Data_Start BETWEEN @Start and @End 
	UNION
	SELECT 4 as Station, cnt as Count, Data_Start as Start, Data_End as [End], Status_Code as StatusCode, Fault_Code as FaultCode, Warning_Code as WarningCode
	From MASMEC_SelAsmb_OP20C_Telemetry
	WHERE Data_Start BETWEEN @Start and @End 
	UNION
	SELECT 5 as Station, cnt as Count, Data_Start as Start, Data_End as [End], Status_Code as StatusCode, Fault_Code as FaultCode, Warning_Code as WarningCode
	From MASMEC_SelAsmb_OP30A_Telemetry
	WHERE Data_Start BETWEEN @Start and @End 
	UNION
	SELECT 6 as Station, cnt as Count, Data_Start as Start, Data_End as [End], Status_Code as StatusCode, Fault_Code as FaultCode, Warning_Code as WarningCode
	From MASMEC_SelAsmb_OP30B_Telemetry
	WHERE Data_Start BETWEEN @Start and @End 
	UNION
	SELECT 7 as Station, cnt as Count, Data_Start as Start, Data_End as [End], Status_Code as StatusCode, Fault_Code as FaultCode, Warning_Code as WarningCode
	From MASMEC_SelAsmb_OP30C_Telemetry
	WHERE Data_Start BETWEEN @Start and @End  
END

GO
GRANT EXECUTE
    ON OBJECT::[dbo].[sp_MASMEC_GetAllTelemetry] TO [dbcache]
    AS [dbo];

