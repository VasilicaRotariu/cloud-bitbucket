﻿-- =============================================
-- Author:		James Lee
-- Create date: 20131120
-- Description:	Returns build count of line 1
-- =============================================
CREATE PROCEDURE [dbo].[sp_E6_Assembly_L1_ZBLNHourlyCount]
	-- Add the parameters for the stored procedure here
	@StartPoint int = 0, 
	@NumberofHoursBack int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	-- Fill the table variable with the rows for your result set
	DECLARE @Start datetime = DATEADD(HOUR, -@NumberofHoursBack - @StartPoint, CURRENT_TIMESTAMP), @End datetime = DATEADD(HOUR, -@StartPoint + 1, CURRENT_TIMESTAMP)

	/* Standard Declares */	
	DECLARE @_ResultCount table (Date datetime, Hour int, HourTime time, Quantity float)
	DECLARE @_HourTotals table (Date datetime, HourStart time, Hour int, HourTotal float)
	DECLARE @_FullTimeRange table (DateRange datetime)
	DECLARE @table_StartEnd_Range table (startTime datetime, endTime datetime)	
	
	/* Adjust date time entry to be yyyy-mm-dd HH:00:00.000 */
	SET @Start = DATEADD(hour, DATEPART(hour,@Start), DATEADD(dd, 0, DATEDIFF(dd, 0, @Start)))
	SET @End = DATEADD(hour, DATEPART(hour,@End), DATEADD(dd, 0, DATEDIFF(dd, 0, @End)))

	/**** Pre populate a timespan table to start every point at 0 ****/
	INSERT INTO @table_StartEnd_Range
	select @Start, @End

	INSERT INTO @_FullTimeRange
	select
		dateadd(hh, n.Number, t.startTime) as Hours
	from @table_StartEnd_Range t
		inner join util_Numbers n
    -- assuming your numbers start at 1 rather than 0
    on n.Number-1 <= datediff(hh, t.startTime, t.endTime)

	/**** Get the results for the timespan *****/
	INSERT INTO @_ResultCount
	SELECT CAST(PDate as Date) as Date, DATEPART(HOUR, PDate) as Hour,  CONVERT(time,DATEADD(hour,DATEPART(HOUR, PDate), '00:00:00.000'), 114)      ,
	COUNT(*) as Quantity FROM Euro6.dbo.ZBLNTorque
	WHERE PDate between @Start and @End
	and LineID = 1 and GLOBAL_RESULT = 0
	GROUP BY CAST(PDate as Date), DATEPART(hour, PDate) 
	order by Date desc
	
	/***** Get the day totals  ******/
	INSERT INTO @_HourTotals
	SELECT FTR.DateRange, overview.HourTime, overview.Hour, overview.DayTotal FROM @_FullTimeRange FTR
	LEFT JOIN (
	SELECT Date + CAST(CONVERT(time,DATEADD(hour,Hour, '00:00:00.000'), 114) as datetime) as Date, 
	CONVERT(time,DATEADD(hour,Hour, '00:00:00.000'), 114) as HourTime, Hour, SUM(Quantity) as DayTotal FROM @_ResultCount
	group by date  , Hour
	) overview on overview.Date = FTR.DateRange
	order by overview.Date desc

	/* Return the results */
	--INSERT INTO @Results( DayTotals.Date, DayTotals.HourStart, DayTotals.Hour, DayTotals.HourTotal)
	DECLARE @Value int 
	SELECT @Value = SUM(DayTotals.HourTotal) FROM @_HourTotals DayTotals
	Where DayTotals.Date BETWEEN @Start and @End and HourStart IS NOT NULL
	--order by DayTotals.Date desc

	RETURN @Value
END
