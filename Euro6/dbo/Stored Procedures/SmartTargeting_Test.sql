﻿




-- Returns some test data to the client.
CREATE PROCEDURE [dbo].[SmartTargeting_Test]
	@data dbo.TargetedRawData OUTPUT
AS
BEGIN

	DECLARE @partID nvarchar(20)
	DECLARE @ret int = 0
	DECLARE @result0 float = 0.0001
	DECLARE @result1 int = 12345
	DECLARE @result2 bit = 1
	DECLARE @result3 datetime2(7) = sysutcdatetime()
	DECLARE @result4 datetime2(7) = sysdatetime()
	DECLARE @result5 varchar = null
	SET NOCOUNT ON;

	-- Get the input data value
	SET @partID = @data.GetPartID(0)

	-- Do calculation

	-- Set the output data value
	SET @data.SetOutputValue_SqlDouble(0, @result0)
	SET @data.SetOutputValue_SqlInt32(1, @result1)
	SET @data.SetOutputValue_SqlBoolean(2, @result2)
	SET @data.SetOutputValue_SqlDateTime(3, @result3)
	SET @data.SetOutputValue_SqlDateTime(4, @result4)
	SET @data.SetOutputValue_SqlString(5, @result5)
	SET @data.SetOutputValue_SqlString(6, @partID)

	RETURN @ret
END




