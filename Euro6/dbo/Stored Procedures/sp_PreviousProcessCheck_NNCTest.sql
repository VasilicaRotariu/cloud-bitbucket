﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_PreviousProcessCheck_NNCTest] 
	-- Add the parameters for the stored procedure here
	@PlantNbr INT,
	@ResultUID INT,
	@Parameter VARCHAR(MAX),

	@SerialNumber VARCHAR(MAX),	 
	@Result VARCHAR(MAX) OUT,			
	@ErrorDescription varchar(MAX) OUT 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    IF @PlantNbr IN (20683, 20790)
	BEGIN       
		-- QUERY 
		SELECT TOP 1 @Result=[FailCode] FROM [Euro6].[dbo].[NNCAssembly]
		WHERE [NozzleSerial] = @SerialNumber
		ORDER BY [PDate] DESC;

		IF @Result = NULL OR @@ROWCOUNT = 0
		BEGIN
			SELECT @ResultUID=UID FROM [Euro6].[dbo].[20603_MatchGrind_Results]
					WHERE Nozzle_Matrix = @SerialNumber 
					IF @ResultUID = NULL or @@ROWCOUNT = 0
						BEGIN
							SET @Result = 999
							SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[NNCAssembly] for serial : '  + @SerialNumber
						END
					ELSE
						BEGIN
							SET @Result = 0
							SET @ErrorDescription = 'PASSED'
						END
		END
		ELSE
		BEGIN
			IF @Result != 0
				BEGIN
				--modification 27-09-16
					--IF @Result = 1 
					--	BEGIN
					--		SET @ErrorDescription = 'FAILED'
					--	END 
					--ELSE
					--	BEGIN
					--		SET @ErrorDescription = 'FAILED : Failure code ' + @Result
					--	END       
					SELECT @ResultUID=UID FROM [Euro6].[dbo].[20603_MatchGrind_Results]
					WHERE Nozzle_Matrix = @SerialNumber and PDate  > (
						SELECT TOP 1 [TDate]
    					FROM [Euro6].[dbo].[NNCAssembly] where NozzleSerial = @SerialNumber ORDER BY TDate DESC								
						)
					IF @ResultUID = NULL or @@ROWCOUNT = 0
						BEGIN
							IF @Result = 1 
								BEGIN
									SET @ErrorDescription = 'FAILED'
								END 
							ELSE
								BEGIN
									SET @ErrorDescription = 'FAILED : Failure code ' + @Result
								END
						END      
									
					ELSE
						BEGIN
							Set @Result = 0
							SET @ErrorDescription = 'PASSED'
						END
				END

			ELSE                          
				BEGIN
					SET @Result = 0
					SET @ErrorDescription = 'PASSED'
				END
		END
	END
END
