﻿-- Converts process identity and part identity from normalised values
-- to values that the PreviousProcessCheck stored procedure needs.
-- For example, converts 'SA1', "
CREATE PROCEDURE [dbo].[_ConvertNewPPCToLegacy]
	@ProcessID_IN varchar(255),
	@PartID_IN varchar(255),
	@ProcessID_OUT varchar(255) OUTPUT,
	@PartID_OUT varchar(255) OUTPUT
AS
BEGIN
	-- 0 means that the method has converted the value.
	-- Anything else means that the value was not converted and the operation will fail within the DLL.
	DECLARE @Ret int = 1;

	SET @ProcessID_OUT = @ProcessID_IN
	SET @PartID_OUT = @PartID_IN

	/*
	LIKE expressions:
	%   Any string of zero or more characters.
	_   Any single character.
	[ ] Any single character within the specified range ([a-f]) or set ([abcdef]).
	[^] Any single character not within the specified range ([^a-f]) or set ([^abcdef]).
	*/

	IF @ProcessID_IN IN ('TEST')
	BEGIN
		IF @PartID_IN LIKE '%TEST%'
		BEGIN
			SET @ProcessID_OUT = '987654'
			SET @PartID_OUT = 'XYZ987654321A'
			SET @Ret = 0
		END
	END

	RETURN @Ret;
END
