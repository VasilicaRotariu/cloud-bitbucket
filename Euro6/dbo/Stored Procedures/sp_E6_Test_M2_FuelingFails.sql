﻿-- =============================================
-- Author:		James Lee
-- Create date: 20130911
-- Description:	Gets the number of points passing the fueling - ish... it's late...
-- =============================================
CREATE PROCEDURE [dbo].[sp_E6_Test_M2_FuelingFails] 
	-- Add the parameters for the stored procedure here
	@startdate datetime = '20130903 00:00:00.000', 
	@enddate datetime = '20130911 23:59:59.000',
	@SelectedTestPlans ListBox_StringDisplayStringValue READONLY,
	@SelectedBatches ListBox_StringDisplayStringValue READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DECLARE @Selection int = 6

IF (@Selection = 0 or @Selection = 1)
BEGIN
SELECT TOP 1000 [SetRP]
      ,[NCVDuration]
      ,AVG([TrimmedInjDelivery]) as Avg_TrimdInjDel
      ,AVG([NominalDelivery]) as Avg_NomDel
      ,(SELECT AVG(ERROR) as Avg_Error FROM Euro6.dbo.TestData
where UF = 1 and SetRP = TD.SetRP and NCVDuration = TD.NCVDuration) as Avg_Error_UF
      ,SUM([UF]) as UF_Fails
	  ,(SELECT AVG(ERROR) as Avg_Error FROM Euro6.dbo.TestData
where LF = 1 and SetRP = TD.SetRP and NCVDuration = TD.NCVDuration) as Avg_Error_LF
      ,SUM([LF]) as LF_Fails
  FROM [Euro6].[dbo].[TestData] TD

  where UF IS NOT NULL and (UF = 1 or LF = 1)

  Group by SetRP, NCVDuration

  --order by UF_Fails desc
  
  order by SetRP desc, NCVDuration desc

  END
  
IF (@Selection = 0 or @Selection = 2)
BEGIN
  /********************************************************************/

  /****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [SetRP]
      ,[NCVDuration]
 --     ,AVG([TrimmedInjDelivery]) as Avg_TrimdInjDel
 --     ,AVG([NominalDelivery]) as Avg_NomDel
 --     ,(SELECT AVG(ERROR) as Avg_Error FROM Euro6.dbo.TestData
--where UF = 1 and SetRP = TD.SetRP and NCVDuration = TD.NCVDuration) as Avg_Error_UF
      ,SUM([UF]) as UF_Fails
--	  ,(SELECT AVG(ERROR) as Avg_Error FROM Euro6.dbo.TestData
--where LF = 1 and SetRP = TD.SetRP and NCVDuration = TD.NCVDuration) as Avg_Error_LF
      ,SUM([LF]) as LF_Fails
  FROM (
  SELECT [TestDataID]
      ,[TestNo]
      ,[SetRP]
      ,[FAQ]
      ,[BLFlow]
      ,[RailPressure]
      ,[InjTemp]
      ,[Delivery]
      ,[BLPressure]
      ,[InjSquirt1]
      ,[InjSquirt2]
      ,[InjSquirt3]
      ,[InjSquirt4]
      ,[InjSquirt5]
      ,[Del3SDs]
      ,[NominalMDP]
      ,[Trim]
      ,[TrimmedInjDelivery]
      ,[NominalDelivery]
      ,[Error]
      ,[UF]
      ,[LF]
  , NCVDuration = (case
  WHEN NCVDuration between 0 and 99 then 0
  WHEN NCVDuration between 100 and 199 then 100
  WHEN NCVDuration between 200 and 299 then 200
  WHEN NCVDuration between 300 and 399 then 300
  WHEN NCVDuration between 400 and 499 then 400
  WHEN NCVDuration between 500 and 599 then 500
  WHEN NCVDuration between 600 and 699 then 600
  WHEN NCVDuration between 700 and 799 then 700
  WHEN NCVDuration between 800 and 899 then 800
  WHEN NCVDuration between 900 and 999 then 900
  WHEN NCVDuration between 1000 and 1099 then 1000
  WHEN NCVDuration between 1100 and 1199 then 1100
  WHEN NCVDuration between 1200 and 1299 then 1200
  WHEN NCVDuration between 1300 and 1399 then 1300
  WHEN NCVDuration between 1400 and 1499 then 1400
  WHEN NCVDuration between 1500 and 1599 then 1500
  WHEN NCVDuration between 1600 and 1699 then 1600
  WHEN NCVDuration between 1700 and 1799 then 1700
  WHEN NCVDuration between 1800 and 1899 then 1800
  WHEN NCVDuration between 1900 and 1999 then 1900
  WHEN NCVDuration between 2000 and 2099 then 2000
  WHEN NCVDuration between 2100 and 2199 then 2100
  WHEN NCVDuration between 2200 and 2299 then 2200
  WHEN NCVDuration between 2300 and 2399 then 2300
  else 2400 end)
  from [Euro6].[dbo].[TestData]
  ) TD

  JOIN ( SELECT * FROM Header ) Head on Head.TestNo = TD.TestNo

  where UF IS NOT NULL and (UF = 1 or LF = 1)

  --and Head.Date BETWEEN '20130902 00:00:00' and '20130902 23:59:59'

  Group by SetRP, NCVDuration

  order by UF_Fails desc
  
  --order by SetRP desc, NCVDuration desc
  
  END

  
IF (@Selection = 0 or @Selection = 3)
BEGIN

  DECLARE @RPNCVFails table(
  [RP-NCVDuration] varchar(10),
  UF_Fails int,
  LF_Fails int)

  INSERT INTO @RPNCVFails 
SELECT TOP 1000 
CONVERT(varchar(4),[SetRP]) + '-' + CONVERT(varchar(4),[NCVDuration]) as [RP-NCVDuration]
      ,SUM([UF]) as UF_Fails
      ,SUM([LF]) as LF_Fails
  FROM (
  SELECT [TestNo]
      ,[SetRP]
      ,[UF]
      ,[LF]
	 -- ,NCVDuration
  , NCVDuration = (case
  WHEN NCVDuration between 0 and 99 then 50
  WHEN NCVDuration between 100 and 199 then 150
  WHEN NCVDuration between 200 and 299 then 250
  WHEN NCVDuration between 300 and 399 then 350
  WHEN NCVDuration between 400 and 499 then 450
  WHEN NCVDuration between 500 and 599 then 550
  WHEN NCVDuration between 600 and 699 then 650
  WHEN NCVDuration between 700 and 799 then 750
  WHEN NCVDuration between 800 and 899 then 850
  WHEN NCVDuration between 900 and 999 then 950
  WHEN NCVDuration between 1000 and 1099 then 1050
  WHEN NCVDuration between 1100 and 1199 then 1150
  WHEN NCVDuration between 1200 and 1299 then 1250
  WHEN NCVDuration between 1300 and 1399 then 1350
  WHEN NCVDuration between 1400 and 1499 then 1450
  WHEN NCVDuration between 1500 and 1599 then 1550
  WHEN NCVDuration between 1600 and 1699 then 1650
  WHEN NCVDuration between 1700 and 1799 then 1750
  WHEN NCVDuration between 1800 and 1899 then 1850
  WHEN NCVDuration between 1900 and 1999 then 1950
  WHEN NCVDuration between 2000 and 2099 then 2050
  WHEN NCVDuration between 2100 and 2199 then 2150
  WHEN NCVDuration between 2200 and 2299 then 2250
  WHEN NCVDuration between 2300 and 2399 then 2350
  else 2400 end) 
  from [Euro6].[dbo].[TestData]
  ) TD

  JOIN ( SELECT * FROM Header ) Head on Head.TestNo = TD.TestNo

  where UF IS NOT NULL and (UF = 1 or LF = 1)

  and Head.Date BETWEEN @startdate and @enddate

  Group by SetRP, NCVDuration

  --order by UF_Fails desc
  
  --order by SetRP desc, NCVDuration desc
  
  DECLARE @Results table ( Duration varchar(13), Fails int)
  
  INSERT INTO @Results
  SELECT [RP-NCVDuration] + '_UF', UF_Fails FROM @RPNCVFails
  
  INSERT INTO @Results
  SELECT [RP-NCVDuration] + '_LF', LF_Fails FROM @RPNCVFails
  
  SELECT * FROM @Results
  order by Fails desc

  END
  
   
IF (@Selection = 0 or @Selection = 4)
BEGIN

  DECLARE @RPNCVFails4 table(
  [RP-NCVDuration] varchar(10),
  UF_Fails int,
  LF_Fails int)

  INSERT INTO @RPNCVFails4 
SELECT TOP 1000 
CONVERT(varchar(4),[SetRP]) + '-' + CONVERT(varchar(4),[NCVDuration]) as [RP-NCVDuration]
      ,SUM([UF]) as UF_Fails
      ,SUM([LF]) as LF_Fails
  FROM (
  SELECT [TestNo]
      ,[SetRP]
      ,[UF]
      ,[LF]
	 -- ,NCVDuration
  , NCVDuration = (case
  WHEN NCVDuration between 0 and 99 then 50
  WHEN NCVDuration between 100 and 199 then 150
  WHEN NCVDuration between 200 and 299 then 250
  WHEN NCVDuration between 300 and 399 then 350
  WHEN NCVDuration between 400 and 499 then 450
  WHEN NCVDuration between 500 and 599 then 550
  WHEN NCVDuration between 600 and 699 then 650
  WHEN NCVDuration between 700 and 799 then 750
  WHEN NCVDuration between 800 and 899 then 850
  WHEN NCVDuration between 900 and 999 then 950
  WHEN NCVDuration between 1000 and 1099 then 1050
  WHEN NCVDuration between 1100 and 1199 then 1150
  WHEN NCVDuration between 1200 and 1299 then 1250
  WHEN NCVDuration between 1300 and 1399 then 1350
  WHEN NCVDuration between 1400 and 1499 then 1450
  WHEN NCVDuration between 1500 and 1599 then 1550
  WHEN NCVDuration between 1600 and 1699 then 1650
  WHEN NCVDuration between 1700 and 1799 then 1750
  WHEN NCVDuration between 1800 and 1899 then 1850
  WHEN NCVDuration between 1900 and 1999 then 1950
  WHEN NCVDuration between 2000 and 2099 then 2050
  WHEN NCVDuration between 2100 and 2199 then 2150
  WHEN NCVDuration between 2200 and 2299 then 2250
  WHEN NCVDuration between 2300 and 2399 then 2350
  else 2400 end)   
  from [Euro6].[dbo].[TestData]
  ) TD

  JOIN ( SELECT TestNo, Date FROM Header
	INNER JOIN @SelectedTestPlans STP on Header.TestPlan = STP.DisplayValue
	INNER JOIN @SelectedBatches SB on Header.Batch = SB.DisplayValue 
	) Head on Head.TestNo = TD.TestNo

  where UF IS NOT NULL and (UF = 1 or LF = 1)

  and Head.Date BETWEEN @startdate and @enddate

  Group by SetRP, NCVDuration

  --order by UF_Fails desc
  
  --order by SetRP desc, NCVDuration desc
  
  DECLARE @Results4 table ( Duration varchar(13), Fails int)
  
  INSERT INTO @Results4
  SELECT [RP-NCVDuration] + '_UF', UF_Fails FROM @RPNCVFails4
  
  INSERT INTO @Results4
  SELECT [RP-NCVDuration] + '_LF', LF_Fails FROM @RPNCVFails4
  
  SELECT TOP 100 * FROM @Results4
  order by Fails desc

  END
   
IF (@Selection = 0 or @Selection = 5)
BEGIN

  DECLARE @RPNCVFails5 table(
  [RP-NCVDuration] varchar(10),
  UF_Fails int,
  LF_Fails int)

  INSERT INTO @RPNCVFails5 
SELECT TOP 1000 
CONVERT(varchar(4),[SetRP]) + '-' + CONVERT(varchar(4),[NCVDuration]) as [RP-NCVDuration]
      ,SUM([UF]) as UF_Fails
      ,SUM([LF]) as LF_Fails
  FROM (
  SELECT [TestNo]
      ,[SetRP]
      ,[UF]
      ,[LF]
	  ,NCVDuration
  /*, NCVDuration = (case
  WHEN NCVDuration between 0 and 99 then 50
  WHEN NCVDuration between 100 and 199 then 150
  WHEN NCVDuration between 200 and 299 then 250
  WHEN NCVDuration between 300 and 399 then 350
  WHEN NCVDuration between 400 and 499 then 450
  WHEN NCVDuration between 500 and 599 then 550
  WHEN NCVDuration between 600 and 699 then 650
  WHEN NCVDuration between 700 and 799 then 750
  WHEN NCVDuration between 800 and 899 then 850
  WHEN NCVDuration between 900 and 999 then 950
  WHEN NCVDuration between 1000 and 1099 then 1050
  WHEN NCVDuration between 1100 and 1199 then 1150
  WHEN NCVDuration between 1200 and 1299 then 1250
  WHEN NCVDuration between 1300 and 1399 then 1350
  WHEN NCVDuration between 1400 and 1499 then 1450
  WHEN NCVDuration between 1500 and 1599 then 1550
  WHEN NCVDuration between 1600 and 1699 then 1650
  WHEN NCVDuration between 1700 and 1799 then 1750
  WHEN NCVDuration between 1800 and 1899 then 1850
  WHEN NCVDuration between 1900 and 1999 then 1950
  WHEN NCVDuration between 2000 and 2099 then 2050
  WHEN NCVDuration between 2100 and 2199 then 2150
  WHEN NCVDuration between 2200 and 2299 then 2250
  WHEN NCVDuration between 2300 and 2399 then 2350
  else 2400 end)   */
  from [Euro6].[dbo].[TestData]
  ) TD

  JOIN ( SELECT TestNo, Date FROM Header
  where 
  TestPlan IN (SELECT DisplayValue FROM @SelectedTestPlans) and
  Batch IN (SELECT DisplayValue FROM @SelectedBatches) and
  Date Between @startdate and @enddate
	--INNER JOIN @SelectedTestPlans STP on Header.TestPlan = STP.DisplayValue
	--INNER JOIN @SelectedBatches SB on Header.Batch = SB.DisplayValue 
	) Head on Head.TestNo = TD.TestNo

  where UF IS NOT NULL and (UF = 1 or LF = 1)

  --and Head.Date BETWEEN @startdate and @enddate

  Group by SetRP, NCVDuration

  --order by UF_Fails desc
  
  --order by SetRP desc, NCVDuration desc
  
  DECLARE @Results5 table ( Duration varchar(13), Fails int)
  
  INSERT INTO @Results5
  SELECT [RP-NCVDuration] + '_UF', UF_Fails FROM @RPNCVFails5
  
  INSERT INTO @Results5
  SELECT [RP-NCVDuration] + '_LF', LF_Fails FROM @RPNCVFails5
  
  SELECT * FROM @Results5
  order by Fails desc

  END
  
IF (@Selection = 0 or @Selection = 6)
BEGIN

  DECLARE @RPNCVFails6 table(
  [RP-NCVDuration] varchar(10),
  UF_Fails int,
  LF_Fails int)

  INSERT INTO @RPNCVFails6 
SELECT TOP 1000 
CONVERT(varchar(4),[SetRP]) + '-' + CONVERT(varchar(4),[NCVDuration]) as [RP-NCVDuration]
      ,SUM([UF]) as UF_Fails
      ,SUM([LF]) as LF_Fails
  FROM (
  SELECT [TestNo]
      ,[SetRP]
      ,[UF]
      ,[LF]
	  ,NCVDuration
  from [Euro6].[dbo].[TestData]
  ) TD

  JOIN ( SELECT TestNo, Date FROM Header
  where 
  TestPlan IN (SELECT DisplayValue FROM @SelectedTestPlans) and
  Batch IN (SELECT DisplayValue FROM @SelectedBatches) and
  Date Between @startdate and @enddate
	) Head on Head.TestNo = TD.TestNo

  where UF IS NOT NULL and (UF = 1 or LF = 1)

  Group by SetRP, NCVDuration
  
  --DECLARE @Results6 table ( Duration varchar(13), Fails int)
  
  INSERT INTO TestData_Fails_tmp
  SELECT @@SPID, [RP-NCVDuration] + '_UF', UF_Fails FROM @RPNCVFails6
  
  INSERT INTO TestData_Fails_tmp
  SELECT @@SPID, [RP-NCVDuration] + '_LF', LF_Fails FROM @RPNCVFails6
  
  SELECT Duration, Fails FROM TestData_Fails_tmp
  order by Fails desc

  DELETE TestData_Fails_tmp WHERE [SessionID] = @@SPID

  END

END
