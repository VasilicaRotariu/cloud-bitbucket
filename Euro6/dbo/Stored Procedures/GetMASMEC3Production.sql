﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetMASMEC3Production]
	@Start datetime, @End datetime
AS
BEGIN
	 
	SET NOCOUNT ON;

	SELECT COUNT(*) as Total
	FROM MASMEC_SelAsmb_OP30C_Results
	WHERE Result = 0 
		AND PDate BETWEEN @Start AND @End
END
