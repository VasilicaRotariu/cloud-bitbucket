﻿
CREATE PROCEDURE [dbo].[sp_SA2_GetStatsLastRollingHour] 
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @DateStart as datetime2(7)
	DECLARE @DateEnd as datetime2(7)
	SET @DateEnd = SYSUTCDATETIME()
	SET @DateStart = DATEADD(hh, -1, @DateEnd)
	EXEC sp_SA2_GetStatsBetweenDates @DateStart, @DateEnd
END

GO
GRANT EXECUTE
    ON OBJECT::[dbo].[sp_SA2_GetStatsLastRollingHour] TO [dbcache]
    AS [dbo];

