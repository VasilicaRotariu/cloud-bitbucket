﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_PreviousProcessCheck_InjectorAssy] 
	-- Add the parameters for the stored procedure here
	@PlantNbr INT,
	@ResultUID INT,
	@Parameter VARCHAR(MAX),

	@SerialNumber VARCHAR(MAX),	 
	@Result VARCHAR(MAX) OUT,			
	@ErrorDescription varchar(MAX) OUT 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF @Parameter IN ('NCV')
		BEGIN
				

			DECLARE @RArmPress INT, @RSelAssy INT

			-- Normal previous process on the automatic armature press and manual armature press (Zygo) 
			SELECT TOP 1 @RArmPress = [Result] 
			FROM(
				SELECT [PDate], [Result] FROM [Euro6].[dbo].[NCVArmaturePress]
				WHERE [DataMatrix] = @SerialNumber
				UNION all
				SELECT [PDate], [Result] FROM [Euro6].[dbo].[Zygo]
				WHERE [SerialNumber] = @SerialNumber
			) as armPress
			ORDER BY [PDate] DESC

			-- Extra check to make sure the NCVs passed Selective assembly (this should be done on armature press)
			-- SELECT TOP 1 @RSelAssy = [Result]
			-- FROM(
			-- 	SELECT [UTC] AS [PDate], CAST (CASE WHEN [FailureCode] = 1 THEN 0 ELSE [FailureCode] END AS INT) AS [Result] 
			-- 	FROM [Euro6].[dbo].[SelAssyAssembly]
			-- 	WHERE [BodyMatrix] = @SerialNumber
			-- 	UNION all
			-- 	SELECT [UTC] AS [PDate],CAST(CASE WHEN [PassedGuide] = 1 AND [PassedSeat] = 1 THEN 0 ELSE 1 END AS INT) AS [Result]
			-- 	FROM [SELASSEMBLY\SELASSY].[SA_Stats].[dbo].[NCV_MeasureComplete]
			-- 	WHERE [Matrix] = @SerialNumber
			-- ) AS selAssy
			-- ORDER BY [PDate] DESC

			SET @RSelAssy = 0
					
			IF (@RArmPress = 0 and @RSelAssy = 0)
			BEGIN 
				SET @Result = 0
			END
			ELSE 
			BEGIN
				IF @RArmPress is null or @RSelAssy is null 
				BEGIN
					SET @Result = 999
				END
				ELSE 
				BEGIN
					SET @Result = 1
				END	
			END



			IF @Result = NULL OR @@ROWCOUNT = 0
				BEGIN
					SET @Result = 999
					SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[NCVArmaturePress] or [Euro6].[dbo].[Zygo] for serial : '  + @SerialNumber
				END
			ELSE
				BEGIN
					IF @Result != 0
						BEGIN
							IF @Result = 1 
								BEGIN
									SET @ErrorDescription = 'FAILED'
								END 
							ELSE
								BEGIN
									SET @ErrorDescription = 'FAILED : Failure code ' + @Result
								END                                 
						END
					ELSE                          
						BEGIN
							SET @Result = 0
							SET @ErrorDescription = 'PASSED'
						END
				END
		END
		ELSE
		IF @Parameter IN ('Nozzle')
		BEGIN
			DECLARE @ResultNNCTest SMALLINT 
			DECLARE @PDateNNCTest DateTime
			DECLARE @ResultZBLNTest SMALLINT
			DECLARE @PDateZBLNTest DateTime
					
			SELECT top 1 @PDateZBLNTest = [PDate], @ResultZBLNTest = [FailCode]
			FROM [Euro6].[dbo].[NozzleTestWithError_Results]
			WHERE [Serial] = @SerialNumber
			ORDER BY [PDate] DESC;
					
										
			SELECT top 1 @PDateNNCTest = [PDate], @ResultNNCTest=[OA_Result]
			FROM [NNCNozzleTest].[dbo].[Result]
			WHERE [Serial_No_Long] = @SerialNumber
			ORDER BY [PDate] DESC;	

			IF @PDateNNCTest > @PDateZBLNTest or @PDateZBLNTest is null
			BEGIN
				SET @Result = @ResultNNCTest							
			END
			ELSE
			BEGIN
				SET @Result = @ResultZBLNTest
			END

			IF @Result is NULL OR @@ROWCOUNT = 0
				BEGIN
					SET @Result = 999
					SET @ErrorDescription = 'FAILED : No Record Found in [NNCNozzleTest].[dbo].[Result] for serial : '  + @SerialNumber
				END
			ELSE
				BEGIN
					IF @Result != 0
						BEGIN
							IF @Result = 1 
								BEGIN
									SET @ErrorDescription = 'FAILED'
								END 
							ELSE
								BEGIN
									SET @ErrorDescription = 'FAILED : Failure code ' + @Result
								END                                 
						END
					ELSE                          
						BEGIN
							SET @Result = 0
							SET @ErrorDescription = 'PASSED'
						END
				END
		END
		ELSE
		BEGIN
			SET @Result = 999
			SET @ErrorDescription = 'FAILED : Error in the Previous Process Request'
		END
			
	END

	-- Sonplas NNC Assembly machine 20751 , 20752
	IF @PlantNbr IN (20751, 20752)
	BEGIN        
		DECLARE @EXHResult VARCHAR(MAX)
		DECLARE @EXHPdate DateTime

		IF @Parameter IN ('Nozzle')
		BEGIN
			DECLARE @NHResult VARCHAR(MAX)
			DECLARE @ONHResult VARCHAR(MAX)
					
			DECLARE @NHPDate DateTime
			DECLARE @ONHPDate DateTime		

			SELECT TOP 1 @NHResult = [FailCode], @NHPDate = [PDate] 
				FROM [Euro6].[dbo].[NozzleHegsProduction]
				WHERE [SerialNum] = @SerialNumber
			ORDER BY PDate desc

			SELECT TOP 1 @ONHResult = [UploadCode],  @ONHPDate = [PDate] 
				FROM [Euro6].[dbo].[OldNozzleHegsProduction]
				WHERE [SerialNum] = @SerialNumber
			ORDER BY [PDate] DESC;

			SELECT TOP 1 @EXHResult = EXH.[Failcode],  @EXHPdate = EXH.[PDate] FROM [Euro6].[dbo].[20445_SonplasHEG_Measure_Results] AS EXH
			WHERE EXH.[Serial] = @SerialNumber
			or EXH.[Serial] = LOWER(@SerialNumber)
			ORDER BY EXH.[PDate] DESC;

					

			IF (@NHResult is NULL AND @ONHResult is NULL and @EXHResult is null)  
			BEGIN
				SET @Result = 999
				SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[NozzleHegsData] or [Euro6].[dbo].[OldNozzleHegs] or [Euro6].[dbo].[20445_SonplasHEG_Measure_Results] for serial : '  + @SerialNumber
			END
			ELSE
				BEGIN
					IF (@NHResult is NULL and @EXHResult is null) 
						BEGIN
							SET @Result = @ONHResult
						END 
					IF (@ONHResult is NULL and @EXHResult is null) 
						BEGIN
							SET @Result = @NHResult
						END
					IF (@NHResult is null  and @ONHResult is null) 
					BEGIN
						SET @Result = @EXHResult
					END
							

					IF (@NHPDate > @ONHPDate)
						BEGIN	
							if (@NHPDate > @EXHPDate)
								Begin						
									SET @Result = @NHResult	
								end								
						END
					if (@ONHPDate > @NHPDate)
						begin
							if (@ONHPDate > @EXHPDate)
								begin
									set @Result = @ONHResult
								end
						end

					if (@EXHPDate >  @ONHPDate)
						begin
							if (@EXHPDate > @NHPDate)
								begin 
									set @Result = @EXHResult
								end
						end

					IF (@Result != '0')
						BEGIN
							IF (@Result = '1') 
								BEGIN
									SET @ErrorDescription = 'FAILED'
								END 
							ELSE
								BEGIN
									if (@Result = '(0)  IO')
										BEGIN
											SET @Result = 0
											SET @ErrorDescription = 'PASSED'
										END
									ELSE
										BEGIN
											SET @ErrorDescription = 'FAILED : Failure code ' + @Result
											SET @Result = 1
										END						
								END                                 
						END
					ELSE                          
						BEGIN
							SET @Result = 0
							SET @ErrorDescription = 'PASSED'
						END
				END
			END
			ELSE
			BEGIN
				IF @Parameter IN ('Piston Guide')
				BEGIN	

					DECLARE @CHResult VARCHAR(MAX)

						
					DECLARE @CHPdate DateTime
							
											
					SELECT TOP 1 @CHResult=[FailCode], @CHPdate = CH.[PDate] FROM [Euro6].[dbo].[ComponentHegs] as CH 
					WHERE CH.[Serial] = @SerialNumber
					ORDER BY CH.[PDate] DESC;
						

					SELECT TOP 1 @EXHResult = EXH.[Failcode],  @EXHPdate = EXH.[PDate] FROM [Euro6].[dbo].[20445_SonplasHEG_Measure_Results] AS EXH
					WHERE EXH.[Serial] = @SerialNumber
					ORDER BY EXH.[PDate] DESC;


					IF (@CHResult is NULL and @EXHResult is null)
						BEGIN
							SET @Result = '999'
							SET @ErrorDescription = 'FAILED : No Record Found in [Euro6].[dbo].[ComponentHegs] or [Euro6].[dbo].[20445_SonplasHEG_Measure_Results] for serial : '  + @SerialNumber
						END
					ELSE
						BEGIN

						IF (@CHResult is NULL ) 
							BEGIN
									
								SET @Result = @EXHResult
							END 
						IF (@EXHResult is NULL) 
							BEGIN
								SET @Result = @CHResult
							END
							

						IF (@CHPdate > @EXHPdate)
							BEGIN									
								SET @Result = @CHResult								
							END
						ELSE
							BEGIN
								SET @Result =  @EXHResult
							END

							IF (@Result != '0')
						BEGIN
							IF (@Result = '1') 
								BEGIN
									SET @ErrorDescription = 'FAILED'
								END 
							ELSE
								BEGIN
									if (@Result = '(0)  IO')
										BEGIN
											SET @Result = 0
											SET @ErrorDescription = 'PASSED'
										END
									ELSE
										BEGIN
											SET @ErrorDescription = 'FAILED : Failure code ' + @Result
										END						
								END                                 
						END
					ELSE                          
						BEGIN
							SET @Result = 0
							SET @ErrorDescription = 'PASSED'
						END
						END
			END
		END				 
				
	END 
