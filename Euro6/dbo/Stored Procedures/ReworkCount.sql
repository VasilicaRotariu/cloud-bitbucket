﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ReworkCount]
	@Serial varchar(13)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT ISNULL(SUM(Cnt), 0)
	FROM 
	(
		SELECT COUNT(*) as Cnt
		FROM dbo.NozzleHegsProduction 
		WHERE SerialNum = @Serial

		UNION

		SELECT COUNT(*) as Cnt
		FROM dbo.OldNozzleHegsProduction
		WHERE SerialNum = @Serial
	) as Totals

END
