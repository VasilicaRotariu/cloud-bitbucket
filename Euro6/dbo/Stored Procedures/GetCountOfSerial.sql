﻿-- =============================================
-- Author:		<James with a bit of help from Alex>
-- Create date: <3/6/2015>
-- Description:	<Gets the amount of times a serial exists in a table>
-- =============================================
CREATE PROCEDURE [dbo].[GetCountOfSerial]
	-- Add the parameters for the stored procedure here
	@Serial varchar(20),
	@TableName varchar(MAX),
	@ColumnName varchar(MAX),
	@Count int OUT
AS
BEGIN  
	 
	DECLARE @ReworkedAmount int;
	DECLARE @sqlCommand nvarchar(MAX)

	DECLARE @ParmDefinition nvarchar(500);

	SELECT @sqlCommand = 'SELECT @ReworkedAmountOUT = COUNT(' + @ColumnName + ') FROM ' + @TableName + ' WHERE ' + @ColumnName + ' = ' + @Serial ;
	SET @ParmDefinition = N'@ReworkedAmountOUT int OUTPUT'; 

	EXEC SP_EXECUTESQL @sqlCommand, @ParmDefinition, @ReworkedAmountOUT = @ReworkedAmount OUTPUT;
		
    SET @Count = @ReworkedAmount
END
