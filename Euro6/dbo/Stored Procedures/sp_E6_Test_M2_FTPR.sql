﻿
-- =============================================
-- Author:		James Lee
-- Create date: 2013-08-14
-- Description:	Generates Daily FTPR and Output
-- =============================================
CREATE PROCEDURE [dbo].[sp_E6_Test_M2_FTPR] 
	-- Add the parameters for the stored procedure here
	@StartDate datetime = null, 
	@EndDate datetime = null,
	@Range varchar(50) = 'Day'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- Insert statements for procedure here
	--SET @EndDate = DATEADD(DAY,1,@EndDate)
	
	
	/* Standard Declares */
	
	DECLARE @_ResultCount table (Result float, Date datetime, Hour int, HourTime time, 
	Quantity float)
	
	
	DECLARE @_DayTotals table (Date datetime, HourTime time, Hour int, DayTotal float)
	
	DECLARE @_FullTimeRange table (DateRange datetime)
	declare @table_StartEnd_Range table (
    startTime datetime
,   endTime datetime
)	
	
	
	IF (@Range = 'Hour')
	BEGIN
	/**** Pre populate a timespan table to start every point at 0 ****/

	INSERT INTO @table_StartEnd_Range
	select @StartDate, @EndDate

	INSERT INTO @_FullTimeRange
	select
		dateadd(hh, n.Number, t.startTime) as Hours
	from @table_StartEnd_Range t
		inner join util_Numbers n
    -- assuming your numbers start at 1 rather than 0
    on n.Number-1 <= datediff(hh, t.startTime, t.endTime)
	
	/**** Get the results for the timespan *****/
	
	INSERT INTO @_ResultCount
	
	SELECT Result, CAST(Date as Date) as Date, DATEPART(HOUR, DATE) as Hour,  CONVERT(time,DATEADD(hour,DATEPART(HOUR, DATE), '00:00:00.000'), 114)      ,
	COUNT(*) as Quantity	
	FROM Euro6.dbo.Header
	where Date BETWEEN @StartDate and @EndDate
	GROUP BY CAST(Date as Date), DATEPART(hour, Date), 
	Result
	order by Date asc, Result asc
	
	/***** Get the day totals  ******/
	
	INSERT INTO @_DayTotals
	SELECT FTR.DateRange, overview.HourTime, overview.Hour, overview.DayTotal FROM @_FullTimeRange FTR
	LEFT JOIN (
	SELECT Date + CAST(CONVERT(time,DATEADD(hour,Hour, '00:00:00.000'), 114) as datetime) as Date, 
	CONVERT(time,DATEADD(hour,Hour, '00:00:00.000'), 114) as HourTime, Hour, SUM(Quantity) as DayTotal FROM @_ResultCount
	group by date  , Hour
	) overview on overview.Date = FTR.DateRange
	
	order by overview.Date desc
	/*  **************************************** Replaced 20130821 JL
	INSERT INTO @_DayTotals
	SELECT overview.Date + CAST(CONVERT(time,DATEADD(hour,overview.Hour, '00:00:00.000'), 114) as datetime), 
	CONVERT(time,DATEADD(hour,overview.Hour, '00:00:00.000'), 114), overview.Hour, SUM(overview.Quantity) as DayTotal FROM @_ResultCount overview
	group by overview.date  , overview.Hour
	order by overview.Date desc
	*/
	SELECT DayTotals.*
	, Result0.Quantity as Result_0
	, ROUND((Result0.Quantity / DayTotals.DayTotal) * 100,1) as R_0_Percentage
	, Result1.Quantity as Result_1
	, ROUND((Result1.Quantity / DayTotals.DayTotal) * 100,1) as R_1_Percentage
	, Result2.Quantity as Result_2
	, ROUND((Result2.Quantity / DayTotals.DayTotal) * 100,1) as R_2_Percentage
	, Result3.Quantity as Result_3
	, ROUND((Result3.Quantity / DayTotals.DayTotal) * 100,1) as R_3_Percentage
	
	FROM @_DayTotals DayTotals
	
	LEFT JOIN (
	SELECT * FROM @_ResultCount where Result = 0
	) Result0 on DATEPART(Day,Result0.Date) = DATEPART(Day,DayTotals.Date) and Result0.Hour = DayTotals.Hour
	
	LEFT JOIN (
	SELECT * FROM @_ResultCount where Result = 1
	) Result1 on DATEPART(Day,Result1.Date) = DATEPART(Day,DayTotals.Date) and Result1.Hour = DayTotals.Hour
	
	LEFT JOIN (
	SELECT * FROM @_ResultCount where Result = 2
	) Result2 on DATEPART(Day,Result2.Date) = DATEPART(Day,DayTotals.Date) and Result2.Hour = DayTotals.Hour
	
	LEFT JOIN (
	SELECT * FROM @_ResultCount where Result = 3
	) Result3 on DATEPART(Day,Result3.Date) = DATEPART(Day,DayTotals.Date) and Result3.Hour = DayTotals.Hour
	
	Where DayTotals.Date BETWEEN @StartDate and @EndDate
	
	order by 
	DayTotals.Date desc
	
	END
	
	IF (@Range = 'Day')
	BEGIN
	
	/**** Pre populate a timespan table to start every point at 0 ****/

	INSERT INTO @table_StartEnd_Range
	select @StartDate, @EndDate

	INSERT INTO @_FullTimeRange
	select
		dateadd(DAY, n.Number, t.startTime) as Hours
	from @table_StartEnd_Range t
		inner join util_Numbers n
    -- assuming your numbers start at 1 rather than 0
    on n.Number-1 <= datediff(DAY, t.startTime, t.endTime)
	
	-- Insert statements for procedure here
	--SET @EndDate = DATEADD(DAY,1,@EndDate)
	
	/**** Get the results for the timespan *****/
	DECLARE @_ResultCount_Day table (Result float, Date datetime, Quantity float)
	
	INSERT INTO @_ResultCount_Day
	
	SELECT Result, CAST(Date as Date) as Date, COUNT(*) as Quantity	
	FROM Euro6.dbo.Header
	where Date BETWEEN @StartDate and @EndDate
	and Batch = 'Virgin'
	GROUP BY CAST(Date as Date), Result
	order by Date asc, Result asc
	
	/***** Get the day totals  ******/
	DECLARE @_DayTotals_Day table (Date datetime, DayTotal float)
	/*
	INSERT INTO @_DayTotals_Day
	
	SELECT overview.Date, SUM(overview.Quantity) as DayTotal FROM @_ResultCount_Day overview
	group by overview.date order by overview.Date desc
	*/
	
	INSERT INTO @_DayTotals_Day
	SELECT FTR.DateRange, overview.DayTotal FROM @_FullTimeRange FTR
	LEFT JOIN (
	SELECT Date, SUM(Quantity) as DayTotal FROM @_ResultCount_Day
	group by date
	) overview on overview.Date = FTR.DateRange
	
	order by FTR.DateRange desc
	
	SELECT DayTotals.*
	, Result0.Quantity as Result_0
	, ROUND((Result0.Quantity / DayTotals.DayTotal) * 100,1) as R_0_Percentage
	, Result1.Quantity as Result_1
	, ROUND((Result1.Quantity / DayTotals.DayTotal) * 100,1) as R_1_Percentage
	, Result2.Quantity as Result_2
	, ROUND((Result2.Quantity / DayTotals.DayTotal) * 100,1) as R_2_Percentage
	, Result3.Quantity as Result_3
	, ROUND((Result3.Quantity / DayTotals.DayTotal) * 100,1) as R_3_Percentage
	
	FROM @_DayTotals_Day DayTotals
	
	LEFT JOIN (
	SELECT * FROM @_ResultCount_Day where Result = 0
	) Result0 on Result0.Date = DayTotals.Date
	
	LEFT JOIN (
	SELECT * FROM @_ResultCount_Day where Result = 1
	) Result1 on Result1.Date = DayTotals.Date
	
	LEFT JOIN (
	SELECT * FROM @_ResultCount_Day where Result = 2
	) Result2 on Result2.Date = DayTotals.Date
	
	LEFT JOIN (
	SELECT * FROM @_ResultCount_Day where Result = 3
	) Result3 on Result3.Date = DayTotals.Date
	
	WHERE DayTotals.Date BETWEEN @StartDate and DATEADD(DAY,-1,@EndDate)
	
	END
	
END

