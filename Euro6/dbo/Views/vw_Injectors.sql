﻿



CREATE VIEW [dbo].[vw_Injectors]
AS

SELECT TOP 10000 NLS.PDate, 
--HeaderLeak.Assembly_Modality_Code, RSPL.Assembly_Modality_Code, NLS.Assembly_Modality_Code,

--TEMP PLACEMENT................
--Test data
InjectorTestHeader.DelphiSerial, InjectorTestHeader.Comment, InjectorTestHeader.Date, InjectorTestHeader.Result,



--Start of Assy line
HeaderLeak.Injector_ID, RSPL.NCV_DataMatrix, NLS.Nozzle_DataMatrix

--Part Data from Station 2
,RSPL.InjectorCode, RSPL.TAG_Code, RSPL.PreLoadValue, RSPL.PreLoadError, RSPL.SetPoint_Load, RSPL.Result_SHIM
,RSPL.SHIM_Measured, RSPL.SHIM_Required

--Part Data from Station 3
,NLS.Resistor_Value

--Test data from a machine testing NCVs.
,Sonplas.Backleak,Sonplas.StaticFlow,Sonplas.NOP,Sonplas.WetSeat


--ZBLN
,ZBLN.Measure_Screwing_Torque_Program_Torque, ZBLN.Measure_Screwing_Angle_Program_Angle, ZBLN.Measure_Screwing_Torque_Program_Angle





	--Start of Assembly
  FROM [Euro6].[dbo].[HeaderLeak] HeaderLeak
  
  --Assembly Station 2
  JOIN (
  SELECT *
  FROM Euro6.dbo.RSPL
  WHERE PDate IN (
  SELECT MAX(PDate) From Euro6.dbo.RSPL Group By NCV_DataMatrix
  )
  ) RSPL on HeaderLeak.Injector_ID = RSPL.Injector_ID
  
  --Assembly Station 3
  LEFT OUTER JOIN (
  SELECT *
  FROM Euro6.dbo.NLS
  WHERE PDate IN (
  SELECT MAX(PDate) From Euro6.dbo.NLS Group By Nozzle_DataMatrix
  )
  ) NLS on HeaderLeak.Injector_ID = NLS.Injector_ID
  
  -- Test data for NCV from somewhere.....
  LEFT OUTER JOIN (
  SELECT * FROM Euro6.dbo.Sonplas
  ) Sonplas on NLS.Nozzle_DataMatrix = Sonplas.SerialNumber

  -- Test Rig data
  --LEFT OUTER
  JOIN (
  SELECT * FROM Euro6.dbo.Header
  WHERE Date IN (
  SELECT MAX(Date) From Euro6.dbo.Header Group By SUBSTRING(AEASerial,0,11)
  )
  ) InjectorTestHeader on SUBSTRING(InjectorTestHeader.AEASerial,0,11) = HeaderLeak.Injector_ID

  -- 20131029 edit - RH, Add in ZBLN Torques
  JOIN (
  SELECT * FROM Euro6.dbo.ZBLNTorque
  where PDate IN (
  SELECT MAX(PDate) From Euro6.dbo.ZBLNTorque Group By Injector_ID)
  ) ZBLN on HeaderLeak.Injector_ID = ZBLN.Injector_ID
  
  --where headerleak.injector_id = '2133225387'
  
  
  order by InjectorTestHeader.Date desc





GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "HeaderLeak"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 122
               Right = 297
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vw_Injectors';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vw_Injectors';

