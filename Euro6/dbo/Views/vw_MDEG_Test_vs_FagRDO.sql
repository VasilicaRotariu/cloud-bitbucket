﻿



CREATE VIEW [dbo].[vw_MDEG_Test_vs_FagRDO]
AS


SELECT 'M2' as Rig, 'Main' as LineType, 
/* Test Rig */
 TestNo, Date, Comment, Batch, Result, A_NCV.Injector_ID as AEASerial, Nozzle_DataMatrix, NCV_DataMatrix,
 /* RDO */
(SELECT TOP 1 Date FROM Euro6.dbo.HPVT where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as RDODate,
(SELECT TOP 1 Flow FROM Euro6.dbo.HPVT where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as RDOFlow,
/* Fag Tilt */
(SELECT TOP 1 Date FROM Euro6.dbo.Zygo where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as NCVDate,
(SELECT TOP 1 Fag FROM Euro6.dbo.Zygo where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as NCVFag,
(SELECT TOP 1 Tilt FROM Euro6.dbo.Zygo where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as NCVTilt,
/* Shim */
(SELECT TOP 1 PDate FROM Euro6.dbo.RSPL R where Injector_ID = SUBSTRING(AEASerial,1,10) and R.PDate < Date order by PDate desc) as ShimDate,
(SELECT TOP 1 SHIM_Measured FROM Euro6.dbo.RSPL R where Injector_ID = SUBSTRING(AEASerial,1,10) and R.PDate < Date order by PDate desc) as ShimMeasured,
(SELECT TOP 1 SHIM_Required FROM Euro6.dbo.RSPL R where Injector_ID = SUBSTRING(AEASerial,1,10) and R.PDate < Date order by PDate desc) as ShimRequired,
/* Capnut Torque */
(SELECT TOP 1 PDate FROM Euro6.dbo.ZBLNTorque where Injector_ID = SUBSTRING(AEASerial,1,10) and PDate < Date order by PDate desc) as TorqueDate,
(SELECT TOP 1 Measure_Screwing_Torque_Program_Angle FROM Euro6.dbo.ZBLNTorque where Injector_ID = SUBSTRING(AEASerial,1,10) and PDate < Date order by PDate desc) as FinalTorque,
/* Nozzle Test */
(SELECT TOP 1 PDate FROM Euro6.dbo.[20474_SonplasNozzleTest_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as NozzleTestDate,
(SELECT TOP 1 Flow_MeasuredFlow FROM Euro6.dbo.[20474_SonplasNozzleTest_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as NozzleTestFlow,
(SELECT TOP 1 NOP_OpeningPressureSingleShot FROM Euro6.dbo.[20474_SonplasNozzleTest_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as NozzleTestNOP,
 /* Eve's Heg - INO flow */
(SELECT TOP 1 PDate FROM Euro6.dbo.[20445_SonplasHEG_Measure_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as INODate,
(SELECT TOP 1 Flow_2 FROM Euro6.dbo.[20445_SonplasHEG_Measure_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as INOFlow
 /* PG on Ian's Heg */
 -- No match grind data so can't do.


 FROM Euro6.dbo.Header T_H         
                
JOIN (SELECT NCV_DataMatrix, Injector_ID FROM Euro6.dbo.RSPL) A_NCV on A_NCV.Injector_ID = SUBSTRING(T_H.AEASerial,1,10)                         
JOIN (SELECT Nozzle_DataMatrix, Injector_ID FROM Euro6.dbo.NLS) A_Noz on A_Noz.Injector_ID = SUBSTRING(T_H.AEASerial,1,10)              
                
WHERE Batch = 'Virgin' and TestPlan = 'J1C00001 C6R' and AEASerial != ''  
           
 
UNION ALL

SELECT 'M6' as Rig, 'Main' as LineType, 
 TestNo, Date, Comment, Batch, Result, A_NCV.Injector_ID as AEASerial, Nozzle_DataMatrix, NCV_DataMatrix,
(SELECT TOP 1 Date FROM Euro6.dbo.HPVT where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as RDODate,
(SELECT TOP 1 Flow FROM Euro6.dbo.HPVT where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as RDOFlow,
(SELECT TOP 1 Date FROM Euro6.dbo.Zygo where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as NCVDate,
(SELECT TOP 1 Fag FROM Euro6.dbo.Zygo where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as NCVFag,
(SELECT TOP 1 Tilt FROM Euro6.dbo.Zygo where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as NCVTilt,
(SELECT TOP 1 PDate FROM Euro6.dbo.RSPL where Injector_ID = SUBSTRING(AEASerial,1,10) and PDate < Date order by PDate desc) as ShimDate,
(SELECT TOP 1 SHIM_Measured FROM Euro6.dbo.RSPL where Injector_ID = SUBSTRING(AEASerial,1,10) and PDate < Date order by PDate desc) as ShimMeasured,
(SELECT TOP 1 SHIM_Required FROM Euro6.dbo.RSPL where Injector_ID = SUBSTRING(AEASerial,1,10) and PDate < Date order by PDate desc) as ShimRequired,
(SELECT TOP 1 PDate FROM Euro6.dbo.ZBLNTorque where Injector_ID = SUBSTRING(AEASerial,1,10) and PDate < Date order by PDate desc) as TorqueDate,
(SELECT TOP 1 Measure_Screwing_Torque_Program_Angle FROM Euro6.dbo.ZBLNTorque where Injector_ID = SUBSTRING(AEASerial,1,10) and PDate < Date order by PDate desc) as FinalTorque,
(SELECT TOP 1 PDate FROM Euro6.dbo.[20474_SonplasNozzleTest_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as NozzleTestDate,
(SELECT TOP 1 Flow_MeasuredFlow FROM Euro6.dbo.[20474_SonplasNozzleTest_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as NozzleTestFlow,
(SELECT TOP 1 NOP_OpeningPressureSingleShot FROM Euro6.dbo.[20474_SonplasNozzleTest_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as NozzleTestNOP,
 /* Eve's Heg - INO flow */
(SELECT TOP 1 PDate FROM Euro6.dbo.[20445_SonplasHEG_Measure_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as INODate,
(SELECT TOP 1 Flow_2 FROM Euro6.dbo.[20445_SonplasHEG_Measure_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as INOFlow
 
 FROM Euro6.dbo.HeaderM6 T_H         
                
JOIN (SELECT NCV_DataMatrix, Injector_ID FROM Euro6.dbo.RSPL) A_NCV on A_NCV.Injector_ID = SUBSTRING(T_H.AEASerial,1,10)                         
JOIN (SELECT Nozzle_DataMatrix, Injector_ID FROM Euro6.dbo.NLS) A_Noz on A_Noz.Injector_ID = SUBSTRING(T_H.AEASerial,1,10)               
                
WHERE Batch = 'Virgin' and TestPlan = 'J1C00001 C6R' and AEASerial != ''   
          

UNION ALL


SELECT 'M2' as Rig, 'Pilot' as LineType, 
 TestNo, Date, Comment, Batch, Result, A_NCV.InjectorSerialNumber as AEASerial, Nozzle_DataMatrix, NCV_DataMatrix,
(SELECT TOP 1 Date FROM Euro6.dbo.HPVT where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as RDODate,
(SELECT TOP 1 Flow FROM Euro6.dbo.HPVT where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as RDOFlow,
(SELECT TOP 1 Date FROM Euro6.dbo.Zygo where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as NCVDate,
(SELECT TOP 1 Fag FROM Euro6.dbo.Zygo where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as NCVFag,
(SELECT TOP 1 Tilt FROM Euro6.dbo.Zygo where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as NCVTilt,
'' as ShimDate, '' as ShimMeasured, '' as ShimRequired, '' as TorqueDate, '' as FinalTorque,
(SELECT TOP 1 PDate FROM Euro6.dbo.[20474_SonplasNozzleTest_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as NozzleTestDate,
(SELECT TOP 1 Flow_MeasuredFlow FROM Euro6.dbo.[20474_SonplasNozzleTest_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as NozzleTestFlow,
(SELECT TOP 1 NOP_OpeningPressureSingleShot FROM Euro6.dbo.[20474_SonplasNozzleTest_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as NozzleTestNOP,
 /* Eve's Heg - INO flow */
(SELECT TOP 1 PDate FROM Euro6.dbo.[20445_SonplasHEG_Measure_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as INODate,
(SELECT TOP 1 Flow_2 FROM Euro6.dbo.[20445_SonplasHEG_Measure_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as INOFlow
 
 FROM Euro6.dbo.Header T_H         
                
JOIN (SELECT NCVSerialNumber as NCV_DataMatrix, InjectorSerialNumber FROM Euro6.dbo.[MDEGLowVolumeLineResults]) A_NCV on A_NCV.InjectorSerialNumber = T_H.AEASerial                       
JOIN (SELECT NozzleSerialNumber as Nozzle_DataMatrix, InjectorSerialNumber FROM Euro6.dbo.[MDEGLowVolumeLineResults]) A_Noz on A_Noz.InjectorSerialNumber = T_H.AEASerial             
                
WHERE Batch = 'Virgin' and TestPlan = 'J1C00001 C6R' and AEASerial != '' 
       

UNION ALL


SELECT 'M6' as Rig, 'Pilot' as LineType, 
 TestNo, Date, Comment, Batch, Result, A_NCV.InjectorSerialNumber as AEASerial, Nozzle_DataMatrix, NCV_DataMatrix,
(SELECT TOP 1 Date FROM Euro6.dbo.HPVT where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as RDODate,
(SELECT TOP 1 Flow FROM Euro6.dbo.HPVT where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as RDOFlow,
(SELECT TOP 1 Date FROM Euro6.dbo.Zygo where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as NCVDate,
(SELECT TOP 1 Fag FROM Euro6.dbo.Zygo where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as NCVFag,
(SELECT TOP 1 Tilt FROM Euro6.dbo.Zygo where SerialNumber = NCV_DataMatrix and PDate < Date order by PDate DESC) as NCVTilt,
'' as ShimDate, '' as ShimMeasured, '' as ShimRequired, '' as TorqueDate, '' as FinalTorque,
(SELECT TOP 1 PDate FROM Euro6.dbo.[20474_SonplasNozzleTest_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as NozzleTestDate,
(SELECT TOP 1 Flow_MeasuredFlow FROM Euro6.dbo.[20474_SonplasNozzleTest_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as NozzleTestFlow,
(SELECT TOP 1 NOP_OpeningPressureSingleShot FROM Euro6.dbo.[20474_SonplasNozzleTest_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as NozzleTestNOP,
 /* Eve's Heg - INO flow */
(SELECT TOP 1 PDate FROM Euro6.dbo.[20445_SonplasHEG_Measure_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as INODate,
(SELECT TOP 1 Flow_2 FROM Euro6.dbo.[20445_SonplasHEG_Measure_Results] where Serial = Nozzle_DataMatrix and Pdate < Date order by PDate desc) as INOFlow
 
 FROM Euro6.dbo.HeaderM6 T_H         
                
JOIN (SELECT NCVSerialNumber as NCV_DataMatrix, InjectorSerialNumber FROM Euro6.dbo.[MDEGLowVolumeLineResults]) A_NCV on SUBSTRING(A_NCV.InjectorSerialNumber,1,10) = T_H.AEASerial                       
JOIN (SELECT NozzleSerialNumber as Nozzle_DataMatrix, InjectorSerialNumber FROM Euro6.dbo.[MDEGLowVolumeLineResults]) A_Noz on SUBSTRING(A_Noz.InjectorSerialNumber,1,10) = T_H.AEASerial             
                
WHERE Batch = 'Virgin' and TestPlan = 'J1C00001 C6R' and AEASerial != ''






GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vw_MDEG_Test_vs_FagRDO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vw_MDEG_Test_vs_FagRDO';

