﻿
CREATE VIEW [dbo].[NozzleHegsMasterData_Vw]
AS
SELECT
		TR.[PDate]
      ,TR.[SerialNum]
      ,TR.[Type]
      ,TR.[FailCode]
	  ,EC.FailCodeText
      ,TR.[PM_Pressure]
      ,TR.[PM_Flow]
      ,TR.[PM_Density]
      ,TR.[PM_UsedDensity]
      ,TR.[PM_Temperature]
      ,TR.[PM_Pressure1BP]
      ,TR.[PM_Pressure2BP]
      ,TR.[PM_FlowBP]
      ,TR.[PM_DensityBP]
      ,TR.[PM_UsedDensityBP]
      ,TR.[PM_TemperatureBP]
      ,TR.[PM_MeasureTime]
      ,TR.[PM_CycleTime]
      ,TR.[PM_NestNr]
      ,TR.[FM_Pressure]
      ,TR.[FM_Flow]
      ,TR.[FM_Density]
      ,TR.[FM_UsedDensity]
      ,TR.[FM_Temperature]
      ,TR.[FM_Pressure1BP]
      ,TR.[FM_Pressure2BP]
      ,TR.[FM_FlowBP]
      ,TR.[FM_DensityBP]
      ,TR.[FM_UsedDensityBP]
      ,TR.[FM_TemperatureBP]
      ,TR.[FM_MeasureTime]
      ,TR.[FM_CycleTime]
      ,TR.[FM_NestNr]
	  ,TR.IDate
	  ,TR.TDate
FROM            
dbo.[NozzleHegsMasterData] AS TR 
LEFT JOIN dbo.[NozzleHegsMasterDataErrorCode] AS EC ON EC.FailCode = TR.FailCode


GO
CREATE TRIGGER [dbo].[NozzleHegsMasterDataTrgger] 
   ON  [dbo].[NozzleHegsMasterData_Vw]
   INSTEAD OF INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE 
			@FailCode INT,
			@FailText VARCHAR(200)
    --- Check for the existence of current eror codes
	--- Using cursor because of issues with BulkCopy which is used by the transfer Service
	DECLARE ErrorCodes CURSOR FOR
	SELECT FailCode, FailCodeText from INSERTED
	OPEN ErrorCodes
	WHILE(1=1)
	BEGIN
		FETCH NEXT FROM ErrorCodes INTO @FailCode, @FailText
		IF @@FETCH_STATUS <> 0
			BREAK
		IF @FailCode IS NOT NULL AND NOT EXISTS(SELECT 1 FROM [dbo].[NozzleHegsMasterDataErrorCode] WHERE FailCode = @FailCode)
		BEGIN
			INSERT INTO [dbo].[NozzleHegsMasterDataErrorCode](FailCode, FailCodeText) VALUES (@FailCode, @FailText)
		END
	END
	CLOSE ErrorCodes
	DEALLOCATE ErrorCodes
	---
	--- Insert new record into the concrete table
	---
	INSERT INTO [dbo].[NozzleHegsMasterData]
	(
		[PDate]
      ,[SerialNum]
      ,[Type]
      ,[FailCode]
      ,[PM_Pressure]
      ,[PM_Flow]
      ,[PM_Density]
      ,[PM_UsedDensity]
      ,[PM_Temperature]
      ,[PM_Pressure1BP]
      ,[PM_Pressure2BP]
      ,[PM_FlowBP]
      ,[PM_DensityBP]
      ,[PM_UsedDensityBP]
      ,[PM_TemperatureBP]
      ,[PM_MeasureTime]
      ,[PM_CycleTime]
      ,[PM_NestNr]
      ,[FM_Pressure]
      ,[FM_Flow]
      ,[FM_Density]
      ,[FM_UsedDensity]
      ,[FM_Temperature]
      ,[FM_Pressure1BP]
      ,[FM_Pressure2BP]
      ,[FM_FlowBP]
      ,[FM_DensityBP]
      ,[FM_UsedDensityBP]
      ,[FM_TemperatureBP]
      ,[FM_MeasureTime]
      ,[FM_CycleTime]
      ,[FM_NestNr]
	)
	SELECT
		[PDate]
      ,[SerialNum]
      ,[Type]
      ,[FailCode]
      ,[PM_Pressure]
      ,[PM_Flow]
      ,[PM_Density]
      ,[PM_UsedDensity]
      ,[PM_Temperature]
      ,[PM_Pressure1BP]
      ,[PM_Pressure2BP]
      ,[PM_FlowBP]
      ,[PM_DensityBP]
      ,[PM_UsedDensityBP]
      ,[PM_TemperatureBP]
      ,[PM_MeasureTime]
      ,[PM_CycleTime]
      ,[PM_NestNr]
      ,[FM_Pressure]
      ,[FM_Flow]
      ,[FM_Density]
      ,[FM_UsedDensity]
      ,[FM_Temperature]
      ,[FM_Pressure1BP]
      ,[FM_Pressure2BP]
      ,[FM_FlowBP]
      ,[FM_DensityBP]
      ,[FM_UsedDensityBP]
      ,[FM_TemperatureBP]
      ,[FM_MeasureTime]
      ,[FM_CycleTime]
      ,[FM_NestNr]
	FROM INSERTED
	---
END

