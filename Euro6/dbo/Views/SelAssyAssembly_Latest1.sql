﻿
CREATE VIEW [dbo].[SelAssyAssembly_Latest1]
AS

SELECT *
FROM  dbo.SelAssyAssembly
WHERE (AssemblyCollarClearance_mm IS NOT NULL) 
AND UTC IN
	(SELECT MAX(UTC) AS Expr1
	FROM dbo.SelAssyAssembly AS SelAssyAssembly_1
	GROUP BY BodyMatrix)

