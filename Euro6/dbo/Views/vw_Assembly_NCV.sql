﻿/*
SELECT     TOP 100 dbo.RSPL.PDate AS Assembly_PDate, dbo.RSPL.Injector_ID AS Assembly_InjectorID, dbo.RSPL.NCV_DataMatrix, 
                      dbo.RSPL.PreLoadValue AS Assembly_PreLoadValue, dbo.RSPL.PreLoadError AS Assembly_PreLoadError, dbo.RSPL.SHIM_Measured AS Assembly_SHIM_Measured,
                       dbo.RSPL.SHIM_Required AS Assembly_SHIM_Required, dbo.RSPL.Spring_Value AS Assembly_Spring_Value, dbo.Zygo.PDate AS Zygo_PDate, 
                      dbo.Zygo.Result AS Zygo_Result, dbo.Zygo.Fag AS Zygo_Fag, dbo.Zygo.Tilt AS Zygo_Tilt, dbo.HPVT.PDate AS HPVT_PDate, dbo.HPVT.Result AS HPVT_Result, 
                      dbo.HPVT.Temp AS HPVT_Temp, dbo.HPVT.Flow AS HPVT_Flow
FROM         dbo.RSPL CROSS JOIN
                      dbo.Zygo CROSS JOIN
                      dbo.HPVT
                      
                      */
CREATE VIEW [dbo].[vw_Assembly_NCV]
AS
SELECT     TOP (100) PERCENT E6_Assembly_NCV.PDate AS Assembly_PDate, E6_Assembly_NCV.Injector_ID AS Assembly_InjectorID, E6_Assembly_NCV.LineID AS BuildLine, 
                      E6_Assembly_NCV.NCV_DataMatrix, E6_Assembly_NCV.PreLoadValue AS Assembly_PreLoadValue, E6_Assembly_NCV.PreLoadError AS Assembly_PreLoadError, 
                      E6_Assembly_NCV.SHIM_Measured AS Assembly_SHIM_Measured, E6_Assembly_NCV.SHIM_Required AS Assembly_SHIM_Required, 
                      E6_Assembly_NCV.Spring_Value AS Assembly_Spring_Value, E6_NCV_Zygo.PDate AS Zygo_PDate, E6_NCV_Zygo.Result AS Zygo_Result, 
                      E6_NCV_Zygo.Fag AS Zygo_Fag, E6_NCV_Zygo.Tilt AS Zygo_Tilt
FROM         dbo.RSPL AS E6_Assembly_NCV LEFT OUTER JOIN
                          (SELECT     PDate, PartNumber, SerialNumber, Result, Fag, Tilt
                            FROM          dbo.Zygo
                            WHERE      (PDate IN
                                                       (SELECT     MAX(PDate) AS Expr1
                                                         FROM          dbo.Zygo AS Zygo_1
                                                         GROUP BY SerialNumber))) AS E6_NCV_Zygo ON E6_NCV_Zygo.SerialNumber = E6_Assembly_NCV.NCV_DataMatrix
WHERE     (E6_Assembly_NCV.LineID <> 1)
ORDER BY Assembly_PDate DESC

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "E6_Assembly_NCV"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 122
               Right = 259
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "E6_NCV_Zygo"
            Begin Extent = 
               Top = 6
               Left = 297
               Bottom = 122
               Right = 456
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vw_Assembly_NCV';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vw_Assembly_NCV';

