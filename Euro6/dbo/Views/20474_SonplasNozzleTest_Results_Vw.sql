﻿
CREATE VIEW [dbo].[20474_SonplasNozzleTest_Results_Vw]
AS
SELECT
TR.Pdate,
TR.InputDate, 
TR.Serial, 
TR.Flow_Pressure, 
TR.Flow_MeasuredFlow, 
TR.NOP_OpeningPressureSingleShot, 
TR.NOP_OpeningPressureChatter, 
TR.NOP_ChatterNumberOfPeaks, 
TR.NOP_ChatterFrequency, 
TR.WetSeat_TipLeak, 
TR.WetSeat_Leak, 
TR.BackLeak_Leak, 
TR.BackLeak_PressureDrop, 
TR.BackLeak_MeasureTime, 
TR.BackLeak_PressureStop, 
TR.FailCode, 
TR.OutputDate,
EC.Description AS FailCode_Description
FROM            
dbo.[20474_SonplasNozzleTest_Results] AS TR 
INNER JOIN dbo.[20474_ErrorCodes] AS EC ON EC.ID = TR.FailCode


GO
CREATE TRIGGER [dbo].[20474_ErrorCodesUpdate] 
   ON  [dbo].[20474_SonplasNozzleTest_Results_Vw]
   INSTEAD OF INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE 
			@FailCode INT,
			@FailText VARCHAR(200)
    --- Check for the existence of current eror codes
	--- Using cursor because of issues with BulkCopy which is used by the transfer Service
	DECLARE ErrorCodes CURSOR FOR
	SELECT FailCode, FailCode_Description from INSERTED
	OPEN ErrorCodes
	WHILE(1=1)
	BEGIN
		FETCH NEXT FROM ErrorCodes INTO @FailCode, @FailText
		IF @@FETCH_STATUS <> 0
			BREAK
		IF @FailCode IS NOT NULL AND NOT EXISTS(SELECT 1 FROM [dbo].[NozzleHegsMasterDataErrorCode] WHERE FailCode = @FailCode)
		BEGIN
			INSERT INTO [dbo].[NozzleHegsMasterDataErrorCode](FailCode, FailCodeText) VALUES (@FailCode, @FailText)
		END
	END
	CLOSE ErrorCodes
	DEALLOCATE ErrorCodes
	---
	--- Insert new record into the concrete table
	---
	INSERT INTO [dbo].[20474_SonplasNozzleTest_Results]
	(
		Pdate,
		InputDate,
		OutputDate,
		Serial, 
		Flow_Pressure,
		Flow_MeasuredFlow, NOP_OpeningPressureSingleShot, NOP_OpeningPressureChatter, NOP_ChatterNumberOfPeaks, 
		NOP_ChatterFrequency, WetSeat_TipLeak, WetSeat_Leak, BackLeak_Leak, BackLeak_PressureDrop, BackLeak_MeasureTime, BackLeak_PressureStop,
		FailCode
	)
	SELECT
		Pdate,
		InputDate,
		OutputDate,
		Serial, 
		Flow_Pressure,
		Flow_MeasuredFlow, NOP_OpeningPressureSingleShot, NOP_OpeningPressureChatter, NOP_ChatterNumberOfPeaks, 
		NOP_ChatterFrequency, WetSeat_TipLeak, WetSeat_Leak, BackLeak_Leak, BackLeak_PressureDrop, BackLeak_MeasureTime, BackLeak_PressureStop,
		FailCode
	FROM INSERTED
	---
END

