﻿
CREATE VIEW [dbo].[SA_GetSPCFeatures]
AS
SELECT MCT.PlantNumber, MCT.ComponentType, MCT.SPCPartNumberAlias, MCT.SPCPartDescriptionAlias, MT.MasterType, MMFT.Feature, MMFT.SPCFeatureDescriptionAlias, MMF.CalibratedValue
FROM SelAssy_MasterComponentTypes MCT
JOIN SelAssy_Masters M ON M.MasterComponentType_FID = MCT.MasterComponentType_UID
JOIN SelAssy_MasterTypes MT ON M.MasterLimitType_FID = MT.MasterLimitType_UID
JOIN SelAssy_MasterMeasurementFeatures MMF ON MMF.Master_FID = M.Master_UID
JOIN SelAssy_MasterMeasurementFeatureTypes MMFT ON MMFT.SelAssy_MasterMeasurementFeatureType_UID = MMF.SelAssy_MasterMeasurementFeatureType_FID
