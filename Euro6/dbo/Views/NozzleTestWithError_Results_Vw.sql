﻿


CREATE VIEW [dbo].[NozzleTestWithError_Results_Vw]
AS
SELECT
TR.Pdate,
TR.InputDate, 
TR.Serial, 
TR.Flow_Pressure, 
TR.Flow_MeasuredFlow, 
TR.NOP_OpeningPressureSingleShot, 
TR.NOP_OpeningPressureChatter, 
TR.NOP_ChatterNumberOfPeaks, 
TR.NOP_ChatterFrequency, 
TR.WetSeat_TipLeak, 
TR.WetSeat_Leak, 
TR.BackLeak_Leak, 
TR.BackLeak_PressureDrop, 
TR.BackLeak_MeasureTime, 
TR.BackLeak_PressureStop, 
TR.FailCode,
EC.Description AS FailCode_Description
FROM            
dbo.[NozzleTestWithError_Results] AS TR 
LEFT JOIN dbo.[NozzleTest_ErrorCodes] AS EC ON EC.ID = TR.FailCode




GO


---
--- Trigger on NozzleTestWithError_Results to copy error code description to ErroCodes table(no point having repreat descriptions in  20474_SonplasNozzleTest_Results table)
---
CREATE TRIGGER [dbo].[NozzleTestWithErrors_CodesUpdate] 
   ON  [dbo].[NozzleTestWithError_Results_Vw]
   INSTEAD OF INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE 
			@FailCode INT

	SELECT @FailCode = FailCode
	FROM INSERTED
    --- Check for the existence of current eror codes
	---
	IF NOT EXISTS(SELECT 1 FROM [NozzleTest_ErrorCodes] where ID = @FailCode)
	BEGIN
		INSERT INTO [dbo].[NozzleTest_ErrorCodes](ID, [Description]) 
			SELECT FailCode, FailCode_Description from INSERTED
	END
	---
	--- Insert new record into the concrete table
	---
	INSERT INTO [dbo].[NozzleTestWithError_Results]
	(
		Pdate,
		InputDate,
		Serial, 
		Flow_Pressure,
		Flow_MeasuredFlow, NOP_OpeningPressureSingleShot, NOP_OpeningPressureChatter, NOP_ChatterNumberOfPeaks, 
		NOP_ChatterFrequency, WetSeat_TipLeak, WetSeat_Leak, BackLeak_Leak, BackLeak_PressureDrop, BackLeak_MeasureTime, BackLeak_PressureStop,
		FailCode
	)
	SELECT
		Pdate,
		InputDate,
		Serial, 
		Flow_Pressure,
		Flow_MeasuredFlow, NOP_OpeningPressureSingleShot, NOP_OpeningPressureChatter, NOP_ChatterNumberOfPeaks, 
		NOP_ChatterFrequency, WetSeat_TipLeak, WetSeat_Leak, BackLeak_Leak, BackLeak_PressureDrop, BackLeak_MeasureTime, BackLeak_PressureStop,
		FailCode
	FROM INSERTED
	---
END
