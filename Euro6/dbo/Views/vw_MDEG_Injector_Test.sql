﻿CREATE VIEW [dbo].[vw_MDEG_Injector_Test]
AS
SELECT     HD.Date AS PDate, TD.TestNo, HD.ISN AS InjectorSerialNumber, HD.AEASerial, HD.Result, HD.TestPlan, HD.Batch, HD.Comment, HD.TrimCode, HD.Trim1, HD.MDP1, 
                      HD.BLStatic, HD.BLDynamic, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1400) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_Q0_2400_1400, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1600) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_Q1_1800_1600, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1800) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_Q2_1400_1800, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 2000) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_Q3_800_2000, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 245) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T1_2400_245, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 250) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T1_2400_250, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 265) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T2_2400_265, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 300) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T3_2400_300, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 360) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T4_2400_360, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 370) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T4_2400_370, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1000) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T5_2400_1000, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1050) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T5_2400_1050, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1600) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T6_2400_1600, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1800) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T6_2400_1800, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 2000) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_LL_2400_2000, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 255) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T1_1800_255, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 260) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T1_1800_260, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 400) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T2_1800_400, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 420) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T2_1800_420, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1000) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T3_1800_1000, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1050) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T3_1800_1050, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1150) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T4_1800_1150, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1200) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T4_1800_1200, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 265) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T1_1400_265, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 360) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T2_1400_360, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 780) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T3_1400_780, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1000) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T4_1400_1000, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1050) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T4_1400_1050, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 290) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T1_800_290, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 300) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T1_800_300, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 310) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T1_800_310, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 320) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T1_800_320, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 330) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T1_800_330, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 420) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T2_800_420, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 400) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T2_800_400, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 640) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T3_800_640, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1000) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T4_800_1000, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1050) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T4_800_1050, MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 600) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T1_350_600, MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1000) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T2_350_1000, MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1050) THEN TD.Delivery ELSE NULL END) 
                      AS Delivery_Untrimmed_RAW_T2_350_1050, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1400) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_Q0_2400_1400, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1600) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_Q1_1800_1600, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1800) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_Q2_1400_1800, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 2000) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_Q3_800_2000, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 245) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T1_2400_245, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 250) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T1_2400_250, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 265) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T2_2400_265, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 300) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T3_2400_300, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 360) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T4_2400_360, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 370) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T4_2400_370, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1000) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T5_2400_1000, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1050) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T5_2400_1050, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1600) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T6_2400_1600, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1800) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T6_2400_1800, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 2000) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_LL_2400_2000, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 255) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T1_1800_255, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 260) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T1_1800_260, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 400) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T2_1800_400, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 420) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T2_1800_420, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1000) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T3_1800_1000, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1050) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T3_1800_1050, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1150) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T4_1800_1150, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1200) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T4_1800_1200, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 265) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T1_1400_265, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 360) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T2_1400_360, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 780) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T3_1400_780, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1000) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T4_1400_1000, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1050) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T4_1400_1050, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 290) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T1_800_290, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 300) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T1_800_300, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 310) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T1_800_310, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 320) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T1_800_320, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 330) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T1_800_330, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 420) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T2_800_420, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 400) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T2_800_400, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 640) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T3_800_640, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1000) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T4_800_1000, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1050) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T4_800_1050, MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 600) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T1_350_600, MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1000) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T2_350_1000, MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1050) THEN TD.Error ELSE NULL END) 
                      AS Delivery_Trimmed_Error_T2_350_1050, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1400) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_Q0_2400_1400, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1600) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_Q1_1800_1600, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1800) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_Q2_1400_1800, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 2000) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_Q3_800_2000, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 245) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T1_2400_245, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 250) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T1_2400_250, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 265) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T2_2400_265, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 300) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T3_2400_300, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 360) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T4_2400_360, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 370) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T4_2400_370, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1000) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T5_2400_1000, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1050) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T5_2400_1050, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1600) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T6_2400_1600, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1800) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T6_2400_1800, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 2000) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_LL_2400_2000, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 255) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T1_1800_255, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 260) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T1_1800_260, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 400) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T2_1800_400, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 420) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T2_1800_420, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1000) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T3_1800_1000, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1050) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T3_1800_1050, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1150) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T4_1800_1150, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1200) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T4_1800_1200, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 265) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T1_1400_265, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 360) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T2_1400_360, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 780) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T3_1400_780, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1000) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T4_1400_1000, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1050) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T4_1400_1050, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 290) THEN TD.Del3SDs ELSE NULL END) 
                      AS Delivery_3SDs_T1_800_290, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 300) THEN TD.Del3SDs ELSE NULL END) AS Delivery_3SDs_T1_800_300, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 310) THEN TD.Del3SDs ELSE NULL END) AS Delivery_3SDs_T1_800_310, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 320) THEN TD.Del3SDs ELSE NULL END) AS Delivery_3SDs_T1_800_320, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 330) THEN TD.Del3SDs ELSE NULL END) AS Delivery_3SDs_T1_800_330, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 420) THEN TD.Del3SDs ELSE NULL END) AS Delivery_3SDs_T2_800_420, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 400) THEN TD.Del3SDs ELSE NULL END) AS Delivery_3SDs_T2_800_400, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 640) THEN TD.Del3SDs ELSE NULL END) AS Delivery_3SDs_T3_800_640, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1000) THEN TD.Del3SDs ELSE NULL END) AS Delivery_3SDs_T4_800_1000, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1050) THEN TD.Del3SDs ELSE NULL END) AS Delivery_3SDs_T4_800_1050, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 600) THEN TD.Del3SDs ELSE NULL END) AS Delivery_3SDs_T1_350_600, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1000) THEN TD.Del3SDs ELSE NULL END) AS Delivery_3SDs_T2_350_1000, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1050) THEN TD.Del3SDs ELSE NULL END) AS Delivery_3SDs_T2_350_1050, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1400) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_Q0_2400_1400, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1600) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_Q1_1800_1600, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1800) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_Q2_1400_1800, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 2000) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_Q3_800_2000, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 245) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T1_2400_245, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 250) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T1_2400_250, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 265) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T2_2400_265, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 300) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T3_2400_300, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 360) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T4_2400_360, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 370) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T4_2400_370, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1000) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T5_2400_1000, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1050) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T5_2400_1050, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1600) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T6_2400_1600, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1800) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T6_2400_1800, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 2000) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_LL_2400_2000, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 255) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T1_1800_255, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 260) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T1_1800_260, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 400) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T2_1800_400, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 420) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T2_1800_420, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1000) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T3_1800_1000, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1050) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T3_1800_1050, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1150) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T4_1800_1150, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1200) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T4_1800_1200, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 265) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T1_1400_265, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 360) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T2_1400_360, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 780) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T3_1400_780, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1000) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T4_1400_1000, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1050) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T4_1400_1050, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 290) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T1_800_290, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 300) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T1_800_300, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 310) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T1_800_310, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 320) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T1_800_320, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 330) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T1_800_330, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 420) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T2_800_420, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 400) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T2_800_400, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 640) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T3_800_640, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1000) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T4_800_1000, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1050) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T4_800_1050, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 600) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T1_350_600, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1000) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T2_350_1000, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1050) THEN TD.RailPressure ELSE NULL END) AS Rail_Pressure_T2_350_1050, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1400) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_Q0_2400_1400, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1600) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_Q1_1800_1600, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1800) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_Q2_1400_1800, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 2000) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_Q3_800_2000, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 245) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T1_2400_245, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 250) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T1_2400_250, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 265) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T2_2400_265, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 300) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T3_2400_300, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 360) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T4_2400_360, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 370) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T4_2400_370, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1000) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T5_2400_1000, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1050) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T5_2400_1050, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1600) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T6_2400_1600, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1800) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T6_2400_1800, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 2000) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_LL_2400_2000, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 255) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T1_1800_255, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 260) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T1_1800_260, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 400) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T2_1800_400, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 420) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T2_1800_420, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1000) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T3_1800_1000, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1050) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T3_1800_1050, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1150) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T4_1800_1150, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1200) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T4_1800_1200, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 265) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T1_1400_265, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 360) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T2_1400_360, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 780) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T3_1400_780, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1000) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T4_1400_1000, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1050) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T4_1400_1050, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 290) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T1_800_290, MAX(CASE WHEN (TD.SetRP = 800 AND 
                      TD.NCVDuration = 300) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T1_800_300, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 310) 
                      THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T1_800_310, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 320) THEN TD.BLFlow ELSE NULL END) 
                      AS BL_Flow_T1_800_320, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 330) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T1_800_330, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 420) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T2_800_420, MAX(CASE WHEN (TD.SetRP = 800 AND 
                      TD.NCVDuration = 400) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T2_800_400, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 640) 
                      THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T3_800_640, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1000) THEN TD.BLFlow ELSE NULL END) 
                      AS BL_Flow_T4_800_1000, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1050) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T4_800_1050, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 600) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T1_350_600, MAX(CASE WHEN (TD.SetRP = 350 AND 
                      TD.NCVDuration = 1000) THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T2_350_1000, MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1050) 
                      THEN TD.BLFlow ELSE NULL END) AS BL_Flow_T2_350_1050, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1400) THEN TD.BLPressure ELSE NULL 
                      END) AS BL_Pressure_Q0_2400_1400, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1600) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_Q1_1800_1600, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1800) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_Q2_1400_1800, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 2000) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_Q3_800_2000, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 245) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T1_2400_245, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 250) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T1_2400_250, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 265) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T2_2400_265, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 300) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T3_2400_300, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 360) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T4_2400_360, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 370) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T4_2400_370, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1000) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T5_2400_1000, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1050) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T5_2400_1050, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1600) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T6_2400_1600, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1800) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T6_2400_1800, MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 2000) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_LL_2400_2000, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 255) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T1_1800_255, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 260) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T1_1800_260, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 400) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T2_1800_400, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 420) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T2_1800_420, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1000) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T3_1800_1000, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1050) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T3_1800_1050, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1150) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T4_1800_1150, MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1200) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T4_1800_1200, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 265) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T1_1400_265, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 360) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T2_1400_360, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 780) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T3_1400_780, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1000) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T4_1400_1000, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1050) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T4_1400_1050, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 290) THEN TD.BLPressure ELSE NULL END) 
                      AS BL_Pressure_T1_800_290, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 300) THEN TD.BLPressure ELSE NULL END) AS BL_Pressure_T1_800_300, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 310) THEN TD.BLPressure ELSE NULL END) AS BL_Pressure_T1_800_310, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 320) THEN TD.BLPressure ELSE NULL END) AS BL_Pressure_T1_800_320, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 330) THEN TD.BLPressure ELSE NULL END) AS BL_Pressure_T1_800_330, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 420) THEN TD.BLPressure ELSE NULL END) AS BL_Pressure_T2_800_420, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 400) THEN TD.BLPressure ELSE NULL END) AS BL_Pressure_T2_800_400, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 640) THEN TD.BLPressure ELSE NULL END) AS BL_Pressure_T3_800_640, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1000) THEN TD.BLPressure ELSE NULL END) AS BL_Pressure_T4_800_1000, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1050) THEN TD.BLPressure ELSE NULL END) AS BL_Pressure_T4_800_1050, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 600) THEN TD.BLPressure ELSE NULL END) AS BL_Pressure_T1_350_600, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1000) THEN TD.BLPressure ELSE NULL END) AS BL_Pressure_T2_350_1000, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1050) THEN TD.BLPressure ELSE NULL END) AS BL_Pressure_T2_350_1050, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1400) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_Q0_2400_1400, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1600) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_Q1_1800_1600, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1800) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_Q2_1400_1800, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 2000) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_Q3_800_2000, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 245) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T1_2400_245, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 250) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T1_2400_250, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 265) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T2_2400_265, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 300) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T3_2400_300, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 360) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T4_2400_360, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 370) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T4_2400_370, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1000) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T5_2400_1000, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1050) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T5_2400_1050, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1600) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T6_2400_1600, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1800) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T6_2400_1800, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 2000) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_LL_2400_2000, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 255) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T1_1800_255, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 260) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T1_1800_260, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 400) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T2_1800_400, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 420) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T2_1800_420, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1000) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T3_1800_1000, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1050) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T3_1800_1050, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1150) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T4_1800_1150, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1200) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T4_1800_1200, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 265) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T1_1400_265, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 360) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T2_1400_360, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 780) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T3_1400_780, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1000) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T4_1400_1000, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1050) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T4_1400_1050, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 290) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T1_800_290, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 300) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T1_800_300, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 310) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T1_800_310, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 320) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T1_800_320, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 330) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T1_800_330, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 420) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T2_800_420, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 400) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T2_800_400, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 640) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T3_800_640, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1000) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T4_800_1000, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1050) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T4_800_1050, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 600) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T1_350_600, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1000) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T2_350_1000, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1050) THEN TD.InjTemp ELSE NULL END) AS Injector_Temp_T2_350_1050, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1400) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_Q0_2400_1400, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1600) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_Q1_1800_1600, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1800) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_Q2_1400_1800, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 2000) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_Q3_800_2000, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 245) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T1_2400_245, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 250) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T1_2400_250, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 265) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T2_2400_265, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 300) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T3_2400_300, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 360) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T4_2400_360, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 370) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T4_2400_370, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1000) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T5_2400_1000, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1050) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T5_2400_1050, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1600) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T6_2400_1600, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1800) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T6_2400_1800, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 2000) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_LL_2400_2000, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 255) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T1_1800_255, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 260) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T1_1800_260, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 400) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T2_1800_400, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 420) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T2_1800_420, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1000) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T3_1800_1000, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1050) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T3_1800_1050, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1150) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T4_1800_1150, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1200) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T4_1800_1200, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 265) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T1_1400_265, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 360) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T2_1400_360, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 780) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T3_1400_780, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1000) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T4_1400_1000, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1050) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T4_1400_1050, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 290) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T1_800_290, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 300) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T1_800_300, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 310) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T1_800_310, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 320) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T1_800_320, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 330) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T1_800_330, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 420) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T2_800_420, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 400) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T2_800_400, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 640) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T3_800_640, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1000) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T4_800_1000, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1050) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T4_800_1050, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 600) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T1_350_600, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1000) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T2_350_1000, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1050) THEN TD.InjSquirt1 ELSE NULL END) AS Shot1_Delivery_T2_350_1050, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1400) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_Q0_2400_1400, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1600) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_Q1_1800_1600, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1800) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_Q2_1400_1800, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 2000) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_Q3_800_2000, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 245) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T1_2400_245, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 250) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T1_2400_250, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 265) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T2_2400_265, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 300) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T3_2400_300, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 360) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T4_2400_360, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 370) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T4_2400_370, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1000) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T5_2400_1000, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1050) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T5_2400_1050, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1600) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T6_2400_1600, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1800) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T6_2400_1800, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 2000) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_LL_2400_2000, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 255) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T1_1800_255, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 260) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T1_1800_260, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 400) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T2_1800_400, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 420) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T2_1800_420, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1000) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T3_1800_1000, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1050) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T3_1800_1050, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1150) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T4_1800_1150, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1200) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T4_1800_1200, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 265) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T1_1400_265, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 360) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T2_1400_360, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 780) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T3_1400_780, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1000) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T4_1400_1000, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1050) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T4_1400_1050, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 290) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T1_800_290, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 300) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T1_800_300, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 310) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T1_800_310, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 320) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T1_800_320, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 330) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T1_800_330, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 420) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T2_800_420, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 400) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T2_800_400, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 640) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T3_800_640, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1000) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T4_800_1000, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1050) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T4_800_1050, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 600) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T1_350_600, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1000) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T2_350_1000, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1050) THEN TD.InjSquirt2 ELSE NULL END) AS Shot2_Delivery_T2_350_1050, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1400) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_Q0_2400_1400, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1600) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_Q1_1800_1600, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1800) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_Q2_1400_1800, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 2000) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_Q3_800_2000, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 245) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T1_2400_245, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 250) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T1_2400_250, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 265) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T2_2400_265, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 300) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T3_2400_300, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 360) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T4_2400_360, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 370) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T4_2400_370, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1000) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T5_2400_1000, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1050) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T5_2400_1050, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1600) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T6_2400_1600, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1800) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T6_2400_1800, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 2000) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_LL_2400_2000, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 255) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T1_1800_255, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 260) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T1_1800_260, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 400) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T2_1800_400, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 420) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T2_1800_420, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1000) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T3_1800_1000, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1050) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T3_1800_1050, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1150) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T4_1800_1150, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1200) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T4_1800_1200, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 265) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T1_1400_265, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 360) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T2_1400_360, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 780) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T3_1400_780, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1000) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T4_1400_1000, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1050) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T4_1400_1050, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 290) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T1_800_290, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 300) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T1_800_300, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 310) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T1_800_310, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 320) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T1_800_320, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 330) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T1_800_330, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 420) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T2_800_420, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 400) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T2_800_400, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 640) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T3_800_640, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1000) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T4_800_1000, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1050) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T4_800_1050, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 600) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T1_350_600, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1000) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T2_350_1000, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1050) THEN TD.InjSquirt3 ELSE NULL END) AS Shot3_Delivery_T2_350_1050, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1400) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_Q0_2400_1400, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1600) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_Q1_1800_1600, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1800) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_Q2_1400_1800, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 2000) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_Q3_800_2000, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 245) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T1_2400_245, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 250) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T1_2400_250, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 265) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T2_2400_265, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 300) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T3_2400_300, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 360) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T4_2400_360, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 370) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T4_2400_370, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1000) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T5_2400_1000, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1050) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T5_2400_1050, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1600) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T6_2400_1600, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1800) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T6_2400_1800, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 2000) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_LL_2400_2000, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 255) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T1_1800_255, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 260) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T1_1800_260, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 400) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T2_1800_400, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 420) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T2_1800_420, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1000) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T3_1800_1000, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1050) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T3_1800_1050, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1150) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T4_1800_1150, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1200) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T4_1800_1200, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 265) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T1_1400_265, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 360) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T2_1400_360, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 780) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T3_1400_780, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1000) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T4_1400_1000, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1050) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T4_1400_1050, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 290) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T1_800_290, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 300) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T1_800_300, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 310) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T1_800_310, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 320) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T1_800_320, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 330) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T1_800_330, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 420) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T2_800_420, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 400) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T2_800_400, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 640) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T3_800_640, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1000) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T4_800_1000, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1050) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T4_800_1050, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 600) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T1_350_600, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1000) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T2_350_1000, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1050) THEN TD.InjSquirt4 ELSE NULL END) AS Shot4_Delivery_T2_350_1050, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1400) THEN TD.InjSquirt5 ELSE NULL END) AS Expr1, MAX(CASE WHEN (TD.SetRP = 1800 AND 
                      TD.NCVDuration = 1600) THEN TD.InjSquirt5 ELSE NULL END) AS Expr2, MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1800) 
                      THEN TD.InjSquirt5 ELSE NULL END) AS Expr3, MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 2000) THEN TD.InjSquirt5 ELSE NULL END) AS Expr4, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 245) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T1_2400_245, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 250) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T1_2400_250, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 265) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T2_2400_265, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 300) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T3_2400_300, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 360) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T4_2400_360, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 370) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T4_2400_370, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1000) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T5_2400_1000, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1050) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T5_2400_1050, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1600) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T6_2400_1600, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 1800) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T6_2400_1800, 
                      MAX(CASE WHEN (TD.SetRP = 2400 AND TD.NCVDuration = 2000) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_LL_2400_2000, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 255) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T1_1800_255, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 260) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T1_1800_260, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 400) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T2_1800_400, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 420) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T2_1800_420, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1000) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T3_1800_1000, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1050) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T3_1800_1050, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1150) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T4_1800_1150, 
                      MAX(CASE WHEN (TD.SetRP = 1800 AND TD.NCVDuration = 1200) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T4_1800_1200, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 265) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T1_1400_265, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 360) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T2_1400_360, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 780) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T3_1400_780, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1000) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T4_1400_1000, 
                      MAX(CASE WHEN (TD.SetRP = 1400 AND TD.NCVDuration = 1050) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T4_1400_1050, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 290) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T1_800_290, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 300) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T1_800_300, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 310) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T1_800_310, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 320) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T1_800_320, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 330) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T1_800_330, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 420) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T2_800_420, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 400) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T2_800_400, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 640) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T3_800_640, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1000) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T4_800_1000, 
                      MAX(CASE WHEN (TD.SetRP = 800 AND TD.NCVDuration = 1050) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T4_800_1050, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 600) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T1_350_600, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1000) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T2_350_1000, 
                      MAX(CASE WHEN (TD.SetRP = 350 AND TD.NCVDuration = 1050) THEN TD.InjSquirt5 ELSE NULL END) AS Shot5_Delivery_T2_350_1050
FROM         dbo.TestData AS TD INNER JOIN
                      dbo.Header AS HD ON TD.TestNo = HD.TestNo
WHERE     (HD.TestPlan = 'J1C00001 C6R')
GROUP BY HD.Date, TD.TestNo, HD.ISN, HD.AEASerial, HD.Result, HD.TestPlan, HD.Batch, HD.Comment, HD.TrimCode, HD.Trim1, HD.MDP1, HD.BLStatic, HD.BLDynamic

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TD"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 122
               Right = 234
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "HD"
            Begin Extent = 
               Top = 6
               Left = 272
               Bottom = 122
               Right = 447
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vw_MDEG_Injector_Test';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vw_MDEG_Injector_Test';

