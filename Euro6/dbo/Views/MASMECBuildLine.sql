﻿CREATE VIEW [dbo].[MASMECBuildLine]
AS
SELECT        dbo.MASMEC_SelAsmb_OP10_Results.Pdate, dbo.MASMEC_SelAsmb_OP10_Results.Serial, '20741' AS Asset, 
                         dbo.MASMEC_SelAsmb_OP20C_Results.Result AS FinalResult, 
                         dbo.MASMEC_SelAsmb_OP10_Results.HeaderScrew_Screw1_Torque_MeasureValue AS OP10_HeaderScrew1TorqueValue, 
                         dbo.MASMEC_SelAsmb_OP10_Results.HeaderScrew_Screw2_Torque_MeasureValue AS OP10_HeaderScrew2TorqueValue, 
                         dbo.MASMEC_SelAsmb_OP10_Results.HeaderScrew_Screw1_Angle_MeasureValue AS OP10_HeaderScrew1AngleValue, 
                         dbo.MASMEC_SelAsmb_OP10_Results.HeaderScrew_Screw2_Angle_MeasureValue AS OP10_HeaderScrew2AngleValue, 
                         dbo.MASMEC_SelAsmb_OP10_Results.Leakage_MeasureValue, dbo.MASMEC_SelAsmb_OP20A_Results.PreLoad_MeasureValue, 
                         dbo.MASMEC_SelAsmb_OP20A_Results.Shim_Required, dbo.MASMEC_SelAsmb_OP20B_Results.NCV_SerialNumber, 
                         dbo.MASMEC_SelAsmb_OP20B_Results.Nozzle_SerialNumber, dbo.MASMEC_SelAsmb_OP30A_Results.Capnut_ScrewTorque_Torque_MeasureValue, 
                         dbo.MASMEC_SelAsmb_OP30A_Results.Capnut_ScrewTorque_Angle_MeasureValue, 
                         dbo.MASMEC_SelAsmb_OP30A_Results.Capnut_ScrewAngle_Torque_MeasureValue, 
                         dbo.MASMEC_SelAsmb_OP30A_Results.Capnut_ScrewAngle_Angle_MeasureValue, 
                         dbo.MASMEC_SelAsmb_OP30A_Results.Orientation_Angle_After_Screw_MeasureValue, dbo.MASMEC_SelAsmb_OP30C_Results.Leakage_MeasureValue AS Expr1, 
                         dbo.MASMEC_SelAsmb_OP30C_Results.Resistance_MeasureValue, dbo.MASMEC_SelAsmb_OP10_Results.LOP
FROM            dbo.MASMEC_SelAsmb_OP10_Results LEFT OUTER JOIN
                         dbo.MASMEC_SelAsmb_OP20A_Results ON dbo.MASMEC_SelAsmb_OP10_Results.Serial = dbo.MASMEC_SelAsmb_OP20A_Results.Serial LEFT OUTER JOIN
                         dbo.MASMEC_SelAsmb_OP20B_Results ON dbo.MASMEC_SelAsmb_OP10_Results.Serial = dbo.MASMEC_SelAsmb_OP20B_Results.Serial LEFT OUTER JOIN
                         dbo.MASMEC_SelAsmb_OP20C_Results ON dbo.MASMEC_SelAsmb_OP10_Results.Serial = dbo.MASMEC_SelAsmb_OP20C_Results.Serial LEFT OUTER JOIN
                         dbo.MASMEC_SelAsmb_OP30A_Results ON dbo.MASMEC_SelAsmb_OP10_Results.Serial = dbo.MASMEC_SelAsmb_OP30A_Results.Serial LEFT OUTER JOIN
                         dbo.MASMEC_SelAsmb_OP30B_Results ON dbo.MASMEC_SelAsmb_OP10_Results.Serial = dbo.MASMEC_SelAsmb_OP30B_Results.Serial LEFT OUTER JOIN
                         dbo.MASMEC_SelAsmb_OP30C_Results ON dbo.MASMEC_SelAsmb_OP10_Results.Serial = dbo.MASMEC_SelAsmb_OP30C_Results.Serial

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[37] 4[18] 2[24] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MASMEC_SelAsmb_OP10_Results"
            Begin Extent = 
               Top = 102
               Left = 38
               Bottom = 251
               Right = 344
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MASMEC_SelAsmb_OP20A_Results"
            Begin Extent = 
               Top = 0
               Left = 537
               Bottom = 157
               Right = 717
            End
            DisplayFlags = 280
            TopColumn = 11
         End
         Begin Table = "MASMEC_SelAsmb_OP20B_Results"
            Begin Extent = 
               Top = 2
               Left = 725
               Bottom = 133
               Right = 1021
            End
            DisplayFlags = 280
            TopColumn = 5
         End
         Begin Table = "MASMEC_SelAsmb_OP20C_Results"
            Begin Extent = 
               Top = 53
               Left = 969
               Bottom = 224
               Right = 1234
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MASMEC_SelAsmb_OP30A_Results"
            Begin Extent = 
               Top = 438
               Left = 831
               Bottom = 612
               Right = 1333
            End
            DisplayFlags = 280
            TopColumn = 33
         End
         Begin Table = "MASMEC_SelAsmb_OP30B_Results"
            Begin Extent = 
               Top = 109
               Left = 611
               Bottom = 238
               Right = 854
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MASMEC_SelAsmb_OP30C_Results"
            Begin Extent = 
      ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'MASMECBuildLine';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'         Top = 386
               Left = 364
               Bottom = 603
               Right = 1028
            End
            DisplayFlags = 280
            TopColumn = 23
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 14
         Width = 284
         Width = 2220
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'MASMECBuildLine';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'MASMECBuildLine';

