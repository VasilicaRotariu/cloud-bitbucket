﻿CREATE VIEW [dbo].[NozzleHegs]
AS
    SELECT  [nhd].[TDate],
            [nhd].[PDate],
            [nhd].[PlantNumber],
            [nhd].[SerialNum],
            [nhd].[Product],
            [nhd].[Type],
            [nhd].[FailCode],
            [nhfc].[FailCodeText],
            [nhd].[FromWhichPosalux],
            [nhd].[FromWhichPosaluxHead],
            [nhd].[PM_Pressure],
            [nhd].[PM_Flow],
            [nhd].[PM_Density],
            [nhd].[PM_UsedDensity],
            [nhd].[PM_Temperature],
            [nhd].[PM_Pressure1BP],
            [nhd].[PM_Pressure2BP],
            [nhd].[PM_FlowBP],
            [nhd].[PM_DeltaQ],
            [nhd].[PM_DensityBP],
            [nhd].[PM_UsedDensityBP],
            [nhd].[PM_TemperatureBP],
            [nhd].[PM_MeasureTime],
            [nhd].[PM_CycleTime],
            [nhd].[PM_NestNr],
            [nhd].[G_Pressure],
            [nhd].[G_FlowStart],
            [nhd].[G_FlowStop],
            [nhd].[G_Temperature],
            [nhd].[G_Density],
            [nhd].[G_UsedDensity],
            [nhd].[G_GrindTime],
            [nhd].[G_CycleTime],
            [nhd].[G_NestNr],
            [nhd].[G_RegulationOn],
            [nhd].[G_UseGrindTimeCorr],
            [nhd].[G_UseControlledCD],
            [nhd].[G_UseDeltaQ],
            [nhd].[USED_FACTOR1],
            [nhd].[USED_FACTOR2],
            [nhd].[G_Error],
            [nhd].[FM_Pressure],
            [nhd].[FM_Flow],
            [nhd].[FM_Density],
            [nhd].[FM_UsedDensity],
            [nhd].[FM_Temperature],
            [nhd].[FM_Pressure1BP],
            [nhd].[FM_Pressure2BP],
            [nhd].[FM_FlowBP],
            [nhd].[FM_DeltaQ],
            [nhd].[FM_DensityBP],
            [nhd].[FM_UsedDensityBP],
            [nhd].[FM_TemperatureBP],
            [nhd].[FM_MeasureTime],
            [nhd].[FM_CycleTime],
            [nhd].[FM_NestNr],
            [nhd].[IDate]
    FROM    [dbo].[NozzleHegsData] AS [nhd]
    LEFT JOIN [dbo].[NozzleHegsFailCodes] AS [nhfc] ON [nhd].[FailCode] = [nhfc].[FailCode]

GO
-- =============================================
-- Author     :	Steve Smith
-- Create date: 27-Mar-2014
-- Description:	Replace normal INSERT to strip out FailCodeText
-- =============================================
-- Modifier   :	Steve Smith
-- Alter  date: 2-Jul-2014
-- Description:	BulkInsert rows missing so changed to allow loop of INSERTED records rather than assuming 1 row as before
-- =============================================
-- Modifier   :	Steven Cook
-- Alter  date: 21-Jul-2014
-- Description:	Modified check before new FailCode insertion to look for existing FailCodes - rather than the FailCodeText
-- =============================================
CREATE TRIGGER [dbo].[NozzleHegsInsteadOfINSERT]
ON  [dbo].[NozzleHegs] 
INSTEAD OF INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Variables for loop
	DECLARE @rows BIGINT, @row BIGINT, @FailCode INT, @FailCodeText VARCHAR(MAX);

	-- Declare a temporary table to hold all the rows inserted
	DECLARE @tbl TABLE ([UID] BIGINT,[TDate] DATETIME2(7),[PDate] DATETIME,[PlantNumber] INT,[SerialNum] VARCHAR(50),[Product] VARCHAR(50),[Type] VARCHAR(50),
			[FailCode] INT,[FailCodeText] VARCHAR(MAX),[FromWhichPosalux] INT,[FromWhichPosaluxHead] INT,[PM_Pressure] FLOAT,
			[PM_Flow] FLOAT,[PM_Density] FLOAT,[PM_UsedDensity] FLOAT,[PM_Temperature] FLOAT,[PM_Pressure1BP] FLOAT,[PM_Pressure2BP] FLOAT,
			[PM_FlowBP] FLOAT,[PM_DeltaQ] FLOAT,[PM_DensityBP] FLOAT,[PM_UsedDensityBP] FLOAT,[PM_TemperatureBP] FLOAT,[PM_MeasureTime] FLOAT,
			[PM_CycleTime] FLOAT,[PM_NestNr] INT,[G_Pressure] FLOAT,[G_FlowStart] FLOAT,[G_FlowStop] FLOAT,[G_Temperature] FLOAT,
			[G_Density] FLOAT,[G_UsedDensity] FLOAT,[G_GrindTime] FLOAT,[G_CycleTime] FLOAT,[G_NestNr] INT,[G_RegulationOn] BIT,
			[G_UseGrindTimeCorr] BIT,[G_UseControlledCD] BIT,[G_UseDeltaQ] BIT,[USED_FACTOR1] FLOAT,[USED_FACTOR2] FLOAT,[G_Error] INT,
			[FM_Pressure] FLOAT,[FM_Flow] FLOAT,[FM_Density] FLOAT,[FM_UsedDensity] FLOAT,[FM_Temperature] FLOAT,[FM_Pressure1BP] FLOAT,
			[FM_Pressure2BP] FLOAT,[FM_FlowBP] FLOAT,[FM_DeltaQ] FLOAT,[FM_DensityBP] FLOAT,[FM_UsedDensityBP] FLOAT,[FM_TemperatureBP] FLOAT,
			[FM_MeasureTime] FLOAT,[FM_CycleTime] FLOAT,[FM_NestNr] INT);

	-- Insert everything into temp table with an incrementing [UID]
	INSERT INTO @tbl
	SELECT ROW_NUMBER() OVER(ORDER BY [TDate] ASC),[TDate],[PDate],[PlantNumber],[SerialNum],[Product],[Type],[FailCode],
			[FailCodeText],[FromWhichPosalux],[FromWhichPosaluxHead],[PM_Pressure],[PM_Flow],[PM_Density],[PM_UsedDensity],
			[PM_Temperature],[PM_Pressure1BP],[PM_Pressure2BP],[PM_FlowBP],[PM_DeltaQ],[PM_DensityBP],[PM_UsedDensityBP],
			[PM_TemperatureBP],[PM_MeasureTime],[PM_CycleTime],[PM_NestNr],[G_Pressure],[G_FlowStart],[G_FlowStop],
			[G_Temperature],[G_Density],[G_UsedDensity],[G_GrindTime],[G_CycleTime],[G_NestNr],[G_RegulationOn],
			[G_UseGrindTimeCorr],[G_UseControlledCD],[G_UseDeltaQ],[USED_FACTOR1],[USED_FACTOR2],[G_Error],[FM_Pressure],
			[FM_Flow],[FM_Density],[FM_UsedDensity],[FM_Temperature],[FM_Pressure1BP],[FM_Pressure2BP],[FM_FlowBP],
			[FM_DeltaQ],[FM_DensityBP],[FM_UsedDensityBP],[FM_TemperatureBP],[FM_MeasureTime],[FM_CycleTime],[FM_NestNr]
	FROM INSERTED;
  
	-- Count rows inserted into temp table
	SELECT @rows=@@ROWCOUNT;
	-- Start at first row
	SET @row=1;
	-- Loop each row
	WHILE(@row<=@rows)
		BEGIN
			-- Get the current FailCode and Text for this loop
			SELECT @FailCode=[FailCode], @FailCodeText=[FailCodeText] FROM @tbl WHERE [UID]=@row;
			-- Check we have something
			IF(ISNULL(@FailCodeText,'argh')='argh')
			BEGIN
				SET @FailCodeText = 'WasNULL';
			END
			
		    -- Find ID and create if does not exist   
			-- SC: 21/Jul/2014 - Modified to stop duplicate FailCodes being entered (currently a row is being entered with a blank FailCodeText) 
			IF NOT EXISTS(SELECT * FROM [dbo].[NozzleHegsFailCodes] WHERE [FailCode] = @FailCode)
				BEGIN
					INSERT INTO [dbo].[NozzleHegsFailCodes]([FailCode],[FailCodeText]) VALUES(@FailCode,@FailCodeText);
				END
		    -- Insert Data with ID
			INSERT INTO [dbo].[NozzleHegsData]([TDate],[PDate],[PlantNumber],[SerialNum],[Product],[Type],[FailCode],[FromWhichPosalux],[FromWhichPosaluxHead],[PM_Pressure],[PM_Flow],[PM_Density],[PM_UsedDensity],[PM_Temperature],[PM_Pressure1BP],[PM_Pressure2BP],[PM_FlowBP],[PM_DeltaQ],[PM_DensityBP],[PM_UsedDensityBP],[PM_TemperatureBP],[PM_MeasureTime],[PM_CycleTime],[PM_NestNr],[G_Pressure],[G_FlowStart],[G_FlowStop],[G_Temperature],[G_Density],[G_UsedDensity],[G_GrindTime],[G_CycleTime],[G_NestNr],[G_RegulationOn],[G_UseGrindTimeCorr],[G_UseControlledCD],[G_UseDeltaQ],[USED_FACTOR1],[USED_FACTOR2],[G_Error],[FM_Pressure],[FM_Flow],[FM_Density],[FM_UsedDensity],[FM_Temperature],[FM_Pressure1BP],[FM_Pressure2BP],[FM_FlowBP],[FM_DeltaQ],[FM_DensityBP],[FM_UsedDensityBP],[FM_TemperatureBP],[FM_MeasureTime],[FM_CycleTime],[FM_NestNr])
			SELECT [TDate],[PDate],[PlantNumber],[SerialNum],[Product],[Type],[FailCode],[FromWhichPosalux],[FromWhichPosaluxHead],[PM_Pressure],[PM_Flow],[PM_Density],[PM_UsedDensity],[PM_Temperature],[PM_Pressure1BP],[PM_Pressure2BP],[PM_FlowBP],[PM_DeltaQ],[PM_DensityBP],[PM_UsedDensityBP],[PM_TemperatureBP],[PM_MeasureTime],[PM_CycleTime],[PM_NestNr],[G_Pressure],[G_FlowStart],[G_FlowStop],[G_Temperature],[G_Density],[G_UsedDensity],[G_GrindTime],[G_CycleTime],[G_NestNr],[G_RegulationOn],[G_UseGrindTimeCorr],[G_UseControlledCD],[G_UseDeltaQ],[USED_FACTOR1],[USED_FACTOR2],[G_Error],[FM_Pressure],[FM_Flow],[FM_Density],[FM_UsedDensity],[FM_Temperature],[FM_Pressure1BP],[FM_Pressure2BP],[FM_FlowBP],[FM_DeltaQ],[FM_DensityBP],[FM_UsedDensityBP],[FM_TemperatureBP],[FM_MeasureTime],[FM_CycleTime],[FM_NestNr]
			FROM @tbl WHERE [UID] = @row;
			-- Increment the loop count
			SET @row = @row + 1;
		END
END