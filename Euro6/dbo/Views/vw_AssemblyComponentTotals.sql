﻿


CREATE VIEW [dbo].[vw_AssemblyComponentTotals]
AS

SELECT   P.PDate, RIGHT(N.Injector_ID, 9) AS ISN, R.NCV_DataMatrix AS NCVSerial, N.Nozzle_DataMatrix AS NozzleSerial, P.PistonGuideSerial AS PGSerial, N.Nozzle_DataMatrixCount, R.NCV_DataMatrixCount
FROM     dbo.NLS AS N
		 INNER JOIN
         dbo.RSPL AS R ON N.Injector_ID = R.Injector_ID
		 INNER JOIN
		 DBO.NNCAssembly AS P ON P.NozzleSerial=N.Nozzle_DataMatrix
UNION ALL
SELECT 	P.PDate, RIGHT(N.Serial, 9) AS ISN,N.NCV_SerialNumber AS NCVSerial, N.Nozzle_SerialNumber AS NozzleSerial, P.PistonGuideSerial,  N.NozzleCount, N.NozzleCount
FROM    dbo.MASMEC_SelAsmb_OP20B_Results as N	
		INNER JOIN 
		dbo.NNCAssembly AS P ON N.Nozzle_SerialNumber=P.NozzleSerial


GO








-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE TRIGGER [dbo].[vw_AssemblyComponentTotals_ONINSERT]
   ON  [dbo].[vw_AssemblyComponentTotals]
   INSTEAD OF INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for trigger here
	 INSERT INTO [dbo].[AssemblyComponentTotals](PDate,ISN,NCVSerial,NozzleSerial,NCVSerialCount,NozzleSerialCount)
     SELECT PDate,ISN,NCVSerial,NozzleSerial,'0','0' FROM INSERTED	

END

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "N"
            Begin Extent = 
               Top = 11
               Left = 68
               Bottom = 141
               Right = 302
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "R"
            Begin Extent = 
               Top = 16
               Left = 328
               Bottom = 146
               Right = 562
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 1545
         Table = 1170
         Output = 975
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vw_AssemblyComponentTotals';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vw_AssemblyComponentTotals';

