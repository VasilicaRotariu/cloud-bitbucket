﻿-- =============================================
-- Author:		James Lee
-- Create date: 20131104
-- Description:	Returns a 120 day back run of pass rate
-- =============================================
CREATE FUNCTION [dbo].[Return120DayM2PassRate] 
(
)
RETURNS 
@Results TABLE 
(
	-- Add the column definitions for the TABLE variable here
	Date datetime, DayTotal float, Result_0 int, R_0_Percentage float --, Result_1 int, R_1_Percentage float, Result_2 int, R_2_Percentage float, Result_3 int, R_3_Percentage float
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set
	
	
	

	DECLARE @_ResultCount table (Result float, Date datetime, Hour int, HourTime time, 
	Quantity float)
	
	
	DECLARE @_DayTotals table (Date datetime, HourTime time, Hour int, DayTotal float)
	
	DECLARE @_FullTimeRange table (DateRange datetime)
	declare @table_StartEnd_Range table (
    startTime datetime
,   endTime datetime
)	
	
	DECLARE @BackLookPeriod int = 120
	DECLARE @Start datetime = DATEADD(DAY, -@BackLookPeriod, CURRENT_TIMESTAMP), @End datetime = DATEADD(DAY, 1, CURRENT_TIMESTAMP)
	
	/**** Pre populate a timespan table to start every point at 0 ****/
	
	/* Adjust date time entry to be yyyy-mm-dd HH:00:00.000 */
	SET @Start = DATEADD(dd, 0, DATEDIFF(dd, 0, @Start))
	SET @End = DATEADD(dd, 0, DATEDIFF(dd, 0, @End))
	
	INSERT INTO @table_StartEnd_Range
	select @Start, @End

	INSERT INTO @_FullTimeRange
	select
		dateadd(DAY, n.Number, t.startTime) as Hours
	from @table_StartEnd_Range t
		inner join util_Numbers n
    -- assuming your numbers start at 1 rather than 0
    on n.Number-1 <= datediff(DAY, t.startTime, t.endTime)
	
	-- Insert statements for procedure here
	--SET @EndDate = DATEADD(DAY,1,@EndDate)
	
	/**** Get the results for the timespan *****/
	DECLARE @_ResultCount_Day table (Result float, Date datetime, Quantity float)
	
	INSERT INTO @_ResultCount_Day
	
	SELECT Result, CAST(Date as Date) as Date, COUNT(*) as Quantity	
	FROM Euro6.dbo.Header H
	where Date BETWEEN @Start and @End
	and TestPlan = 'J1C00001 C6R'
	and Batch in ('Virgin')
	GROUP BY CAST(Date as Date), Result
	order by Date asc, Result asc
	
	/***** Get the day totals  ******/
	DECLARE @_DayTotals_Day table (Date datetime, DayTotal float)
	
	INSERT INTO @_DayTotals_Day
	SELECT FTR.DateRange, overview.DayTotal FROM @_FullTimeRange FTR
	LEFT JOIN (
	SELECT Date, SUM(Quantity) as DayTotal FROM @_ResultCount_Day
	group by date
	) overview on overview.Date = FTR.DateRange
	
	order by FTR.DateRange desc
	
	INSERT INTO @Results
	SELECT DayTotals.*
	, Result0.Quantity as Result_0
	, ROUND((Result0.Quantity / DayTotals.DayTotal) * 100,1) as R_0_Percentage /*
	, Result1.Quantity as Result_1
	, ROUND((Result1.Quantity / DayTotals.DayTotal) * 100,1) as R_1_Percentage
	, Result2.Quantity as Result_2
	, ROUND((Result2.Quantity / DayTotals.DayTotal) * 100,1) as R_2_Percentage
	, Result3.Quantity as Result_3
	, ROUND((Result3.Quantity / DayTotals.DayTotal) * 100,1) as R_3_Percentage */
	
	FROM @_DayTotals_Day DayTotals
	
	LEFT JOIN (
	SELECT * FROM @_ResultCount_Day where Result = 0
	) Result0 on Result0.Date = DayTotals.Date
	
	LEFT JOIN (
	SELECT * FROM @_ResultCount_Day where Result = 1
	) Result1 on Result1.Date = DayTotals.Date
	
	LEFT JOIN (
	SELECT * FROM @_ResultCount_Day where Result = 2
	) Result2 on Result2.Date = DayTotals.Date
	
	LEFT JOIN (
	SELECT * FROM @_ResultCount_Day where Result = 3
	) Result3 on Result3.Date = DayTotals.Date
	
	WHERE DayTotals.Date BETWEEN @Start and DATEADD(DAY,-1,@End)


	order by DayTotals.Date desc



	
	RETURN 
END
