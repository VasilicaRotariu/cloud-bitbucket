﻿-- =============================================
-- Author:		James Lee
-- Create date: 22/10/2013
-- Description:	NCV RDO Flow data
-- =============================================
CREATE FUNCTION [dbo].[ReturnNCVRDOData] 
(	
	-- Add the parameters for the function here
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	/*
SELECT [PDate] as [PDate]
      ,[Serial] as [Matrix]
      ,[Flow2] as [RDOFlow]
	  ,20679 as [PN]
  FROM [Euro6].[dbo].[20679_SonplasHEG_Measure_Results]
  where PDate IN (SELECT MAX(PDate) From Euro6.dbo.[20679_SonplasHEG_Measure_Results] Group by Serial)
  union all
  SELECT [Pdate] as [PDate]
      ,[Serial] as [Matrix]
      ,[Flow2] as [RDOFlow]
	  ,20538 as [PN]
  FROM [Euro6].[dbo].[20538_SonplasHEG_Measure_Results]
  where PDate IN (SELECT MAX(PDate) From Euro6.dbo.[20538_SonplasHEG_Measure_Results] Group by Serial)
  union all
  SELECT [PDate] as [PDate]
      ,[Serial] as [Matrix]
	  ,[Flow_2] as [RDOFlow]
	  ,20445 as [PN]
  FROM [Euro6].[dbo].[20445_SonplasHEG_Measure_Results]
  where PDate IN (SELECT MAX(PDate) From Euro6.dbo.[20445_SonplasHEG_Measure_Results] Group by Serial)
  */

  -- Updated 02/04/2014 - Phil Harper request.
  -- Need to update Stonehouse view to show the RDO flow data.

  SELECT PDate as PDate
  ,[Serial] COLLATE Database_Default as Matrix
  ,[FM_Flow2] as RDOFlow
  ,PlantNumber as PN
   FROM
  [Euro6].dbo.[ManualComponentHeg]
  where PDate IN (SELECT MAX(PDate) from Euro6.dbo.[ManualComponentHeg] Group By Serial)

  union all

  SELECT PDate as PDate
  ,[Serial] COLLATE Database_Default as Matrix
  ,[FM_Flow2] as RDOFlow
  ,AssetNumber as PN
   FROM
  [Euro6].dbo.[ComponentHegs]
  where PDate IN (SELECT MAX(PDate) from Euro6.dbo.[ComponentHegs] Group By Serial)

)
