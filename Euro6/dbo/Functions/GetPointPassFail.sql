﻿-- =============================================
-- Author:		James Lee
-- Create date: 20130910
-- Description:	Gets the pass fail for each test point
-- =============================================
CREATE FUNCTION [dbo].[GetPointPassFail] 
(
	-- Add the parameters for the function here
	@p int
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result int

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = @p

	-- Return the result of the function
	RETURN @Result

END
