﻿-- =============================================
-- Author:		James Lee
-- Create date: 2013-08-19
-- Description:	Interpolate
-- =============================================
CREATE FUNCTION [dbo].[Interpolate] 
(
	-- Add the parameters for the function here
	@x_big float,
	@x_small float,
	@x_knownValue float,
	@y_big float,
	@y_small float
)
RETURNS float
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result float
	
	
	-- Add the T-SQL statements to compute the return value here
	
	/*** Interpolate a positive gradient ***/
	IF (@x_small < @x_big AND @y_small < @y_big)
	BEGIN
	SET @Result =
	
	
	( ( (@x_knownValue - @x_small)/(@x_big - @x_small) ) 
									* ( @y_big - @y_small ) + @y_small )
		END							
		
	/*** Interpolate a negative gradient ***/
	IF (@x_small < @x_big AND @y_small > @y_big)
	BEGIN
	SET @Result =
	
	
	( ( ( (@x_knownValue - @x_small)/(@x_big - @x_small) ) 
									* ( @y_small - @y_big ) ) + @y_big )
		END
		
		/*** If Big and Small Y are the same, don't change. ***/
		IF (@y_small = @y_big)
		BEGIN
		SET @Result = @y_small
		END
	-- Return the result of the function
	RETURN @Result

END
