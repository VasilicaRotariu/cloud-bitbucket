﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[CompressLine3InjectorType] 
(
	-- Add the parameters for the function here
	@InjectorType int
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	IF @InjectorType IN ( 1, 3 )
	BEGIN
		RETURN 1 -- RETURN MX13
	END
	ELSE IF @InjectorType IN ( 2 )
	BEGIN
		RETURN 2 -- RETURN MX11
	END
	 
	RETURN 0
END
