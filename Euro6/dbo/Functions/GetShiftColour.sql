﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetShiftColour]
(
	@Date datetime 
)
RETURNS varchar(20)
AS
BEGIN  
	
	 
	DECLARE @WeekNumber tinyint = DATEPART(WW, @Date)

	DECLARE @evenOrOdd varchar(4) = 'Odd'
	DECLARE @evenOrOddTest int = @WeekNumber % 2

	IF @evenOrOddTest = 0
		SET @evenOrOdd = 'Even';		
	
    RETURN   
    CASE 
			WHEN DATEPART(HH, @Date) >= 06 AND @evenOrOdd = 'Even'AND DATEPART(HH, @Date) < 14 THEN 'GREEN'
            WHEN DATEPART(HH, @Date) >= 14 AND @evenOrOdd = 'Even'AND DATEPART(HH, @Date) < 22 THEN 'ORANGE'
            WHEN DATEPART(HH, @Date) >= 06 AND @evenOrOdd = 'Odd'AND DATEPART(HH, @Date) < 14 THEN 'GREEN'
            WHEN DATEPART(HH, @Date) >= 14 AND @evenOrOdd = 'Odd'AND DATEPART(HH, @Date) < 22 THEN 'ORANGE'
            ELSE 'BLUE'
    END 
END


