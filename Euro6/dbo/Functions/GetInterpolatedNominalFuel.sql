﻿-- =============================================
-- Author:		James Lee
-- Create date: 2013-08-14
-- Description:	Returns the interpolated nominal fuel
-- =============================================
CREATE FUNCTION [dbo].[GetInterpolatedNominalFuel] 
(
	-- Add the parameters for the function here
	@Injector_SetRP float,
	@Injector_TrimmedNCVDuration float
)
RETURNS float
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Delivery float
	

	
	DECLARE @ValidFromDate datetime
	
	SET @ValidFromDate = (
	SELECT TOP 1 MAX(ValidFrom) 
	From Euro6.dbo.Test_Nominals_C6
	group by validfrom
	order by validfrom desc
	)

	DECLARE @LowerNCVDuration float
	DECLARE @LowerNomFueling float
	DECLARE @UpperNCVDuration float
	DECLARE @UpperNomFueling float


	-- Add the T-SQL statements to compute the return value here	
	
	-- Get the upper NCV Duration
	SET @UpperNCVDuration = (
	SELECT TOP 1 NCVDuration From Euro6.dbo.Test_Nominals_C6
	WHERE SetRP = @Injector_SetRP and ValidFrom = @ValidFromDate and NCVDuration > @Injector_TrimmedNCVDuration
	order by NCVDuration asc
	)
	-- Get the upper Nominal delivery
	SET @UpperNomFueling = (
	SELECT TOP 1 Delivery FROM Euro6.dbo.Test_Nominals_C6
	WHERE SetRP = @Injector_SetRP and ValidFrom = @ValidFromDate and NCVDuration = @UpperNCVDuration
	)
	
	-- Get the lower NCV Duration
	SET @LowerNCVDuration = (
	SELECT TOP 1 NCVDuration From Euro6.dbo.Test_Nominals_C6
	WHERE SetRP = @Injector_SetRP and ValidFrom = @ValidFromDate and NCVDuration < @Injector_TrimmedNCVDuration
	order by NCVDuration desc
	)
	-- Get the lower Nominal Delivery
	SET @LowerNomFueling = (
	SELECT TOP 1 Delivery FROM Euro6.dbo.Test_Nominals_C6
	WHERE SetRP = @Injector_SetRP and ValidFrom = @ValidFromDate and NCVDuration = @LowerNCVDuration
	)

	/*
	   Injector Trimmed NCV - Lower NCV
	(  --------------------------------  )  *  (UpperFuel - LowerFuel) + LowerFuel
	   Upper NCV       -      Lower NCV
	
	*/
	
	SELECT @Delivery =
	(
	((@Injector_TrimmedNCVDuration - @LowerNCVDuration)
					/
		(@UpperNCVDuration - @LowerNCVDuration))
				* ( @UpperNomFueling - @LowerNomFueling ) + @LowerNomFueling
	)
	-- Return the result of the function
	RETURN @Delivery

END
