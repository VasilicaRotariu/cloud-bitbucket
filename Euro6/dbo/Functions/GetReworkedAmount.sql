﻿-- =============================================
-- Author:		<James with a bit of help from Alex>
-- Create date: <3/6/2015>
-- Description:	<Gets the amount of times part has been reworked>
-- =============================================
CREATE FUNCTION [dbo].[GetReworkedAmount]
(
	@Serial varchar(15),
	@TableCount int,
	@TableNames varchar(MAX),
	@ColumnNames varchar(MAX)
)
RETURNS int
AS
BEGIN  
	
	 
	DECLARE @ReworkedAmount int;
	DECLARE @CurrentChar int;
	DECLARE @CurrentTable int = 0;
	DECLARE @TableName VARCHAR(20);
	DECLARE @ColumnName VARCHAR(20);

	WHILE (@CurrentTable < @TableCount)
	BEGIN
		SET @TableName = SUBSTRING(@TableNames, 0, CHARINDEX(',', @TableNames) - 1);
		SET @TableNames = SUBSTRING(@TableNames,  CHARINDEX(',', @TableNames) + 1, LEN(@TableNames));

		SET @ColumnName = SUBSTRING(@ColumnNames, 0, CHARINDEX(',', @ColumnNames) - 1);
		SET @ColumnNames = SUBSTRING(@ColumnNames,  CHARINDEX(',', @ColumnNames) + 1, LEN(@ColumnNames));

		DECLARE @sqlCommand varchar(1000)

		DECLARE @CurrentAmount int;
		
		Exec Euro6.dbo.[GetCountOfSerial] 
		@Serial = @Serial,
		@TableName = @TableName, 
		@ColumnName = @ColumnName,
		@Count = @CurrentAmount OUT

		SET @ReworkedAmount = @ReworkedAmount + @CurrentAmount;
		
		SET @CurrentTable = @CurrentTable + 1;
	END	
	
    RETURN   
    @ReworkedAmount
END

