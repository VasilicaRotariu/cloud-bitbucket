﻿
-- =============================================
-- Author:		James Lee
-- Create date: 2013-08-19
-- Description:	Checks the Test Limits to see if band fails/passes
-- =============================================
CREATE FUNCTION [dbo].[GetBandPassFailM6] 
(
	-- Add the parameters for the function here
	@TrumpetGraphNumber int,
	@PassFailBand int,
	@Upper_pos1_Or_Lower_pos2 int, --Added 20130820 JL
	@TestNo int,
	@TestPlanName varchar(50)
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result int

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = @TrumpetGraphNumber
	
	----------------------- Select the relevent graph X Y.
	DECLARE @GraphingLimits table (
	NominalFuel float,
	Error float
	)
	
	INSERT @GraphingLimits
	SELECT x, y 
	FROM Euro6.dbo.Test_Limits_C6
	where Graph = @TrumpetGraphNumber and [Set] = @PassFailBand
	and TestPlan = (SELECT Uid FROM Euro6.dbo.Test_TestPlans where Name = @TestPlanName)
	
	----------------------- Set Upper & Lower bounds to check
	DECLARE @GraphXLimitUpper float
	DECLARE @GraphXLimitLower float
	
	SET @GraphXLimitUpper = (SELECT TOP 1 NominalFuel FROM @GraphingLimits order by NominalFuel desc)	
	SET @GraphXLimitLower = (SELECT TOP 1 NominalFuel FROM @GraphingLimits order by NominalFuel asc)
	
	----------------------- Get correct Rail Pressure for Testplan:
	DECLARE @SetRP float
	
	SET @SetRP = (
	SELECT TOP 1
	CASE
		WHEN @TrumpetGraphNumber = 1 THEN RP1
		WHEN @TrumpetGraphNumber = 2 THEN RP2
		WHEN @TrumpetGraphNumber = 3 THEN RP3
		WHEN @TrumpetGraphNumber = 4 THEN RP4
		WHEN @TrumpetGraphNumber = 5 THEN RP5
	END
	 FROM
	Euro6.dbo.Test_TestPlans
	WHERE Name = @TestPlanName
	)
	
	
	----------------------- Check through the Nominal fuels to see where the error may be. Interpolate between.
	DECLARE @Results table (
	NominalFueling float,
	Error float,
	Interpolate float,
	Result int
	)
	
	INSERT @Results
	SELECT NominalDelivery,
	ERROR,
	Euro6.dbo.Interpolate(
		/* Big X */			(SELECT TOP 1 NominalFuel FROM @GraphingLimits where NominalFuel > NominalDelivery order by NominalFuel asc),
		/* Small X */		(SELECT TOP 1 NominalFuel FROM @GraphingLimits where NominalFuel < NominalDelivery order by NominalFuel desc),
		/* Known X */		NominalDelivery,
		/* Big Y */			(SELECT TOP 1 Error FROM @GraphingLimits where NominalFuel > NominalDelivery order by NominalFuel asc),
		/* Small Y */		(SELECT TOP 1 Error FROM @GraphingLimits where NominalFuel < NominalDelivery order by NominalFuel desc)) as Interpolate,
		CASE
		--When the error is greater than the +ve or less than the -ve, result is +1, -1 respectivly.
			WHEN ERROR > Euro6.dbo.Interpolate(
		/* Big X */			(SELECT TOP 1 NominalFuel FROM @GraphingLimits where NominalFuel > NominalDelivery order by NominalFuel asc),
		/* Small X */		(SELECT TOP 1 NominalFuel FROM @GraphingLimits where NominalFuel < NominalDelivery order by NominalFuel desc),
		/* Known X */		NominalDelivery,
		/* Big Y */			(SELECT TOP 1 Error FROM @GraphingLimits where NominalFuel > NominalDelivery order by NominalFuel asc),
		/* Small Y */		(SELECT TOP 1 Error FROM @GraphingLimits where NominalFuel < NominalDelivery order by NominalFuel desc))
			 THEN 1
			WHEN ERROR < (0 - Euro6.dbo.Interpolate(
		/* Big X */			(SELECT TOP 1 NominalFuel FROM @GraphingLimits where NominalFuel > NominalDelivery order by NominalFuel asc),
		/* Small X */		(SELECT TOP 1 NominalFuel FROM @GraphingLimits where NominalFuel < NominalDelivery order by NominalFuel desc),
		/* Known X */		NominalDelivery,
		/* Big Y */			(SELECT TOP 1 Error FROM @GraphingLimits where NominalFuel > NominalDelivery order by NominalFuel asc),
		/* Small Y */		(SELECT TOP 1 Error FROM @GraphingLimits where NominalFuel < NominalDelivery order by NominalFuel desc)) )
			 THEN -1
			WHEN ERROR BETWEEN (0 - Euro6.dbo.Interpolate(
		/* Big X */			(SELECT TOP 1 NominalFuel FROM @GraphingLimits where NominalFuel > NominalDelivery order by NominalFuel asc),
		/* Small X */		(SELECT TOP 1 NominalFuel FROM @GraphingLimits where NominalFuel < NominalDelivery order by NominalFuel desc),
		/* Known X */		NominalDelivery,
		/* Big Y */			(SELECT TOP 1 Error FROM @GraphingLimits where NominalFuel > NominalDelivery order by NominalFuel asc),
		/* Small Y */		(SELECT TOP 1 Error FROM @GraphingLimits where NominalFuel < NominalDelivery order by NominalFuel desc)) )
		AND (Euro6.dbo.Interpolate(
		/* Big X */			(SELECT TOP 1 NominalFuel FROM @GraphingLimits where NominalFuel > NominalDelivery order by NominalFuel asc),
		/* Small X */		(SELECT TOP 1 NominalFuel FROM @GraphingLimits where NominalFuel < NominalDelivery order by NominalFuel desc),
		/* Known X */		NominalDelivery,
		/* Big Y */			(SELECT TOP 1 Error FROM @GraphingLimits where NominalFuel > NominalDelivery order by NominalFuel asc),
		/* Small Y */		(SELECT TOP 1 Error FROM @GraphingLimits where NominalFuel < NominalDelivery order by NominalFuel desc)) )
			 THEN 0
		END as Result
	FROM Euro6.dbo.TestDataM6_Pre
	where TestNo = @TestNo and NominalDelivery BETWEEN @GraphXLimitLower and @GraphXLimitUpper
	and SetRP = @SetRP
	
	
	--- Check to see if there are any -ves or +ves.
	DECLARE @UpperFails int, @LowerFails int
	
	--- Count the number of +ves
	SET @UpperFails = (SELECT COUNT(*) From @Results where Result > 0)
	
	--- Count the number of -ves
	SET @LowerFails = (SELECT COUNT(*) FROM @Results where Result < 0)
	
	IF (COUNT(@Result) = 0 or @Upper_pos1_Or_Lower_pos2 = 0)
	BEGIN
	SET @Result = 999
	END
	ELSE
	BEGIN

--Added 20130820 JL	
	-- If the request was upper fails, then send upper
	IF (@Upper_pos1_Or_Lower_pos2 = 1) --> 0)
	BEGIN
		IF (@UpperFails = 0)
		BEGIN
		SET @Result = 0
		END
		ELSE
		BEGIN
		SET @Result = 1
		END
	END
	
	-- If the request was lower fails, then send lower
	IF (@Upper_pos1_Or_Lower_pos2 = 2) --< 0)
	BEGIN
		IF (@LowerFails = 0)
		BEGIN
		SET @Result = 0
		END
		ELSE
		BEGIN
		SET @Result = 1
		END
	END
--END Added...
	/*
	
	IF (@UpperFails > @LowerFails)
	BEGIN
	SET @Result = 1
	END
	ELSE
	BEGIN
	
		IF (@UpperFails = 0 AND @LowerFails = 0)
		BEGIN
		SET @Result = 0
		END
		ELSE
		BEGIN
	
			IF (@UpperFails <= @LowerFails)
			BEGIN
			SET @Result = -1
			END
		END
	END */
	
	END	
	

	-- Return the result of the function
	RETURN @Result

END

