﻿-- =============================================
-- Author:		James Lee
-- Create date: 20131104
-- Description:	Returns the Fag and Tilt of injectors going through test
-- =============================================
CREATE FUNCTION [dbo].[ReturnInjectorTestWithFagAndTilt] 
(
)
RETURNS 
@Results TABLE 
(
	-- Add the column definitions for the TABLE variable here
	TestDate datetime, 
	TestNo int, 
	InjectorSerialNumber varchar(30), 
	AEASerial varchar(30),
	Result int,
    Fag float,
    RDOFlow float
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set
	INSERT INTO @Results
	Select  PDate as TestDate, TestNo, Test.InjectorSerialNumber, AEASerial

--, Delivery_Untrimmed_RAW__2400_300, Delivery_Untrimmed_RAW__2400_310, Delivery_Untrimmed_RAW__2400_320, Delivery_Untrimmed_RAW__2400_330

      ,Test.[Result]
   ,Fag
   ,RDO.Flow as RDOFlow

From Euro6.dbo.[vw_MDEG_Injector_Test_v2] Test



  JOIN (

  SELECT NCV_DataMatrix, Injector_ID FROM Euro6.dbo.RSPL

  ) as Assembly_NCV on Assembly_NCV.Injector_ID = SUBSTRING(Test.AEASerial,1,10)

  JOIN (

  SELECT SerialNumber, Fag FROM Euro6.dbo.Zygo
  where PDate IN (SELECT MAX(PDate) FROM Euro6.dbo.Zygo group by SerialNumber)

  ) as Fag on Assembly_NCV.NCV_DataMatrix = Fag.SerialNumber

  JOIN (

  SELECT SerialNumber, Flow FROM Euro6.dbo.HPVT
  where PDate IN (SELECT MAX(PDate) FROM Euro6.dbo.HPVT group by SerialNumber)

  ) as RDO on Assembly_NCV.NCV_DataMatrix = RDO.SerialNumber


WHERE TestPlan = 'J1C00001 C6R'
and Batch in ('Virgin')
and InjectorSerialNumber like '%[0-9]%'

	--and Delivery_3SDs__2400_330 < 3 and Delivery_Untrimmed_RAW__2400_330 > 22 and Delivery_Untrimmed_RAW__2400_330 < 23

ORDER BY PDate DESC
	RETURN 
END
