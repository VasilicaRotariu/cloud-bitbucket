﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetShift]
(
	@Date datetime 
)
RETURNS varchar(20)
AS
BEGIN  	
    RETURN   
    CASE 
            WHEN DATEPART(HH, @Date) >= 06 AND DATEPART(HH, @Date) < 14 THEN 'EARLY'
            WHEN DATEPART(HH, @Date) >= 14 AND DATEPART(HH, @Date) < 22 THEN 'LATE'
            ELSE 'NIGHT'
    END 
END

