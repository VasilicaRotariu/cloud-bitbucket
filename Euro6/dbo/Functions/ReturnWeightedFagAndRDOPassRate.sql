﻿-- =============================================
-- Author:		James Lee
-- Create date: 20131105
-- Description:	Returns the injector RDO anf FAG values
-- =============================================
CREATE FUNCTION [dbo].[ReturnWeightedFagAndRDOPassRate] 
(
	@StartDate datetime
)
RETURNS 
@Results TABLE 
(
	-- Add the column definitions for the TABLE variable here
	Fag float,
	RDOFlow float,
	WeightedPassChance float,
	PassChance float,
	TotalInjectorsInRegion int,
	PassInjectorsInRegion int,
	TotalInjectorsInMap int
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set
DECLARE @Results_All table (Quantity int, Fag float, RDOFlow float)

INSERT INTO @Results_All
SELECT TOP 3000 
	  Count(Result) as Quantity
      ,ROUND([Fag],0,1) as Fag
      ,ROUND([RDOFlow] * 2,1,1)/2 as RDOFlow
  FROM [Euro6].[dbo].[vw_M2ResultsWithFagTiltAndDailyPassRate]

  where date > @StartDate --and Result = 0
  
  group by ROUND(Fag,0,1), ROUND(RDOFlow * 2,1,1)/2

  order by FAG desc


DECLARE @Results_Passes table (Quantity int, Fag float, RDOFlow float)

INSERT INTO @Results_Passes
SELECT TOP 3000 
	  Count(Result) as Quantity
      ,ROUND([Fag],0,1) as Fag
      ,ROUND([RDOFlow] * 2,1,1)/2 as RDOFlow
  FROM [Euro6].[dbo].[vw_M2ResultsWithFagTiltAndDailyPassRate]
  
  where Result = 0

  and date > @StartDate

  group by ROUND(Fag,0,1), ROUND(RDOFlow * 2,1,1) / 2

  order by FAG desc

  INSERT INTO @Results
  SELECT 
  AR.Fag, 
  AR.RDOFlow, 
  ((CAST(PR.Quantity as float) / CAST(AR.Quantity as float)) * (CAST(AR.Quantity as float) / CAST(Total.Total as float))) as WeightedPassChance,
  ((CAST(PR.Quantity as float) / CAST(AR.Quantity as float)) * 100) as PassChance,
  AR.Quantity as TotalInjectorsInRegion,
  PR.Quantity as PassInjectorsInRegion,
  Total.Total
  
  FROM @Results_All AR
  JOIN ( SELECT * FROM @Results_Passes) PR on AR.Fag = PR.Fag and AR.RDOFlow = PR.RDOFlow

  JOIN ( SELECT SUM(Quantity) as Total FROM @Results_All ) Total on 0=0


  order by TotalInjectorsInRegion desc

  RETURN
	
END
