﻿-- =============================================
-- Author:		James Lee
-- Create date: 20130813
-- Description:	Generate MDP from Nominal
-- =============================================
CREATE FUNCTION [dbo].[GetNominalMDP] 
(
	-- Add the parameters for the function here
	@ValidFromDate datetime,
	@SetRP int
)
RETURNS float
AS
BEGIN
	-- Declare the return variable here
	DECLARE @MDP float
	
	
	DECLARE @LowerNCVDuration int
	
	SELECT @LowerNCVDuration = ( SELECT TOP 1 NCVDuration FROM Euro6.dbo.Test_Nominals_C6
	WHERE SetRP = /*@SetRP*/ 2400 and ValidFrom = @ValidFromDate and Delivery < 1
	order by NCVDuration desc
	)

	-- Add the T-SQL statements to compute the return value here
	SELECT @MDP = ROUND(((1 - (
SELECT TOP 1 Delivery--, NCVDuration
From Euro6.dbo.Test_Nominals_C6

--********* For MDEG, only use the RP of 2400, not @SetRP
Where SetRP = /*@SetRP*/ 2400 and Delivery < 1
and ValidFrom = @ValidFromDate-- IN (SELECT MAX(ValidFrom) FROM Euro6.dbo.Test_Nominals_C6 group by ValidFrom)
order by NCVDuration desc
))/((
SELECT TOP 1 Delivery--, NCVDuration
From Euro6.dbo.Test_Nominals_C6
Where SetRP = /*@SetRP*/ 2400 and Delivery > 1
and ValidFrom = @ValidFromDate -- IN (SELECT MAX(ValidFrom) FROM Euro6.dbo.Test_Nominals_C6 group by ValidFrom)
order by NCVDuration asc
)-(
SELECT TOP 1 Delivery--, NCVDuration
From Euro6.dbo.Test_Nominals_C6
Where SetRP = /*@SetRP*/ 2400 and Delivery < 1
and ValidFrom = @ValidFromDate -- IN (SELECT MAX(ValidFrom) FROM Euro6.dbo.Test_Nominals_C6 group by ValidFrom)
order by NCVDuration desc
)))*5 + @LowerNCVDuration,1)

	-- Return the result of the function
	RETURN @MDP

END
