﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetCountOfSerialFunction]
(
	-- Add the parameters for the stored procedure here
	@Serial varchar(20),
	@TableName varchar(MAX),
	@ColumnName varchar(MAX)
)
RETURNS int
AS
BEGIN
	DECLARE @ReworkedAmount int;
	DECLARE @sqlCommand nvarchar(MAX);

	DECLARE @ParmDefinition nvarchar(500);

	SELECT @sqlCommand = 'SELECT @ReworkedAmountOUT = COUNT(' + @ColumnName + ') FROM ' + @TableName + ' WHERE ' + @ColumnName + ' = ' + @Serial ;
	SET @ParmDefinition = N'@ReworkedAmountOUT int OUTPUT'; 

	EXEC SP_EXECUTESQL @sqlCommand, @ParmDefinition, @ReworkedAmountOUT = @ReworkedAmount OUTPUT;

	RETURN 
	@ReworkedAmount
END
