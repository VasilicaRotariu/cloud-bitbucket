﻿-- =============================================
-- Author:		James Lee
-- Create date: 2013-08-15
-- Description:	Generate Trimmed Injector Fuel and Error
-- =============================================
CREATE FUNCTION [dbo].[GetTrimmedInjectorFuelAndError] 
(
	-- Add the parameters for the function here
	@TestNo int, 
	@NominalMDP float
)
RETURNS 
@Results TABLE 
(
	-- Add the column definitions for the TABLE variable here
SetRP int,
NCVDuration int,
Untrimmed_Delivery float,
Trimmed_Delivery float,
Nominal_Delivery float,
Error float
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set
	
	
--Get Injector tested MDP1
DECLARE @InjMDP1 float
SELECT @InjMDP1 = MDP1 FROM Euro6.dbo.Header_Pre where TestNo = @TestNo

INSERT INTO @Results
SELECT 
RawTestData.SetRP,
RawTestData.NCVDuration,
RawTestData.Delivery as Untrimmed_Delivery,
dbo.GetInterpolatedFuel(@TestNo,(RawTestData.NCVDuration + (@InjMDP1 - @NominalMDP/*@NominalMDP - @InjMDP1*/)),RawTestData.SetRP) as Trimmed_Delivery,
Nominals.Delivery as Nominal_Delivery,
--dbo.GetInterpolatedFuel(@TestNo,(RawTestData.NCVDuration + (-18.69036)),RawTestData.SetRP) - Nominals.Delivery as Error,
dbo.GetInterpolatedFuel(@TestNo,(RawTestData.NCVDuration + (@InjMDP1 - @NominalMDP)),RawTestData.SetRP) - Nominals.Delivery as Error
 FROM Euro6.dbo.TestData RawTestData
 
 
JOIN (
SELECT * FROM Euro6.dbo.Test_Nominals_C6
where ValidFrom = (SELECT MAX(ValidFrom) From Euro6.dbo.Test_Nominals_C6)
) Nominals on RawTestData.SetRP = Nominals.SetRP and RawTestData.NCVDuration = Nominals.NCVDuration
 
 
 where TestNo = @TestNo
	
	RETURN 
END
