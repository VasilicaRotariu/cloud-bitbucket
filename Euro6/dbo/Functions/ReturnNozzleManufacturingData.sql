﻿-- =============================================
-- Author:		James Lee
-- Create date: 17/10/2013
-- Description:	Returns Nozzle Build Data
-- =============================================
CREATE FUNCTION [dbo].[ReturnNozzleManufacturingData] 
(	
	-- Add the parameters for the function here
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here

SELECT 
MatchGrind.PDate as MatchGrind_PDate,
MatchGrind.Nozzle_Matrix,
MatchGrind.PG_Matrix,
MatchGrind.Lift,
MatchGrind.Clearance,

LiftOverCheck.PDate as LiftOvercheck_PDate,
LiftOverCheck.LiftOvercheck,
LiftOverCheck.SpringForce,

NeedleInFlow.[NeedleInFlow PDate] as Old100barMachine_NeedleInFlow_PDate,
NeedleInFlow.NeedleInFlow as Old100barMachine_NeedleInFlow,

INOFlow.[PDate INO] as INOFlow_PDate,
INOFlow.[INO Flow] as INO_Flow,

NozzleTest.Pdate as NozzleTest_PDate,
NozzleTest.Flow_Pressure as NeedleInFlow_TestPressure_bar,
NozzleTest.Flow_MeasuredFlow as NeedleInFlow,
NozzleTest.BackLeak_Leak as Backleak,
NozzleTest.NOP_OpeningPressureSingleShot as NOP,

NozzleEDMHEG.[PDate] as NozzleOutFlow_PDate,
NozzleEDMHEG.[Flow] as NozzleOutFlow


FROM ( SELECT * FROM [20603_MatchGrind_Results]
  WHERE PDATE IN (SELECT MAX(PDATE) FROM [Euro6].[dbo].[20603_MatchGrind_Results] Group By Nozzle_Matrix, PG_Matrix)
union all
SELECT * FROM [20604_MatchGrind_Results]
  WHERE PDATE IN (SELECT MAX(PDATE) FROM [Euro6].[dbo].[20604_MatchGrind_Results] Group By Nozzle_Matrix, PG_Matrix) ) AS MatchGrind

LEFT
JOIN (
SELECT * FROM [60081_LiftOvercheck_Results]
WHERE PDATE IN (SELECT MAX(PDate) FROM Euro6.dbo.[60081_LiftOvercheck_Results] Group By Nozzle_Matrix)
) as LiftOvercheck
on MatchGrind.Nozzle_Matrix = LiftOvercheck.Nozzle_Matrix

LEFT
JOIN (
SELECT [Pdate] as [NeedleInFlow PDate]
	  ,Serial as [Nozzle Matrix]
      ,[VolumeFlow] as [NeedleInFlow]
  FROM [Euro6].[dbo].[20422_Results]
  where PDate IN (SELECT MAX(PDate) From Euro6.dbo.[20422_Results] where type != 'Test_only' Group by serial, Date_Code, Component_Number
  )
) as NeedleInFlow
on MatchGrind.Nozzle_Matrix = NeedleInFlow.[Nozzle Matrix]

LEFT
JOIN (
SELECT * FROM 
(

SELECT [PDate] AS [PDate INO], CONVERT (VARCHAR, [Serial] COLLATE DATABASE_DEFAULT) AS [PG Matrix], [FM_Flow2] AS [INO Flow], CONVERT(VARCHAR, [AssetNumber]) AS [INO_PN]
FROM [Euro6].[dbo].[ComponentHegs] 
WHERE PDate IN ( SELECT MAX(PDate) FROM [Euro6].[dbo].[ComponentHegs] GROUP BY Serial )
union all
SELECT [Pdate] AS [PDate INO], CONVERT (VARCHAR, [Serial] COLLATE DATABASE_DEFAULT) AS [PG Matrix], [FM_Flow2] AS [INO Flow], CONVERT (VARCHAR, [PlantNumber]) AS [INO_PN]  
FROM [Euro6].[dbo].[ManualComponentHeg] 
WHERE PDate IN ( SELECT MAX(PDate) FROM [Euro6].[dbo].[ManualComponentHeg] GROUP BY Serial )
union all
SELECT [PDate] AS [PDate INO], CONVERT (VARCHAR, [Serial] COLLATE DATABASE_DEFAULT) AS [PG Matrix], [Flow_2] AS [INO Flow], CONVERT (VARCHAR, 20445) AS [INO_PN]  
FROM [Euro6].[dbo].[20445_SonplasHEG_Measure_Results] 
WHERE PDate IN ( SELECT MAX(PDate) FROM Euro6.dbo.[20445_SonplasHEG_Measure_Results] GROUP BY Serial ) 


  )
  AS U 
) as INOFlow
on MatchGrind.PG_Matrix = INOFlow.[PG Matrix] COLLATE SQL_Latin1_General_CP1_CI_AS

LEFT
JOIN (
SELECT [Serial]
	  ,Pdate
      ,[Flow_MeasuredFlow]
	  ,BackLeak_Leak
	  ,NOP_OpeningPressureSingleShot
	  ,Flow_Pressure
  FROM [Euro6].[dbo].[20474_SonplasNozzleTest_Results]
  where Pdate IN (SELECT MAX(PDate) FROM [Euro6].[dbo].[20474_SonplasNozzleTest_Results] Group by Serial)
  ) as NozzleTest
on MatchGrind.Nozzle_Matrix = NozzleTest.Serial

LEFT
JOIN (
SELECT [PDate]
      ,[Serial]
      ,[Flow]
  FROM [Euro6].[dbo].[20537_SonplasEDMHEG_Measure_Results]
  where PDate IN (SELECT MAX(PDate) FROM Euro6.dbo.[20537_SonplasEDMHEG_Measure_Results] Group By Serial)
) as NozzleEDMHEG
on MatchGrind.Nozzle_Matrix = NozzleEDMHEG.Serial

where MatchGrind.Nozzle_Matrix IS NOT NULL

  --and (LiftOverCheck.PDate > MatchGrind.PDate or LiftOverCheck.PDate IS NULL)
  --and (NeedleInFlow.[NeedleInFlow PDate] > MatchGrind.PDate or NeedleInFlow.[NeedleInFlow PDate] IS NULL)
  --and (INOFlow.[PDate INO] > MatchGrind.PDate OR INOFlow.[PDate INO] IS NULL)
  --and (NozzleTest.Pdate > MatchGrind.PDate OR NozzleTest.Pdate IS NULL)


)
