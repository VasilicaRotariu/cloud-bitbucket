﻿-- =============================================
-- Author:		James Lee
-- Create date: 2013-08-15
-- Description:	Interpolate fueling for given NCV logic
-- =============================================
CREATE FUNCTION [dbo].[GetInterpolatedFuel] 
(
	-- Add the parameters for the function here
	@TestNo int,
	@NCVLogic float,
	@SetRP int
)
RETURNS float
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Delivery float


	-- Add the T-SQL statements to compute the return value here
	
	--SET @Delivery = 234
	
	
	-- Get the test data for the testno, and setRP.
	DECLARE @TestData table (SetRP int, NCVDuration float, Untrimmed_Delivery float)
	
	INSERT INTO @TestData
	SELECT 
		RawTestData.SetRP,
		RawTestData.NCVDuration,
		RawTestData.Delivery as Untrimmed_Delivery
	--FROM Euro6.dbo.TestData RawTestData  /* USE THIS FOR TESTING OF PREVIOUS DATA */
	FROM Euro6.dbo.TestData_Pre RawTestData
	where TestNo = @TestNo
	
	-- Find the upper NCV logic and fuel above request.
	DECLARE @UpperNCVDuration float, @UpperFuelDelivery float
	
	SELECT TOP 1 @UpperNCVDuration = NCVDuration, @UpperFuelDelivery = Untrimmed_Delivery
	FROM @TestData WHERE NCVDuration >= @NCVLogic and SetRP = @SetRP Order By NCVDuration asc
	
	-- Find the lower NCV logic and fuel below request.
	DECLARE @LowerNCVDuration float, @LowerFuelDelivery float
	
	SELECT TOP 1 @LowerNCVDuration = NCVDuration, @LowerFuelDelivery = Untrimmed_Delivery
	FROM @TestData WHERE NCVDuration < @NCVLogic and SetRP = @SetRP Order By NCVDuration desc
	
	
	-- Calculate the fuel delivery between the 2 points
	
	/*
	   Injector Trimmed NCV - Lower NCV
	(  --------------------------------  )  *  (UpperFuel - LowerFuel) + LowerFuel
	   Upper NCV       -      Lower NCV
	
	*/
	
	IF @UpperFuelDelivery IS NOT NULL
	BEGIN
	
	SET @Delivery =
	
	( ( (@NCVLogic - @LowerNCVDuration)/(@UpperNCVDuration - @LowerNCVDuration) ) 
									* ( @UpperFuelDelivery - @LowerFuelDelivery ) + @LowerFuelDelivery )
	END
	
	IF @UpperFuelDelivery IS NULL -- Then you need to extrapolate further...
	BEGIN
	-- Need to place the lower in the higher, and find the new lower.
	SET @UpperFuelDelivery = @LowerFuelDelivery
	SET @UpperNCVDuration = @LowerNCVDuration
	
	SELECT TOP 1 @LowerNCVDuration = NCVDuration, @LowerFuelDelivery = Untrimmed_Delivery
	FROM @TestData WHERE NCVDuration < @NCVLogic and NCVDuration != @LowerNCVDuration Order By NCVDuration desc
	
	-- Now need to extrapolate further out... 
	
	/*
	   Injector Trimmed NCV - Lower NCV
	(  --------------------------------  )  *  (UpperFuel - LowerFuel) + LowerFuel
	   Upper NCV       -      Lower NCV
	
	*/
	
	SET @Delivery =
	
	( ( (@NCVLogic - @LowerNCVDuration)/(@UpperNCVDuration - @LowerNCVDuration) ) 
									* ( @UpperFuelDelivery - @LowerFuelDelivery ) + @LowerFuelDelivery )
	
	END
	
	
	IF @LowerFuelDelivery IS NULL
	BEGIN
	SET @Delivery = 0
	END
	

	-- Return the result of the function
	RETURN @Delivery

END
