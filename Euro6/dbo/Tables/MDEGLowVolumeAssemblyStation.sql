﻿CREATE TABLE [dbo].[MDEGLowVolumeAssemblyStation] (
    [ID]                   INT           IDENTITY (-2147483648, 1) NOT NULL,
    [TDate]                DATETIME2 (7) NOT NULL,
    [InjectorSerialNumber] VARCHAR (12)  NOT NULL,
    [Result]               INT           NOT NULL,
    [Torque1]              FLOAT (53)    NOT NULL,
    [Angle1]               FLOAT (53)    NOT NULL,
    [Torque2]              FLOAT (53)    NOT NULL,
    [Angle2]               FLOAT (53)    NOT NULL,
    [Leak1]                FLOAT (53)    NOT NULL,
    [Leak2]                FLOAT (53)    NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

