﻿CREATE TABLE [dbo].[tmp_AssemblyArchiveData] (
    [Uid]                     INT          IDENTITY (1, 1) NOT NULL,
    [PistonGuidePartNumber]   VARCHAR (10) NOT NULL,
    [NozzlePartNumber]        VARCHAR (10) NOT NULL,
    [PistonGuideCodeDate]     DATETIME     NOT NULL,
    [NozzleCodeDate]          DATETIME     NOT NULL,
    [PistonGuideSerial_Date]  VARCHAR (12) NOT NULL,
    [PistonGuideSerialNumber] VARCHAR (12) NOT NULL,
    [PistonGuideDateCode]     VARCHAR (12) NOT NULL,
    [SPOPreHone]              FLOAT (53)   NOT NULL,
    [SPOPostHone]             FLOAT (53)   NOT NULL,
    [NozzleSerial_Date]       VARCHAR (12) NOT NULL,
    [NozzleSerial]            VARCHAR (12) NOT NULL,
    [NozzleDateCode]          VARCHAR (4)  NOT NULL,
    [INOFlow]                 FLOAT (53)   NOT NULL,
    [NozzleLift]              FLOAT (53)   NOT NULL,
    [PGClearance]             FLOAT (53)   NOT NULL,
    [NeedleInFlow]            FLOAT (53)   NOT NULL,
    [NozzleBackLeak]          FLOAT (53)   NOT NULL
);

