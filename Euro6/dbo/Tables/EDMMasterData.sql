﻿CREATE TABLE [dbo].[EDMMasterData] (
    [PDate]      DATETIME      NULL,
    [TDate]      DATETIME2 (7) NULL,
    [MinFlow]    FLOAT (53)    NULL,
    [MaxFlow]    FLOAT (53)    NULL,
    [MinDeltaQ]  FLOAT (53)    NULL,
    [MaxDeltaQ]  FLOAT (53)    NULL,
    [NozzleType] VARCHAR (50)  NULL
);

