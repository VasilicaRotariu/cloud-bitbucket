﻿CREATE TABLE [dbo].[SA1_Pin_MeasureComplete] (
    [UID]         INT           NOT NULL,
    [UTC]         DATETIME2 (7) NOT NULL,
    [PinGUID]     VARCHAR (50)  NULL,
    [PassedGuide] BIT           NOT NULL,
    [PassedSeat]  BIT           NOT NULL,
    [DiameterTop] FLOAT (53)    NOT NULL,
    [DiameterBtm] FLOAT (53)    NOT NULL,
    [SeatLength]  FLOAT (53)    NOT NULL,
    CONSTRAINT [PK_Pin_MeasureComplete] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 90)
);

