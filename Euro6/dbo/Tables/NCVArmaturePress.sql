﻿CREATE TABLE [dbo].[NCVArmaturePress] (
    [PDate]                         DATETIME        NOT NULL,
    [IDate]                         DATETIME        DEFAULT (getdate()) NOT NULL,
    [PlantNumber]                   INT             NOT NULL,
    [Result]                        TINYINT         NOT NULL,
    [DataMatrix]                    VARCHAR (12)    NOT NULL,
    [DataMatrix_Result]             TINYINT         NOT NULL,
    [Grease_Result]                 TINYINT         NOT NULL,
    [Press_Result]                  TINYINT         NOT NULL,
    [Gauge_Result]                  TINYINT         NOT NULL,
    [FAG]                           FLOAT (53)      NOT NULL,
    [Tilt]                          FLOAT (53)      NOT NULL,
    [Force_Measurement_Distance]    FLOAT (53)      NOT NULL,
    [Force_at_Measurement_Position] FLOAT (53)      NOT NULL,
    [Minimum_Force]                 FLOAT (53)      NOT NULL,
    [Maximum_Force]                 FLOAT (53)      NOT NULL,
    [Piezo_Stack_Start_Position]    FLOAT (53)      NOT NULL,
    [Piezo_Stack_End_Position]      FLOAT (53)      NOT NULL,
    [Fixed_Adjustment_Slope]        FLOAT (53)      NOT NULL,
    [Fixed_Adjustment_Offset]       FLOAT (53)      NOT NULL,
    [Dynamic_Adjustment_Slope]      FLOAT (53)      NOT NULL,
    [Dynamic_Adjustment_Offset]     FLOAT (53)      NOT NULL,
    [Dynamic_Adjustment_Enabled]    TINYINT         NOT NULL,
    [Pin_Position_Change]           INT             NOT NULL,
    [Grease_OK_Area_Change]         BIGINT          NOT NULL,
    [Grease_NOK_Area_Change]        BIGINT          NOT NULL,
    [Press_Data]                    VARBINARY (MAX) NULL,
    [UID]                           DATETIME2 (7)   NOT NULL,
    [MatrixCount]                   INT             NULL,
    [Query_Successful]              TINYINT         NULL,
    [Data_Is_Valid]                 TINYINT         NULL,
    [dll_Adjustment_Value]          FLOAT (53)      NULL
);


GO
CREATE CLUSTERED INDEX [IX_NCVArmaturePress_PDateDESC]
    ON [dbo].[NCVArmaturePress]([PDate] DESC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [DataMatrixIndex]
    ON [dbo].[NCVArmaturePress]([DataMatrix] ASC)
    INCLUDE([PDate], [Result]) WITH (FILLFACTOR = 90);


GO

-- =============================================
-- Author:		Guillaume Carre
-- Create date: 31/07/2014
-- Description:	trigger that counts the number of time this component has been built 
--
-- UPDATES:
-- 2014-Sep-15	Steve Smith
--				Was called [dbo].[UpdateNCVArmaturePress], now called [dbo].[AfterInsertNCVArmaturePress]
--				Change to AFTER INSERT as to not interrupt insert speed for the client.
--				Removed use of CURSOR's in favour of WHILE loop.
--
-- =============================================
CREATE TRIGGER [dbo].[AfterInsertNCVArmaturePress] ON [dbo].[NCVArmaturePress] AFTER INSERT
AS
	BEGIN
		-- Don't show messages 
		SET NOCOUNT ON;
		-- Temporary table to hold records to update
		DECLARE @tbl TABLE([Row] INT, [PDate] DATETIME, [DataMatrix] VARCHAR(11));
		-- Insert records to update
		INSERT INTO @tbl
		SELECT TOP 500 ROW_NUMBER() OVER(ORDER BY [PDate]) AS [Row], [PDate], [DataMatrix] 
		FROM [dbo].[NCVArmaturePress] AS npf
		WHERE [MatrixCount] IS NULL
		ORDER BY [PDate] ASC;
		-- Temp variables for loop
		DECLARE @Row INT, @Rows INT, @DMCount INT, @DM VARCHAR(11), @PD DATETIME;
		-- Count rows inserted into temp table
		SELECT @Rows=@@ROWCOUNT;
		-- Start at first row
		SELECT @Row=1;
		-- Loop each row
		WHILE(@Row<=@Rows)
			BEGIN
				-- Grab Matrix & PDate for this loop          
				SELECT @DM=[DataMatrix], @PD=[PDate] FROM @tbl WHERE [Row]=@Row;
				-- Count instances for this matrix prior to this date
				SELECT @DMCount=ISNULL(COUNT(1),0)+1 FROM [dbo].[NCVArmaturePress] WHERE [DataMatrix]=@DM AND [PDate]<@PD;
				-- Update the specific record in the live table
				UPDATE [dbo].[NCVArmaturePress] SET [MatrixCount] = @DMCount WHERE [PDate]=@PD AND [DataMatrix]=@DM;
				-- Increment the loop count
				SET @Row = @Row + 1;
			END
		-- Show messages
		SET NOCOUNT OFF;
		
	END
