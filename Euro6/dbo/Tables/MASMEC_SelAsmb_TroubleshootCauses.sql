﻿CREATE TABLE [dbo].[MASMEC_SelAsmb_TroubleshootCauses] (
    [CauseID]  INT            NOT NULL,
    [FaultID]  INT            NOT NULL,
    [Cause]    VARCHAR (1000) NOT NULL,
    [Severity] INT            DEFAULT ((1)) NOT NULL,
    [InUse]    BIT            DEFAULT ((1)) NOT NULL,
    [Type]     TINYINT        DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_MASMEC_SelAsmb_troubleshootCauses] PRIMARY KEY CLUSTERED ([CauseID] ASC) WITH (FILLFACTOR = 80)
);

