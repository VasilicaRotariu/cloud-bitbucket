﻿CREATE TABLE [dbo].[InsulationTest] (
    [PDate]            DATETIME     NOT NULL,
    [PumpSerialNumber] VARCHAR (12) NOT NULL,
    [PlantNumber]      INT          NOT NULL,
    [Result]           INT          NOT NULL,
    [Res]              FLOAT (53)   NOT NULL,
    [Leak]             FLOAT (53)   NOT NULL,
    [Head]             INT          NOT NULL,
    [PartNo]           VARCHAR (10) NOT NULL,
    CONSTRAINT [PK_InsulationTest] PRIMARY KEY CLUSTERED ([PDate] ASC) WITH (FILLFACTOR = 80)
);

