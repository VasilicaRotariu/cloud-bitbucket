﻿CREATE TABLE [dbo].[ZBLN_Wet_Pallet_Data] (
    [UID]               BIGINT         IDENTITY (1, 1) NOT NULL,
    [Matrix]            TEXT           NOT NULL,
    [Pallet]            INT            NOT NULL,
    [Pass_Fail]         BIT            NOT NULL,
    [Nozzle_Int_Length] NUMERIC (8, 5) NOT NULL,
    [Nozzle_Int_Bore]   NUMERIC (8, 5) NOT NULL,
    [PGFace_to_Lift]    NUMERIC (8, 5) NOT NULL,
    [PGBore_Bottom]     NUMERIC (8, 5) NOT NULL,
    [PGBore_Middle]     NUMERIC (8, 5) NOT NULL,
    [PGBore_Top]        NUMERIC (8, 5) NOT NULL,
    [Spring_Length]     NUMERIC (8, 5) NOT NULL,
    [SS_Dimension]      NUMERIC (8, 5) NOT NULL,
    [SS_Force]          NUMERIC (3, 2) NOT NULL,
    [Avg_PG_Bore]       NUMERIC (8, 5) NOT NULL,
    [Req_Lift]          NUMERIC (8, 5) NOT NULL,
    [TDate]             DATETIME       NULL,
    CONSTRAINT [PK_tbl_Wet_Pallet_Data] PRIMARY KEY CLUSTERED ([Pallet] ASC) WITH (FILLFACTOR = 80)
);

