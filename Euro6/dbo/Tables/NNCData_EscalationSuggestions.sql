﻿CREATE TABLE [dbo].[NNCData_EscalationSuggestions] (
    [EscalationID] INT            NOT NULL,
    [Suggestion]   VARCHAR (1000) NOT NULL,
    [LDate]        DATETIME       NOT NULL,
    [ELOrder]      INT            NOT NULL,
    [HyperLink]    VARCHAR (200)  NULL,
    PRIMARY KEY CLUSTERED ([EscalationID] ASC, [ELOrder] ASC) WITH (FILLFACTOR = 80)
);

