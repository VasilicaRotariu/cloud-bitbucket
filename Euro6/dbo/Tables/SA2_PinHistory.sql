﻿CREATE TABLE [dbo].[SA2_PinHistory] (
    [PinHistory_UID]      INT             NOT NULL,
    [PinType_UID]         INT             NOT NULL,
    [Pin_UID]             INT             NOT NULL,
    [EventUTC]            DATETIME2 (7)   NOT NULL,
    [EventType]           INT             CONSTRAINT [DF_PinHistory_EventType] DEFAULT ((0)) NOT NULL,
    [PinState]            INT             NOT NULL,
    [PinFlag]             INT             NOT NULL,
    [RemovedByFinalise]   BIT             CONSTRAINT [DF_PinHistory_RemovedByFinalise1] DEFAULT ((0)) NOT NULL,
    [PinGuideDiameterTop] DECIMAL (12, 8) NULL,
    [PinGuideDiameterBtm] DECIMAL (12, 8) NULL,
    [PinSeatLength]       DECIMAL (12, 8) NULL,
    [PinCollarDiameter]   DECIMAL (12, 8) NULL,
    [Area]                INT             NULL,
    [CIndex]              INT             NULL,
    [RIndex]              INT             NULL,
    CONSTRAINT [PK_SA2_PinHistory] PRIMARY KEY CLUSTERED ([PinHistory_UID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_SA2_PinHistory_MissingIndex2]
    ON [dbo].[SA2_PinHistory]([EventUTC] ASC)
    INCLUDE([EventType], [Pin_UID], [PinState]);


GO
CREATE NONCLUSTERED INDEX [IX_SA2_PinHistory_MissingIndex]
    ON [dbo].[SA2_PinHistory]([PinState] ASC, [EventUTC] ASC)
    INCLUDE([Pin_UID]);

