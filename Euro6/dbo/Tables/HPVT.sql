﻿CREATE TABLE [dbo].[HPVT] (
    [PDate]        DATETIME     NOT NULL,
    [SerialNumber] VARCHAR (11) NOT NULL,
    [PartNumber]   VARCHAR (10) NOT NULL,
    [Result]       INT          NOT NULL,
    [Temp]         FLOAT (53)   NULL,
    [Flow]         FLOAT (53)   NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_HPVT]
    ON [dbo].[HPVT]([SerialNumber] DESC);

