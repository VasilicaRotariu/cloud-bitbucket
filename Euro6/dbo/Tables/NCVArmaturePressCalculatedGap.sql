﻿CREATE TABLE [dbo].[NCVArmaturePressCalculatedGap] (
    [UID]         INT           IDENTITY (1, 1) NOT NULL,
    [Matrix]      VARCHAR (50)  NOT NULL,
    [DateCreated] DATETIME2 (7) NOT NULL,
    [DateUpdated] DATETIME2 (7) NOT NULL,
    [Result]      FLOAT (53)    NOT NULL,
    CONSTRAINT [PK_NCVArmaturePressCalculatedGap] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [NCVArmaturePressCalculatedGap_U1]
    ON [dbo].[NCVArmaturePressCalculatedGap]([Matrix] ASC, [DateCreated] ASC)
    INCLUDE([Result]);

