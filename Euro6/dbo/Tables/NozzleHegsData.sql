﻿CREATE TABLE [dbo].[NozzleHegsData] (
    [TDate]                DATETIME2 (7) NOT NULL,
    [PDate]                DATETIME      NOT NULL,
    [PlantNumber]          INT           NOT NULL,
    [SerialNum]            VARCHAR (50)  NOT NULL,
    [Product]              VARCHAR (50)  NULL,
    [Type]                 VARCHAR (50)  NULL,
    [FailCode]             INT           NULL,
    [FromWhichPosalux]     INT           NULL,
    [FromWhichPosaluxHead] INT           NULL,
    [PM_Pressure]          FLOAT (53)    NULL,
    [PM_Flow]              FLOAT (53)    NULL,
    [PM_Density]           FLOAT (53)    NULL,
    [PM_UsedDensity]       FLOAT (53)    NULL,
    [PM_Temperature]       FLOAT (53)    NULL,
    [PM_Pressure1BP]       FLOAT (53)    NULL,
    [PM_Pressure2BP]       FLOAT (53)    NULL,
    [PM_FlowBP]            FLOAT (53)    NULL,
    [PM_DeltaQ]            FLOAT (53)    NULL,
    [PM_DensityBP]         FLOAT (53)    NULL,
    [PM_UsedDensityBP]     FLOAT (53)    NULL,
    [PM_TemperatureBP]     FLOAT (53)    NULL,
    [PM_MeasureTime]       FLOAT (53)    NULL,
    [PM_CycleTime]         FLOAT (53)    NULL,
    [PM_NestNr]            INT           NULL,
    [G_Pressure]           FLOAT (53)    NULL,
    [G_FlowStart]          FLOAT (53)    NULL,
    [G_FlowStop]           FLOAT (53)    NULL,
    [G_Temperature]        FLOAT (53)    NULL,
    [G_Density]            FLOAT (53)    NULL,
    [G_UsedDensity]        FLOAT (53)    NULL,
    [G_GrindTime]          FLOAT (53)    NULL,
    [G_CycleTime]          FLOAT (53)    NULL,
    [G_NestNr]             INT           NULL,
    [G_RegulationOn]       BIT           NULL,
    [G_UseGrindTimeCorr]   BIT           NULL,
    [G_UseControlledCD]    BIT           NULL,
    [G_UseDeltaQ]          BIT           NULL,
    [USED_FACTOR1]         FLOAT (53)    NULL,
    [USED_FACTOR2]         FLOAT (53)    NULL,
    [G_Error]              INT           NULL,
    [FM_Pressure]          FLOAT (53)    NULL,
    [FM_Flow]              FLOAT (53)    NULL,
    [FM_Density]           FLOAT (53)    NULL,
    [FM_UsedDensity]       FLOAT (53)    NULL,
    [FM_Temperature]       FLOAT (53)    NULL,
    [FM_Pressure1BP]       FLOAT (53)    NULL,
    [FM_Pressure2BP]       FLOAT (53)    NULL,
    [FM_FlowBP]            FLOAT (53)    NULL,
    [FM_DeltaQ]            FLOAT (53)    NULL,
    [FM_DensityBP]         FLOAT (53)    NULL,
    [FM_UsedDensityBP]     FLOAT (53)    NULL,
    [FM_TemperatureBP]     FLOAT (53)    NULL,
    [FM_MeasureTime]       FLOAT (53)    NULL,
    [FM_CycleTime]         FLOAT (53)    NULL,
    [FM_NestNr]            INT           NULL,
    [IDate]                DATETIME      CONSTRAINT [DF_NozzleHegsData_IDate] DEFAULT (getdate()) NOT NULL,
    [SerialCount]          INT           NULL,
    [tr]                   TINYINT       NULL,
    CONSTRAINT [PK_NozzleHegs] PRIMARY KEY CLUSTERED ([TDate] ASC) WITH (FILLFACTOR = 80)
);


GO

-- =============================================
-- Author:		Guillaume Carre 
-- Create date: 23/09/2014
-- Description:	trigger counting the number of time this component has been built 
-- =============================================
CREATE TRIGGER [dbo].[AfterInsertNozzleHegsData] ON [dbo].[NozzleHegsData] AFTER INSERT
AS
    BEGIN
        -- Don't show messages 
        SET NOCOUNT ON;
        -- Temporary table to hold records to update
        DECLARE @tbl TABLE([Row] INT, [PDate] DATETIME, [SerialNum] VARCHAR(max));
        -- Insert records to update
        INSERT INTO @tbl
        SELECT TOP 10 ROW_NUMBER() OVER(ORDER BY [PDate]) AS [Row], [PDate], [SerialNum] 
        FROM [dbo].[NozzleHegsData] AS npf
        WHERE [SerialCount] IS NULL
        ORDER BY [PDate] ASC;
        -- Temp variables for loop
        DECLARE @Row INT, @Rows INT, @DMCount INT, @DM VARCHAR(max), @PD DATETIME;
        -- Count rows inserted into temp table
        SELECT @Rows=@@ROWCOUNT;
        -- Start at first row
        SELECT @Row=1;
        -- Loop each row
        WHILE(@Row<=@Rows)
            BEGIN
                -- Grab Matrix & PDate for this loop          
                SELECT @DM=[SerialNum], @PD=[PDate] FROM @tbl WHERE [Row]=@Row;
                -- Count instances for this matrix prior to this date
                SELECT @DMCount=ISNULL(COUNT(1),0)+1 FROM [dbo].[NozzleHegsData] WHERE [SerialNum]=@DM AND [PDate]<@PD;
                -- Update the specific record in the live table
                UPDATE [dbo].[NozzleHegsData] SET [SerialCount] = @DMCount WHERE [PDate]=@PD AND [SerialNum]=@DM;
                -- Increment the loop count
                SET @Row = @Row + 1;
            END
        -- Show messages
        SET NOCOUNT OFF;

    END


GO
DISABLE TRIGGER [dbo].[AfterInsertNozzleHegsData]
    ON [dbo].[NozzleHegsData];

