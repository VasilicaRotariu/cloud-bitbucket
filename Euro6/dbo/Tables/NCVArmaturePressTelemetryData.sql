﻿CREATE TABLE [dbo].[NCVArmaturePressTelemetryData] (
    [UID]          BIGINT   IDENTITY (1, 1) NOT NULL,
    [Data_Start]   DATETIME NOT NULL,
    [Data_End]     DATETIME NULL,
    [StatusCode]   INT      NULL,
    [FaultCode]    INT      NULL,
    [Warning_Code] INT      NULL,
    CONSTRAINT [PK_TelemetryData] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 80)
);

