﻿CREATE TABLE [dbo].[ActF2NCVPinBatch] (
    [PlantNumber]                            SMALLINT      NULL,
    [Data_Matrix]                            VARCHAR (13)  NULL,
    [Fail_Code_DMC]                          TINYINT       NULL,
    [Pdate]                                  DATETIME2 (7) NOT NULL,
    [Variant_Code]                           TINYINT       NULL,
    [Fail_Code_VC]                           TINYINT       NULL,
    [Pin_Orientation]                        TINYINT       NULL,
    [Fail_Code_Pin]                          TINYINT       NULL,
    [Measure_Name_1]                         VARCHAR (30)  NULL,
    [Result_1]                               FLOAT (53)    NULL,
    [Nominal_1]                              FLOAT (53)    NULL,
    [UTL_1]                                  FLOAT (53)    NULL,
    [LTL_1]                                  FLOAT (53)    NULL,
    [Fail_Code_1]                            TINYINT       NULL,
    [Measure_Name_2]                         VARCHAR (30)  NULL,
    [Result_2]                               FLOAT (53)    NULL,
    [Nominal_2]                              FLOAT (53)    NULL,
    [UTL_2]                                  FLOAT (53)    NULL,
    [LTL_2]                                  FLOAT (53)    NULL,
    [Fail_Code_2]                            TINYINT       NULL,
    [Measure_Name_3]                         VARCHAR (30)  NULL,
    [Result_3]                               FLOAT (53)    NULL,
    [Nominal_3]                              FLOAT (53)    NULL,
    [UTL_3]                                  FLOAT (53)    NULL,
    [LTL_3]                                  FLOAT (53)    NULL,
    [Fail_Code_3]                            TINYINT       NULL,
    [Measure_Name_4]                         VARCHAR (30)  NULL,
    [Result_4]                               FLOAT (53)    NULL,
    [Nominal_4]                              TINYINT       NULL,
    [UTL_4]                                  FLOAT (53)    NULL,
    [LTL_4]                                  FLOAT (53)    NULL,
    [Fail_Code_4]                            TINYINT       NULL,
    [Measure_Name_5]                         VARCHAR (30)  NULL,
    [Result_5]                               FLOAT (53)    NULL,
    [Nominal_5]                              FLOAT (53)    NULL,
    [UTL_5]                                  FLOAT (53)    NULL,
    [LTL_5]                                  FLOAT (53)    NULL,
    [Fail_Code_5]                            TINYINT       NULL,
    [Measure_Name_6]                         VARCHAR (30)  NULL,
    [Result_6]                               FLOAT (53)    NULL,
    [Nominal_6]                              FLOAT (53)    NULL,
    [UTL_6]                                  FLOAT (53)    NULL,
    [LTL_6]                                  FLOAT (53)    NULL,
    [Fail_Code_6]                            TINYINT       NULL,
    [Final_Fail_Code]                        TINYINT       NULL,
    [Empty_out_signal]                       TINYINT       NULL,
    [coolant_inlet_temp_low_pres]            FLOAT (53)    NULL,
    [coolant_inlet_temp_high_pres]           FLOAT (53)    NULL,
    [reference_signal]                       TINYINT       NULL,
    [calibration_signal]                     TINYINT       NULL,
    [Wheel_dress_signal_external]            TINYINT       NULL,
    [Wheel_dress_signal_internal]            TINYINT       NULL,
    [Tool_change_external]                   TINYINT       NULL,
    [Tool_change_internal]                   TINYINT       NULL,
    [Target_Value_Stem]                      FLOAT (53)    NULL,
    [Target_Value_Seat_To_Seat]              FLOAT (53)    NULL,
    [Target_Value_Collar_Offset]             FLOAT (53)    NULL,
    [manual_correction_stem_dia_T]           FLOAT (53)    NULL,
    [manual_correction_seat_to_seat]         FLOAT (53)    NULL,
    [manual_correction_overall_length]       FLOAT (53)    NULL,
    [manual_correction_seat_dia]             FLOAT (53)    NULL,
    [manual_correction_dia_R]                FLOAT (53)    NULL,
    [manual_correction_R_length]             FLOAT (53)    NULL,
    [manual_correction_overall_length_rough] FLOAT (53)    NULL,
    [manual_correction_over_centre]          FLOAT (53)    NULL,
    [manual_correction_taper]                FLOAT (53)    NULL,
    [correction_value_stem_dia_T]            FLOAT (53)    NULL,
    [correction_value_seat]                  FLOAT (53)    NULL,
    [correction_value_taper]                 FLOAT (53)    NULL,
    [auto_correction_stem_dia_T]             FLOAT (53)    NULL,
    [auto_correction_seat_to_seat]           FLOAT (53)    NULL,
    [auto_correction_taper]                  FLOAT (53)    NULL,
    [IDate]                                  DATETIME      CONSTRAINT [DF__ActF2NCVP__IDate__047AA831] DEFAULT (sysdatetime()) NULL
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170117-122956]
    ON [dbo].[ActF2NCVPinBatch]([PlantNumber] ASC, [Pdate] ASC)
    INCLUDE([IDate], [Result_1], [Result_2], [Result_4], [Result_5]);

