﻿CREATE TABLE [dbo].[Test_Nominals_M6_C6] (
    [Uid]         INT        IDENTITY (1, 1) NOT NULL,
    [ValidFrom]   DATETIME   NOT NULL,
    [SetRP]       INT        NOT NULL,
    [NCVDuration] INT        NOT NULL,
    [Delivery]    FLOAT (53) NOT NULL,
    [TestPlan]    INT        NULL
);

