﻿CREATE TABLE [dbo].[MASMEC_SelAsmb_OP20C_Results] (
    [Tdate]             DATETIME     DEFAULT (getdate()) NOT NULL,
    [Idate]             DATETIME     NOT NULL,
    [Pdate]             DATETIME     NULL,
    [Serial]            VARCHAR (12) NULL,
    [LOP]               VARCHAR (20) NOT NULL,
    [Rework_Version]    INT          NULL,
    [Result]            INT          NULL,
    [Fail_UID]          INT          NULL,
    [PlantNo]           INT          NULL,
    [Operator_ID]       VARCHAR (10) NULL,
    [Operator_FullName] VARCHAR (50) NULL,
    [Injector_Type]     INT          NULL,
    [ShiftNumber]       INT          NULL,
    [Line_Id]           INT          NULL,
    [UID]               BIGINT       NULL,
    CONSTRAINT [PK_MASMEC_SelAsmb_OP20OUT_Results] PRIMARY KEY CLUSTERED ([Idate] DESC) WITH (FILLFACTOR = 80),
    CONSTRAINT [fk_MASMEC_SelAsmb_OP20C_Results_ProcessTable] FOREIGN KEY ([Serial]) REFERENCES [dbo].[MASMEC_SelAsmb_ProcessTable] ([Serial]) ON DELETE CASCADE ON UPDATE CASCADE
);

