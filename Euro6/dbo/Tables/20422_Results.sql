﻿CREATE TABLE [dbo].[20422_Results] (
    [uid]                 INT           IDENTITY (1, 1) NOT NULL,
    [Pdate]               DATETIME      NOT NULL,
    [M_Date]              DATE          NULL,
    [M_Time]              TIME (7)      NULL,
    [Type]                VARCHAR (50)  NULL,
    [OrderID]             VARCHAR (50)  NULL,
    [Component_Number]    VARCHAR (50)  NULL,
    [Date_Code]           VARCHAR (20)  NULL,
    [Serial]              VARCHAR (20)  NULL,
    [Upstream_Pressure]   FLOAT (53)    NULL,
    [DownStream_Pressure] FLOAT (53)    NULL,
    [Startflow_KG]        FLOAT (53)    NULL,
    [Startflow_ml]        FLOAT (53)    NULL,
    [Massflow]            FLOAT (53)    NULL,
    [VolumeFlow]          FLOAT (53)    NULL,
    [Density]             FLOAT (53)    NULL,
    [Test_Temp]           FLOAT (53)    NULL,
    [Waveness]            FLOAT (53)    NULL,
    [Std_Deviation]       VARCHAR (50)  NULL,
    [Time_1]              TIME (7)      NULL,
    [CTP]                 FLOAT (53)    NULL,
    [ControllerError]     FLOAT (53)    NULL,
    [TestPlan]            VARCHAR (100) NULL
);


GO
CREATE CLUSTERED INDEX [IX_20422_Results]
    ON [dbo].[20422_Results]([Pdate] DESC, [Serial] ASC);

