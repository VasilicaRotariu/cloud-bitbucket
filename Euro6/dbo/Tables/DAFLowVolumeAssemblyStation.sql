﻿CREATE TABLE [dbo].[DAFLowVolumeAssemblyStation] (
    [ID]                   INT           IDENTITY (-2147483648, 1) NOT NULL,
    [TDate]                DATETIME2 (7) NOT NULL,
    [InjectorSerialNumber] VARCHAR (12)  NOT NULL,
    [Result]               INT           NOT NULL,
    [Torque1]              FLOAT (53)    NOT NULL,
    [Angle1]               FLOAT (53)    NOT NULL,
    [Torque2]              FLOAT (53)    NOT NULL,
    [Angle2]               FLOAT (53)    NOT NULL,
    [Leak1]                FLOAT (53)    NOT NULL,
    [Leak2]                FLOAT (53)    NOT NULL,
    [Hostname]             VARCHAR (25)  NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);


GO
--	=============================================
--
--	Author		:	<Author,,Name>
--	Create date	:	<Create Date,,>
--	Description	:	<Description,,>
--
--	=============================================
CREATE TRIGGER [dbo].[DAFLowVolumeAssemblyStationOnINSERT] ON [dbo].[DAFLowVolumeAssemblyStation] AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	DECLARE @tbl TABLE([row] INT IDENTITY(1,1) NOT NULL, [TDate] DATETIME2(7));
	INSERT INTO @tbl([TDate]) SELECT [TDate] FROM INSERTED;
	DECLARE @row INT = 1, @rows INT = @@ROWCOUNT, @tDate DATETIME2(7);
	
	WHILE(@row <= @rows)
	BEGIN
		SELECT @tDate = [TDate] FROM @tbl WHERE [row] = @row;
		UPDATE [dbo].[DAFLowVolumeAssemblyStation] SET [Hostname] = LEFT(HOST_NAME(), 25) WHERE [TDate] = @tDate;
		SET @row += 1;
	END

END
