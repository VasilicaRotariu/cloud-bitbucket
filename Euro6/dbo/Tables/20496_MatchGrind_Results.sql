﻿CREATE TABLE [dbo].[20496_MatchGrind_Results] (
    [Uid]           INT          IDENTITY (1, 1) NOT NULL,
    [PDate]         DATETIME     NOT NULL,
    [Nozzle_Matrix] VARCHAR (25) NOT NULL,
    [PG_Matrix]     VARCHAR (25) NOT NULL,
    [PGBore]        FLOAT (53)   NULL,
    [Taper]         FLOAT (53)   NULL,
    [Clearance]     FLOAT (53)   NULL,
    [Lift]          FLOAT (53)   NULL,
    [Force]         FLOAT (53)   NULL
);

