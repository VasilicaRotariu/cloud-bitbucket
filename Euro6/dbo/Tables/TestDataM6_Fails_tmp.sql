﻿CREATE TABLE [dbo].[TestDataM6_Fails_tmp] (
    [Uid]       BIGINT       IDENTITY (1, 1) NOT NULL,
    [SessionID] INT          NOT NULL,
    [Duration]  VARCHAR (13) NOT NULL,
    [Fails]     INT          NOT NULL
);

