﻿CREATE TABLE [dbo].[SelAssy_MasterMeasurementFeatureTypes] (
    [SelAssy_MasterMeasurementFeatureType_UID] INT          IDENTITY (1, 1) NOT NULL,
    [Feature]                                  VARCHAR (50) NOT NULL,
    [SPCFeatureDescriptionAlias]               VARCHAR (50) NULL,
    CONSTRAINT [PK_SelAssy_MasterMeasurementFeatureTypes] PRIMARY KEY CLUSTERED ([SelAssy_MasterMeasurementFeatureType_UID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SelAssy_MasterMeasurementFeatureTypes_U1]
    ON [dbo].[SelAssy_MasterMeasurementFeatureTypes]([Feature] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[U1]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SelAssy_MasterMeasurementFeatureTypes', @level2type = N'COLUMN', @level2name = N'Feature';

