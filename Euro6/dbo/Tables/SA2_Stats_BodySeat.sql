﻿CREATE TABLE [dbo].[SA2_Stats_BodySeat] (
    [BodySeatStatUID]             INT             IDENTITY (1, 1) NOT NULL,
    [BodySeatStatUTC]             DATETIME2 (7)   NOT NULL,
    [BodyUID]                     INT             NOT NULL,
    [BodyMatrixCode]              VARCHAR (50)    NOT NULL,
    [BodyType]                    VARCHAR (50)    NOT NULL,
    [BodySeatMeasCount]           INT             NOT NULL,
    [BodySeatLength]              DECIMAL (12, 8) NOT NULL,
    [BodySeatLengthUTL]           DECIMAL (12, 8) NOT NULL,
    [BodySeatLengthLTL]           DECIMAL (12, 8) NOT NULL,
    [BodySeatLengthDetermination] INT             NOT NULL,
    CONSTRAINT [PK_Stats_BodySeat] PRIMARY KEY CLUSTERED ([BodySeatStatUID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_BodySeat_Det]
    ON [dbo].[SA2_Stats_BodySeat]([BodySeatLengthDetermination] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_BodySeat_UTC]
    ON [dbo].[SA2_Stats_BodySeat]([BodySeatStatUTC] ASC) WITH (FILLFACTOR = 90);

