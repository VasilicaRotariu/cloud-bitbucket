﻿CREATE TABLE [dbo].[NCVArmaturePressCalculatedGapAndTilt] (
    [UID]         INT           IDENTITY (1, 1) NOT NULL,
    [Matrix]      VARCHAR (50)  NOT NULL,
    [DateCreated] DATETIME2 (7) NOT NULL,
    [DateUpdated] DATETIME2 (7) NOT NULL,
    [Gap]         FLOAT (53)    NOT NULL,
    [Tilt]        FLOAT (53)    NOT NULL,
    CONSTRAINT [PK_NCVArmaturePressCalculatedGapAndTilt] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [NonClusteredIndex-20160926-160916]
    ON [dbo].[NCVArmaturePressCalculatedGapAndTilt]([Matrix] ASC, [DateCreated] ASC);

