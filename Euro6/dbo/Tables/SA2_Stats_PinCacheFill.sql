﻿CREATE TABLE [dbo].[SA2_Stats_PinCacheFill] (
    [PinCacheFillStatUID]    INT             IDENTITY (1, 1) NOT NULL,
    [PinCacheFillStatUTC]    DATETIME2 (7)   NOT NULL,
    [PinUID]                 INT             NOT NULL,
    [PinGUID]                VARCHAR (50)    NOT NULL,
    [PinType]                VARCHAR (50)    NOT NULL,
    [Grade]                  VARCHAR (50)    NOT NULL,
    [SubGrade]               VARCHAR (50)    NOT NULL,
    [GradeExtended]          VARCHAR (50)    NOT NULL,
    [GradeMax]               DECIMAL (12, 8) NOT NULL,
    [GradeCount]             INT             NOT NULL,
    [GradeDetermination]     INT             NULL,
    [SubGradeMax]            DECIMAL (12, 8) NOT NULL,
    [SubGradeCount]          INT             NOT NULL,
    [SubGradeDetermination]  INT             NULL,
    [CacheFillDetermination] INT             NOT NULL,
    CONSTRAINT [PK_Stats_PinCacheFill] PRIMARY KEY CLUSTERED ([PinCacheFillStatUID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_PinCacheFill_UTC]
    ON [dbo].[SA2_Stats_PinCacheFill]([PinCacheFillStatUTC] ASC) WITH (FILLFACTOR = 90);

