﻿CREATE TABLE [dbo].[NNCData_Tolerances] (
    [ComponentType]         VARCHAR (50) NOT NULL,
    [MachineID]             INT          NOT NULL,
    [DataColumn]            VARCHAR (50) NOT NULL,
    [Min]                   FLOAT (53)   NULL,
    [Max]                   FLOAT (53)   NULL,
    [Mean]                  FLOAT (53)   NULL,
    [EL_ToleranceAmplitude] FLOAT (53)   NULL,
    [EL_PromptQuantity]     INT          CONSTRAINT [defaultValue] DEFAULT ((1)) NULL,
    [EL_CheckTime]          INT          CONSTRAINT [defaultValueCheckTime] DEFAULT ((30)) NULL,
    [CycleTime]             INT          NULL
);

