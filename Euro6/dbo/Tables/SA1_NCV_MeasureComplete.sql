﻿CREATE TABLE [dbo].[SA1_NCV_MeasureComplete] (
    [UID]         INT           NOT NULL,
    [UTC]         DATETIME2 (7) NOT NULL,
    [Matrix]      VARCHAR (50)  NOT NULL,
    [PassedGuide] BIT           NOT NULL,
    [PassedSeat]  BIT           NOT NULL,
    [DiameterTop] FLOAT (53)    NOT NULL,
    [DiameterBtm] FLOAT (53)    NOT NULL,
    [SeatLength]  FLOAT (53)    NOT NULL,
    CONSTRAINT [PK_NCV_MeasureComplete] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 90)
);

