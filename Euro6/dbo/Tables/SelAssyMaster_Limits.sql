﻿CREATE TABLE [dbo].[SelAssyMaster_Limits] (
    [ProductType]   INT          NOT NULL,
    [MasterType]    VARCHAR (10) NOT NULL,
    [ComponentType] VARCHAR (20) NOT NULL,
    [ResultType]    VARCHAR (50) NOT NULL,
    [UpperLimit]    FLOAT (53)   NOT NULL,
    [LowerLimit]    FLOAT (53)   NOT NULL,
    CONSTRAINT [PK_SelAssyMaster_Limits] PRIMARY KEY CLUSTERED ([ProductType] ASC, [MasterType] ASC, [ComponentType] ASC, [ResultType] ASC) WITH (FILLFACTOR = 80)
);

