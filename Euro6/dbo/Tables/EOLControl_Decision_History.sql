﻿CREATE TABLE [dbo].[EOLControl_Decision_History] (
    [ComponentType] VARCHAR (8)   NOT NULL,
    [NozzleID]      VARCHAR (16)  NOT NULL,
    [PistonGuideID] VARCHAR (16)  NOT NULL,
    [Date]          DATETIME      NOT NULL,
    [Decision]      VARCHAR (4)   NOT NULL,
    [Error]         VARCHAR (MAX) NULL,
    [SAPSerial]     VARCHAR (10)  NULL
);

