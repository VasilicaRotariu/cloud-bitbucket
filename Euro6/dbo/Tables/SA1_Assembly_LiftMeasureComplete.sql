﻿CREATE TABLE [dbo].[SA1_Assembly_LiftMeasureComplete] (
    [UID]     INT           NOT NULL,
    [UTC]     DATETIME2 (7) NOT NULL,
    [Matrix]  VARCHAR (50)  NOT NULL,
    [PinGUID] VARCHAR (50)  NULL,
    [Passed]  BIT           NOT NULL,
    [Lift]    FLOAT (53)    NOT NULL,
    CONSTRAINT [PK_Assembly_LiftMeasureComplete] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 90)
);

