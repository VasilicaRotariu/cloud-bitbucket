﻿CREATE TABLE [dbo].[SA2_StatsLimits] (
    [StatLimitUID]                    INT           IDENTITY (1, 1) NOT NULL,
    [StatLimitChangeUTC]              DATETIME2 (7) CONSTRAINT [DF_SA2_StatsLimits_StatLimitChangeUTC] DEFAULT (sysutcdatetime()) NOT NULL,
    [Pin_Onload_Target]               INT           NOT NULL,
    [Pin_Measurement_Target]          INT           NOT NULL,
    [Pin_IntoCache_Target]            INT           NOT NULL,
    [Body_Onload_Target]              INT           NOT NULL,
    [Body_Measurement_Target]         INT           NOT NULL,
    [Body_MatchPin_Target]            INT           NOT NULL,
    [Assembly_Insertion_Target]       INT           NOT NULL,
    [Assembly_SeatLift_Target]        INT           NOT NULL,
    [Assembly_GuideClearance_Target]  INT           NOT NULL,
    [Assembly_TopSeatLeak_Target]     INT           NOT NULL,
    [Assembly_BtmSeatLeak_Target]     INT           NOT NULL,
    [Assembly_CollarClearance_Target] INT           NOT NULL,
    [Assembly_Offload_Target]         INT           NOT NULL,
    CONSTRAINT [PK_SA2_StatsLimits_1] PRIMARY KEY CLUSTERED ([StatLimitUID] ASC) WITH (FILLFACTOR = 80)
);

