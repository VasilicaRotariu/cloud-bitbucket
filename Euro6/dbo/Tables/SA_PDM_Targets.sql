﻿CREATE TABLE [dbo].[SA_PDM_Targets] (
    [Date]   DATE    NOT NULL,
    [Shift]  TINYINT NOT NULL,
    [Target] INT     CONSTRAINT [DF_SA_PDM_Targets_Target] DEFAULT ((900)) NOT NULL,
    CONSTRAINT [PK_SAP_PDM_Targets] PRIMARY KEY CLUSTERED ([Date] ASC, [Shift] ASC) WITH (FILLFACTOR = 80)
);

