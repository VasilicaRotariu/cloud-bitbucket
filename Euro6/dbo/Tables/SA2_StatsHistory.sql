﻿CREATE TABLE [dbo].[SA2_StatsHistory] (
    [SA2_StatsHistoryUID] INT           IDENTITY (1, 1) NOT NULL,
    [DateStart]           DATETIME2 (7) NOT NULL,
    [DateEnd]             DATETIME2 (7) NOT NULL,
    [Interval]            INT           CONSTRAINT [DF_SA2_StatsHistory_Interval] DEFAULT ((60)) NOT NULL,
    [PinOnload_Count]     INT           CONSTRAINT [DF_SA2_StatsHistory_PinOnload_Count] DEFAULT ((0)) NOT NULL,
    [PinOnload_Fail]      INT           CONSTRAINT [DF_SA2_StatsHistory_PinOnload_Fail] DEFAULT ((0)) NOT NULL,
    [PinGuide_Count]      INT           NOT NULL,
    [PinGuide_Fail]       INT           NOT NULL,
    [PinGuideTaper_Fail]  INT           CONSTRAINT [DF_SA2_StatsHistory_PinGuideTaper_Fail] DEFAULT ((0)) NOT NULL,
    [PinSeat_Count]       INT           NOT NULL,
    [PinSeat_Fail]        INT           NOT NULL,
    [PinCollar_Count]     INT           NOT NULL,
    [PinCollar_Fail]      INT           NOT NULL,
    [PinCacheFill_Count]  INT           NOT NULL,
    [PinCacheFill_Fail]   INT           NOT NULL,
    [BodyOnload_Count]    INT           CONSTRAINT [DF_SA2_StatsHistory_PinOnload_Count1] DEFAULT ((0)) NOT NULL,
    [BodyOnload_Fail]     INT           CONSTRAINT [DF_SA2_StatsHistory_PinOnload_Fail1] DEFAULT ((0)) NOT NULL,
    [BodyGuide_Count]     INT           NOT NULL,
    [BodyGuide_Fail]      INT           NOT NULL,
    [BodyGuideTaper_Fail] INT           CONSTRAINT [DF_SA2_StatsHistory_BodyGuideTaper_Fail] DEFAULT ((0)) NOT NULL,
    [BodySeat_Count]      INT           NOT NULL,
    [BodySeat_Fail]       INT           NOT NULL,
    [BodyCollar_Count]    INT           NOT NULL,
    [BodyCollar_Fail]     INT           NOT NULL,
    [BodyMatchPin_Count]  INT           NOT NULL,
    [BodyMatchPin_Fail]   INT           NOT NULL,
    [AssyInsertion_Count] INT           NOT NULL,
    [AssyInsertion_Fail]  INT           NOT NULL,
    [AssyLift_Count]      INT           NOT NULL,
    [AssyLift_Fail]       INT           NOT NULL,
    [AssyGuide_Count]     INT           NOT NULL,
    [AssyGuide_Fail]      INT           NOT NULL,
    [AssyBtmSeat_Count]   INT           NOT NULL,
    [AssyBtmSeat_Fail]    INT           NOT NULL,
    [AssyTopSeat_Count]   INT           NOT NULL,
    [AssyTopSeat_Fail]    INT           NOT NULL,
    [AssyCollar_Count]    INT           NOT NULL,
    [AssyCollar_Fail]     INT           NOT NULL,
    [BodyFinalise_Count]  INT           NOT NULL,
    [BodyFinalise_Fail]   INT           NOT NULL,
    CONSTRAINT [PK_SA2_StatsHistory] PRIMARY KEY CLUSTERED ([SA2_StatsHistoryUID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170117-152228]
    ON [dbo].[SA2_StatsHistory]([DateStart] ASC, [Interval] ASC)
    INCLUDE([AssyBtmSeat_Count], [AssyBtmSeat_Fail], [AssyCollar_Count], [AssyCollar_Fail], [AssyGuide_Count], [AssyGuide_Fail], [AssyInsertion_Count], [AssyInsertion_Fail], [AssyLift_Count], [AssyLift_Fail], [AssyTopSeat_Count], [AssyTopSeat_Fail], [BodyCollar_Count], [BodyCollar_Fail], [BodyFinalise_Count], [BodyFinalise_Fail], [BodyGuide_Count], [BodyGuide_Fail], [BodyGuideTaper_Fail], [BodyMatchPin_Count], [BodyMatchPin_Fail], [BodyOnload_Count], [BodyOnload_Fail], [BodySeat_Count], [BodySeat_Fail], [DateEnd], [PinCacheFill_Count], [PinCacheFill_Fail], [PinCollar_Count], [PinCollar_Fail], [PinGuide_Count], [PinGuide_Fail], [PinGuideTaper_Fail], [PinOnload_Count], [PinOnload_Fail], [PinSeat_Count], [PinSeat_Fail], [SA2_StatsHistoryUID]);

