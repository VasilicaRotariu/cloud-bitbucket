﻿CREATE TABLE [dbo].[MASMEC_SelAsmb_OP20B_Telemetry] (
    [cnt]          BIGINT   NOT NULL,
    [Data_Start]   DATETIME NOT NULL,
    [Data_End]     DATETIME NOT NULL,
    [Status_Code]  INT      NOT NULL,
    [Fault_Code]   INT      NOT NULL,
    [Tdate]        DATETIME DEFAULT (getdate()) NOT NULL,
    [Warning_Code] INT      NOT NULL,
    CONSTRAINT [PK_MASMEC_SelAsmb_OP20B_Telemetry] PRIMARY KEY CLUSTERED ([cnt] ASC) WITH (FILLFACTOR = 80)
);

