﻿CREATE TABLE [dbo].[OldNozzleHegsGrind] (
    [PDate]                  DATETIME      NOT NULL,
    [PlantNumber]            INT           NOT NULL,
    [SerialNum]              VARCHAR (50)  NOT NULL,
    [G_FlowStart]            FLOAT (53)    NULL,
    [G_FlowStop]             FLOAT (53)    NULL,
    [G_Pressure]             FLOAT (53)    NULL,
    [G_GrindTime]            FLOAT (53)    NULL,
    [G_Temperature]          FLOAT (53)    NULL,
    [G_UsedDensity]          FLOAT (53)    NULL,
    [G_MeasuredDensity]      FLOAT (53)    NULL,
    [G_FactorMeasureToGrind] FLOAT (53)    NULL,
    [G_FlowCorrectionFactor] FLOAT (53)    NULL,
    [G_TargetGrinding]       FLOAT (53)    NULL,
    [NestNr]                 INT           NULL,
    [G_Error]                INT           NULL,
    [TDate]                  DATETIME2 (7) NOT NULL,
    CONSTRAINT [PK__OldNozzl__3F6BDCDE1FCDBCEB] PRIMARY KEY CLUSTERED ([PlantNumber] ASC, [TDate] ASC) WITH (FILLFACTOR = 80)
);

