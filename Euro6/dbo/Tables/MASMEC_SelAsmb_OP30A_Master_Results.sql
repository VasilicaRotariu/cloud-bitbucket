﻿CREATE TABLE [dbo].[MASMEC_SelAsmb_OP30A_Master_Results] (
    [Tdate]                                      DATETIME     DEFAULT (getdate()) NOT NULL,
    [Idate]                                      DATETIME     NOT NULL,
    [Pdate]                                      DATETIME     NULL,
    [Serial]                                     VARCHAR (12) NOT NULL,
    [LOP]                                        VARCHAR (20) NOT NULL,
    [Rework_Version]                             INT          NULL,
    [Result]                                     INT          NOT NULL,
    [PlantNo]                                    INT          NULL,
    [Injector_Type]                              INT          NULL,
    [MasterType]                                 INT          NULL,
    [Target_Orientation_Angle]                   FLOAT (53)   NULL,
    [Tollerance_Angle_After_Screw]               FLOAT (53)   NULL,
    [Orientation_Angle_After_Screw_MeasureValue] FLOAT (53)   NULL,
    [ShiftNumber]                                INT          NULL,
    [UID]                                        BIGINT       NULL,
    CONSTRAINT [PK_MASMEC_SelAsmb_OP30A_Master_Results] PRIMARY KEY CLUSTERED ([Idate] DESC) WITH (FILLFACTOR = 80)
);

