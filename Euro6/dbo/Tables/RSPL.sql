﻿CREATE TABLE [dbo].[RSPL] (
    [PDate]                        DATETIME      NOT NULL,
    [PlantNumber]                  SMALLINT      NOT NULL,
    [Assembly_Modality_Code]       VARCHAR (50)  NOT NULL,
    [Injector_ID]                  VARCHAR (10)  NOT NULL,
    [TEST_NUMBER]                  SMALLINT      NOT NULL,
    [LineID]                       SMALLINT      NOT NULL,
    [InjectorCode]                 VARCHAR (10)  NOT NULL,
    [TAG_Code]                     VARCHAR (10)  NULL,
    [NCV_DataMatrix]               VARCHAR (20)  NULL,
    [UserName]                     VARCHAR (10)  NULL,
    [InDate]                       DATETIME      NULL,
    [OutDate]                      DATETIME      NULL,
    [GLOBAL_RESULT]                SMALLINT      NULL,
    [Result_Load]                  SMALLINT      NULL,
    [PreLoadValue]                 REAL          NULL,
    [PreLoadError]                 REAL          NULL,
    [SetPoint_Load]                REAL          NULL,
    [Result_SHIM]                  SMALLINT      NULL,
    [SHIM_Standard]                REAL          NULL,
    [SHIM_Measured]                REAL          NULL,
    [SHIM_Required]                REAL          NULL,
    [Spring_Result]                SMALLINT      NULL,
    [Spring_Min]                   REAL          NULL,
    [Spring_Max]                   REAL          NULL,
    [Spring_Value]                 REAL          NULL,
    [PIN_Lenght_Result]            SMALLINT      NULL,
    [PIN_Lenght_Min]               SMALLINT      NULL,
    [PIN_Lenght_Max]               SMALLINT      NULL,
    [PIN_Lenght_Value]             REAL          NULL,
    [Result_Stroke]                SMALLINT      NULL,
    [Stroke]                       REAL          NULL,
    [RESISTANCE_Result]            SMALLINT      NULL,
    [RESISTANCE_Min]               REAL          NULL,
    [RESISTANCE_Max]               REAL          NULL,
    [RESISTANCE_Measure]           REAL          NULL,
    [ShiftNumber]                  SMALLINT      NULL,
    [Result_Load_Ver]              SMALLINT      NULL,
    [PreLoadValue_Ver]             REAL          NULL,
    [PreLoadError_Ver]             REAL          NULL,
    [Result_Stroke_Ver]            SMALLINT      NULL,
    [Stroke_Ver]                   REAL          NULL,
    [TEST_MODALITY]                SMALLINT      NULL,
    [SHIM_Modality]                SMALLINT      NULL,
    [SHIM_Verify]                  BIT           NULL,
    [Final_Verify]                 SMALLINT      NULL,
    [Final_Verify_Frequency]       SMALLINT      NULL,
    [Spring_Rate]                  REAL          NULL,
    [Spring_Rate_Min]              REAL          NULL,
    [Spring_Rate_Max]              REAL          NULL,
    [Spring_Rate_Result]           SMALLINT      NULL,
    [HysteresisMeasure]            REAL          NULL,
    [HysteresisMeasureResult]      SMALLINT      NULL,
    [HysteresisVerification]       REAL          NULL,
    [HysteresisVerificationResult] SMALLINT      NULL,
    [VER_SPRINGRATE]               REAL          NULL,
    [VER_SPRINGRATE_RESULT]        SMALLINT      NULL,
    [OMV_Min]                      INT           NULL,
    [OMV_Max]                      INT           NULL,
    [OMV_Value]                    INT           NULL,
    [NCV_DataMatrixCount]          BIGINT        CONSTRAINT [DF_RSPL_NCV_DataMatrixCount] DEFAULT ((0)) NULL,
    [TDate]                        DATETIME2 (7) CONSTRAINT [DF_RSPL_TDate] DEFAULT (getdate()) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_RSPL_1]
    ON [dbo].[RSPL]([Injector_ID] DESC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_RSPL]
    ON [dbo].[RSPL]([Injector_ID] DESC, [NCV_DataMatrix] DESC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [<Name of Missing Index, sysname,>]
    ON [dbo].[RSPL]([NCV_DataMatrix] ASC)
    INCLUDE([Injector_ID]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_GLOBAL_RESULT_PDATE]
    ON [dbo].[RSPL]([GLOBAL_RESULT] ASC, [PDate] ASC)
    INCLUDE([Injector_ID], [LineID], [InjectorCode], [PreLoadValue], [SHIM_Measured]);


GO
CREATE NONCLUSTERED INDEX [IX_PDATE]
    ON [dbo].[RSPL]([PDate] ASC)
    INCLUDE([Injector_ID], [NCV_DataMatrix]);


GO
CREATE TRIGGER [dbo].[UpdateRSPLComponentTotals]
   ON  [dbo].[RSPL]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
        -- Temporary table to hold records to update
        DECLARE @tbl TABLE([Row] INT, [PDate] DATETIME, [NCV_DataMatrix] VARCHAR(max));
        -- Insert records to update
        INSERT INTO @tbl
        SELECT TOP 5 ROW_NUMBER() OVER(ORDER BY [PDate]) AS [Row], [PDate], [NCV_DataMatrix] 
        FROM [dbo].[RSPL] AS npf
        WHERE [NCV_DataMatrixCount] IS NULL or [NCV_DataMatrixCount] = 0
        ORDER BY [PDate] ASC;
        -- Temp variables for loop
        DECLARE @Row INT, @Rows INT, @DMCount INT, @DM VARCHAR(max), @PD DATETIME;
        -- Count rows inserted into temp table
        SELECT @Rows=@@ROWCOUNT;
        -- Start at first row
        SELECT @Row=1;
        -- Loop each row
        WHILE(@Row<=@Rows)
            BEGIN
                -- Grab Matrix & PDate for this loop          
                SELECT @DM=[NCV_DataMatrix], @PD=[PDate] FROM @tbl WHERE [Row]=@Row;

                -- Count instances for this matrix prior to this date
                SELECT @DMCount=ISNULL(COUNT(1),0)+1 
				from(
					SELECT PDate, NCV_DataMatrix
					FROM [Euro6].[dbo].[RSPL]
					WHERE NCV_DataMatrix = @DM AND [PDate]<@PD
					union
					SELECT PDate, NCVSerialNumber
					FROM [Euro6].[dbo].[MDEGLowVolumeLineResults] 
					WHERE NCVSerialNumber = @DM AND [PDate]<@PD) a
                -- Update the specific record in the live table
                UPDATE [dbo].[RSPL] SET [NCV_DataMatrixCount] = @DMCount WHERE [PDate]=@PD AND [NCV_DataMatrix]=@DM;
                -- Increment the loop count
                SET @Row = @Row + 1;
            END
        -- Show messages
        SET NOCOUNT OFF;
END
