﻿CREATE TABLE [dbo].[NNCData_TPM] (
    [MachineID]      INT           NULL,
    [AssetNumber]    INT           NULL,
    [CounterName]    VARCHAR (50)  NULL,
    [CounterLimit]   INT           NULL,
    [CounterCurrent] INT           NULL,
    [SubAsset]       VARCHAR (255) NULL,
    [ID]             INT           IDENTITY (1, 1) NOT NULL
);

