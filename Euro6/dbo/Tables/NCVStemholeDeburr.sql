﻿CREATE TABLE [dbo].[NCVStemholeDeburr] (
    [Uid]           INT           NOT NULL,
    [PDate]         DATETIME      NOT NULL,
    [Serial]        VARCHAR (20)  NOT NULL,
    [OnloadTime]    DATETIME      NOT NULL,
    [GrindPressure] FLOAT (53)    NOT NULL,
    [Flow]          FLOAT (53)    NOT NULL,
    [GrindTime]     FLOAT (53)    NOT NULL,
    [Temp]          FLOAT (53)    NOT NULL,
    [CycleTime]     FLOAT (53)    NOT NULL,
    [Result]        INT           NOT NULL,
    [ErrorText]     VARCHAR (150) NOT NULL,
    CONSTRAINT [PK_NCVStemholeDeburr] PRIMARY KEY CLUSTERED ([Uid] ASC) WITH (FILLFACTOR = 80)
);

