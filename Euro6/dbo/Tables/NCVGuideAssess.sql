﻿CREATE TABLE [dbo].[NCVGuideAssess] (
    [PDate]          DATETIME     NOT NULL,
    [Serial]         VARCHAR (50) NOT NULL,
    [User]           VARCHAR (50) NOT NULL,
    [DiameterTop]    FLOAT (53)   NULL,
    [DiameterBottom] FLOAT (53)   NULL,
    [Taper]          FLOAT (53)   NULL,
    [SeatLength]     FLOAT (53)   NULL,
    [Grade]          VARCHAR (50) NOT NULL,
    [TargetSeat]     FLOAT (53)   NULL,
    [TargetDiameter] FLOAT (53)   NULL
);

