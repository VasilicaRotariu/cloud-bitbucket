﻿CREATE TABLE [dbo].[EOLControl_Decision] (
    [ComponentType] VARCHAR (50) NOT NULL,
    [NozzleID]      VARCHAR (30) NOT NULL,
    [PistonGuideID] VARCHAR (20) NOT NULL,
    [Date]          DATETIME     NOT NULL,
    [Decision]      BIT          NOT NULL
);

