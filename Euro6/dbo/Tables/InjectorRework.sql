﻿CREATE TABLE [dbo].[InjectorRework] (
    [Uid]               INT          IDENTITY (1, 1) NOT NULL,
    [PDate]             DATETIME     NOT NULL,
    [DirectionToRework] VARCHAR (3)  NOT NULL,
    [InjectorSerial]    VARCHAR (10) NOT NULL,
    [BuildNumber]       VARCHAR (2)  NOT NULL,
    [NCV_Matrix]        VARCHAR (11) NOT NULL,
    [Nozzle_Matrix]     VARCHAR (13) NOT NULL,
    [ArmatureRotated]   INT          NOT NULL,
    [NHMChanged]        INT          NOT NULL,
    [CapnutChanged]     INT          NOT NULL,
    [ReworkResultType]  INT          NOT NULL,
    [ReTorqued]         INT          NOT NULL
);

