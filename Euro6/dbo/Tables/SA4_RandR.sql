﻿CREATE TABLE [dbo].[SA4_RandR] (
    [UID]                   BIGINT        NOT NULL,
    [UTC]                   DATETIME      NOT NULL,
    [PlantNumber]           SMALLINT      NULL,
    [MasterType]            VARCHAR (10)  NULL,
    [ComponentType]         VARCHAR (20)  NULL,
    [ValueP.UpperStem]      FLOAT (53)    NULL,
    [ValueP.LowerStem]      FLOAT (53)    NULL,
    [ValueP.CollarOffset]   FLOAT (53)    NULL,
    [ValueP.SeatLenght]     FLOAT (53)    NULL,
    [ValueP.StemTaper]      FLOAT (53)    NULL,
    [ValueP.CollarDiameter] FLOAT (53)    NULL,
    [AreaTemperature]       FLOAT (53)    NULL,
    [TDate]                 DATETIME2 (7) NOT NULL,
    CONSTRAINT [PK_SA4_RandR] PRIMARY KEY CLUSTERED ([UTC] ASC) WITH (FILLFACTOR = 80)
);

