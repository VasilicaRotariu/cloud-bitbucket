﻿CREATE TABLE [dbo].[HighPressureDeburr] (
    [PDate]        DATETIME      NULL,
    [PlantNumber]  INT           NOT NULL,
    [SerialNumber] VARCHAR (13)  NOT NULL,
    [Result]       TINYINT       NOT NULL,
    [IDate]        DATETIME      DEFAULT (getdate()) NULL,
    [UID]          DATETIME2 (7) DEFAULT (sysdatetime()) NULL,
    [PresRange1]   INT           NULL,
    [PresMin1]     INT           NULL,
    [PresMax1]     INT           NULL,
    [PresRange2]   INT           NULL,
    [PresMin2]     INT           NULL,
    [PresMax2]     INT           NULL,
    [PresRange3]   INT           NULL,
    [PresMin3]     INT           NULL,
    [PresMax3]     INT           NULL
);

