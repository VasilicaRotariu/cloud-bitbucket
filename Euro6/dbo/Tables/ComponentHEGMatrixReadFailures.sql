﻿CREATE TABLE [dbo].[ComponentHEGMatrixReadFailures] (
    [PDate]         DATETIME      NOT NULL,
    [AssetNumber]   INT           NOT NULL,
    [ComponentType] VARCHAR (50)  NOT NULL,
    [TDate]         DATETIME2 (7) NOT NULL
);

