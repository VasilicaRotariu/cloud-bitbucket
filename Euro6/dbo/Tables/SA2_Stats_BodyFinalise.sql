﻿CREATE TABLE [dbo].[SA2_Stats_BodyFinalise] (
    [BodyFinaliseStatUID]       INT           IDENTITY (1, 1) NOT NULL,
    [BodyFinaliseStatUTC]       DATETIME2 (7) NOT NULL,
    [BodyUID]                   INT           NOT NULL,
    [BodyMatrixCode]            VARCHAR (50)  NOT NULL,
    [BodyType]                  VARCHAR (50)  NOT NULL,
    [AssemblyUID]               INT           NULL,
    [PinUID]                    INT           NULL,
    [BodyFinaliseDetermination] INT           NOT NULL,
    CONSTRAINT [PK_Stats_BodyFinalise] PRIMARY KEY CLUSTERED ([BodyFinaliseStatUID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_BodyFinalise_Det]
    ON [dbo].[SA2_Stats_BodyFinalise]([BodyFinaliseDetermination] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_BodyFinalise_UTC]
    ON [dbo].[SA2_Stats_BodyFinalise]([BodyFinaliseStatUTC] ASC) WITH (FILLFACTOR = 90);

