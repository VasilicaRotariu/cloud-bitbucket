﻿CREATE TABLE [dbo].[NNCAssembly] (
    [TDate]                        DATETIME2 (7) NOT NULL,
    [AssetNumber]                  INT           NOT NULL,
    [PDate]                        DATETIME      NOT NULL,
    [NozzleSerial]                 VARCHAR (25)  NOT NULL,
    [PistonGuideSerial]            VARCHAR (25)  NULL,
    [PistonGuide_InnerDiameterAvg] FLOAT (53)    NULL,
    [PistonGuide_Taper]            FLOAT (53)    NULL,
    [PistonGuide_Straightness]     FLOAT (53)    NULL,
    [PistonGuide_Height]           FLOAT (53)    NULL,
    [Spring_Length]                FLOAT (53)    NULL,
    [Spring_Force]                 FLOAT (53)    NULL,
    [Spring_Rate]                  FLOAT (53)    NULL,
    [Seat_FinalPosition]           FLOAT (53)    NULL,
    [Seat_FinalForce]              FLOAT (53)    NULL,
    [Seat_PostMeasuredPosition]    FLOAT (53)    NULL,
    [Seat_PostMeasuredForce]       FLOAT (53)    NULL,
    [Needle_OuterDiameterAvg]      FLOAT (53)    NULL,
    [Needle_Taper]                 FLOAT (53)    NULL,
    [Needle_Straightness]          FLOAT (53)    NULL,
    [Capsule_Clearance]            FLOAT (53)    NULL,
    [Capsule_NeedleLift]           FLOAT (53)    NULL,
    [Capsule_SpringForce]          FLOAT (53)    NULL,
    [Failcode]                     INT           NOT NULL,
    [IDate]                        DATETIME      CONSTRAINT [DF_NNCAssembly_IDate] DEFAULT (getdate()) NULL,
    [NozzleCount]                  INT           NULL,
    [PistonGuideCount]             INT           NULL,
    [PuckSerial]                   VARCHAR (10)  NULL,
    [LiftOffset]                   FLOAT (53)    NULL,
    CONSTRAINT [PK_NNCAssembly] PRIMARY KEY CLUSTERED ([TDate] DESC) WITH (FILLFACTOR = 80)
);




GO
CREATE NONCLUSTERED INDEX [NozzleSerialIndex]
    ON [dbo].[NNCAssembly]([NozzleSerial] ASC)
    INCLUDE([Failcode], [PDate]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_NNCAssembly_NozzleSerialASC]
    ON [dbo].[NNCAssembly]([NozzleSerial] ASC, [PDate] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_PistonGuideSerial_PistonGuideCount]
    ON [dbo].[NNCAssembly]([PistonGuideSerial] ASC, [PistonGuideCount] ASC) WITH (FILLFACTOR = 90);


GO


-- =============================================
-- Author:		Guillaume Carre
-- Create date: 07/08/2014
-- Description:	on insertion of a new row in this table count the iteration of the particular nozzle serial and piston guide serial already present in the table
-- =============================================
CREATE TRIGGER [dbo].[UpdateNNCAssemblyTable]
   ON  [dbo].[NNCAssembly]
   INSTEAD OF INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE		
				@CurrentNozzleTotal INT,
				@CurrentPGTotal INT,
				@NewNozzleTotal INT,
				@NewPGTotal INT
	DECLARE
				@TDate datetime2(7),
				@AssetNumber int,
				@PDate datetime, 
				@NozzleSerial varchar(25),
				@PistonGuideSerial varchar(25),
				@PistonGuide_InnerDiameterAvg float,
				@PistonGuide_Taper float,
				@PistonGuide_Straightness float,
				@PistonGuide_Height float,
				@Spring_Length float,
				@Spring_Force float,
				@Spring_Rate float,
				@Seat_FinalPosition float,
				@Seat_FinalForce float,
				@Seat_PostMeasuredPosition float,
				@Seat_PostMeasuredForce	float,
				@Needle_OuterDiameterAvg float,
				@Needle_Taper float,
				@Needle_Straightness float,
				@Capsule_Clearance float,
				@Capsule_NeedleLift	float,
				@Capsule_SpringForce float,
				@Failcode int,
				@IDate datetime,
				@PuckSerial varchar(10),
				@LiftOffset float

	DECLARE SerialRows CURSOR FOR 
	SELECT		[TDate]
				,[AssetNumber]
				,[PDate]
				,[NozzleSerial]
				,[PistonGuideSerial]
				,[PistonGuide_InnerDiameterAvg]
				,[PistonGuide_Taper]
				,[PistonGuide_Straightness]
				,[PistonGuide_Height]
				,[Spring_Length]
				,[Spring_Force]
				,[Spring_Rate]
				,[Seat_FinalPosition]
				,[Seat_FinalForce]
				,[Seat_PostMeasuredPosition]
				,[Seat_PostMeasuredForce]
				,[Needle_OuterDiameterAvg]
				,[Needle_Taper]
				,[Needle_Straightness]
				,[Capsule_Clearance]
				,[Capsule_NeedleLift]
				,[Capsule_SpringForce]
				,[Failcode]
				,[IDate]
				,[PuckSerial]
				,[LiftOffset]
	FROM INSERTED
	OPEN SerialRows
	WHILE(1=1)
	BEGIN
		SET @CurrentNozzleTotal = 0
		SET @CurrentPGTotal = 0

		FETCH NEXT FROM SerialRows INTO 
			@TDate, 
				@AssetNumber,
				@PDate, 
				@NozzleSerial,
				@PistonGuideSerial,
				@PistonGuide_InnerDiameterAvg,
				@PistonGuide_Taper,
				@PistonGuide_Straightness,
				@PistonGuide_Height,
				@Spring_Length,
				@Spring_Force,
				@Spring_Rate,
				@Seat_FinalPosition,
				@Seat_FinalForce,
				@Seat_PostMeasuredPosition,
				@Seat_PostMeasuredForce,
				@Needle_OuterDiameterAvg,
				@Needle_Taper,
				@Needle_Straightness,
				@Capsule_Clearance,
				@Capsule_NeedleLift,
				@Capsule_SpringForce,
				@Failcode,
				@IDate,
				@PuckSerial,
				@LiftOffset
		IF @@FETCH_STATUS <> 0
			BREAK
		SELECT @CurrentNozzleTotal = COUNT(*) FROM [NNCAssembly] WHERE [NozzleSerial] = @NozzleSerial
		SELECT @CurrentPGTotal = COUNT(*) FROM [NNCAssembly] WHERE [PistonGuideSerial] = @PistonGuideSerial

		SET @NewNozzleTotal = @CurrentNozzleTotal + 1
		SET @NewPGTotal = @CurrentPGTotal + 1

		INSERT INTO [NNCAssembly]([TDate]
				,[AssetNumber]
				,[PDate]
				,[NozzleSerial]
				,[PistonGuideSerial]
				,[PistonGuide_InnerDiameterAvg]
				,[PistonGuide_Taper]
				,[PistonGuide_Straightness]
				,[PistonGuide_Height]
				,[Spring_Length]
				,[Spring_Force]
				,[Spring_Rate]
				,[Seat_FinalPosition]
				,[Seat_FinalForce]
				,[Seat_PostMeasuredPosition]
				,[Seat_PostMeasuredForce]
				,[Needle_OuterDiameterAvg]
				,[Needle_Taper]
				,[Needle_Straightness]
				,[Capsule_Clearance]
				,[Capsule_NeedleLift]
				,[Capsule_SpringForce]
				,[Failcode]
				,[IDate]
				,[NozzleCount]
				,[PistonGuideCount]
				,[PuckSerial]
				,[LiftOffset])
			VALUES ( @TDate, 
				@AssetNumber,
				@PDate, 
				@NozzleSerial,
				@PistonGuideSerial,
				@PistonGuide_InnerDiameterAvg,
				@PistonGuide_Taper,
				@PistonGuide_Straightness,
				@PistonGuide_Height,
				@Spring_Length,
				@Spring_Force,
				@Spring_Rate,
				@Seat_FinalPosition,
				@Seat_FinalForce,
				@Seat_PostMeasuredPosition,
				@Seat_PostMeasuredForce,
				@Needle_OuterDiameterAvg,
				@Needle_Taper,
				@Needle_Straightness,
				@Capsule_Clearance,
				@Capsule_NeedleLift,
				@Capsule_SpringForce,
				@Failcode,
				@IDate,
				@NewNozzleTotal,
				@NewPGTotal,
				@PuckSerial,
				@LiftOffset)
	END
	CLOSE SerialRows
	DEALLOCATE SerialRows
END
