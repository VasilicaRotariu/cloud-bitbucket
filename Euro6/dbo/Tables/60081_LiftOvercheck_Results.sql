﻿CREATE TABLE [dbo].[60081_LiftOvercheck_Results] (
    [Uid]           INT          IDENTITY (1, 1) NOT NULL,
    [PDate]         DATETIME     NOT NULL,
    [Nozzle_Matrix] VARCHAR (13) NOT NULL,
    [SpringForce]   FLOAT (53)   NOT NULL,
    [LiftOvercheck] FLOAT (53)   NOT NULL
);


GO
CREATE CLUSTERED INDEX [IX_60081_LiftOvercheck_Results]
    ON [dbo].[60081_LiftOvercheck_Results]([PDate] DESC, [Nozzle_Matrix] ASC);

