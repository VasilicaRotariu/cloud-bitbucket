﻿CREATE TABLE [dbo].[SA2_PartTracking_PartLocationHistory] (
    [UID]              INT           IDENTITY (1, 1) NOT NULL,
    [UTC]              DATETIME2 (7) NOT NULL,
    [Robot]            INT           NOT NULL,
    [BodyUID]          INT           NULL,
    [PinUID]           INT           NULL,
    [LocationFrom_FID] INT           NOT NULL,
    [LocationTo_FID]   INT           NOT NULL,
    [ExtraInfo]        VARCHAR (250) NULL,
    CONSTRAINT [PK_PinTracking] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 80)
);

