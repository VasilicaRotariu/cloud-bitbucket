﻿CREATE TABLE [dbo].[NCVArmaturePressStatusCodes] (
    [ID]          INT           NOT NULL,
    [Description] VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_StatusCodes] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

