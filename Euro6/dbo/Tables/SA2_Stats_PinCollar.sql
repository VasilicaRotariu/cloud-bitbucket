﻿CREATE TABLE [dbo].[SA2_Stats_PinCollar] (
    [PinCollarStatUID]               INT             IDENTITY (1, 1) NOT NULL,
    [PinCollarStatUTC]               DATETIME2 (7)   NOT NULL,
    [PinUID]                         INT             NOT NULL,
    [PinGUID]                        VARCHAR (50)    NOT NULL,
    [PinType]                        VARCHAR (50)    NOT NULL,
    [PinCollarMeasCount]             INT             NOT NULL,
    [PinCollarDiameter]              DECIMAL (12, 8) NOT NULL,
    [PinCollarDiameterUTL]           DECIMAL (12, 8) NULL,
    [PinCollarDiameterLTL]           DECIMAL (12, 8) NULL,
    [PinCollarDiameterDetermination] INT             NULL,
    CONSTRAINT [PK_Stats_PinCollar] PRIMARY KEY CLUSTERED ([PinCollarStatUID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_PinCollar_Det]
    ON [dbo].[SA2_Stats_PinCollar]([PinCollarDiameterDetermination] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PinCollar_UTC]
    ON [dbo].[SA2_Stats_PinCollar]([PinCollarStatUTC] ASC) WITH (FILLFACTOR = 90);

