﻿CREATE TABLE [dbo].[ComponentHegs] (
    [TDate]            DATETIME2 (7) NOT NULL,
    [AssetNumber]      INT           NOT NULL,
    [PDate]            DATETIME      NOT NULL,
    [Serial]           VARCHAR (20)  NOT NULL,
    [FailCode]         VARCHAR (50)  NOT NULL,
    [FailClass]        VARCHAR (40)  NOT NULL,
    [Nest]             INT           NOT NULL,
    [PM_Flow]          FLOAT (53)    NULL,
    [PM_Flow2]         FLOAT (53)    NULL,
    [PM_DeltaQ]        FLOAT (53)    NULL,
    [PM_Temp]          FLOAT (53)    NULL,
    [PM_Temp2]         FLOAT (53)    NULL,
    [PM_Pressure]      FLOAT (53)    NULL,
    [PM_BackPressure]  FLOAT (53)    NULL,
    [PM_Pressure2]     FLOAT (53)    NULL,
    [PM_BackPressure2] FLOAT (53)    NULL,
    [PM_FlowRev]       FLOAT (53)    NULL,
    [PM_Flow2Rev]      FLOAT (53)    NULL,
    [PM_Offset]        FLOAT (53)    NULL,
    [PM_Offset2]       FLOAT (53)    NULL,
    [G_Pressure]       FLOAT (53)    NULL,
    [G_Time]           FLOAT (53)    NULL,
    [G_Temp]           FLOAT (53)    NULL,
    [G_Density]        FLOAT (53)    NULL,
    [G_ErrorCode]      FLOAT (53)    NULL,
    [G_Factor]         FLOAT (53)    NULL,
    [G_StopFlow]       FLOAT (53)    NULL,
    [G_Flow]           FLOAT (53)    NULL,
    [G_StartFlow]      FLOAT (53)    NULL,
    [FM_Flow]          FLOAT (53)    NULL,
    [FM_Flow2]         FLOAT (53)    NULL,
    [FM_DeltaQ]        FLOAT (53)    NULL,
    [FM_Temp]          FLOAT (53)    NULL,
    [FM_Temp2]         FLOAT (53)    NULL,
    [FM_Pressure]      FLOAT (53)    NULL,
    [FM_BackPressure]  FLOAT (53)    NULL,
    [FM_Pressure2]     FLOAT (53)    NULL,
    [FM_BackPressure2] FLOAT (53)    NULL,
    [FM_FlowRev]       FLOAT (53)    NULL,
    [FM_Flow2Rev]      FLOAT (53)    NULL,
    [FM_Offset]        FLOAT (53)    NULL,
    [FM_Offset2]       FLOAT (53)    NULL,
    [IDate]            DATETIME      CONSTRAINT [DF_ComponentHegs_IDate] DEFAULT (getdate()) NOT NULL,
    [SerialCount]      INT           NULL
);


GO
CREATE UNIQUE CLUSTERED INDEX [ClusteredIndex-20150730-155104]
    ON [dbo].[ComponentHegs]([TDate] ASC, [AssetNumber] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150730-160153]
    ON [dbo].[ComponentHegs]([Serial] ASC)
    INCLUDE([FailCode], [PDate], [TDate]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150730-122520]
    ON [dbo].[ComponentHegs]([TDate] ASC, [AssetNumber] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150730-153226]
    ON [dbo].[ComponentHegs]([TDate] ASC, [PDate] ASC, [Serial] ASC) WITH (FILLFACTOR = 90);


GO
-- =============================================
-- Author:		Guillaume Carre
-- Create date: 31/07/2014
-- Description:	trigger that counts the number of time this component has been built 
-- =============================================

CREATE TRIGGER [dbo].[UpdateComponentHegs]
   ON  [dbo].[ComponentHegs]
   INSTEAD OF INSERT

AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE		
				@CurrentTotal INT,
				@NewTotal INT,
				@TDate datetime2(7),
				@AssetNumber int,
				@PDate datetime,
				@Serial	varchar(20),
				@FailCode varchar(50),
				@FailClass varchar(40), 
				@Nest int,
				@PM_Flow float,
				@PM_Flow2 float,
				@PM_DeltaQ float,
				@PM_Temp float,
				@PM_Temp2 float,
				@PM_Pressure float,
				@PM_BackPressure float,
				@PM_Pressure2 float,
				@PM_BackPressure2 float,
				@PM_FlowRev float,
				@PM_Flow2Rev float,
				@PM_Offset float,
				@PM_Offset2	float,
				@G_Pressure	float,
				@G_Time	float,
				@G_Temp	float,
				@G_Density float,
				@G_ErrorCode float,
				@G_Factor float,
				@G_StopFlow	float,
				@G_Flow	float,
				@G_StartFlow float,
				@FM_Flow float,
				@FM_Flow2 float,
				@FM_DeltaQ float,
				@FM_Temp float,
				@FM_Temp2 float,
				@FM_Pressure float,
				@FM_BackPressure float,
				@FM_Pressure2 float,
				@FM_BackPressure2 float,
				@FM_FlowRev	float,
				@FM_Flow2Rev float,
				@FM_Offset float,
				@FM_Offset2	float,
				@IDate datetime

    DECLARE CountRows CURSOR FOR
	SELECT
		TDate,
		AssetNumber,
		PDate,
		Serial,
		FailCode,
		FailClass,
		Nest,
		PM_Flow,
		PM_Flow2,
		PM_DeltaQ,
		PM_Temp,
		PM_Temp2,
		PM_Pressure,
		PM_BackPressure,
		PM_Pressure2,
		PM_BackPressure2,
		PM_FlowRev,
		PM_Flow2Rev,
		PM_Offset,
		PM_Offset2,
		G_Pressure,
		G_Time,
		G_Temp,
		G_Density,
		G_ErrorCode,
		G_Factor,
		G_StopFlow,
		G_Flow,
		G_StartFlow,
		FM_Flow,
		FM_Flow2,
		FM_DeltaQ,
		FM_Temp,
		FM_Temp2,
		FM_Pressure,
		FM_BackPressure,
		FM_Pressure2,
		FM_BackPressure2,
		FM_FlowRev,
		FM_Flow2Rev,
		FM_Offset,
		FM_Offset2,
		IDate
	FROM INSERTED

	OPEN CountRows
	WHILE(1=1)
	BEGIN
		SET @CurrentTotal=0
		FETCH NEXT FROM CountRows INTO 
			@TDate,
			@AssetNumber,
			@PDate,
			@Serial,
			@FailCode,
			@FailClass, 
			@Nest,
			@PM_Flow,
			@PM_Flow2,
			@PM_DeltaQ,
			@PM_Temp,
			@PM_Temp2,
			@PM_Pressure,
			@PM_BackPressure,
			@PM_Pressure2,
			@PM_BackPressure2,
			@PM_FlowRev,
			@PM_Flow2Rev,
			@PM_Offset,
			@PM_Offset2,
			@G_Pressure,
			@G_Time,
			@G_Temp,
			@G_Density,
			@G_ErrorCode,
			@G_Factor,
			@G_StopFlow,
			@G_Flow,
			@G_StartFlow,
			@FM_Flow,
			@FM_Flow2,
			@FM_DeltaQ,
			@FM_Temp,
			@FM_Temp2,
			@FM_Pressure,
			@FM_BackPressure,
			@FM_Pressure2,
			@FM_BackPressure2,
			@FM_FlowRev,
			@FM_Flow2Rev,
			@FM_Offset,
			@FM_Offset2,
			@IDate
		IF @@FETCH_STATUS<>0
			BREAK

		SELECT @CurrentTotal = COUNT(*) FROM [ComponentHegs] WHERE Serial = @Serial

		IF (@CurrentTotal IS NULL)
		BEGIN
			SET @CurrentTotal = 0;
		END

		SET @NewTotal = @CurrentTotal + 1
		INSERT INTO [ComponentHegs]
		(
			TDate,
		AssetNumber,
		PDate,
		Serial,
		FailCode,
		FailClass,
		Nest,
		PM_Flow,
		PM_Flow2,
		PM_DeltaQ,
		PM_Temp,
		PM_Temp2,
		PM_Pressure,
		PM_BackPressure,
		PM_Pressure2,
		PM_BackPressure2,
		PM_FlowRev,
		PM_Flow2Rev,
		PM_Offset,
		PM_Offset2,
		G_Pressure,
		G_Time,
		G_Temp,
		G_Density,
		G_ErrorCode,
		G_Factor,
		G_StopFlow,
		G_Flow,
		G_StartFlow,
		FM_Flow,
		FM_Flow2,
		FM_DeltaQ,
		FM_Temp,
		FM_Temp2,
		FM_Pressure,
		FM_BackPressure,
		FM_Pressure2,
		FM_BackPressure2,
		FM_FlowRev,
		FM_Flow2Rev,
		FM_Offset,
		FM_Offset2,
		IDate,
		SerialCount
		)
		VALUES
		(
			@TDate,
			@AssetNumber,
			@PDate,
			@Serial,
			@FailCode,
			@FailClass, 
			@Nest,
			@PM_Flow,
			@PM_Flow2,
			@PM_DeltaQ,
			@PM_Temp,
			@PM_Temp2,
			@PM_Pressure,
			@PM_BackPressure,
			@PM_Pressure2,
			@PM_BackPressure2,
			@PM_FlowRev,
			@PM_Flow2Rev,
			@PM_Offset,
			@PM_Offset2,
			@G_Pressure,
			@G_Time,
			@G_Temp,
			@G_Density,
			@G_ErrorCode,
			@G_Factor,
			@G_StopFlow,
			@G_Flow,
			@G_StartFlow,
			@FM_Flow,
			@FM_Flow2,
			@FM_DeltaQ,
			@FM_Temp,
			@FM_Temp2,
			@FM_Pressure,
			@FM_BackPressure,
			@FM_Pressure2,
			@FM_BackPressure2,
			@FM_FlowRev,
			@FM_Flow2Rev,
			@FM_Offset,
			@FM_Offset2,
			@IDate,
			@NewTotal
		)
		END
		CLOSE CountRows
		DEALLOCATE CountRows
END

GO
DISABLE TRIGGER [dbo].[UpdateComponentHegs]
    ON [dbo].[ComponentHegs];

