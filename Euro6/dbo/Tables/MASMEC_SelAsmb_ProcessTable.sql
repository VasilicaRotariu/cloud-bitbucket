﻿CREATE TABLE [dbo].[MASMEC_SelAsmb_ProcessTable] (
    [Tdate]          DATETIME     DEFAULT (getdate()) NOT NULL,
    [Idate]          DATETIME     NOT NULL,
    [Pdate]          DATETIME     NOT NULL,
    [Serial]         VARCHAR (12) NOT NULL,
    [LOP]            VARCHAR (20) DEFAULT ((0)) NOT NULL,
    [Last_Station]   INT          NULL,
    [Last_Job]       INT          NULL,
    [Result]         INT          NOT NULL,
    [Fail_UID]       INT          DEFAULT ((0)) NULL,
    [Rework_Version] INT          NOT NULL,
    [Next_Station]   INT          NULL,
    [Complete_Date]  DATETIME     NULL,
    [UID]            BIGINT       NULL,
    CONSTRAINT [PK_ProcessTable] PRIMARY KEY CLUSTERED ([Serial] DESC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_MASMEC_SelAsmb_ProcessTable] FOREIGN KEY ([Serial]) REFERENCES [dbo].[MASMEC_SelAsmb_SerialNumbers] ([SerialNumber]) ON DELETE CASCADE ON UPDATE CASCADE
);

