﻿CREATE TABLE [dbo].[SA2_Stats_AssemblyGuideClearance2] (
    [AssemblyGuideClearance2StatUID]       INT             IDENTITY (1, 1) NOT NULL,
    [AssemblyGuideClearance2StatUTC]       DATETIME2 (7)   NOT NULL,
    [AssemblyUID]                          INT             NOT NULL,
    [AssemblyType]                         VARCHAR (50)    NOT NULL,
    [BodyUID]                              INT             NOT NULL,
    [BodyMatrixCode]                       VARCHAR (50)    NOT NULL,
    [BodyType]                             VARCHAR (50)    NOT NULL,
    [BodyGuideClearance2FailCount]         INT             NOT NULL,
    [BodyGuideClearance2MeasurementCount]  INT             NOT NULL,
    [PinUID]                               INT             NOT NULL,
    [PinGUID]                              VARCHAR (50)    NOT NULL,
    [PinType]                              VARCHAR (50)    NOT NULL,
    [PinGuideClearance2FailCount]          INT             NOT NULL,
    [PinGuideClearance2MeasurementCount]   INT             NOT NULL,
    [AssemblyGuideClearance2Raw]           DECIMAL (12, 8) NOT NULL,
    [AssemblyGuideClearance2]              DECIMAL (12, 8) NOT NULL,
    [AssemblyGuideClearance2UTL]           DECIMAL (12, 8) NOT NULL,
    [AssemblyGuideClearance2LTL]           DECIMAL (12, 8) NOT NULL,
    [AssemblyGuideClearance2Determination] INT             NOT NULL,
    CONSTRAINT [PK_Stats_AssemblyGuideClearance2] PRIMARY KEY CLUSTERED ([AssemblyGuideClearance2StatUID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_AssemblyGuideClearance2_Det]
    ON [dbo].[SA2_Stats_AssemblyGuideClearance2]([AssemblyGuideClearance2Determination] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_AssemblyGuideClearance2_UTC]
    ON [dbo].[SA2_Stats_AssemblyGuideClearance2]([AssemblyGuideClearance2StatUTC] ASC);

