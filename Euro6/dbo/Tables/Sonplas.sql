﻿CREATE TABLE [dbo].[Sonplas] (
    [PDate]        DATETIME     NOT NULL,
    [SerialNumber] VARCHAR (13) NOT NULL,
    [Backleak]     FLOAT (53)   NULL,
    [StaticFlow]   FLOAT (53)   NULL,
    [NOP]          FLOAT (53)   NULL,
    [WetSeat]      FLOAT (53)   NULL
);

