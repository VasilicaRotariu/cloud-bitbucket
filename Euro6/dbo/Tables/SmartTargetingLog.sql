﻿CREATE TABLE [dbo].[SmartTargetingLog] (
    [UID]     BIGINT         IDENTITY (1, 1) NOT NULL,
    [UTC]     DATETIME2 (7)  CONSTRAINT [DF_TargetedProcessingLog_UTC] DEFAULT (sysutcdatetime()) NOT NULL,
    [Input]   VARCHAR (4000) NOT NULL,
    [Output]  VARCHAR (4000) NULL,
    [Err]     VARCHAR (MAX)  NULL,
    [Elapsed] FLOAT (53)     NULL,
    CONSTRAINT [PK_TargetedProcessingLog] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 80)
);

