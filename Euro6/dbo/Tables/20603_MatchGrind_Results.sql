﻿CREATE TABLE [dbo].[20603_MatchGrind_Results] (
    [Uid]              INT          IDENTITY (1, 1) NOT NULL,
    [PDate]            DATETIME     NOT NULL,
    [Nozzle_Matrix]    VARCHAR (25) NOT NULL,
    [PG_Matrix]        VARCHAR (25) NOT NULL,
    [PGBore]           FLOAT (53)   NULL,
    [Taper]            FLOAT (53)   NULL,
    [Clearance]        FLOAT (53)   NULL,
    [Lift]             FLOAT (53)   NULL,
    [Force]            FLOAT (53)   NULL,
    [NozzleCount]      INT          NULL,
    [PGCount]          INT          NULL,
    [PG_InnerDiameter] FLOAT (53)   NULL,
    [PG_Taper]         FLOAT (53)   NULL,
    [FailCode]         INT          NULL,
    [AssetNumber]      INT          CONSTRAINT [DF_20603_MatchGrind_Results_AssetNumber] DEFAULT ((20603)) NULL
);


GO



-- =============================================
-- Author:		Guillaume Carre
-- Create date: 23/09/2014
-- Description:	on insertion of a new row in this table count the iteration of the particular nozzle serial and piston guide serial already present in the table
-- =============================================
CREATE TRIGGER [dbo].[Update20603_MatchGrind_Result]
   ON  [dbo].[20603_MatchGrind_Results]
   INSTEAD OF INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE		
				@CurrentNozzleTotal INT,
				@CurrentPGTotal INT,
				@NewNozzleTotal INT,
				@NewPGTotal INT
	DECLARE
				@Uid int,
				@PDate datetime,
				@Nozzle_Matrix varchar(25),
				@PG_Matrix varchar(25),
				@Taper float,
				@Clearance float,
				@Lift float,
				@Force float,
				@PG_Diameter float,
				@PG_Taper float,
				@FailCode int,
				@AssetNumber int

				

	DECLARE SerialRows CURSOR FOR 
	SELECT		[Uid],
				[PDate],
				[Nozzle_Matrix],
				[PG_Matrix],
				[Taper],
				[Clearance],
				[Lift],
				[Force],
				[PG_InnerDiameter],
				[PG_Taper],
				[FailCode],
				[AssetNumber]
	FROM INSERTED
	OPEN SerialRows
	WHILE(1=1)
	BEGIN
		SET @CurrentNozzleTotal = 0
		SET @CurrentPGTotal = 0

		FETCH NEXT FROM SerialRows INTO 
				@Uid,
				@PDate,
				@Nozzle_Matrix,
				@PG_Matrix,
				@Taper,
				@Clearance,
				@Lift,
				@Force,
				@PG_Diameter,
				@PG_Taper,
				@FailCode,
				@AssetNumber
		IF @@FETCH_STATUS <> 0
			BREAK
		SELECT @CurrentNozzleTotal = COUNT(*) FROM [20603_MatchGrind_Results] WHERE [Nozzle_Matrix] = @Nozzle_Matrix
		SELECT @CurrentPGTotal = COUNT(*) FROM [20603_MatchGrind_Results] WHERE [PG_Matrix] = @PG_Matrix

		SET @NewNozzleTotal = @CurrentNozzleTotal + 1
		SET @NewPGTotal = @CurrentPGTotal + 1

		INSERT INTO [20603_MatchGrind_Results](
				[PDate],
				[Nozzle_Matrix],
				[PG_Matrix],
				[Taper],
				[Clearance],
				[Lift],
				[Force],
				[NozzleCount],
				[PGCount],
				[PG_InnerDiameter],
				[PG_Taper],
				[FailCode],
				[AssetNumber])
			VALUES ( 
				@PDate,
				@Nozzle_Matrix,
				@PG_Matrix,
				@Taper,
				@Clearance,
				@Lift,
				@Force,
				@NewNozzleTotal,
				@NewPGTotal,
				@PG_Diameter,
				@PG_Taper,
				@FailCode,
				@AssetNumber)
	END
	CLOSE SerialRows
	DEALLOCATE SerialRows
END

