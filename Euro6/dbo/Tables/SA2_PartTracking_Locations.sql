﻿CREATE TABLE [dbo].[SA2_PartTracking_Locations] (
    [Location_UID] INT          IDENTITY (1, 1) NOT NULL,
    [LocationName] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED ([Location_UID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Location_LocationNameUnique]
    ON [dbo].[SA2_PartTracking_Locations]([LocationName] ASC);

