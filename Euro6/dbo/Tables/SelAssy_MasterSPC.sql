﻿CREATE TABLE [dbo].[SelAssy_MasterSPC] (
    [UID]                        INT           IDENTITY (1, 1) NOT NULL,
    [UTC]                        DATETIME2 (7) NOT NULL,
    [PlantNumber]                SMALLINT      NOT NULL,
    [ComponentType]              VARCHAR (20)  NOT NULL,
    [SPCPartNumberAlias]         VARCHAR (50)  NULL,
    [SPCPartDescriptionAlias]    VARCHAR (50)  NULL,
    [Feature]                    VARCHAR (50)  NOT NULL,
    [SPCFeatureDescriptionAlias] VARCHAR (50)  NULL,
    [Value1]                     FLOAT (53)    NOT NULL,
    [Value2]                     FLOAT (53)    NULL,
    [Value3]                     FLOAT (53)    NULL,
    [Value4]                     FLOAT (53)    NULL,
    [Value5]                     FLOAT (53)    NULL
);


GO
CREATE UNIQUE CLUSTERED INDEX [PK_SelAssy_MasterSPC]
    ON [dbo].[SelAssy_MasterSPC]([UID] ASC);

