﻿CREATE TABLE [dbo].[SA2_Stats_BodyMatchPin] (
    [BodyMatchPinStatUID]       INT             IDENTITY (1, 1) NOT NULL,
    [BodyMatchPinStatUTC]       DATETIME2 (7)   NOT NULL,
    [BodyUID]                   INT             NOT NULL,
    [BodyMatrixCode]            VARCHAR (50)    NOT NULL,
    [BodyType]                  VARCHAR (50)    NOT NULL,
    [BodyMatchCount]            INT             NOT NULL,
    [PinType]                   VARCHAR (50)    NOT NULL,
    [AssemblyType]              VARCHAR (50)    NOT NULL,
    [GuideDiameterTarget]       DECIMAL (12, 8) NOT NULL,
    [SeatLengthTarget]          DECIMAL (12, 8) NOT NULL,
    [CollarDiameterTarget]      DECIMAL (12, 8) NULL,
    [TargetGrade]               VARCHAR (50)    NOT NULL,
    [AssemblyUID]               INT             NULL,
    [PinUID]                    INT             NULL,
    [PinMatchCount]             INT             NULL,
    [BodyMatchPinDetermination] INT             NOT NULL,
    CONSTRAINT [PK_Stats_BodyPinMatch] PRIMARY KEY CLUSTERED ([BodyMatchPinStatUID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_BodyMatchPin_Det]
    ON [dbo].[SA2_Stats_BodyMatchPin]([BodyMatchPinDetermination] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_BodyMatchPin_UTC]
    ON [dbo].[SA2_Stats_BodyMatchPin]([BodyMatchPinStatUTC] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_BodyMatrixCode]
    ON [dbo].[SA2_Stats_BodyMatchPin]([BodyMatrixCode] ASC)
    INCLUDE([BodyMatchPinStatUTC], [GuideDiameterTarget], [SeatLengthTarget], [CollarDiameterTarget]);

