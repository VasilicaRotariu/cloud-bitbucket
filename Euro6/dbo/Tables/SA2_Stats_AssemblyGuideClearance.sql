﻿CREATE TABLE [dbo].[SA2_Stats_AssemblyGuideClearance] (
    [AssemblyGuideClearanceStatUID]       INT             IDENTITY (1, 1) NOT NULL,
    [AssemblyGuideClearanceStatUTC]       DATETIME2 (7)   NOT NULL,
    [AssemblyUID]                         INT             NOT NULL,
    [AssemblyType]                        VARCHAR (50)    NOT NULL,
    [BodyUID]                             INT             NOT NULL,
    [BodyMatrixCode]                      VARCHAR (50)    NOT NULL,
    [BodyType]                            VARCHAR (50)    NOT NULL,
    [BodyGuideClearanceFailCount]         INT             NOT NULL,
    [BodyGuideClearanceMeasurementCount]  INT             NOT NULL,
    [PinUID]                              INT             NOT NULL,
    [PinGUID]                             VARCHAR (50)    NOT NULL,
    [PinType]                             VARCHAR (50)    NOT NULL,
    [PinGuideClearanceFailCount]          INT             NOT NULL,
    [PinGuideClearanceMeasurementCount]   INT             NOT NULL,
    [AssemblyGuideClearanceRaw]           DECIMAL (12, 8) NOT NULL,
    [AssemblyGuideClearancePredicted]     DECIMAL (12, 8) NULL,
    [AssemblyGuideClearance]              DECIMAL (12, 8) NOT NULL,
    [AssemblyGuideClearanceUTL]           DECIMAL (12, 8) NOT NULL,
    [AssemblyGuideClearanceLTL]           DECIMAL (12, 8) NOT NULL,
    [AssemblyGuideClearanceDetermination] INT             NOT NULL,
    CONSTRAINT [PK_Stats_AssemblyGuideClearance] PRIMARY KEY CLUSTERED ([AssemblyGuideClearanceStatUID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_AssemblyGuideClearance_Det]
    ON [dbo].[SA2_Stats_AssemblyGuideClearance]([AssemblyGuideClearanceDetermination] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_AssemblyGuideClearance_UTC]
    ON [dbo].[SA2_Stats_AssemblyGuideClearance]([AssemblyGuideClearanceStatUTC] ASC) WITH (FILLFACTOR = 90);

