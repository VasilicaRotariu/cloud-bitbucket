﻿CREATE TABLE [dbo].[NozzleTestWithError_Results] (
    [Uid]                           INT          IDENTITY (1, 1) NOT NULL,
    [Pdate]                         DATETIME     NOT NULL,
    [Serial]                        VARCHAR (30) NULL,
    [Flow_Pressure]                 FLOAT (53)   NULL,
    [Flow_MeasuredFlow]             FLOAT (53)   NULL,
    [NOP_OpeningPressureSingleShot] FLOAT (53)   NULL,
    [NOP_OpeningPressureChatter]    FLOAT (53)   NULL,
    [NOP_ChatterNumberOfPeaks]      FLOAT (53)   NULL,
    [NOP_ChatterFrequency]          FLOAT (53)   NULL,
    [WetSeat_TipLeak]               FLOAT (53)   NULL,
    [WetSeat_Leak]                  FLOAT (53)   NULL,
    [BackLeak_Leak]                 FLOAT (53)   NULL,
    [BackLeak_PressureDrop]         FLOAT (53)   NULL,
    [BackLeak_MeasureTime]          FLOAT (53)   NULL,
    [BackLeak_PressureStop]         FLOAT (53)   NULL,
    [FailCode]                      INT          NULL,
    [InputDate]                     DATETIME     NULL
);


GO
CREATE NONCLUSTERED INDEX [SerialIndex]
    ON [dbo].[NozzleTestWithError_Results]([Serial] ASC)
    INCLUDE([FailCode], [Pdate]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_NozzleTestWithError_SerialASC]
    ON [dbo].[NozzleTestWithError_Results]([Serial] ASC, [Pdate] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_NozzleTestWithError_PDateASC]
    ON [dbo].[NozzleTestWithError_Results]([Pdate] ASC, [Serial] ASC, [Uid] ASC)
    INCLUDE([FailCode]);

