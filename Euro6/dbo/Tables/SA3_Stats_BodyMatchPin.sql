﻿CREATE TABLE [dbo].[SA3_Stats_BodyMatchPin] (
    [BodyMatchPinStatUID]       INT             IDENTITY (1, 1) NOT NULL,
    [BodyMatchPinStatUTC]       DATETIME2 (7)   NOT NULL,
    [BodyUID]                   INT             NOT NULL,
    [BodyMatrixCode]            VARCHAR (50)    NOT NULL,
    [BodyType]                  VARCHAR (50)    NOT NULL,
    [BodyMatchCount]            INT             NOT NULL,
    [PinType]                   VARCHAR (50)    NOT NULL,
    [AssemblyType]              VARCHAR (50)    NOT NULL,
    [GuideDiameterTarget]       DECIMAL (12, 8) NOT NULL,
    [SeatLengthTarget]          DECIMAL (12, 8) NOT NULL,
    [CollarDiameterTarget]      DECIMAL (12, 8) NULL,
    [TargetGrade]               VARCHAR (50)    NOT NULL,
    [AssemblyUID]               INT             NULL,
    [PinUID]                    INT             NULL,
    [PinMatchCount]             INT             NULL,
    [BodyMatchPinDetermination] INT             NOT NULL
);


GO
CREATE UNIQUE CLUSTERED INDEX [PK_SA3_Stats_BodyPinMatch]
    ON [dbo].[SA3_Stats_BodyMatchPin]([BodyMatchPinStatUID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_SA3_STATS_BODYMATCHPIN]
    ON [dbo].[SA3_Stats_BodyMatchPin]([BodyMatrixCode] ASC) WITH (FILLFACTOR = 90);

