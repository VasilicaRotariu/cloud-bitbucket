﻿CREATE TABLE [dbo].[SelAssy_MasterComponentTypes] (
    [MasterComponentType_UID] INT          IDENTITY (1, 1) NOT NULL,
    [PlantNumber]             SMALLINT     NOT NULL,
    [ComponentType]           VARCHAR (20) NOT NULL,
    [TimeLimitSeconds]        INT          CONSTRAINT [DF_SelAssy_MasterComponentTypes_TimeLimitSeconds] DEFAULT ((600)) NOT NULL,
    [LastScrapeUID]           BIGINT       NULL,
    [SPCPartNumberAlias]      VARCHAR (50) NULL,
    [SPCPartDescriptionAlias] VARCHAR (50) NULL,
    CONSTRAINT [PK_SelAssyMaster_SPC] PRIMARY KEY CLUSTERED ([MasterComponentType_UID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SelAssy_MasterComponentTypes_U1]
    ON [dbo].[SelAssy_MasterComponentTypes]([PlantNumber] ASC, [ComponentType] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[U1]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SelAssy_MasterComponentTypes', @level2type = N'COLUMN', @level2name = N'PlantNumber';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[U1]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SelAssy_MasterComponentTypes', @level2type = N'COLUMN', @level2name = N'ComponentType';

