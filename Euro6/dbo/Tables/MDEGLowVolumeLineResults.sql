﻿CREATE TABLE [dbo].[MDEGLowVolumeLineResults] (
    [UID]                     INT          NULL,
    [IDate]                   DATETIME     CONSTRAINT [DF__MDEGLowVo__IDate__50FB042B] DEFAULT (getdate()) NOT NULL,
    [PDate]                   DATETIME     NOT NULL,
    [InjectorSerialNumber]    VARCHAR (12) NOT NULL,
    [NCVSerialNumber]         VARCHAR (11) NOT NULL,
    [NozzleSerialNumber]      VARCHAR (13) NOT NULL,
    [NCVSerialNumberCount]    BIGINT       NULL,
    [NozzleSerialNumberCount] BIGINT       NULL,
    CONSTRAINT [PK_MDEGLowVolumeLineResults] PRIMARY KEY CLUSTERED ([PDate] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150622-162648]
    ON [dbo].[MDEGLowVolumeLineResults]([NozzleSerialNumber] ASC);


GO
CREATE TRIGGER [dbo].[UpdateMDEGLowVolumeLineResultsTotals]
   ON  [dbo].[MDEGLowVolumeLineResults]
   INSTEAD OF INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE		
				@CurrentNozzleSerialTotal INT,
				@CurrentNCVSerialTotal INT,

				@CurrentNLSNozzleSerialTotal INT,
				@CurrentRSPLNCVSerialTotal INT,

				@NewNCVSerialCount INT,
				@NewNozzleSerialCount INT,
				@NewTotal INT;

	DECLARE 	@PDate DATETIME,
				@IDate DATETIME,
				@InjectorSerialNumber NVARCHAR(12),
				@NCVSerialNumber NVARCHAR(11),
				@NozzleSerialNumber NVARCHAR(13)

	DECLARE SerialRows CURSOR FOR
	--SELECT MAX(NozzleSerial) as NozzleSerial, COUNT(NozzleSerial) AS NozzleSerialCount FROM INSERTED GROUP BY NozzleSerial
	SELECT 	IDate, PDate, InjectorSerialNumber, NCVSerialNumber, NozzleSerialNumber FROM INSERTED
	OPEN SerialRows
	WHILE(1=1)
	BEGIN
		SET @CurrentNozzleSerialTotal = 0
		SET @CurrentNCVSerialTotal = 0
		FETCH NEXT FROM SerialRows INTO @IDate, @PDate, @InjectorSerialNumber, @NCVSerialNumber, @NozzleSerialNumber
		IF @@FETCH_STATUS <> 0
			BREAK

		SELECT @CurrentNozzleSerialTotal = COUNT(*) FROM MDEGLowVolumeLineResults  WHERE NozzleSerialNumber = @NozzleSerialNumber
		SELECT @CurrentNCVSerialTotal = COUNT(*) FROM MDEGLowVolumeLineResults  WHERE NCVSerialNumber = @NCVSerialNumber

		--- Now check for existence of @NozzleSerial & @NCVSerial on NLS & RSPL
		SELECT @CurrentNLSNozzleSerialTotal = COUNT(*) FROM NLS  WHERE Nozzle_DataMatrix = @NozzleSerialNumber
		SELECT @CurrentRSPLNCVSerialTotal = COUNT(*) FROM RSPL  WHERE NCV_DataMatrix = @NCVSerialNumber
		IF(@CurrentNLSNozzleSerialTotal IS NOT NULL AND @CurrentNLSNozzleSerialTotal > @CurrentNozzleSerialTotal)	
		BEGIN
			SET @CurrentNozzleSerialTotal = @CurrentNLSNozzleSerialTotal
		END
		IF(@CurrentRSPLNCVSerialTotal IS NOT NULL AND @CurrentRSPLNCVSerialTotal > @CurrentNCVSerialTotal)
		BEGIN
			SET @CurrentNCVSerialTotal = @CurrentRSPLNCVSerialTotal
		END
		---
		IF(@CurrentNozzleSerialTotal IS NULL)
		BEGIN
			SET @CurrentNozzleSerialTotal = 0;
		END
		IF(@CurrentNCVSerialTotal IS NULL)
		BEGIN
			SET @CurrentNCVSerialTotal = 0
		END

		SET @NewNozzleSerialCount = @CurrentNozzleSerialTotal + 1
		SET @NewNCVSerialCount = @CurrentNCVSerialTotal + 1;

		INSERT INTO MDEGLowVolumeLineResults(IDate, PDate, InjectorSerialNumber, NCVSerialNumber, NozzleSerialNumber, NCVSerialNumberCount, NozzleSerialNumberCount)
		VALUES(@IDate, @PDate, @InjectorSerialNumber, @NCVSerialNumber, @NozzleSerialNumber, @NewNCVSerialCount, @NewNozzleSerialCount)
	END
	CLOSE SerialRows
	DEALLOCATE SerialRows
END
