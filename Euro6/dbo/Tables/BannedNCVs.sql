﻿CREATE TABLE [dbo].[BannedNCVs] (
    [SerialNumber] NVARCHAR (50) NOT NULL,
    [FailCode]     TINYINT       CONSTRAINT [DF_BannedNCV_FailCode] DEFAULT ((1)) NOT NULL,
    [Pdate]        DATETIME      CONSTRAINT [DF_BannedNCV_Pdate] DEFAULT (sysdatetime()) NOT NULL
);



