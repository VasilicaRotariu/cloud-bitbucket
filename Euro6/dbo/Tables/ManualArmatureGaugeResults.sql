﻿CREATE TABLE [dbo].[ManualArmatureGaugeResults] (
    [PDate]             DATETIME2 (7) NOT NULL,
    [IDate]             DATETIME2 (7) NULL,
    [Product Type]      VARCHAR (12)  NOT NULL,
    [Product Version]   INT           NOT NULL,
    [PlantNumber]       INT           NOT NULL,
    [Result]            TINYINT       NOT NULL,
    [DataMatrix]        VARCHAR (12)  NOT NULL,
    [DataMatrix_Result] TINYINT       NOT NULL,
    [IsNCV]             BIT           NOT NULL,
    [Gauge_Result]      TINYINT       NOT NULL,
    [FAG]               FLOAT (53)    NOT NULL,
    [Tilt]              FLOAT (53)    NOT NULL,
    [TDate]             DATETIME2 (7) CONSTRAINT [DF_ManualArmatureGaugeResults_TDate] DEFAULT (sysdatetime()) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ManualArmatureGaugeResults_DataMatrixASC]
    ON [dbo].[ManualArmatureGaugeResults]([DataMatrix] ASC)
    INCLUDE([PDate], [Result]) WITH (FILLFACTOR = 90);

