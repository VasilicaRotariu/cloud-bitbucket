﻿CREATE TABLE [dbo].[BannedNozzles] (
    [SerialNumber] NVARCHAR (50) NOT NULL,
    [FailCode]     TINYINT       CONSTRAINT [DF_BannedNozzles_FailCode] DEFAULT ((1)) NOT NULL,
    [Pdate]        DATETIME      CONSTRAINT [DF_BannedNozzles_Pdate] DEFAULT (sysdatetime()) NOT NULL
);

