﻿CREATE TABLE [dbo].[SA1_NCV_OffloadComplete] (
    [UID]          INT           NOT NULL,
    [UTC]          DATETIME2 (7) NOT NULL,
    [Matrix]       VARCHAR (50)  NOT NULL,
    [PinGUID]      VARCHAR (50)  NULL,
    [Passed]       BIT           NOT NULL,
    [Attempts]     INT           NOT NULL,
    [LappingGrade] VARCHAR (1)   NULL,
    CONSTRAINT [PK_NCV_OffloadComplete] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 90)
);

