﻿CREATE TABLE [dbo].[ReworkCapnutTorque] (
    [PDate]                                         NVARCHAR (50) NOT NULL,
    [PlantNumber]                                   NVARCHAR (50) NOT NULL,
    [Assembly_Modality_Code]                        NVARCHAR (50) NULL,
    [Injector_ID]                                   NVARCHAR (20) NOT NULL,
    [TEST_NUMBER]                                   SMALLINT      NOT NULL,
    [LineID]                                        SMALLINT      NOT NULL,
    [InjectorCode]                                  NVARCHAR (20) NOT NULL,
    [TAG_Code]                                      NVARCHAR (40) NULL,
    [UserName]                                      NVARCHAR (50) NOT NULL,
    [InDate]                                        NVARCHAR (14) NOT NULL,
    [OutDate]                                       NVARCHAR (14) NOT NULL,
    [GLOBAL_RESULT]                                 SMALLINT      NOT NULL,
    [Result_LeakTest]                               SMALLINT      NULL,
    [LeakTest_Program]                              SMALLINT      NULL,
    [Measure_LeakTest]                              FLOAT (53)    NULL,
    [UnitMeasure_LeakTest]                          NVARCHAR (5)  NULL,
    [Leak_Test_CODE]                                SMALLINT      NULL,
    [ShiftNumber]                                   SMALLINT      NULL,
    [Measure_LoadCellScrewing_Torque_TorqueProgram] FLOAT (53)    NULL,
    [Measure_LoadCellScrewing_Torque_AngleProgram]  FLOAT (53)    NULL,
    [MaxTorqueOverCheck]                            FLOAT (53)    NULL,
    [MinTorqueOverCheck]                            FLOAT (53)    NULL,
    [Hole_Position_Request]                         FLOAT (53)    NULL,
    [Groove_Number]                                 SMALLINT      NULL,
    [Positioning_Delta]                             FLOAT (53)    NULL,
    [Final_Position_Measure]                        FLOAT (53)    NULL,
    [First_Position_Measure]                        FLOAT (53)    NULL,
    [Hole_Position_Result]                          FLOAT (53)    NULL,
    [Measure_Screwing_Torque_OverCheck]             FLOAT (53)    NULL,
    [Result_Screwing_Torque_OverCheck]              SMALLINT      NULL,
    [GR_Residual_Torque]                            FLOAT (53)    NULL,
    [Measure_Screwing_Torque]                       FLOAT (53)    NULL,
    [Result_Screwing_Torque]                        SMALLINT      NULL,
    [Measure_Screwing_Angle]                        FLOAT (53)    NULL,
    [Result_Screwing_Angle]                         SMALLINT      NULL,
    [Measure_Screwing_Corsa]                        FLOAT (53)    NULL,
    [Result_Screwing_Corsa]                         SMALLINT      NULL,
    [Max_Corsa_CAPNUT]                              FLOAT (53)    NULL,
    [Min_Corsa_CAPNUT]                              FLOAT (53)    NULL,
    [Final_Position_Tollerance]                     FLOAT (53)    NULL,
    [First_Position_Tollerance]                     FLOAT (53)    NULL,
    [Groove_Matching_File]                          NVARCHAR (50) NULL,
    [Hole_Matching_File]                            NVARCHAR (50) NULL,
    [Screwing_Program_Angle]                        SMALLINT      NULL,
    [Screwing_Program_Torque]                       SMALLINT      NULL,
    [Nozzle_DataMatrix]                             NVARCHAR (50) NULL,
    [Rework_Tag]                                    CHAR (2)      NULL,
    [TDate]                                         DATETIME2 (7) CONSTRAINT [DF_ReworkCapnutTorque_TDate] DEFAULT (getdate()) NULL
);

