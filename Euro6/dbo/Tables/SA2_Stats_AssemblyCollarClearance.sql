﻿CREATE TABLE [dbo].[SA2_Stats_AssemblyCollarClearance] (
    [AssemblyCollarClearanceStatUID]       INT             IDENTITY (1, 1) NOT NULL,
    [AssemblyCollarClearanceStatUTC]       DATETIME2 (7)   NOT NULL,
    [AssemblyUID]                          INT             NOT NULL,
    [AssemblyType]                         VARCHAR (50)    NOT NULL,
    [BodyUID]                              INT             NOT NULL,
    [BodyMatrixCode]                       VARCHAR (50)    NOT NULL,
    [BodyType]                             VARCHAR (50)    NOT NULL,
    [BodyCollarClearanceFailCount]         INT             NOT NULL,
    [BodyCollarClearanceMeasurementCount]  INT             NOT NULL,
    [PinUID]                               INT             NOT NULL,
    [PinGUID]                              VARCHAR (50)    NOT NULL,
    [PinType]                              VARCHAR (50)    NOT NULL,
    [PinCollarClearanceFailCount]          INT             NOT NULL,
    [PinCollarClearanceMeasurementCount]   INT             NOT NULL,
    [AssemblyCollarClearanceRaw]           DECIMAL (12, 8) NOT NULL,
    [AssemblyCollarClearancePredicted]     DECIMAL (12, 8) CONSTRAINT [DF_SA2_Stats_AssemblyCollarClearance_AssemblyGuideClearancePredicted] DEFAULT ((0)) NULL,
    [AssemblyCollarClearance]              DECIMAL (12, 8) NOT NULL,
    [AssemblyCollarClearanceUTL]           DECIMAL (12, 8) NOT NULL,
    [AssemblyCollarClearanceLTL]           DECIMAL (12, 8) NOT NULL,
    [AssemblyCollarClearanceDetermination] INT             NOT NULL,
    CONSTRAINT [PK_Stats_AssemblyCollarClearance] PRIMARY KEY CLUSTERED ([AssemblyCollarClearanceStatUID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_AssemblyCollarClearance_Det]
    ON [dbo].[SA2_Stats_AssemblyCollarClearance]([AssemblyCollarClearanceDetermination] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_AssemblyCollarClearance_UTC]
    ON [dbo].[SA2_Stats_AssemblyCollarClearance]([AssemblyCollarClearanceStatUTC] ASC);

