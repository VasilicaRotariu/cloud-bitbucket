﻿CREATE TABLE [dbo].[ZBLN_Sekler_Data] (
    [Date&Time]         DATETIME       NOT NULL,
    [MatrixID]          CHAR (14)      NOT NULL,
    [ValveLowerDia_V]   NUMERIC (8, 5) NOT NULL,
    [ValveUpperDia_W]   NUMERIC (8, 5) NOT NULL,
    [UpperGuideTapper]  NUMERIC (8, 5) NOT NULL,
    [Seat_To_Lift_Y]    NUMERIC (8, 5) NOT NULL,
    [CoolantTemp]       INT            NOT NULL,
    [AmbientTemp]       INT            NOT NULL,
    [WHL1Dressing]      INT            NOT NULL,
    [Whl2Dressing]      INT            NOT NULL,
    [MC_Upper_Dia]      NUMERIC (8, 5) NOT NULL,
    [MC_Lower_Dia]      NUMERIC (8, 5) NOT NULL,
    [MC_Lift]           NUMERIC (8, 5) NOT NULL,
    [MC_SprSeat_Length] NUMERIC (8, 5) NOT NULL,
    [MC_Taper]          NUMERIC (8, 5) NOT NULL,
    [W_Speed1]          NUMERIC (8, 5) NOT NULL,
    [W_Speed2]          NUMERIC (8, 5) NOT NULL,
    [CycleTime]         NUMERIC (8, 2) NOT NULL,
    [OK2Delete]         TINYINT        NULL,
    [TDate]             DATETIME       NULL
);

