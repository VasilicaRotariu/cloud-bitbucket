﻿CREATE TABLE [dbo].[tmp_WPSonplasOutput] (
    [Uid]           INT          IDENTITY (1, 1) NOT NULL,
    [Pdate]         DATETIME     NOT NULL,
    [PartNumber]    VARCHAR (10) NOT NULL,
    [SerialNumber]  VARCHAR (15) NOT NULL,
    [OverallResult] INT          NOT NULL,
    [TrimCode]      VARCHAR (50) NOT NULL
);

