﻿CREATE TABLE [dbo].[NozzleRebuilds] (
    [NozzleSerial]            VARCHAR (25) NOT NULL,
    [TimeStamp]               DATETIME     NOT NULL,
    [NozzleRebuildReasonsFID] INT          NULL,
    [ReplacementSerial]       VARCHAR (25) NULL
);

