﻿CREATE TABLE [dbo].[ProductLaserMarking] (
    [UID]                   INT           IDENTITY (1, 1) NOT NULL,
    [PDate]                 DATETIME2 (7) NULL,
    [TDate]                 DATETIME2 (7) CONSTRAINT [DF_ProductLaserMarking_TDate] DEFAULT (sysutcdatetime()) NULL,
    [SerialNumber]          INT           NULL,
    [AssetNumber]           VARCHAR (12)  CONSTRAINT [DF_ProductLaserMarking_AssetNumber] DEFAULT (sysutcdatetime()) NULL,
    [PartNumber]            VARCHAR (12)  NULL,
    [TrimType]              VARCHAR (12)  NULL,
    [DataMatrixString]      VARCHAR (16)  NULL,
    [MatrixGrade]           VARCHAR (1)   NULL,
    [TagString]             VARCHAR (305) NULL,
    [DMResult]              BIT           NULL,
    [Contrast]              FLOAT (53)    NULL,
    [Uniformity]            FLOAT (53)    NULL,
    [Growth]                FLOAT (53)    NULL,
    [Angle]                 FLOAT (53)    NULL,
    [DMRead]                VARCHAR (50)  NULL,
    [FixedPatternDamage]    FLOAT (53)    NULL,
    [TwoDimModulation]      FLOAT (53)    NULL,
    [UnusedErrorCorrection] FLOAT (53)    NULL,
    [PhotoTaken]            BIT           NULL,
    CONSTRAINT [PK_ProductLaserMarking] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_ProductLaserMarking_DMRead]
    ON [dbo].[ProductLaserMarking]([DMRead] ASC);

