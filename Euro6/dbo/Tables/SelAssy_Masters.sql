﻿CREATE TABLE [dbo].[SelAssy_Masters] (
    [Master_UID]              INT  IDENTITY (1, 1) NOT NULL,
    [MasterComponentType_FID] INT  NOT NULL,
    [MasterLimitType_FID]     INT  NOT NULL,
    [Comments]                TEXT NULL,
    CONSTRAINT [PK_SelAssy_Masters] PRIMARY KEY CLUSTERED ([Master_UID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_SelAssy_Masters_SelAssy_MasterComponentTypes] FOREIGN KEY ([MasterComponentType_FID]) REFERENCES [dbo].[SelAssy_MasterComponentTypes] ([MasterComponentType_UID]) ON DELETE CASCADE,
    CONSTRAINT [FK_SelAssy_Masters_SelAssy_MasterType] FOREIGN KEY ([MasterLimitType_FID]) REFERENCES [dbo].[SelAssy_MasterTypes] ([MasterLimitType_UID]) ON DELETE CASCADE
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SelAssy_Master_U1]
    ON [dbo].[SelAssy_Masters]([MasterComponentType_FID] ASC, [MasterLimitType_FID] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[U1]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SelAssy_Masters', @level2type = N'COLUMN', @level2name = N'MasterComponentType_FID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[U1]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SelAssy_Masters', @level2type = N'COLUMN', @level2name = N'MasterLimitType_FID';

