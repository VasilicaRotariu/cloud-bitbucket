﻿CREATE TABLE [dbo].[LowVolumeMX13Line1] (
    [PDate]             DATETIME     NOT NULL,
    [ISN]               VARCHAR (10) NOT NULL,
    [ProductType]       VARCHAR (7)  NOT NULL,
    [NCVSerial]         VARCHAR (11) NOT NULL,
    [NozzleSerial]      VARCHAR (13) NOT NULL,
    [NCVSerialCount]    BIGINT       CONSTRAINT [DF_LowVolumeMX13Line1_NCVSerialCount] DEFAULT ((0)) NOT NULL,
    [NozzleSerialCount] BIGINT       CONSTRAINT [DF_LowVolumeMX13Line1_NozzleSerialCount] DEFAULT ((0)) NOT NULL
);


GO
CREATE TRIGGER [dbo].[UpdateLowVolumeMX13Line1ComponentTotals]
   ON  [dbo].[LowVolumeMX13Line1]
   INSTEAD OF INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE		@NCVSerial NVARCHAR(11),
				@NozzleSerial NVARCHAR(13),
				@CurrentNozzleSerialTotal INT,
				@CurrentNCVSerialTotal INT,
				@CurrentNLSNozzleSerialTotal INT,
				@CurrentRSPLNCVSerialTotal INT,
				@NewNCVSerialCount INT,
				@NewNozzleSerialCount INT,
				@NewTotal INT;

	DECLARE 	@PDate DATETIME,
				@ISN NVARCHAR(10),
				@ProductType NVARCHAR(7)

	DECLARE NozzleSerialRows CURSOR FOR
	--SELECT MAX(NozzleSerial) as NozzleSerial, COUNT(NozzleSerial) AS NozzleSerialCount FROM INSERTED GROUP BY NozzleSerial
	SELECT 	PDate ,ISN ,ProductType ,NCVSerial ,NozzleSerial FROM INSERTED
	OPEN NozzleSerialRows
	WHILE(1=1)
	BEGIN
		SET @CurrentNozzleSerialTotal = 0
		SET @CurrentNCVSerialTotal = 0
		FETCH NEXT FROM NozzleSerialRows INTO @PDate, @ISN, @ProductType, @NCVSerial, @NozzleSerial
		IF @@FETCH_STATUS <> 0
			BREAK

		SELECT @CurrentNozzleSerialTotal = COUNT(*) FROM LowVolumeMX13Line1  WHERE NozzleSerial = @NozzleSerial
		SELECT @CurrentNCVSerialTotal = COUNT(*) FROM LowVolumeMX13Line1  WHERE NCVSerial = @NCVSerial
		--- Now check for existence of @NozzleSerial & @NCVSerial on NLS & RSPL
		SELECT @CurrentNLSNozzleSerialTotal = COUNT(*) FROM NLS  WHERE Nozzle_DataMatrix = @NozzleSerial
		SELECT @CurrentRSPLNCVSerialTotal = COUNT(*) FROM RSPL  WHERE NCV_DataMatrix = @NCVSerial
		IF(@CurrentNLSNozzleSerialTotal IS NOT NULL AND @CurrentNLSNozzleSerialTotal > @CurrentNozzleSerialTotal)	
		BEGIN
			SET @CurrentNozzleSerialTotal = @CurrentNLSNozzleSerialTotal
		END
		IF(@CurrentRSPLNCVSerialTotal IS NOT NULL AND @CurrentRSPLNCVSerialTotal > @CurrentNCVSerialTotal)
		BEGIN
			SET @CurrentNCVSerialTotal = @CurrentRSPLNCVSerialTotal
		END
		---
		IF(@CurrentNozzleSerialTotal IS NULL)
		BEGIN
			SET @CurrentNozzleSerialTotal = 0;
		END
		IF(@CurrentNCVSerialTotal IS NULL)
		BEGIN
			SET @CurrentNCVSerialTotal = 0
		END
		SET @NewNozzleSerialCount = @CurrentNozzleSerialTotal + 1;
		SET @NewNCVSerialCount = @CurrentNCVSerialTotal + 1;

		INSERT INTO LowVolumeMX13Line1(PDate ,ISN, ProductType, NCVSerial, NozzleSerial, NCVSerialCount, NozzleSerialCount)
		VALUES(@PDate ,@ISN ,@ProductType ,@NCVSerial ,@NozzleSerial, @NewNCVSerialCount, @NewNozzleSerialCount)
	END
	CLOSE NozzleSerialRows
	DEALLOCATE NozzleSerialRows
END