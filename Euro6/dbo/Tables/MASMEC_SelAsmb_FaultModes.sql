﻿CREATE TABLE [dbo].[MASMEC_SelAsmb_FaultModes] (
    [UID]              INT            NOT NULL,
    [FaultDescription] VARCHAR (1000) NULL,
    CONSTRAINT [PK_MASMEC_SelAsmb_FaultModes] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 80)
);

