﻿CREATE TABLE [dbo].[NCV_ReGrader] (
    [Machine] VARCHAR (MAX) NOT NULL,
    [Grade]   VARCHAR (10)  NOT NULL,
    [Collar]  VARCHAR (10)  NOT NULL,
    [Dia]     VARCHAR (10)  NOT NULL,
    [Seat]    VARCHAR (10)  NOT NULL,
    [Matrix]  VARCHAR (20)  NOT NULL,
    [PDate]   DATETIME2 (7) NOT NULL,
    [TDate]   DATETIME2 (7) NOT NULL
);


GO
CREATE CLUSTERED INDEX [IX_NCVReGrader_PDateDESC]
    ON [dbo].[NCV_ReGrader]([PDate] ASC) WITH (FILLFACTOR = 90);

