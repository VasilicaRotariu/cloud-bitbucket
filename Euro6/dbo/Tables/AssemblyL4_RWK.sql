﻿CREATE TABLE [dbo].[AssemblyL4_RWK] (
    [PDate]         NVARCHAR (50) NOT NULL,
    [Injector_ID]   NVARCHAR (20) NOT NULL,
    [InjectorCode]  NVARCHAR (9)  NULL,
    [NCV_Serial]    NVARCHAR (20) NOT NULL,
    [Nozzle_Serial] NVARCHAR (20) NOT NULL,
    [TDate]         DATETIME2 (7) CONSTRAINT [DF_AssemblyL4_RWK_TDate] DEFAULT (sysdatetime()) NOT NULL
);

