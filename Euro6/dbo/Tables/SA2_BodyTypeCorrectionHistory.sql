﻿CREATE TABLE [dbo].[SA2_BodyTypeCorrectionHistory] (
    [BodyTypeCorrectionHistory_UID]   INT             IDENTITY (1, 1) NOT NULL,
    [BodyType_FID]                    INT             NOT NULL,
    [UTC]                             DATETIME2 (7)   NOT NULL,
    [BodyGuideDiameterAve_Correction] DECIMAL (12, 8) NOT NULL,
    [BodySeatLength_Correction]       DECIMAL (12, 8) NOT NULL,
    [BodyCollarDiameter_Correction]   DECIMAL (12, 8) NOT NULL,
    CONSTRAINT [PK_BodyTypeCorrectionHistory] PRIMARY KEY CLUSTERED ([BodyTypeCorrectionHistory_UID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_SA2_BodyTypeCorrectionHistory_UIDASC]
    ON [dbo].[SA2_BodyTypeCorrectionHistory]([BodyTypeCorrectionHistory_UID] ASC);

