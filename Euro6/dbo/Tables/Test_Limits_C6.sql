﻿CREATE TABLE [dbo].[Test_Limits_C6] (
    [Uid]       INT          IDENTITY (1, 1) NOT NULL,
    [Type]      VARCHAR (50) NULL,
    [Serial]    VARCHAR (50) NULL,
    [ValidFrom] DATETIME     NOT NULL,
    [Graph]     INT          NOT NULL,
    [Set]       INT          NOT NULL,
    [x]         FLOAT (53)   NOT NULL,
    [y]         FLOAT (53)   NOT NULL,
    [TestPlan]  INT          NULL
);

