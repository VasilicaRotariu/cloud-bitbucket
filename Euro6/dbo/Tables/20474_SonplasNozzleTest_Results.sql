﻿CREATE TABLE [dbo].[20474_SonplasNozzleTest_Results] (
    [Uid]                           INT          IDENTITY (1, 1) NOT NULL,
    [Pdate]                         DATETIME     NOT NULL,
    [Serial]                        VARCHAR (30) NULL,
    [Flow_Pressure]                 FLOAT (53)   CONSTRAINT [DF_20474_SonplasNozzleTest_Results_Flow_Pressure] DEFAULT ((0)) NULL,
    [Flow_MeasuredFlow]             FLOAT (53)   CONSTRAINT [DF_20474_SonplasNozzleTest_Results_Flow_MeasuredFlow] DEFAULT ((0)) NULL,
    [NOP_OpeningPressureSingleShot] FLOAT (53)   CONSTRAINT [DF_20474_SonplasNozzleTest_Results_NOP_OpeningPressureSingleShot] DEFAULT ((0)) NULL,
    [NOP_OpeningPressureChatter]    FLOAT (53)   CONSTRAINT [DF_20474_SonplasNozzleTest_Results_NOP_OpeningPressureChatter] DEFAULT ((0)) NULL,
    [NOP_ChatterNumberOfPeaks]      FLOAT (53)   CONSTRAINT [DF_20474_SonplasNozzleTest_Results_NOP_ChatterNumberOfPeaks] DEFAULT ((0)) NULL,
    [NOP_ChatterFrequency]          FLOAT (53)   CONSTRAINT [DF_20474_SonplasNozzleTest_Results_NOP_ChatterFrequency] DEFAULT ((0)) NULL,
    [WetSeat_TipLeak]               FLOAT (53)   CONSTRAINT [DF_20474_SonplasNozzleTest_Results_WetSeat_TipLeak] DEFAULT ((0)) NULL,
    [WetSeat_Leak]                  FLOAT (53)   CONSTRAINT [DF_20474_SonplasNozzleTest_Results_WetSeat_Leak] DEFAULT ((0)) NULL,
    [BackLeak_Leak]                 FLOAT (53)   CONSTRAINT [DF_20474_SonplasNozzleTest_Results_BackLeak_Leak] DEFAULT ((0)) NULL,
    [BackLeak_PressureDrop]         FLOAT (53)   CONSTRAINT [DF_20474_SonplasNozzleTest_Results_BackLeak_PressureDrop] DEFAULT ((0)) NULL,
    [BackLeak_MeasureTime]          FLOAT (53)   CONSTRAINT [DF_20474_SonplasNozzleTest_Results_BackLeak_MeasureTime] DEFAULT ((0)) NULL,
    [BackLeak_PressureStop]         FLOAT (53)   CONSTRAINT [DF_20474_SonplasNozzleTest_Results_BackLeak_PressureStop] DEFAULT ((0)) NULL,
    [FailCode]                      INT          NULL,
    [InputDate]                     DATETIME     NULL,
    [OutputDate]                    DATETIME     NULL,
    [SerialCount]                   INT          NULL
);


GO
CREATE CLUSTERED INDEX [IX_20474_SonplasNozzleTest_Results]
    ON [dbo].[20474_SonplasNozzleTest_Results]([Pdate] ASC, [Serial] ASC) WITH (FILLFACTOR = 90);


GO


-- =============================================
-- Author:		Guillaume Carre 
-- Create date: 23/09/2014
-- Description:	trigger counting the number of time this component has been built 
-- =============================================
CREATE TRIGGER [dbo].[AfterInsert20474_SonplasNozzleTest_Results]  ON [dbo].[20474_SonplasNozzleTest_Results]  AFTER INSERT
AS
    BEGIN
        -- Don't show messages 
        SET NOCOUNT ON;
        -- Temporary table to hold records to update
        DECLARE @tbl TABLE([Row] INT, [PDate] DATETIME, [Serial] VARCHAR(max));
        -- Insert records to update
        INSERT INTO @tbl
        SELECT TOP 20 ROW_NUMBER() OVER(ORDER BY [PDate]) AS [Row], [PDate], [Serial] 
        FROM [dbo].[20474_SonplasNozzleTest_Results] AS npf
        WHERE [SerialCount] IS NULL
        ORDER BY [PDate] ASC;
        -- Temp variables for loop
        DECLARE @Row INT, @Rows INT, @DMCount INT, @DM VARCHAR(max), @PD DATETIME;
        -- Count rows inserted into temp table
        SELECT @Rows=@@ROWCOUNT;
        -- Start at first row
        SELECT @Row=1;
        -- Loop each row
        WHILE(@Row<=@Rows)
            BEGIN
                -- Grab Matrix & PDate for this loop          
                SELECT @DM=[Serial], @PD=[PDate] FROM @tbl WHERE [Row]=@Row;
                -- Count instances for this matrix prior to this date
                SELECT @DMCount=ISNULL(COUNT(1),0)+1 FROM [dbo].[20474_SonplasNozzleTest_Results] WHERE [Serial]=@DM AND [PDate]<@PD;
                -- Update the specific record in the live table
                UPDATE [dbo].[20474_SonplasNozzleTest_Results] SET [SerialCount] = @DMCount WHERE [PDate]=@PD AND [Serial]=@DM;
                -- Increment the loop count
                SET @Row = @Row + 1;
            END
        -- Show messages
        SET NOCOUNT OFF;

    END


