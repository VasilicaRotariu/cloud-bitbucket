﻿CREATE TABLE [dbo].[DAFLowVolumeSerialNumbers] (
    [SerialNumber] VARCHAR (9)   NOT NULL,
    [TDate]        DATETIME2 (7) NULL,
    PRIMARY KEY CLUSTERED ([SerialNumber] ASC) WITH (FILLFACTOR = 90)
);

