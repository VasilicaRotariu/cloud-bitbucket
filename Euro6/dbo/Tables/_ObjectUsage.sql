﻿CREATE TABLE [dbo].[_ObjectUsage] (
    [DateTime] DATETIME2 (7) DEFAULT (sysdatetime()) NOT NULL,
    [HostName] VARCHAR (50)  NOT NULL,
    [UserName] VARCHAR (50)  NOT NULL,
    [Object]   VARCHAR (100) NOT NULL,
    [In]       VARCHAR (MAX) NOT NULL,
    [Out]      VARCHAR (MAX) NOT NULL,
    [Duration] TIME (7)      NOT NULL
);


GO

--	Trigger to keep table tidy. Stops it getting too big if we forget about it.
CREATE TRIGGER [dbo].[_ObjectUsageCleanup] ON [dbo].[_ObjectUsage] AFTER INSERT AS
BEGIN
	DECLARE @HostName VARCHAR(50), @UserName VARCHAR(50);
	SELECT @HostName = [HostName], @UserName = [UserName] FROM INSERTED;
    DELETE [dbo].[_ObjectUsage] 
	WHERE 
	(
		[DateTime] < DATEADD(DAY, -7, SYSDATETIME()) AND 
		[HostName] = @HostName AND 
		[UserName] = @UserName
	);
END
