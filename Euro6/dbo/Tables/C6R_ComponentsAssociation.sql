﻿CREATE TABLE [dbo].[C6R_ComponentsAssociation] (
    [Nozzle_Matrix] VARCHAR (20) NULL,
    [PG_Matrix]     VARCHAR (20) NULL,
    [ISN]           VARCHAR (30) NULL,
    [PDate]         DATETIME     CONSTRAINT [DF_C6R_ComponentsAssociation_PDate] DEFAULT (sysdatetime()) NULL
);

