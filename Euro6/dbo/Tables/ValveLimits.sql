﻿CREATE TABLE [dbo].[ValveLimits] (
    [PartNumber]  VARCHAR (10) NOT NULL,
    [Type]        VARCHAR (2)  NOT NULL,
    [Description] VARCHAR (50) NULL,
    [FagLL]       FLOAT (53)   NOT NULL,
    [FagUL]       FLOAT (53)   NOT NULL,
    [TiltLL]      FLOAT (53)   NOT NULL,
    [TiltUL]      FLOAT (53)   NOT NULL,
    [H_FagLL]     FLOAT (53)   NOT NULL,
    [H_FagUL]     FLOAT (53)   NOT NULL,
    [M_FagLL]     FLOAT (53)   NOT NULL,
    [M_FagUL]     FLOAT (53)   NOT NULL,
    [L_FagLL]     FLOAT (53)   NOT NULL,
    [L_FagUL]     FLOAT (53)   NOT NULL,
    CONSTRAINT [PK_ValveLimits] PRIMARY KEY CLUSTERED ([PartNumber] ASC) WITH (FILLFACTOR = 80)
);

