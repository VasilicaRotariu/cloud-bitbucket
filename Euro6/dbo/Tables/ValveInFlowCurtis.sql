﻿CREATE TABLE [dbo].[ValveInFlowCurtis] (
    [Serial]             VARCHAR (13)  NOT NULL,
    [PDate]              DATETIME      NOT NULL,
    [FailCode]           SMALLINT      NOT NULL,
    [UncalibratedResult] FLOAT (53)    NULL,
    [CalibratedResult]   FLOAT (53)    NULL,
    [FlowPressure]       FLOAT (53)    NULL,
    [Temperature]        FLOAT (53)    NULL,
    [TestHead]           INT           NOT NULL,
    [AssetNumber]        SMALLINT      NOT NULL,
    [TDate]              DATETIME2 (7) CONSTRAINT [DF__ValveInFl__TDate__00075F81] DEFAULT (sysdatetime()) NOT NULL,
    CONSTRAINT [PK_ValveInFlowCurtis] PRIMARY KEY CLUSTERED ([TDate] DESC) WITH (FILLFACTOR = 90)
);

