﻿CREATE TABLE [dbo].[SQUAWKEmu] (
    [UID]         INT           IDENTITY (1, 1) NOT NULL,
    [PDate]       DATETIME2 (7) CONSTRAINT [DF_SQUAWKEmu_PDate] DEFAULT (sysutcdatetime()) NOT NULL,
    [Serial]      VARCHAR (25)  NOT NULL,
    [AssetNumber] SMALLINT      NOT NULL,
    [Component]   VARCHAR (25)  NOT NULL,
    [Data]        REAL          NOT NULL,
    [Data1]       TINYINT       NULL,
    [Data2]       SMALLINT      CONSTRAINT [DF_SQUAWKEmu_Data2] DEFAULT ((0)) NOT NULL,
    [Result]      BIT           NULL,
    CONSTRAINT [PK_SQUAWKEmu] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 80)
);

