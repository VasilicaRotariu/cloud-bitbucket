﻿CREATE TABLE [dbo].[ShiftProductionMonitorTargets] (
    [Date]             DATE     CONSTRAINT [DF_ShiftProductionMonitorTargets_Date] DEFAULT (getdate()) NOT NULL,
    [NightShiftTarget] SMALLINT CONSTRAINT [DF_ShiftProductionMonitorTargets_NightShiftTarget] DEFAULT ((900)) NOT NULL,
    [EarlyShiftTarget] SMALLINT CONSTRAINT [DF_ShiftProductionMonitorTargets_EarlyShiftTarget] DEFAULT ((900)) NOT NULL,
    [LateShiftTarget]  SMALLINT CONSTRAINT [DF_ShiftProductionMonitorTargets_LateShiftTarget] DEFAULT ((900)) NOT NULL,
    CONSTRAINT [PK_ShiftProductionMonitorTargets] PRIMARY KEY CLUSTERED ([Date] ASC) WITH (FILLFACTOR = 80)
);

