﻿CREATE TABLE [dbo].[MASMEC_DayReportCardEntries] (
    [ID]          INT           IDENTITY (1, 1) NOT NULL,
    [Date]        DATETIME      NOT NULL,
    [Planned]     TIME (7)      CONSTRAINT [DF__DayReport__Plann__787EE5A0] DEFAULT ('00:00:00') NOT NULL,
    [WED]         TIME (7)      CONSTRAINT [DF__DayReportCa__WED__797309D9] DEFAULT ('00:00:00') NOT NULL,
    [PED]         TIME (7)      CONSTRAINT [DF__DayReportCa__PED__7A672E12] DEFAULT ('00:00:00') NOT NULL,
    [Starved]     TIME (7)      CONSTRAINT [DF__DayReport__Starv__7B5B524B] DEFAULT ('00:00:00') NOT NULL,
    [Labour]      TIME (7)      CONSTRAINT [DF__DayReport__Labou__7C4F7684] DEFAULT ('00:00:00') NOT NULL,
    [Setting]     TIME (7)      CONSTRAINT [DF__DayReport__Setti__7D439ABD] DEFAULT ('00:00:00') NOT NULL,
    [ChangeOver]  TIME (7)      CONSTRAINT [DF__DayReport__Chang__7E37BEF6] DEFAULT ('00:00:00') NOT NULL,
    [Masters]     TIME (7)      CONSTRAINT [DF__DayReport__Maste__7F2BE32F] DEFAULT ('00:00:00') NOT NULL,
    [Comment]     VARCHAR (500) CONSTRAINT [DF__DayReport__Comme__00200768] DEFAULT ('') NOT NULL,
    [CommentLock] BIT           CONSTRAINT [DF__DayReport__Comme__160F4887] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_MASMEC_DayReportCardEntries] PRIMARY KEY CLUSTERED ([Date] ASC) WITH (FILLFACTOR = 80)
);

