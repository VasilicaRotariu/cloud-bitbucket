﻿CREATE TABLE [dbo].[SelAssy_MasterMeasurementFeatures] (
    [MasterMeasurementFeature_UID]             INT        IDENTITY (1, 1) NOT NULL,
    [Master_FID]                               INT        NOT NULL,
    [SelAssy_MasterMeasurementFeatureType_FID] INT        NOT NULL,
    [CalibratedValue]                          FLOAT (53) NOT NULL,
    CONSTRAINT [FK_SelAssy_MasterMeasurementFeature_SelAssy_Masters] FOREIGN KEY ([Master_FID]) REFERENCES [dbo].[SelAssy_Masters] ([Master_UID]) ON DELETE CASCADE,
    CONSTRAINT [FK_SelAssy_MasterMeasurementFeatures_SelAssy_MasterMeasurementFeatureTypes] FOREIGN KEY ([SelAssy_MasterMeasurementFeatureType_FID]) REFERENCES [dbo].[SelAssy_MasterMeasurementFeatureTypes] ([SelAssy_MasterMeasurementFeatureType_UID]) ON DELETE CASCADE
);


GO
CREATE UNIQUE CLUSTERED INDEX [PK_SelAssy_MasterMeasurementFeature]
    ON [dbo].[SelAssy_MasterMeasurementFeatures]([MasterMeasurementFeature_UID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SelAssy_MasterMeasurementFeatures_U1]
    ON [dbo].[SelAssy_MasterMeasurementFeatures]([Master_FID] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[U1]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SelAssy_MasterMeasurementFeatures', @level2type = N'COLUMN', @level2name = N'Master_FID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[U1]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SelAssy_MasterMeasurementFeatures', @level2type = N'COLUMN', @level2name = N'SelAssy_MasterMeasurementFeatureType_FID';

