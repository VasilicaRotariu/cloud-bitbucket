﻿CREATE TABLE [dbo].[SelAssyAssembly] (
    [UID]                              BIGINT        NOT NULL,
    [UTC]                              DATETIME      NULL,
    [PlantNumber]                      SMALLINT      NULL,
    [PinIdentification]                BIGINT        NULL,
    [PinAssemblyAttempts]              TINYINT       NULL,
    [PinSizeNeeded]                    FLOAT (53)    NULL,
    [PinSizeUsed]                      FLOAT (53)    NULL,
    [PinUTCPinUsed]                    DATETIME      NULL,
    [BodyMatrix]                       VARCHAR (11)  NULL,
    [PinType]                          VARCHAR (12)  NULL,
    [BodyType]                         VARCHAR (12)  NULL,
    [AssemblyType]                     VARCHAR (40)  NULL,
    [BodyAssemblyAttempts]             TINYINT       NULL,
    [InsertionLength]                  FLOAT (53)    NULL,
    [InsertionLengthResult]            TINYINT       NULL,
    [InsertionForce]                   FLOAT (53)    NULL,
    [AssemblyLiftPredicted]            FLOAT (53)    NULL,
    [AssemblyLift]                     FLOAT (53)    NULL,
    [AssemblyLiftResult]               TINYINT       NULL,
    [AssemblyLiftGaugeForce]           FLOAT (53)    NULL,
    [AssemblyLiftGaugeForceResult]     TINYINT       NULL,
    [AssemblyClearancePredicted]       FLOAT (53)    NULL,
    [AssemblyClearance]                FLOAT (53)    NULL,
    [AssemblyClearanceResult]          TINYINT       NULL,
    [AssemblyCollarClearancePredicted] FLOAT (53)    NULL,
    [AssemblyCollarClearance]          FLOAT (53)    NULL,
    [AssemblyCollarClearanceResult]    TINYINT       NULL,
    [AssemblyTopSeatLeak]              FLOAT (53)    NULL,
    [AssemblyTopSeatLeakResult]        TINYINT       NULL,
    [AssemblyBtmSeatLeak]              FLOAT (53)    NULL,
    [AssemblyBtmSeatLeakResult]        TINYINT       NULL,
    [AssemblyAreaTemperature]          FLOAT (53)    NULL,
    [FailureCode]                      SMALLINT      NULL,
    [AssemblyClearance_mm]             FLOAT (53)    NULL,
    [AssemblyCollarClearance_mm]       FLOAT (53)    NULL,
    [AssemblyClearance2]               FLOAT (53)    NULL,
    [AssemblyClearance2_mm]            FLOAT (53)    NULL,
    [TDate]                            DATETIME2 (7) CONSTRAINT [DF_SelAssyAssembly_TDate] DEFAULT (sysdatetime()) NOT NULL,
    CONSTRAINT [PK_SelAssyAssembly] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_SelAssyAssembly_UTCASC]
    ON [dbo].[SelAssyAssembly]([UTC] ASC)
    INCLUDE([UID], [PlantNumber], [PinIdentification], [PinAssemblyAttempts], [PinSizeNeeded], [PinSizeUsed], [PinUTCPinUsed], [BodyMatrix], [PinType], [BodyType], [AssemblyType], [BodyAssemblyAttempts], [InsertionLength], [InsertionLengthResult], [InsertionForce], [AssemblyLiftPredicted], [AssemblyLift], [AssemblyLiftResult], [AssemblyLiftGaugeForce], [AssemblyLiftGaugeForceResult], [AssemblyClearancePredicted], [AssemblyClearance], [AssemblyClearanceResult], [AssemblyCollarClearancePredicted], [AssemblyCollarClearance], [AssemblyCollarClearanceResult], [AssemblyTopSeatLeak], [AssemblyTopSeatLeakResult], [AssemblyBtmSeatLeak], [AssemblyBtmSeatLeakResult], [AssemblyAreaTemperature], [FailureCode], [AssemblyClearance_mm], [AssemblyCollarClearance_mm], [AssemblyClearance2], [AssemblyClearance2_mm], [TDate]);

