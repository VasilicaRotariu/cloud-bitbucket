﻿CREATE TABLE [dbo].[NNCFW420_Results] (
    [UID]                          INT          IDENTITY (1, 1) NOT NULL,
    [AssetNumber]                  INT          NOT NULL,
    [PDate]                        DATETIME     NOT NULL,
    [NozzleSerial]                 VARCHAR (25) NOT NULL,
    [PistonGuideSerial]            VARCHAR (25) NOT NULL,
    [PistonGuide_InnerDiameterAvg] FLOAT (53)   NOT NULL,
    [PistonGuide_Taper]            FLOAT (53)   NOT NULL,
    [Needle_Taper]                 FLOAT (53)   NOT NULL,
    [Capsule_NeedleLift]           FLOAT (53)   NOT NULL,
    [Capsule_Clearance]            FLOAT (53)   NOT NULL,
    [Capsule_VIN]                  FLOAT (53)   NOT NULL,
    [Capsule_NOP_Diameter]         FLOAT (53)   NOT NULL,
    [Capsule_NOP_Pressure]         FLOAT (53)   NOT NULL,
    [Failcode]                     INT          NOT NULL
);


GO
CREATE CLUSTERED INDEX [IX_NNCFW420_PDateDESC]
    ON [dbo].[NNCFW420_Results]([PDate] DESC, [NozzleSerial] ASC) WITH (FILLFACTOR = 90);

