﻿CREATE TABLE [dbo].[MatchGrind] (
    [PDate]               DATETIME     NOT NULL,
    [Nozzle_SerialNumber] VARCHAR (13) NOT NULL,
    [PG_SerialNumber]     VARCHAR (13) NOT NULL,
    [Result]              INT          NOT NULL,
    [PGBore]              FLOAT (53)   NULL,
    [RefLift]             FLOAT (53)   NULL,
    [Lift]                FLOAT (53)   NULL,
    [Taper]               FLOAT (53)   NULL,
    [Clearance]           FLOAT (53)   NULL,
    [VM1]                 FLOAT (53)   NULL,
    [VM2]                 FLOAT (53)   NULL
);

