﻿CREATE TABLE [dbo].[ProductLaserMarking_TEMP] (
    [SerialNumber]          VARCHAR (12)  NULL,
    [PDate]                 DATETIME2 (7) NULL,
    [TDate]                 DATETIME2 (7) CONSTRAINT [DF_ProductLaserMarking_TEMP_TDate] DEFAULT (sysutcdatetime()) NULL,
    [AssetNumber]           VARCHAR (12)  CONSTRAINT [DF_ProductLaserMarking_TEMP_AssetNumber] DEFAULT (sysutcdatetime()) NULL,
    [PartNumber]            VARCHAR (12)  NULL,
    [TrimType]              VARCHAR (12)  NULL,
    [DataMatrixString]      VARCHAR (16)  NULL,
    [MatrixGrade]           VARCHAR (1)   NULL,
    [TagString]             VARCHAR (305) NULL,
    [DMResult]              BIT           NULL,
    [Contrast]              FLOAT (53)    NULL,
    [Uniformity]            FLOAT (53)    NULL,
    [DMRead]                VARCHAR (50)  NULL,
    [FixedPatternDamage]    FLOAT (53)    NULL,
    [TwoDimModulation]      FLOAT (53)    NULL,
    [UnusedErrorCorrection] FLOAT (53)    NULL,
    [PhotoTaken]            BIT           NULL
);

