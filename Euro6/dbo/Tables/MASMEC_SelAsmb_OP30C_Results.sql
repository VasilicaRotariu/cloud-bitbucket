﻿CREATE TABLE [dbo].[MASMEC_SelAsmb_OP30C_Results] (
    [Tdate]                          DATETIME     DEFAULT (getdate()) NOT NULL,
    [Idate]                          DATETIME     NOT NULL,
    [Pdate]                          DATETIME     NULL,
    [Serial]                         VARCHAR (12) NOT NULL,
    [LOP]                            VARCHAR (20) NOT NULL,
    [Rework_Version]                 INT          DEFAULT ((0)) NULL,
    [Result]                         INT          NOT NULL,
    [Fail_UID]                       INT          DEFAULT ((0)) NULL,
    [PlantNo]                        INT          DEFAULT ((0)) NULL,
    [Operator_ID]                    VARCHAR (10) DEFAULT ((0)) NULL,
    [Operator_FullName]              VARCHAR (50) DEFAULT ((0)) NULL,
    [Injector_Type]                  INT          NULL,
    [LD500_Program]                  INT          NULL,
    [Leak_Min]                       FLOAT (53)   NULL,
    [Leak_Max]                       FLOAT (53)   NULL,
    [Enable_ReTest_Leak]             INT          NULL,
    [Limit_ReTest_Leak]              FLOAT (53)   NULL,
    [Leakage_Min_LD500_MeasureValue] FLOAT (53)   NULL,
    [Leakage_Max_LD500_MeasureValue] FLOAT (53)   NULL,
    [Leakage_MeasureValue]           FLOAT (53)   NULL,
    [Leakage_Result]                 INT          NULL,
    [Pressure_Leak_Test_LD500]       FLOAT (53)   NULL,
    [UnitMeasure_LeakTest]           INT          NULL,
    [UnitMeasure_PressureTest]       INT          NULL,
    [Result_Code_LD500]              INT          NULL,
    [Resistance_Min]                 FLOAT (53)   NULL,
    [Resistance_Max]                 FLOAT (53)   NULL,
    [Resistance_MeasureValue]        FLOAT (53)   NULL,
    [Resistance_Result]              INT          NULL,
    [ShiftNumber]                    INT          NULL,
    [Line_Id]                        INT          NULL,
    [UID]                            BIGINT       NULL,
    CONSTRAINT [PK_MASMEC_SelAsmb_OP30C_Results] PRIMARY KEY CLUSTERED ([Idate] DESC) WITH (FILLFACTOR = 80),
    CONSTRAINT [fk_MASMEC_SelAsmb_OP30C_Results_ProcessTable] FOREIGN KEY ([Serial]) REFERENCES [dbo].[MASMEC_SelAsmb_ProcessTable] ([Serial]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150302-150032]
    ON [dbo].[MASMEC_SelAsmb_OP30C_Results]([Pdate] ASC) WITH (FILLFACTOR = 90);

