﻿CREATE TABLE [dbo].[ManualAssembly] (
    [PDate]               DATETIME        CONSTRAINT [DF_ManualAssembly_PDate] DEFAULT (getdate()) NOT NULL,
    [Serial]              VARCHAR (50)    NOT NULL,
    [Lift]                DECIMAL (12, 8) NOT NULL,
    [Clearance]           DECIMAL (12, 8) NOT NULL,
    [CalculatedClearance] AS              (power([Clearance],(0.7))*(0.00015)),
    [Result]              INT             NOT NULL,
    [Asset]               INT             CONSTRAINT [DF_ManualAssembly_Asset] DEFAULT ((20562)) NOT NULL
);

