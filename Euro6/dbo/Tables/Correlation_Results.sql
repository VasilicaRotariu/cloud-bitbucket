﻿CREATE TABLE [dbo].[Correlation_Results] (
    [UID]                 INT            NOT NULL,
    [PDate]               DATETIME2 (7)  NOT NULL,
    [Asset]               VARCHAR (5)    NOT NULL,
    [CalibrationStateFID] INT            NOT NULL,
    [Serial]              VARCHAR (13)   NOT NULL,
    [AverageWaterFlow]    FLOAT (53)     NOT NULL,
    [OilFlow]             FLOAT (53)     NOT NULL,
    [DeltaQ]              FLOAT (53)     NOT NULL,
    [Head]                INT            NOT NULL,
    [MessageString]       VARCHAR (1000) NOT NULL,
    [TDate]               DATETIME2 (7)  CONSTRAINT [DF_Correlation_Results_TDate] DEFAULT (sysdatetime()) NOT NULL,
    [ErrorCode]           INT            CONSTRAINT [DF_Correlation_Results_ErrorCode] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Correlation_Results] PRIMARY KEY CLUSTERED ([UID] ASC)
);

