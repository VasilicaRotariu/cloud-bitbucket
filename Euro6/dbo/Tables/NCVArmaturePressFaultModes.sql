﻿CREATE TABLE [dbo].[NCVArmaturePressFaultModes] (
    [UID]              INT           NOT NULL,
    [FaultDescription] VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_FaultModes] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 80)
);

