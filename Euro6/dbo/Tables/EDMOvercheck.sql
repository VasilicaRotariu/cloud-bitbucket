﻿CREATE TABLE [dbo].[EDMOvercheck] (
    [AssetNumber]                     INT           NULL,
    [NozzleType]                      VARCHAR (50)  NULL,
    [DateTime]                        DATETIME      NULL,
    [CalibrationState]                VARCHAR (50)  NULL,
    [SerialNumber]                    VARCHAR (50)  NULL,
    [FlowDifference]                  FLOAT (53)    NULL,
    [DeltaQ]                          FLOAT (53)    NULL,
    [NominalFlowValues]               FLOAT (53)    NULL,
    [AverageWaterFlow]                FLOAT (53)    NULL,
    [OilFlow]                         FLOAT (53)    NULL,
    [WaterDifference]                 FLOAT (53)    NULL,
    [WaterTotal]                      FLOAT (53)    NULL,
    [Water1]                          FLOAT (53)    NULL,
    [Water2]                          FLOAT (53)    NULL,
    [Water3]                          FLOAT (53)    NULL,
    [HeadNumber]                      INT           NULL,
    [FlowDifferencePercentageOfError] FLOAT (53)    NULL,
    [UID]                             DATETIME2 (7) NULL,
    [CalibrationCheck]                BIT           NULL
);

