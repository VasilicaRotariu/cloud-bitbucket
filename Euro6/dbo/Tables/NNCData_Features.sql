﻿CREATE TABLE [dbo].[NNCData_Features] (
    [MachineID]                 INT          NOT NULL,
    [DataColumn]                VARCHAR (50) NOT NULL,
    [FeatureName]               VARCHAR (50) NOT NULL,
    [Enabled_ReworkViewer]      BIT          NOT NULL,
    [Enabled_Analyser]          BIT          NOT NULL,
    [Enabled_EOLCheck]          BIT          NOT NULL,
    [Enabled_EventLogging]      BIT          NULL,
    [Enabled_Pareto]            BIT          NULL,
    [Enable_EL_FromErrorColumn] BIT          DEFAULT ((0)) NOT NULL,
    [ErrorColumn]               VARCHAR (50) NULL
);

