﻿CREATE TABLE [dbo].[MASMEC_SelAsmb_StateCodes] (
    [ID]          INT          NOT NULL,
    [Description] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_MASMEC_SelAsmb_StateCodes] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

