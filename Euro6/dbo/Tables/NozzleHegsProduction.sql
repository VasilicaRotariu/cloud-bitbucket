﻿CREATE TABLE [dbo].[NozzleHegsProduction] (
    [PDate]                DATETIME      NOT NULL,
    [SerialNum]            VARCHAR (50)  NOT NULL,
    [Product]              VARCHAR (50)  NULL,
    [Type]                 VARCHAR (50)  NULL,
    [FailCode]             INT           NULL,
    [FailCodeText]         VARCHAR (MAX) NULL,
    [FromWhichPosalux]     VARCHAR (50)  NULL,
    [FromWhichPosaluxHead] INT           NULL,
    [PM_Pressure]          FLOAT (53)    NULL,
    [PM_Flow]              FLOAT (53)    NULL,
    [PM_Density]           FLOAT (53)    NULL,
    [PM_UsedDensity]       FLOAT (53)    NULL,
    [PM_Temperature]       FLOAT (53)    NULL,
    [PM_Pressure1BP]       FLOAT (53)    NULL,
    [PM_Pressure2BP]       FLOAT (53)    NULL,
    [PM_FlowBP]            FLOAT (53)    NULL,
    [PM_DeltaQ]            FLOAT (53)    NULL,
    [PM_DensityBP]         FLOAT (53)    NULL,
    [PM_UsedDensityBP]     FLOAT (53)    NULL,
    [PM_TemperatureBP]     FLOAT (53)    NULL,
    [PM_MeasureTime]       FLOAT (53)    NULL,
    [PM_CycleTime]         FLOAT (53)    NULL,
    [PM_NestNr]            INT           NULL,
    [G_Pressure]           FLOAT (53)    NULL,
    [G_FlowStart]          FLOAT (53)    NULL,
    [G_FlowStop]           FLOAT (53)    NULL,
    [G_Temperature]        FLOAT (53)    NULL,
    [G_Density]            FLOAT (53)    NULL,
    [G_UsedDensity]        FLOAT (53)    NULL,
    [G_GrindTime]          FLOAT (53)    NULL,
    [G_CycleTime]          FLOAT (53)    NULL,
    [G_NestNr]             INT           NULL,
    [G_RegulationOn]       BIT           NULL,
    [G_UseGrindTimeCorr]   BIT           NULL,
    [G_UseControlledCD]    BIT           NULL,
    [G_UseDeltaQ]          BIT           NULL,
    [USED_FACTOR1]         FLOAT (53)    NULL,
    [USED_FACTOR2]         FLOAT (53)    NULL,
    [G_Error]              INT           NULL,
    [FM_Pressure]          FLOAT (53)    NULL,
    [FM_Flow]              FLOAT (53)    NULL,
    [FM_Density]           FLOAT (53)    NULL,
    [FM_UsedDensity]       FLOAT (53)    NULL,
    [FM_Temperature]       FLOAT (53)    NULL,
    [FM_Pressure1BP]       FLOAT (53)    NULL,
    [FM_Pressure2BP]       FLOAT (53)    NULL,
    [FM_FlowBP]            FLOAT (53)    NULL,
    [FM_DeltaQ]            FLOAT (53)    NULL,
    [FM_DensityBP]         FLOAT (53)    NULL,
    [FM_UsedDensityBP]     FLOAT (53)    NULL,
    [FM_TemperatureBP]     FLOAT (53)    NULL,
    [FM_MeasureTime]       FLOAT (53)    NULL,
    [FM_CycleTime]         FLOAT (53)    NULL,
    [FM_NestNr]            INT           NULL,
    [TDate]                DATETIME2 (7) NULL,
    [PlantNumber]          INT           NULL,
    [SerialNumCount]       INT           NULL
);


GO
CREATE CLUSTERED INDEX [ClusteredIndex-20151120-093842]
    ON [dbo].[NozzleHegsProduction]([TDate] ASC, [PlantNumber] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [PreviousProcess]
    ON [dbo].[NozzleHegsProduction]([SerialNum] ASC)
    INCLUDE([FailCode], [PDate]) WITH (FILLFACTOR = 90);

