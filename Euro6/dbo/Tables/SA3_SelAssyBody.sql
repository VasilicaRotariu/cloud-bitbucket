﻿CREATE TABLE [dbo].[SA3_SelAssyBody] (
    [UID]                                BIGINT        IDENTITY (1, 1) NOT NULL,
    [UTC]                                DATETIME      NULL,
    [PlantNumber]                        SMALLINT      NULL,
    [BodyTrayMatrix]                     VARCHAR (5)   NULL,
    [BodyDrawMatrix]                     VARCHAR (4)   NULL,
    [BodyMatrix]                         VARCHAR (11)  NULL,
    [BodyType]                           VARCHAR (12)  NULL,
    [BodyGrade]                          VARCHAR (2)   NULL,
    [BodySeatLength]                     FLOAT (53)    NULL,
    [BodySeatLengthPlusGaugePlaneOffset] FLOAT (53)    NULL,
    [BodySeatLengthResult]               TINYINT       NULL,
    [BodyDiameterTop]                    FLOAT (53)    NULL,
    [BodyDiameterBtm]                    FLOAT (53)    NULL,
    [BodyDiameterAve]                    FLOAT (53)    NULL,
    [BodyDiameterAveResult]              TINYINT       NULL,
    [BodyTaper]                          FLOAT (53)    NULL,
    [BodyTaperResult]                    TINYINT       NULL,
    [BodyCollarDiameter]                 FLOAT (53)    NULL,
    [BodyCollarDiameterResult]           TINYINT       NULL,
    [BodyUsage]                          TINYINT       NULL,
    [BodyAreaTemperature]                FLOAT (53)    NULL,
    [FailureCode]                        SMALLINT      NULL,
    [TDate]                              DATETIME2 (7) CONSTRAINT [DF_SA3_SelAssyBody_TDate] DEFAULT (sysdatetime()) NOT NULL,
    CONSTRAINT [PK_SA3_SelAssyBody] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_BodyType_BodySeatLength]
    ON [dbo].[SA3_SelAssyBody]([BodyType] ASC, [BodySeatLength] ASC)
    INCLUDE([UTC]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_BodyType_BodyDiameterAve]
    ON [dbo].[SA3_SelAssyBody]([BodyType] ASC, [BodyDiameterAve] ASC)
    INCLUDE([UTC], [BodyDiameterTop], [BodyDiameterBtm], [BodyTaper]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_BodyType_BodyDiameter_BodyCollarDiameter]
    ON [dbo].[SA3_SelAssyBody]([BodyType] ASC, [BodyDiameterAve] ASC, [BodyCollarDiameter] ASC)
    INCLUDE([UTC]) WITH (FILLFACTOR = 90);

