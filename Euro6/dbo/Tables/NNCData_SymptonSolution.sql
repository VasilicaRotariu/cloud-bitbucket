﻿CREATE TABLE [dbo].[NNCData_SymptonSolution] (
    [MachineCategory] INT          NOT NULL,
    [FeatureName]     VARCHAR (50) NOT NULL,
    [Problem]         VARCHAR (50) NULL,
    [ProblemID]       VARCHAR (6)  NULL
);


GO
CREATE CLUSTERED INDEX [ClusteredIndex-20150813-075759]
    ON [dbo].[NNCData_SymptonSolution]([MachineCategory] ASC, [FeatureName] ASC);

