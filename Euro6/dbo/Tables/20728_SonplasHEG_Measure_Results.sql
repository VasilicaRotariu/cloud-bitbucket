﻿CREATE TABLE [dbo].[20728_SonplasHEG_Measure_Results] (
    [Uid]           INT          IDENTITY (1, 1) NOT NULL,
    [PDate]         DATETIME     NOT NULL,
    [MachineTime]   DATETIME     NULL,
    [Serial]        VARCHAR (20) NULL,
    [Flow]          FLOAT (53)   NULL,
    [Flow2]         FLOAT (53)   NULL,
    [Pressure]      FLOAT (53)   NULL,
    [BackPressure]  FLOAT (53)   NULL,
    [Pressure2]     FLOAT (53)   NULL,
    [BackPressure2] FLOAT (53)   NULL,
    [MeasureTime]   FLOAT (53)   NULL,
    [Temperature]   FLOAT (53)   NULL,
    [Offset]        FLOAT (53)   NULL,
    [Offset2]       FLOAT (53)   NULL,
    [Error]         INT          NULL,
    [DeltaQ]        FLOAT (53)   NULL,
    [MeasureTime2]  FLOAT (53)   NULL,
    [Temperature2]  FLOAT (53)   NULL,
    [FlowReverse]   FLOAT (53)   NULL,
    [Flow2Reverse]  FLOAT (53)   NULL
);


GO
CREATE CLUSTERED INDEX [IX_20728_SonplasHEG_Measure_Results]
    ON [dbo].[20728_SonplasHEG_Measure_Results]([PDate] DESC, [Serial] ASC);

