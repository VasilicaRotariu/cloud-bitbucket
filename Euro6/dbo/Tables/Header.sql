﻿CREATE TABLE [dbo].[Header] (
    [hid]          BIGINT        IDENTITY (1, 1) NOT NULL,
    [TestNo]       BIGINT        NOT NULL,
    [ISN]          VARCHAR (13)  NULL,
    [TestPlan]     VARCHAR (20)  NULL,
    [Vacuum]       VARCHAR (10)  NULL,
    [CoilRes]      FLOAT (53)    NULL,
    [CoilLeak]     FLOAT (53)    NULL,
    [MDP1]         INT           NULL,
    [MDP2]         INT           NULL,
    [MDP3]         INT           NULL,
    [MDP4]         INT           NULL,
    [MDP5]         INT           NULL,
    [BLStatic]     FLOAT (53)    NULL,
    [BLDynamic]    FLOAT (53)    NULL,
    [AEASerial]    VARCHAR (12)  NULL,
    [DelphiSerial] VARCHAR (12)  NULL,
    [TrimCode]     VARCHAR (8)   NULL,
    [Batch]        VARCHAR (20)  NULL,
    [Comment]      VARCHAR (255) NULL,
    [Date]         DATETIME      NULL,
    [Result]       TINYINT       NULL,
    [ReworkID]     INT           NULL,
    [Trim1]        FLOAT (53)    NULL,
    [Trim2]        FLOAT (53)    NULL,
    [Trim3]        FLOAT (53)    NULL,
    [Trim4]        FLOAT (53)    NULL,
    [Trim5]        FLOAT (53)    NULL,
    [P1-B1_L]      INT           NULL,
    [P1-B1_H]      INT           NULL,
    [P1-B2_L]      INT           NULL,
    [P1-B2_H]      INT           NULL,
    [P1-B3_L]      INT           NULL,
    [P1-B3_H]      INT           NULL,
    [P1-B4_L]      INT           NULL,
    [P1-B4_H]      INT           NULL,
    [P1-B5_L]      INT           NULL,
    [P1-B5_H]      INT           NULL,
    [P2-B1_L]      INT           NULL,
    [P2-B1_H]      INT           NULL,
    [P2-B2_L]      INT           NULL,
    [P2-B2_H]      INT           NULL,
    [P2-B3_L]      INT           NULL,
    [P2-B3_H]      INT           NULL,
    [P2-B4_L]      INT           NULL,
    [P2-B4_H]      INT           NULL,
    [P2-B5_L]      INT           NULL,
    [P2-B5_H]      INT           NULL,
    [P3-B1_L]      INT           NULL,
    [P3-B1_H]      INT           NULL,
    [P3-B2_L]      INT           NULL,
    [P3-B2_H]      INT           NULL,
    [P3-B3_L]      INT           NULL,
    [P3-B3_H]      INT           NULL,
    [P3-B4_L]      INT           NULL,
    [P3-B4_H]      INT           NULL,
    [P3-B5_L]      INT           NULL,
    [P3-B5_H]      INT           NULL,
    [P4-B1_L]      INT           NULL,
    [P4-B1_H]      INT           NULL,
    [P4-B2_L]      INT           NULL,
    [P4-B2_H]      INT           NULL,
    [P4-B3_L]      INT           NULL,
    [P4-B3_H]      INT           NULL,
    [P4-B4_L]      INT           NULL,
    [P4-B4_H]      INT           NULL,
    [P4-B5_L]      INT           NULL,
    [P4-B5_H]      INT           NULL,
    [P5-B1_L]      INT           NULL,
    [P5-B1_H]      INT           NULL,
    [P5-B2_L]      INT           NULL,
    [P5-B2_H]      INT           NULL,
    [P5-B3_L]      INT           NULL,
    [P5-B3_H]      INT           NULL,
    [P5-B4_L]      INT           NULL,
    [P5-B4_H]      INT           NULL,
    [P5-B5_L]      INT           NULL,
    [P5-B5_H]      INT           NULL
);


GO
CREATE CLUSTERED INDEX [idx_Header_HID]
    ON [dbo].[Header]([hid] ASC);


GO
CREATE NONCLUSTERED INDEX [idx_Header_AEASerial]
    ON [dbo].[Header]([AEASerial] ASC);


GO
CREATE NONCLUSTERED INDEX [idx_Header_Batch]
    ON [dbo].[Header]([Batch] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Header]
    ON [dbo].[Header]([Date] DESC);


GO
CREATE NONCLUSTERED INDEX [idx_Header_ISN]
    ON [dbo].[Header]([ISN] ASC);


GO
CREATE NONCLUSTERED INDEX [idx_Header_A]
    ON [dbo].[Header]([TestNo] ASC);


GO
CREATE NONCLUSTERED INDEX [idx_Header_B]
    ON [dbo].[Header]([TestNo] ASC)
    INCLUDE([BLDynamic], [BLStatic], [Date], [MDP1], [Result], [Trim1]);


GO
CREATE NONCLUSTERED INDEX [idx_Header_TestPlan]
    ON [dbo].[Header]([TestPlan] ASC);


GO
CREATE NONCLUSTERED INDEX [<Name of Missing Index, sysname,>]
    ON [dbo].[Header]([TestPlan] ASC, [Batch] ASC)
    INCLUDE([AEASerial], [BLDynamic], [BLStatic], [Comment], [Date], [ISN], [MDP1], [Result], [TestNo], [Trim1], [TrimCode]);


GO
CREATE NONCLUSTERED INDEX [Result_TestPlanBatchDate]
    ON [dbo].[Header]([TestPlan] ASC, [Batch] ASC, [Date] ASC)
    INCLUDE([Result]);


GO
CREATE NONCLUSTERED INDEX [idx_Header_TrimCode]
    ON [dbo].[Header]([TrimCode] ASC);

