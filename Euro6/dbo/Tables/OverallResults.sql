﻿CREATE TABLE [dbo].[OverallResults] (
    [Injector_ID]            VARCHAR (10) NOT NULL,
    [InjectorCode]           VARCHAR (10) NOT NULL,
    [InDate]                 DATETIME     NOT NULL,
    [OutDate]                DATETIME     NOT NULL,
    [LineID]                 SMALLINT     NOT NULL,
    [Station1]               SMALLINT     NOT NULL,
    [Station2]               SMALLINT     NOT NULL,
    [Station4]               SMALLINT     NOT NULL,
    [Station5]               SMALLINT     NOT NULL,
    [Station6]               SMALLINT     NOT NULL,
    [ShiftNumber]            SMALLINT     NOT NULL,
    [LastScrap]              SMALLINT     NOT NULL,
    [LastStationScrap]       SMALLINT     NOT NULL,
    [Assembly_Modality_Code] VARCHAR (20) NOT NULL
);

