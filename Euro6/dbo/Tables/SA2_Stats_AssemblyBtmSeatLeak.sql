﻿CREATE TABLE [dbo].[SA2_Stats_AssemblyBtmSeatLeak] (
    [AssemblyBtmSeatLeakStatUID]       INT             IDENTITY (1, 1) NOT NULL,
    [AssemblyBtmSeatLeakStatUTC]       DATETIME2 (7)   NOT NULL,
    [AssemblyUID]                      INT             NOT NULL,
    [AssemblyType]                     VARCHAR (50)    NOT NULL,
    [BodyUID]                          INT             NOT NULL,
    [BodyMatrixCode]                   VARCHAR (50)    NOT NULL,
    [BodyType]                         VARCHAR (50)    NOT NULL,
    [BodyBtmSeatLeakFailCount]         INT             NOT NULL,
    [BodyBtmSeatLeakMeasurementCount]  INT             NOT NULL,
    [PinUID]                           INT             NOT NULL,
    [PinGUID]                          VARCHAR (50)    NOT NULL,
    [PinType]                          VARCHAR (50)    NOT NULL,
    [PinBtmSeatLeakFailCount]          INT             NOT NULL,
    [PinBtmSeatLeakMeasurementCount]   INT             NOT NULL,
    [AssemblyBtmSeatLeak]              DECIMAL (12, 8) NOT NULL,
    [AssemblyBtmSeatLeakUTL]           DECIMAL (12, 8) NOT NULL,
    [AssemblyBtmSeatLeakLTL]           DECIMAL (12, 8) NOT NULL,
    [AssemblyBtmSeatLeakDetermination] INT             NOT NULL,
    CONSTRAINT [PK_Stats_AssemblyBtmSeatLeak] PRIMARY KEY CLUSTERED ([AssemblyBtmSeatLeakStatUID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_AssemblyBtmSeatLeak_Det]
    ON [dbo].[SA2_Stats_AssemblyBtmSeatLeak]([AssemblyBtmSeatLeakDetermination] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_AssemblyBtmSeatLeak_UTC]
    ON [dbo].[SA2_Stats_AssemblyBtmSeatLeak]([AssemblyBtmSeatLeakStatUTC] ASC);

