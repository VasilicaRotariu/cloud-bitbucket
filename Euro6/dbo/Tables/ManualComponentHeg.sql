﻿CREATE TABLE [dbo].[ManualComponentHeg] (
    [TDate]                      DATETIME2 (7) NOT NULL,
    [PlantNumber]                INT           NOT NULL,
    [PDate]                      DATETIME      NOT NULL,
    [Serial]                     VARCHAR (20)  NOT NULL,
    [Comment]                    VARCHAR (512) NULL,
    [Description]                VARCHAR (255) NULL,
    [PM_Flow]                    FLOAT (53)    NULL,
    [PM_Flow2]                   FLOAT (53)    NULL,
    [PM_Temperature]             FLOAT (53)    NULL,
    [PM_Error]                   INT           NULL,
    [PM_DeltaQ]                  FLOAT (53)    NULL,
    [PM_Temperature2]            FLOAT (53)    NULL,
    [PM_FlowReverse]             FLOAT (53)    NULL,
    [PM_Flow2Reverse]            FLOAT (53)    NULL,
    [G_Pressure]                 FLOAT (53)    NULL,
    [G_HoningTime]               FLOAT (53)    NULL,
    [G_Temperature]              FLOAT (53)    NULL,
    [G_Density]                  FLOAT (53)    NULL,
    [G_Error]                    INT           NULL,
    [G_Factor]                   FLOAT (53)    NULL,
    [G_UseGrindTimeCorrection]   INT           NULL,
    [G_Gain1]                    FLOAT (53)    NULL,
    [G_Offset1]                  FLOAT (53)    NULL,
    [G_UseControlCoeffDischarge] INT           NULL,
    [G_Gain2]                    FLOAT (53)    NULL,
    [G_Offset2]                  FLOAT (53)    NULL,
    [G_RealStopFlow]             FLOAT (53)    NULL,
    [G_Flow]                     FLOAT (53)    NULL,
    [FM_Flow]                    FLOAT (53)    NULL,
    [FM_Flow2]                   FLOAT (53)    NULL,
    [FM_Temperature]             FLOAT (53)    NULL,
    [FM_Error]                   INT           NULL,
    [FM_DeltaQ]                  FLOAT (53)    NULL,
    [FM_Temperature2]            FLOAT (53)    NULL,
    [FM_FlowReverse]             FLOAT (53)    NULL,
    [FM_Flow2Reverse]            FLOAT (53)    NULL,
    [Nest]                       INT           NULL,
    [UnloadCode]                 INT           NULL,
    [IDate]                      DATETIME      CONSTRAINT [DF_ManualComponentHeg_IDate] DEFAULT (getdate()) NOT NULL,
    [SerialCount]                INT           NULL,
    CONSTRAINT [PK_ManualComponentHeg] PRIMARY KEY CLUSTERED ([TDate] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170117-122434]
    ON [dbo].[ManualComponentHeg]([Serial] ASC)
    INCLUDE([Comment], [Description], [FM_DeltaQ], [FM_Error], [FM_Flow], [FM_Flow2], [FM_Flow2Reverse], [FM_FlowReverse], [FM_Temperature], [FM_Temperature2], [G_Density], [G_Error], [G_Factor], [G_Flow], [G_Gain1], [G_Gain2], [G_HoningTime], [G_Offset1], [G_Offset2], [G_Pressure], [G_RealStopFlow], [G_Temperature], [G_UseControlCoeffDischarge], [G_UseGrindTimeCorrection], [IDate], [Nest], [PDate], [PlantNumber], [PM_DeltaQ], [PM_Error], [PM_Flow], [PM_Flow2], [PM_Flow2Reverse], [PM_FlowReverse], [PM_Temperature], [PM_Temperature2], [SerialCount], [TDate], [UnloadCode]) WITH (FILLFACTOR = 90);


GO


-- =============================================
-- Author:		Guillaume Carre 
-- Create date: 23/09/2014
-- Description:	trigger counting the number of time this component has been built 
-- =============================================
CREATE TRIGGER [dbo].[AfterInsertManualComponentHeg] ON [dbo].[ManualComponentHeg] AFTER INSERT
AS
    BEGIN
        -- Don't show messages 
        SET NOCOUNT ON;
        -- Temporary table to hold records to update
        DECLARE @tbl TABLE([Row] INT, [PDate] DATETIME, [Serial] VARCHAR(max));
        -- Insert records to update
        INSERT INTO @tbl
        SELECT TOP 50 ROW_NUMBER() OVER(ORDER BY [PDate]) AS [Row], [PDate], [Serial] 
        FROM [dbo].[ManualComponentHeg] AS npf
        WHERE [SerialCount] IS NULL
        ORDER BY [PDate] ASC;
        -- Temp variables for loop
        DECLARE @Row INT, @Rows INT, @DMCount INT, @DM VARCHAR(max), @PD DATETIME;
        -- Count rows inserted into temp table
        SELECT @Rows=@@ROWCOUNT;
        -- Start at first row
        SELECT @Row=1;
        -- Loop each row
        WHILE(@Row<=@Rows)
            BEGIN
                -- Grab Matrix & PDate for this loop          
                SELECT @DM=[Serial], @PD=[PDate] FROM @tbl WHERE [Row]=@Row;
                -- Count instances for this matrix prior to this date
                SELECT @DMCount=ISNULL(COUNT(1),0)+1 FROM [dbo].[ManualComponentHeg] WHERE [Serial]=@DM AND [PDate]<@PD;
                -- Update the specific record in the live table
                UPDATE [dbo].[ManualComponentHeg] SET [SerialCount] = @DMCount WHERE [PDate]=@PD AND [Serial]=@DM;
                -- Increment the loop count
                SET @Row = @Row + 1;
            END
        -- Show messages
        SET NOCOUNT OFF;

    END


