﻿CREATE TABLE [dbo].[ManualArmatureProducts] (
    [Product]              VARCHAR (3)   NOT NULL,
    [Part Type]            VARCHAR (12)  NOT NULL,
    [Version]              INT           NOT NULL,
    [Creation Date]        DATETIME2 (7) NOT NULL,
    [Part ID]              VARCHAR (12)  NULL,
    [Assembly Number]      VARCHAR (12)  NULL,
    [Body Part Number]     VARCHAR (12)  NULL,
    [Armature Part Number] VARCHAR (12)  NULL,
    [Pin Part Number]      VARCHAR (12)  NULL,
    [FAG Target]           FLOAT (53)    NOT NULL,
    [FAG Tolerance]        FLOAT (53)    NOT NULL,
    [Tilt Target]          FLOAT (53)    NOT NULL,
    [Tilt Tolerance]       FLOAT (53)    NOT NULL,
    [Piezo Fixed Offset]   FLOAT (53)    NOT NULL,
    [Piezo Load Slope]     FLOAT (53)    NOT NULL,
    [Press Data Set]       VARCHAR (2)   NOT NULL,
    [useTraysRHS]          SMALLINT      NOT NULL,
    [Comment]              VARCHAR (60)  NOT NULL,
    [TDate]                DATETIME2 (7) CONSTRAINT [DF_ManualArmatureProducts_TDate] DEFAULT (sysdatetime()) NOT NULL
);

