﻿CREATE TABLE [dbo].[SelAssy_MasterTypes] (
    [MasterLimitType_UID] INT          IDENTITY (1, 1) NOT NULL,
    [MasterType]          VARCHAR (10) NOT NULL,
    CONSTRAINT [PK_SelAssy_MasterType] PRIMARY KEY CLUSTERED ([MasterLimitType_UID] ASC) WITH (FILLFACTOR = 80)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[U1]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SelAssy_MasterTypes', @level2type = N'COLUMN', @level2name = N'MasterType';

