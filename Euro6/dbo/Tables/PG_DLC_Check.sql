﻿CREATE TABLE [dbo].[PG_DLC_Check] (
    [Uid]         INT           IDENTITY (1, 1) NOT NULL,
    [Pdate]       DATETIME2 (7) NOT NULL,
    [AssetNumber] INT           NOT NULL,
    [Serial]      VARCHAR (20)  NOT NULL,
    [FailCode]    VARCHAR (50)  NOT NULL,
    [FailClass]   VARCHAR (40)  NOT NULL,
    [Count]       INT           NULL,
    [TDate]       DATETIME2 (7) CONSTRAINT [DF_PG_DLC_Check_TDate] DEFAULT (sysdatetime()) NOT NULL
);

