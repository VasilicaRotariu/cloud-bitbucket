﻿CREATE TABLE [dbo].[NNCData_Escalations] (
    [MachineID]      INT           NULL,
    [AssetNumber]    INT           NULL,
    [FailType]       VARCHAR (50)  NULL,
    [CounterLimit]   INT           NULL,
    [CounterCurrent] FLOAT (53)    NULL,
    [SubAsset]       VARCHAR (255) NULL,
    [ID]             INT           IDENTITY (1, 1) NOT NULL,
    [ComponentType]  VARCHAR (50)  NULL,
    [DataColumn]     VARCHAR (50)  DEFAULT ('type here') NOT NULL,
    [SuggestionID]   INT           NULL
);


GO
CREATE CLUSTERED INDEX [ClusteredIndex-20150813-160717]
    ON [dbo].[NNCData_Escalations]([MachineID] ASC, [AssetNumber] ASC, [FailType] ASC);

