﻿CREATE TABLE [dbo].[SA2_Stats_AssemblyInsertion] (
    [AssemblyInsertionStatUID]             INT             IDENTITY (1, 1) NOT NULL,
    [AssemblyInsertionStatUTC]             DATETIME2 (7)   NOT NULL,
    [AssemblyUID]                          INT             NOT NULL,
    [AssemblyType]                         VARCHAR (50)    NOT NULL,
    [BodyUID]                              INT             NOT NULL,
    [BodyMatrixCode]                       VARCHAR (50)    NOT NULL,
    [BodyType]                             VARCHAR (50)    NOT NULL,
    [BodyInsertionFailCount]               INT             NOT NULL,
    [BodyInsertionMeasurementCount]        INT             NOT NULL,
    [PinUID]                               INT             NOT NULL,
    [PinGUID]                              VARCHAR (50)    NOT NULL,
    [PinType]                              VARCHAR (50)    NOT NULL,
    [PinInsertionFailCount]                INT             NOT NULL,
    [PinInsertionMeasurementCount]         INT             NOT NULL,
    [AssemblyInsertionLength]              DECIMAL (12, 8) NOT NULL,
    [AssemblyInsertionLengthUTL]           DECIMAL (12, 8) NOT NULL,
    [AssemblyInsertionLengthLTL]           DECIMAL (12, 8) NOT NULL,
    [AssemblyInsertionLengthDetermination] INT             NOT NULL,
    CONSTRAINT [PK_Stats_AssemblyInsertion] PRIMARY KEY CLUSTERED ([AssemblyInsertionStatUID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_AssemblyInsertion_Det]
    ON [dbo].[SA2_Stats_AssemblyInsertion]([AssemblyInsertionLengthDetermination] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_AssemblyInsertion_UTC]
    ON [dbo].[SA2_Stats_AssemblyInsertion]([AssemblyInsertionStatUTC] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_SA2_Stats_AssemblyInsertion_UIDASC]
    ON [dbo].[SA2_Stats_AssemblyInsertion]([AssemblyInsertionStatUID] ASC);

