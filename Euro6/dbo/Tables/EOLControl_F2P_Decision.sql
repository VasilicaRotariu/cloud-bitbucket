﻿CREATE TABLE [dbo].[EOLControl_F2P_Decision] (
    [ComponentType] VARCHAR (50)  NOT NULL,
    [NozzleID]      VARCHAR (30)  NOT NULL,
    [PistonGuideID] VARCHAR (20)  NOT NULL,
    [Date]          DATETIME      NOT NULL,
    [Decision]      VARCHAR (4)   NOT NULL,
    [Error]         VARCHAR (500) NULL
);

