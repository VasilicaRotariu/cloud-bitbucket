﻿CREATE TABLE [dbo].[LaserTrayResults] (
    [Tray_ID]          VARCHAR (33)  CONSTRAINT [DF_LaserTrayResults_Tray_ID] DEFAULT (sysdatetime()) NOT NULL,
    [Station]          SMALLINT      NOT NULL,
    [PDate]            DATETIME2 (7) NOT NULL,
    [TDate]            DATETIME2 (7) CONSTRAINT [DF_LaserTrayResults_TDate] DEFAULT (sysdatetime()) NOT NULL,
    [Reference]        VARCHAR (15)  NOT NULL,
    [Type_fab]         VARCHAR (1)   NULL,
    [Line]             TINYINT       NULL,
    [AssetNumber]      INT           NOT NULL,
    [Fixture]          TINYINT       NULL,
    [Number_parts_OK]  TINYINT       NULL,
    [Number_parts_NOK] TINYINT       NULL
) ON [Indexes];


GO
CREATE CLUSTERED INDEX [ClusteredIndex-PDate]
    ON [dbo].[LaserTrayResults]([PDate] ASC)
    ON [Indexes];

