﻿CREATE TABLE [dbo].[MASMEC_SelAsmb_OP20B_Results] (
    [Tdate]                           DATETIME     DEFAULT (getdate()) NOT NULL,
    [Idate]                           DATETIME     NOT NULL,
    [Pdate]                           DATETIME     NULL,
    [Serial]                          VARCHAR (12) NULL,
    [LOP]                             VARCHAR (20) NOT NULL,
    [Rework_Version]                  INT          NULL,
    [Result]                          INT          NULL,
    [Fail_UID]                        INT          NULL,
    [PlantNo]                         INT          NULL,
    [Operator_ID]                     VARCHAR (10) NULL,
    [Operator_FullName]               VARCHAR (50) NULL,
    [Injector_Type]                   INT          NULL,
    [HeaderScrew_Stroke_Min]          FLOAT (53)   NULL,
    [HeaderScrew_Stroke_Max]          FLOAT (53)   NULL,
    [NCV_SerialNumber]                VARCHAR (12) NULL,
    [Nozzle_SerialNumber]             VARCHAR (13) NULL,
    [HeaderScrew_Stroke_MeasureValue] FLOAT (53)   NULL,
    [HeaderScrew_Stroke_Result]       INT          NULL,
    [Program_Motor_Orientation]       INT          NULL,
    [Program_Motor_Screwing]          INT          NULL,
    [Program_Motor_Find_Nozzle_DM]    INT          NULL,
    [ShiftNumber]                     INT          NULL,
    [LineId]                          INT          NULL,
    [UID]                             BIGINT       NULL,
    [SerialCount]                     INT          NULL,
    [NCVCount]                        INT          NULL,
    [NozzleCount]                     INT          NULL,
    CONSTRAINT [PK_MASMEC_SelAsmb_OP20B_Results] PRIMARY KEY CLUSTERED ([Idate] DESC) WITH (FILLFACTOR = 80),
    CONSTRAINT [fk_MASMEC_SelAsmb_OP20B_Results_ProcessTable] FOREIGN KEY ([Serial]) REFERENCES [dbo].[MASMEC_SelAsmb_ProcessTable] ([Serial]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20160222-142716]
    ON [dbo].[MASMEC_SelAsmb_OP20B_Results]([Serial] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Result_Nozzle_SerialNumber]
    ON [dbo].[MASMEC_SelAsmb_OP20B_Results]([Result] ASC, [Nozzle_SerialNumber] ASC)
    INCLUDE([Pdate], [Serial], [NCV_SerialNumber]) WITH (FILLFACTOR = 90);


GO


-- =============================================
-- Author:		Guillaume Carre
-- Create date: 23/09/2014
-- Description:	on insertion of a new row in this table count the iteration of the particular nozzle serial and piston guide serial already present in the table
-- =============================================
CREATE TRIGGER [dbo].[UpdateMASMEC_SelAsmb_OP20B_Results]
   ON  [dbo].[MASMEC_SelAsmb_OP20B_Results]
   INSTEAD OF INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE		
				@CurrentSerialTotal INT,
				@CurrentNozzleTotal INT,
				@CurrentNCVTotal INT,
				@NewSerialTotal INT,
				@NewNozzleTotal INT,
				@NewNCVTotal INT
	DECLARE
				@Tdate datetime,
				@Idate datetime,
				@Pdate datetime,
				@Serial varchar(12),
				@LOP varchar(20),
				@Rework_Version int,
				@Result	int,
				@Fail_UID int,
				@PlantNo int,
				@Operator_ID varchar(10),
				@Operator_FullName varchar(50),
				@Injector_Type int,
				@HeaderScrew_Stroke_Min float,
				@HeaderScrew_Stroke_Max float,
				@NCV_SerialNumber varchar(12),
				@Nozzle_SerialNumber varchar(13),
				@HeaderScrew_Stroke_MeasureValue float,
				@HeaderScrew_Stroke_Result int,
				@Program_Motor_Orientation int,
				@Program_Motor_Screwing	int,
				@Program_Motor_Find_Nozzle_DM int,
				@ShiftNumber int,
				@LineId	int,
				@UID bigint

	DECLARE SerialRows CURSOR FOR 
	SELECT		[Tdate],
				[Idate],
				[Pdate],
				[Serial],
				[LOP],
				[Rework_Version],
				[Result],
				[Fail_UID],
				[PlantNo],
				[Operator_ID],
				[Operator_FullName],
				[Injector_Type],
				[HeaderScrew_Stroke_Min],
				[HeaderScrew_Stroke_Max],
				[NCV_SerialNumber],
				[Nozzle_SerialNumber],
				[HeaderScrew_Stroke_MeasureValue],
				[HeaderScrew_Stroke_Result],
				[Program_Motor_Orientation],
				[Program_Motor_Screwing],
				[Program_Motor_Find_Nozzle_DM],
				[ShiftNumber],
				[LineId],
				[UID]
	FROM INSERTED
	OPEN SerialRows
	WHILE(1=1)
	BEGIN
		SET @CurrentSerialTotal = 0
		SET @CurrentNozzleTotal = 0
		SET @CurrentNCVTotal = 0

		FETCH NEXT FROM SerialRows INTO 
				@Tdate,
				@Idate,
				@Pdate,
				@Serial,
				@LOP,
				@Rework_Version,
				@Result,
				@Fail_UID,
				@PlantNo,
				@Operator_ID,
				@Operator_FullName,
				@Injector_Type,
				@HeaderScrew_Stroke_Min,
				@HeaderScrew_Stroke_Max,
				@NCV_SerialNumber,
				@Nozzle_SerialNumber,
				@HeaderScrew_Stroke_MeasureValue,
				@HeaderScrew_Stroke_Result,
				@Program_Motor_Orientation,
				@Program_Motor_Screwing,
				@Program_Motor_Find_Nozzle_DM,
				@ShiftNumber,
				@LineId,
				@UID
		IF @@FETCH_STATUS <> 0
			BREAK
		SELECT @CurrentSerialTotal = COUNT(*) FROM [MASMEC_SelAsmb_OP20B_Results] WHERE [Serial] = @Serial
		SELECT @CurrentNozzleTotal = COUNT(*) FROM [MASMEC_SelAsmb_OP20B_Results]  WHERE [Nozzle_SerialNumber] = @Nozzle_SerialNumber
		SELECT @CurrentNCVTotal = COUNT(*) FROM [MASMEC_SelAsmb_OP20B_Results] WHERE [NCV_SerialNumber] = @NCV_SerialNumber

		SET @NewSerialTotal = @CurrentSerialTotal + 1
		SET @NewNozzleTotal = @CurrentNozzleTotal + 1
		SET @NewNCVTotal = @CurrentNCVTotal + 1

		INSERT INTO [MASMEC_SelAsmb_OP20B_Results]([Tdate],
				[Idate],
				[Pdate],
				[Serial],
				[LOP],
				[Rework_Version],
				[Result],
				[Fail_UID],
				[PlantNo],
				[Operator_ID],
				[Operator_FullName],
				[Injector_Type],
				[HeaderScrew_Stroke_Min],
				[HeaderScrew_Stroke_Max],
				[NCV_SerialNumber],
				[Nozzle_SerialNumber],
				[HeaderScrew_Stroke_MeasureValue],
				[HeaderScrew_Stroke_Result],
				[Program_Motor_Orientation],
				[Program_Motor_Screwing],
				[Program_Motor_Find_Nozzle_DM],
				[ShiftNumber],
				[LineId],
				[UID],
				[SerialCount],
				[NCVCount],
				[NozzleCount])
			VALUES ( @Tdate,
				@Idate,
				@Pdate,
				@Serial,
				@LOP,
				@Rework_Version,
				@Result,
				@Fail_UID,
				@PlantNo,
				@Operator_ID,
				@Operator_FullName,
				@Injector_Type,
				@HeaderScrew_Stroke_Min,
				@HeaderScrew_Stroke_Max,
				@NCV_SerialNumber,
				@Nozzle_SerialNumber,
				@HeaderScrew_Stroke_MeasureValue,
				@HeaderScrew_Stroke_Result,
				@Program_Motor_Orientation,
				@Program_Motor_Screwing,
				@Program_Motor_Find_Nozzle_DM,
				@ShiftNumber,
				@LineId,
				@UID,
				@NewSerialTotal,
				@NewNCVTotal,
				@NewNozzleTotal)
	END
	CLOSE SerialRows
	DEALLOCATE SerialRows
END


