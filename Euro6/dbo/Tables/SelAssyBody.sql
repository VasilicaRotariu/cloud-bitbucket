﻿CREATE TABLE [dbo].[SelAssyBody] (
    [UID]                      BIGINT        NOT NULL,
    [UTC]                      DATETIME      NULL,
    [PlantNumber]              SMALLINT      NULL,
    [BodyTrayMatrix]           VARCHAR (5)   NULL,
    [BodyDrawMatrix]           VARCHAR (4)   NULL,
    [BodyMatrix]               VARCHAR (11)  NULL,
    [BodyType]                 VARCHAR (12)  NULL,
    [BodyGrade]                VARCHAR (2)   NULL,
    [BodySeatLength]           FLOAT (53)    NULL,
    [BodySeatLengthResult]     TINYINT       NULL,
    [BodyDiameterTop]          FLOAT (53)    NULL,
    [BodyDiameterBtm]          FLOAT (53)    NULL,
    [BodyDiameterAve]          FLOAT (53)    NULL,
    [BodyDiameterAveResult]    TINYINT       NULL,
    [BodyTaper]                FLOAT (53)    NULL,
    [BodyTaperResult]          TINYINT       NULL,
    [BodyCollarDiameter]       FLOAT (53)    NULL,
    [BodyCollarDiameterResult] TINYINT       NULL,
    [BodyUsage]                TINYINT       NULL,
    [BodyAreaTemperature]      FLOAT (53)    NULL,
    [FailureCode]              SMALLINT      NULL,
    [TDate]                    DATETIME2 (7) CONSTRAINT [DF_SelAssyBody_TDate] DEFAULT (sysdatetime()) NOT NULL,
    CONSTRAINT [PK_SelAssyBody] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_SelAssyBody_UTCDESC]
    ON [dbo].[SelAssyBody]([UTC] DESC);


GO
CREATE NONCLUSTERED INDEX [IX_SelAssyBody_TDateASC]
    ON [dbo].[SelAssyBody]([TDate] ASC)
    INCLUDE([UID], [UTC], [PlantNumber], [BodyTrayMatrix], [BodyDrawMatrix], [BodyMatrix], [BodyType], [BodyGrade], [BodySeatLength], [BodySeatLengthResult], [BodyDiameterTop], [BodyDiameterBtm], [BodyDiameterAve], [BodyDiameterAveResult], [BodyTaper], [BodyTaperResult], [BodyCollarDiameter], [BodyCollarDiameterResult], [BodyUsage], [BodyAreaTemperature], [FailureCode]);


GO
CREATE NONCLUSTERED INDEX [IX_BodyMatrix_BodySeatLength_UTC]
    ON [dbo].[SelAssyBody]([BodyMatrix] ASC, [BodySeatLength] ASC, [UTC] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_BodyType_BodyDiameterAve]
    ON [dbo].[SelAssyBody]([BodyType] ASC, [BodyDiameterAve] ASC)
    INCLUDE([UTC], [BodyDiameterTop], [BodyDiameterBtm], [BodyTaper]) WITH (FILLFACTOR = 90);

