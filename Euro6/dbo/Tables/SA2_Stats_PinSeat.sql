﻿CREATE TABLE [dbo].[SA2_Stats_PinSeat] (
    [PinSeatStatUID]             INT             IDENTITY (1, 1) NOT NULL,
    [PinSeatStatUTC]             DATETIME2 (7)   NOT NULL,
    [PinUID]                     INT             NOT NULL,
    [PinGUID]                    VARCHAR (50)    NOT NULL,
    [PinType]                    VARCHAR (50)    NOT NULL,
    [PinSeatMeasCount]           INT             NOT NULL,
    [PinSeatLength]              DECIMAL (12, 8) NOT NULL,
    [PinSeatLengthUTL]           DECIMAL (12, 8) NOT NULL,
    [PinSeatLengthLTL]           DECIMAL (12, 8) NOT NULL,
    [PinSeatLengthDetermination] INT             NOT NULL,
    CONSTRAINT [PK_Stats_PinSeat] PRIMARY KEY CLUSTERED ([PinSeatStatUID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_PinSeat_Det]
    ON [dbo].[SA2_Stats_PinSeat]([PinSeatLengthDetermination] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PinSeat_UTC]
    ON [dbo].[SA2_Stats_PinSeat]([PinSeatStatUTC] ASC) WITH (FILLFACTOR = 90);

