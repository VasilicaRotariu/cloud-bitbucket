﻿CREATE TABLE [dbo].[Test_TestPlans] (
    [Uid]      INT          IDENTITY (1, 1) NOT NULL,
    [Name]     VARCHAR (50) NOT NULL,
    [Customer] VARCHAR (50) NOT NULL,
    [RP1]      INT          NOT NULL,
    [RP2]      INT          NOT NULL,
    [RP3]      INT          NOT NULL,
    [RP4]      INT          NOT NULL,
    [RP5]      INT          NOT NULL
);

