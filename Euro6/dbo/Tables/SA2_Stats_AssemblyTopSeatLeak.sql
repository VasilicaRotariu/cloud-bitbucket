﻿CREATE TABLE [dbo].[SA2_Stats_AssemblyTopSeatLeak] (
    [AssemblyTopSeatLeakStatUID]       INT             IDENTITY (1, 1) NOT NULL,
    [AssemblyTopSeatLeakStatUTC]       DATETIME2 (7)   NOT NULL,
    [AssemblyUID]                      INT             NOT NULL,
    [AssemblyType]                     VARCHAR (50)    NOT NULL,
    [BodyUID]                          INT             NOT NULL,
    [BodyMatrixCode]                   VARCHAR (50)    NOT NULL,
    [BodyType]                         VARCHAR (50)    NOT NULL,
    [BodyTopSeatLeakFailCount]         INT             NOT NULL,
    [BodyTopSeatLeakMeasurementCount]  INT             NOT NULL,
    [PinUID]                           INT             NOT NULL,
    [PinGUID]                          VARCHAR (50)    NOT NULL,
    [PinType]                          VARCHAR (50)    NOT NULL,
    [PinTopSeatLeakFailCount]          INT             NOT NULL,
    [PinTopSeatLeakMeasurementCount]   INT             NOT NULL,
    [AssemblyTopSeatLeak]              DECIMAL (12, 8) NOT NULL,
    [AssemblyTopSeatLeakUTL]           DECIMAL (12, 8) NOT NULL,
    [AssemblyTopSeatLeakLTL]           DECIMAL (12, 8) NOT NULL,
    [AssemblyTopSeatLeakDetermination] INT             NOT NULL,
    CONSTRAINT [PK_Stats_AssemblyTopSeatLeak] PRIMARY KEY CLUSTERED ([AssemblyTopSeatLeakStatUID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_AssemblyTopSeatLeak_Det]
    ON [dbo].[SA2_Stats_AssemblyTopSeatLeak]([AssemblyTopSeatLeakDetermination] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_AssemblyTopSeatLeak_UTC]
    ON [dbo].[SA2_Stats_AssemblyTopSeatLeak]([AssemblyTopSeatLeakStatUTC] ASC);

