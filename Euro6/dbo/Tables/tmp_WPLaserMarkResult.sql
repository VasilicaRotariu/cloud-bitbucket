﻿CREATE TABLE [dbo].[tmp_WPLaserMarkResult] (
    [Uid]          INT          IDENTITY (1, 1) NOT NULL,
    [Pdate]        DATETIME     NOT NULL,
    [PartNumber]   VARCHAR (10) NOT NULL,
    [SerialNumber] VARCHAR (15) NOT NULL,
    [Result]       INT          NOT NULL,
    [DMCString]    VARCHAR (50) NOT NULL
);

