﻿CREATE TABLE [dbo].[SA1_NCV_MatchPinComplete] (
    [UID]              INT           NOT NULL,
    [UTC]              DATETIME2 (7) NOT NULL,
    [Matrix]           VARCHAR (50)  NOT NULL,
    [PinGUID]          VARCHAR (50)  NULL,
    [Passed]           BIT           NOT NULL,
    [TargetDiameter]   FLOAT (53)    NULL,
    [TargetSeatLength] FLOAT (53)    NULL,
    CONSTRAINT [PK_NCV_MatchPinComplete] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 90)
);

