﻿CREATE TABLE [dbo].[SA2_Stats_BodyGuide] (
    [BodyGuideStatUID]                  INT             IDENTITY (1, 1) NOT NULL,
    [BodyGuideStatUTC]                  DATETIME2 (7)   NOT NULL,
    [BodyUID]                           INT             NOT NULL,
    [BodyMatrixCode]                    VARCHAR (50)    NOT NULL,
    [BodyType]                          VARCHAR (50)    NOT NULL,
    [BodyGuideMeasCount]                INT             NOT NULL,
    [BodyGuideDiameterTop]              DECIMAL (12, 8) NOT NULL,
    [BodyGuideDiameterTopUTL]           DECIMAL (12, 8) NOT NULL,
    [BodyGuideDiameterTopLTL]           DECIMAL (12, 8) NOT NULL,
    [BodyGuideDiameterTopDetermination] INT             NOT NULL,
    [BodyGuideDiameterBtm]              DECIMAL (12, 8) NOT NULL,
    [BodyGuideDiameterBtmUTL]           DECIMAL (12, 8) NOT NULL,
    [BodyGuideDiameterBtmLTL]           DECIMAL (12, 8) NOT NULL,
    [BodyGuideDiameterBtmDetermination] INT             NOT NULL,
    [BodyGuideDiameterAve]              DECIMAL (12, 8) NOT NULL,
    [BodyGuideDiameterAveUTL]           DECIMAL (12, 8) NOT NULL,
    [BodyGuideDiameterAveLTL]           DECIMAL (12, 8) NOT NULL,
    [BodyGuideDiameterAveDetermination] INT             NOT NULL,
    [BodyGuideTaper]                    DECIMAL (12, 8) NOT NULL,
    [BodyGuideTaperUTL]                 DECIMAL (12, 8) NOT NULL,
    [BodyGuideTaperLTL]                 DECIMAL (12, 8) NOT NULL,
    [BodyGuideTaperDetermination]       INT             NOT NULL,
    [BodyGuideDetermination]            AS              (case when [BodyGuideDiameterTopDetermination]=(0) AND [BodyGuideDiameterBtmDetermination]=(0) AND [BodyGuideDiameterAveDetermination]=(0) AND [BodyGuideTaperDetermination]=(0) then (0) else (1) end) PERSISTED NOT NULL,
    CONSTRAINT [PK_Stats_BodyGuide] PRIMARY KEY CLUSTERED ([BodyGuideStatUID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_BodyGuide_UTC]
    ON [dbo].[SA2_Stats_BodyGuide]([BodyGuideStatUTC] ASC) WITH (FILLFACTOR = 90);

