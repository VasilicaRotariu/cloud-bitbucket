﻿CREATE TABLE [dbo].[util_Numbers] (
    [Number] INT NOT NULL,
    CONSTRAINT [PK_Numbers] PRIMARY KEY CLUSTERED ([Number] ASC) WITH (FILLFACTOR = 80)
);

