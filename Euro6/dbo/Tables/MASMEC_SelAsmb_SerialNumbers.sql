﻿CREATE TABLE [dbo].[MASMEC_SelAsmb_SerialNumbers] (
    [SerialNumber] VARCHAR (12) DEFAULT ((0)) NOT NULL,
    [Tdate]        DATETIME     DEFAULT (getdate()) NOT NULL,
    [PDate]        DATETIME     NOT NULL,
    [LOP]          VARCHAR (20) NOT NULL,
    [dailyCounter] INT          NULL,
    [UID]          BIGINT       NULL,
    CONSTRAINT [PK_SerialNumbers] PRIMARY KEY CLUSTERED ([SerialNumber] ASC) WITH (FILLFACTOR = 80)
);

