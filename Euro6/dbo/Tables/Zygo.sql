﻿CREATE TABLE [dbo].[Zygo] (
    [PDate]        DATETIME     NOT NULL,
    [PartNumber]   VARCHAR (10) NOT NULL,
    [SerialNumber] VARCHAR (11) NOT NULL,
    [Result]       INT          NOT NULL,
    [Fag]          FLOAT (53)   NULL,
    [Tilt]         FLOAT (53)   NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_Serial]
    ON [dbo].[Zygo]([SerialNumber] DESC);

