﻿CREATE TABLE [dbo].[NNCData_Machines] (
    [MachineID]      INT           NOT NULL,
    [TableName]      VARCHAR (50)  NOT NULL,
    [Title]          VARCHAR (50)  NOT NULL,
    [ToleranceClass] VARCHAR (10)  NOT NULL,
    [TableLocation]  VARCHAR (50)  NULL,
    [DateFormat]     VARCHAR (50)  NULL,
    [ServerIndex]    INT           NULL,
    [ELTableName]    VARCHAR (50)  NULL,
    [SubAssetColumn] VARCHAR (255) NULL,
    [ProcessGroup]   INT           NULL
);

