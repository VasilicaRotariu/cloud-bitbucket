﻿CREATE TABLE [dbo].[LaserMarkingResults] (
    [Serial]      VARCHAR (33)  NOT NULL,
    [Tray_ID]     VARCHAR (33)  NOT NULL,
    [Station]     SMALLINT      NOT NULL,
    [PDate]       DATETIME2 (7) NOT NULL,
    [TDate]       DATETIME2 (7) CONSTRAINT [DF_LaserMarkingResults_TDate] DEFAULT (sysdatetime()) NOT NULL,
    [Reference]   VARCHAR (15)  NOT NULL,
    [Type_fab]    VARCHAR (1)   NULL,
    [Line]        TINYINT       NULL,
    [AssetNumber] INT           NOT NULL,
    [Fixture]     TINYINT       NULL,
    [FailCode]    VARCHAR (50)  NOT NULL,
    [FailClass]   VARCHAR (40)  NULL
) ON [Indexes];


GO
CREATE CLUSTERED INDEX [ClusteredIndex-Pdate]
    ON [dbo].[LaserMarkingResults]([PDate] ASC)
    ON [Indexes];

