﻿CREATE TABLE [dbo].[SA2_Stats_AssemblySeatLift] (
    [AssemblySeatLiftStatUID]       INT             IDENTITY (1, 1) NOT NULL,
    [AssemblySeatLiftStatUTC]       DATETIME2 (7)   NOT NULL,
    [AssemblyUID]                   INT             NOT NULL,
    [AssemblyType]                  VARCHAR (50)    NOT NULL,
    [BodyUID]                       INT             NOT NULL,
    [BodyMatrixCode]                VARCHAR (50)    NOT NULL,
    [BodyType]                      VARCHAR (50)    NOT NULL,
    [BodySeatLiftFailCount]         INT             NOT NULL,
    [BodySeatLiftMeasurementCount]  INT             NOT NULL,
    [PinUID]                        INT             NOT NULL,
    [PinGUID]                       VARCHAR (50)    NOT NULL,
    [PinType]                       VARCHAR (50)    NOT NULL,
    [PinSeatLiftFailCount]          INT             NOT NULL,
    [PinSeatLiftMeasurementCount]   INT             NOT NULL,
    [AssemblySeatLiftPredicted]     DECIMAL (12, 8) NULL,
    [AssemblySeatLift]              DECIMAL (12, 8) NOT NULL,
    [AssemblySeatLiftUTL]           DECIMAL (12, 8) NOT NULL,
    [AssemblySeatLiftLTL]           DECIMAL (12, 8) NOT NULL,
    [AssemblySeatLiftDetermination] INT             NOT NULL,
    CONSTRAINT [PK_Stats_AssemblyLift] PRIMARY KEY CLUSTERED ([AssemblySeatLiftStatUID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_AssemblySeatLift_Det]
    ON [dbo].[SA2_Stats_AssemblySeatLift]([AssemblySeatLiftDetermination] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_AssemblySeatLift_UTC]
    ON [dbo].[SA2_Stats_AssemblySeatLift]([AssemblySeatLiftStatUTC] ASC) WITH (FILLFACTOR = 90);

