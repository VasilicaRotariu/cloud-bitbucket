﻿CREATE TABLE [dbo].[ShiftProductionMonitorSettings] (
    [OELow]         REAL NOT NULL,
    [OEHigh]        REAL NOT NULL,
    [ThresholdLow]  REAL NOT NULL,
    [ThresholdHigh] REAL NOT NULL
);

