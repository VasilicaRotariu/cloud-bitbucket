﻿CREATE TABLE [dbo].[TestData] (
    [TestDataID]         BIGINT       IDENTITY (1, 1) NOT NULL,
    [TestNo]             BIGINT       NOT NULL,
    [SetRP]              INT          NULL,
    [NCVDuration]        INT          NULL,
    [FAQ]                VARCHAR (20) NULL,
    [BLFlow]             FLOAT (53)   NULL,
    [RailPressure]       FLOAT (53)   NULL,
    [InjTemp]            FLOAT (53)   NULL,
    [Delivery]           FLOAT (53)   NULL,
    [BLPressure]         FLOAT (53)   NULL,
    [InjSquirt1]         FLOAT (53)   NULL,
    [InjSquirt2]         FLOAT (53)   NULL,
    [InjSquirt3]         FLOAT (53)   NULL,
    [InjSquirt4]         FLOAT (53)   NULL,
    [InjSquirt5]         FLOAT (53)   NULL,
    [Del3SDs]            FLOAT (53)   NULL,
    [NominalMDP]         FLOAT (53)   NULL,
    [Trim]               FLOAT (53)   NULL,
    [TrimmedInjDelivery] FLOAT (53)   NULL,
    [NominalDelivery]    FLOAT (53)   NULL,
    [Error]              FLOAT (53)   NULL,
    [UF]                 INT          NULL,
    [LF]                 INT          NULL
);


GO
CREATE CLUSTERED INDEX [idx_TestData_TestDataID]
    ON [dbo].[TestData]([TestDataID] ASC);


GO
CREATE NONCLUSTERED INDEX [idx_TestData_A]
    ON [dbo].[TestData]([TestNo] ASC);


GO
CREATE NONCLUSTERED INDEX [idx_TestData_B]
    ON [dbo].[TestData]([TestNo] ASC)
    INCLUDE([Delivery], [NCVDuration], [SetRP]);

