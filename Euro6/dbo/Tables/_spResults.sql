﻿CREATE TABLE [dbo].[_spResults] (
    [PDate]        DATETIME2 (7) NULL,
    [PlantNumber]  SMALLINT      NULL,
    [SerialNumber] VARCHAR (25)  NULL,
    [Result]       TINYINT       NULL,
    [FailReason]   VARCHAR (255) NULL,
    [spName]       VARCHAR (255) NULL
);


GO
CREATE CLUSTERED INDEX [IX_spResults_PDateDESC]
    ON [dbo].[_spResults]([PDate] DESC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_spResults_SerialNumberASC]
    ON [dbo].[_spResults]([SerialNumber] ASC) WITH (FILLFACTOR = 90);

