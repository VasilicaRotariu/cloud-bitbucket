﻿CREATE TABLE [dbo].[BannedPGs] (
    [SerialNumber] NVARCHAR (50) NOT NULL,
    [FailCode]     TINYINT       CONSTRAINT [DF_BannedPGs_FailCode] DEFAULT ((1)) NOT NULL,
    [Pdate]        DATETIME      CONSTRAINT [DF_BannedPGs_Pdate] DEFAULT (sysdatetime()) NOT NULL
);

