﻿CREATE TABLE [dbo].[SA2_Stats_PinGuide] (
    [PinGuideStatUID]                  INT             IDENTITY (1, 1) NOT NULL,
    [PinGuideStatUTC]                  DATETIME2 (7)   NOT NULL,
    [PinUID]                           INT             NOT NULL,
    [PinGUID]                          VARCHAR (50)    NOT NULL,
    [PinType]                          VARCHAR (50)    NULL,
    [PinGuideMeasCount]                INT             NOT NULL,
    [PinGuideDiameterTop]              DECIMAL (12, 8) NOT NULL,
    [PinGuideDiameterTopUTL]           DECIMAL (12, 8) NOT NULL,
    [PinGuideDiameterTopLTL]           DECIMAL (12, 8) NOT NULL,
    [PinGuideDiameterTopDetermination] INT             NOT NULL,
    [PinGuideDiameterBtm]              DECIMAL (12, 8) NOT NULL,
    [PinGuideDiameterBtmUTL]           DECIMAL (12, 8) NOT NULL,
    [PinGuideDiameterBtmLTL]           DECIMAL (12, 8) NOT NULL,
    [PinGuideDiameterBtmDetermination] INT             NOT NULL,
    [PinGuideDiameterAve]              DECIMAL (12, 8) NOT NULL,
    [PinGuideDiameterAveUTL]           DECIMAL (12, 8) NOT NULL,
    [PinGuideDiameterAveLTL]           DECIMAL (12, 8) NOT NULL,
    [PinGuideDiameterAveDetermination] INT             NOT NULL,
    [PinGuideTaper]                    DECIMAL (12, 8) NOT NULL,
    [PinGuideTaperUTL]                 DECIMAL (12, 8) NOT NULL,
    [PinGuideTaperLTL]                 DECIMAL (12, 8) NOT NULL,
    [PinGuideTaperDetermination]       INT             NOT NULL,
    [PinDetermination]                 AS              (case when [PinGuideDiameterTopDetermination]=(0) AND [PinGuideDiameterBtmDetermination]=(0) AND [PinGuideDiameterAveDetermination]=(0) AND [PinGuideTaperDetermination]=(0) then (0) else (1) end) PERSISTED NOT NULL,
    CONSTRAINT [PK_Stats_PinGuide] PRIMARY KEY CLUSTERED ([PinGuideStatUID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_PinGuide_UTC]
    ON [dbo].[SA2_Stats_PinGuide]([PinGuideStatUTC] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_PinGuide_Determination]
    ON [dbo].[SA2_Stats_PinGuide]([PinGuideTaperDetermination] ASC, [PinGuideDiameterAveDetermination] ASC, [PinGuideDiameterBtmDetermination] ASC, [PinGuideDiameterTopDetermination] ASC);

