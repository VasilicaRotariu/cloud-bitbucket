﻿CREATE TABLE [dbo].[SA3_SelAssyPin] (
    [UID]                     BIGINT        IDENTITY (1, 1) NOT NULL,
    [UTC]                     DATETIME      NULL,
    [PlantNumber]             SMALLINT      NULL,
    [PinTrayMatrix]           VARCHAR (255) NULL,
    [PinDrawMatrix]           VARCHAR (4)   NULL,
    [PinType]                 VARCHAR (12)  NULL,
    [PinChacePositionX]       TINYINT       NULL,
    [PinChacePositionY]       TINYINT       NULL,
    [PinIdentification]       BIGINT        NULL,
    [PinGUID]                 VARCHAR (50)  NULL,
    [PinSeatLength]           FLOAT (53)    NULL,
    [PinSeatLengthResult]     TINYINT       NULL,
    [PinDiameterTop]          FLOAT (53)    NULL,
    [PinDiameterBtm]          FLOAT (53)    NULL,
    [PinDiameterAve]          FLOAT (53)    NULL,
    [PinDiameterAveResult]    TINYINT       NULL,
    [PinTaper]                FLOAT (53)    NULL,
    [PinTaperResult]          TINYINT       NULL,
    [PinCollarDiameter]       FLOAT (53)    NULL,
    [PinCollarDiameterResult] TINYINT       NULL,
    [PinGradeTray]            VARCHAR (2)   NULL,
    [PinGradeMeasurement]     VARCHAR (2)   NULL,
    [PinOrientation]          TINYINT       NULL,
    [PinCoating]              TINYINT       NULL,
    [PinUsage]                TINYINT       NULL,
    [PinAreaTemperature]      FLOAT (53)    NULL,
    [FailureCode]             SMALLINT      NULL,
    [TDate]                   DATETIME2 (7) CONSTRAINT [DF_SA3_SelAssyPin_TDate] DEFAULT (sysdatetime()) NOT NULL,
    CONSTRAINT [PK_SA3_SelAssyPin] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_PinType_PinDiameter_PinCollarDiameter]
    ON [dbo].[SA3_SelAssyPin]([PinType] ASC, [PinDiameterAve] ASC, [PinCollarDiameter] ASC)
    INCLUDE([UTC]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_PinType_PinSeatLength]
    ON [dbo].[SA3_SelAssyPin]([PinType] ASC, [PinSeatLength] ASC)
    INCLUDE([UTC]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_PinType_PinDiameterAve]
    ON [dbo].[SA3_SelAssyPin]([PinType] ASC, [PinDiameterAve] ASC)
    INCLUDE([UTC], [PinDiameterTop], [PinDiameterBtm], [PinTaper]) WITH (FILLFACTOR = 90);

