﻿CREATE TABLE [dbo].[SA2_Stats_BodyCollar] (
    [BodyCollarStatUID]               INT             IDENTITY (1, 1) NOT NULL,
    [BodyCollarStatUTC]               DATETIME2 (7)   NOT NULL,
    [BodyUID]                         INT             NOT NULL,
    [BodyMatrixCode]                  VARCHAR (50)    NOT NULL,
    [BodyType]                        VARCHAR (50)    NOT NULL,
    [BodyCollarMeasCount]             INT             NOT NULL,
    [BodyCollarDiameter]              DECIMAL (12, 8) NOT NULL,
    [BodyCollarDiameterUTL]           DECIMAL (12, 8) NULL,
    [BodyCollarDiameterLTL]           DECIMAL (12, 8) NULL,
    [BodyCollarDiameterDetermination] INT             NULL,
    CONSTRAINT [PK_Stats_BodyCollar] PRIMARY KEY CLUSTERED ([BodyCollarStatUID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_BodyCollar_Det]
    ON [dbo].[SA2_Stats_BodyCollar]([BodyCollarDiameterDetermination] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Stats_BodyCollar_UTC]
    ON [dbo].[SA2_Stats_BodyCollar]([BodyCollarStatUTC] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_SA2_Stats_BodyCollar_UIDASC]
    ON [dbo].[SA2_Stats_BodyCollar]([BodyCollarStatUID] ASC);

