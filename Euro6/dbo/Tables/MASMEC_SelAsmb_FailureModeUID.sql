﻿CREATE TABLE [dbo].[MASMEC_SelAsmb_FailureModeUID] (
    [UID]         INT          NOT NULL,
    [FailureMode] VARCHAR (50) NULL,
    CONSTRAINT [PK_MASMEC_SelAsmb_FailureModeUID] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 80)
);

