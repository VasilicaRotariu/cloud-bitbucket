﻿CREATE TABLE [dbo].[SA2_Stats_PinReserveLocation] (
    [PinReserveLocationStatUID]       INT           IDENTITY (1, 1) NOT NULL,
    [PinReserveLocationStatUTC]       DATETIME2 (7) NOT NULL,
    [PinUID]                          INT           NOT NULL,
    [A]                               INT           NULL,
    [C]                               INT           NULL,
    [R]                               INT           NULL,
    [PinReserveLocationDetermination] INT           NOT NULL,
    CONSTRAINT [PK_SA2_Stats_PinReserveLocation] PRIMARY KEY CLUSTERED ([PinReserveLocationStatUID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_SA2_Stats_PinReserveLocation_PinReserveLocationStatUID]
    ON [dbo].[SA2_Stats_PinReserveLocation]([PinReserveLocationStatUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PinReserveLocationStatUTC_PinReserveLocationDetermination]
    ON [dbo].[SA2_Stats_PinReserveLocation]([PinReserveLocationStatUTC] ASC, [PinReserveLocationDetermination] ASC);

