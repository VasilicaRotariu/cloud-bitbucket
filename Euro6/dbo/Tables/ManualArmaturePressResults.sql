﻿CREATE TABLE [dbo].[ManualArmaturePressResults] (
    [PDate]                       DATETIME2 (7)   NOT NULL,
    [IDate]                       DATETIME2 (7)   NULL,
    [Product Type]                VARCHAR (12)    NOT NULL,
    [Product Version]             INT             NOT NULL,
    [PlantNumber]                 INT             NOT NULL,
    [DataMatrix]                  VARCHAR (12)    NOT NULL,
    [DataMatrix_Result]           TINYINT         NOT NULL,
    [IsNCV]                       BIT             NOT NULL,
    [Press_Result]                TINYINT         NOT NULL,
    [Piezo_Stack_Position]        FLOAT (53)      NOT NULL,
    [Fixed_Adjustment_Offset]     FLOAT (53)      NOT NULL,
    [Tracking_Adjustment]         FLOAT (53)      NOT NULL,
    [Tracking_Adjustment_Enabled] TINYINT         NOT NULL,
    [Press_Data]                  VARBINARY (MAX) NULL,
    [TDate]                       DATETIME2 (7)   CONSTRAINT [DF_ManualArmaturePressResults_TDate] DEFAULT (sysdatetime()) NOT NULL
);


GO
CREATE CLUSTERED INDEX [IX_ManualArmaturePress_PDateDESC]
    ON [dbo].[ManualArmaturePressResults]([PDate] DESC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_ManualArmaturePress_DataMatrixASC]
    ON [dbo].[ManualArmaturePressResults]([DataMatrix] ASC) WITH (FILLFACTOR = 90);

