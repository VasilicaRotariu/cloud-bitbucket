﻿CREATE TABLE [dbo].[MASMEC_SelAsmb_OP20A_Results] (
    [Tdate]                        DATETIME     DEFAULT (getdate()) NOT NULL,
    [Idate]                        DATETIME     NOT NULL,
    [Pdate]                        DATETIME     NULL,
    [Serial]                       VARCHAR (12) NOT NULL,
    [LOP]                          VARCHAR (20) NOT NULL,
    [Rework_Version]               INT          DEFAULT ((0)) NULL,
    [Result]                       INT          NOT NULL,
    [Fail_UID]                     INT          NULL,
    [PlantNo]                      INT          DEFAULT ((0)) NULL,
    [Operator_ID]                  VARCHAR (10) DEFAULT ((0)) NULL,
    [Operator_FullName]            VARCHAR (50) DEFAULT ((0)) NULL,
    [Injector_Type]                INT          NULL,
    [Set_Point_Load]               FLOAT (53)   NULL,
    [Tollerance_Load]              FLOAT (53)   NULL,
    [Load_Result]                  INT          NULL,
    [PreLoad_MeasureValue]         FLOAT (53)   NULL,
    [PreLoad_Error_MeasureValue]   FLOAT (53)   NULL,
    [Tollerance_Shim]              FLOAT (53)   NULL,
    [Tollerance_Verify_Shim]       FLOAT (53)   NULL,
    [Shim_Result]                  INT          NULL,
    [Standard_Shim]                FLOAT (53)   NULL,
    [Shim_MeasureValue]            FLOAT (53)   NULL,
    [Shim_Required]                FLOAT (53)   NULL,
    [Verify_Shim_Result]           INT          NULL,
    [Verify_Shim_MeasureValue]     FLOAT (53)   NULL,
    [Spring_Result]                INT          NULL,
    [Shim_Spring_MeasureValue]     FLOAT (53)   NULL,
    [ShiftNumber]                  INT          NULL,
    [LineId]                       INT          NULL,
    [Shim_Spring_Result]           INT          NULL,
    [Enable_Presence_Control_Shim] INT          NULL,
    [OvercheckLoadValue]           FLOAT (53)   NULL,
    [OffsetShim]                   FLOAT (53)   NULL,
    [OverCheckShim_Value]          FLOAT (53)   NULL,
    [UID]                          BIGINT       NULL,
    [Consecutive_OutOfRange]       INT          NULL,
    [Expected_Shim_Thickness]      FLOAT (53)   NULL,
    [Min_Expected_Shim_Thickness]  FLOAT (53)   NULL,
    [Max_Expected_Shim_Thickness]  FLOAT (53)   NULL,
    CONSTRAINT [PK_MASMEC_SelAsmb_OP20A_Results] PRIMARY KEY CLUSTERED ([Idate] DESC) WITH (FILLFACTOR = 80),
    CONSTRAINT [fk_MASMEC_SelAsmb_op20A_Results_ProcessTable] FOREIGN KEY ([Serial]) REFERENCES [dbo].[MASMEC_SelAsmb_ProcessTable] ([Serial]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150302-150005]
    ON [dbo].[MASMEC_SelAsmb_OP20A_Results]([Pdate] ASC) WITH (FILLFACTOR = 90);

