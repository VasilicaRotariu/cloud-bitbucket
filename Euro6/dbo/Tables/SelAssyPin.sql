﻿CREATE TABLE [dbo].[SelAssyPin] (
    [UID]                     BIGINT        NOT NULL,
    [UTC]                     DATETIME      NULL,
    [PlantNumber]             SMALLINT      NULL,
    [PinTrayMatrix]           VARCHAR (255) NULL,
    [PinDrawMatrix]           VARCHAR (4)   NULL,
    [PinType]                 VARCHAR (12)  NULL,
    [PinChacePositionX]       TINYINT       NULL,
    [PinChacePositionY]       TINYINT       NULL,
    [PinIdentification]       BIGINT        NULL,
    [PinGUID]                 VARCHAR (50)  NULL,
    [PinSeatLength]           FLOAT (53)    NULL,
    [PinSeatLengthResult]     TINYINT       NULL,
    [PinDiameterTop]          FLOAT (53)    NULL,
    [PinDiameterBtm]          FLOAT (53)    NULL,
    [PinDiameterAve]          FLOAT (53)    NULL,
    [PinDiameterAveResult]    TINYINT       NULL,
    [PinTaper]                FLOAT (53)    NULL,
    [PinTaperResult]          TINYINT       NULL,
    [PinCollarDiameter]       FLOAT (53)    NULL,
    [PinCollarDiameterResult] TINYINT       NULL,
    [PinGradeTray]            VARCHAR (2)   NULL,
    [PinGradeMeasurement]     VARCHAR (2)   NULL,
    [PinOrientation]          TINYINT       NULL,
    [PinCoating]              TINYINT       NULL,
    [PinUsage]                TINYINT       NULL,
    [PinAreaTemperature]      FLOAT (53)    NULL,
    [FailureCode]             SMALLINT      NULL,
    [TDate]                   DATETIME2 (7) CONSTRAINT [DF_SelAssyPin_TDate] DEFAULT (sysdatetime()) NOT NULL,
    CONSTRAINT [PK_SelAssyPin] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_PinType_PinDiameterAve]
    ON [dbo].[SelAssyPin]([PinType] ASC, [PinDiameterAve] ASC)
    INCLUDE([UTC], [PinDiameterTop], [PinDiameterBtm], [PinTaper]) WITH (FILLFACTOR = 90);

