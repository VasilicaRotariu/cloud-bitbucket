﻿CREATE TABLE [dbo].[MDEGLowVolumeSerialNumbers] (
    [SerialNumber] VARCHAR (12)  NOT NULL,
    [TDate]        DATETIME2 (7) NULL,
    PRIMARY KEY CLUSTERED ([SerialNumber] ASC) WITH (FILLFACTOR = 80)
);

