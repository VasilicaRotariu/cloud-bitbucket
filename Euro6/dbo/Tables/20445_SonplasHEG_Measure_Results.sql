﻿CREATE TABLE [dbo].[20445_SonplasHEG_Measure_Results] (
    [Uid]         INT          IDENTITY (1, 1) NOT NULL,
    [PDate]       DATETIME     NOT NULL,
    [Serial]      VARCHAR (15) NOT NULL,
    [OnLoadDate]  DATETIME     NULL,
    [OffLoadDate] DATETIME     NULL,
    [Product]     VARCHAR (50) NULL,
    [TestPlan]    VARCHAR (50) NULL,
    [Mode]        VARCHAR (15) NULL,
    [Batch]       VARCHAR (10) NULL,
    [Order]       VARCHAR (80) NULL,
    [Component]   VARCHAR (30) NULL,
    [Comment]     VARCHAR (35) NULL,
    [PartDate]    VARCHAR (10) NULL,
    [Pressure_1]  FLOAT (53)   NULL,
    [BPressure_1] FLOAT (53)   NULL,
    [Flow_1]      FLOAT (53)   NULL,
    [Temp_1]      FLOAT (53)   NULL,
    [Pressure_2]  FLOAT (53)   NULL,
    [BPressure_2] FLOAT (53)   NULL,
    [Flow_2]      FLOAT (53)   NULL,
    [Temp_2]      FLOAT (53)   NULL,
    [DeltaQ]      FLOAT (53)   NULL,
    [DI15]        INT          NULL,
    [CycleTime]   FLOAT (53)   NULL,
    [Failcode]    VARCHAR (40) NULL,
    [SerialCount] INT          NULL
);


GO
CREATE CLUSTERED INDEX [IX_20445_SonplasHEG_Measure_Results]
    ON [dbo].[20445_SonplasHEG_Measure_Results]([PDate] DESC, [Serial] ASC);


GO


-- =============================================
-- Author:		Guillaume Carre 
-- Create date: 23/09/2014
-- Description:	trigger counting the number of time this component has been built 
-- =============================================
CREATE TRIGGER [dbo].[AfterInsert20445_SonplasHEG_Measure_Results]  ON [dbo].[20445_SonplasHEG_Measure_Results]  AFTER INSERT
AS
    BEGIN
        -- Don't show messages 
        SET NOCOUNT ON;
        -- Temporary table to hold records to update
        DECLARE @tbl TABLE([Row] INT, [PDate] DATETIME, [Serial] VARCHAR(max));
        -- Insert records to update
        INSERT INTO @tbl
        SELECT TOP 100 ROW_NUMBER() OVER(ORDER BY [PDate]) AS [Row], [PDate], [Serial] 
        FROM [dbo].[20445_SonplasHEG_Measure_Results] AS npf
        WHERE [SerialCount] IS NULL
        ORDER BY [PDate] ASC;
        -- Temp variables for loop
        DECLARE @Row INT, @Rows INT, @DMCount INT, @DM VARCHAR(max), @PD DATETIME;
        -- Count rows inserted into temp table
        SELECT @Rows=@@ROWCOUNT;
        -- Start at first row
        SELECT @Row=1;
        -- Loop each row
        WHILE(@Row<=@Rows)
            BEGIN
                -- Grab Matrix & PDate for this loop          
                SELECT @DM=[Serial], @PD=[PDate] FROM @tbl WHERE [Row]=@Row;
                -- Count instances for this matrix prior to this date
                SELECT @DMCount=ISNULL(COUNT(1),0)+1 FROM [dbo].[20445_SonplasHEG_Measure_Results] WHERE [Serial]=@DM AND [PDate]<@PD;
                -- Update the specific record in the live table
                UPDATE [dbo].[20445_SonplasHEG_Measure_Results] SET [SerialCount] = @DMCount WHERE [PDate]=@PD AND [Serial]=@DM;
                -- Increment the loop count
                SET @Row = @Row + 1;
            END
        -- Show messages
        SET NOCOUNT OFF;

    END


