﻿CREATE TABLE [dbo].[NozzleRebuildReasons] (
    [NozzleRebuildReasonUID] INT          IDENTITY (1, 1) NOT NULL,
    [RebuildReason]          VARCHAR (50) NOT NULL
);

