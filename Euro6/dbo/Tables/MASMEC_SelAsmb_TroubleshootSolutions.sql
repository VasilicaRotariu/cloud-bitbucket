﻿CREATE TABLE [dbo].[MASMEC_SelAsmb_TroubleshootSolutions] (
    [SolutionID] INT            NOT NULL,
    [CauseID]    INT            NULL,
    [Solution]   VARCHAR (1000) NOT NULL,
    [Severity]   INT            CONSTRAINT [DF__MASMEC_Se__Sever__4E88ABD4] DEFAULT ((1)) NOT NULL,
    [InUse]      BIT            DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_MASMEC_SelAsmb_troubleshootSolution] PRIMARY KEY CLUSTERED ([SolutionID] ASC) WITH (FILLFACTOR = 80)
);

