﻿CREATE TABLE [dbo].[20128_UVA_Nomyline] (
    [UID]                INT           IDENTITY (1, 1) NOT NULL,
    [Chuck]              NVARCHAR (50) NOT NULL,
    [BoreDiameter]       FLOAT (53)    NOT NULL,
    [ODDiameter]         FLOAT (53)    NOT NULL,
    [RepeatSeatDress]    INT           NOT NULL,
    [RepeatBoreDress]    INT           NOT NULL,
    [SeatDressCounter]   INT           NOT NULL,
    [BoreDressCounter]   INT           NOT NULL,
    [WheelChangeCounter] INT           NOT NULL,
    [DateTime]           DATETIME      NOT NULL,
    CONSTRAINT [PK_20128] PRIMARY KEY CLUSTERED ([UID] ASC) WITH (FILLFACTOR = 80)
);

