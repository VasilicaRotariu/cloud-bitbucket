﻿CREATE TABLE [dbo].[SA3_PinHistory] (
    [PinHistory_UID]      INT             NOT NULL,
    [PinType_UID]         INT             NOT NULL,
    [Pin_UID]             INT             NOT NULL,
    [EventUTC]            DATETIME2 (7)   NOT NULL,
    [EventType]           INT             CONSTRAINT [DF_SA3_PinHistory_EventType] DEFAULT ((0)) NOT NULL,
    [PinState]            INT             NOT NULL,
    [PinFlag]             INT             NOT NULL,
    [RemovedByFinalise]   BIT             CONSTRAINT [DF_SA3_PinHistory_RemovedByFinalise1] DEFAULT ((0)) NOT NULL,
    [PinGuideDiameterTop] DECIMAL (12, 8) NULL,
    [PinGuideDiameterBtm] DECIMAL (12, 8) NULL,
    [PinSeatLength]       DECIMAL (12, 8) NULL,
    [PinCollarDiameter]   DECIMAL (12, 8) NULL,
    [Area]                INT             NULL,
    [CIndex]              INT             NULL,
    [RIndex]              INT             NULL,
    CONSTRAINT [PK_SA3_PinHistory] PRIMARY KEY CLUSTERED ([PinHistory_UID] ASC) WITH (FILLFACTOR = 80)
);

