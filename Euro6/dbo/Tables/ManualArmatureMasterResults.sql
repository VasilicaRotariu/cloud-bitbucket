﻿CREATE TABLE [dbo].[ManualArmatureMasterResults] (
    [PDate]             DATETIME2 (7) NOT NULL,
    [IDate]             DATETIME2 (7) NULL,
    [PlantNumber]       INT           NOT NULL,
    [DataMatrix]        VARCHAR (12)  NOT NULL,
    [DataMatrix_Result] TINYINT       NOT NULL,
    [IsNCV]             BIT           NOT NULL,
    [Master_Result]     TINYINT       NOT NULL,
    [FAG]               FLOAT (53)    NOT NULL,
    [Tilt]              FLOAT (53)    NOT NULL,
    [Master_Type]       TINYINT       NOT NULL,
    [Master_Nominal]    FLOAT (53)    NOT NULL,
    [Master_Tolerance]  FLOAT (53)    NOT NULL,
    [TDate]             DATETIME2 (7) CONSTRAINT [DF_ManualArmatureMasterResults_TDate] DEFAULT (sysdatetime()) NOT NULL
);

