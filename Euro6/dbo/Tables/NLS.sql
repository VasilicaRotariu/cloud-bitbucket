﻿CREATE TABLE [dbo].[NLS] (
    [PDate]                        DATETIME      NOT NULL,
    [PlantNumber]                  SMALLINT      NOT NULL,
    [Assembly_Modality_Code]       VARCHAR (50)  NOT NULL,
    [Injector_ID]                  VARCHAR (10)  NOT NULL,
    [Nozzle_DataMatrix]            VARCHAR (20)  NOT NULL,
    [TEST_NUMBER]                  SMALLINT      NOT NULL,
    [LineID]                       SMALLINT      NOT NULL,
    [InjectorCode]                 VARCHAR (10)  NOT NULL,
    [UserName]                     VARCHAR (10)  NULL,
    [InDate]                       DATETIME      NULL,
    [OutDate]                      DATETIME      NULL,
    [GLOBAL_RESULT]                SMALLINT      NULL,
    [Result_Load]                  SMALLINT      NULL,
    [PreLoadValue]                 REAL          NULL,
    [PreLoadError]                 REAL          NULL,
    [SetPoint_Load]                REAL          NULL,
    [Result_SHIM]                  SMALLINT      NULL,
    [SHIM_Standard]                REAL          NULL,
    [SHIM_Measured]                REAL          NULL,
    [SHIM_Required]                REAL          NULL,
    [Result_Stroke]                SMALLINT      NULL,
    [Stroke_Measured]              REAL          NULL,
    [ShiftNumber]                  SMALLINT      NULL,
    [SHIM_Modality]                SMALLINT      NULL,
    [SHIM_Verify]                  BIT           NULL,
    [Final_Verify]                 SMALLINT      NULL,
    [Final_Verify_Frequency]       SMALLINT      NULL,
    [Result_Load_Ver]              SMALLINT      NULL,
    [PreLoadValue_Ver]             REAL          NULL,
    [PreLoadError_Ver]             REAL          NULL,
    [Result_Stroke_Ver]            SMALLINT      NULL,
    [Stroke_Measured_Ver]          REAL          NULL,
    [Spring_Rate]                  REAL          NULL,
    [Spring_Rate_Min]              REAL          NULL,
    [Spring_Rate_Max]              REAL          NULL,
    [Spring_Rate_Result]           SMALLINT      NULL,
    [HysteresisMeasure]            REAL          NULL,
    [HysteresisMeasureResult]      SMALLINT      NULL,
    [HysteresisVerification]       REAL          NULL,
    [HysteresisVerificationResult] SMALLINT      NULL,
    [Spring_Rate_Ver]              REAL          NULL,
    [Spring_Result]                SMALLINT      NULL,
    [Spring_Min]                   REAL          NULL,
    [Spring_Max]                   REAL          NULL,
    [Spring_Value]                 REAL          NULL,
    [Resistor_Min]                 REAL          NULL,
    [Resistor_Max]                 REAL          NULL,
    [Resistor_Value]               REAL          NULL,
    [Insulation_Time]              INT           NULL,
    [Insulation_Min]               INT           NULL,
    [Insulation_Value]             INT           NULL,
    [TAG_Code]                     VARCHAR (10)  NULL,
    [Nozzle_DataMatrixCount]       BIGINT        CONSTRAINT [DF_NLS_Nozzle_DataMatrixCount] DEFAULT ((0)) NULL,
    [TDate]                        DATETIME2 (7) NULL
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150622-160319]
    ON [dbo].[NLS]([Injector_ID] ASC)
    INCLUDE([Nozzle_DataMatrix]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150622-160251]
    ON [dbo].[NLS]([Nozzle_DataMatrix] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Global_result]
    ON [dbo].[NLS]([GLOBAL_RESULT] ASC)
    INCLUDE([PDate], [Injector_ID], [Nozzle_DataMatrix], [InjectorCode]) WITH (FILLFACTOR = 90);


GO
CREATE TRIGGER [dbo].[UpdateNLSComponentTotals]
   ON  [dbo].[NLS]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
        -- Temporary table to hold records to update
        DECLARE @tbl TABLE([Row] INT, [PDate] DATETIME, [Nozzle_DataMatrix] VARCHAR(max));
        -- Insert records to update
        INSERT INTO @tbl
        SELECT TOP 5 ROW_NUMBER() OVER(ORDER BY [PDate]) AS [Row], [PDate], [Nozzle_DataMatrix] 
        FROM [dbo].[NLS] AS npf
        WHERE [Nozzle_DataMatrixCount] IS NULL or [Nozzle_DataMatrixCount] = 0
        ORDER BY [PDate] ASC;
        -- Temp variables for loop
        DECLARE @Row INT, @Rows INT, @DMCount INT, @DM VARCHAR(max), @PD DATETIME;
        -- Count rows inserted into temp table
        SELECT @Rows=@@ROWCOUNT;
        -- Start at first row
        SELECT @Row=1;
        -- Loop each row
        WHILE(@Row<=@Rows)
            BEGIN
                -- Grab Matrix & PDate for this loop          
                SELECT @DM=[Nozzle_DataMatrix], @PD=[PDate] FROM @tbl WHERE [Row]=@Row;
                -- Count instances for this matrix prior to this date
                SELECT @DMCount=ISNULL(COUNT(1),0)+1 
				from(
					SELECT PDate, [Nozzle_DataMatrix]
					FROM [Euro6].[dbo].[NLS]
					WHERE [Nozzle_DataMatrix] = @DM AND [PDate]<@PD
					union
					SELECT PDate, NozzleSerialNumber
					FROM [Euro6].[dbo].[MDEGLowVolumeLineResults] 
					WHERE NozzleSerialNumber = @DM AND [PDate]<@PD) a
                -- Update the specific record in the live table
                UPDATE [dbo].[NLS] SET [Nozzle_DataMatrixCount] = @DMCount WHERE [PDate]=@PD AND [Nozzle_DataMatrix]=@DM;
                -- Increment the loop count
                SET @Row = @Row + 1;
            END
        -- Show messages
        SET NOCOUNT OFF;
END
